#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Usage: completion-server.sh [port]"
    echo ""
    echo "Start a server for code completion"
	exit 1
fi 

env JAVA_OPS="-Xmx16g" sbt --error "run-main edu.rice.pliny.main.CodeCompletionServer $1"
