#!/bin/bash

if [ "$#" -lt 1 ]; then
	  echo "Usage: query_pdb_java.sh [partial program pathname] (method_name)"
    echo ""
    echo "Query the Java PDB using the input program"
	  exit 1
fi

if [ ! -f "$1" ]; then
	  echo "Source file does not exist: $1"
	  exit 1
fi

if [ "$#" -ge 2 ]; then
    sbt "code_completion/runMain edu.rice.pliny.main.QueryJavaPDBMain $1 $2"
else
    sbt "code_completion/runMain edu.rice.pliny.main.QueryJavaPDBMain $1"
fi
    

