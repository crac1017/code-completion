#!/bin/bash

if [ "$#" -eq 0 ]; then
    sbt test
elif [ "$#" -eq 1 ]; then
    sbt "testOnly $1"
else
	  echo "Usage: unit_test.sh (test class)"
    echo ""
    echo "Run all the unit tests. A test class could be provided as an argument."
    exit 1
fi 

