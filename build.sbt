name := "code-completion"
organization in ThisBuild := "edu.rice.pliny"
scalaVersion in ThisBuild := "2.12.3"

lazy val global = project
  .in(file("."))
  .settings(settings)
  .aggregate(
    apted,
    bayou,
    beanshell,
    pliny_source_feature,
    code_completion
  )

lazy val pliny_source_feature = project
  .settings(
    name := "pliny_source_feature",
    settings,
    libraryDependencies ++= Seq (
      "org.glassfish" % "javax.json" % "1.0.4",
      "org.slf4j" % "slf4j-api" % "1.7.21",
      "org.tinylog" % "tinylog" % "1.1",
      "org.tinylog" % "slf4j-binding" % "1.1",
      "com.github.fge" % "json-schema-validator" % "2.2.6",
      "commons-io" % "commons-io" % "2.5",
      "org.eclipse.jdt" % "org.eclipse.jdt.core" % "3.10.0",
      "junit" % "junit" % "4.12",
      "com.novocode" % "junit-interface" % "0.11" % "test"
    )
  )

lazy val bayou = project
  .settings(
    resolvers += Resolver.url("Eclipse release", url("https://repo.eclipse.org/content/groups/releases/")),
    name := "bayou",
    unmanagedBase := baseDirectory.value / "lib",
    settings,
    libraryDependencies ++= Seq (
      "com.google.code.gson" % "gson" % "2.8.0",
      "org.eclipse.platform" % "org.eclipse.equinox.app" % "1.3.500",
      "org.eclipse.jdt" % "org.eclipse.jdt.core" % "3.14.0",
      // "org.slf4j" % "slf4j-nop" % "1.7.25",
      "net.openhft" % "compiler" % "2.3.0",
      "commons-io" % "commons-io" % "2.5",
      "org.apache.httpcomponents" % "httpclient" % "4.5.2",
      "org.apache.logging.log4j" % "log4j-core" % "2.9.1",
      "org.apache.logging.log4j" % "log4j-api" % "2.9.1",
      "commons-cli" % "commons-cli" % "1.4",
      "org.apache.commons" % "commons-lang3" % "3.5",
      "junit" % "junit" % "4.12",
      "org.json" % "json" % "20170516",
      "com.amazonaws" % "aws-java-sdk" % "1.11.136",
      "org.eclipse.jetty" % "jetty-servlet" % "9.4.5.v20170502",
      "org.mockito" % "mockito-all" % "1.10.19" % "test",
      "org.apache.commons" % "commons-math3" % "3.6.1",
      "org.apache.commons" % "commons-text" % "1.2",
      "com.google.guava" % "guava" % "23.1-jre",
      "com.google.googlejavaformat" % "google-java-format" % "1.5",
      "org.reflections" % "reflections" % "0.9.11"
      /*
      "org.eclipse.jdt" % "org.eclipse.jdt.core" % "3.12.3",
      */
    )
  )

lazy val beanshell = project
  .settings(
    name := "beanshell",
    settings,
    libraryDependencies ++= Seq (
      "junit" % "junit" % "4.12" % "test",
      "bsf" % "bsf" % "2.4.0",
      "javax.servlet" % "servlet-api" % "2.5" % "provided"
    )
  )

lazy val apted = project
  .settings(
    name := "apted",
    settings,
    libraryDependencies ++= Seq (
      "junit" % "junit" % "4.12" % "test",
      "com.google.code.gson" % "gson" % "2.8.2" % "provided"
    )
  )

lazy val code_completion = project
  .settings(
    name := "code_completion",
    settings,
    mainClass in (Compile, run) := Some("edu.rice.pliny.codecomplete.main.CodeCompletionMain"),
    unmanagedBase := baseDirectory.value / "lib",
    libraryDependencies ++= Seq(
      "org.tinylog" % "tinylog" % "1.3.4",
      "org.tinylog" % "slf4j-binding" % "1.3.4",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",

      // api translation examples
      "org.apache.poi" % "poi" % "3.17",
      "org.apache.poi" % "poi-ooxml" % "3.17",
      "org.apache.tika" % "tika-core" % "1.18",
      "org.docx4j" % "docx4j" % "3.3.7" exclude("org.slf4j", "*"),
      "com.itextpdf" % "itextpdf" % "5.5.13",
      "org.apache.pdfbox" % "pdfbox" % "2.0.9",
      "com.opencsv" % "opencsv" % "4.1",
      "org.asynchttpclient" % "async-http-client" % "2.4.4",
      "com.squareup.okhttp3" % "okhttp" % "3.10.0",
      "org.jsoup" % "jsoup" % "1.11.2",
      "com.github.penggle" % "htmlcleaner" % "2.21",
      "com.edwardraff" % "JSAT" % "0.0.9",
      "com.github.haifengl" %% "smile-scala" % "1.5.1",
      "org.nd4j" % "nd4j-native-platform" % "0.9.1",
      "com.j2html" % "j2html" % "1.2.2",
      "com.hp.gagawa" % "gagawa" % "1.0.1",
      "org.apache.commons" % "commons-email" % "1.5",
      "ca.pjer" % "ekmeans" % "2.0.0",
      "fr.inria.gforge.spoon" % "spoon-core" % "6.2.0" exclude("org.eclipse.jdt", "*"),
      "org.glassfish.grizzly" % "grizzly-http-server" % "2.4.0",
      "org.slick2d" % "slick2d-core" % "1.0.2",
      "ca.pjer" % "ekmeans" % "2.0.0",
      "com.hierynomus" % "sshj" % "0.24.0",
      "com.sparkjava" % "spark-core" % "2.7.2",
      "org.jtwig" % "jtwig-core" % "5.87.0.RELEASE",
      "com.google.crypto.tink" % "tink" % "1.2.2",
      "org.la4j" % "la4j" % "0.6.0",

      "org.mockito" % "mockito-core" % "2.27.0",
      "org.xerial" % "sqlite-jdbc" % "3.27.2.1",
      "com.google.guava" % "guava" % "24.1-jre",
      "org.deeplearning4j" % "deeplearning4j-core" % "1.0.0-beta2",
      "org.nd4j" % "nd4j-native-platform" % "1.0.0-beta2" exclude("org.slf4j", "*"),
      "org.deeplearning4j" % "deeplearning4j-nlp" % "1.0.0-beta2" exclude("org.slf4j", "*"),
      "org.deeplearning4j" % "deeplearning4j-nlp-uima" % "1.0.0-beta2",
      // "org.graphstream" % "gs-core" % "1.3",
      // "org.graphstream" % "gs-ui" % "1.3",
      // "org.graphstream" % "gs-algo" % "1.3",
      "edu.stanford.nlp" % "stanford-corenlp" % "3.9.1",
      "commons-io" % "commons-io" % "2.6",
      "com.github.javaparser" % "java-symbol-solver-logic" % "0.6.0",
      "com.github.javaparser" % "java-symbol-solver-model" % "0.6.0",
      "com.github.javaparser" % "java-symbol-solver-core" % "0.6.0",
      "com.typesafe.akka" %% "akka-actor" % "2.5.4",
      "org.eclipse.jdt" % "org.eclipse.jdt.core" % "3.10.0",
      "org.glassfish" % "javax.json" % "1.0.4",
      "com.typesafe" % "config" % "1.3.1",
      "com.martiansoftware" % "nailgun-server" % "0.9.1",
      "org.eclipse.jetty" % "jetty-server" % "9.3.12.v20160915",
      "org.eclipse.jetty" % "jetty-servlet" % "9.3.12.v20160915",
      "junit" % "junit" % "4.12" % "test",
      "io.spray" %%  "spray-json" % "1.3.2",
      // "org.apache-extras.beanshell" % "bsh" % "2.0b6",
      // "org.slf4j" % "slf4j-api" % "1.7.25",
      // "org.scalaj" %% "scalaj-collection" % "1.6",
      "com.lihaoyi" %% "fastparse" % "1.0.0",
      "org.scalactic" %% "scalactic" % "3.0.1",
      "org.scalatest" %% "scalatest" % "3.0.1" % "test"
      // "com.novocode" % "junit-interface" % "0.10-M1" % "test"
    ),
    // libraryDependencies ~= { _.map(_.exclude("org.slf4j", "*")) }
  )
  .dependsOn(
    beanshell,
    apted,
    bayou,
    pliny_source_feature
  )

version := "0.1"

lazy val settings = Seq (
  parallelExecution in Test := false,
  fork in ThisBuild in Test := false,
  showSuccess := false,
  logLevel in run := sbt.Level.Warn,

)
lazy val http4sVersion = "0.14.3"

