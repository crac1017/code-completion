#!/bin/bash

function run_map {
    echo "Mapping $1 benchmark program..."
    # env JAVA_OPTS="-Xmx32G" sbt --error "code_completion/runMain edu.rice.pliny.apitrans.MapAPIMain $1 $2 $3"
    env JAVA_OPTS="-Xmx32G" sbt --error "code_completion/runMain edu.rice.pliny.apitrans.MapAPIMain $1"
    echo "Renaming log file to $1.log"
    mv -f code_completion.log "$1.log"
}

run_map csv 
run_map csv3 
run_map csv_db
run_map csv_delim 
run_map email_check
run_map email_delete
run_map email_login
run_map email_send
run_map ftp
run_map ftp_delete
run_map ftp_download
run_map ftp_login
run_map ftp_upload
run_map graphics
run_map gui
run_map htm
run_map htm_addattr
run_map htm_addnode
run_map htm_parse
run_map htm_rmattr
run_map htm_title

# no results for html3 because they are all object creation calls
run_map htm3
run_map http
run_map http_post
run_map httpserver
run_map ml_classification
run_map ml_cluster
run_map ml_neural
run_map ml_regression
run_map ml_stemming
run_map nlp_pos
run_map nlp_sentence
run_map nlp_token
run_map pdf_read
run_map pdf_write
run_map word
run_map word3
