#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Usage: complete.sh [partial program pathname] (class name) (method name) (jar paths) (dir paths)"
    echo ""
    echo "Complete a partial Java program"
	exit 1
fi 

if [ ! -f "$1" ]; then
	echo "Source file does not exist: $1"
	exit 1
fi 

env JAVA_OPS="-Xmx16g" sbt --error "run-main edu.rice.pliny.main.CodeCompletionMain $1 $2 $3 $4 $5"
