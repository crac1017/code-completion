#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "Usage: query_mapper.sh [qualified method name] [package name] [k]"
    echo ""
    echo "Query the mapper using a qualified method name and find similar methods in the provided package"
	exit 1
fi 

env JAVA_OPTS="-Xmx32G" sbt --error "code_completion/runMain edu.rice.pliny.apitrans.QueryMethodMain $1 $2 $3"
