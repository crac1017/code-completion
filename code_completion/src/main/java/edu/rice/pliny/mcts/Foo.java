package edu.rice.pliny.mcts;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.IOException;
import java.net.InetSocketAddress;

public class Foo {
    static public void main(String[] args) {
        System.out.println("Hello world.");
    }


    public void run(HttpHandler handler, int port, String url) throws IOException {

        //refactor:org.glassfish
        {
            InetSocketAddress address = new InetSocketAddress(port);
            HttpServer server = HttpServer.create(address, 0);
            server.createContext(url, handler);
            server.start();
        }
    }

    public void list_files(String username, String password, String host, String path) throws IOException {
        //refactor:net.schmizz
        {
            FTPClient f = new FTPClient();
            f.connect(host);
            f.login(username, password);
            FTPFile[] files = f.listFiles(path);
            f.disconnect();
        }
    }

    public void read(String filename) throws IOException {
        //refactor:org.apache.pdfbox
        {
            PdfReader reader = new PdfReader(filename);
            String text = PdfTextExtractor.getTextFromPage(reader, 1);
            reader.close();
        }
    }
}
