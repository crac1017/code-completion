package edu.rice.pliny.apitrans.problems;

import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.regression.LogisticRegression;
import jsat.regression.RidgeRegression;
import jsat.clustering.kmeans.NaiveKMeans;
import jsat.classifiers.neuralnetwork.BackPropagationNet;
import jsat.classifiers.DataPoint;

public class ML {
    static public int classify(ClassificationDataSet train_data, DataPoint test_data) {
        LogisticRegression model = new LogisticRegression();
        model.trainC(train_data);
        CategoricalResults r = model.classify(test_data);
        int c = r.mostLikely();
        return c;
    }

    static public int cluster(ClassificationDataSet train_data, int k) {
        NaiveKMeans model = new NaiveKMeans();
        int[] assignments = new int[train_data.getSampleSize()];
        model.cluster(train_data, assignments);
        return assignments[0];
    }

    static public double regress(RegressionDataSet train_data, DataPoint test_data) {
        RidgeRegression model = new RidgeRegression(0.1);
        model.train(train_data);
        double d = model.regress(test_data);
        return d;
    }

    static public double neural(RegressionDataSet train_data, DataPoint test_data) {
        BackPropagationNet model = new BackPropagationNet();
        model.train(train_data);
        double d = model.regress(test_data);
        return d;
    }
}
