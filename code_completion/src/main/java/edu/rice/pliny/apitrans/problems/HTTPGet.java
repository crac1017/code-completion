package edu.rice.pliny.apitrans.problems;

import okhttp3.Call;
import okhttp3.OkHttpClient;

import java.io.IOException;
import java.net.URL;

public class HTTPGet {
    static public String get(URL url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        okhttp3.Request.Builder builder = new okhttp3.Request.Builder();
        builder.url(url);
        okhttp3.Request request = builder.build();
        Call call = client.newCall(request);
        okhttp3.Response response = call.execute();
        okhttp3.ResponseBody rbody = response.body();
        String text = rbody.string();
        return text;
    }
}
