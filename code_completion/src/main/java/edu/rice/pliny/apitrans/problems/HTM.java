package edu.rice.pliny.apitrans.problems;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;
import java.lang.System;

public class HTM {

    static public String add_attr(String content, String id, String attr, String attr_val) {
        Document doc = Jsoup.parse(content);
        Element target = doc.getElementById(id);
        Element e = target.attr(attr, attr_val);
        return doc.toString();
    }


    static public String add_node(String content, String id, String new_html) {
        Document doc = Jsoup.parse(content);
        Element target = doc.getElementById(id);
        Element e = target.appendElement(new_html);
        return doc.toString();
    }

    static public String get_link(String content) {
        Document doc = Jsoup.parse(content);
        Elements links = doc.getElementsByAttribute("href");
        Element link_tag = links.first();
        String href = link_tag.attr("href");
        return href;
    }

    static public String rm_attr(String content, String attr) {
        Document doc = Jsoup.parse(content);
        Elements nodes = doc.getElementsByAttribute(attr);
        Element target = nodes.first();
        Node n = target.removeAttr(attr);
        return doc.toString();
    }
}
