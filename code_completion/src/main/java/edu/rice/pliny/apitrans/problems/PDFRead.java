package edu.rice.pliny.apitrans.problems;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;

public class PDFRead {
    static public String read(String filename) throws IOException {
        File infile = new File(filename);
        PDDocument document = PDDocument.load(infile);
        PDFTextStripper stripper = new PDFTextStripper();
        String text = stripper.getText(document);
        document.close();
        return text;
    }
}
