package edu.rice.pliny.apitrans.problems;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CSV {
    static public String[] readRow(String filename) throws IOException {
        File f = new File(filename);
        FileReader fr = new FileReader(f);
        CSVReader csv_reader = new CSVReader(fr);
        String[] fields = csv_reader.readNext();
        return fields;
    }


    static public void writeRow(String filename, String[] entries) throws IOException {
        FileWriter fw = new FileWriter(filename);
        CSVWriter writer = new CSVWriter(fw);
        writer.writeNext(entries, false);
        writer.close();
    }
}