package edu.rice.pliny.apitrans.problems;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import java.io.IOException;

public class FTP {
    static public void login(String username, String password, String host, SSHClient ssh) throws IOException {
        ssh.authPassword(username, password);
        ssh.connect(host);
        ssh.disconnect();
    }

    static public void list_files(String username, String password, String host, String path, SSHClient ssh, SFTPClient sftp) throws IOException {
        ssh.authPassword(username, password);
        ssh.connect(host);
        sftp.ls(path);
        sftp.close();
        ssh.disconnect();
    }

}
