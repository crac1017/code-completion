package edu.rice.pliny.apitrans.problems;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.ServerConfiguration;

import java.io.IOException;

public class HServer{

    static public HttpServer run(int port, String url, HttpHandler handler) throws IOException {
        HttpServer server = HttpServer.createSimpleServer(null, "localhost", port);
        ServerConfiguration config = server.getServerConfiguration();
        config.addHttpHandler(handler, url);
        server.start();
        return server;
    }
}
