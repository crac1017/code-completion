package edu.rice.pliny

import com.typesafe.config.{Config, ConfigFactory}
import org.pmw.tinylog.writers.{ConsoleWriter, FileWriter}
import org.pmw.tinylog.{Configurator, Level}

/**
  * A config object for the entire application
  */
object AppConfig {
  val log_config : Configurator = Configurator.currentConfig()
  log_config.removeAllWriters()

  // config for application level
  val app_conf : Config = ConfigFactory.load()
  val log_level : String = app_conf.getString("log_level")
  val log_file : String = app_conf.getString("log_to_file")
  val log_to_console : Boolean = app_conf.getBoolean("log_to_console")
  val using_parllel : Boolean = app_conf.getBoolean("parallel")

  assume(log_file != "", "Log file cannot be empty.")
  log_config.addWriter(new FileWriter(log_file))
  if(log_to_console) log_config.addWriter(new ConsoleWriter())
  log_config.level(Level.valueOf(log_level))
  log_config.activate()

  // config for the interpreter
  val interpreter_conf : Config = app_conf.getConfig("interpreter")
  object InterpreterConfig {

    // timeout for running each java code snippet
    var time_limit: Int = interpreter_conf.getInt("time_limit")
  }

  val translation_conf : Config = app_conf.getConfig("translation")
  object TranslationConfig {

    var algorithm : String = translation_conf.getString("algorithm")

    // Word2vec related config
    var word2vec_epoch : Int = translation_conf.getInt("word2vec_epoch")
    var word2vec_filename : String = translation_conf.getString("word2vec_filename")
    var word2vec_wordvec_filename : String = translation_conf.getString("word2vec_wordvec_filename")
    var tsne_iter : Int = translation_conf.getInt("tsne_iter")
    var tsne_nwords : Int = translation_conf.getInt("tsne_nwords")
    var tsne_words_filename : String = translation_conf.getString("tsne_words_filename")

    // fastext config
    var fasttext_vec_filename : String = translation_conf.getString("fasttext_vec_filename")

    // cache config
    var cache_filename : String = translation_conf.getString("cache_filename")
    var cache_use : Boolean = translation_conf.getBoolean("use_cache")
    var use_bert : Boolean = translation_conf.getBoolean("use_bert")
    var bert_embedding_filename : String = translation_conf.getString("bert_embedding_filename")
  }

  // config for PDB
  val pdb_conf : Config = app_conf.getConfig("pdb")
  object PDBConfig {
    var path : String = pdb_conf.getString("path")
    var query_script_name : String = pdb_conf.getString("query_script_name")
    var query_result_name : String = pdb_conf.getString("query_result_name")
    var nl_terms_path : String = pdb_conf.getString("nl_terms_path")

    var db : String = pdb_conf.getString("db")
    var set : String = pdb_conf.getString("set")
    var hostname : String = pdb_conf.getString("hostname")
    var port : Int = pdb_conf.getInt("port")
    var logfile : String = pdb_conf.getString("logfile")
    var cluster_mode : Boolean = pdb_conf.getBoolean("cluster_mode")
    var memory : Int = pdb_conf.getInt("memory")
    val query_conf : Config = pdb_conf.getConfig("query")
    object QueryConfig {
      var weighted_nl_terms : Double = query_conf.getDouble("weighted_nl_terms")
      var weighted_nl_terms_intersect : Double = query_conf.getDouble("weighted_nl_terms_intersect")
      var names : Double = query_conf.getDouble("names")
    }
  }

  // config for code completion
  val code_completion_conf : Config = app_conf.getConfig("code_completion")
  object CodeCompletionConfig {
    var solution_num : Int = code_completion_conf.getInt("solution_num")
    assume(solution_num > 0, "Solution num is too small.")

    var time_limit : Int = code_completion_conf.getInt("time_limit")
    assume(time_limit > 0, "Time limit should be greater than 0.")

    var query_type : String = code_completion_conf.getString("query_type")
    var local_dir_path : String = code_completion_conf.getString("local_dir_path")
    var local_json_path : String = code_completion_conf.getString("local_json_path")

    var output : String = code_completion_conf.getString("output")
    var db_prog_num : Int = code_completion_conf.getInt("db_prog_num")
  }
}
