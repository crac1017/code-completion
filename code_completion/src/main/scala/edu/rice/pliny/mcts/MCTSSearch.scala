package edu.rice.pliny.mcts

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.mcts.evaluator.{NodeEvaluator, TestCaseEvaluator}
import edu.rice.pliny.mcts.vis.TreeVisualizer

import scala.collection.mutable


/**
  * This is the main class for monte carlo tree search algorithm
  */
class MCTSSearch(val evaluator : NodeEvaluator) {
  val logger = Logger(this.getClass)
  val EPS : Double = 1e-6

  // this is the result of this MCTS task
  var search_result : SearchResult = NoSolution()

  /**
    * Expand the leaf and generate one or more children
    */
  private def expand(leaf : MCTSTree) : Unit = {
    logger.debug("Expanding ================\n" + leaf.toString)
    val new_children = leaf.get_valid_moves
    // new_children.foreach(child => child.set_prior(prior_eval.evaluate(child)))
    new_children.foreach(child => child.set_reward(evaluator.evaluate(child)))
    leaf.add_children(new_children)
  }

  /**
    * Select the best child
    */
  def select_best(tree : MCTSTree, include_deadend : Boolean = false) : Option[MCTSTree] = {
    val valid_children = tree.get_children.filter(c => include_deadend || !c.is_deadend)
    if(valid_children.isEmpty) {
      this.logger.debug("No Best MCTSTree\n")
      return None
    }

    val children = valid_children.map(n => {
      val v = n.get_reward / (n.get_visited.toDouble + this.EPS) +
        (1 / Math.sqrt(2.0)) * Math.sqrt(2.0 * Math.log(tree.get_visited.toDouble + 1.0) / (n.get_visited.toDouble + this.EPS))
      // + Random.nextDouble() * this.EPS
      /*
        */
      /*
      val v = n.get_reward / (n.get_visited + 1).toDouble + Random.nextDouble() * this.EPS
        */
      /*
      val v = n.get_reward + (n.get_prior / Math.max(n.get_visited, 1).toDouble)
      */
      n.set_final_reward(v)
      (n, v)
    })
    children.foreach(c => {
      this.logger.debug("Candidate children with value {} ============\n{}\n", c._2, c._1.toString)
    })

    val best = children.foldLeft(children.head) ((acc, t) => if(acc._2 > t._2) acc else t)._1
    this.logger.debug("Best MCTSTree ============\n{}\n", best.toString)

    Some(best)
  }

  /**
    * Make a node and its parent a deadend if the parent has only one child
    */
  def make_deadend(tree : MCTSTree) : Unit = {
    this.logger.debug("Deadend  =========\n{}\n", tree.toString)
    tree.set_deadend(true)
    if(tree.get_parent.isDefined && tree.get_parent.get.get_children.count(c => !c.is_deadend) == 0) {
      make_deadend(tree.get_parent.get)
    }
  }

  /**
    * Perform the MCTS on the tree once. Return an optional solution tree node
    */
  def grow(tree : MCTSTree) : Unit = {
    this.logger.debug("Growing Tree ============\n{}\n", tree.toString)
    val visited = mutable.ArrayBuffer.empty[MCTSTree]

    // walk all the way down to the leaf
    visited += tree
    def select_leaf_helper(node : MCTSTree) : Unit = {
      if(node.get_children.forall(c => c.is_deadend)) {
        return
      }
      val best = this.select_best(node).get
      visited += best
      select_leaf_helper(best)
    }
    select_leaf_helper(tree)
    val leaf = visited.last

    // expand the leaf
    if(!leaf.is_complete) {
      this.expand(leaf)
    }

    if(leaf.get_children.isEmpty) {
      // we are done with this node. Set the path to a deadend
      this.make_deadend(leaf)
      return
    }

    // check if we have a solution
    for(c <- leaf.get_children.filter(l => l.is_complete)) {
      if(c.is_solution) {
        this.search_result = Solution(tree, c)
        return
      } else {
        this.make_deadend(c)
      }
    }

    val new_best = this.select_best(leaf, include_deadend = true)
    if(new_best.isEmpty) {
      return
    }

    visited += new_best.get

    // calculate the new value and propagate it back to all the parents
    val new_val = this.evaluator.evaluate(new_best.get)
    visited.foreach(n => {
      /*
      */
      n.set_reward(new_val + n.get_reward)
      n.set_visited(n.get_visited + 1)
      /*
      n.set_reward(new_best.get.get_reward + n.get_reward)
      n.set_visited(n.get_visited + 1)
      */
      // n.set_reward((new_val + n.get_reward) / n.get_visited.toDouble)
    })

    // check if we have found a solution when we use test case evaluator
    this.evaluator match {
      case tce : TestCaseEvaluator =>
        if(tce.solution.isDefined) {
          this.search_result = Solution(tree, tce.solution.get)
        }
      case _ =>
    }
  }

  /**
    * Perform the MCTS on the tree. Return a tuple where the first element
    * is the original tree after expansion and the second is an optional
    * solution node
    */
  def complete(starting_tree : MCTSTree, max_iter : Int) : SearchResult = {
    var working_tree = starting_tree

    /**
      * Select the best child using max reward
      */
    def get_best_child(tree : MCTSTree) : Option[MCTSTree] = {
      var result : Option[MCTSTree] = None
      for(c <- tree.get_children) {
        if(result.isEmpty || result.get.get_final_reward < c.get_final_reward) {
          result = Some(c)
        }
      }
      result
    }

    while(!working_tree.is_solution && !working_tree.is_deadend) {
      this.logger.info("Working tree ==================\n{}\n", working_tree.toString)
      for(_ <- 0 until max_iter) {
        this.grow(working_tree)
        this.search_result match {
          case Solution(_, leaf) =>
            this.logger.info("Solution found =============\n{}\n", leaf)
            return this.search_result
          case _ =>
        }
      }

      // pick the best child
      val best_child = get_best_child(working_tree)
      if(best_child.isEmpty) {
        make_deadend(working_tree)
      } else {
        working_tree = best_child.get
      }
      this.logger.info("New working tree ==================\n{}\n", working_tree.toString)
      TreeVisualizer.visualize(starting_tree)
    }
    if(working_tree.is_deadend) {
      this.logger.info("Working tree is deadend.\n")
      NoSolution()
    } else {
      this.logger.info("Solution found =============\n{}\n", working_tree)
      Solution(starting_tree, working_tree)
    }
  }

}
