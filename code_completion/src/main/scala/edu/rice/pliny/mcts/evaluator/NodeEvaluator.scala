package edu.rice.pliny.mcts.evaluator

import edu.rice.pliny.mcts.MCTSTree

/**
  * This is the class used to evaluate a node's value
  */
abstract class NodeEvaluator {
  val BIAS = 0.1
  def evaluate(tree : MCTSTree) : Double
}
