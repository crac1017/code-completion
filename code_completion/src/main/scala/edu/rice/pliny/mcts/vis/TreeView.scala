package edu.rice.pliny.mcts.vis

import java.awt._
import javax.swing.JComponent

import edu.rice.pliny.mcts.MCTSTree

import scala.collection.mutable

/**
 * Created by Simon M. Lucas
 * sml@essex.ac.uk
 * Date: 29-Nov-2010
 * Time: 22:23:45
 * <p/>
 * Simple TreeView for a MCTS Tree
 */
class TreeView(r : MCTSTree) extends JComponent {
  var root : MCTSTree = r
  var nw = 30
  var nh = 20
  var inset = 20
  var minWidth = 300
  var heightPerLevel = 40
  var fg = Color.black
  var bg = Color.cyan
  var nodeBg = Color.white
  var highlighted = Color.red
  // the highlighted set of nodes...
  var high = mutable.HashMap.empty[MCTSTree, Color]

  override def paintComponent(gg : Graphics) : Unit = {
    // Font font =
    // g.setFont();
    val g = gg.asInstanceOf[Graphics2D]
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    val y = inset
    val x = this.getWidth / 2
    g.setColor(bg)
    g.fillRect(0, 0, this.getWidth, this.getHeight)
    this.draw(g, root, x, y, (1.1 * this.getWidth).asInstanceOf[Int] - inset * 0)
  }

  def draw(g : Graphics2D, cur : MCTSTree, x : Int, y : Int, wFac : Int) : Unit = {
    // draw this one, then it's children

    val children = cur.get_children.toArray
    val arity = children.length
    for (i <- 0 until arity) {
      if (children(i).get_visited > 0) {
        val xx = (i + 1.0) * wFac / (arity + 1) + (x - wFac / 2)
        val yy = y + heightPerLevel
        g.setColor(fg)
        g.drawLine(x, y, xx.toInt, yy)
        draw(g, children(i), xx.toInt, yy, wFac / arity)
      }
    }
    drawNode(g, cur, x, y)
  }

  def drawNode(g : Graphics2D, node : MCTSTree, x : Int, y : Int): Unit = {
    val s = node.get_reward + "/" + node.get_visited
    g.setColor(nodeBg)
    // if (high.contains(node)) g.setColor(highlighted);
    g.fillOval(x - nw / 2, y - nh / 2, nw, nh)
    g.setColor(fg)
    g.drawOval(x - nw / 2, y - nh / 2, nw, nh)
    g.setColor(fg)
    val fm = g.getFontMetrics()
    val rect = fm.getStringBounds(s, g)
    g.drawString(s, x - (rect.getWidth / 2).toFloat, (y + rect.getHeight / 2).toFloat)
  }

  override def getPreferredSize() : Dimension = {
    // should make this depend on the tree ...
    new Dimension(minWidth, heightPerLevel * (10 - 1) + inset * 2)
  }

  def showTree(title : String) : TreeView = {
    new JEasyFrame(this, title)
    this
  }
}