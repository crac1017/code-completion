package edu.rice.pliny.mcts.evaluator

import com.github.javaparser.ast.`type`.PrimitiveType
import com.github.javaparser.ast.body.{Parameter, VariableDeclarator}
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.visitor._
import com.github.javaparser.ast.{Node, NodeList}
import com.typesafe.scalalogging.Logger
import costmodel.CostModel
import distance.APTED
import edu.rice.pliny.draft.{Draft, DraftNode}
import edu.rice.pliny.language.java.{JavaDraft, JavaDraftIteratorFactory, JavaUtils}
import edu.rice.pliny.mcts.{DraftTree, MCTSTree}
import node.StringNodeData
import parser.BracketStringInputParser

object DraftCodeModel extends costmodel.CostModel[StringNodeData] {
  val logger = Logger(this.getClass)
  val STMT_COST = 100.0f
  val EXPR_COST = 1.0f
  val LABEL_COST = Map(
    "ArrayAccess" -> EXPR_COST,
    "ArrayCreation" -> EXPR_COST,
    "ArrayInitializer" -> EXPR_COST,
    "Binary" -> EXPR_COST,
    "Cast" -> EXPR_COST,
    "Conditional" -> EXPR_COST,
    "Enclosed" -> EXPR_COST,
    "FieldAccess" -> EXPR_COST,
    "InstanceOf" -> EXPR_COST,
    "Lambda" -> EXPR_COST,
    "MethodCall" -> EXPR_COST,
    "MethodReference" -> EXPR_COST,
    "Name" -> EXPR_COST,
    "NormalAnnotation" -> EXPR_COST,
    "Null" -> EXPR_COST,
    "ObjectCreation" -> EXPR_COST,
    "SingleMemberAnnotation" -> EXPR_COST,
    "String" -> EXPR_COST,
    "Super" -> EXPR_COST,
    "This" -> EXPR_COST,
    "Type" -> EXPR_COST,
    "Unary" -> EXPR_COST,
    "Assign" -> EXPR_COST,
    "AssertStmt" -> STMT_COST,
    "BlockStmt" -> EXPR_COST,
    "BreakStmt" -> EXPR_COST,
    "Catch" -> STMT_COST,
    "ContinueStmt" -> EXPR_COST,
    "DoStmt" -> STMT_COST,
    // "EmptyStmt" -> 0.0f,
    "ExprStmt" -> EXPR_COST,
    "ForeachStmt" -> STMT_COST,
    "ForStmt" -> STMT_COST,
    "IfStmt" -> STMT_COST,
    "LabeledStmt" -> STMT_COST,
    "ReturnStmt" -> EXPR_COST,
    "SwitchEntryStmt" -> STMT_COST,
    "SwitchStmt" -> STMT_COST,
    "SynStmt" -> STMT_COST,
    "ThrowStmt" -> STMT_COST,
    "TryStmt" -> STMT_COST,
    "WhileStm" -> STMT_COST)

  /**
    * Calculates the cost of deleting a node.
    *
    * @param n the node considered to be deleted.
    * @return the cost of deleting node n.
    */
  override def del(n: node.Node[StringNodeData]): Float = {
    val v = this.LABEL_COST.getOrElse(n.getNodeData.getLabel, 1.0f) * n.getNodeCount * 100.0f
    this.logger.info("DEL {}: {}", n.getNodeData.getLabel, v)
    v
  }

  /**
    * Calculates the cost of inserting a node.
    *
    * @param n the node considered to be inserted.
    * @return the cost of inserting node n.
    */
  override def ins(n: node.Node[StringNodeData]): Float = {
    val v = this.LABEL_COST.getOrElse(n.getNodeData.getLabel, 1.0f) * n.getNodeCount
    this.logger.info("INS {}: {}", n.getNodeData.getLabel, v)
    v
  }

  /**
    * Calculates the cost of renaming (mapping) two nodes.
    *
    * @param n1 the source node of rename.
    * @param n2 the destination node of rename.
    * @return the cost of renaming (mapping) node n1 to n2.
    */
  override def ren(n1: node.Node[StringNodeData], n2: node.Node[StringNodeData]): Float = {
    val l1 = n1.getNodeData.getLabel
    val l2 = n2.getNodeData.getLabel
    val v = {
      if(l1 == "#" || l1 == "EmptyStmt") {
        this.ins(n2)
      } else if(l2 == "#" || l2 == "EmptyStmt") {
        this.del(n2)
      } else {
        (this.LABEL_COST.getOrElse(l1, 1.0f) + this.LABEL_COST.getOrElse(l2, 1.0f)) * (n1.getNodeCount + n2.getNodeCount) * 100.0f
      }

    }
    this.logger.info("REN from {} to {}: {}", l1, l2, v)
    v
  }

}

/**
  * This class evaluate a node by looking at how close it is to the
  * given reference program
  */
class SyntaxDistEvaluator(draft : Draft) extends NodeEvaluator {
  val logger = Logger(this.getClass)


  override def evaluate(tree: MCTSTree): Double = {
    val tree_draft = tree.asInstanceOf[DraftTree].draft

    // get the skeleton distance
    /*
    val v = {
      val draft_skel = this.gen_skeleton(draft.asInstanceOf[JavaDraft].method)
      val tree_skel = this.gen_skeleton(tree.asInstanceOf[DraftTree].draft.asInstanceOf[JavaDraft].method)
      this.logger.info("Draft skeleton ==================\n{}\n", draft_skel.toString)
      this.logger.info("Tree skeleton ==================\n{}\n", tree_skel.toString)
      // 1.0 - (this.distance(draft_skel, tree_skel).toDouble / (draft_skel.length + tree_skel.length).toDouble)
      1.0 - tree_dist(draft_skel, tree_skel, DraftCodeModel)
    }
    */
    val v = {
      val draft_skel = abstract_names(draft.asInstanceOf[JavaDraft].method).removeComment().toString
      val tree_skel = abstract_names(tree_draft.asInstanceOf[JavaDraft].method).removeComment().toString
      this.logger.debug("Draft skeleton ==================\n{}\n", draft_skel.toString)
      this.logger.debug("Tree skeleton ==================\n{}\n", tree_skel.toString)
      1.0 - (this.distance(draft_skel, tree_skel).toDouble / (draft_skel.length + tree_skel.length).toDouble)

    }
    // this.logger.info("Calculating ref distance between ============\n{}\n=================\n{}\n", draft.toStringNoComment, tree_draft.toStringNoComment)
    this.logger.debug("Syntax distance is {}", v)
    v


    /*
    // put a limit on the search space
    if(tree_draft.get_expr_count >= draft.get_expr_count * 2 ||
       tree_draft.get_stmt_count >= draft.get_stmt_count * 2 ||
      tree_draft.get_node_count >= draft.get_node_count * 2) {
      return 0.0
    }

    // get the skeleton distance
    val skel_dist =
    {
      val draft_skel = this.gen_skeleton(draft.asInstanceOf[JavaDraft].method).toString
      val tree_skel = this.gen_skeleton(tree.asInstanceOf[DraftTree].draft.asInstanceOf[JavaDraft].method).toString
      this.logger.debug("Draft skeleton ==================\n{}\n", draft_skel.toString)
      this.logger.debug("Tree skeleton ==================\n{}\n", tree_skel.toString)
      1.0 - (this.distance(draft_skel, tree_skel).toDouble / (draft_skel.length + tree_skel.length).toDouble)
      // tree_dist(draft_skel, tree_skel, new PerEditOperationStringNodeDataCostModel(1.0f, 100.0f, 100.0f))
    }

    return skel_dist

    // get the expression distance
    this.logger.debug("Expression distance:\n")
    val expr_dist =
    {
      val draft_exprs = get_all_exprs(draft)
      val tree_exprs = get_all_exprs(tree.asInstanceOf[DraftTree].draft)
      val dist_mat = Array.fill(draft_exprs.length, tree_exprs.length)(1.0)
      for(i <- draft_exprs.indices) {
        for(j <- tree_exprs.indices) {
          dist_mat(i)(j) =
            if (draft.same_role(draft_exprs(i), tree_exprs(j)))
              tree_dist(abstract_names(draft_exprs(i).asInstanceOf[JavaDraftNode].node),
                        abstract_names(tree_exprs(j).asInstanceOf[JavaDraftNode].node),
                new PerEditOperationStringNodeDataCostModel(1.0f, 50.0f, 50.0f))
            else 1.0
          this.logger.debug("{} -> {} = {}", draft_exprs(i).toString, tree_exprs(j).toString, dist_mat(i)(j))
        }
      }

      val alg = new HungarianAlgorithm(dist_mat)
      val mapping = alg.execute()
      var dist = 0.0
      for(i <- 0 until mapping.length) {
        if(mapping(i) > 0) {
          this.logger.debug("Picked {} -> {} = {}", draft_exprs(i).toString, tree_exprs(mapping(i)).toString, dist_mat(i)(mapping(i)))
          dist += dist_mat(i)(mapping(i))
        } else {
          this.logger.debug("Picked {} -> empty", draft_exprs(i).toString)
          dist += 1.0
        }
      }
      dist / mapping.length.toDouble
    }

    // this is the skeleton distance
    /*
    val draft_str = this.abstract_vars(draft.asInstanceOf[JavaDraft]).toStringNoComment
    val tree_str = this.abstract_vars(tree.asInstanceOf[DraftTree].node.asInstanceOf[JavaDraft]).toStringNoComment
    val r = 1.0 - (this.distance(draft_str, tree_str).toDouble / (draft_str.length + tree_str.length).toDouble)
    this.logger.debug("Distance between ==================\n{}\n and =================\n{} is\n {}", draft_str, tree_str, r)
    r
    */
    this.logger.debug("Skeleton distance : {}\texpression distance: {}", skel_dist, expr_dist)
    skel_dist * 0.5 + expr_dist * 0.5
    */
  }

  private def tree_dist(n1 : Node, n2 : Node, cost_model : costmodel.CostModel[StringNodeData]) : Double = {
    /*
    if(JavaUtils.is_hole(n1) || JavaUtils.is_hole(n2)) {
      return 0.0
    }
    */

    val label1 = JavaUtils.get_label(n1)
    val label2 = JavaUtils.get_label(n2)
    val parser = new BracketStringInputParser
    val algo = new APTED[CostModel[StringNodeData], StringNodeData](cost_model)
    val t1 = parser.fromString(label1)
    val t2 = parser.fromString(label2)
    this.logger.debug("t1 : {}", t1)
    this.logger.debug("t2 : {}", t2)
    algo.computeEditDistance(t1, t2).toDouble / ((t1.getNodeCount + t2.getNodeCount) * DraftCodeModel.STMT_COST * 100.0f).toDouble
  }

  private def get_all_exprs(draft : Draft) : Array[DraftNode] = {
    val iter = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(p => draft.is_expr(p) && !draft.is_expr(p.get_parent.get)))
    iter.get_all_nodes
  }

  private def gen_skeleton(node : Node) : Node = {
    val abs_visitor = new ModifierVisitor[Any] {
      override def visit(node : AssertStmt, arg : Any) = {
        node.setCheck(new NameExpr("#"))
        node
      }
      override def visit(node : DoStmt, arg : Any) = {
        node.setCondition(new NameExpr("#"))
        node.getBody.accept[Visitable, Any](this, null)
        node
      }
      override def visit(node : ForeachStmt, arg : Any) = {
        node.setIterable(new NameExpr("#"))
        node.setVariable(new VariableDeclarationExpr(new PrimitiveType(PrimitiveType.Primitive.INT), "#"))
        node.getBody.accept[Visitable, Any](this, null)
        node
      }
      override def visit(node : IfStmt, arg : Any) = {
        node.setCondition(new NameExpr("#"))
        node.getThenStmt.accept[Visitable, Any](this, null)
        if(node.getElseStmt.isPresent) {
          node.getElseStmt.get().accept[Visitable, Any](this, null)
        }
        node
      }
      override def visit(node : ReturnStmt, arg : Any) = {
        node.setExpression(new NameExpr("#"))
        node
      }
      override def visit(node : SwitchStmt, arg : Any) = {
        node.setSelector(new NameExpr("#"))
        val entries = node.getEntries
        for(i <- 0 until entries.size()) {
          val slist = entries.get(i).getStatements
          for(j <- 0 until slist.size()) {
            val stmt = slist.get(j)
            stmt.accept[Visitable, Any](this, null)
          }
        }
        node
      }
      override def visit(node : SynchronizedStmt, arg : Any) = {
        node.setExpression(new NameExpr("#"))
        node.getBody.accept[Visitable, Any](this, null)
        node
      }
      override def visit(node : WhileStmt, arg : Any) = {
        node.setCondition(new NameExpr("#"))
        node.getBody.accept[Visitable, Any](this, null)
        node
      }
      override def visit(p : Parameter, arg : Any) = {
        p.setName("#")
        p
      }
      override def visit(node : ExpressionStmt, arg : Any) = {
        node.getExpression match {
          case mce : MethodCallExpr if mce.getNameAsString == JavaUtils.HOLE_NAME =>
            //new ExpressionStmt(new MethodCallExpr().setName(new SimpleName("__pliny_hole__")))
            new EmptyStmt()
          case _ => new ExpressionStmt(node.getExpression.accept[Visitable, Any](this, null).asInstanceOf[Expression])
        }
      }
      override def visit(vdor : VariableDeclarator, arg : Any) = {
        if(vdor.getInitializer.isPresent) {
          val new_init = vdor.getInitializer.get().accept[Visitable, Any](this, null).asInstanceOf[Expression]
          new VariableDeclarator(vdor.getType, "#", new_init)
        } else {
          new VariableDeclarator(vdor.getType, "#")
        }
      }
      override def visit(mc : MethodCallExpr, arg : Any) = {
        if(mc.getName.getIdentifier.equals(JavaUtils.HOLE_NAME)) {
          if(mc.getParentNode.get().isInstanceOf[ExpressionStmt]) {
            mc
          } else {
            new NameExpr("#")
          }
          // mc
        } else {
          val new_args = new NodeList[Expression]()
          for(i <- 0 until mc.getArguments.size()) {
            new_args.add(mc.getArguments.get(i).accept[Visitable, Any](this, null).asInstanceOf[Expression])
          }
          if(mc.getScope.isPresent) {
            val new_scope = mc.getScope.get().accept[Visitable, Any](this, null).asInstanceOf[Expression]
            new MethodCallExpr(new_scope, mc.getName, new_args)
          } else {
            new MethodCallExpr().setName(mc.getName).setArguments(new_args)
          }
        }
      }
      override def visit(node : NameExpr, arg : Any) = {
        new NameExpr("#")
      }
      override def visit(lit : IntegerLiteralExpr, arg : Any) = {
        new NameExpr("#")
      }
    }

    node.clone().accept[Visitable, Any](abs_visitor, null).asInstanceOf[Node]
  }

  private def abstract_names(node : Node) : Node = {
    val renamer = new ModifierVisitor[Any] {
      override def visit(node : NameExpr, arg : Any) = {
        new NameExpr("#")
      }
      override def visit(vdor : VariableDeclarator, arg : Any) = {
        if(vdor.getInitializer.isPresent) {
          val new_init = vdor.getInitializer.get().accept[Visitable, Any](this, null).asInstanceOf[Expression]
          new VariableDeclarator(vdor.getType, "#", new_init)
        } else {
          new VariableDeclarator(vdor.getType, "#")
        }
      }
      override def visit(param : Parameter, arg : Any) = {
        new Parameter(param.getType, "#")
      }
      override def visit(lit : IntegerLiteralExpr, arg : Any) = {
        new NameExpr("#")
      }
      override def visit(es : ExpressionStmt, arg : Any) = {
        es.getExpression match {
          case mce : MethodCallExpr if mce.getNameAsString == JavaUtils.HOLE_NAME =>
            //new ExpressionStmt(new MethodCallExpr().setName(new SimpleName("__pliny_hole__")))
            new EmptyStmt()
          case _ => new ExpressionStmt(es.getExpression.accept[Visitable, Any](this, null).asInstanceOf[Expression])
        }
      }
      override def visit(mc : MethodCallExpr, arg : Any) = {
        if(mc.getName.getIdentifier.equals(JavaUtils.HOLE_NAME)) {
          if(mc.getParentNode.get().isInstanceOf[ExpressionStmt]) {
            mc
          } else {
            new NameExpr("#")
          }
          // mc
        } else {
          val new_args = new NodeList[Expression]()
          for(i <- 0 until mc.getArguments.size()) {
            new_args.add(mc.getArguments.get(i).accept[Visitable, Any](this, null).asInstanceOf[Expression])
          }
          if(mc.getScope.isPresent) {
            val new_scope = mc.getScope.get().accept[Visitable, Any](this, null).asInstanceOf[Expression]
            new MethodCallExpr(new_scope, mc.getName, new_args)
          } else {
            new MethodCallExpr().setName(mc.getName).setArguments(new_args)
          }
        }
      }
    }
    node.clone().accept[Visitable, Any](renamer, null).asInstanceOf[Node]
  }

  private def minimum(i1: Int, i2: Int, i3: Int): Int =
    scala.math.min(scala.math.min(i1, i2), i3)

  private def distance(s1:String, s2:String) = {
    val dist=Array.tabulate(s2.length+1, s1.length+1){(j,i)=>if(j==0) i else if (i==0) j else 0}

    for(j<-1 to s2.length; i<-1 to s1.length)
      dist(j)(i)=if(s2(j-1)==s1(i-1)) dist(j-1)(i-1)
      else minimum(dist(j-1)(i)+1, dist(j)(i-1)+1, dist(j-1)(i-1)+1)

    dist(s2.length)(s1.length)
  }
}
