package edu.rice.pliny.mcts.bayou

import java.io.File
import java.net.URLClassLoader

import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.{ArrayCreationLevel, Node, NodeList}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft.DraftNode
import edu.rice.pliny.language.java._
import spray.json._

import scala.collection.mutable

object BayouSketchUtils {
  val logger = Logger(this.getClass)

  /**
    * Parse a JSON file containing multiple Bayou sketches
    */
  def parse_sketch(sketch_filename : String, draft_filename : String, jar_paths : Seq[String] = Seq()) : Seq[JavaDraft] = {
    val class_loader : URLClassLoader = URLClassLoader.newInstance(jar_paths.map(p => new File(p).toURL).toArray , getClass.getClassLoader)
    val result = mutable.ArrayBuffer.empty[JavaDraft]

    /**
      * Make a list of argument holes
      */
    def mk_args(name_list : Seq[String]) : NodeList[Expression] = {
      val result = new NodeList[Expression]

      name_list.filter(name => !name.isEmpty).foreach(name => {
        val t = name match {
          case "int" => classOf[Int]
          case "byte" => classOf[Byte]
          case "short" => classOf[Short]
          case "char" => classOf[Char]
          case "long" => classOf[Long]
          case "boolean" => classOf[Boolean]
          case "float" => classOf[Float]
          case "double" => classOf[Double]
          case _ =>
            // add the imports first
            if(jar_paths.nonEmpty) Class.forName(name, true, class_loader) else Class.forName(name)
        }
        val hole = JavaUtils.gen_expr_hole_without_test(Some(t))
        result.add(hole)
      })
      result
    }

    /**
      * Make a hole for the scope
      */
    def mk_scope(scope : FieldAccessExpr) : MethodCallExpr = {
      val result = JavaUtils.gen_expr_hole_without_test().asInstanceOf[MethodCallExpr]
      result.getArguments.add(new StringLiteralExpr(scope.toString))
      result
    }

    /**
      * Make a constraint hole from an Bayou API call node
      */
    def mk_constraint(cons : JsObject) : Expression = {
      cons.getFields("node", "_call") match {
        case Seq(JsString(type_str), JsString(call_str)) =>
          assume(type_str == "DAPICall")

          val paren_index = call_str.indexOf('(')

          val names = call_str.substring(0, paren_index).split("\\.")

          // get the scope
          val scope = JavaUtils.name_list_to_fieldaccess(names.slice(0, names.size - 1))

          // get the method name
          val method_name = names.last

          // get the arguments
          val args_str = call_str.substring(paren_index + 1, call_str.length - 1)
          val args = args_str.split(",")

          if(method_name == scope.getName.asString()) {
            // if the method name is a type name, we create an object creation expression
            val result = new ObjectCreationExpr()
            result.setType(scope.toString)
            // result.setScope(scope)
            result.setArguments(mk_args(args))
            result
          } else {
            // if the method name is not a type name, we create a method call
            val result = new MethodCallExpr()
            result.setScope(mk_scope(scope))
            result.setName(method_name)
            result.setArguments(mk_args(args))
          }
      }
    }

    /**
      * This function makes a pliny hole from a set of API constraints
      */
    def mk_pliny_hole(api_call : Seq[JsValue], t: Option[Class[_]]) : MethodCallExpr = {
      val hole = JavaUtils.gen_expr_hole_without_test(t).asInstanceOf[MethodCallExpr]

      // set the return type
      if(t.isEmpty) {
        hole.getArguments.add(new StringLiteralExpr("void"))
      }

      // set the constraints
      val array_creation = new ArrayCreationExpr().setElementType("int")
      array_creation.getLevels.add(new ArrayCreationLevel(100))
      val cons_array = new NodeList[Expression]
      api_call.foreach(v => {
        cons_array.add(mk_constraint(v.asJsObject))
      })
      array_creation.setInitializer(new ArrayInitializerExpr(cons_array))

      hole.getArguments.add(array_creation)
      hole
    }

    /**
      * Convert an JSON node into an AST node
      */
    def helper(ast_json : JsObject) : Node = {
      ast_json.getFields("node") match {
        case Seq(JsString(node_type)) =>
          node_type match {
            case "DSubTree" =>
              ast_json.getFields("_nodes") match {
                case Seq(JsArray(nlist)) =>
                  val slist = new NodeList[Statement]
                  nlist.foreach(v => {
                    val s = helper(v.asJsObject)
                    slist.add(s match {
                      case e : Expression => new ExpressionStmt(e)
                      case _ => s.asInstanceOf[Statement]
                    })
                  })
                  new BlockStmt(slist)
              }
            case "DLoop" =>
              ast_json.getFields("_cond", "_body") match {
                case Seq(JsArray(cond), JsArray(body)) =>
                  val wloop = new WhileStmt()
                    .setCondition(mk_pliny_hole(cond, Some(classOf[Boolean])))

                  // body
                  val slist = new NodeList[Statement]
                  body.foreach(b => {
                    val s = helper(b.asJsObject)
                    slist.add(s match {
                      case e: Expression => new ExpressionStmt(e)
                      case _ => s.asInstanceOf[Statement]
                    })
                  })
                  wloop.setBody(new BlockStmt(slist))
                  wloop
              }
            case "DAPICall" =>
              mk_pliny_hole(Seq(ast_json), None)
            case "DBranch" =>
              ast_json.getFields("_cond", "_then", "_else") match {
                case Seq(JsArray(cond), JsArray(_then), JsArray(_else)) =>
                  val ifs = new IfStmt().setCondition(mk_pliny_hole(cond, Some(classOf[Boolean])))

                  // set the then branch
                  val then_body = new NodeList[Statement]
                  _then.foreach(v => {
                    val s = helper(v.asJsObject)
                    then_body.add(s match {
                      case e : Expression => new ExpressionStmt(e)
                      case _ => s.asInstanceOf[Statement]
                    })
                  })
                  ifs.setThenStmt(new BlockStmt(then_body))

                  // set the else branch
                  val else_body = new NodeList[Statement]
                  _else.foreach(v => {
                    val s = helper(v.asJsObject)
                    else_body.add(s match {
                      case e : Expression => new ExpressionStmt(e)
                      case _ => s.asInstanceOf[Statement]
                    })
                  })
                  ifs.setThenStmt(new BlockStmt(else_body))

                  ifs
              }
            case _ =>
              logger.warn("Unknown node type: {}", node_type)
              assert(false)
              null
          }
      }
    }


    val draft = JavaUtils.parse_file(draft_filename, None, None, None, jar_paths, Seq())
    if(draft.isEmpty) {
      return result
    }

    val json_data = scala.io.Source.fromFile(sketch_filename).mkString.parseJson.asJsObject
    val sketch_nodes = json_data.getFields("asts") match {
      case Seq(JsArray(asts)) =>
        asts.map(a => helper(a.asJsObject))
    }

    // look for the first evidence call and replace it with the draft node
    val target = draft.get.get_stmt_hole_with_tests

    // we cannot find any evidence
    if(target.isEmpty) {
      return result
    }

    // remove the evidence import
    {
      val old_imports = draft.get.compilation_unit.getImports
      var target_index = -1
      for(i <- 0 until old_imports.size()) {
        val class_name = old_imports.get(i).getName.getIdentifier
        if(class_name == "Evidence") {
          target_index = i
        }
      }

      if(target_index != -1) {
        draft.get.compilation_unit.getImports.remove(target_index)
      }
    }

    // get the java comment for this hole
    val comment = target.get.asInstanceOf[JavaDraftHole].node.getComment.get

    // put new draft in
    for(elem <- sketch_nodes) {
      val target_parent = target.get.get_parent.get
      elem.setComment(comment)
      val new_draft = draft.get.replace(target.get, target_parent, Seq(new JavaDraftNode(elem))).asInstanceOf[JavaDraft]
      result += new_draft
    }

    result
  }

  /**
    * Cache for hole fillings
    */
  val expr_filling_cache : mutable.Map[Int, Seq[DraftNode]] = mutable.Map.empty[Int, Seq[DraftNode]]
  val stmt_filling_cache : mutable.Map[Int, Seq[Seq[DraftNode]]] = mutable.Map.empty[Int, Seq[Seq[DraftNode]]]
  def clear_filling_cache() : Unit = {
    expr_filling_cache.clear()
    stmt_filling_cache.clear()
  }

}
