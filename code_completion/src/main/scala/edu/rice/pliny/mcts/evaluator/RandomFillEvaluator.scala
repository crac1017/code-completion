package edu.rice.pliny.mcts.evaluator

import edu.rice.pliny.mcts.MCTSTree

import scala.util.Random

/**
  * This class evaluate a node by randomly make moves on the node
  */
class RandomFillEvaluator extends NodeEvaluator {
  Random.setSeed(System.currentTimeMillis())

  override def evaluate(tree: MCTSTree): Double = Random.nextDouble()
}
