package edu.rice.pliny.mcts.sygus

object SygusAST {
  trait Expr
  case class SExpr(exprs : Seq[Expr]) extends Expr
  case class Id(v : String) extends Expr
  case class Num(v : Long) extends Expr
  case class Str(s : String) extends Expr

  def to_src(e : Expr) : String = e match {
    case Id(s) => s
    case Num(v) => java.lang.Long.toString(v)
    case Str(s) => "\"" + s + "\""
    case SExpr(exprs) =>
      val builder = new StringBuilder
      builder.append("(")
      exprs.zipWithIndex.foreach(p => {
        if(p._2 > 0) { builder.append(" ") }
        builder.append(this.to_src(p._1))
      })
      builder.append(")")
      builder.toString()
  }
}
