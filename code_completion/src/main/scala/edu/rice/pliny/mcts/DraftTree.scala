package edu.rice.pliny.mcts

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft.{Draft, DraftHole, DraftNode}
import edu.rice.pliny.language.java._

import scala.collection.mutable
import com.github.javaparser.ast.Node
import com.github.javaparser.ast.body.VariableDeclarator
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.visitor.{ModifierVisitor, Visitable}
import com.github.javaparser.symbolsolver.model.typesystem.Type

class DraftTree(val draft : Draft,
                val parent : Option[DraftTree],
                val interpreter : JavaFastInterpreter,
                var visited : Int = 0,
                val children : mutable.ArrayBuffer[DraftTree] = mutable.ArrayBuffer.empty[DraftTree],
                var reward : Double = 0.0,
                var prior : Double = 0.0,
                var depth : Int = 0,
                var deadend : Boolean = false,
                var final_reward : Double = 0.0,
                val relevant_progs : Seq[Draft] = mutable.ArrayBuffer.empty[Draft]) extends MCTSTree {

  val logger = Logger(this.getClass)
  type T = Draft
  override def get_node: Draft = this.draft
  override def get_children: Seq[MCTSTree] = this.children
  override def add_children(s : Seq[MCTSTree]): Unit = this.children ++= s.asInstanceOf[Seq[DraftTree]]
  override def get_visited : Int = this.visited
  override def set_visited(i : Int): Unit = this.visited = i
  override def get_parent : Option[MCTSTree] = this.parent
  override def get_reward : Double = this.reward
  override def set_reward(v : Double) : Unit = this.reward = v
  override def get_final_reward : Double = this.final_reward
  override def set_final_reward(v : Double) : Unit = this.final_reward = v
  override def get_prior : Double = this.prior
  override def set_prior(p : Double) : Unit = this.prior = p
  override def set_deadend(b: Boolean): Unit = this.deadend = b
  override def is_deadend: Boolean = this.deadend
  override def get_depth : Int = this.depth
  override def set_depth(d : Int) : Unit = this.depth = d

  override def hashCode : Int = this.draft.hashCode()

  override def equals(o : Any) : Boolean = {
    if(!o.isInstanceOf[DraftTree]) {
      return false
    }
    val other = o.asInstanceOf[DraftTree]
    other.draft.equals(this.draft)
  }

  val used_moves = mutable.Set.empty[Draft]
  val expr_hole : Option[DraftHole] = this.draft.get_expr_hole
  val stmt_hole : Option[DraftHole] = this.draft.get_stmt_hole_without_tests

  /**
    * Replace all names into holes
    */
  val abstract_holes : mutable.Map[String, Node] = mutable.Map.empty[String, Node]
  if(parent.isDefined) {
    abstract_holes ++= parent.get.abstract_holes
  }

  /**
    * Record the used references from the reference program to avoid duplicate renaming
    */
  val used_refs : mutable.Set[String] = mutable.Set.empty[String]
  if(parent.isDefined) {
    used_refs ++= parent.get.used_refs
  }

  private def abstract_node(node : Node, src_draft : JavaDraft) : Visitable = {
    val modifier = new ModifierVisitor[Any] {
      override def visit(n : NameExpr, arg : Any) : Visitable = {
        if(draft.asInstanceOf[JavaDraft].imported_class.contains(n.getNameAsString)) {
          n
        } else {
          if(!abstract_holes.contains(n.getNameAsString)) {
            val t = src_draft.get_type(new JavaDraftNode(n)).asInstanceOf[JavaDraftNodeType].t
            val h = JavaUtils.gen_expr_hole_without_test(src_draft.type_to_class(t))
            abstract_holes.put(n.getNameAsString, h)
          }
          abstract_holes(n.getNameAsString)
        }
      }

      override def visit(n : VariableDeclarator, arg : Any) : Visitable = {
        if(n.getInitializer.isPresent) {
          n.getInitializer.get().accept(this, null).asInstanceOf[Expression]
        }
        n
      }
    }
    val result = node.clone().accept[Visitable, Any](modifier, null)
    result
  }

  /**
    * Get valid expression files for this hole. Since this is doing splicing thing, we only want the expressions
    * in the reference programs
    */
  def get_valid_expr_fills(hole: DraftHole, relevant_progs : Seq[Draft]): Seq[DraftNode] = {
    val cache = mutable.Map.empty[Type, mutable.Set[Expression]]

    // get hole type
    /*
    var hole_type = this.get_type(hole).asInstanceOf[JavaDraftNodeType].t
    if(hole_type.isWildcard) {
      hole_type = hole.asInstanceOf[JavaDraftHole].ttype
    }
    */
    var hole_type = hole.asInstanceOf[JavaDraftHole].ttype

    // if this is a wilecard, then this is a user-provided hole. Then we can extract expressions. Otherwise, we are doing
    // renaming
    val only_want_names = !hole_type.isWildcard
    if(hole_type.isWildcard) {
      hole_type = this.draft.get_type(hole).asInstanceOf[JavaDraftNodeType].t
    }

    this.logger.debug("Hole type : {}", hole_type.describe())

    def add_to_cache(e : Expression, t : Type) : Unit = {
      if(!cache.contains(t)) {
        cache.put(t, mutable.Set.empty[Expression])
      }
      cache(t) += e
    }

    /**
      * This function extracts expressions from a draft
      */
    def extract_expr(draft : JavaDraft) : Unit = {
      val iter = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(p => draft.is_expr(p)))
      while(iter.hasNext) {
        val node = iter.next()
        if(this.draft.same_role(hole, node)) {
          val node_type = draft.get_type(node).asInstanceOf[JavaDraftNodeType].t
          node.asInstanceOf[JavaDraftNode].node match {
            case name : NameExpr =>
              if(this.draft.asInstanceOf[JavaDraft].type_env.contains(name)) {
                add_to_cache(name, node_type)
              }
            case _ =>
              add_to_cache(this.abstract_node(node.asInstanceOf[JavaDraftNode].node.asInstanceOf[Expression], draft).asInstanceOf[Expression], node_type)
          }
        }
      }
    }

    val hole_id = hole.asInstanceOf[JavaDraftHole].node.asInstanceOf[MethodCallExpr].getArguments.get(0).asInstanceOf[IntegerLiteralExpr].asInt()
    // extract expressions from relevant programs
    if(!only_want_names) {
      relevant_progs.foreach(rp => extract_expr(rp.asInstanceOf[JavaDraft]))
    } else {
      for(vname <- this.draft.asInstanceOf[JavaDraft].defset) {
        val name_node = new NameExpr(vname)
        val t = this.draft.get_type(new JavaDraftNode(name_node)).asInstanceOf[JavaDraftNodeType].t
        add_to_cache(name_node, t)
      }
    }

    if(hole_type.isWildcard) {
      val result = cache.values.foldLeft(Seq.empty[Node])((acc, s) => acc ++ s.toSeq)
        .map(n => new JavaDraftNode(n))
      result.foreach(n => this.logger.debug("Valid Expr Fills {}", n.node))
      result
    } else {
      val result = cache.foldLeft(Seq.empty[Node])((acc, p) => {
        if(p._1.isWildcard || this.draft.asInstanceOf[JavaDraft].is_subtype(p._1, hole_type)) {
          acc ++ p._2.toSeq
        } else {
          acc
        }
      }).map(n => new JavaDraftNode(n))
      result.foreach(n => this.logger.debug("Valid Expr Fills {}", n.node))
      result
    }
  }

  def get_valid_expr_moves : Seq[DraftTree] = {
    if(this.expr_hole.isEmpty) {
      return Seq()
    }
    val result = mutable.ArrayBuffer.empty[Draft]


    this.logger.debug("Working on hole " + this.expr_hole.get.toString)
    val cand_exprs = this.get_valid_expr_fills(this.expr_hole.get, relevant_progs)
    // val cand_exprs = this.draft.asInstanceOf[JavaDraft].get_relevant_exprs(this.expr_hole.get, relevant_progs)
    cand_exprs.foreach(e => {
      this.logger.debug("Replacing {} with {}", this.expr_hole.get.toString, e.toString)
      val new_node = this.draft.asInstanceOf[JavaDraft].replace(this.expr_hole.get, e)
      this.logger.debug("New draft:\n" + new_node.toString)
      if(!used_moves.contains(new_node)) {
        result += new_node
        used_moves += new_node
      }
    })
    result
      .map(m => new DraftTree(m, Some(this), this.interpreter, depth = this.depth + 1, relevant_progs = this.relevant_progs))
  }

  def get_valid_stmt_moves : Seq[DraftTree] = {
    if(this.stmt_hole.isEmpty) {
      return Seq()
    }
    val result = mutable.ArrayBuffer.empty[Draft]
    val hole_parent = this.stmt_hole.get.get_parent.get
    this.logger.debug("Working on statement hole " + this.stmt_hole.get.toString)
    val cand_stmts = this.draft.get_valid_stmt_fills(this.stmt_hole.get, relevant_progs)
    cand_stmts.foreach(stmts => {
      val new_node = this.draft.replace(this.stmt_hole.get, hole_parent, stmts)
      this.logger.debug("New draft:\n" + new_node.toString)
      if(!used_moves.contains(new_node)) {
        result += new_node
        used_moves += new_node
      }
    })
    result
      .map(m => new DraftTree(m, Some(this), this.interpreter, depth = this.depth + 1, relevant_progs = this.relevant_progs))
  }

  /**
    * Return a sequence of valid next moves
    */
  override def get_valid_moves : Seq[MCTSTree] = {
    // next expression moves
    if(this.stmt_hole.isDefined) { return this.get_valid_stmt_moves }
    // next statement moves
    if(this.expr_hole.isDefined) { return this.get_valid_expr_moves }
    Seq()
  }

  /**
    * Check whether this node will have potential child nodes.
    */
  override def is_complete: Boolean = !this.draft.has_hole

  /**
    * Check whether this node is a solution to our problem
    */
  override def is_solution: Boolean = {
    if(!this.is_complete) { return false }
    logger.info("Testing =============\n" + this.draft.toString)
    val test = this.draft.get_test.get
    test.accept(this.interpreter, this.draft.toString)._1
  }

  override def toString : String = {
    "Final Reward   :   " + this.final_reward.toString + "\n" +
    "Reward         :   " + this.reward.toString + "\n" +
    "Prior          :   " + this.prior.toString + "\n" +
    "Visited        : " + this.visited + "\n" +
    "Parent Visited : " + (if(this.parent.isDefined) this.parent.get.get_visited else "N/A") + "\n" +
    "Deadend        : " + this.deadend + "\n" +
    this.draft.toString
  }
}
