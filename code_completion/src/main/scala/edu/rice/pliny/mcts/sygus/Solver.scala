package edu.rice.pliny.mcts.sygus

object Solver {
  /**
    * This is the main entry function for solving sygus problem
    * @param problem - sygus problem definition content
    * @return synthesis runtime
    */
  def solve(problem : String) : Long = {
    // parse the problem
    val ast = SygusParser.parse(problem)

    // failed to parse the problem
    if(ast.isEmpty) {
      return -1
    }

    val interpreter = new SygusInterpreter

    // evaluate all the sexps
    ast.get.foreach(sexpr => {
      interpreter.eval(sexpr.asInstanceOf[SygusAST.SExpr])
    })

    // get the synthesis result
    interpreter.synth_result
  }
}
