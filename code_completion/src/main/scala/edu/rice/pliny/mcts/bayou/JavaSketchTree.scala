package edu.rice.pliny.mcts.bayou

import com.github.javaparser.JavaParser
import com.github.javaparser.ast.body.VariableDeclarator
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.visitor.TreeVisitor
import com.github.javaparser.ast.{Node, NodeList}
import com.github.javaparser.symbolsolver.model.typesystem.{Type, Wildcard}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft.{Draft, DraftNode, TestCase}
import edu.rice.pliny.language.java._
import edu.rice.pliny.mcts.{DraftTree, SketchHashCodeVisitor, SketchNewNodeHashCodeVisitor}

import scala.collection.mutable


class JavaSketchTree(override val draft : JavaDraft,
                     override val parent : Option[JavaSketchTree],
                     override val interpreter : JavaFastInterpreter,
                     sketch_visited : Int = 0,
                     override val children : mutable.ArrayBuffer[DraftTree] = mutable.ArrayBuffer.empty[DraftTree],
                     sketch_reward : Double = 0.0,
                     sketch_prior : Double = 0.0,
                     sketch_depth : Int = 0,
                     sketch_deadend : Boolean = false,
                     sketch_final_reward : Double = 0.0,
                     override val relevant_progs : Seq[Draft] = mutable.ArrayBuffer.empty[Draft])
  extends DraftTree(draft, parent, interpreter, sketch_visited, children, sketch_reward, sketch_prior, sketch_depth, sketch_deadend, sketch_final_reward, relevant_progs) {

  override val logger = Logger(this.getClass)
  override def equals(o : Any) : Boolean = {
    if(!o.isInstanceOf[JavaSketchTree]) {
      return false
    }
    val other = o.asInstanceOf[JavaSketchTree]
    other.draft.equals(this.draft)
  }

  abstract class BNode
  trait BHole {
    // the exact hole in the java language
    def hole : MethodCallExpr

    // the type of this hole
    def ttype : Type
  }
  class BExpr(val e : Expression) extends BNode
  class BType(val hole : MethodCallExpr)  extends BNode with BHole {
    def ttype : Type = {
      JavaUtils.str_to_type(hole.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), draft.type_solver)
    }
  }

  class AbsCall(val hole : MethodCallExpr) extends BNode with BHole {
    def name : String = {
      hole.getArguments.get(2) match {
        case mce : MethodCallExpr => mce.getName.asString()
        case _ : ObjectCreationExpr => "new"
      }
    }
    def scope : Option[BNode] = {
      hole.getArguments.get(2) match {
        case mce : MethodCallExpr =>
          if(mce.getScope.isPresent) Some(new BExpr(mce.getScope.get())) else None
        case oc : ObjectCreationExpr =>
          if(oc.getScope.isPresent) Some(new BExpr(oc.getScope.get())) else None
      }
    }
    def args : Seq[BNode] = {
      val result = mutable.ArrayBuffer.empty[BNode]
      val args = hole.getArguments.get(2) match {
        case mce : MethodCallExpr => mce.getArguments
        case oc : ObjectCreationExpr => oc.getArguments
      }
      for(i <- 0 until args.size()) {
        if(JavaUtils.is_hole(args.get(i))) {
          result += new BType(args.get(i).asInstanceOf[MethodCallExpr])
        } else {
          result += new BExpr(args.get(i))
        }
      }
      result
    }
    def ttype : Type = JavaUtils.str_to_type(hole.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), draft.type_solver)
  }

  /**
    * Make a abstract call hole. If we are making a Bayou hole in for readline, it will look like this:
    *
    * __pliny_hole__(0, return value.class, BufferedReader.readline(arg_type1, arg_type2));
    */
  def mk_abs_call(method_name : String, scope : Class[_], args_type : Seq[Class[_]], ret_type : Class[_]) : Expression = {
    val hole = JavaUtils.gen_expr_hole_without_test(Some(ret_type)).asInstanceOf[MethodCallExpr]

    val abs_call = new MethodCallExpr().setName(method_name)
    abs_call.setScope(JavaUtils.gen_expr_hole_without_test(Some(scope)))
    val abs_args = new NodeList[Expression]()
    args_type.foreach(a => abs_args.add(JavaUtils.gen_expr_hole_without_test(Some(a))))
    abs_call.setArguments(abs_args)

    hole.addArgument(abs_call)
    hole
  }

  /**
    * Make a abstract call hole. If we are making a Bayou hole in for making a File Reader, it will look like this:
    *
    * __pliny_hole__(0, FileReader.class, FileReader.new(arg_type1, arg_type2));
    */
  def mk_abs_obj_creation(scope : Class[_], obj_type : Class[_], args_type : Seq[Class[_]]) : Expression = {
    val hole = JavaUtils.gen_expr_hole_without_test(Some(obj_type)).asInstanceOf[MethodCallExpr]

    val abs_call = new ObjectCreationExpr()
    abs_call.setType(JavaParser.parseClassOrInterfaceType(obj_type.getName))
    val abs_args = new NodeList[Expression]()
    args_type.foreach(a => abs_args.add(JavaUtils.gen_expr_hole_without_test(Some(a))))
    abs_call.setArguments(abs_args)

    hole.addArgument(abs_call)
    hole
  }

  /**
    * Use enumerative search to generate a set of AST nodes satisfying a set of constraints
    * @param hole - the hole we will use the expressions to filling in and it also has the type constraint
    * @param relevant_progs - a set of candidate expressions that we can use
    * @param cons_array - this is a set of syntax constraints
    */
  def syn_with_constraints(hole: Node, relevant_progs: Seq[Draft], cons_array: NodeList[Expression]) : Seq[DraftNode] = {

    def sat_cons(target : Node) : Boolean = {
      def check_obj_creation(cons : ObjectCreationExpr, target : Node) : Boolean = {
        this.logger.debug("Checking object creation.")
        var sat = true

        // test whether it has object creation
        new TreeVisitor {
          override def process(node: Node) : Unit = {
            if(sat) return
            logger.debug("Looking at Node {}", node.toString())
            node match {
              case o : ObjectCreationExpr =>
                // check return types
                sat = draft.is_subtype(draft.type_solver.convertToUsage(o.getType), draft.type_solver.convertToUsage(cons.getType))
                logger.debug("sat: {}, return type: {} constraint type: {}.", sat, o.getType.toString, cons.getType)
              case _ => Unit
            }
          }
        }.visitPreOrder(target)
        sat
      }

      def check_method_call(cons : MethodCallExpr, target : Node) : Boolean = {
        var sat = false
        // val cons_type = draft.type_solver.solveMethodAsUsage(cons)
        val cons_scope_type =
          if(cons.getScope.isPresent) Some(draft.get_type(new JavaDraftNode(cons.getScope.get())).asInstanceOf[JavaDraftNodeType].t)
          else None
        val cons_method_name = cons.getName.asString()
        this.logger.debug("Checking method call using {}.{}", cons_scope_type, cons_method_name)

        // test whether it has object creation
        new TreeVisitor {
          override def process(node: Node) : Unit = {
            if(sat) return
            logger.debug("Looking at Node {}", node.toString())
            node match {
              case m : MethodCallExpr =>
                logger.debug("Getting the type for {}", m.getScope)
                // draft.type_env.foreach(p => logger.debug("Type for {} = {}", p._1.toString, p._2.toString))
                val m_scope_type =
                // if(m.getScope.isPresent) Some(draft.type_solver.getType(m.getScope.get()))
                  if(m.getScope.isPresent) Some(draft.get_type(new JavaDraftNode(m.getScope.get())).asInstanceOf[JavaDraftNodeType].t)
                  else None
                val m_method_name = m.getName.asString()
                // val m_type = draft.type_solver.solveMethodAsUsage(m)
                logger.debug("candidate method {}.{}", m_scope_type, m_method_name)
                sat = cons_method_name.equals(m_method_name) &&
                  ((cons_scope_type.isEmpty && m_scope_type.isEmpty) ||
                    (cons_scope_type.isDefined && m_scope_type.isDefined && draft.is_subtype(m_scope_type.get, cons_scope_type.get)))
                logger.debug("sat = {}", sat)
              case _ => Unit
            }
          }
        }.visitPreOrder(target)
        sat
      }

      var sat = true

      for(i <- 0 until cons_array.size()) {
        val cons = cons_array.get(i)
        cons match {
          case oce : ObjectCreationExpr =>
            sat = check_obj_creation(oce, target)
          case mce : MethodCallExpr =>
            sat = check_method_call(mce, target)
          case _ => Unit
        }
      }

      sat
    }

    val visited = mutable.Set.empty[Int]
    /**
      * Do a BFS search within a limit step range
      */
    def search(node : Node, range_limit : Int) : Seq[(Node, Int)] = {
      val result = mutable.ArrayBuffer.empty[(Node, Int)]
      val queue = mutable.Queue.empty[(Node, Int)]
      queue += ((node, 0))

      while(queue.nonEmpty) {
        this.logger.debug("Queue size : {} =============\n", queue.size)
        this.logger.debug("Result node Num : {} =============\n", result.size)
        // result.foreach(n => this.logger.debug("{}", n))
        val (front, range) = queue.dequeue()
        this.logger.debug("Searching on {} at range {}", front.toString, range)
        visited += SketchHashCodeVisitor.hashCode(front)

        if(range < range_limit) {
          val holes = JavaUtils.get_all_holes(front)
          if(holes.isEmpty) {
            if(sat_cons(front)) {
              this.logger.debug("Adding node {}", front.toString)
              result += ((front, range))
            }
          } else {
            for(h <- holes) {
              val hole_type =
                if(h.asInstanceOf[MethodCallExpr].getArguments.size() > 1) {
                  JavaUtils.str_to_type(h.asInstanceOf[MethodCallExpr].getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), this.draft.type_solver)
                } else {
                  Wildcard.UNBOUNDED
                }
              if(!hole_type.isWildcard) {
                val cand_exprs =
                  if(JavaUtils.is_expr_hole(h)) {
                    this.draft.get_valid_expr_fills(new JavaDraftHole(h, None, None, ttype = hole_type), relevant_progs)
                      // filter out the method call whose scope is an object creation expression or variable declaration
                      .filter(n => {
                      val jnode = n.asInstanceOf[JavaDraftNode].node
                      if(!jnode.isInstanceOf[MethodCallExpr]) {
                        true
                      } else {
                        val mcall = jnode.asInstanceOf[MethodCallExpr]
                        if(!mcall.getScope.isPresent) {
                          true
                        } else {
                          val scope = mcall.getScope.get()
                          !scope.isInstanceOf[ObjectCreationExpr] && !scope.isInstanceOf[VariableDeclarationExpr]
                        }
                      }
                    })
                  } else {
                    this.draft.get_valid_stmt_fills(new JavaDraftHole(h.getParentNode.get(), None, None, ttype = hole_type), relevant_progs)
                      .filter(s => s.size == 1 &&
                        !s.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[TryStmt] &&
                        !s.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[IfStmt] &&
                        !s.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[WhileStmt] &&
                        !s.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[ForStmt] &&
                        !s.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[ForeachStmt])
                      .map(s => s.head)
                  }
                // cand_exprs.foreach(n => this.logger.debug("Search candidate expr: {}", n.toString))
                cand_exprs.foreach(n => {
                  // this.logger.debug("Replacing {} with {} inside {}", h, n.toString, front)

                  // calculate the new node's hash code before actually generating the new node
                  val hash_code =
                    if(JavaUtils.is_expr_hole(h)) {
                      SketchNewNodeHashCodeVisitor.hashCode(front, h, n.asInstanceOf[JavaDraftNode].node)
                    } else {
                      SketchNewNodeHashCodeVisitor.hashCode(front, h.getParentNode.get(), n.asInstanceOf[JavaDraftNode].node)
                    }

                  // get the hole count
                  val hole_count = SketchNewNodeHashCodeVisitor.hole_count()
                  if(!visited.contains(hash_code) && hole_count + range < range_limit) {
                    val new_node =
                      if(JavaUtils.is_expr_hole(h)) {
                        JavaUtils.replace(front.clone(), h, n.asInstanceOf[JavaDraftNode].node)
                      } else {
                        JavaUtils.replace(front.clone(), h.getParentNode.get(), n.asInstanceOf[JavaDraftNode].node)
                      }
                    // this.logger.debug("New node {} with max range {}", new_node.toString, JavaUtils.get_hole_count(new_node) + range)
                    queue.enqueue((new_node, range + 1))
                    visited += hash_code
                  }
                })
              }
            }
          }
        }
      }
      result
    }
    val nodes = search(hole, 5)
    nodes.map(n => new JavaDraftNode(n._1))
  }

  /**
    * Generate a set of draft node for filling
    */
  def get_valid_expr_fills(hole: BHole, relevant_progs: Seq[Draft]) : Seq[DraftNode] = {
    val result = mutable.ArrayBuffer.empty[DraftNode]

    if(hole.hole.getArguments.size() <= 2) {
      this.logger.debug("Synthesizing normal expressions.")
      result ++= this.draft.get_valid_expr_fills(new JavaDraftHole(hole.hole, None, None, ttype = hole.ttype), relevant_progs)
    } else {
      hole.hole.getArguments.get(2) match {
        case _ : ArrayCreationExpr if hole.hole.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString() != "void" =>
          val cons_array = hole.hole.getArguments.get(2).asInstanceOf[ArrayCreationExpr].getInitializer.get().getValues
          this.logger.debug("Synthesizing expressions with constraints.")
          result ++= this.syn_with_constraints(hole.hole, relevant_progs, cons_array)
        case oce : ObjectCreationExpr =>
          this.logger.debug("Concretizing object creation expression.")
          result += new JavaDraftNode(oce)
        case mce : MethodCallExpr =>
          this.logger.debug("Concretizing method call expression.")
          result += new JavaDraftNode(mce)
      }
    }

    result.foreach(node => this.logger.debug("Candidate Bayou expr {}", node.toString))
    result
  }

  /**
    * Synthesize a statement given a constraint
    * @param hole - this is the statement hole
    * @param mt - the return type of the method call
    * @param relevant_progs - relevant programs
    * @param mce - the method call constraint
    * @return
    */
  def syn_stmt_with_constraints(hole: BHole, mt: Type, relevant_progs: Seq[Draft], mce: MethodCallExpr) : Seq[DraftNode] = {
    val cons_array = hole.hole.getArguments.get(2).asInstanceOf[ArrayCreationExpr].getInitializer.get().getValues
    val stmt_hole = hole.hole.getParentNode.get()
    this.syn_with_constraints(stmt_hole, relevant_progs, cons_array)
  }

  /**
    * Generate a set of draft node for statement filling
    */
  def get_valid_stmt_fills(hole: BHole, relevant_progs: Seq[Draft]) : Seq[Seq[DraftNode]] = {
    val result = mutable.ArrayBuffer.empty[Seq[DraftNode]]
    assert(hole.hole.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString() == "void")

    val hole_id = hole.hole.getArguments.get(0).asInstanceOf[IntegerLiteralExpr].asInt()

    // this should only contain a single constraint which is the concretized method call
    val cons = hole.hole.getArguments.get(2).asInstanceOf[ArrayCreationExpr].getInitializer.get().getValues.get(0)

    // get the concretized call
    cons match {
      case oce : ObjectCreationExpr =>
        val vdor = new VariableDeclarator(oce.getType, JavaUtils.mk_var(hole_id), oce)
        val decl = new VariableDeclarationExpr(vdor)
        result += Seq(new JavaDraftNode(new ExpressionStmt(decl)))
      case mce : MethodCallExpr =>
        // get the class type name
        val ct = this.draft.type_env(mce.getScope.get())

        // get the method name
        this.logger.debug("method: {}", mce.toString)
        val mname = JavaUtils.classname_to_name(ct.asReferenceType().describe() + "." + mce.getName.asString())

        // get the type of the method
        this.draft.get_methods_from_class()
        val mt = this.draft.class_method_types(mname)

        if(mt.isVoid) {
          // if the return type is void, we simply return a method call
          result += Seq(new JavaDraftNode(new ExpressionStmt(mce)))
        } else {
          val slist = this.syn_stmt_with_constraints(hole, mt, relevant_progs, mce)
          result ++= slist.map(n => Seq(n))
        }

    }

    result
  }

  /**
    * Get a Bayou hole from the program
    * @return
    */
  def get_bhole : Option[BHole] = {
    val hole = this.draft.get_hole
    if(hole.isEmpty) {
      return None
    }

    // check the __pliny_hole__ function call to construct different bayou hole
    val node = hole.get.asInstanceOf[JavaDraftHole].node

    val hole_call = node match {
      case mce : MethodCallExpr => mce
      case es : ExpressionStmt => es.getExpression.asInstanceOf[MethodCallExpr]
    }

    if(hole_call.getArguments.size() <= 2) {
      // this is a normal hole with types
      Some(new BType(hole_call))
    } else {
      Some(new AbsCall(hole_call))
    }
  }

  /**
    * Test whether we need to synthesize expressions for statements for a hole
    */
  private def is_stmt_hole(hole : BHole) : Boolean = {
    hole.hole.getArguments.size() > 2 &&
    hole.hole.getArguments.get(2).isInstanceOf[ArrayCreationExpr] &&
    hole.hole.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString().equals("void")
  }

  /**
    * Return a sequence of valid next moves
    */

  override def get_valid_moves : Seq[JavaSketchTree] = {
    val result = mutable.ArrayBuffer.empty[JavaDraft]

    val hole = this.get_bhole
    if(hole.isDefined) {

      val hole_hash = hole.get.hole.hashCode()
      if(!this.is_stmt_hole(hole.get)) {
        // next expression moves
        this.logger.debug("Working on hole " + hole.get.hole.toString)
        if(!BayouSketchUtils.expr_filling_cache.contains(hole_hash)) {
          this.logger.debug("Generating expressions...")
          val exprs = this.get_valid_expr_fills(hole.get, relevant_progs)
          BayouSketchUtils.expr_filling_cache(hole_hash) = exprs
        }

        val cand_exprs = BayouSketchUtils.expr_filling_cache(hole_hash)
        cand_exprs.foreach(e => {
          this.logger.debug("Replacing {} with {}", hole.get.hole.toString, e.toString)
          val new_node = this.draft.replace(new JavaDraftNode(hole.get.hole), e).asInstanceOf[JavaDraft]
          this.logger.debug("New Sketch:\n" + new_node.toString)
          if(!used_moves.contains(new_node)) {
            result += new_node
            used_moves += new_node
          }
        })
      } else {
        // next statement moves
        val stmt_hole = new JavaDraftHole(hole.get.hole.getParentNode.get(), None, None)
        val hole_parent = stmt_hole.get_parent.get


        this.logger.debug("Working on statement hole " + stmt_hole.toString)
        if(!BayouSketchUtils.stmt_filling_cache.contains(hole_hash)) {
          this.logger.debug("Generating statements...")
          val stmts = this.get_valid_stmt_fills(hole.get, relevant_progs)
          BayouSketchUtils.stmt_filling_cache(hole_hash) = stmts
        }
        val cand_stmts = BayouSketchUtils.stmt_filling_cache(hole_hash)
        cand_stmts.foreach(stmts => {
          val new_node = this.draft.replace(stmt_hole, hole_parent, stmts).asInstanceOf[JavaDraft]
          this.logger.debug("New draft:\n" + new_node.toString)
          if(!used_moves.contains(new_node)) {
            result += new_node
            used_moves += new_node
          }
        })
      }

    }

    /*
    // we handle expression holes first
    val all_holes = this.node.get_all_expr_hole ++ this.node.get_all_stmt_hole_without_tests
    all_holes.foreach(h => result ++= this.fill_hole(this.node, h))
    */
    result.map(m => new JavaSketchTree(m, Some(this), this.interpreter, relevant_progs = this.relevant_progs))
  }

  /**
    * Get the test and solution from the hole
    */
  private def get_test_and_solution : Option[(TestCase, String)] = {
    var result : Option[(TestCase, String)] = None
    new TreeVisitor {
      override def process(node: Node): Unit = {
        val t = JavaUtils.parse_comment_test(node)
        if(t._1.isDefined && t._2.isDefined) {
          result = Some((t._1.get, node.toString()))
        }
      }
    }.visitPreOrder(this.draft.method)
    result
  }

  /**
    * Check whether this node is a solution to our problem
    */
  override def is_solution: Boolean = {
    if(!this.is_complete) { return false }
    val ts = this.get_test_and_solution
    val test = ts.get._1
    val code = ts.get._2
    logger.info("Testing Code =============\n{}\n{}\n", test.toString, code)
    val result = test.accept(this.interpreter, code)
    logger.info("Result: {}", result._1)
    logger.info("Runtime: {}", result._2)
    logger.info("Output =================\n{}\n", result._3)
    result._1
  }
}
