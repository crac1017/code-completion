package edu.rice.pliny.mcts.integer

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.mcts.MCTSTree

import scala.collection.mutable

/**
  * This class grows a number to another number. It is mainly for testing the MCTS framework.
  */
class IntTree(val node : Int,
              val parent : Option[IntTree],
              var visited : Int = 0,
              val children : mutable.ArrayBuffer[IntTree] = mutable.ArrayBuffer.empty[IntTree],
              var reward : Double = 0.0,
              var prior : Double = 0.0,
              var depth : Int = 0,
              var final_reward : Double = 0.0,
              var deadend : Boolean = false) extends MCTSTree {

  val SOLUTION_NUM = 100
  val MAX_NUM = 300

  val logger = Logger(this.getClass)
  type T = Int
  override def get_node: Int = this.node
  override def get_children: Seq[MCTSTree] = this.children
  override def add_children(s : Seq[MCTSTree]): Unit = this.children ++= s.asInstanceOf[Seq[IntTree]]
  override def get_visited : Int = this.visited
  override def set_visited(i : Int): Unit = this.visited = i
  override def get_parent : Option[MCTSTree] = this.parent
  override def get_reward : Double = this.reward
  override def set_reward(v : Double) : Unit = this.reward = v
  override def get_final_reward : Double = this.final_reward
  override def set_final_reward(v : Double) : Unit = this.final_reward = v
  override def get_prior : Double = this.prior
  override def set_prior(v : Double) : Unit = this.prior = v
  override def get_depth : Int = this.depth
  override def set_depth(v : Int) : Unit = this.depth = v

  override def set_deadend(b: Boolean): Unit = this.deadend = b
  override def is_deadend: Boolean = this.deadend

  /**
    * Return a sequence of valid next moves
    */
  override def get_valid_moves: Seq[MCTSTree] = {
    val result = mutable.ArrayBuffer.empty[Int]
    result += (this.node + 1)
    result += (this.node + 2)
    result += (this.node + 3)
    result += (this.node - 1)
    result += (this.node - 2)
    result += (this.node - 3)
    result.map(m => new IntTree(m, Some(this)))
  }

  /**
    * Check whether this node will have potential child nodes.
    */
  override def is_complete: Boolean = this.node >= this.MAX_NUM

  /**
    * Check whether this node is a solution to our problem
    */
  override def is_solution: Boolean = this.node == this.SOLUTION_NUM

  override def toString : String = {
    "Value:   " + this.reward.toString + "\n" +
    "Visited: " + this.visited + "\n" +
    this.node.toString
  }
}
