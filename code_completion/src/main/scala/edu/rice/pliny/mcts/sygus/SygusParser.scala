package edu.rice.pliny.mcts.sygus

import com.typesafe.scalalogging.Logger
import fastparse.all
import fastparse.all._
object SygusParser {
  val logger : Logger = Logger(this.getClass)

  /*
  import fastparse.WhitespaceApi
  val White : WhitespaceApi.Wrapper = WhitespaceApi.Wrapper{
    import fastparse.all._
    NoTrace(" ".rep)
  }
  import fastparse.noApi._
  import White._
  */

  val pletter = P(CharIn('a' to 'z') | CharIn('A' to 'Z'))
  val pdigit = P(CharIn('0' to '9'))
  val psymbol = P(CharIn("=-+_%?."))



  val pdecimal: all.Parser[Unit] = P(pdigit.rep(1))
  val phex = P("#x" ~ (pdigit | CharIn('a' to 'f')).rep(1).!)
  val pspace: all.Parser[Unit] = P(CharsWhileIn(" \r\n").rep)

  // val StringChars = NamedFunction(!"\"\\".contains(_: Char), "StringChars")
  val pescape = P( "\\" ~ CharIn("\"/\\bfnrt") )
  val pstrChars = P(CharsWhile(!"\"\\".contains(_: Char)) )
  val pstring = P(pspace ~ "\"" ~/ (pstrChars | pescape).rep.! ~ "\"").map(SygusAST.Str)

  val pnumber : P[SygusAST.Num] =
    P(pdecimal.!.map(s => SygusAST.Num(s.toInt)) | phex.map(s => SygusAST.Num(java.lang.Long.parseLong(s, 16))))
  val pid : P[SygusAST.Id] = P((pletter | pdigit | psymbol).rep(1)).!.map(SygusAST.Id)

  val patom : P[SygusAST.Expr] = P(pnumber | pid | pstring)
  val psexpr : P[SygusAST.Expr] = P(pexpr ~ pspace).rep(1).map(SygusAST.SExpr)
  val pexpr : P[SygusAST.Expr] = P(patom | ("(" ~ psexpr ~ ")"))

  val pproblem : P[Seq[SygusAST.Expr]] = P(pexpr ~ pspace).rep(1)

  // parse the problem content and store all s-expr into the problem var
  def parse(src : String) : Option[Seq[SygusAST.Expr]] = {
    // delete comments
    val src_no_comment : String = {
      val builder = new StringBuilder
      val lines = src.split("\n")
      for(line <- lines) {
        val semicolon = line.indexOf(';')
        if(semicolon == -1) {
          builder.append(line + "\n")
        } else if(semicolon > 0) {
          builder.append(line.substring(0, semicolon) + "\n")
        }
      }
      builder.toString().trim
    }

    pproblem.parse(src_no_comment) match {
      case Parsed.Success(value, _) =>
        this.logger.info("Parse successful.")
        Some(value)
      case Parsed.Failure(parser, index, extra) =>
        this.logger.error("Failed to parse the problem ===============\n{}\n", src)
        this.logger.error("Trace : {}\n", extra.traced.trace)
        None
    }
  }
}
