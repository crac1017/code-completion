package edu.rice.pliny.mcts.sygus

import com.github.javaparser.JavaParser
import com.github.javaparser.ast.NodeList
import com.github.javaparser.ast.`type`.{PrimitiveType, Type}
import com.github.javaparser.ast.expr._
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.mcts.sygus.SygusAST.{Id, Num, SExpr, Str}

import scala.collection.mutable

object SygusUtils {

  val logger = Logger(this.getClass)

  /**
    * Convert this into a java expression
    */
  def to_java_expr(e : SygusAST.Expr) : Expression = e match {
    case SExpr(exprs) =>
      // function application
      val fname = this.to_java_expr(exprs.head).asInstanceOf[NameExpr].getNameAsString
      fname match {
        case "+" => new BinaryExpr(this.to_java_expr(exprs.tail.head), this.to_java_expr(exprs.tail(1)), BinaryExpr.Operator.PLUS)
        case "-" => new BinaryExpr(this.to_java_expr(exprs.tail.head), this.to_java_expr(exprs.tail(1)), BinaryExpr.Operator.MINUS)
        case "int.to.str" =>
          val call = new MethodCallExpr()
          call.setName("toString")
          call.setScope(new NameExpr("Integer"))
          call.getArguments.add(this.to_java_expr(exprs.tail.head))
          call
        case "str.++" =>
          val call = new MethodCallExpr()
          call.setName("concat")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call.getArguments.add(this.to_java_expr(exprs.tail(1)))
          call
        case "str.at" =>
          val call = new MethodCallExpr()
          call.setName("charAt")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call.getArguments.add(this.to_java_expr(exprs.tail(1)))
          call
        case "str.substr" =>
          val call = new MethodCallExpr()
          call.setName("substring")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call.getArguments.add(this.to_java_expr(exprs.tail(1)))
          call.getArguments.add(this.to_java_expr(exprs.tail(2)))
          call
        case "str.replace" =>
          val call = new MethodCallExpr()
          call.setName("replace")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call.getArguments.add(this.to_java_expr(exprs.tail(1)))
          call.getArguments.add(this.to_java_expr(exprs.tail(2)))
          call
        case "str.len" =>
          val call = new MethodCallExpr()
          call.setName("length")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call
        case "str.to.int" =>
          val call = new MethodCallExpr()
          call.setName("parseInt")
          call.setScope(new NameExpr("Integer"))
          call.getArguments.add(this.to_java_expr(exprs.tail.head))
          call
        case "str.indexof" =>
          val call = new MethodCallExpr()
          call.setName("indexOf")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call.getArguments.add(this.to_java_expr(exprs.tail(1)))
          call.getArguments.add(this.to_java_expr(exprs.tail(2)))
          call
        case "str.prefixof" =>
          val call = new MethodCallExpr()
          call.setName("startsWith")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call.getArguments.add(this.to_java_expr(exprs.tail(1)))
          call
        case "str.suffixof" =>
          val call = new MethodCallExpr()
          call.setName("endsWith")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call.getArguments.add(this.to_java_expr(exprs.tail(1)))
          call
        case "str.contains" =>
          val call = new MethodCallExpr()
          call.setName("contains")
          call.setScope(this.to_java_expr(exprs.tail.head))
          call.getArguments.add(this.to_java_expr(exprs.tail(1)))
          call
        case _ =>
          val args = new NodeList[Expression]
          exprs.tail.foreach(a => args.add(this.to_java_expr(a)))
          new MethodCallExpr().setName(fname).setArguments(args)
      }
    case Num(v) => new LongLiteralExpr(v)
    case Id(s) => new NameExpr(s)
    case Str(s) => new StringLiteralExpr(s)
  }


  /**
    * Get the type from the type expression
    */
  def get_type(expr : SygusAST.Expr) : Type = {
    expr match {
      case Id("Int") => PrimitiveType.intType()
      case Id("Bool") => PrimitiveType.booleanType()
      case Id("String") => JavaParser.parseClassOrInterfaceType("String")
      case SExpr(mutable.ArrayBuffer(Id("BitVec"), Num(n))) if n <= 64 => PrimitiveType.longType()
      case t =>
        this.logger.error("Unsupported argument type: {}", t)
        throw new Exception("Unsupported argument type: " + SygusAST.to_src(t))
    }
  }

  /**
    * A function for getting types from reflection classes
    */
  def get_class(expr : SygusAST.Expr) : Class[_] = {
    expr match {
      case Id("Int") => classOf[Int]
      case Id("Bool") => classOf[Boolean]
      case Id("String") => classOf[String]
      case SExpr(mutable.ArrayBuffer(Id("BitVec"), Num(n))) if n <= 64 => classOf[Long]
      case t =>
        this.logger.error("Unsupported argument type: {}", t)
        throw new Exception("Unsupported argument type: " + SygusAST.to_src(t))
    }
  }
}
