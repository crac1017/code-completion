package edu.rice.pliny.mcts

trait SearchResult
// we have no solution. Stop growing the tree
case class NoSolution() extends SearchResult
// this is the best solution we have got so far
case class Solution(root : MCTSTree, target : MCTSTree) extends SearchResult

