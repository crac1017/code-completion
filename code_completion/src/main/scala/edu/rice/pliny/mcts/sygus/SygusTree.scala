package edu.rice.pliny.mcts.sygus

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.expr.{Expression, MethodCallExpr, NameExpr}
import com.github.javaparser.ast.visitor.TreeVisitor
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.language.java._
import edu.rice.pliny.mcts.MCTSTree

import scala.collection.mutable

object SygusTree {
  // maximum expression size
  val MAX_SIZE : Int = 30
}

class SygusTree(val expr : Expression,
                val grammar : SygusGrammar,
                val curr_size : Int,
                val max_size : Int,
                val parent : Option[SygusTree],
                val interpreter : JavaFastInterpreter,
                val draft : JavaDraft,
                var visited : Int = 0,
                val children : mutable.ArrayBuffer[SygusTree] = mutable.ArrayBuffer.empty[SygusTree],
                var reward : Double = 0.0,
                var prior : Double = 0.0,
                var depth : Int = 0,
                var deadend : Boolean = false,
                var final_reward : Double = 0.0) extends MCTSTree {

  val logger = Logger(this.getClass)
  type T = Expression

  override def hashCode : Int = this.expr.hashCode()

  /**
    * Calculate the size of a given expression
    */
  private def get_node_size(e : Expression) : Int = {
    var node_size = 0
    new TreeVisitor {
      override def visitPreOrder(node: Node): Unit = {
        this.process(node)
      }
      override def process(node : Node): Unit = {
        node match {
          case mce : MethodCallExpr if mce.getNameAsString == JavaUtils.HOLE_NAME =>
            node_size += 1
          case mce : MethodCallExpr =>
            if(mce.getNameAsString == JavaUtils.HOLE_NAME) {
              node_size += 1
            } else {
              node_size += 1
              this.process(mce.getName)
              if(mce.getScope.isPresent) {
                this.process(mce.getScope.get)
              }
              for(i <- 0 until mce.getArguments.size()) {
                this.process(mce.getArguments.get(i))
              }
            }
          case ne : NameExpr =>
            node_size += 1
          case _ =>
            node_size += 1
            for(i <- 0 until node.getChildNodes.size()) {
              this.process(node.getChildNodes.get(i))
            }
        }
      }
    }.visitPreOrder(e)
    node_size
  }
  /**
    * Return a sequence of valid next moves
    */
  override def get_valid_moves : Seq[SygusTree] = {
    val moves = this.grammar.get_moves(this.expr)
    val sizes = moves.map(this.get_node_size)
    moves.zip(sizes)
      .filter(p => p._2 < this.max_size)
      .map(p =>
        new SygusTree(p._1,
          this.grammar,
          p._2,
          max_size,
          Some(this),
          this.interpreter,
          draft = this.draft,
          depth = this.depth + 1))
  }

  /**
    * Check whether this node is a solution to our problem
    */
  override def is_solution: Boolean = {
    if(!this.is_complete) { return false }
    val final_draft = this.draft.replace(this.draft.get_expr_hole.get, new JavaDraftNode(this.expr))
    val test = final_draft.get_test.get
    val result = test.accept(this.interpreter, final_draft.toString)
    logger.debug("Result: {}", result._1)
    logger.debug("Runtime: {}", result._2)
    logger.debug("Output =================\n{}\n", result._3)
    result._1
  }

  override def toString : String = {
    "Final Reward   :   " + this.final_reward.toString + "\n" +
      "Reward         :   " + this.reward.toString + "\n" +
      // "Prior          :   " + this.prior.toString + "\n" +
      "Visited        : " + this.visited + "\n" +
      "Parent Visited : " + (if(this.parent.isDefined) this.parent.get.visited else "N/A") + "\n" +
      "Deadend        : " + this.deadend + "\n" +
      "Draft          : " + this.expr.toString + " (" + this.get_node_size(this.expr) + ")" + "\n"
  }

  /**
    * Check whether this node will have potential child nodes.
    */
  override def is_complete: Boolean = !this.expr.toString().contains(JavaUtils.HOLE_NAME)
  override def get_node: T = this.expr
  override def get_children: Seq[MCTSTree] = this.children
  override def add_children(s : Seq[MCTSTree]): Unit = this.children ++= s.asInstanceOf[Seq[SygusTree]]
  override def get_visited : Int = this.visited
  override def set_visited(i : Int): Unit = this.visited = i
  override def get_parent : Option[MCTSTree] = this.parent
  override def get_reward : Double = this.reward
  override def set_reward(v : Double) : Unit = this.reward = v
  override def get_final_reward : Double = this.final_reward
  override def set_final_reward(v : Double) : Unit = this.final_reward = v
  override def set_deadend(b: Boolean): Unit = this.deadend = b
  override def is_deadend: Boolean = this.deadend
  override def get_depth : Int = this.depth
  override def set_depth(d : Int) : Unit = this.depth = d
  override def get_prior: Double = this.prior
  override def set_prior(v: Double): Unit = this.prior = v
}
