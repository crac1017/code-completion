package edu.rice.pliny.mcts.integer

import edu.rice.pliny.mcts.MCTSTree
import edu.rice.pliny.mcts.evaluator.NodeEvaluator

/**
  * This class evaluate a node by looking at how close it is to the
  * given reference program
  */
class IntDistEvaluator(target : Int) extends NodeEvaluator {
  override def evaluate(tree: MCTSTree): Double = {
    val int_tree = tree.asInstanceOf[IntTree]
    (target - Math.abs(int_tree.node - target)).toDouble
  }
}

