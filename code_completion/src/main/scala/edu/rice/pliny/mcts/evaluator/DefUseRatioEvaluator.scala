package edu.rice.pliny.mcts.evaluator

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.body.{Parameter, VariableDeclarator}
import com.github.javaparser.ast.expr.NameExpr
import com.github.javaparser.ast.visitor.TreeVisitor
import edu.rice.pliny.draft.Draft
import edu.rice.pliny.language.java.JavaDraft
import edu.rice.pliny.mcts.{DraftTree, MCTSTree}

import scala.collection.mutable

/**
  * This class evaluate a node by randomly make moves on the node
  */
class DefUseRatioEvaluator extends NodeEvaluator {

  /**
    * Calculate the def and usage ratio. Must return a double in [0.0, 1.0]
    */
  def def_use_ratio(draft: Draft): Double = {
    val jdraft = draft.asInstanceOf[JavaDraft]
    val def_set = jdraft.defset
    val use_set = mutable.Set.empty[String]

    new TreeVisitor {
      override def visitPreOrder(node: Node): Unit = {
        this.process(node)
      }

      override def process(node: Node): Unit = {
        node match {
          case pm : Parameter => Unit
          case vdor : VariableDeclarator => if(vdor.getInitializer.isPresent) this.visitPreOrder(vdor.getInitializer.get())
          case name : NameExpr => use_set += name.getName.asString()
          case _ =>
            for(i <- 0 until node.getChildNodes.size()) {
              this.visitPreOrder(node.getChildNodes.get(i))
            }
        }
      }
    }.visitPreOrder(jdraft.method)

    val used_count = def_set.foldLeft(0)((acc, name) => if(use_set.contains(name)) acc + 1 else acc)
    used_count.toDouble / def_set.size.toDouble
  }

  override def evaluate(tree: MCTSTree): Double = {
    val draft = tree.asInstanceOf[DraftTree].draft
    this.def_use_ratio(draft)
  }
}
