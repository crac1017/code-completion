package edu.rice.pliny.mcts

abstract class MCTSTree {
  type T
  def get_node : T
  def get_children : Seq[MCTSTree]
  def add_children(s : Seq[MCTSTree]) : Unit
  def get_visited : Int
  def set_visited(i : Int) : Unit
  def get_parent : Option[MCTSTree]
  def get_reward : Double
  def set_reward(v : Double) : Unit
  def get_final_reward : Double
  def set_final_reward(v : Double) : Unit
  def get_prior : Double
  def set_prior(v : Double) : Unit
  def get_depth : Int
  def set_depth(v : Int) : Unit

  /**
    * Sometimes a path in the tree might be a deadend that we don't
    * want to go in
    */
  def set_deadend(b : Boolean) : Unit
  def is_deadend : Boolean

  /**
    * Return a sequence of valid next moves
    */
  def get_valid_moves : Seq[MCTSTree]

  /**
    * Check whether this node will have potential child nodes.
    */
  def is_complete : Boolean

  /**
    * Check whether this node is a solution to our problem
    */
  def is_solution : Boolean

  def toString : String
}
