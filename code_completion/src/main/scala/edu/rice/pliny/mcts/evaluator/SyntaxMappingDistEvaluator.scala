package edu.rice.pliny.mcts.evaluator

import com.github.javaparser.ast.body._
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.`type`.PrimitiveType
import com.github.javaparser.ast.visitor.{ModifierVisitor, Visitable}
import com.github.javaparser.ast.{Node, NodeList}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.language.java.{JavaDraft, JavaDraftIteratorFactory, JavaDraftNode, JavaUtils}
import edu.rice.pliny.mcts.{DraftTree, HungarianAlgorithm, MCTSTree}

import scala.collection.JavaConverters._
import scala.collection.mutable

/**
  * This distance metric does min-bipartite matching on the nodes of a graph. This is the current distance metric we
  * are using.
  */
class SyntaxMappingDistEvaluator(ref_prog : JavaDraft) extends NodeEvaluator {

  val logger = Logger(this.getClass)
  var consider_order = true

  def get_post_order(jd : JavaDraft) : (Array[Node], mutable.Map[Int, Int]) = {
    val result = mutable.Map.empty[Int, Int]
    val iter = JavaDraftIteratorFactory.create_draft_iterator(jd)
    iter.get_all_nodes.zipWithIndex.foreach(p => {
      val hs = java.lang.System.identityHashCode(p._1.asInstanceOf[JavaDraftNode].node)
      result += ((hs, p._2))
    })
    (iter.get_all_nodes.map(d => d.asInstanceOf[JavaDraftNode].node), result)
  }

  val ref_skeleton = ref_prog.create_new_draft(this.gen_skeleton(ref_prog.method).asInstanceOf[MethodDeclaration])
  val (ref_nodes : Array[Node], ref_post_order : mutable.Map[Int, Int]) = this.get_post_order(ref_skeleton)

  var draft_nodes : Array[Node] = _
  var draft_post_order : mutable.Map[Int, Int] = _

  val REDUNDANT_HOLE_COST : Int = 100

  def is_hole(node : Node) : Boolean = {
    node match {
      case es : ExpressionStmt =>
        is_hole(es.getExpression)
      case mce : MethodCallExpr => mce.getNameAsString == JavaUtils.HOLE_NAME
      case _ => false
    }
  }

  def node_dist(n1 : Option[Node],
                n2 : Option[Node]) : (Int, Array[(Int, Int, Int)]) = {
    if(n1.isEmpty && n2.isEmpty) {
      (0, Array.empty[(Int, Int, Int)])
    } else {
      var c1 : Seq[Node] = Seq.empty[Node]
      var c2 : Seq[Node] = Seq.empty[Node]
      var label_diff = 1
      var type_diff = 0
      if(n1.isDefined && n2.isEmpty) {
        c1 = n1.get.getChildNodes.asScala
        if(this.is_hole(n1.get)) {
          label_diff = this.REDUNDANT_HOLE_COST
        }
      } else if(n2.isDefined && n1.isEmpty) {
        c2 = n2.get.getChildNodes.asScala
        if(this.is_hole(n2.get)) {
          label_diff = this.REDUNDANT_HOLE_COST
        }
      } else {
        c1 = n1.get.getChildNodes.asScala
        c2 = n2.get.getChildNodes.asScala
        if(n1.get.isInstanceOf[MethodDeclaration]) {
          c1 = c1.filter(!(_: Node).isInstanceOf[Parameter])
        }
        if(n2.get.isInstanceOf[MethodDeclaration]) {
          c2 = c2.filter(!(_: Node).isInstanceOf[Parameter])
        }

        label_diff = if(n1.get.getClass.equals(n2.get.getClass)) 0 else 1
        if(this.is_hole(n1.get) || this.is_hole(n2.get)) {
          label_diff = 0
        }

        // check the type of function and tuple
        (n1.get, n2.get) match {
          case (p1 : ArrayInitializerExpr, p2 : ArrayInitializerExpr) => if(p1.getValues.size != p2.getValues.size) type_diff = 1
          case (p1 : MethodCallExpr, p2 : MethodCallExpr) =>
            if(p1.getArguments.size != p2.getArguments.size) type_diff = 1
          case (_, _) =>
        }
      }
      if(c1.isEmpty && c2.isEmpty) {
        return (label_diff + type_diff, Array.empty[(Int, Int, Int)])
      }


      if(c1.length >= c2.length) {
        // construct the distance matrix
        val dist_mat = mutable.ArrayBuffer.empty[Array[Double]]
        val sub_mapping = mutable.ArrayBuffer.empty[Array[Array[(Int, Int, Int)]]]
        for(i <- c1.indices) {
          val row = mutable.ArrayBuffer.empty[Double]
          val row_mapping = mutable.ArrayBuffer.empty[Array[(Int, Int, Int)]]
          for(j <- c1.indices) {
            if(j >= c2.length) {
              val result = this.node_dist(Some(c1(i)), None)
              row += result._1
              row_mapping += result._2
            } else {
              if(this.is_hole(c1(i)) || this.is_hole(c2(j))) {
                row += 0
                row_mapping += Array.empty[(Int, Int, Int)]
              } else {
                val result = this.node_dist(Some(c1(i)), Some(c2(j)))

                row += result._1
                row_mapping += result._2
              }
            }
          }
          sub_mapping += row_mapping.toArray
          dist_mat += row.toArray
        }
        // run min-bipartite matching
        val alg = new HungarianAlgorithm(dist_mat.toArray)
        val mapping = alg.execute()
        val final_mapping = mutable.ArrayBuffer.empty[(Int, Int, Int)]
        var dist : Double = 0.0
        for(i <- 0 until mapping.length) {
          val d = dist_mat(i)(mapping(i))
          dist += d
          final_mapping ++= sub_mapping(i)(mapping(i))
          if(mapping(i) < c2.length) {
            var order_penalty = 0
            if(this.consider_order && c1(i).isInstanceOf[Statement] && c2(mapping(i)).isInstanceOf[Statement] &&
              !this.is_hole(c1(i)) && !this.is_hole(c2(mapping(i)))) {
              for(j <- 0 until i) {
                if(c1(i).isInstanceOf[Statement] &&
                  mapping(i) < c2.length && c2(mapping(i)).isInstanceOf[Statement] &&
                  mapping(i) < mapping(j) &&
                  JavaUtils.depends(c1(i), c1(j))) {
                  order_penalty += 1
                  dist += 1
                }
              }
            }

            val m = (this.draft_post_order(java.lang.System.identityHashCode(c1(i))), this.ref_post_order(java.lang.System.identityHashCode(c2(mapping(i)))), d.toInt + order_penalty)
            final_mapping += m
          } else {
            val m = (this.draft_post_order(java.lang.System.identityHashCode(c1(i))), -1, d.toInt)
            final_mapping += m
          }
        }
        (dist.toInt + label_diff + type_diff, final_mapping.toArray)
      } else {
        // construct the distance matrix
        val dist_mat = mutable.ArrayBuffer.empty[Array[Double]]
        val sub_mapping = mutable.ArrayBuffer.empty[Array[Array[(Int, Int, Int)]]]
        for(i <- c2.indices) {
          val row = mutable.ArrayBuffer.empty[Double]
          val row_mapping = mutable.ArrayBuffer.empty[Array[(Int, Int, Int)]]
          for(j <- c2.indices) {
            if(j >= c1.length) {
              val result = this.node_dist(None, Some(c2(i)))
              row += result._1
              row_mapping += result._2
            } else {
              if(this.is_hole(c1(j)) || this.is_hole(c2(i))) {
                row += 0
                row_mapping += Array.empty[(Int, Int, Int)]
              } else {
                val result = this.node_dist(Some(c1(j)), Some(c2(i)))
                row += result._1
                row_mapping += result._2
              }
            }
          }
          sub_mapping += row_mapping.toArray
          dist_mat += row.toArray
        }
        // run min-bipartite matching
        val alg = new HungarianAlgorithm(dist_mat.toArray)
        val mapping = alg.execute()
        val final_mapping = mutable.ArrayBuffer.empty[(Int, Int, Int)]
        var dist : Double = 0.0
        for(i <- 0 until mapping.length) {
          val d = dist_mat(i)(mapping(i))
          dist += d
          final_mapping ++= sub_mapping(i)(mapping(i))
          if(mapping(i) < c1.length) {
            // order penalty
            var order_penalty = 0
            if(this.consider_order && c1(mapping(i)).isInstanceOf[Statement] && c2(i).isInstanceOf[Statement] &&
              !this.is_hole(c1(mapping(i))) && !this.is_hole(c2(i))) {
              for(j <- 0 until i) {
                if(mapping(j) < c1.length && c1(mapping(j)).isInstanceOf[Statement] &&
                  c2(j).isInstanceOf[Statement] &&
                  mapping(i) < mapping(j) &&
                  JavaUtils.depends(c2(i), c2(j))) {
                  order_penalty += 1
                  dist += 1
                }
              }
            }

            val m = (this.draft_post_order(java.lang.System.identityHashCode(c1(mapping(i)))), this.ref_post_order(java.lang.System.identityHashCode(c2(i))), d.toInt + order_penalty)
            final_mapping += m
          } else {
            val hs = java.lang.System.identityHashCode(c2(i))
            val m = (-1, this.ref_post_order(hs), d.toInt)
            final_mapping += m
          }
        }
        (dist.toInt + label_diff + type_diff, final_mapping.toArray.sortBy(p => p._1))
      }
    }
  }

  def calculate_distance(x: JavaDraft) : (Int, Array[(Int, Int, Int)]) = {
    val result = this.node_dist(Some(x.method), Some(this.ref_skeleton.method))
    val mapping = result._2.:+(this.draft_post_order(java.lang.System.identityHashCode(x.method.getBody.get())), this.ref_post_order(java.lang.System.identityHashCode(this.ref_skeleton.method.getBody.get())), result._1)
    (result._1, mapping)
  }

  override def evaluate(tree: MCTSTree) : Double = {
    val draft = {
      val d = tree.asInstanceOf[DraftTree].draft.asInstanceOf[JavaDraft]
      d.create_new_draft(this.gen_skeleton(d.method).asInstanceOf[MethodDeclaration])
    }
    val result = this.get_post_order(draft)
    this.draft_nodes = result._1
    this.draft_post_order = result._2
    this.logger.debug("Draft ==================\n{}\n", draft.toString)
    this.logger.debug("Ref ==================\n{}\n", this.ref_skeleton.toString)
    val (dist, mapping) = this.calculate_distance(draft)
    for(m <- mapping) {
      val (a_node, a_type) =
        if(m._1 != -1) {
          val node = draft_nodes(m._1)
          (node.toString, node.getClass.toString)
        } else {
          ("None", "None")
        }
      val (b_node, b_type) =
        if(m._2 != -1) {
          val node = ref_nodes(m._2)
          (node.toString, node.getClass.toString)
        } else {
          ("None", "None")
        }
      this.logger.debug("{} ({}) -> {} ({}) : {}", a_node, a_type, b_node, b_type, m._3)
    }
    Math.max(1.0 - dist.toDouble / (draft.get_node_count + this.ref_skeleton.get_node_count).toDouble, 0.0)
  }

  private def gen_skeleton(node : Node) : Node = {
    val abs_visitor = new ModifierVisitor[Any] {
      override def visit(node : AssertStmt, arg : Any) = {
        node.setCheck(new NameExpr("#"))
        node
      }
      override def visit(node : DoStmt, arg : Any) = {
        node.setCondition(new NameExpr("#"))
        node.getBody.accept[Visitable, Any](this, null)
        node
      }
      override def visit(node : ForeachStmt, arg : Any) = {
        node.setIterable(new NameExpr("#"))
        node.setVariable(new VariableDeclarationExpr(new PrimitiveType(PrimitiveType.Primitive.INT), "#"))
        node.getBody.accept[Visitable, Any](this, null)
        node
      }
      override def visit(node : IfStmt, arg : Any) = {
        node.setCondition(new NameExpr("#"))
        node.getThenStmt.accept[Visitable, Any](this, null)
        if(node.getElseStmt.isPresent) {
          node.getElseStmt.get().accept[Visitable, Any](this, null)
        }
        node
      }
      override def visit(node : ReturnStmt, arg : Any) = {
        node.setExpression(new NameExpr("#"))
        node
      }
      override def visit(node : SwitchStmt, arg : Any) = {
        node.setSelector(new NameExpr("#"))
        val entries = node.getEntries
        for(i <- 0 until entries.size()) {
          val slist = entries.get(i).getStatements
          for(j <- 0 until slist.size()) {
            val stmt = slist.get(j)
            stmt.accept[Visitable, Any](this, null)
          }
        }
        node
      }
      override def visit(node : SynchronizedStmt, arg : Any) = {
        node.setExpression(new NameExpr("#"))
        node.getBody.accept[Visitable, Any](this, null)
        node
      }
      override def visit(node : WhileStmt, arg : Any) = {
        node.setCondition(new NameExpr("#"))
        node.getBody.accept[Visitable, Any](this, null)
        node
      }
      override def visit(p : Parameter, arg : Any) = {
        p.setName("#")
        p
      }
      override def visit(node : ExpressionStmt, arg : Any) = {
        node.getExpression match {
          case mce : MethodCallExpr if mce.getNameAsString == JavaUtils.HOLE_NAME =>
            //new ExpressionStmt(new MethodCallExpr().setName(new SimpleName("__pliny_hole__")))
            // new EmptyStmt()
            node
          case _ =>
            node.setExpression(node.getExpression.accept[Visitable, Any](this, null).asInstanceOf[Expression])
        }
      }
      override def visit(vdor : VariableDeclarator, arg : Any) = {
        if(vdor.getInitializer.isPresent) {
          val new_init = vdor.getInitializer.get().accept[Visitable, Any](this, null).asInstanceOf[Expression]
          vdor.setInitializer(new_init)
        } else {
          new VariableDeclarator(vdor.getType, "#")
        }
      }
      override def visit(mc : MethodCallExpr, arg : Any) = {
        if(mc.getName.getIdentifier.equals(JavaUtils.HOLE_NAME)) {
          /*
          if(mc.getParentNode.get().isInstanceOf[ExpressionStmt]) {
            mc
          } else {
            new NameExpr("#")
          }
          */
          mc
        } else {
          val new_args = new NodeList[Expression]()
          for(i <- 0 until mc.getArguments.size()) {
            new_args.add(mc.getArguments.get(i).accept[Visitable, Any](this, null).asInstanceOf[Expression])
          }
          if(mc.getScope.isPresent) {
            val new_scope = mc.getScope.get().accept[Visitable, Any](this, null).asInstanceOf[Expression]
            new MethodCallExpr(new_scope, mc.getName, new_args)
          } else {
            new MethodCallExpr().setName(mc.getName).setArguments(new_args)
          }
        }
      }
      override def visit(node : NameExpr, arg : Any) = {
        new NameExpr("#")
      }
      override def visit(lit : IntegerLiteralExpr, arg : Any) = {
        new NameExpr("#")
      }
    }

    node.clone().accept[Visitable, Any](abs_visitor, null).asInstanceOf[Node]
  }
}