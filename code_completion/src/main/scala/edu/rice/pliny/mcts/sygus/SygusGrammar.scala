package edu.rice.pliny.mcts.sygus

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.expr.{Expression, MethodCallExpr, NameExpr, StringLiteralExpr}
import com.github.javaparser.ast.visitor.{ModifierVisitor, TreeVisitor, Visitable}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.language.java.{JavaDraft, JavaDraftHole, JavaDraftNode, JavaUtils}
import edu.rice.pliny.mcts.sygus.SygusAST.{Id, SExpr}

import scala.collection.mutable

/**
  * Create a grammar given a list of s-expr
  *
  * @param rule_list - a list of production rules
  */
class SygusGrammar(rule_list : Seq[SygusAST.SExpr]) {

  // this keeps the type of the symbol and its corresponding hole
  val symbol_type : mutable.Map[String, Class[_]] = mutable.Map.empty[String, Class[_]]
  var modifier : ModifierVisitor[Any] = _
  val logger = Logger(this.getClass)

  def read_production_rule(prod_expr : SygusAST.SExpr) : (String, mutable.Set[Expression])= {
    val start_symbol = prod_expr.exprs.head.asInstanceOf[Id].v

    // type of this production
    val t = SygusUtils.get_class(prod_expr.exprs(1))

    // read all the productions
    val productions : mutable.Set[Expression] = {
      val result : mutable.Set[Expression] = mutable.Set.empty[Expression]
      prod_expr.exprs(2).asInstanceOf[SExpr].exprs
        .map(SygusUtils.to_java_expr)
        .foreach(e => {
          result += e.accept[Visitable, Any](this.modifier, null).asInstanceOf[Expression]
        })
      result
    }
    (start_symbol, productions)
  }

  // read all the production rules
  val moves: mutable.Map[String, mutable.Set[Expression]] = {
    val result = mutable.Map.empty[String, mutable.Set[Expression]]

    // record all the symbol type
    rule_list.foreach(rl => {
      val symbol = rl.exprs.head.asInstanceOf[Id].v
      // type of this production
      val t = SygusUtils.get_class(rl.exprs(1))
      this.symbol_type.put(symbol, t)

      this.modifier = new ModifierVisitor[Any] {
        override def visit(name : NameExpr, arg : Any) : Visitable = {
          if(symbol_type.contains(name.getNameAsString)) {
            val hole = JavaUtils.gen_expr_hole_without_test(Some(symbol_type(name.getNameAsString))).asInstanceOf[MethodCallExpr]
            hole.getArguments.add(new StringLiteralExpr(name.getNameAsString))
            hole
          } else {
            name
          }
        }
      }
    })

    rule_list.foreach(rl => {
      val (start_symbol, rules) = read_production_rule(rl)
      result.put(start_symbol, rules)
    })
    result
  }

  /**
    * Get a list of moves given a java program
    */
  def get_moves(draft : JavaDraft) : Seq[JavaDraft] = {
    val result : mutable.ArrayBuffer[JavaDraft] = mutable.ArrayBuffer.empty[JavaDraft]
    val holes = draft.get_all_expr_hole
    holes.foreach(h => {
      val hcall = h.asInstanceOf[JavaDraftHole].node.asInstanceOf[MethodCallExpr]
      val symbol : String =
        if(hcall.getArguments.size() > 2) {
          hcall.getArguments.get(2).asInstanceOf[StringLiteralExpr].asString()
        } else {
          "Start"
        }
      val moves = this.moves(symbol.toString)
      moves.foreach(m => {
        result += draft.replace(h, new JavaDraftNode(m)).asInstanceOf[JavaDraft]
      })
    })
    result
  }

  /*
  private def get_hole(expr : Expression) : Option[MethodCallExpr] = {

  }*/

  /**
    * Get a list of moves given a java expression
    */
  def get_moves(input_expr : Expression) : Seq[Expression] = {
    val result : mutable.ArrayBuffer[Expression] = mutable.ArrayBuffer.empty[Expression]
    new TreeVisitor {
      override def process(node: Node) : Unit = {
        node match {
          case mce : MethodCallExpr if mce.getNameAsString == JavaUtils.HOLE_NAME =>
            val symbol : String =
              if(mce.getArguments.size() > 2) {
                mce.getArguments.get(2).asInstanceOf[StringLiteralExpr].asString()
              } else {
                "Start"
              }
            moves(symbol).foreach(m => {
              result += JavaUtils.replace(input_expr.clone(), mce, m).asInstanceOf[Expression]
            })
          case _ =>
        }
      }
    }.visitPreOrder(input_expr.clone())
    result
  }
}
