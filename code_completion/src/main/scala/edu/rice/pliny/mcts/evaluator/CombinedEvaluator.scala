package edu.rice.pliny.mcts.evaluator

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.mcts.MCTSTree

/**
  * This class evaluate a node by looking at how close it is to the
  * given reference program
  */
class CombinedEvaluator(evaluators : Seq[NodeEvaluator], weights : Seq[Double]) extends NodeEvaluator {
  val logger = Logger(this.getClass)

  override def evaluate(tree: MCTSTree): Double = {
    assert(weights.sum == 1.0)
    val scores = evaluators.map(e => e.evaluate(tree))
    scores.zip(evaluators).foreach(e => this.logger.debug("Score for {} : {}", e._2.getClass.toString, e._1))
    val result = scores.zip(weights).foldLeft(0.0)((acc, s) => acc + s._1 * s._2)
    this.logger.debug("Combined score: {}", result)
    result
  }
}
