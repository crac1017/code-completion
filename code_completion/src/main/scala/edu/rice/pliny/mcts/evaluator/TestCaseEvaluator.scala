package edu.rice.pliny.mcts.evaluator

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.language.java.JavaDraftNode
import edu.rice.pliny.mcts.sygus.SygusTree
import edu.rice.pliny.mcts.{DraftTree, MCTSTree}

import scala.collection.mutable
import scala.util.Random


object TestCaseEvaluator {
  val TEST_NUM_TOKEN = "_test_num_"
  val MAX_SIM = 20
  val MAX_DEPTH = 1000
  val OLD_SCORE : mutable.Map[Int, Double] = mutable.Map.empty[Int, Double]
}

/**
  * This class evaluate a node by looking at how close it is to the
  * given reference program
  */
class TestCaseEvaluator(max_depth : Int) extends NodeEvaluator {
  val logger = Logger(this.getClass)
  var solution : Option[MCTSTree] = None

  /**
    * Exhaustive randomness. We might not be using it, because it keeps making small changes in the search and it is
    * not efficient compared to real randomness.
    */
  object FRandom {
    var record_size : Int = 0
    val record : Array[mutable.Queue[Int]] =
      Array.fill(max_depth)(mutable.Queue.empty[Int])
    val depth_map : Array[(Int, Int)] = Array.fill(max_depth * 2)((-1, -1))
    var latest_depth : Int = -1

    def clear() : Unit = {
      this.record_size = 0
      this.latest_depth = -1
      for(i <- this.depth_map.indices) {
        this.depth_map.update(i, (-1, -1))
      }
    }

    def forward(depth : Int) : Unit = {
      val (new_depth, prev) = this.depth_map(depth)
      if(new_depth == -1) {
        return
      }
      // println("Forward: " + depth + " new_depth: " + new_depth + " prev: " + prev)
      this.record_size = new_depth + 1
      this.record(new_depth).dequeue()
      if(this.record(new_depth).isEmpty) {
        // this.record_size -= 1
        this.depth_map(depth) = (-1, -1)
        if(prev >= 0) {
          this.latest_depth = prev
          this.forward(prev)
        }
      }
    }

    /**
      * Get the next non-picked integer
      */
    def next_int(max : Int, depth : Int) : Int = {
      // println("DEPTH: " + depth + " MAX: " + max)

      // since depth doesn't necessariliy starts from 0 and sometimes it will jump forward by a few numbers, we are
      // creating an indirection by mapping the first depth into 0 and second depth into 1 and etc.
      // this new_depth is the mapped depth used to index the record
      val new_depth =
        if(this.depth_map(depth)._1 == -1) {
          // if we haven't seen this depth number, it means that we need to push a new set of indexes into the stack
          this.record(this.record_size).clear()
          this.record(this.record_size) ++= Random.shuffle((0 until max).toList)
          this.depth_map(depth) = (this.record_size, this.latest_depth)
          this.latest_depth = depth
          this.record_size += 1
          this.record_size - 1
        } else {
          this.depth_map(depth)._1
        }

      val ret = this.record(new_depth).head
      // println("depth: " + depth + " max: " + max + " new_depth: " + new_depth + " ret: " + ret)
      ret
    }
  }

  /**
    * Randomly complete a draft
    */
  def random_complete(dtree : MCTSTree) : Option[MCTSTree] = {
    if(dtree.is_complete) {
      // FRandom.forward(dtree.get_parent.get.get_depth)
      Some(dtree)
    } else {
      val moves = dtree.get_valid_moves
      if(moves.isEmpty) {
        None
      } else {
        // this.random_complete(moves(FRandom.next_int(moves.size, dtree.get_depth)))
        this.random_complete(moves(Random.nextInt(moves.size)))
      }
    }
  }

  /**
    * Get the test score
    */
  def get_test_score(tree : MCTSTree) : Double = {
    val (draft, interpreter) = tree match {
      case dt : DraftTree =>
        (dt.draft, dt.interpreter)
      case st : SygusTree =>
        val final_draft = st.draft.replace(st.draft.get_expr_hole.get, new JavaDraftNode(st.expr))
        (final_draft, st.interpreter)
      case _ => throw new Exception("There is no draft for this tree.")
    }

    val result = draft.get_test.get.accept(interpreter, draft.toString)
    val inter = interpreter.inter

    // get the number of test cases
    val test_num = inter.get(TestCaseEvaluator.TEST_NUM_TOKEN).asInstanceOf[Int]
    var pass_count = 0

    for(i <- 0 until test_num) {
      val case_token = s"_case_${i}_"
      val pass = inter.get(case_token).asInstanceOf[Boolean]
      pass_count += (if(pass) 1 else 0)
    }

    val score = pass_count.toDouble / test_num.toDouble
    this.logger.debug("Score : {} - {}/{}", score, pass_count, test_num)
    /*
    if(score >= 1.0) {
      this.solution = Some(tree)
    }
    */
    if(score.isNaN) {
      0.0
    } else {
      score
    }
  }

  override def evaluate(tree: MCTSTree) : Double = {
    // this.FRandom.clear()
    this.logger.info("Evaluating ===================\n{}\n", tree.toString)
    var hash_hit_count : Int = 0
    val scores = (0 until TestCaseEvaluator.MAX_SIM).map(_ => {
      // this.logger.info("Completing program =============\n{}\n", stree.draft)
      val completed_tree = this.random_complete(tree)
      if(completed_tree.isDefined) {
        val hash = completed_tree.hashCode()
        if(TestCaseEvaluator.OLD_SCORE.contains(hash)) {
          hash_hit_count += 1
          (completed_tree, TestCaseEvaluator.OLD_SCORE(hash))
        } else {
          // this.logger.info("Getting score for completing program =============\n{}\n", completed_tree.get.draft)
          val score = this.get_test_score(completed_tree.get)
          TestCaseEvaluator.OLD_SCORE.put(hash, score)
          (completed_tree, score)
        }
      } else {
        (None, 0.0)
      }
    })
    this.logger.info("Hash hit: {}/{}", hash_hit_count, TestCaseEvaluator.MAX_SIM)
    val result = scores.map(_._2).max
    if(result == Double.NaN) {
      this.logger.warn("Maximum test score is 0.0")
      0.0
    } else {
      if(result == 0.0) {
        this.logger.warn("Maximum test score is 0.0")
      } else {
        this.logger.info("Maximum test score is not zero: {}", result)
      }
      result
    }
  }
}
