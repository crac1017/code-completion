package edu.rice.pliny.mcts

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.mcts.evaluator.{NodeEvaluator, TestCaseEvaluator}
import edu.rice.pliny.mcts.sygus.SygusTree

import scala.collection.mutable
import scala.util.Random


/**
  * Result classes
  */
/**
  * This is the main class for monte carlo tree search algorithm
  */
class BanditSearch(val evaluator : NodeEvaluator) {
  val logger = Logger(this.getClass)
  // def EPS : Double = 1e-6 * Random.nextDouble()
  val EPS : Double = 1e-6

  // this is the result of this MCTS task
  var search_result : SearchResult = NoSolution()

  /**
    * Expand the leaf and generate one or more children
    */
  private def expand(leaf : MCTSTree) : Unit = {
    logger.info("Expanding ================\n" + leaf.toString)
    val new_children = leaf.get_valid_moves
    // new_children.foreach(child => child.set_prior(prior_eval.evaluate(child)))
    new_children.foreach(child => child.set_reward(evaluator.evaluate(child)))
    logger.info("{} children after expanding.", new_children.size)
    leaf.add_children(new_children)
  }

  /**
    * Update the current solution
    */
  private def update_solution(root : MCTSTree, leaf : MCTSTree) : Unit = {
    // update the mcts result
    this.search_result match {
      case NoSolution() =>
        this.logger.info("NEW SOLUTION =================\n{}\n", leaf)
        this.search_result = Solution(root, leaf)
      case Solution(_, s) =>
        if(s.get_reward < leaf.get_reward) {
          this.logger.info("BETTER SOLUTION ==================\n{}\n", leaf)
          this.search_result = Solution(root, leaf)
        }
    }
  }

  /**
    * Select the best child
    */
  def select_best(tree : MCTSTree, include_deadend : Boolean = false) : Option[MCTSTree] = {
    val valid_children = tree.get_children.filter(c => include_deadend || !c.is_deadend)
    if(valid_children.isEmpty) {
      this.logger.debug("No Best MCTSTree\n")
      return None
    }

    val children = valid_children.map(n => {
      val v = n.get_reward / (n.get_visited.toDouble + this.EPS) +
        (1 / Math.sqrt(2.0)) * Math.sqrt(2.0 * Math.log(tree.get_visited.toDouble + 1.0) / (n.get_visited.toDouble + this.EPS))
      // + Random.nextDouble() * this.EPS
      /*
        */
      /*
      val v = n.get_reward / (n.get_visited + 1).toDouble + Random.nextDouble() * this.EPS
        */
      /*
      val v = n.get_reward + (n.get_prior / Math.max(n.get_visited, 1).toDouble)
      */
      n.set_final_reward(v)
      (n, v)
    })
    children.foreach(c => {
      this.logger.debug("Candidate children with value {} ============\n{}\n", c._2, c._1.toString)
    })

    val best = children.foldLeft(children.head) ((acc, t) => if(acc._2 > t._2) acc else t)._1
    this.logger.info("Best MCTSTree ============\n{}\n", best.toString)

    Some(best)
  }

  /**
    * Make a node and its parent a deadend if the parent has only one child
    */
  def make_deadend(tree : MCTSTree) : Unit = {
    this.logger.info("Deadend  =========\n{}\n", tree.toString)
    tree.set_deadend(true)
    if(tree.get_parent.isDefined && tree.get_parent.get.get_children.count(c => !c.is_deadend) == 0) {
      make_deadend(tree.get_parent.get)
    }
  }

  /**
    * Perform the MCTS on the tree once. Return an optional solution tree node
    */
  def grow(tree : MCTSTree) : SearchResult = {
    this.logger.info("Growing Tree ============\n{}\n", tree.toString)
    val visited = mutable.ArrayBuffer.empty[MCTSTree]

    // walk all the way down to the leaf
    visited += tree
    def select_leaf_helper(node : MCTSTree) : Unit = {
      if(node.get_children.forall(c => c.is_deadend)) {
        return
      }
      val best = this.select_best(node).get
      visited += best
      select_leaf_helper(best)
    }
    select_leaf_helper(tree)
    val leaf = visited.last

    // expand the leaf
    if(!leaf.is_complete) {
      this.expand(leaf)
    }

    if(leaf.get_children.isEmpty) {
      // we are done with this node. Set the path to a deadend
      this.logger.info("Empty children.")
      this.make_deadend(leaf)
      return this.search_result
    }

    // check if we have a solution
    for(c <- leaf.get_children.filter(l => l.is_complete)) {
      if(c.is_solution) {
        this.update_solution(tree, c)
        return this.search_result
      } else {
        this.logger.info("Incorrect solution.")
        this.make_deadend(c)
      }
    }

    val new_best = this.select_best(leaf, include_deadend = true)
    if(new_best.isEmpty) {
      return this.search_result
    }

    visited += new_best.get

    // calculate the new value and propagate it back to all the parents
    val new_val = this.evaluator.evaluate(new_best.get)
    visited.foreach(n => {
      /*
      */
      n.set_reward(new_val + n.get_reward)
      n.set_visited(n.get_visited + 1)
      /*
      n.set_reward(new_best.get.get_reward + n.get_reward)
      n.set_visited(n.get_visited + 1)
      */
      // n.set_reward((new_val + n.get_reward) / n.get_visited.toDouble)
    })

    // return a solution if there's any
    for(n <- visited.filter(v => v.is_complete)) {
      if(n.is_solution) {
        this.update_solution(tree, n)
      } else {
        this.make_deadend(n)
      }
    }

    // check if we have found a solution when we use test case evaluator
    this.evaluator match {
      case tce : TestCaseEvaluator =>
        if(tce.solution.isDefined) {
          this.search_result = Solution(tree, tce.solution.get)
        }
      case _ =>
    }
    this.search_result
  }

  /**
    * Perform the MCTS on the tree. Return a tuple where the first element
    * is the original tree after expansion and the second is an optional
    * solution node
    */
  def complete(tree : MCTSTree, max_iter : Int) : SearchResult = {
    for(_ <- 0 until max_iter) {
      this.grow(tree)
      if(tree.is_deadend) {
        return this.search_result
      } else if(this.search_result.isInstanceOf[Solution]) {
        return this.search_result
      }
    }
    this.search_result
  }

}
