package edu.rice.pliny.mcts.sygus

import com.github.javaparser.ast.NodeList
import com.github.javaparser.ast.`type`.Type
import com.github.javaparser.ast.body.{MethodDeclaration, Parameter}
import com.github.javaparser.ast.expr.Expression
import com.github.javaparser.ast.stmt.{BlockStmt, ReturnStmt, Statement}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.Utils
import edu.rice.pliny.language.java.{JavaDraft, JavaDraftNode, JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.mcts.evaluator.{RandomFillEvaluator, TestCaseEvaluator}
import edu.rice.pliny.mcts.sygus.SygusAST.{Id, SExpr}
import edu.rice.pliny.mcts.vis.TreeVisualizer

import scala.collection.mutable

class SygusInterpreter {

  val cu_str : String =
    """
      |public class Main {
      |    public void main() {
      |    }
      |}
    """.stripMargin
  val cu_template : JavaDraft = JavaUtils.parse_src(this.cu_str, Some("Main"), Some("main"), None, Seq(), Seq()).get

  val logger = Logger(this.getClass)

  // this is the grammar in synth-fun
  var grammar : SygusGrammar = _

  // defined functions
  val defined_funcs : mutable.ArrayBuffer[MethodDeclaration] = mutable.ArrayBuffer.empty[MethodDeclaration]

  // function with a return statement which contains a hole
  var synth_fun : MethodDeclaration = _
  var synth_hole : Expression = _

  // test cases
  val constraints : mutable.ArrayBuffer[String] = mutable.ArrayBuffer.empty[String]

  // synthesize result. Runtime only at the moment
  var synth_result : Long = -1L
  var solution : MethodDeclaration = _

  /**
    * Get the function parameter
    */
  def get_func_params(params : SygusAST.Expr) : NodeList[Parameter] = {
    def helper(param : SygusAST.Expr) : (String, Type) = {
      val exprs = param.asInstanceOf[SExpr].exprs
      val arg_name = exprs.head.asInstanceOf[Id].v
      val arg_type = SygusUtils.get_type(exprs.tail.head)
      (arg_name, arg_type)
    }

    val result = new NodeList[Parameter]
    params.asInstanceOf[SExpr].exprs.foreach(param => {
      val (pname, ptype) = helper(param)
      result.add(new Parameter(ptype, pname))
    })
    result
  }

  /**
    * Get the function boy
    */
  def get_func_body(body : SygusAST.Expr) : BlockStmt = {
    val ret_stmt = new ReturnStmt(SygusUtils.to_java_expr(body))
    val stmt_list = new NodeList[Statement]
    stmt_list.add(ret_stmt)
    new BlockStmt(stmt_list)
  }

  /**
    * Get the grammar from the synth-func command
    */
  def get_synth_grammar(expr: SygusAST.Expr) : SygusGrammar =
    new SygusGrammar(expr.asInstanceOf[SExpr].exprs.asInstanceOf[Seq[SExpr]])

  /**
    * translate a function definition into a java function definition
    * @param func_def - an s-expr representing the function
    */
  def define_func(func_def: SygusAST.SExpr): MethodDeclaration = {
    assert(func_def.exprs.head.asInstanceOf[Id].v == "define-fun")
    val fname = func_def.exprs(1).asInstanceOf[Id].v
    val params = get_func_params(func_def.exprs(2))
    val rettype = SygusUtils.get_type(func_def.exprs(3))
    val body = get_func_body(func_def.exprs(4))

    new MethodDeclaration()
      .setName(fname)
      .setParameters(params)
      .setType(rettype)
      .setBody(body)
  }

  /**
    * Create a grammar from the synth-func command
    * @param synth_func - the s-expr that has the grammar
    */
  def mk_synth_fun(synth_func: SExpr) : (MethodDeclaration, SygusGrammar) = {
    assert(synth_func.exprs.head.asInstanceOf[Id].v == "synth-fun")
    val fname = synth_func.exprs(1).asInstanceOf[Id].v
    val params = this.get_func_params(synth_func.exprs(2))
    val rettype = SygusUtils.get_class(synth_func.exprs(3))
    val grammar = this.get_synth_grammar(synth_func.exprs(4))

    this.synth_hole = JavaUtils.gen_expr_hole_without_test(Some(rettype))
    val body = {
      val ret_stmt = new ReturnStmt(this.synth_hole)
      val stmt_list = new NodeList[Statement]
      stmt_list.add(ret_stmt)
      new BlockStmt(stmt_list)
    }
    val method = new MethodDeclaration()
      .setName(fname)
      .setParameters(params)
      .setType(rettype)
      .setBody(body)

    (method, grammar)
  }

  /**
    * Convert the constraint into a piece of beanshell code
    *
    * We only consider test cases
    */
  def mk_tests(cons : SExpr) : String = {
    assert(cons.exprs.head.asInstanceOf[Id].v == "constraint")
    val cons_expr = cons.exprs(1).asInstanceOf[SExpr]

    // we only consider test cases
    assert(cons_expr.exprs.head.asInstanceOf[Id].v == "=")

    val left = SygusUtils.to_java_expr(cons_expr.exprs(1))
    val right = SygusUtils.to_java_expr(cons_expr.exprs(2))

    left.toString + " == " + right.toString
  }

  /**
    * Start synthesizing the program
    */
  def synthesize() : Unit = {
    // construct the test comment
    val test_comment : String = {
      val builder = new StringBuilder
      builder.append("\nCOMMENT:\ncomment here\n\nTEST:\n")
      builder.append(s"boolean _result_ = false;\n")
      // put all the defined function
      this.defined_funcs.foreach(df => {
        builder.append(df.toString + "\n")
      })

      // put __pliny_solution__
      builder.append("__pliny_solution__\n")

      // put all the tests
      val test_result_builder = new StringBuilder
      this.constraints.zipWithIndex.foreach(cons => {
        builder.append(cons._1 + "\n")
        if(cons._2 > 0) {
          test_result_builder.append(" && ")
        }
        test_result_builder.append(s"_case_${cons._2}_")
      })
      builder.append(s"int ${TestCaseEvaluator.TEST_NUM_TOKEN} = ${this.constraints.size};\n")
      builder.append(s"_result_ = ${test_result_builder.toString()};\n")

      builder.toString()
    }

    // construct the draft
    if(!this.synth_fun.getComment.isPresent) {
      this.synth_fun.setJavadocComment(test_comment)
    }
    val draft = this.cu_template.create_new_draft(this.synth_fun)

    // construct a MCTS task
    val tree = new SygusTree(this.synth_hole, this.grammar, 1, SygusTree.MAX_SIZE, None, new JavaFastInterpreter(Seq()), draft)
    val task = new BanditSearch(new TestCaseEvaluator(TestCaseEvaluator.MAX_SIM))

    // synthesize
    val (runtime, solution) = Utils.time(task.complete(tree, 200))
    this.logger.info("Runtime: {} ms", runtime)
    // visualize
    TreeVisualizer.visualize(tree)
    solution match {
      case Solution(root, leaf) =>
        // val view = new TreeView(new_tree)
        // view.showTree("foo")
        // println("Solution ==============")
        // println(leaf)
        this.solution = {
          val stree = leaf.asInstanceOf[SygusTree]
          stree.draft.replace(stree.draft.get_expr_hole.get, new JavaDraftNode(stree.expr)).asInstanceOf[JavaDraft].method
        }
        this.synth_result = runtime
      case NoSolution() => // println("No Solution.")
    }
  }

  def eval(expr : SExpr) : Unit = {
    expr.exprs.head match {
      case Id("set-logic") => this.logger.warn("Ignoring set-logic command")
      case Id("declare-var") => this.logger.warn("Ignoring declare-var command")
      case Id("define-fun") => this.defined_funcs += this.define_func(expr)
      case Id("synth-fun") =>
        val result = this.mk_synth_fun(expr)
        this.synth_fun = result._1
        this.grammar = result._2
      case Id("constraint") =>
        val test = this.mk_tests(expr)
        val code = s"boolean _case_${this.constraints.size}_ = $test;"
        this.constraints += code
      case Id("check-synth") =>
        // assert we have everything
        assert(this.grammar != null)
        assert(this.synth_fun != null)
        assert(this.synth_hole != null)
        assert(this.constraints != null)
        this.synthesize()
      case _ =>
        this.logger.warn("Unsupported command: {}", expr.exprs.head)
        throw new Exception("Unsupported command: " + expr.exprs.head)
    }
  }
}
