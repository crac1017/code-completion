package edu.rice.pliny.mcts.evaluator

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft.Draft
import edu.rice.pliny.mcts.{DraftTree, MCTSTree}
import scala.util.Random
import scala.collection.mutable

class RelevantExprTestCaseEvaluator(ref_progs : Seq[Draft]) {
  /*
  val logger = Logger(this.getClass)
  var solution : Option[MCTSTree] = None

  /**
    * Randomly complete a draft
    */
  def random_complete(dtree : DraftTree) : Option[MCTSTree] = {
    logger.info(dtree.toString)
    if(dtree.is_complete) {
      // FRandom.forward(dtree.get_parent.get.get_depth)
      Some(dtree)
    } else {
      val moves = dtree.get_valid_moves
      if(moves.isEmpty) {
        None
      } else {
        // this.random_complete(moves(FRandom.next_int(moves.size, dtree.get_depth)))
        this.random_complete(moves(Random.nextInt(moves.size)))
      }
    }
  }

  /**
    * Get the test score
    */
  def get_test_score(tree : DraftTree) : Double = {
    val (draft, interpreter) = tree match {
      case dt : DraftTree =>
        (dt.draft, dt.interpreter)
      case _ => throw new Exception("There is no draft for this tree.")
    }

    val result = draft.get_test.get.accept(interpreter, draft.toString)
    val inter = interpreter.inter

    // get the number of test cases
    val test_num = inter.get(TestCaseEvaluator.TEST_NUM_TOKEN).asInstanceOf[Int]
    var pass_count = 0

    for(i <- 0 until test_num) {
      val case_token = s"_case_${i}_"
      val pass = inter.get(case_token).asInstanceOf[Boolean]
      pass_count += (if(pass) 1 else 0)
    }


    val score = pass_count.toDouble / test_num.toDouble
    this.logger.debug("Score : {} - {}/{}", score, pass_count, test_num)
    if(score >= 1.0) {
      this.solution = Some(tree)
    }
    if(score.isNaN) {
      0.0
    } else {
      score
    }
  }

  override def evaluate(tree: MCTSTree) : Double = {
    // this.FRandom.clear()
    val old_score : mutable.Map[Int, Double] = mutable.Map.empty[Int, Double]
    val scores = (0 until TestCaseEvaluator.MAX_SIM).map(_ => {
      // this.logger.info("Completing program =============\n{}\n", stree.draft)
      val completed_tree = this.random_complete(tree)
      if(completed_tree.isDefined) {
        val hash = completed_tree.hashCode()
        if(old_score.contains(hash)) {
          (completed_tree, old_score(hash))
        } else {
          // this.logger.info("Getting score for completing program =============\n{}\n", completed_tree.get.draft)
          (completed_tree, this.get_test_score(completed_tree.get))
        }
      } else {
        (None, 0.0)
      }
    })
    val result = scores.map(_._2).max
    if(result > 0.0) {
      println("FOO")
    }
    if(result == Double.NaN) {
      this.logger.warn("Maximum test score is 0.0")
      0.0
    } else {
      if(result == 0.0) {
        this.logger.warn("Maximum test score is 0.0")
      }
      result
    }
  }
  */
}
