package edu.rice.pliny.mcts.vis

import java.io.PrintWriter

import OwenDiff.{Diff, Equal}
import com.github.javaparser.ast.expr.{Expression, MethodCallExpr, NameExpr}
import com.github.javaparser.ast.visitor.{ModifierVisitor, Visitable}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.language.java.JavaUtils
import edu.rice.pliny.mcts.splicing.JavaSplicingTree
import edu.rice.pliny.mcts.{DraftTree, MCTSTree}
import edu.rice.pliny.mcts.sygus.SygusTree

import scala.collection.mutable

object TreeVisualizer {
  val logger = Logger(this.getClass)
  var count = 0

  def visualize(tree : MCTSTree) : Unit = {
    val builder = new StringBuilder
    builder.append("digraph MCTS {\n")

    val mapping = mutable.Map.empty[MCTSTree, String]
    val ori_lines = tree.get_node.toString.split("\n").toList

    def get_label(node : MCTSTree) : String = {
      /*
      node.get_node.toString
      */
      /*
      val lines = node.get_node.toString.split("\n").toList
      val diff_list = Diff.diff(ori_lines, lines)
      val b = new StringBuilder
      diff_list.foreach {
        case _: Equal => Unit
        case d => b.append(d.toString + "\n")
      }

      b.toString().replace("\n", "\\n").replace("\"", "\\\"")
      */
      val node_string = node match {
        case st : SygusTree =>
          val modifier = new ModifierVisitor[Any] {
            override def visit(n: MethodCallExpr, arg: Any): Visitable = {
              if(n.getName.asString() == JavaUtils.HOLE_NAME) {
                new NameExpr("??")
              } else {
                if(n.getScope.isPresent) {
                  n.setScope(n.getScope.get().accept(this, null).asInstanceOf[Expression])
                }
                for(i <- 0 until n.getArguments.size()) {
                  n.getArguments.set(i, n.getArguments.get(i).accept(this, null).asInstanceOf[Expression])
                }
                n
              }
            }
          }
          st.expr.clone().accept(modifier, null).toString + "\n"
        case jst : JavaSplicingTree =>
          val modifier = new ModifierVisitor[Any] {
            override def visit(n: MethodCallExpr, arg: Any): Visitable = {
              if(n.getName.asString() == JavaUtils.HOLE_NAME) {
                new NameExpr("??")
              } else {
                if(n.getScope.isPresent) {
                  n.setScope(n.getScope.get().accept(this, null).asInstanceOf[Expression])
                }
                for(i <- 0 until n.getArguments.size()) {
                  n.getArguments.set(i, n.getArguments.get(i).accept(this, null).asInstanceOf[Expression])
                }
                n
              }
            }
          }
          jst.draft.method.clone().removeComment().accept(modifier, null).toString + "\n"
        case _ => node.toString
      }
      node_string.replace("\n", "\\n").replace("\"", "\\\"")
    }

    def get_style(node : MCTSTree, max_size : Double) : String = {
      val result = new StringBuilder
      val size = node.get_reward * 10.0 / max_size
      if(size == 0) {
        result.append("shape = \"point\"")
      } else {
        result.append(String.format("height = \"%s\"", size.toInt.toString))
        result.append(", ")
        result.append(String.format("width = \"%s\"", size.toInt.toString))
        if(node.is_deadend) {
          result.append(", ")
          result.append("style = \"dotted\"")
        }
      }
      result.toString()
    }

    def get_max_value(tree : MCTSTree, curr : Double) : Double = {
      val curr_max = Math.max(curr, tree.get_reward)
      if(tree.get_children.isEmpty) {
        curr_max
      } else {
        tree.get_children.map(n => get_max_value(n, curr_max)).max
      }
    }

    def helper(node : MCTSTree, max_val : Double) : Unit = {
      node.get_children.foreach(c => {
        if(!mapping.contains(node)) {
          val id = "node_" + count
          mapping(node) = id
          count += 1
          builder.append(String.format("  %s [label = \"%s\n%s\", %s];\n", id, get_label(node), node.get_reward.toString, get_style(node, max_val)))
        }
        val a = mapping(node)

        if(!mapping.contains(c)) {
          val id = "node_" + count
          mapping(c) = id
          count += 1
          if(c.get_reward.toInt > 0) {
            builder.append(String.format("  %s [label = \"%s\n%s\", %s];\n", id, get_label(c), c.get_reward.toString, get_style(c, max_val)))
          }
        }
        val b = mapping(c)

        if(c.get_reward.toInt > 0) {
          builder.append(s"  $a -> $b;\n")
          helper(c, max_val)
        }
      })
    }

    helper(tree, get_max_value(tree, 0))
    builder.append("}\n")
    new PrintWriter("mcts.dot") {
      write(builder.toString)
      close()
    }
  }
}
