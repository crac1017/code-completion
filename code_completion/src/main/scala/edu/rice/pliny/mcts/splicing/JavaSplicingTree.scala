package edu.rice.pliny.mcts.splicing

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.body.VariableDeclarator
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.visitor.{ModifierVisitor, Visitable}
import com.github.javaparser.symbolsolver.model.typesystem.Type
import edu.rice.pliny.draft.{Draft, DraftHole, DraftNode}
import edu.rice.pliny.language.java._
import edu.rice.pliny.mcts.DraftTree

import scala.collection.mutable


class JavaSplicingTree(override val draft : JavaDraft,
                       override val parent : Option[DraftTree],
                       override val interpreter : JavaFastInterpreter,
                       sketch_visited : Int = 0,
                       override val children : mutable.ArrayBuffer[DraftTree] = mutable.ArrayBuffer.empty[DraftTree],
                       sketch_reward : Double = 0.0,
                       sketch_prior : Double = 0.0,
                       sketch_depth : Int = 0,
                       sketch_deadend : Boolean = false,
                       sketch_final_reward : Double = 0.0,
                       override val relevant_progs : Seq[Draft] = mutable.ArrayBuffer.empty[Draft])
  extends DraftTree(draft, parent, interpreter, sketch_visited, children, sketch_reward, sketch_prior, sketch_depth, sketch_deadend, sketch_final_reward, relevant_progs) {

  /**
    * Replace all names into holes
    */
  private def abstract_node(node : Node, src_draft : JavaDraft, draft_index : Int) : Visitable = {
    val modifier = new ModifierVisitor[Any] {
      override def visit(n : NameExpr, arg : Any) : Visitable = {
        if(draft.imported_class.contains(n.getNameAsString)) {
          n
        } else {
          if(!abstract_holes.contains(n.getNameAsString)) {
            val t = src_draft.get_type(new JavaDraftNode(n)).asInstanceOf[JavaDraftNodeType].t
            val h = JavaUtils.gen_expr_hole_without_test(src_draft.type_to_class(t))
            abstract_holes.put(n.getNameAsString, h)
          }
          abstract_holes(n.getNameAsString)
        }
      }

      override def visit(n : VariableDeclarator, arg : Any) : Visitable = {
        if(n.getInitializer.isPresent) {
          n.getInitializer.get().accept(this, null).asInstanceOf[Expression]
        }
        n.setName(s"_${n.getName.asString()}_${draft_index}_")
      }
    }
    val result = node.clone().accept[Visitable, Any](modifier, null)
    result
  }

  /**
    * Get valid expression files for this hole. Since this is doing splicing thing, we only want the expressions
    * in the reference programs
    */
  override def get_valid_expr_moves : Seq[JavaSplicingTree] = {
    val cache = mutable.Map.empty[Type, mutable.Set[Expression]]

    // get hole type
    /*
    var hole_type = this.get_type(hole).asInstanceOf[JavaDraftNodeType].t
    if(hole_type.isWildcard) {
      hole_type = hole.asInstanceOf[JavaDraftHole].ttype
    }
    */
    /*
    val hole_call = this.expr_hole.get.asInstanceOf[JavaDraftHole].node.asInstanceOf[MethodCallExpr]
    val t_string = hole_call.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString()
    if(t_string == "[[I") {
      println("FOO")
    }
    */
    var hole_type = this.expr_hole.get.asInstanceOf[JavaDraftHole].ttype

    // if this is a wilecard, then this is a user-provided hole. Then we can extract expressions. Otherwise, we are doing
    // renaming
    val only_want_names = !hole_type.isWildcard
    if(hole_type.isWildcard) {
      hole_type = this.draft.get_type(this.expr_hole.get).asInstanceOf[JavaDraftNodeType].t
    }


    this.logger.debug("Hole type : {}", hole_type.describe())

    def add_to_cache(e : Expression, t : Type) : Unit = {
      if(!cache.contains(t)) {
        cache.put(t, mutable.Set.empty[Expression])
      }
      cache(t) += e
    }

    /**
      * This function extracts expressions from a draft
      */
    def extract_expr(draft : JavaDraft, draft_index : Int) : Unit = {
      val iter = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(p => draft.is_expr(p)))
      while(iter.hasNext) {
        val node = iter.next()
        if(this.draft.same_role(this.expr_hole.get, node)) {
          val node_type = draft.get_type(node).asInstanceOf[JavaDraftNodeType].t
          node.asInstanceOf[JavaDraftNode].node match {
            case name : NameExpr =>
              if(this.draft.type_env.contains(name)) {
                add_to_cache(name, node_type)
              }
            case _ =>
              add_to_cache(this.abstract_node(node.asInstanceOf[JavaDraftNode].node.asInstanceOf[Expression], draft, draft_index).asInstanceOf[Expression], node_type)
          }
        }
      }
    }

    val hole_id = this.expr_hole.get.asInstanceOf[JavaDraftHole].node.asInstanceOf[MethodCallExpr].getArguments.get(0).asInstanceOf[IntegerLiteralExpr].asInt()
    // extract expressions from relevant programs
    if(!only_want_names) {
      relevant_progs.zipWithIndex.foreach(rp => extract_expr(rp._1.asInstanceOf[JavaDraft], rp._2))
    } else {
      for(vname <- this.draft.defset.diff(this.used_refs)) {
        val name_node = new NameExpr(vname)
        val t = this.draft.get_type(new JavaDraftNode(name_node)).asInstanceOf[JavaDraftNodeType].t
        add_to_cache(name_node, t)
      }
    }

    {
      if(hole_type.isWildcard) {
        val nodes_result = cache.values.foldLeft(Seq.empty[Node])((acc, s) => acc ++ s.toSeq)
          .map(n => new JavaDraftNode(n))
        nodes_result.foreach(n => this.logger.debug("Valid Expr Fills {}", n.node))
        nodes_result
      } else {
        val nodes_result = cache.foldLeft(Seq.empty[Node])((acc, p) => {
          if(p._1.isWildcard || this.draft.is_subtype(p._1, hole_type)) {
            acc ++ p._2.toSeq
          } else {
            acc
          }
        }).map(n => new JavaDraftNode(n))
        nodes_result.foreach(n => this.logger.debug("Valid Expr Fills {}", n.node))
        nodes_result
      }
    }.map(e => {
      this.logger.debug("Replacing {} with {}", this.expr_hole.get.toString, e.toString)
      val new_node = this.draft.replace(this.expr_hole.get, e).asInstanceOf[JavaDraft]
      this.logger.debug("New Sketch:\n" + new_node.toString)
      if (!used_moves.contains(new_node)) {
        used_moves += new_node
      }
      (e, new_node)
    }).map(m => {
      if(only_want_names) {
        // we need to record the renaming
        val ref = m._1.node.asInstanceOf[NameExpr].getNameAsString
        val new_tree = new JavaSplicingTree(m._2, Some(this), this.interpreter, relevant_progs = this.relevant_progs)
        new_tree.used_refs += ref
        new_tree
      } else {
        new JavaSplicingTree(m._2, Some(this), this.interpreter, relevant_progs = this.relevant_progs)
      }
    })
  }

  override def get_valid_stmt_moves : Seq[JavaSplicingTree] = {
    val result = mutable.ArrayBuffer.empty[JavaDraft]
    val stmts_result = mutable.ArrayBuffer.empty[Seq[Node]]
    this.logger.debug("front parent {}", this.stmt_hole.get.get_parent.get.asInstanceOf[JavaDraftNode].node.getClass)

    // add an empty sequence to stop the statement sequence
    stmts_result += Seq()

    relevant_progs.zipWithIndex.foreach(rp => {
      val iter = JavaDraftIteratorFactory.create_stmt_sliding_window(rp._1, None)
      // we are generating statement holes
      while(iter.hasNext) {
        val window = iter.next()
        logger.debug("Using window :\n" + iter.print_window(window))
        stmts_result += window._1.map(stmt => abstract_node(stmt.asInstanceOf[JavaDraftNode].node, rp._1.asInstanceOf[JavaDraft], rp._2).asInstanceOf[Statement])
      }

    })
    stmts_result.foreach(s => this.logger.debug("Candidate stmts =========\n {}", if(s.isEmpty) "Empty" else s.foldLeft("")((acc, node) => acc + "\n" + node.toString())))
    stmts_result
      .map(s => s.map(new JavaDraftNode(_)))
      .map(stmts => {
        val new_node = this.draft.replace(this.stmt_hole.get, this.stmt_hole.get, stmts).asInstanceOf[JavaDraft]
        this.logger.debug("New draft:\n" + new_node.toString)
        if(!used_moves.contains(new_node)) {
          used_moves += new_node
        }
        new_node
      })
    .map(m => new JavaSplicingTree(m, Some(this), this.interpreter, relevant_progs = this.relevant_progs))
  }


  /**
    * Return a sequence of valid next moves
    */

  override def get_valid_moves : Seq[JavaSplicingTree] = {
    // next expression moves
    if(this.stmt_hole.isDefined) { return this.get_valid_stmt_moves }
    // next statement moves
    if(this.expr_hole.isDefined) { return this.get_valid_expr_moves }
    Seq()
  }

  /**
    * Check whether this node is a solution to our problem
    */
  override def is_solution: Boolean = {
    if(!this.is_complete) { return false }
    val test = this.draft.get_test.get
    val result = test.accept(this.interpreter, this.draft.toString)
    logger.info("Result: {}", result._1)
    logger.info("Runtime: {}", result._2)
    logger.info("Output =================\n{}\n", result._3)
    result._1
  }
}
