package edu.rice.pliny

import java.io._

import edu.rice.pliny.draft.{Draft, DraftHole}
import edu.rice.pliny.language.java.{JavaDraft, JavaUtils}

import scala.collection.mutable

object Utils {
  case class CompletionSolution(draft : Draft, runtime : Long, line_num : Int)

  object ActorMsg {
    case class StartHoleTask(draft : Draft)
    case class HoleTaskSolution(s : CompletionSolution)
    case class StartStmtFilling(draft : Draft, hole : DraftHole)
    case class StartExprFilling(draft : Draft)
    case class StmtFillingSolution(completionSolution: CompletionSolution)
    case class ExprFillingSolution(completionSolution: CompletionSolution)
    case object StopHoleTask
    case object RequestAllSolutions
    case class ResponseAllSolutions(solutions : mutable.ArrayBuffer[CompletionSolution])
  }

  def read_file(filename : String) : String = scala.io.Source.fromFile(filename).mkString

  def write_file(file : File, content : String) : Unit = {
    val bw = new BufferedWriter(new FileWriter(file, false))
    bw.write(content)
    bw.close()
  }

  /**
    * Return runtime in milliseconds
    */
  def time[R](block: => R): (Long, R) = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    // println("Elapsed time: " + (t1 - t0) + "ns")
    ((t1 - t0) / 1000000, result)
  }

  def post_process(d : Draft) : Draft = d match {
    case jd : JavaDraft => JavaUtils.post_process(jd)
    case _ => d
  }

  /**
    * List all the file paths under the given directory
    */
  def tree(dir : File) : Array[File] = {
    val curr = dir.listFiles()
    curr ++ curr.filter(_.isDirectory).flatMap(tree)
  }

  /**
    * Concat an array of strings
    */
  def string_join(arr : Array[String], sep : String = "") : String = {
    val builder = new java.lang.StringBuilder
    arr.zipWithIndex.foreach(p => {
      if(p._2 > 0) {
        builder.append(sep)
      }
      builder.append(p._1)
    })
    builder.toString
  }


  /*
  * Object in scala for calculating cosine similarity
  * Reuben Sutton - 2012
  * More information: http://en.wikipedia.org/wiki/Cosine_similarity
  */
  object CosineSimilarity {

    /*
     * This method takes 2 equal length arrays of integers
     * It returns a double representing similarity of the 2 arrays
     * 0.9925 would be 99.25% similar
     * (x dot y)/||X|| ||Y||
     */
    def cosineSimilarity(x: Array[Double], y: Array[Double]): Double = {
      require(x.size == y.size)
      dotProduct(x, y)/(magnitude(x) * magnitude(y))
    }

    /*
     * Return the dot product of the 2 arrays
     * e.g. (a[0]*b[0])+(a[1]*a[2])
     */
    def dotProduct(x: Array[Double], y: Array[Double]): Double = {
      (for((a, b) <- x zip y) yield a * b) sum
    }

    /*
     * Return the magnitude of an array
     * We multiply each element, sum it, then square root the result.
     */
    def magnitude(x: Array[Double]): Double = {
      math.sqrt(x map(i => i*i) sum)
    }

  }
}
