package edu.rice.pliny.draft

trait IteratorFactory {

  /**
    * This is used for filling in statement holes
    */
  def create_stmt_sliding_window(draft: Draft, pred: Option[DraftNode => Boolean]) : DraftStmtWindowIterator

  /**
    * Given a draft in any language, create its draft node iterator
    */
  def create_draft_iterator(draft : Draft, pred : Option[DraftNode => Boolean] = None) : DraftNodeIterator
  def create_draftnode_iterator(node : DraftNode, pred : Option[DraftNode => Boolean] = None) : DraftNodeIterator

  /**
    * Given a draft in any language, create its reference iterator
    */
  def create_draftnode_ref_iterator(draft : DraftNode, pred : Option[DraftNode => Boolean] = None) : DraftRefIterator
  def create_draft_ref_iterator(draft : Draft, pred : Option[DraftNode => Boolean] = None) : DraftRefIterator
}
