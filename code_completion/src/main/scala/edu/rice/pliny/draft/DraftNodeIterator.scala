package edu.rice.pliny.draft

/**
  * This represents a program in any language
  */
abstract class DraftNodeIterator(draftnode : DraftNode) extends Iterator[DraftNode] {

  /**
    * Get the current node
    */
  def get_curr_node: Option[DraftNode]

  /**
    * Get all the nodes
    */
  def get_all_nodes : Array[DraftNode]

  override def toString : String = {
    val builder = new StringBuilder()
    builder.append("Draft Node Iterator for draft node:")
    builder.append(draftnode.toString)
    builder.append("Current node:")
    builder.append(this.get_curr_node.toString)
    builder.result()
  }
}
