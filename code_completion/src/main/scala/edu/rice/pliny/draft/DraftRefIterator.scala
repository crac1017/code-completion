package edu.rice.pliny.draft

/**
  * This is an iterator for a draft's references
  */
abstract class DraftRefIterator(draftnode : DraftNode) extends Iterator[DraftNode] {

  /**
    * Return the first draft node in the order to depth-first search
    */
  def get_curr_node: DraftNode

  /**
    * Get all the nodes
    */
  def get_all_nodes : Array[DraftNode]

  override def toString : String = {
    val builder = new StringBuilder()
    builder.append("Draft Node Iterator for draft:")
    builder.append(draftnode.toString)
    builder.append("Current node:")
    builder.append(get_curr_node.toString)
    builder.result()
  }
}
