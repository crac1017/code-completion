package edu.rice.pliny.draft

/**
  * This is the interpreter for java snippets. It is used to check whether a snippet is correct or not
  */
abstract class DraftInterpreter {

  /**
    * This is used to test whether the given code snippet is correct under the given input test
    */
  def test(test_case : TestCase, code : String) : (Boolean, Long, String)

  /**
    * This is used to test whether the given code snippet is correct under the given input test
    */
  def test(test_case : TestCase, code : Draft) : (Boolean, Long, String)
}
