package edu.rice.pliny.draft

/**
  * Test case class for drafts
  */
abstract class TestCase {
  def accept(interp : DraftInterpreter, code : String) : (Boolean, Long, String) = interp.test(this, code)
}
