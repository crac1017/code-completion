package edu.rice.pliny.draft

import scala.collection.mutable

/**
  * This class is used as a sliding window on the program statements to get a set of statements
  */
abstract class DraftStmtWindowIterator(draft : Draft) extends Iterator[(Seq[DraftNode], DraftNode)] {

  type Window = (Seq[DraftNode], DraftNode)

  /**
    * Get the current list of statements along with their parent
    */
  def get_curr_node: Window

  /**
    * Get all the statement lists and their parents
    */
  def get_all_nodes : mutable.ArrayBuffer[Window]


  override def toString : String = {
    val builder = new StringBuilder()
    builder.append("Draft Statement Window Iterator for draft node:")
    builder.append(draft.toString)
    builder.append("Current node:")
    builder.append(this.get_curr_node.toString)
    builder.result()
  }

  def print_window(window: Window) : String

}

