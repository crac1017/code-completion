package edu.rice.pliny.draft

/**
  * This represents a program in any language
  */
abstract class Draft {

  /**
    * This returns the function-level test case of the draft
    */
  def get_test : Option[TestCase]

  /**
    * This returns the function-level comment for querying
    */
  def get_comment : Option[String]

  /**
    * This function is used to rename the variables to avoid name collision. It takes a string transformation function
    * and applies it to every variable name in the draft
    */
  def rename(f : String => String, cond : Option[DraftNode => Boolean]) : Draft

  /**
    * This is used to rename a single variable
    */
  def rename(from : String, to : String) : Draft

  /**
    * This is used to rename an arbitrary node
    */
  def rename(node : DraftNode, from : String, to : String) : DraftNode

  /**
    * This is used to rename a list of arbitrary nodes
    */
  def rename(node : Seq[DraftNode], from : String, to : String) : Seq[DraftNode] = node.map(n => this.rename(n, from ,to))

  /**
    * Get a deep copy of the current draft
    */
  def get_copy : Draft

  /**
    * Test whether this is an expression node
    */
  def is_expr(n: DraftNode) : Boolean
  def is_stmt(n: DraftNode) : Boolean

  /**
    * Test whether this node is a variable
    */
  def is_var(n : DraftNode) : Boolean

  /**
    * Check whether this hole is an expression hole or not
    */
  def is_expr_hole(node: DraftHole): Boolean

  /**
    * Check whether these two nodes serve the same role in the surrounding context
    */
  def same_role(hole: DraftNode, n: DraftNode) : Boolean

  /**
    * Infer the type of a node
    */
  def get_type(node: DraftNode) : DraftNodeType

  override def toString : String
  def toStringNoComment : String

  /**
    * This function is used to rename the reference inside a stand-alone window
    */
  def replace(block : Seq[DraftNode], src : DraftNode, target : DraftNode) : Seq[DraftNode]

  def replace(src : DraftNode, target : DraftNode) : Draft
  def replace(src : DraftNode, target_parent : DraftNode, target : Seq[DraftNode]) : Draft

  /**
    * Return the first node in the depth-first order from left to right. Return None if there's no hole.
    * We assume that the first argument of the hole is the comment, the second is the test's input and the third is
    * the expected output or code snippet testing the correctness
    */
  def get_hole : Option[DraftHole]
  def get_stmt_hole_with_tests : Option[DraftHole]
  def get_stmt_hole_without_tests : Option[DraftHole]
  def get_expr_hole : Option[DraftHole]
  def get_all_expr_hole : Seq[DraftHole]
  def get_all_stmt_hole_without_tests : Seq[DraftHole]
  def has_hole : Boolean =
    this.get_expr_hole.isDefined ||
    this.get_stmt_hole_without_tests.isDefined ||
    this.get_stmt_hole_with_tests.isDefined

  /**
    * Return the first undefined reference
    */
  def get_unref : Option[DraftNode]

  /**
    * Check if this reference is defined within the draft
    */
  def is_defined(ref : DraftNode) : Boolean


  /**
    * The size of the draft. Usually its the number of nodes in the AST
    */
  def size : Int

  /**
    * Check whether the renaming is valid or not.
    * @param completed_draft - the completed draft
    * @param unref - the reference that needs to be replaced
    * @param relevant_prog - the completed draft where new references come from
    * @param r - the new reference
    * @return true if this renaming is valid
    */
  def valid_rename(completed_draft : Draft, unref : DraftNode,
                   relevant_prog : Draft, r : DraftNode) : Boolean


  /**
    * Check whether the expression filling is valid
    * @param working_draft - the draft that receives the filling
    * @param hole - the hole to be filled
    * @param relevant_prog - the draft where the new node is from
    * @param n - the new node
    * @return true if this filling is valid
    */
  def valid_expr_fill(working_draft : Draft, hole : DraftNode,
                      relevant_prog : Draft, n : DraftNode) : Boolean

  /**
    * Return a sequence of valid expressions that could be filled into this
    * expression hole
    */
  def get_valid_expr_fills(hole : DraftHole, relevant_progs : Seq[Draft]) : Seq[DraftNode]

  /**
    * Return a sequence of valid statements that could be filled into this
    * statement hole
    */
  def get_valid_stmt_fills(hole : DraftHole, relevant_progs : Seq[Draft]) : Seq[Seq[DraftNode]]

  /**
    * Get the number of total statements and expressions and nodes
    */
  def get_stmt_count : Int
  def get_expr_count : Int
  def get_node_count : Int
}
