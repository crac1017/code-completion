package edu.rice.pliny.draft

/**
  * This is the hole in a draft
  */
trait DraftHole extends DraftNode {
  def get_comments : Option[String]
  def get_test : Option[TestCase]
}
