package edu.rice.pliny.draft

/**
  * This represents a program in any language. The constructor accepts a set of test cases used to check whether
  * this block of code is correct by running it
  */
abstract class DraftNode {

  // get the parent node
  def get_parent : Option[DraftNode]

  def get_copy : DraftNode

  def get_size : Int

  def get_children : Seq[DraftNode]

  override def toString : String
}
