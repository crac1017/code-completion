package edu.rice.pliny.splicing

import akka.actor.{Actor, ActorLogging}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.{AppConfig, CodeDB, Utils}
import edu.rice.pliny.draft._
import edu.rice.pliny.Utils.{ActorMsg, CompletionSolution}

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class StmtHoleTask(interp : DraftInterpreter,
                   code_db : CodeDB,
                   iterfac : IteratorFactory,
                   solution_limit : Option[Int] = None) extends Actor {
  val logger = Logger(this.getClass)

  var current_task : Option[Future[Unit]] = None
  private var terminated : Boolean = false
  def terminate() : Unit = this.synchronized { this.terminated = true }
  def is_terminated : Boolean = this.synchronized { this.terminated }

  /**
    * Complete a draft program
    */
  def complete(draft : Draft, hole : DraftHole) : Unit = {
    logger.info("Completing Draft ===========\n" + draft.toString)


    // get some relevant programs
    val db_prog = this.code_db.query(draft, hole.get_comments.get)
    for(p <- db_prog.zipWithIndex) {
      if(p._2 < AppConfig.CodeCompletionConfig.db_prog_num) {
        val relevant_prog = p._1._1.rename(s => s"_${p._2}_${s}_", Some(n => p._1._1.is_var(n)))
        logger.info("Using Relevant Program ==========\n" + relevant_prog.toString)
        Future{complete(draft, hole, relevant_prog)}
      }
    }
  }

  private def complete(my_draft : Draft, my_hole : DraftHole, relevant_prog : Draft) : Unit = {
    logger.debug("Working on statement hole with parent =======\n " + my_hole.get_parent.toString)
    val working_draft = my_draft.get_copy
    val sliding_window = this.iterfac.create_stmt_sliding_window(relevant_prog, None)
    while(sliding_window.hasNext) {
      val window = sliding_window.next()
      logger.debug("Using window :\n" + sliding_window.print_window(window))

      // if((window._1.size == 13)) { //} && window._1.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[ForeachStmt])) {
        val new_draft = working_draft.replace(my_hole, window._2, window._1)
        rename(new_draft, window._1, relevant_prog, my_hole)
        if(this.is_terminated) return
        }
      // }
  }

  def valid_rename(completed_draft: Draft, unref: DraftNode, relevant_prog : Draft, r: DraftNode, used_ref: mutable.Set[String]): Boolean = {
    if(used_ref.contains(r.toString.trim)) { return false }
    completed_draft.valid_rename(completed_draft, unref, relevant_prog, r)
  }

  /**
    * Rename all the undefined references in the completed fraft
    */
  def rename(completed_draft : Draft, window : Seq[DraftNode], relevant_prog : Draft, my_hole : DraftHole) : Unit = {

    val used_ref = mutable.Set.empty[String]

    def rename_helper(working_draft : Draft, working_window : Seq[DraftNode]) : Unit = {
      logger.info("Renaming Program ==============\n" + working_draft)
      if(this.is_terminated) return

      val unref = working_draft.get_unref
      if(unref.isEmpty) {
        // no undefined references. Ready to run the program
        logger.info("No more undefined reference.")

        val test_result = this.test_prog(my_hole, working_window)

        if(test_result._1) {
          logger.info("Solution found ===========\n" + working_draft)
          val solution = Utils.post_process(working_draft)
          this.context.parent !
            ActorMsg.StmtFillingSolution(CompletionSolution(solution, test_result._2, solution.toString.split("\n").length))
          this.terminate()
        } else {
          logger.info("Solution incorrect ==========\n" + test_result._3)
        }
        return
      }

      logger.debug(s"Renaming reference: ${unref.get.toString}")
      val defined_iter = this.iterfac.create_draft_ref_iterator(working_draft, Some(n => working_draft.is_defined(n)))
      val tested = mutable.Set.empty[String]
      while(defined_iter.hasNext) {
        val r = defined_iter.next()
        logger.debug(s"Testing reference: ${r.toString}")
        if(valid_rename(working_draft, unref.get, relevant_prog, r, used_ref) && !tested.contains(r.toString.trim) && unref.get.toString != r.toString) {
          used_ref += r.toString.trim
          tested += r.toString.trim
          logger.debug(s"Using reference: ${r.toString}")
          val new_draft = working_draft.replace(unref.get, r)
          val new_window = working_draft.rename(working_window, unref.get.toString, r.toString)
          rename_helper(new_draft, new_window)
          used_ref -= r.toString.trim
          logger.debug(s"Backing on reference: ${unref.toString}")
        }
      }
    }
    rename_helper(completed_draft, window)
  }



  /**
    * Run the program and test whether the program is correct.
    */
  def test_prog(my_hole : DraftHole, candidate : Seq[DraftNode]) : (Boolean, Long, String) = {
    val candidate_str = candidate.foldLeft("")((s, node) => s + "\n" + node.toString) + "\n"
    logger.info("Testing =============\n" + candidate_str)
    val java_test = my_hole.get_test.get
    val result = this.interp.test(java_test, candidate_str.replace("\\n", "\n"))
    logger.info("Test Result: " + result._1 + "\t" + result._2 + " ms")
    result
  }

  override def receive: Receive = {
    case ActorMsg.StartStmtFilling(draft, hole) =>
      this.current_task = Some(Future{this.complete(draft, hole)})
      this.current_task.get.onComplete {
        case Success(posts) =>
          logger.info("StmtHoleTask is finished.")
        case Failure(t) =>
          logger.error("StmtHoleTask has failed:")
          logger.error("  " + t.getMessage)
          logger.error(t.getStackTrace.foldLeft("")((total, curr) => total + "  " + curr.toString + "\n"))
      }
      this.current_task = None

    case ActorMsg.StopHoleTask =>
      logger.info("Terminating statement hole filling task.")
      this.terminate()
  }
}
