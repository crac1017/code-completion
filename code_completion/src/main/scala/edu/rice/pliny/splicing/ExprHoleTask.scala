package edu.rice.pliny.splicing

import akka.actor.Actor
import com.github.javaparser.ast.stmt.ForStmt
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.{AppConfig, CodeDB, Utils}
import edu.rice.pliny.Utils.{ActorMsg, CompletionSolution}
import edu.rice.pliny.draft._
import edu.rice.pliny.language.java.JavaDraftNode

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
  * This task is used to fill all the left-over expression holes. The given drat has to have no holes.
  */
class ExprHoleTask(interp : DraftInterpreter,
                   code_db : CodeDB,
                   iterfac : IteratorFactory,
                   solution_limit : Option[Int] = None) extends Actor {
  val logger = Logger(this.getClass)

  var current_task : Option[Future[Unit]] = None

  logger.debug("My parent actor is: " + this.context.parent.toString())

  private var terminated : Boolean = false
  def terminate() : Unit = this.synchronized { this.terminated = true }
  def is_terminated : Boolean = this.synchronized { this.terminated }

  /**
    * Complete all the expression holes in the draft
    */
  def complete(my_draft : Draft) : Unit = {
    logger.info("Completing Draft ===========\n" + my_draft.toString)

    if(my_draft.get_expr_hole.isEmpty && my_draft.get_stmt_hole_without_tests.isEmpty) {
      logger.info("No holes. We are done.")
      this.context.parent !
        ActorMsg.ExprFillingSolution(CompletionSolution(my_draft, 0L, my_draft.toString.split("\n").length))
    }
    val db_prog = this.code_db.query(my_draft, my_draft.get_comment.get)

    // get some relevant programs
    for(p <- db_prog.zipWithIndex) {
      if(p._2 < AppConfig.CodeCompletionConfig.db_prog_num) {
        val relevant_prog = p._1._1.rename(s => s"_${p._2}_${s}_", Some(n => p._1._1.is_var(n)))
        logger.info("Using Relevant Program ==========\n" + relevant_prog.toString)
        Future{complete(my_draft, relevant_prog)}
      }
    }
  }

  /**
    * Gather all expressions from a given set of drafts
    */
  private def gather_expr(good_prog : Draft) : mutable.ArrayBuffer[DraftNode] = {
    val result = mutable.ArrayBuffer.empty[DraftNode]
    val iter = this.iterfac.create_draft_iterator(good_prog, Some(p => good_prog.is_expr(p)))
    while(iter.hasNext) {
      val node = iter.next()
      result += node
    }
    result
  }

  /**
    * Complete the given expression hole given some good programs
    */
  def complete(my_draft : Draft, db_prog : Draft) : Unit = {
    logger.info("Completing Draft ===========\n" + my_draft.toString)
    // get all expressions
    val used_ref = mutable.Set.empty[String]
    val used_exprs = mutable.Set.empty[String]
    val used_nodes = mutable.Set.empty[String]

    def replace_stmt_hole(working_draft : Draft, hole : DraftHole) : Unit = {
      logger.debug("Working on statement hole {} with parent =======\n " + hole.get_parent.toString, hole)
      val sliding_window = this.iterfac.create_stmt_sliding_window(db_prog, None)
      while(sliding_window.hasNext) {
        val window = sliding_window.next()
        val candidate_nodes = mutable.Set.empty[String]
        window._1.foreach(n => candidate_nodes += n.toString)
        if(candidate_nodes.intersect(used_nodes).isEmpty) {
          logger.debug("Using window :\n" + sliding_window.print_window(window))

          // if((window._1.size == 1) && window._1.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[ForStmt]) { //} && window._1.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[ForeachStmt])) {
          used_nodes ++= candidate_nodes
          // if(window._1.head.asInstanceOf[JavaDraftNode].node.isInstanceOf[ForStmt]) {
          val new_draft = working_draft.replace(hole, window._2, window._1)

          val new_stmt_hole = new_draft.get_stmt_hole_without_tests
          if(new_stmt_hole.isDefined) {
            replace_stmt_hole(new_draft, new_stmt_hole.get)
          } else {
            rename_helper(new_draft)
          }
          used_nodes --= candidate_nodes
        // }

        logger.debug("Backing on statement hole " + hole.toString + " with parent ======\n" + hole.get_parent.toString)
        if(this.is_terminated) return
        }
      }
      logger.debug("No more windows.")
    }


    def replace_expr_hole(working_draft : Draft, expr_hole : DraftHole) : Unit = {
      if(this.is_terminated) return
      logger.info("Filling Draft Using Expressions ==============\n" + working_draft)
      /*
      // used expressions and references
      val expr_hole = working_draft.get_expr_hole
      if(expr_hole.isEmpty && working_draft.get_test.isDefined) {
        logger.info("No more expression holes.")
        // test the completed draft

        /*
        val test_result = this.test_prog(working_draft)
        if(test_result._1) {
          logger.info("Solution found ==============\n" + working_draft)
          this.solutions += CompletionSolution(working_draft, test_result._2, working_draft.toString.split("\n").length)
        }
        */
        this.solutions += CompletionSolution(working_draft, test_result._2, working_draft.toString.split("\n").length)
        return
      }
      */

      logger.debug("Working on expression hole " + expr_hole.toString + " with parent ======\n" + expr_hole.get_parent.toString)

      val all_expr_nodes = this.gather_expr(db_prog)

      for(expr <- all_expr_nodes) {
        logger.debug("Testing expression " + expr.toString + " with parent =======\n" + expr.get_parent.toString)
        if(valid_expr_fill(working_draft, expr_hole, db_prog, expr, used_exprs)) {
          logger.debug("Using expression")
          used_exprs += expr.toString.trim
          val new_draft = working_draft.replace(expr_hole, expr)

          val new_expr_hole = new_draft.get_expr_hole
          if(new_expr_hole.isDefined) {
            replace_expr_hole(new_draft, new_expr_hole.get)
          } else {
            val new_stmt_hole = new_draft.get_stmt_hole_without_tests
            if(new_stmt_hole.isDefined) {
              replace_stmt_hole(new_draft, new_stmt_hole.get)
            } else {
              rename_helper(new_draft)
            }
          }

          // rename_helper(new_draft, expr._2)
          used_exprs -= expr.toString.trim
          logger.debug("Backing on expression hole " + expr_hole.toString + " with parent ======\n" + expr_hole.get_parent.toString)
        }
      }
      logger.debug("No more expressions.")
    }

    def rename_helper(working_draft : Draft) : Unit = {
      if(this.is_terminated) return
      logger.info("Renaming Program ==============\n" + working_draft)
      val unref = working_draft.get_unref
      if(unref.isEmpty) {
        logger.info("No more undefined reference. Back to replacing nodes")
        // keep replacing nodes
        // replace_helper(working_draft)

        val test_result = this.test_prog(working_draft)
        if(test_result._1) {
          logger.info("Solution found ==============\n" + working_draft)
          val solution = Utils.post_process(working_draft)
          this.context.parent !
            ActorMsg.ExprFillingSolution(CompletionSolution(solution, test_result._2, solution.toString.split("\n").length))
        } else {
          logger.info("Solution incorrect ============\n" + test_result._3)
        }
        return
      }

      logger.debug(s"Renaming reference: ${unref.toString}")
      val tested = mutable.Set.empty[String]
      val defined_iter = this.iterfac.create_draft_ref_iterator(working_draft, Some(n => working_draft.is_defined(n)))
      while(defined_iter.hasNext) {
        val r = defined_iter.next()
        logger.debug(s"Testing reference: ${r.toString}")
        if(valid_rename(working_draft, unref.get, db_prog, r, used_ref) && !tested.contains(r.toString.trim)) {
          used_ref += r.toString.trim
          logger.debug(s"Using reference: ${r.toString}")
          val new_draft = working_draft.replace(unref.get, r)
          rename_helper(new_draft)
          used_ref -= r.toString.trim
          logger.debug(s"Backing on reference: ${unref.toString}")
        }
        tested += r.toString.trim
      }
      logger.debug("No more reference.")
    }

    def replace_helper(working_draft : Draft) : Unit = {
      // logger.info("Looking for expression hole.")
      val expr_hole = working_draft.get_expr_hole
      if(expr_hole.isDefined) {
        replace_expr_hole(working_draft, expr_hole.get)
      } else {
        // logger.info("Cannot find any expression hole.")
        // logger.info("Looking for statement holes with no tests.")
        val stmt_hole = working_draft.get_stmt_hole_without_tests
        if(stmt_hole.isDefined) {
          replace_stmt_hole(working_draft, stmt_hole.get)
        } else {
          logger.warn("No holes. We are done.")
          this.context.parent !
            ActorMsg.ExprFillingSolution(CompletionSolution(working_draft, 0L, working_draft.toString.split("\n").length))
        }
      }
    }

    replace_helper(my_draft)
  }

  def valid_expr_fill(working_draft: Draft, hole: DraftNode, relevant_prog : Draft, n: DraftNode, used_node: mutable.Set[String]): Boolean = {
    // check if this node has been used
    if(used_node.contains(n.toString.trim())) { return false }
    logger.debug("We haven't used this node.")

    if(!working_draft.valid_expr_fill(working_draft, hole, relevant_prog, n)) { return false }
    logger.debug("Expression filling is valid")
    true
  }

  def valid_rename(completed_draft: Draft, unref: DraftNode, relevant_prog : Draft, r: DraftNode, used_ref: mutable.Set[String]): Boolean = {
    if(used_ref.contains(r.toString.trim)) { return false }
    completed_draft.valid_rename(completed_draft, unref, relevant_prog, r)
  }

  /**
    * Run the program and test whether the program is correct.
    */
  def test_prog(completed_draft : Draft) : (Boolean, Long, String) = {
    logger.info("Testing =============\n" + completed_draft.toString)
    val test = completed_draft.get_test.get
    test.accept(this.interp, completed_draft.toString)
  }

  override def receive: Receive = {
    case ActorMsg.StartExprFilling(draft) =>
      this.current_task = Some(Future{this.complete(draft)})
      this.current_task.get.onComplete {
        case Success(posts) =>
          logger.info("ExprHoleTask is finished.")
        case Failure(t) =>
          logger.error("ExprHoleTask has failed:")
          logger.error("  " + t.getMessage)
          logger.error(t.getStackTrace.foldLeft("")((total, curr) => total + "  " + curr.toString + "\n"))
      }
      this.current_task = None

    case ActorMsg.StopHoleTask =>
      logger.info("Terminating expression hole filling task.")
      this.terminate()
  }
}
