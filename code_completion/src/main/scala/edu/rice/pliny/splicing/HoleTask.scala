package edu.rice.pliny.splicing

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.typesafe.scalalogging.{Logger, StrictLogging}
import edu.rice.pliny.CodeDB
import edu.rice.pliny.Utils.{ActorMsg, CompletionSolution}
import edu.rice.pliny.draft._

import scala.util.Random

/**
  * This is the main interface for filling holes. It will complete all the statement holes first and then start
  * working on the expression holes.
  */
class HoleTask(interp : DraftInterpreter,
               code_db : CodeDB,
               iterfac : IteratorFactory,
               solution_limit : Option[Int] = None) extends Actor {
  val logger = Logger(this.getClass)

  val stmt_task: ActorRef = this.context.actorOf(Props(new StmtHoleTask(this.interp, this.code_db, this.iterfac, this.solution_limit)),
    name = "stmt_task-" + Random.nextInt().toString)
  val expr_task: ActorRef = this.context.actorOf(Props(new ExprHoleTask(this.interp, this.code_db, this.iterfac, this.solution_limit)),
    name = "expr_task-" + Random.nextInt().toString)
  logger.debug("My parent actor is : " + this.context.parent.toString())

  def complete(draft : Draft) : Unit = {
    logger.info("Looking for holes with tests.")

    def stmt_hole_helper(working_draft : Draft): Unit = {
      val hole = working_draft.get_stmt_hole_with_tests
      if(hole.isDefined && hole.get.get_test.isDefined) {
        logger.info("Found a statement hole with tests.")
        this.stmt_task ! ActorMsg.StartStmtFilling(working_draft, hole.get)

      } else {
        val expr_hole = working_draft.get_expr_hole
        val stmt_hole_notests = working_draft.get_stmt_hole_without_tests
        if(expr_hole.isDefined || stmt_hole_notests.isDefined) {
          expr_hole_helper(working_draft)
        } else {
          logger.info("No more holes. We are done. Sending a solution back.")
          this.context.parent !
            ActorMsg.HoleTaskSolution(CompletionSolution(working_draft, 0L, working_draft.toString.split("\n").length))
        }

      }
    }

    def expr_hole_helper(working_draft : Draft) : Unit = this.expr_task ! ActorMsg.StartExprFilling(working_draft)

    stmt_hole_helper(draft)
  }

  override def receive: Receive = {
    case ActorMsg.StartHoleTask(draft) =>
      logger.debug("Received a StartHoleTask message :")
      logger.debug(draft.toString)
      this.complete(draft)
    case ActorMsg.StmtFillingSolution(s) =>
      logger.debug("Received a StmtFillingSolution message :")
      logger.debug(s.toString)
      val stmt_hole = s.draft.get_stmt_hole_with_tests
      if(stmt_hole.isDefined) {
        logger.debug("Still has stmt holes with tests.")
        this.self ! ActorMsg.StartHoleTask(s.draft)
      } else {
        logger.debug("No more stmt holes with tests.")
        val expr_hole = s.draft.get_expr_hole
        val stmt_hole_notests = s.draft.get_stmt_hole_without_tests
        if(expr_hole.isDefined || stmt_hole_notests.isDefined) {
          logger.debug("Has expression holes or holes with no tests.")
          this.expr_task ! ActorMsg.StartExprFilling(s.draft)
        } else {
          logger.info("No more holes. We are done. Sending solution back.")
          this.context.parent ! ActorMsg.HoleTaskSolution(s)
        }
      }
    case ActorMsg.ExprFillingSolution(s) =>
      logger.debug("Received an ExprFillSolution message :\n" + s.toString)
      this.context.parent ! ActorMsg.HoleTaskSolution(s)
    case ActorMsg.StopHoleTask =>
      logger.info("Stopping all filling tasks.")
      this.context.children.foreach(c => c ! ActorMsg.StopHoleTask)
  }
}
