package edu.rice.pliny.apitrans

import java.io.File

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.apitrans.hint.mapper.JavadocMapper
import edu.rice.pliny.language.java.JavaUtils

object MapAPIMain {
  val logger = Logger(this.getClass)
  val APITRANS_DIR_PREFIX = "code_completion/src/test/resources/apitrans/benchmark"

  def get_filenames(dirname : String) : Seq[String] = {
    new File(dirname).listFiles().filter(_.isFile).map(f => f.getAbsolutePath)
  }

  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    if(args.length < 1) {
      System.err.println(
        """Usage: [benchmark name]

         Map the API in the "from" package into a set of APIs in the "to" package

         filename - a java program contains API calls using library indicated by the "from" package
      """)
      return
    }

    val bench = args(0)

    logger.info("benchmark name: {}", bench)

    // extract a list of fully qualified method names from the input program

    val filename = APITRANS_DIR_PREFIX + s"/$bench/$bench.java"
    val jar_dir = new File(APITRANS_DIR_PREFIX + s"/$bench/jars")
    val jar_paths =
      if(jar_dir.exists()) {
        get_filenames(jar_dir.getAbsolutePath)
      } else {
        Seq.empty[String]
      }
    jar_paths.foreach(jp => this.logger.info("Loading jar {}", jp))
    val prog = JavaUtils.parse_file(filename, None, None, None, jar_paths = jar_paths, dir_paths = Seq()).get
    val block = APITrans.find_refactoring_block(prog).get
    this.logger.info("Refactor block to {} ===============\n{}", block._2, block._1)

    val methods = APITransNL.extract_methods(prog, block._1)

    this.logger.info("Extracted methods :")
    methods.foreach(m => this.logger.info("{} : {}", m._1, m._2))
    if(methods.isEmpty) {
      logger.error("Failed to extract any methods.")
      return
    }

    val mapper = JavadocMapper.load_mapper
    if(mapper.isEmpty) {
      logger.error("Failed to load mapper from disk.")
      return
    }

    methods.foreach(m => {
      val (pname : String, cname : String, mname : String) = APITransNL.split(m._1)
      logger.info("Mapping from {} {} {} : {}", pname, cname, mname, m._2)
      val desc = mapper.get.methods.get_method(pname, cname, mname, m._2)
      if(desc.isDefined) {
        val translated = APITransNL.query(desc.get, mapper.get, block._2)
        logger.info("Translation result ===========\n{} {} {} : {}", pname, cname, mname, m._2)
        if(translated.isEmpty) {
          logger.info("Failed to translate methods.")
        } else {
          logger.info(translated.foldLeft("")((acc, tm) => acc + "\n  " + tm._1.package_name + " " + tm._1.class_name + " " + tm._1.name + " : " + tm._1.t + " - " + tm._2))
        }
      } else {
        logger.warn("Cannot find method.")
      }
    })
    mapper.get.unseen_fasttext_words.foreach(w => this.logger.warn("Unseen Fasttext word: {}", w))
  }
}
