package edu.rice.pliny.apitrans.hint.mapper

import java.io.{File, FileOutputStream, PrintWriter}

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.Utils
import edu.rice.pliny.apitrans.hint.mapper.MethodDesc.FuncType
import org.apache.commons.lang3.ArrayUtils

import scala.collection.mutable
import spray.json._

/**
  * This class creates a translation cache stored on the disk
  */
class MapperCache(val filename : String, val mapper : JavadocMapper) {

  val logger = Logger(this.getClass)
  val data_file = new File(filename)

  // qualified method name -> package name -> sequence of (qualified method name, similarity)
  val data : mutable.Map[String, mutable.Map[String, Seq[(String, Double)]]] =
    if(data_file.exists()) {
      this.load_data()
    } else {
      mutable.Map.empty[String, mutable.Map[String, Seq[(String, Double)]]]
    }


  private def desc2str(desc: MethodDesc) : String = {
    val src_method = desc.package_name + "." + desc.class_name + "." + desc.name
    val stype =
      if(desc.t.params.isEmpty) {
        "() -> " + desc.t.ret
      } else if(desc.t.params.size == 1) {
        desc.t.params.head + " -> " + desc.t.ret
      } else {
        desc.t.params.tail.foldLeft(desc.t.params.head)((acc, t) => acc + " -> " + t) + " -> " + desc.t.ret
      }
    this.logger.debug("desc2str : {} ======================\n{}\n", desc.toString, src_method + " : " + stype)
    src_method + " : " + stype
  }

  private def str2desc(desc_str : String) : MethodDesc = {
    val (method, mtype) = {
      val s = desc_str.split(" : ")
      (s(0), s(1))
    }

    val (pname, cname, mname) = {
      val s = method.split("\\.")
      (Utils.string_join(ArrayUtils.subarray(s, 0, s.length - 2), "."), s(s.length - 2), s(s.length - 1))
    }

    val types = mtype.split(" -> ")
    val param_types =
      if(types.head == "()") {
        Seq.empty[String]
      } else {
        types.slice(0, types.length - 1).toSeq
      }
    val ftype = FuncType(param_types, types.last)

    logger.info("pname : {}\tcname : {}\tmname : {}\tftype: {}", pname, cname, mname, ftype)
    val result = this.mapper.methods.get_method(pname, cname, mname, ftype).get
    this.logger.debug("str2desc : {} ===========================\n{}", desc_str, result.toString)
    result
  }

  /**
    * translate a method
    */
  def query(method : MethodDesc, dest_pkg : String, k : Int = 10) : Seq[(MethodDesc, Double)] = {
    val src = this.desc2str(method)
    this.logger.debug("Query cache with method {} for package {}", src, dest_pkg)

    if(!this.data.contains(src)) {
      this.logger.debug("Adding new method entry for translation cache.")
      // we never translate this method
      this.data.put(src, mutable.Map.empty[String, Seq[(String, Double)]])
    }

    if(!this.data(src).contains(dest_pkg)) {
      this.logger.debug("Querying actually for method.")
      // we don't have the translation for this package yet
      val sim_methods = this.mapper.query(method, dest_pkg, k = k)
      val method_strings = sim_methods.map(sm => (this.desc2str(sm._1), sm._2))
      this.data(src).put(dest_pkg, method_strings)
      save()
      sim_methods
    } else {
      this.logger.debug("Got cache results for method.")
      // just get the results from the cache
      val target_methods = this.data(src)(dest_pkg)
      target_methods.map(tm => (this.str2desc(tm._1), tm._2)).sortBy(_._2)
    }
  }

  def data2json() : JsValue = {
    JsObject(this.data.map(m => m._1 ->
      JsObject(m._2.map(pkg => pkg._1 ->
        JsArray(pkg._2.map(p => JsObject("method" -> JsString(p._1), "dist" -> JsNumber(p._2))).toVector)
      ).toMap)
    ).toMap)
  }

  /**
    * save the real query result
    */
  def save() : Unit = {
    val writer = new PrintWriter(new FileOutputStream(this.data_file.getAbsolutePath, false))
    val json = this.data2json()
    this.logger.info("Saving translation cache ==============\n{}", json.prettyPrint)

    writer.write(json.prettyPrint)
    writer.flush()
    writer.close()
  }

  /**
    * Load data from the disk
    */
  def load_data() : mutable.Map[String, mutable.Map[String, Seq[(String, Double)]]] = {
    this.logger.info("Loading data from the disk...")
    val result = mutable.Map.empty[String, mutable.Map[String, Seq[(String, Double)]]]

    val json = scala.io.Source.fromFile(this.data_file).mkString.parseJson
    json.asJsObject.fields.foreach(m =>  {
      result.put(m._1, mutable.Map.empty[String, Seq[(String, Double)]])
      m._2.asJsObject.fields.foreach(pkg =>
        result(m._1).put(pkg._1,
          pkg._2.asInstanceOf[JsArray].elements.map(o =>
            (o.asJsObject.fields("method").asInstanceOf[JsString].value,
              o.asJsObject.fields("dist").asInstanceOf[JsNumber].value.toDouble))))
    })
    result
  }
}
