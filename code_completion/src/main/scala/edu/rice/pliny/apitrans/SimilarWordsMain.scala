package edu.rice.pliny.apitrans

import java.util.Scanner

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer

import scala.collection.JavaConverters._
import scala.collection.mutable

object SimilarWordsMain {
  val logger = Logger(this.getClass)

  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    if(args.length < 1) {
      System.err.println(
        """Usage: [word2vec model filename]

         Generate 2D word matrix
      """)
      return
    }

    val model_filename = args(0)

    logger.info("model filename: {}", model_filename)

    // extract a list of fully qualified method names from the input program
    val model = WordVectorSerializer.readWord2VecModel(model_filename)

    print("enter a word > ")
    val scanner = new Scanner(System.in)
    while(scanner.hasNext) {
      val query_word = scanner.next()

      // enter words that we want to calculate similarities
      print("Enter target word > ")
      val target_words = mutable.ArrayBuffer.empty[String]
      var input_word = scanner.next()
      while(input_word != ".") {
        if(!input_word.trim.isEmpty) {
          target_words += input_word.trim
          println("Saved |" + input_word.trim + "|")
        }
        print("Enter target word > ")
        input_word = scanner.next()
      }

      if(target_words.isEmpty) {
        println(s"query word: '$query_word'")
        val sim_words = model.wordsNearest(query_word, 20).asScala
        for(sw <- sim_words) {
          val sim = model.similarity(query_word, sw)
          println(s"  $sw : $sim")
        }
        println()
      } else {
        println(s"$query_word similarities: ")
        target_words
          .map(tw => (tw, model.similarity(query_word, tw)))
          .sortBy(-_._2)
          .foreach(tw => println(s"  ${tw._1} : ${tw._2}"))
        println()
      }

      print("enter a word > ")
    }
  }
}
