package edu.rice.pliny.apitrans.hint.mapper
import edu.rice.pliny.apitrans.hint.mapper.MethodDesc.FuncType
import spray.json._

object MethodDescJsonProtocol extends DefaultJsonProtocol {

  implicit object MethodDescJsonFormat extends RootJsonFormat[MethodDesc] {
    override def read(json: JsValue): MethodDesc = {
      json.asJsObject.getFields("t", "desc", "name", "class_desc", "class_name", "package_name", "params") match {
        case Seq(JsObject(t), JsString(desc), JsString(name), JsString(class_desc), JsString(class_name), JsString(package_name), JsObject(param_names)) =>
          new MethodDesc(
            FuncType(t("param").asInstanceOf[JsArray].elements.map(_.asInstanceOf[JsString].value), t("ret").asInstanceOf[JsString].value),
            desc, name, class_desc, class_name, package_name,
            param_names.toSeq.map(p => (p._1, p._2.asInstanceOf[JsString].value)))
      }
    }

    override def write(obj: MethodDesc): JsValue =  {
      JsObject(
        "t" -> JsObject("ret" -> JsString(obj.t.ret),
          "param" -> JsArray(obj.t.params.toVector.map(JsString(_)))),
        "desc" -> JsString(obj.desc),
        "name" -> JsString(obj.name),
        "class_desc" -> JsString(obj.class_desc),
        "class_name" -> JsString(obj.class_name),
        "package_name" -> JsString(obj.package_name),
        "params" -> JsObject(obj.params.map(p => p._1 -> JsString(p._2)).toMap)
      )
    }
  }
}

