package edu.rice.pliny.apitrans.hint

import com.github.javaparser.symbolsolver.model.typesystem.Type
import edu.rice.pliny.apitrans.JarAnalyzer.JarFunc

/**
  * This class provides hints for API sequence synthesis
  */
abstract class HintModel(task : String) {
  def is_relevant_method(func : JarFunc) : Boolean
  def is_relevant_constructor(func : JarFunc) : Boolean
  def is_relevant_type(t : Type) : Boolean
}
