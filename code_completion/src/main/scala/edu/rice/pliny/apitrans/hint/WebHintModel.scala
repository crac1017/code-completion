package edu.rice.pliny.apitrans.hint

import java.util

import com.github.javaparser.symbolsolver.model.typesystem.Type
import edu.rice.pliny.apitrans.JarAnalyzer.{JarConstructor, JarFunc, JarMethod}
import edu.rice.pliny.mcts.{StanfordLemmatizer, Stemmer, TFIDFCalculator}
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

import scala.collection.mutable

/**
  * get some keywords from several stackoverflow pages
  */
class WebHintModel(task : String, web_pages : Seq[String]) extends HintModel(task) {

  val calc = new TFIDFCalculator()
  val object_created = mutable.Set.empty[String]

  def get_tokens(text : String) : util.List[String] = {
    val raw_tokens = text.replaceAll("[^A-Za-z0-9]", " ").split("\\s+")
    val all_tokens : util.List[String] = new util.ArrayList[String]()
    val lemmantizer = new StanfordLemmatizer
    val stemmer = new Stemmer
    raw_tokens.foreach(t => {
      val processed_token = t.replaceAll("[^A-Za-z0-9]", "")
      /*
      val tlist = lemmantizer.detect_sentences(t.toLowerCase.replaceAll("[^A-Za-z0-9]", "")).asScala
      val stemmed_list = new util.ArrayList[String](tlist.map(stemmer.stem).asJava)
      all_tokens.addAll(stemmed_list)
      */
      if(!all_tokens.isEmpty && all_tokens.get(all_tokens.size() - 1) == "new") {
        object_created += processed_token
      }
      all_tokens.add(processed_token)
    })
    // all_tokens.forEach(println(_))
    all_tokens
  }

  /**
    * Get all text under the given tag type
    */
  def get_text_by_tag(doc : Document, tag_type : String) : String = {
    val builder = new StringBuilder
    val tag_list = doc.select(tag_type)
    for(i <- 0 until tag_list.size()) {
      val tag = tag_list.get(i)
      builder.append(" " + tag.text())
    }
    builder.toString()
  }

  // document list
  val doc_list : util.List[util.List[String]] = {
    val result = new util.ArrayList[util.List[String]]()
    web_pages.foreach(p => {
      val html_doc = Jsoup.parse(scala.io.Source.fromFile(p).mkString)
      result.add(get_tokens(get_text_by_tag(html_doc, "code")))
      result.add(get_tokens(get_text_by_tag(html_doc, "pre")))
    })
    result
  }

  def is_mentioned(word : String) : Boolean = this.freq(word) > 0.0

  /**
    * Get term frequency across all the documents
    */
  def freq(word : String) : Double = {
    var result = 0.0
    for(i <- 0 until doc_list.size()) {
      result += this.calc.tf(doc_list.get(i), word)
    }
    result
  }

  override def is_relevant_method(func: JarFunc): Boolean = {
    func match {
      case JarMethod(name, t, clz, method) =>
        val class_name = clz.getName.split("\\.").last
        this.is_mentioned(name) && this.is_mentioned(class_name)
      case JarConstructor(name, t, clz, cons) => false
    }
  }

  override def is_relevant_constructor(func: JarFunc): Boolean = {
    func match {
      case JarMethod(name, t, clz, method) => false
      case JarConstructor(name, t, clz, cons) =>
        val class_name = clz.getName.split("\\.").last
        this.object_created(class_name)
    }
  }

  override def is_relevant_type(t: Type): Boolean = {
    if(t.isPrimitive) {
      true
    } else if(t.isArray) {
      this.is_relevant_type(t.asArrayType().getComponentType)
    } else {
      this.object_created.contains(t.asReferenceType().getId.split("\\.").last) ||
        t.asReferenceType().getId.startsWith("java")
    }
  }
}
