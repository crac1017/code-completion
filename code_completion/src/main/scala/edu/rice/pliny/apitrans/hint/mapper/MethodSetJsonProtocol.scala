package edu.rice.pliny.apitrans.hint.mapper
import spray.json._
import scala.collection.mutable
import MethodDescJsonProtocol._

object MethodSetJsonProtocol extends DefaultJsonProtocol {
  implicit object MethodJsonFormat extends RootJsonFormat[MethodSet] {
    override def read(json: JsValue): MethodSet = {
      val obj = json.asJsObject

      val result = new MethodSet
      obj.fields.foreach(pkg => {
        val pkg_name = pkg._1
        result.data.put(pkg_name, mutable.Map.empty[String, mutable.Set[MethodDesc]])

        val class_set = pkg._2.asJsObject.fields
        class_set.foreach(clz => {
          val clz_name = clz._1
          result.data(pkg_name).put(clz_name, mutable.Set.empty[MethodDesc])

          val method_array = clz._2.asInstanceOf[JsArray]
          result.data(pkg_name)(clz_name) ++= method_array.elements.map(e => e.convertTo[MethodDesc])
          result.size += method_array.elements.size
        })
      })

      result
    }

    override def write(obj: MethodSet): JsValue =
      obj.data.map(pkg => pkg._1 -> pkg._2.map(clz => clz._1 -> clz._2.toArray.toJson).toMap.toJson).toMap.toJson
  }
}

