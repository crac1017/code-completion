package edu.rice.pliny.apitrans

import java.lang.reflect.Modifier

import com.github.javaparser.JavaParser
import com.github.javaparser.ast.body.{MethodDeclaration, VariableDeclarator}
import com.github.javaparser.ast.{Node, NodeList}
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.visitor.VoidVisitorAdapter
import com.github.javaparser.symbolsolver.model.typesystem._
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.Utils
import edu.rice.pliny.apitrans.JarAnalyzer.{JarConstructor, JarFunc, JarMethod}
import edu.rice.pliny.apitrans.APITrans.{Binding, Env, TypeBinding}
import edu.rice.pliny.apitrans.hint.HintModel
import edu.rice.pliny.language.java._

import scala.collection.mutable

class APITransSearch(override val task_name : String,
                     val hint : HintModel,
                     override val interpreter : JavaFastInterpreter,
                     override val jar : JarAnalyzer) extends APITrans(task_name, interpreter, jar) {
  val logger = Logger(this.getClass)

  /**
    * This function adds a level of transformation into expressions to generate new expressions. We need
    * to create things like x[0] where we only have x in the environment.
    */
  private def transform(expr : Expression, etype : Type) : Option[(Seq[Expression], Type)] = {
    etype match {
      case _ if etype.isArray =>
        val new_t = etype.asArrayType().getComponentType
        Some(Seq(new ArrayAccessExpr(expr, new IntegerLiteralExpr(0))), new_t)
      case _ => None
    }
  }

  private def get_available_exprs(bindings : Seq[Binding]) : Seq[Binding] = {
    val result = mutable.ArrayBuffer.empty[Binding]
    // add environment expressions
    bindings.foreach(p => {
      val expr = p._1
      val t = p._2

      result += p
      val new_exprs = this.transform(expr, t)
      if(new_exprs.isDefined) {
        val new_t = new_exprs.get._2
        new_exprs.get._1.foreach(e => result += ((e, new_t)))
      }
    })
    result
  }


  /**
    * Refactor the input program which uses API calls into another program
    * that uses another set of API calls. Here, we assume that the API calls happen in a basic block.
    * ret_val is a pair of (variable name, variable type) representing the returning type of the API
    * sequence and also the receiver variable name.
    */
  override def refactor(prog : JavaDraft, ret_val : (String, String)) : Seq[(BlockStmt, JavaDraft)] = {
    val block = APITrans.find_refactoring_block(prog)
    if(block.isEmpty) {
      this.logger.warn("Cannot find refactoring block.")
      return Seq.empty[(BlockStmt, JavaDraft)]
    }
    val to_pkg = block.get._2

    this.logger.info("Refactoring block =========\n{}\n", block.get.toString)
    this.logger.info("To package {}", to_pkg)

    val env = APITrans.construct_env(prog, block.get._1)
    val ret_tb =
      if(ret_val._2 == "void") {
        APITrans.VOID_PLACEHOLDER
      } else {
        TypeBinding(ret_val._1, JavaUtils.str_to_type(ret_val._2, prog.type_solver))
      }
    this.search_seq(prog, env, block.get._1, to_pkg, ret_tb)
  }

  /**
    * Search for a correct API call sequence.
    */
  private def search_seq(prog : JavaDraft, init_env : Env, block : BlockStmt, to_pname : String, last_type : TypeBinding) : Seq[(BlockStmt, JavaDraft)] = {

    // this is a set of subtyping relations in case of the type solver doesn't work
    val subtyping_map : Map[String, Set[String]] = Map (
      "BodyContentHandler" -> Set("ContentHandler")
      ,"TikaInputStream" -> Set("InputStream")
      ,"PDType1Font" -> Set("PDFont")

      // Email
      ,"DefaultAuthenticator" -> Set("Authenticator")
      ,"InternetAddress" -> Set("Address")
      ,"MimeMessage" -> Set("Message")

      // GUI
      ,"Button" -> Set("Component")
      ,"Label" -> Set("Component")
      ,"StackPane" -> Set("Parent")
      ,"ObservableList" -> Set("List")

      // regression
      ,"RidgeRegresion$Trainer" -> Set("Trainer")
      ,"Trainer" -> Set("RidgeRegression$Trainer")

      // HTML 3
      ,"Html" -> Set("Node")
      ,"Body" -> Set("Node")
      ,"P" -> Set("Node")
      ,"Title" -> Set("Node")
      ,"Div" -> Set("Node")
      ,"Head" -> Set("Node")
      ,"H1" -> Set("Node")

      // CSV
      ,"FileReader" -> Set("Reader")
      ,"FileWriter" -> Set("Writer")

      // jparser
      ,"MethodCallExpr" -> Set("Expression")
      ,"FieldAccessExpr" -> Set("Expression")
      ,"NameExpr" -> Set("Expression")
      // ,"ExpressionStmt" -> Set("Node")

      // jparser3
      ,"EVisitor" -> Set("ASTVisitor")

      // WORD3
      ,"FileOutputStream" -> Set("OutputStream")
    )

    // this is a set of types we know they are not subtyping. We are using this because sometimes the JavaParser's
    // isAssignable crashes on some subtyping checks.
    val not_subtyping_map : Map[String, Set[String]] = Map(
      // regression
      "Trainer" -> Set("RidgeRegression")
      ,"RidgeRegression$Trainer" -> Set("RidgeRegression")
      ,"AttributeDataset" -> Set("RidgeRegression", "RidgeRegression$Trainer")
    )

    // test whether ta is a subtype of tb using subtyping map
    def has_subtyping_map(ta : Type, tb : Type) : Boolean = {
      val tas = ta.describe().split("\\.").last
      val tbs = tb.describe().split("\\.").last
      subtyping_map.contains(tas) && subtyping_map(tas).contains(tbs)
    }

    // test whether ta is a subtype of tb using subtyping map
    def has_no_subtyping_map(ta : Type, tb : Type) : Boolean = {
      val tas = ta.describe().split("\\.").last
      val tbs = tb.describe().split("\\.").last
      not_subtyping_map.contains(tas) && not_subtyping_map(tas).contains(tbs)
    }

    def is_subtype(ta : Type, tb : Type) : Boolean = {
      // logger.debug("ta : {}", ta.describe())
      // logger.debug("tb : {}", tb.describe())
      if(has_no_subtyping_map(ta, tb)) {
        return false
      }

      has_subtyping_map(ta, tb) || prog.is_subtype(ta, tb)
    }

    /**
      * Merge a new set of requirement types. We also consider subtyping here. It also returns a variable renaming
      * for mapping the variable shadowed by the subtyping. For example, if we need an integer v0 and a new requirement
      * asks for a number v1. Then we have to rename v1 in the expression to v0 since we didn't accept v1.
      */
    def merge_req_types(old_req : mutable.Set[TypeBinding], new_req : Set[TypeBinding]) : (mutable.Set[TypeBinding], Map[String, String]) = {
      val result = mutable.Set.empty[TypeBinding]
      val renaming = mutable.Map.empty[String, String]
      result ++= old_req
      for(nr <- new_req) {
        var sub_type_var : Option[String] = None
        for(or <- result) {
          if(sub_type_var.isEmpty) {
            // if(nr.t.isAssignableBy(or.t) || has_subtyping_map(or.t, nr.t)) {
            if(is_subtype(or.t, nr.t)) { sub_type_var = Some(or.name) }
          }
        }
        if(sub_type_var.isEmpty) {
          result += nr
        } else {
          if(!renaming.contains(nr.name)) {
            renaming.put(nr.name, sub_type_var.get)
          }
        }
      }
      (result, renaming.toMap)
    }

    val all_methods: Seq[JarFunc] = jar.funcs
      .filter(p => p._1.getName.startsWith("java.") || p._1.getName.startsWith(to_pname))
      .foldLeft(Seq.empty[JarFunc])((acc, p) => acc ++ p._2)
      .filter(f => { f match {
        case JarMethod(name, _, clz, _) =>
          hint.is_relevant_method(f)
        case JarConstructor(name, _, _, _) =>
          hint.is_relevant_constructor(f)
      }
      })
    // .sortBy(method_relevance)

    this.logger.debug("========== Total {} methods:{}\n", all_methods.size, all_methods.foldLeft("")((acc, m) => acc + "\n" + (m match {
      case JarMethod(name, t, clazz, _) => "Method: " + name + " : " + t.toString + " in " + clazz.getName
      case JarConstructor(name, t, clazz, _) => "Constructor: " + name + " : " + t.toString + " in " + clazz.getName
    })))

    this.logger.debug("last type: {} : {}", last_type.name, last_type.t.toString)


    /**
      * Generate all possible arguments from a list of list of expressions. We might also be generating new variables
      * for arguments.
      * @param seq - each element is a sequence of candidate expression for a specific parameter and we might be
      *            creating new variable for it
      * @return - a sequence of candidate arguments and their requirement name/types
      */
    def all_possible_params(seq: Seq[(Seq[Expression], Type)]) : Seq[(NodeList[Expression], Set[TypeBinding])] = {
      val result = mutable.ArrayBuffer.empty[(NodeList[Expression], Set[TypeBinding])]

      val CONSTANTS : Map[Type, Set[Expression]] = Map(
        PrimitiveType.INT -> Set(
          new IntegerLiteralExpr(-1),
          new IntegerLiteralExpr(0),
          new IntegerLiteralExpr(1)
        )
      )

      def helper(working_args : NodeList[Expression], working_req : Set[TypeBinding],
                 remaining : Seq[(Seq[Expression], Type)]) : Unit = {
        if(remaining.isEmpty) {
          result += ((working_args, working_req))
          return
        }

        val cand_exprs = remaining.head._1
        val t = remaining.head._2
        for(expr <- cand_exprs) {
          // new arguments
          val new_args = new NodeList[Expression]
          new_args.addAll(working_args)
          new_args.add(expr)
          helper(new_args, working_req, remaining.tail)
        }

        // add in constants
        if(CONSTANTS.contains(t)) {
          CONSTANTS(t).foreach(e => {
            val new_args = new NodeList[Expression]
            new_args.addAll(working_args)
            new_args.add(e)
            helper(new_args, working_req, remaining.tail)
          })
        }

        // we create a new variable that needs to be fulfilled in the next step of creating API sequence
        // if(cand_exprs.isEmpty && !CONSTANTS.contains(t)) {
        {
          val new_var = JavaUtils.mk_var()
          val new_args = new NodeList[Expression]
          val new_expr = new NameExpr(new_var)
          new_args.addAll(working_args)
          new_args.add(new_expr)
          helper(new_args, working_req + TypeBinding(new_var, t), remaining.tail)

          // regression task hack
          if(task_name == "regression" && t.describe() == "double[]") {
            // add new variable array access expression
            val new_var2 = JavaUtils.mk_var()
            val new_args2 = new NodeList[Expression]
            val new_expr2 = new ArrayAccessExpr(new NameExpr(new_var2), new IntegerLiteralExpr(0))
            val new_t = new ArrayType(t)
            new_args2.addAll(working_args)
            new_args2.add(new_expr2)
            helper(new_args2, working_req + TypeBinding(new_var2, new_t), remaining.tail)
          }
        }
      }

      helper(new NodeList[Expression], Set.empty[TypeBinding], seq)
      result
    }


    // working_seq - This is the sequence we are working on
    val working_seq = mutable.ListBuffer.empty[Statement]
    // environment
    val env = init_env
    // A set of requirement type and its variable name we need to fulfill
    var req_types = mutable.Set(last_type)
    // a set of defined object types in the working sequence
    var defined_types = mutable.Set.empty[Type]

    val seq_result = mutable.ArrayBuffer.empty[(BlockStmt, JavaDraft)]

    /**
      * Helper function for constructing sequences backwards. Each step has a requirement type that we need to fulfill.
      */
    def helper() : Unit = {

      /**
        * Get a method call from a statement sequence
        */
      def get_method_name(expr: Expression) : String = {
        expr match {
          case mce : MethodCallExpr =>
            if(mce.getScope.isPresent) {
              if(task_name == "graphics" || task_name == "jparser3" || task_name == "kmeans3" ||
                task_name == "word3" || task_name == "pdf3" || task_name == "email" || task_name == "ftp") {
                mce.getNameAsString
              } else {
                mce.toString.replaceAll("v_\\d+", "v")
              }
              /*
              if(mce.getScope.get().toString.matches("v_\\d+")) {
                // mce.getNameAsString
                val args_string = new StringBuilder
                val args = mce.getArguments
                for(i <- 0 until args.size()) {
                }
                mce.getNameAsString
              } else {
                // mce.getScope.get().toString + "." + mce.getNameAsString + "(" + mce.getArguments.toString.replaceAll("v_\\d+", "v") + ")"
                mce.toString.replaceAll("v_\\d+", "v")
              }
              */
            } else {
              mce.getNameAsString
            }
          case oce : ObjectCreationExpr => "new " + oce.getType.getNameAsString
          case decl : VariableDeclarationExpr => get_method_name(decl.getVariables.get(0).getInitializer.get())
        }
      }

      // if there's no requirement type, we are done
      if(req_types.isEmpty && working_seq.nonEmpty) {
        /*
        val new_method = APITrans.inject(prog, block, working_seq)
        // this.logger.info("Testing ==============\n{}\n", new_method.toString)
        val test = prog.get_test.get
        val result = test.accept(interpreter, new_method.toString)
        // this.logger.info("Result: {}", result._1)
        // this.logger.info("Runtime: {}", result._2)
        // this.logger.info("Output =================\n{}\n", result._3)
        if(result._1) {
          this.logger.info("Solution ===================\n{}\n", new_method.toString)
          return Some(prog.create_new_draft(new_method))
        } else {
          this.logger.debug("Incorrect solution =================\n{}\n{}\n", new_method.toString, result._3)
          // return None
        }
        */
        val nlist = new NodeList[Statement]()
        working_seq.foreach(s => {
          nlist.add(s)
        })
        val block_result = new BlockStmt(nlist)
        seq_result += ((block_result, null))
        return
      }

      if(working_seq.size > APITrans.LENGTH_LIMIT) {
        return
      }

      // calculate all the available expressions plus one level of transformation
      val available_exprs = get_available_exprs(env) ++ get_available_exprs(req_types.map(tb => (new NameExpr(tb.name), tb.t)).toSeq)

      {
        this.logger.debug("========== Working sequence:{}\n", working_seq.foldLeft("")((acc, s) => acc + "\n" + s.toString))
        this.logger.debug("========== Environment:{}\n", env.foldLeft("")((acc, e) => acc + "\n" + e._1.toString + " : " + e._2.toString))
        this.logger.debug("========== Requirement types:{}\n", req_types.foldLeft("")((acc, t) => acc + "\n" + t.name + " : " + t.t.toString))
        this.logger.debug("========== Defined types:{}\n", defined_types.foldLeft("")((acc, t) => acc + "\n" + t.describe()))
        this.logger.debug("========== Available Expressions:{}\n", available_exprs.foldLeft("")((acc, b) => acc + "\n" + b._1.toString + " : " + b._2.describe()))
      }


      val working_req_types =
        if(req_types.isEmpty) {
          Set(APITrans.VOID_PLACEHOLDER)
        } else {
          req_types
        }
      val cand_calls = working_req_types.foldLeft(Seq.empty[(Expression, Set[TypeBinding], Type, TypeBinding)])((all_calls, req_t) => {
        // search for all the methods that can satisfy the requirement type
        val cand_methods = all_methods
          .filter(m => m match {
            case JarMethod(name, t, _, _) =>
              if((task_name == "html3" && name.startsWith("append") && req_t.t.isVoid) ||
                (task_name == "ftp" && name.startsWith("ls") && req_t.t.isVoid)) {
                true
              } else {
                is_subtype(t.ret, req_t.t) || t.ret.isVoid
              }
            case JarConstructor(_, t, clz, _) =>
              if(clz.getName.endsWith("ExpressionStmt")) {
                println("FOO")
              }
              is_subtype(t.ret, req_t.t) || t.ret.isVoid
          })
        this.logger.debug("========== Candidate methods for type {} :{}\n", req_t.t.describe(), cand_methods.foldLeft("")((acc, m) => acc + "\n" + (m match {
          case JarMethod(name, t, clazz, _) => "Method: " + name + " : " + t.toString + " in " + clazz.getName
          case JarConstructor(name, t, clazz, _) => "Constructor: " + name + " : " + t.toString + " in " + clazz.getName
        })))


        // construct all possible method calls
        // the result is a sequence of tuples where
        // the first one is the API call expression
        // the second one is a new set of type requirements
        // the third one is the return type
        // the fourth one is the type requirement we have just fulfilled
        all_calls ++
          cand_methods.foldLeft(Seq.empty[(Expression, Set[TypeBinding], Type, TypeBinding)])((acc, cm) => {
            cm match {
              case JarMethod(name, ft, clazz, method) =>
                val scope_type = prog.class_to_type(clazz)
                val scope_exprs: Seq[Expression] =
                  if (Modifier.isStatic(method.getModifiers)) {
                    val name_list = clazz.getName.split("\\.")
                    if (name_list.length > 1) {
                      // Seq(JavaUtils.name_list_to_fieldaccess(name_list))
                      Seq(new NameExpr(name_list.last))
                    } else {
                      Seq(new NameExpr(name_list.head))
                    }
                  } else {
                    /*
                    env.filter(binding => is_subtype(binding._2, scope_type)).map(_._1) ++
                      req_types.filter(tb => is_subtype(tb.t, scope_type)).map(tb => new NameExpr(tb.name))
                      */
                    available_exprs.filter(b => is_subtype(b._2, scope_type)).map(_._1)
                      .filter(e => req_t.name != e.toString)
                  }


                /*
                val cand_args_list: Seq[(Seq[Expression], Type)] = ft.params.map(t =>
                  (env.filter(binding => is_subtype(binding._2, t)).map(_._1) ++
                    req_types.filter(rt => is_subtype(rt.t, t)).map(rt => new NameExpr(rt.name)).toSeq, t))
                */
                val cand_args_list : Seq[(Seq[Expression], Type)] = ft.params.map(pt => {
                  (available_exprs.filter(b => is_subtype(b._2, pt)).map(_._1).filter(e => req_t.name != e.toString), pt)})

                // these are actual arguments and their requirement types
                val params_list: Seq[(NodeList[Expression], Set[TypeBinding])] = all_possible_params(cand_args_list)

                // use arguments to construct method calls
                val call_exprs = mutable.ArrayBuffer.empty[(Expression, Set[TypeBinding], Type, TypeBinding)]
                scope_exprs.foreach(scope => {
                  call_exprs ++= params_list.map(plist => (new MethodCallExpr(scope, name, plist._1), plist._2, ft.ret, req_t))
                })

                // construct a "virtual" scope variable that will be defined later
                if(scope_exprs.isEmpty) {
                  val new_var = JavaUtils.mk_var()
                  val new_scope = new NameExpr(new_var)
                  val scope_req_t = TypeBinding(new_var, scope_type)
                  call_exprs ++= params_list.map(plist => (new MethodCallExpr(new_scope, name, plist._1), plist._2 + scope_req_t, ft.ret, req_t))
                }

                acc ++ call_exprs
              case JarConstructor(_, ft, clazz, _) =>
                /*
                val cand_args_list: Seq[(Seq[Expression], Type)] = ft.params.map(t =>
                  (env.filter(binding => is_subtype(binding._2, t)).map(_._1) ++
                    req_types.filter(rt => is_subtype(rt.t, t)).map(rt => new NameExpr(rt.name)).toSeq, t))
                    */
                val cand_args_list : Seq[(Seq[Expression], Type)] = ft.params.map(pt => {
                  (available_exprs.filter(b => is_subtype(b._2, pt)).map(_._1).filter(e => req_t.name != e.toString), pt)})
                // these are actual arguments and their requirement types
                val params_list: Seq[(NodeList[Expression], Set[TypeBinding])] = all_possible_params(cand_args_list)
                acc ++ params_list.map(plist => (new ObjectCreationExpr().setType(clazz).setArguments(plist._1), plist._2, ft.ret, req_t))
            }
          })
            .filter(call => {
              if (working_seq.isEmpty) {
                true
              } else {
                val call_name = call._1 match {
                  case mce: MethodCallExpr => get_method_name(mce)
                  case oce: ObjectCreationExpr => get_method_name(oce)
                }
                working_seq.forall(s => {
                  val last_method_call = get_method_name(s.asInstanceOf[ExpressionStmt].getExpression)
                  // logger.debug("last_method_call")
                  // logger.debug("{}", s)
                  // logger.debug("{}", !last_method_call.equals(call_name))
                  !last_method_call.equals(call_name)
                })
              }
            })

            // we want to filter out the ones like this:
            // v1.addPage(...) where v1 is Object_1
            // Object1 v0 = new Object1(...)
            // We don't want to have duplicate object creations. So we want to filter out the API calls where it requires
            // something which is defined later in the sequence.
            .filter(call => {
            // call._2.forall(tb => !defined_types.contains(tb.t))
            // logger.debug("defined_types")
            // logger.debug("{}", call)
            // logger.debug("{}", call._2.forall(tb => defined_types.isEmpty || defined_types.forall(dt => !is_subtype(tb.t, dt))))
            call._2.forall(tb => defined_types.isEmpty || defined_types.forall(dt => !is_subtype(tb.t, dt)))
          })
            .filter(call => {
              // logger.debug("relevant")
              // logger.debug("{}", call)
              // logger.debug("{}", call._2.forall(rt => hint.is_relevant_type(rt.t)))
              call._2.forall(rt => hint.is_relevant_type(rt.t))
            })
      })

      this.logger.debug("========== {} Candidate call expressions:\n", cand_calls.size)
      cand_calls.foreach(cc => {
        this.logger.debug("Call {} requires:{}\n", cc._1.toString, cc._2.foldLeft("")((acc, b) => acc + "\n" + b.name + " : " + b.t.toString))
      })

      // resurse on each candidate call
      cand_calls
        // we don't want repeated function calls
        .sortBy(p => p._2.size)
        .foreach(call => {
          val req_t = call._4
          this.logger.debug("Trying {}", call._1.toString)
          val (stmt, new_req_types, new_defined_types) =
            if(task_name == "html3" && call._1.toString.contains("append") && req_t.t.isVoid) {
              val (n_req, renaming) = merge_req_types(req_types, call._2)
              JavaUtils.rename(call._1, renaming)
              (new ExpressionStmt(call._1), n_req, defined_types)
            } else if(task_name == "ftp" && call._1.toString.contains("ls") && req_t.t.isVoid) {
              val (n_req, renaming) = merge_req_types(req_types, call._2)
              JavaUtils.rename(call._1, renaming)
              (new ExpressionStmt(call._1), n_req, defined_types)
            } else if(!call._3.isVoid) {
              // if(!req_t.t.isVoid) {
              // if(!req_t.t.isVoid || req_t.t.) {
              val (n_req, renaming) = merge_req_types(req_types - req_t, call._2)
              JavaUtils.rename(call._1, renaming)
              val decl = new VariableDeclarator(JavaUtils.str_to_syntax_type(req_t.t.describe().replace("$", "\\.").split("\\.").last), req_t.name, call._1)
              val new_types =
                if(req_t.t.isReferenceType && !req_t.t.describe().startsWith("java.") &&
                  // we restrict this to only object creation expr to allow things like
                  // Builder v0 = new Builder();
                  // Builder v1 = v0.set_url(url_str);
                  // Result v2 = v1.build();
                  call._1.isInstanceOf[ObjectCreationExpr]) {
                  // defined_types + req_t.t
                  defined_types + call._3
                } else {
                  defined_types
                }
              (new ExpressionStmt(new VariableDeclarationExpr(decl)), n_req, new_types)
            } else {
              val (n_req, renaming) = merge_req_types(req_types, call._2)
              JavaUtils.rename(call._1, renaming)
              (new ExpressionStmt(call._1), n_req, defined_types)
            }

          this.logger.debug("New statement: {}", stmt.toString)

          // update the environment
          // val new_env : Env = env :+ ((new NameExpr(req_t._1), req_t._2))

          // state the state for next recursive call
          working_seq.prepend(stmt)
          val old_req_types = req_types.clone()
          req_types = new_req_types
          req_types -= APITrans.VOID_PLACEHOLDER
          val old_defined_types = defined_types
          defined_types = new_defined_types

          helper()
          if(seq_result.size > APITrans.SEQ_LIMIT) {
            return
          }

          // restore the previous state
          this.logger.debug("Backing on {}", call._1.toString)
          working_seq.remove(0)
          req_types = old_req_types
          defined_types = old_defined_types
        })

      None
    }

    val (runtime, _) = Utils.time(helper())
    logger.info("Synthesis time: {} ms", runtime)
    seq_result
  }



  /**
    * API translation call functionalities
    */

  /**
    * Find existing defined objects in the program. It returns an expression because the object might not be
    * a simple name
    */
  private def find_obj(prog: JavaDraft, to_class: Name) : mutable.ArrayBuffer[Expression] = {
    val objs = mutable.ArrayBuffer.empty[Expression]

    // add the node if it's an object under the class
    def add(node : Expression) : Unit = {
      val symbol = prog.type_solver.solve(node)
      symbol.getCorrespondingDeclaration.getType match {
        case rt : ReferenceTypeImpl =>
          if(rt.describe() == to_class.asString()) {
            objs += node
          }
      }
    }

    new VoidVisitorAdapter[Any] {
      override def visit(node : NameExpr, arg : Any) : Unit = add(node)
      override def visit(node : FieldAccessExpr, arg : Any) : Unit = add(node)
    }
    objs
  }

  /**
    * Translate each API calls from a library into the calls from another library
    */
  def translate(prog : JavaDraft, from_class : Name, to_class : Name, interpreter : JavaFastInterpreter) : Option[JavaDraft] = {
    val from_calls = this.gather_calls(prog, from_class)
    val to_calls = this.get_class_method(prog, to_class)

    def helper(working_prog : JavaDraft, src_calls : Seq[Expression]) : Option[JavaDraft] = {
      this.logger.info("Working prog ================\n{}\n", working_prog.toString)
      if(src_calls.isEmpty) {
        val test = working_prog.get_test.get
        val result = test.accept(interpreter, working_prog.toString)
        this.logger.info("Result: {}", result._1)
        this.logger.info("Runtime: {}", result._2)
        this.logger.info("Output =================\n{}\n", result._3)
        if(result._1) {
          this.logger.info("Solution ===================\n{}\n", working_prog.toString)
          return Some(working_prog)
        } else {
          this.logger.debug("Incorrect solution =================\n{}\n{}\n", working_prog.toString, result._3)
          return None
        }
      }

      val src_call = src_calls.head
      this.logger.info("Working on =============\n{}\n", src_call.toString)

      // search for calls from the target library
      val target_calls = this.find_target_calls(working_prog, src_call, to_calls, to_class)

      // try to replace the API call
      for(tc <- target_calls) {
        this.logger.info("Trying API call: {} : {}\n", tc._1, tc._2)
        val new_progs = this.replace_call(working_prog, src_call, tc, to_class)
        val candidates = new_progs.map(helper(_, src_calls.tail))
        for(c <- candidates) {
          if(c.isDefined) {
            return c
          }
        }
      }
      None
    }

    helper(prog, from_calls)
  }


  /**
    * extract all the method calls and object creations from the program under the given class
    */
  private def gather_calls(prog: JavaDraft, clazz: Name) : Seq[Expression] = {
    val result = mutable.ArrayBuffer.empty[Expression]
    val visitor = new VoidVisitorAdapter[Any] {
      override def visit(call : MethodCallExpr, arg : Any) : Unit = {
        val scope = call.getScope
        if(scope.isPresent) {
          val symbol = prog.type_solver.solve(scope.get())
          if(symbol.isSolved) {
            symbol.getCorrespondingDeclaration.getType match {
              case rt : ReferenceTypeImpl =>
                if(rt.describe() == clazz.asString()) {
                  result += call
                }
              case _ =>
            }
          } else {
            val imp = find_corresponding_import(prog, scope.get().toString())
            if(imp.isDefined && imp.get.toString() == clazz.toString()) {
              result += call
            }
          }
        }
      }

      override def visit(creation : ObjectCreationExpr, arg : Any) : Unit = {
        val tname = creation.getType.getNameAsString
        val imp = find_corresponding_import(prog, tname)
        if(imp.isDefined && imp.get.toString() == clazz.toString()) {
          result += creation
        }
      }
    }
    prog.method.getBody.get().accept(visitor, null)
    result
  }

  /**
    * Given a class name, find its correpsonding import decl in the compilation unit
    */
  private def find_corresponding_import(prog : JavaDraft, cname : String) : Option[Name] = {
    // the scope is the class name. Get the class name from the imports
    val imports = prog.compilation_unit.getImports
    for(i <- 0 until imports.size()) {
      val imp = imports.get(i)
      if(imp.getName.getIdentifier == cname) {
        return Some(imp.getName)
      }
    }

    // we haven't found the class in the import list. Then it is assumed to be in the java.lang package
    Some(JavaUtils.name_list_to_qname(Array("java", "lang", cname)))
  }

  /**
    * Get all the methods from a given class
    */
  private def get_class_method(prog : JavaDraft, class_name: Name) : Seq[(String, FuncType)] = {
    val full_classname = class_name.toString()

    // get the class from the class name
    val clazz : Class[_] = if(!full_classname.startsWith("java")) {
      Class.forName(full_classname, true, prog.class_loader)
    } else {
      Class.forName(full_classname)
    }

    val methods : Seq[(String, FuncType)] = clazz.getMethods
      .filter(m => java.lang.reflect.Modifier.isPublic(m.getModifiers))
      .map(m => {
        // get parameter types
        val param_types = m.getParameters.map(p => prog.class_to_type(p.getType))

        // get return type
        val ret_type = prog.class_to_type(m.getReturnType)

        // put the type into the set
        val ft = new FuncType(param_types, ret_type)
        (m.getName, ft)
      })

    val cons = clazz.getConstructors
      .map(c => {
        // get parameter types
        val param_types = c.getParameters.map(p => prog.class_to_type(p.getType))
        val ret_type = prog.class_to_type(clazz)

        // put the type into the set
        val ft = new FuncType(param_types, ret_type)
        (clazz.getName, ft)
      })

    methods ++ cons
  }

  /**
    * Get all in-scope variables and their types at the node's position. We only climb upward in the AST.
    */
  private def get_defined_vars(draft : JavaDraft, node : Node) : Seq[(Name, Type)] = {
    val result = mutable.ArrayBuffer.empty[(Name, Type)]

    def helper(n : Node) : Unit = {
      val stmt = JavaUtils.get_enclosing_statement(n).get

      // look for the decls in the same block
      val block = stmt.getParentNode.get()
      val stmt_list = block match {
        case bs : BlockStmt => bs.getStatements
        case ses : SwitchEntryStmt => ses.getStatements
      }
      val stop_index = stmt_list.indexOf(stmt)
      assert(stop_index >= 0 && stop_index < stmt_list.size())
      for(i <- 0 until stop_index) {
        val names = JavaUtils.get_defs(stmt_list.get(i))
        names.map(n => {
          val t = draft.get_type(new JavaDraftNode(n)).asInstanceOf[JavaDraftNodeType].t
          result += ((new Name(n.getNameAsString), t))
        })
      }

      // look at the owner of the block to see if the owner has define new things. Then go up one level if that's possible
      val block_parent = block.getParentNode.get()
      block_parent match {
        case fs : ForStmt =>
          val inits = fs.getInitialization
          for(i <- 0 until inits.size()) {
            val decls = inits.get(i).asInstanceOf[VariableDeclarationExpr].getVariables
            for(j <- 0 until decls.size()) {
              val name = decls.get(j).getName
              val t = draft.type_solver.convert(decls.get(j).getType, decls.get(j))
              result += ((new Name(name.asString()), t))
            }
          }
          helper(fs)
        case md : MethodDeclaration =>
          for(i <- 0 until md.getParameters.size()) {
            val param = md.getParameter(i)
            val name = param.getName
            val t = draft.type_solver.convert(param.getType, param)
            result += ((new Name(name.asString()), t))
          }
        case ts : TryStmt =>
          val decl_exprs = ts.getResources
          for(i <- 0 until decl_exprs.size()) {
            val decls = decl_exprs.get(i).getVariables
            for(j <- 0 until decls.size()) {
              val name = decls.get(j).getName
              val t = draft.type_solver.convert(decls.get(j).getType, decls.get(j))
              result += ((new Name(name.asString()), t))
            }
          }
          helper(ts)
        case _ => helper(block_parent)
      }
    }
    helper(node)
    result
  }

  /**
    * Find a sequence of candidate target calls in the target class
    */
  private def find_target_calls(working_prog: JavaDraft, src_call: Expression, to_calls: Seq[(String, FuncType)], target_class : Name) : Seq[(String, FuncType)] = {
    src_call match {
      case oce : ObjectCreationExpr =>
        // find all the constructors and match the type
        // the param type does not come from the parameter. The paremeters come from the environment.
        val src_func_type = {
          val defined_vars = this.get_defined_vars(working_prog, oce)
          new FuncType(defined_vars.map(_._2), Wildcard.UNBOUNDED)
        }
        to_calls.filter(p => p._1.toString == target_class.toString && working_prog.is_subtype(p._2, src_func_type))
      case mce : MethodCallExpr =>
        val src_func_type = {
          val defined_vars = this.get_defined_vars(working_prog, mce)
          // get the return type
          val ret_type = working_prog.get_type(new JavaDraftNode(mce)).asInstanceOf[JavaDraftNodeType].t
          new FuncType(defined_vars.map(_._2), ret_type)
        }
        to_calls.filter(p => working_prog.is_subtype(p._2, src_func_type))
    }
  }


  /**
    * Replace the source call with the target method. We are still not using the object yet.
    */
  private def replace_call(working_prog: JavaDraft, src_call: Expression, tc: (String, Type), target_class : Name) : Seq[JavaDraft] = {

    /**
      * Search for all the combinations of argument usages
      */
    def mk_args(arg_types : Seq[Type]) : Seq[Seq[Expression]] = {
      val result = mutable.Set.empty[Seq[Expression]]
      val cand_exprs : Seq[Expression] = src_call match {
        case oce : ObjectCreationExpr => this.get_defined_vars(working_prog, oce).map(v => new NameExpr(v._1.asString()))
        case mce : MethodCallExpr => this.get_defined_vars(working_prog, mce).map(v => new NameExpr(v._1.asString()))
      }

      /**
        * We don't care about the side effects of using an expression multipe times
        */
      def helper(working_args : Seq[Expression], rest : Seq[Type]) : Unit = {
        if(rest.isEmpty) {
          result += working_args
          return
        }
        for(i <- cand_exprs.indices) {
          val expr_type = working_prog.get_type(new JavaDraftNode(cand_exprs(i))).asInstanceOf[JavaDraftNodeType].t
          if(working_prog.is_subtype(expr_type, rest.head)) {
            helper(working_args :+ cand_exprs(i), rest.tail)
          }
        }
      }
      helper(Seq.empty[Expression], arg_types)
      result.toSeq
    }

    val candidate_calls : Seq[Expression] = {
      val result = mutable.ArrayBuffer.empty[Expression]
      val args_list = mk_args(tc._2.asInstanceOf[FuncType].params)
      args_list.foreach(alist => {
        val args = new NodeList[Expression]
        alist.foreach(args.add)
        src_call match {
          case _ : ObjectCreationExpr =>
            result += new ObjectCreationExpr()
              // .setType(JavaParser.parseClassOrInterfaceType(target_class.asString()))
              // .setScope(JavaUtils.qname_to_fieldaccess(target_class))
              .setType(JavaParser.parseClassOrInterfaceType(target_class.asString()).removeScope())
              .setArguments(args)
          case _ : MethodCallExpr =>
            val objs = this.get_defined_vars(working_prog, src_call)
              .filter(p => {
                val req_type = JavaUtils.str_to_type(target_class.asString(), working_prog.type_solver)
                working_prog.is_subtype(p._2, req_type)
              })
            result ++= objs.map(o => new MethodCallExpr(new NameExpr(o._1.asString()), tc._1, args))
        }
      })
      result
    }

    val result = src_call match {
      case _ : ObjectCreationExpr =>
        // we need to get to the declarator
        val src_stmt = JavaUtils.get_enclosing_statement(src_call).get
        var src_decl = {
          val all_src_decls = src_stmt.asInstanceOf[ExpressionStmt]
            .getExpression.asInstanceOf[VariableDeclarationExpr].getVariables
          var result : Option[VariableDeclarator] = None
          for(i <- 0 until all_src_decls.size()) {
            val decl = all_src_decls.get(i)
            if(decl.getInitializer.isPresent && decl.getInitializer.get().equals(src_call)) {
              result = Some(decl)
            }
          }
          result.get
        }

        // we need to construct an assignment statement to replace the object creation
        candidate_calls.map(call => {
          val target_stmt = {
            val v = new VariableDeclarator(JavaParser.parseClassOrInterfaceType(target_class.asString()), src_decl.getName, call)
            new ExpressionStmt(new VariableDeclarationExpr(v))
          }
          // working_prog.replace(new JavaDraftNode(src_call), new JavaDraftNode(call)).asInstanceOf[JavaDraft]
          working_prog.replace(new JavaDraftNode(src_stmt), new JavaDraftNode(target_stmt)).asInstanceOf[JavaDraft]
        })
      case _ : MethodCallExpr =>
        candidate_calls.map(call => {
          working_prog.replace(new JavaDraftNode(src_call), new JavaDraftNode(call)).asInstanceOf[JavaDraft]
        })
    }
    result
  }
}

