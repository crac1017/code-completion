package edu.rice.pliny.apitrans.hint.mapper

import java.io.{File, PrintWriter}
import java.util.Properties

import com.github.javaparser.JavaParser
import com.github.javaparser.ast.body.{ClassOrInterfaceDeclaration, ConstructorDeclaration, MethodDeclaration}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.{AppConfig, Utils}
import edu.rice.pliny.apitrans.hint.mapper.JavadocMapper.MethodDist
import edu.rice.pliny.apitrans.hint.mapper.MethodDesc.FuncType
import edu.rice.pliny.mcts.{HungarianAlgorithm, StanfordSentenceProcessor}
import edu.stanford.nlp.pipeline.{CoreDocument, StanfordCoreNLP}
import org.apache.commons.lang.StringEscapeUtils
import org.deeplearning4j.models.embeddings.learning.impl.elements.CBOW
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer
import org.deeplearning4j.models.word2vec.{VocabWord, Word2Vec}
import org.deeplearning4j.text.sentenceiterator.LineSentenceIterator
import org.deeplearning4j.text.stopwords.StopWords
import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.{EndingPreProcessor, StringCleaning}
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory
import smile.nlp.stemmer.PorterStemmer

import scala.collection.mutable
import scala.util.Random
import scala.collection.JavaConverters._

object JavadocMapper {
  val logger = Logger(this.getClass)

  /**
    * A vector representing the distance between two methods
    * @param t - type distance
    * @param desc - description distance
    * @param name - name distance
    * @param class_desc - class description distance
    */
  case class MethodDist(t : Double, desc : Double, name : Double, class_desc : Double)

  /*
  val TYPE_WT : Double = 0.3
  val DESC_WT : Double = 0.2
  val NAME_WT : Double = 0.4
  val CLASS_DESC_WT : Double = 0.1
  */
  val TYPE_WT : Double = 0.1
  val DESC_WT : Double = 0.5
  val NAME_WT : Double = 0.3
  val CLASS_DESC_WT : Double = 0.1

  val TEXT_FILE = "text.txt"
  val METHODS_FILE = "methods.json"


  /**
    * Load a javadoc mapper from the disk
    */
  def load_mapper : Option[JavadocMapper] = {
    val text_file = new File(JavadocMapper.TEXT_FILE)
    val methods_file = new File(JavadocMapper.METHODS_FILE)
    val model_file = new File(AppConfig.TranslationConfig.word2vec_filename)

    if(!text_file.exists()) {
      this.logger.error("{} does not exist.", text_file.getCanonicalPath)
      return None
    }

    if(!methods_file.exists()) {
      this.logger.error("{} does not exist.", methods_file.getCanonicalPath)
      return None
    }

    if(!model_file.exists()) {
      this.logger.error("{} does not exist.", model_file.getCanonicalPath)
      return None
    }
    Some(new JavadocMapper("", ""))
  }

  // Stanford Core NLP pipeline
  val nlp_pipeline = {
    val props = new Properties()
    props.setProperty("annotators", "tokenize, ssplit, pos, lemma")
    new StanfordCoreNLP(props)
  }

  val STOP_WORDS = StopWords.getStopWords.asScala.toSet

  object BestTokenizer extends TokenPreProcess {
    val stemmer = new PorterStemmer
    val ending_proc = new EndingPreProcessor
    override def preProcess(token: String): String = {
      val t1 = StringCleaning.stripPunct(token)
      val t2 = t1.toLowerCase
      val t3 = try {
        /*
        this.synchronized({
          this.stemmer.stem(t2)
        })
        */
        this.ending_proc.preProcess(t2)
      } catch {
        case e : Throwable =>
          println("Bad word: '" + t2 + "'")
          ""
      }
      t3
    }
  }

}

/**
  * Main javadoc mapper class. It takes two sequences of links.
  * @param src_dir_path - a dir containing one or more java files where we want to use for building word2vec model
  * @param method_path - a dir containing java files where we want to extract methods
  */
class JavadocMapper(src_dir_path : String, method_path : String) {
  val logger = Logger(this.getClass)
  val (model, methods) : (Word2Vec, MethodSet) = this.build_model()
  this.logger.info("Finished building Javadoc Mapper. {} methods in total", methods.size)
  val unseen_fasttext_words = mutable.Set.empty[String]

  // load bert embeddings
  val bert_embeddings : mutable.Map[String, mutable.Map[String, Array[Double]]] =
    if(!AppConfig.TranslationConfig.use_bert) {
      mutable.Map.empty[String, mutable.Map[String, Array[Double]]]
    } else {
      val result = mutable.Map.empty[String, mutable.Map[String, Array[Double]]]
      val lines = scala.io.Source.fromFile(AppConfig.TranslationConfig.bert_embedding_filename).getLines()
      val total_sentence = Integer.parseInt(lines.next())
      this.logger.info("Loading {} sentences from BERT embeddings.", total_sentence)
      for(i <- 0 until total_sentence) {
        val sentence = lines.next()
        result.put(sentence, mutable.Map.empty[String, Array[Double]])

        this.logger.debug("Reading {}, {}", i, sentence)
        val word_size = Integer.parseInt(lines.next())
        for(wi <- 0 until word_size) {
          val fields = lines.next().split("\\s")
          val word = fields.head
          val vec = fields.tail.map(s => s.toDouble)
          result(sentence).put(word, vec)
        }
        this.logger.debug("Sentence '{}' has {} words: {}", sentence, word_size, result(sentence).foldLeft("")((acc, p) => acc + "\t'" + p._1 + "'"))
      }
      result
    }

  val fasttext_model = {
    val dict = mutable.Map.empty[String, Array[Double]]
    val vec_lines = scala.io.Source.fromFile(AppConfig.TranslationConfig.fasttext_vec_filename).mkString
    vec_lines
      .split("\n")
      .foreach(line => {
        val fields = line.split("\\s")
        val word = fields.head
        val vec = fields.tail.map(f => f.toDouble)
        dict.put(word, vec)
      })
    dict
  }


  private def save_model(m : Word2Vec) : Unit = {
    WordVectorSerializer.writeWord2VecModel(m, AppConfig.TranslationConfig.word2vec_filename)
    WordVectorSerializer.writeWordVectors(m.getLookupTable, AppConfig.TranslationConfig.word2vec_wordvec_filename)
  }

  private def load_model() : Word2Vec = {
    WordVectorSerializer.readWord2VecModel(AppConfig.TranslationConfig.word2vec_filename)
  }

  /**
    * Calculate word similarity
    */
  private def word_sim(w1 : String, w2 : String) : Double = {
    AppConfig.TranslationConfig.algorithm match {
      case "word2vec" => this.model.similarity(w1, w2)
      case "fasttext" =>
        val pw1 = JavadocMapper.BestTokenizer.preProcess(w1)
        val pw2 = JavadocMapper.BestTokenizer.preProcess(w2)
        if((pw1.isEmpty || pw2.isEmpty) && pw1 != pw2) {
          return 0.0
        }

        if(!fasttext_model.contains(pw1)) {
          this.unseen_fasttext_words += pw1
          return 0.0
        }
        if(!fasttext_model.contains(pw2)) {
          this.unseen_fasttext_words += pw2
          return 0.0
        }
        val v1 = fasttext_model(JavadocMapper.BestTokenizer.preProcess(w1))
        val v2 = fasttext_model(JavadocMapper.BestTokenizer.preProcess(w2))
        Utils.CosineSimilarity.cosineSimilarity(v1, v2)
    }
  }

  /**
    * Preprocess text from the javadoc. Mainly we need to remove special symbols so that we can use the text
    * for training word2vec models.
    */
  private def process_text(text : String) : String = {
    this.logger.debug("Before: {}", text)
    val r = StringEscapeUtils.unescapeHtml(text)
      .replaceAll("<code>(\\w+)</code>", "$1")
      .replaceAll("<CODE>(\\w+)</CODE>", "$1")
      .replaceAll("<code>(\\w+)<code>", "$1")
      .replaceAll("<CODE>(\\w+)<CODE>", "$1")
      .replaceAll("<var>(\\w+)</var>", "$1")
      .replaceAll("<VAR>(\\w+)</VAR>", "$1")
      .replaceAll("<sup>(\\w+)</sup>", "$1")
      .replaceAll("<SUP>(\\w+)</SUP>", "$1")
      .replaceAll("<color>(\\w+)</color>", "$1")
      .replaceAll("<COLOR>(\\w+)</COLOR>", "$1")
      .replaceAll("(?s)<a.*?>(.*?)</a>", "$1")
      .replaceAll("(?s)<A.*?>(.*?)</A>", "$1")
      .replaceAll("(?s)<h.*?>(.*?)</h>", "$1")
      .replaceAll("(?s)<H.*?>(.*?)</H>", "$1")
      .replaceAll("(?s)<h2>(.*?)</h2>", "$1")
      .replaceAll("(?s)<H2>(.*?)</H2>", "$1")
      .replaceAll("(?s)<h3>(.*?)</h3>", "$1")
      .replaceAll("(?s)<H3>(.*?)</H3>", "$1")
      .replaceAll("(?s)<h4>(.*?)</h4>", "$1")
      .replaceAll("(?s)<H4>(.*?)</H4>", "$1")
      .replaceAll("(?s)<cite>(.*?)</cite>", "$1")
      .replaceAll("(?s)<CITE>(.*?)</CITE>", "$1")
      .replaceAll("(?s)<strong.*?>(.*?)</strong>", "$1")
      .replaceAll("\\{@code (\\w+)\\}", "$1")
      .replaceAll("\\{@link (\\w+)\\}", "$1")
      .replaceAll("@(param\\s-\\s|return\\s-\\s|param\\s|return\\s)", "")
      .replaceAll("(?s)\\{@code .*?\\}", "")
      .replaceAll("(?s)\\{@link .*?\\}", "")
      .replaceAll("(?s)\\{@linkplain .*?\\}", "")
      .replaceAll("</?(p|p/|e|E|br|BR|ul|UL|li|LI|Li|em|EM|ol|OL|i|b|U|P|tt|a|T|dl|DL|DT|dt|DD|dd|hr|HR|b|B|i|I)>", "")
      .replaceAll("(?s)<center>.*?</center>", "")
      .replaceAll("(?s)<CENTER>.*?</CENTER>", "")
      .replaceAll("(?s)<code>.*?</code>", "")
      .replaceAll("(?s)<CODE>.*?</CODE>", "")
      .replaceAll("(?s)<code>.*?<code>", "")
      .replaceAll("(?s)<CODE>.*?<CODE>", "")
      .replaceAll("(?s)<img>.*?</img>", "")
      .replaceAll("(?s)<IMG>.*?</IMG>", "")
      .replaceAll("(?s)<table .*?>.*?</table>", "")
      .replaceAll("(?s)<TABLE .*?>.*?</TABLE>", "")
      .replaceAll("(?s)<pre>.*?</pre>", "")
      .replaceAll("(?s)<PRE>.*?</PRE>", "")
      .replaceAll("(?s)<font>.*?</font>", "")
      .replaceAll("(?s)<FONT>.*?</FONT>", "")
      .replaceAll("(?s)<var>.*?</var>", "")
      .replaceAll("(?s)<VAR>.*?</VAR>", "")
      .replaceAll("(?s)<blockquote>.*?</blockquote>", "")
      .replaceAll("(?s)<BLOCKQUOTE>.*?</BLOCKQUOTE>", "")
      .replaceAll("[\\d\\.:,\"\'\\(\\)\\[\\]|/?!;<>{}#]+", "")
      .toLowerCase
      .trim
    this.logger.debug("After: {}", r)
    if(r.isEmpty) {
      ""
    } else if(r.endsWith(".")) {
      r
    } else if(!r.last.isLetterOrDigit) {
      r.slice(0, r.length - 1).concat(".")
    } else {
      r.concat(".")
    }
  }

  /**
    * Get text from a directory containing java source files
    */
  private def inspect_srcdir(dir : String, save_methods : Boolean) : (String, MethodSet) = {
    this.logger.trace("Inspecting src directory {}. Saving methods {}", dir, save_methods)
    val method_result = new MethodSet
    val text_result = new java.lang.StringBuilder

    /**
      * Get text from methods
      */
    def handle_method(m : MethodDeclaration, class_desc : String, class_name : String, package_name : String) : Unit = {
      val (t, param_names) = {
        val ret_t = m.getType.asString()
        val params = m.getParameters

        val param_types = mutable.ArrayBuffer.empty[String]
        val param_names = mutable.Map.empty[String, String]
        for(i <- 0 until params.size()) {
          param_types += params.get(i).getType.asString()
          param_names.put(params.get(i).getNameAsString, "")
        }
        (FuncType(param_types, ret_t), param_names)
      }

      val name = m.getNameAsString
      this.logger.trace("Extracting method '{}'", name)

      val jdoc = m.getJavadoc
      val desc =
        if(jdoc.isPresent) {

          val tags = jdoc.get().getBlockTags
          for(j <- 0 until tags.size()) {
            val t = tags.get(j)
            t.getTagName match {
              case "param" =>
                val ptext = process_text(t.getContent.toText)
                val pname = t.getName
                // put in the parameter javadoc
                if(pname.isPresent) {
                  param_names.put(pname.get(), ptext)
                }
                text_result.append(ptext).append(" ")
              case "return" => text_result.append(process_text(t.toText)).append(" ")
              case _ =>
            }
          }

          process_text(jdoc.get().getDescription.toText)
        } else {
          ""
        }
      text_result.append(desc)
      if(save_methods) {
        method_result.add(new MethodDesc(t, desc, name, class_desc, class_name, package_name, param_names.toSeq))
      }
    }

    def handle_constructor(cons : ConstructorDeclaration, class_desc : String, class_name : String, package_name : String) : Unit = {
      val (t, param_names) = {
        val ret_t = cons.getNameAsString
        val params = cons.getParameters

        val param_types = mutable.ArrayBuffer.empty[String]
        val param_names = mutable.Map.empty[String, String]
        for(i <- 0 until params.size()) {
          param_types += params.get(i).getType.asString()
          param_names.put(params.get(i).getNameAsString, "")
        }
        (FuncType(param_types, ret_t), param_names)
      }

      val name = cons.getNameAsString
      this.logger.trace("Extracting method '{}'", name)

      val jdoc = cons.getJavadoc
      val desc =
        if(jdoc.isPresent) {

          val tags = jdoc.get().getBlockTags
          for(j <- 0 until tags.size()) {
            val t = tags.get(j)
            t.getTagName match {
              case "param" =>
                val ptext = process_text(t.getContent.toText)
                val pname = t.getName
                // put in the parameter javadoc
                if(pname.isPresent) {
                  param_names.put(pname.get(), ptext)
                }
                text_result.append(ptext).append(" ")
              case "return" => text_result.append(process_text(t.toText)).append(" ")
              case _ =>
            }
          }

          process_text(jdoc.get().getDescription.toText)
        } else {
          ""
        }
      text_result.append(desc)
      if(save_methods) {
        method_result.add(new MethodDesc(t, desc, name, class_desc, class_name, package_name, param_names.toSeq))
      }
    }

    val all_files = Utils.tree(new File(dir))
    all_files
      .filter(_.getName.endsWith(".java"))
      .foreach(f => {
        this.logger.info("Analyzing '{}'", f.getAbsolutePath)
        val cu = JavaParser.parse(f)

        val types = cu.getTypes
        val package_name = {
          val pd = cu.getPackageDeclaration
          if(pd.isPresent) {
            pd.get().getNameAsString
          } else {
            "default"
          }
        }

        // for each class in the compilation unit
        for(i <- 0 until types.size()) {
          // get the class description
          types.get(i) match {
            case clz : ClassOrInterfaceDeclaration =>
              val clz_jdoc = clz.getJavadoc
              val clz_desc =
                if(clz_jdoc.isPresent) {
                  // add the text for the word2vec model
                  // we only need the description for classes
                  val d = process_text(clz_jdoc.get().getDescription.toText)
                  text_result.append(d).append(" ")
                  d
                } else {
                  ""
                }

              val class_name = clz.getNameAsString

              val methods = clz.getMethods
              for(j <- 0 until methods.size()) {
                val m = methods.get(j)
                handle_method(m, clz_desc, class_name, package_name)
              }

              val cons_list = clz.getConstructors
              for(j <- 0 until cons_list.size()) {
                handle_constructor(cons_list.get(j), clz_desc, class_name, package_name)
              }
            case _ =>
          }
        }

      })


    (text_result.toString, method_result)
  }

  /**
    * Get all the text and method descriptions from a directory containing java source files
    */
  def build_model() : (Word2Vec, MethodSet) = {
    val methods = new MethodSet
    val all_text = new StringBuilder()

    val text_file = new File(JavadocMapper.TEXT_FILE)
    val methods_file = new File(JavadocMapper.METHODS_FILE)
    val model_file = new File(AppConfig.TranslationConfig.word2vec_filename)

    if(!text_file.exists() || !methods_file.exists()) {
      // get methods
      this.logger.info("Analyzing source directory {} for text", this.src_dir_path)
      val (new_text, _) = this.inspect_srcdir(this.src_dir_path, save_methods = false)
      this.logger.info("Text length {}", new_text.length)

      this.logger.info("Analyzing source directory {} for gathering methods and text", this.src_dir_path)
      val (more_text, new_methods) = this.inspect_srcdir(this.method_path, save_methods = true)
      this.logger.info("Text length {}. {} methods", more_text.length, new_methods.size)

      methods.add(new_methods)
      all_text.append(new_text)
      all_text.append(more_text)
      this.logger.info("Got {} methods", new_methods.size)

      // write text
      {
        val writer = new PrintWriter(text_file.getAbsolutePath)

        // make sure one sentence per line
        val p = new StanfordSentenceProcessor
        val sentences = p.detect_sentences(all_text.toString)
        for(i <- 0 until sentences.size()) {
          writer.println(sentences.get(i).replace("\n", " ").replace("\r\n", " "))
        }

        writer.close()
        this.logger.info("Wrote {} lines to {}", sentences.size(), JavadocMapper.TEXT_FILE)
      }

      // write methods
      {
        methods.save(JavadocMapper.METHODS_FILE)
        this.logger.info("Wrote methods to {}", JavadocMapper.METHODS_FILE)
      }
    } else {
      val all_methods = MethodSet.load(JavadocMapper.METHODS_FILE)
      methods.add(all_methods)
      this.logger.info("Read {} methods from {}", methods.size, JavadocMapper.METHODS_FILE)
    }


    this.logger.info("Finished writing text to {}", JavadocMapper.TEXT_FILE)

    val model =
      if(!model_file.exists()) {
        this.logger.info("Training a Word2Vec model")
        // val sentenceIterator = new FileSentenceIterator(text_file)
        val sentenceIterator = new LineSentenceIterator(text_file)
        // val sentenceIterator = UimaSentenceIterator.createWithPath(text_file.getAbsolutePath)
        val tokenizerFactory = new DefaultTokenizerFactory
        tokenizerFactory.setTokenPreProcessor(JavadocMapper.BestTokenizer)
        // val tokenizerFactory = new UimaTokenizerFactory()
        // tokenizerFactory.setTokenPreProcessor(new StemmingPreprocessor)

        val vec = new Word2Vec.Builder()
          .stopWords(StopWords.getStopWords)
          .minWordFrequency(5)
          .layerSize(500)
          .windowSize(5)
          .seed(Random.nextInt())
          .epochs(AppConfig.TranslationConfig.word2vec_epoch)
          .elementsLearningAlgorithm(new CBOW[VocabWord])
          .iterate(sentenceIterator)
          .tokenizerFactory(tokenizerFactory).build
        vec.fit()
        this.logger.info("Saving the word2vec model...")
        this.save_model(vec)
        vec
      } else {
        this.logger.info("Loading a word2vec model...")
        this.load_model()
      }


    (model, methods)
  }

  /**
    * Calculate the distance between two word lists. 1.0 means they are different and 0 means they are the same
    */
  private def words_dist(list1 : Seq[String], list2 : Seq[String]) : Double = {
    if(list1.size < list2.size) {
      return words_dist(list2, list1)
    }

    val l1 = list1.filter(!JavadocMapper.STOP_WORDS.contains(_))
    val l2 = list2.filter(!JavadocMapper.STOP_WORDS.contains(_))

    /*
    this.logger.debug("List 1 ==================")
    list1.foreach(w => this.logger.debug("'{}'", w))

    this.logger.debug("List 2 ==================")
    list2.foreach(w => this.logger.debug("'{}'", w))
    */

    if(l1.isEmpty) { return 0.0 }
    if(l2.isEmpty) { return 1.0 }

    val mat = Array.fill(l1.length, l2.length)(0.0)
    for(i <- l1.indices) {
      val w1 = l1(i)
      for(j <- l2.indices) {
        val w2 = l2(j)
        val s = this.word_sim(w1, w2)
        mat(i)(j) =
          if(s.isNaN) {
            1.0
          } else {
            1.0 - s
          }
        this.logger.trace("{} - {} : {}", w1, w2, mat(i)(j))
      }
    }

    val algo = new HungarianAlgorithm(mat)
    val mapping = algo.execute()
    var dist = 0.0
    this.logger.debug("Mapping result:")
    for(i <- 0 until mapping.length) {
      val w1 = l1(i)
      if(mapping(i) != -1) {
        val w2 = l2(mapping(i))
        val d = mat(i)(mapping(i))
        this.logger.debug("{} - {} : {}", w1, w2, d)
        dist += d
      } else {
        dist += 1.0
      }
    }
    dist /= mapping.length.toDouble
    this.logger.debug("dist: " + dist)
    dist
  }

  private def embedding_dist(list1: Seq[(String, Array[Double])], list2: Seq[(String, Array[Double])]) : Double = {
    if(list1.size < list2.size) {
      return embedding_dist(list2, list1)
    }
    val l1 = list1
    val l2 = list2

    /*
    this.logger.debug("List 1 ==================")
    list1.foreach(w => this.logger.debug("'{}'", w))

    this.logger.debug("List 2 ==================")
    list2.foreach(w => this.logger.debug("'{}'", w))
    */

    if(l1.isEmpty) { return 0.0 }
    if(l2.isEmpty) { return 1.0 }

    val mat = Array.fill(l1.length, l2.length)(0.0)
    for(i <- l1.indices) {
      val w1 = l1(i)
      for(j <- l2.indices) {
        val w2 = l2(j)
        val s = Utils.CosineSimilarity.cosineSimilarity(w1._2, w2._2)
        mat(i)(j) =
          if(s.isNaN) {
            1.0
          } else {
            1.0 - s
          }
        this.logger.trace("{} - {} : {}", w1._1, w2._1, mat(i)(j))
      }
    }

    val algo = new HungarianAlgorithm(mat)
    val mapping = algo.execute()
    var dist = 0.0
    this.logger.debug("Mapping result:")
    for(i <- 0 until mapping.length) {
      val w1 = l1(i)
      if(mapping(i) != -1) {
        val w2 = l2(mapping(i))
        val d = mat(i)(mapping(i))
        this.logger.debug("{} - {} : {}", w1._1, w2._1, d)
        dist += d
      } else {
        this.logger.debug("{} - NULL : 1.0", w1._1)
        dist += 1.0
      }
    }
    dist /= mapping.length.toDouble
    this.logger.debug("dist: " + dist)
    dist

  }

  /**
    * calculate the distance between two method descriptions.
    * 1.0 means different and 0.0 means the same
    */
  private def desc_dist(desc1 : String, desc2 : String, bert : Boolean = true) : Double = {
    if(AppConfig.TranslationConfig.use_bert && bert) {
      val s1 = desc1.replace("\n", " ").trim
      val s2 = desc2.replace("\n", " ").trim
      this.logger.debug("SENTENCE: {}", s1)
      this.logger.debug("SENTENCE: {}", s2)

      // get the original embeddings
      if(!this.bert_embeddings.contains(s1) || !this.bert_embeddings.contains(s2)) {
        this.logger.warn("Fall back to use word embeddings.")
        return desc_dist(desc1, desc2, bert = false)
      }
      val e1 = this.bert_embeddings(s1)
      val e2 = this.bert_embeddings(s2)

      // filter out the word embeddings we don't want
      val valid_e1 = e1.filter(w => !w._1.replaceAll("\\p{Punct}", "").isEmpty && !JavadocMapper.STOP_WORDS.contains(w._1))
      val valid_e2 = e2.filter(w => !w._1.replaceAll("\\p{Punct}", "").isEmpty && !JavadocMapper.STOP_WORDS.contains(w._1))
      val d = embedding_dist(valid_e1.toSeq, valid_e2.toSeq)
      d
    } else {
      val list1 = desc1.split("\\s+").map(_.toLowerCase.replaceAll("\\p{Punct}", "").trim).filter(!_.isEmpty)
      val list2 = desc2.split("\\s+").map(_.toLowerCase.replaceAll("\\p{Punct}", "").trim).filter(!_.isEmpty)
      words_dist(list1, list2)
    }
  }

  /**
    * Break up a Camal case method name into a list of words
    * For example : isActive -> [is, active]
    */
  private def break_method_name(name : String) : Seq[String] = {
    val builder = new StringBuilder
    for(i <- 0 until name.length) {
      if(Character.isUpperCase(name.charAt(i))) {
        builder.append(",")
      }
      builder.append(Character.toLowerCase(name.charAt(i)))
    }
    builder.toString().split(",")
  }

  val class_desc_dist_cache : mutable.Map[(String, String), Double] = mutable.Map.empty[(String, String), Double]

  /**
    * get top-k similar methods under the given package and class
    * @param desc- we want to find a seq of methods similar to this method.
    */
  def query(desc : MethodDesc, pname : String, clazz_name : Option[String] = None, k : Int = 10) : Seq[(MethodDesc, Double)] = {

    /**
      * Get the first n sentences from the input text
      */
    def get_sentences(text : String, n : Int) : String = {
      if(text.isEmpty) {
        return text
      }
      val doc = new CoreDocument(text)
      JavadocMapper.nlp_pipeline.annotate(doc)

      val builder = new java.lang.StringBuilder()
      for(i <- 0 until n) {
        if(i > 0) {
          builder.append(" ")
        }

        if(i < doc.sentences().size()) {
          builder.append(doc.sentences().get(i))
        }
      }
      builder.toString
    }

    /**
      * Calculate the distance of two method descriptions.
      * Range is [0.0, 1.0] and 1.0 means they are different.
      */
    def dist(m1 : MethodDesc, m2 : MethodDesc) : Double = {
      this.logger.debug("Comparing " + m1.name + " with " + m2.name)
      this.logger.debug(m1.toString)
      this.logger.debug(m2.toString)

      val desc_d = {
        // we only get the first two sentences
        // this.logger.debug("Original desc 1: {}", m1.desc)
        // this.logger.debug("Original desc 2: {}", m2.desc)
        val desc1 = get_sentences(m1.desc, 2)
        val desc2 = get_sentences(m2.desc, 2)
        this.logger.debug("Desc 1: {}", desc1)
        this.logger.debug("Desc 2: {}", desc2)
        desc_dist(desc1, desc2)
      }
      this.logger.debug("Description distance: {}", desc_d)

      val type_dist = {
        val param_d = words_dist(m1.t.params.map(_.toLowerCase), m2.t.params.map(_.toLowerCase))
        var ret_d = this.word_sim(m1.t.ret.toLowerCase, m2.t.ret.toLowerCase)
        if(ret_d.isNaN) {
          ret_d = 1.0
        } else {
          ret_d = 1.0 - ret_d
        }
        (param_d + ret_d) / 2.0
      }
      this.logger.debug("Type distance between '{}' and '{}': {}", m1.t, m2.t, type_dist)

      val name_d = {
        // we break up the method name into a list of words
        val words1 = break_method_name(m1.name)
        val words2 = break_method_name(m2.name)
        words_dist(words1, words2)
      }
      this.logger.debug("Name distance between '{}' and '{}' : {}", m1.name, m2.name, name_d)

      val class_desc_d = {
        if(!class_desc_dist_cache.contains((m1.package_name + "." + m1.class_name, m2.package_name + "." + m2.class_name))) {
          this.logger.debug("Calculating class desc distance between '{}' and '{}'", m1.class_name, m2.class_name)
          val desc1 = get_sentences(m1.class_desc, 5)
          val desc2 = get_sentences(m2.class_desc, 5)
          this.logger.debug("Desc 1: {}", desc1)
          this.logger.debug("Desc 2: {}", desc2)
          val d = desc_dist(desc1, desc2)
          class_desc_dist_cache.put((m1.package_name + "." + m1.class_name, m2.package_name + "." + m2.class_name), d)
        }
        class_desc_dist_cache((m1.package_name + "." + m1.class_name, m2.package_name + "." + m2.class_name))
      }

      this.logger.debug("Class desc distance between '{}' and '{}' : {}", m1.name, m2.name, class_desc_d)

      val md = MethodDist(type_dist, desc_d, name_d, class_desc_d)

      val final_dist = md.t * JavadocMapper.TYPE_WT +
        md.desc * JavadocMapper.DESC_WT +
        md.name * JavadocMapper.NAME_WT +
        md.class_desc * JavadocMapper.CLASS_DESC_WT
      this.logger.debug("Final distance : {}", final_dist)
      final_dist
    }

    val relevant_methods = this.methods.get(pname, clazz_name)
    if(AppConfig.using_parllel) {
      this.logger.info("Using parallel collection...")
      relevant_methods
        .par
        .map(m => (m, dist(desc, m)))
        .seq
        .sortBy(p => p._2)
        .take(k)
    } else {
      this.logger.info("Not using parallel collection...")
      relevant_methods
        .map(m => (m, dist(desc, m)))
        .sortBy(p => p._2)
        .take(k)
    }
  }
}
