package edu.rice.pliny.apitrans

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.apitrans.hint.mapper.JavadocMapper
import edu.rice.pliny.mcts.{StanfordLemmatizer, StanfordSentenceProcessor}

object TrainJavadocMapperMain {
  val logger = Logger(this.getClass)

  def main(args : Array[String]) : Unit = {

    AppConfig.log_config
    if(args.length < 2) {
      System.err.println(
        """Usage: [text path] [methods path]

         Train a Javadoc mapper.

         text path - a directory containing all the java source files used
                     to extract text and build word2vec mode

         methods path - a directory containing all the methods we would like
                        to create mappings for
      """)
      return
    }

    val text_path = args(0)
    val method_path = args(1)
    logger.info("text path : {}", text_path)
    logger.info("method path : {}", method_path)
    // val text_path = "code_completion/src/test/resources/apitrans/mapper"
    // val method_path = "code_completion/src/test/resources/apitrans/mapper/methods"
    val mapper = new JavadocMapper(text_path, method_path)
    logger.info("Done training the model...")
  }
}
