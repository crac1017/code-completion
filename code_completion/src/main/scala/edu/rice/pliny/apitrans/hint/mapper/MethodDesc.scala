package edu.rice.pliny.apitrans.hint.mapper

import spray.json._
import MethodDescJsonProtocol._
import edu.rice.pliny.apitrans.hint.mapper.MethodDesc.FuncType

object MethodDesc {
  def from_json(value : JsValue) : MethodDesc = value.convertTo[MethodDesc]

  // function type
  case class FuncType(params : Seq[String], ret : String)
}

/**
  * Natural language representation of a method
  * @param t - return type
  * @param desc - a processed method description
  * @param name - method name
  * @param class_desc - class description
  * @param class_name - class name
  * @param package_name - package name
  * @param params - parameter name and its javadoc extracted from @param tag
  */
class MethodDesc(val t : FuncType, val desc : String, val name : String, val class_desc : String,
                 val class_name : String, val package_name : String, val params : Seq[(String, String)]) {


  override def toString : String = {
    "Method  : " + name + "\n" +
      "Type    : " + t + "\n" +
      "Desc    : " + desc + "\n" +
      "Class   : " + class_name + "\n" +
      "Package : " + package_name + "\n" +
      "Params  :\n " + this.params.foldLeft("")((acc, param) => acc + "  " + param._1 + " : " + param._2 + "\n")
  }

  def to_json : JsValue = this.toJson

  override def equals(obj: Any): Boolean = {
    obj match {
      case md : MethodDesc =>
        md.t == this.t &&
          md.desc == this.desc &&
          md.name == this.name &&
          md.class_desc == this.class_desc &&
          md.class_name == this.class_name &&
          md.package_name == this.package_name &&
          md.params.equals(this.params)
      case _ => false
    }
  }

  override def hashCode(): Int = {
    this.t.hashCode + this.desc.hashCode + this.name.hashCode +
    this.class_desc.hashCode + this.class_name.hashCode + this.package_name.hashCode + this.params.hashCode()
  }

}

