package edu.rice.pliny.apitrans

import java.io.File

import com.github.javaparser
import com.github.javaparser.JavaParser
import com.github.javaparser.ast.{CompilationUnit, ImportDeclaration, NodeList}
import com.github.javaparser.ast.body._
import com.github.javaparser.ast.comments.{Comment, LineComment}
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt.{BlockStmt, ExpressionStmt, Statement, TryStmt}
import com.github.javaparser.ast.visitor.VoidVisitorAdapter
import com.typesafe.scalalogging.Logger
import edu.rice.cs.caper.bayou.application.api_synthesis_server.{ApiSynthesisLocalClient, Configuration}
import edu.rice.cs.caper.bayou.core.bayou_services_client.api_synthesis.ApiSynthesisClient
import edu.rice.pliny.{AppConfig, Utils}
import edu.rice.pliny.apitrans.APITrans.{Env, TypeBinding}
import edu.rice.pliny.apitrans.hint.mapper.MethodDesc.FuncType
import edu.rice.pliny.apitrans.hint.mapper.{JavadocMapper, MapperCache, MethodDesc}
import edu.rice.pliny.language.java._
import edu.rice.pliny.mcts.HungarianAlgorithm
import javaslang.control.Failure.NonFatal
import org.apache.commons.lang3.ArrayUtils
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.EndingPreProcessor
import smile.nlp.stemmer.PorterStemmer

import scala.collection.JavaConverters._
import scala.collection.mutable

object APITransNL {

  // number of API translation candidate for each source API call
  val API_TRANSLATE_LIMIT = 5

  val METHOD_TYPES = Map(
    "com.sun.net.httpserver.HttpServer.createContext" -> FuncType(Vector("String", "HttpHandler"), "HttpContext"),
    "com.opencsv.CSVWriter.writeAll" -> FuncType(Vector("ResultSet", "boolean"), "int"),
    "okhttp3.Headers.of" -> FuncType(Vector("String"), "Headers"),
    "org.glassfish.grizzly.http.server.HttpServer.createSimpleServer" -> FuncType(Vector(), "HttpServer"),
    "com.sun.net.httpserver.HttpServer.createSimpleServer" -> FuncType(Vector(), "HttpServer"),
    "com.sun.net.httpserver.HttpServer.getServerConfiguration" -> FuncType(Vector(), "ServerConfiguration"),
    // "smile.classification.LogisticRegression.LogisticRegression" -> FuncType(Vector(), "LogisticRegression"),
    "spoon.reflect.factory.FactoryImpl.Code" -> FuncType(Vector(), "CodeFactory"),
    "spoon.reflect.factory.CodeFactory.createInvocation" -> FuncType(Vector("CtExpression<?>", "CtExecutableReference<T>", "CtExpression<?>"), "CtInvocation<T>"),
    "spoon.reflect.factory.CodeFactory.createTypeAccess" -> FuncType(Vector("CtTypeReference<T>"), "CtTypeAccess"),
    "jsat.io.CSV.readR" -> FuncType(Vector("int", "Path", "int", "Set<Integer>"), "RegressionDataSet"),
    "com.aliasi.hmm.HmmDecoder.tag" -> FuncType(Vector("List<String>"), "Tagging<String>"),
    "com.aliasi.sentences.SentenceModel.boundaryIndices" -> FuncType(Vector("String[]", "String[]"), "int[]"),
    "smile.classification.LogisticRegression.Trainer.train" -> FuncType(Vector("double[][]", "double[]"), "LogisticRegression"),
    "smile.regression.RidgeRegression.Trainer.train" -> FuncType(Vector("double[][]", "double[]"), "RidgeRegression"),
    "smile.classification.NeuralNetwork.Trainer.train" -> FuncType(Vector("double[][]", "double[]"), "NeuralNetwork"),
    "smile.classification.LogisticRegression.predict" -> FuncType(Vector("double[]"), "int"),
    "smile.classification.NeuralNetwork.predict" -> FuncType(Vector("double[]"), "int"),
    "smile.regression.RidgeRegression.predict" -> FuncType(Vector("double[]"), "double"),
    // "com.google.crypto.tink.KeysetHandle.generateNew" -> FuncType(Vector("KeyTemplate"), "KeysetHandle"),
    "com.itextpdf.text.pdf.parser.PdfTextExtractor.getTextFromPage" -> FuncType(Vector("PdfReader", "int"), "String"),
    "org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart.setJaxbElement" -> FuncType(Vector("E"), "void")
  )

  val logger = Logger(this.getClass)

  /**
    * Extract a list of fully qualified method names from given block of code
    */
  def extract_methods(prog : JavaDraft, block : BlockStmt) : Seq[(String, MethodDesc.FuncType, Expression)] = {
    val result = mutable.ArrayBuffer.empty[(String, MethodDesc.FuncType, Expression)]

    def get_scope(expr : Expression) : NameExpr = {
      expr match {
        case mce : MethodCallExpr if mce.getScope.isPresent => get_scope(mce.getScope.get())
        case faccess : FieldAccessExpr => get_scope(faccess.getScope)
        case ae : AssignExpr => get_scope(ae.getTarget)
        case ee : EnclosedExpr => get_scope(ee.getInner)
        case name : NameExpr => name
      }
    }

    val visitor = new VoidVisitorAdapter[Any] {
      override def visit(node : MethodCallExpr, args : Any) : Unit = {
        logger.debug("Getting declaration for API call: {}", node.getNameAsString)
        val children = node.getChildNodes
        for(i <- 0 until children.size()) {
          children.get(i).accept(this, args)
        }

        if(node.getScope.isPresent) {
          val scope_expr = get_scope(node)
          val symbol = prog.type_solver.solve(scope_expr)
          logger.debug("Scope type {} : {}", scope_expr.toString, symbol)
          if(symbol.isSolved) {
            val t = symbol.getCorrespondingDeclaration.getType
            val pname = t.describe()
            logger.debug("package : {}", pname)

            val r = pname + "." + node.getNameAsString
            val ftype = {
              if(APITransNL.METHOD_TYPES.contains(r)) {
                logger.warn("Using hacked method type for {}", r)
                APITransNL.METHOD_TYPES(r)
              } else {
                val decl = prog.type_solver.solve(node).getCorrespondingDeclaration
                val sig = decl.getSignature
                val ret_type = decl.getReturnType.describe().split("\\.").last
                val param_str = sig.substring(sig.indexOf('(') + 1, sig.length - 1).trim
                val params =
                  if(param_str.isEmpty) {
                    Vector.empty[String]
                  } else {
                    param_str.split(",").map(p => p.split("\\.").last.trim).toVector
                  }
                FuncType(params, ret_type)
              }
              /*
              val clz = Class.forName(pname, true, prog.class_loader)
              val method = clz.getMethod(node.getNameAsString)
              val sftype = JavaUtils.get_functype(method, prog.type_solver)
              val param_types = sftype.params.map(_.describe().split("\\.").last)
              val ret_type = sftype.ret.describe().split("\\.").last
              */
            }
            result += ((r, ftype, node))
            logger.debug("Adding method {} : {}", r, ftype)
          } else {
            // if this is not solved, probably this is a static function and we should look at the imports
            val imp_list = prog.compilation_unit.getImports
            for(i <- 0 until imp_list.size()) {
              val imp = imp_list.get(i)
              logger.debug("Import : {}", imp.toString)
              // if(imp.getNameAsString.endsWith(scope_expr.toString) && imp.getNameAsString.startsWith(pkg_name)) {
              if(imp.getNameAsString.endsWith(scope_expr.toString)) {
                val r = imp.getNameAsString + "." + node.getNameAsString
                val ftype = {
                  if(APITransNL.METHOD_TYPES.contains(r)) {
                    logger.warn("Using hacked method type for {}", r)
                    APITransNL.METHOD_TYPES(r)
                  } else {
                    val decl = prog.type_solver.solve(node).getCorrespondingDeclaration
                    val sig = decl.getSignature
                    val ret_type = decl.getReturnType.describe().split("\\.").last
                    val param_str = sig.substring(sig.indexOf('(') + 1, sig.length - 1).trim
                    val params =
                      if(param_str.isEmpty) {
                        Vector.empty[String]
                      } else {
                        param_str.split(",").map(p => p.split("\\.").last.trim).toVector
                      }

                    /*
                    val clz = Class.forName(imp.getNameAsString, true, prog.class_loader)
                    val method = clz.getMethod(node.getNameAsString)
                    val sftype = JavaUtils.get_functype(method, prog.type_solver)
                    val param_types = sftype.params.map(_.describe().split("\\.").last)
                    val ret_type = sftype.ret.describe().split("\\.").last
                    */
                    FuncType(params, ret_type)
                  }
                }
                result += ((r, ftype, node))
                logger.debug("Adding import {} : {}", r, ftype)
                return
              }
            }
          }
        }
      }


      override def visit(node : ObjectCreationExpr, args : Any) : Unit = {
        logger.debug("Getting declaration for constructor call: {}", node.getType.asString())
        val children = node.getChildNodes
        for(i <- 0 until children.size()) {
          children.get(i).accept(this, args)
        }

        val imp_list = prog.compilation_unit.getImports
        for(i <- 0 until imp_list.size()) {
          val imp = imp_list.get(i)
          logger.debug("Import : {}", imp.toString)
          // if(imp.getNameAsString.endsWith(scope_expr.toString) && imp.getNameAsString.startsWith(pkg_name)) {
          if(imp.getNameAsString.endsWith(node.getType.asString())) {
            val r = imp.getNameAsString + "." + node.getType.asString()
            val ftype = {
              if(APITransNL.METHOD_TYPES.contains(r)) {
                logger.warn("Using hacked method type for {}", r)
                APITransNL.METHOD_TYPES(r)
              } else {
                val decl = prog.type_solver.solve(node).getCorrespondingDeclaration
                val sig = decl.getSignature
                val ret_type = node.getType.asString()
                val param_str = sig.substring(sig.indexOf('(') + 1, sig.length - 1).trim
                val params =
                  if(param_str.isEmpty) {
                    Vector.empty[String]
                  } else {
                    param_str.split(",").map(p => p.split("\\.").last.trim).toVector
                  }
                FuncType(params, ret_type)
              }
            }
            result += ((r, ftype, node))
            logger.debug("Adding import {} : {}", r, ftype)
            return
          }
        }
      }
    }

    block.accept(visitor, null)
    result
  }

  /**
    * Split a fully qualified method name into package name, class name and method name
    */
  def split(qname : String) : (String, String, String) = {
    val nlist = qname.split("\\.")
    val class_name = nlist(nlist.size - 2)
    val method_name = nlist(nlist.size - 1)
    val pnamelist = ArrayUtils.subarray(nlist, 0, nlist.length - 2)
    val pname = Utils.string_join(pnamelist, ".")
    (pname, class_name, method_name)
  }

  def query(method_desc : MethodDesc, mapper : JavadocMapper, dest_pkg : String, k : Int = 10) : Seq[(MethodDesc, Double)] = {
    logger.info("query methods =========\n{}", method_desc)

    // check the cache file
    val sim_methods =
      if(AppConfig.TranslationConfig.cache_use) {
        this.logger.info("Using cache to translate methods...")
        val cache = new MapperCache(AppConfig.TranslationConfig.cache_filename, mapper)
        cache.query(method_desc, dest_pkg, k = k)
      } else {
        mapper.query(method_desc, dest_pkg, k = k)
      }
    logger.info("{} similar methods =========", sim_methods.size)
    sim_methods.foreach(m => logger.info("{}", m))
    sim_methods
  }

  /*
  /**
    * Clean up the block produced by Bayou, especially remove enclosed object creations like
    * (a1 = O1()).m1()
    * to
    * a1 = O1()
    * a1.m1()
    */
  def clean_bayou_output(stmt : Statement) : Statement = {
    stmt match {
      case bs : BlockStmt =>
        val new_block = new BlockStmt()
        for(i <- 0 until oblock.getStatements.size()) {
          val stmt = oblock.getStatement(i)
        }
        new_block
      case
    }
  }
  */
}

class APITransNL(override val task_name : String,
                 override val interpreter : JavaFastInterpreter,
                 override val jar : JarAnalyzer) extends APITrans(task_name, interpreter, jar) {

  val logger = Logger(this.getClass)
  logger.info("Loading mapper...")
  val mapper = JavadocMapper.load_mapper
  if(mapper.isEmpty) {
    logger.error("Failed to load mapper from disk.")
    System.exit(1)
  }

  /**
    * Refactor the input program which uses API calls into another program
    * that uses another set of API calls. Here, we assume that the API calls happen in a basic block.
    * ret_val is a pair of (variable name, variable type) representing the returning type of the API
    * sequence and also the receiver variable name.
    */
  override def refactor(prog : JavaDraft, ret_val : (String, String)) : Seq[(BlockStmt, JavaDraft)] = {
    val block = APITrans.find_refactoring_block(prog)
    if(block.isEmpty) {
      this.logger.warn("Cannot find refactoring block.")
      return Seq.empty[(BlockStmt, JavaDraft)]
    }
    val to_pkg = block.get._2

    this.logger.info("Refactoring block =========\n{}\n", block.get.toString)
    this.logger.info("To package {}", to_pkg)

    val env = APITrans.construct_env(prog, block.get._1)
    /*
    val ret_tb =
      if(ret_val._2 == "void") {
        JavaAPITrans.VOID_PLACEHOLDER
      } else {
        TypeBinding(ret_val._1, JavaUtils.str_to_type(ret_val._2, prog.type_solver))
      }
      */


    /*
     * main refactoring algorithm
     */

    // extract the methods from the block
    val methods = APITransNL.extract_methods(prog, block.get._1)

    // map ONLY the methods
    val mapped_methods = mutable.ArrayBuffer.empty[(MethodDesc, Seq[(MethodDesc, Double)])]

    methods.foreach(m => {
      val (pname : String, cname : String, mname : String) = APITransNL.split(m._1)
      logger.info("pname : {}\tcname : {}\tmname : {}\tftype: {}", pname, cname, mname, m._2)

      val method = this.mapper.get.methods.get_method(pname, cname, mname, m._2)
      if(method.isDefined) {
        if(cname == mname) {
          mapped_methods += ((method.get, Seq.empty[(MethodDesc, Double)]))
        } else {
          mapped_methods += ((method.get, APITransNL.query(method.get, this.mapper.get, block.get._2, APITransNL.API_TRANSLATE_LIMIT)))
        }
      } else {
        logger.warn("No translated methods.")
      }
    })

    val import_list = prog.compilation_unit.getImports

    // compilation unit for enclosing the method containing the evidence
    val cu = {
      // class decl
      val class_decl = new ClassOrInterfaceDeclaration()
      class_decl.setName("BayouInput")

      val c = new CompilationUnit()
      c.addType(class_decl)

      // add imports
      for(i <- 0 until import_list.size()) {
        c.addImport(import_list.get(i))
      }
      c
    }

    this.search_api_seq(mapped_methods, cu, env, prog, ret_val)
  }

  /**
    * Search all possible permutation of methods.
    * @param cu - the skeleton compilation unit that has class used to enclose the generated method
    * @param env - containing all the defined names and used to construct the method parameters of the input program
    */
  def search_api_seq(mapped_methods: Seq[(MethodDesc, Seq[(MethodDesc, Double)])], cu : CompilationUnit, env : Env,
                     orig_prog : JavaDraft, ret_val : (String, String)) : Seq[(BlockStmt, JavaDraft)] = {

    this.logger.debug("Translated methods =============")
    mapped_methods.foreach(mapping => {
      this.logger.debug("Source method: {}", mapping._1)
      this.logger.debug("Target methods:{}\n", mapping._2.foldLeft("")((acc, s) => acc + "\n" + s._1.name))
    })


    // get variable semantics from the original program
    val semantics = mutable.Map.empty[String, SemanticModel]
    val visitor = new VoidVisitorAdapter[Any] {
      override def visit(mce : MethodCallExpr, args : Any) : Unit = {
        mapped_methods.foreach(mmethod => {
          if(mmethod._1.name == mce.getNameAsString) {
            logger.debug("Getting semantic information from method {}", mce.toString)
            mmethod._1.params.zipWithIndex.filter(p => mce.getArgument(p._2).isInstanceOf[NameExpr]).foreach(p => {
              val arg_name = mce.getArgument(p._2).asInstanceOf[NameExpr].getNameAsString
              val t = JavaUtils.str_to_syntax_type(mmethod._1.t.params(p._2))
              if(semantics.contains(arg_name)) {
                logger.warn("Duplicate semantic. Old : {}\tNew : {}", semantics(arg_name), p._1._1)
              }
              semantics.put(arg_name, (arg_name, Some(p._1._1), t))
            })
          }
        })
      }

      override def visit(cons : ObjectCreationExpr, args : Any) : Unit = {
        mapped_methods.foreach(mmethod => {
          if(mmethod._1.name == cons.getType.asString()) {
            logger.debug("Getting semantic information from object creation {}", cons.toString)
            mmethod._1.params.zipWithIndex.filter(p => cons.getArgument(p._2).isInstanceOf[NameExpr]).foreach(p => {
              val arg_name = cons.getArgument(p._2).asInstanceOf[NameExpr].getNameAsString
              val t = JavaUtils.str_to_syntax_type(mmethod._1.t.params(p._2))
              if(semantics.contains(arg_name)) {
                logger.warn("Duplicate semantic. Old : {}\tNew : {}", semantics(arg_name), p._1._1)
              }
              semantics.put(arg_name, (arg_name, Some(p._1._1), t))
            })
          }
        })
      }
    }
    APITrans.find_refactoring_block(orig_prog).get._1.accept(visitor, null)
    semantics.foreach(s => logger.debug("Semantic for {} : {}", s._1, s._2))

    def bayou_syn(bayou_inseq : Seq[MethodDesc]) : Seq[(BlockStmt, javaparser.ast.`type`.Type, JavaDraft)] = {
      // evidence block
      val eblock = {
        val builder = new java.lang.StringBuilder()
        builder.append("/")
        this.task_name match {
          case "nlp_token" => bayou_inseq.tail.foreach(desc => builder.append("call:").append(desc.name).append(" "))
          case _ =>
            bayou_inseq.foreach(desc => {

              // precondition hack
              this.task_name match {
                case "email_check" if desc.name == "hasNewMessages" =>
                case "email_delete" if desc.name == "hasNewMessages" =>
                case "graphics" if desc.name == "write" =>
                case _ =>
                  builder.append("call:")
                  builder.append(desc.name)
                  builder.append(" ")
              }


              // post condition hack, add stuffs at the end
              this.task_name match {
                case "csv" => builder.append("type:CSVReader ")
                case _ =>
              }
            })
            this.task_name match {
              case "htm" => builder.append("call:first")
              case "ml_classification" => builder.append("call:train")
              case "ml_regression" => builder.append("call:train")
              case "ml_neural" => builder.append("call:train")
              case _ =>
            }
        }

        val evidence = new LineComment(builder.toString)
        val block = new BlockStmt
        block.addOrphanComment(evidence)
        block
      }

      // parameters
      val param_list = new NodeList[Parameter]()
      env.foreach(binding => {
        binding._1 match {
          case ne : NameExpr =>
            param_list.add(new Parameter(JavaUtils.str_to_syntax_type(binding._2.describe()), ne.getNameAsString))
        }
      })

      // body block
      val body = new BlockStmt
      body.addStatement(eblock)

      // method decl
      val method_decl = new MethodDeclaration()
      method_decl.setName("bayou_input")
      method_decl.setParameters(param_list)
      method_decl.setPublic(true)
      method_decl.setBody(body)
      method_decl.setType(new javaparser.ast.`type`.VoidType())

      // enclose the method with cu
      val class_decl = cu.getType(0)
      if(class_decl.getMembers.size() > 0) {
        // we already have a method, set to a new one
        class_decl.setMember(0, method_decl)
      } else {
        // add in the new method
        class_decl.addMember(method_decl)
      }

      this.logger.debug("Bayou Input ============\n{}", cu)

      // val syn_results = mutable.ArrayBuffer.empty[String]

      val syn_results =
      {
        val client = new ApiSynthesisClient("ganymede.cs.rice.edu", Configuration.RequestListenPort.AsInt)
        // new ApiSynthesisClient("localhost", Configuration.RequestListenPort.AsInt);
        // ApiSynthesisClient client = new ApiSynthesisClient("localhost", Configuration.RequestListenPort.AsInt);
        // ApiSynthesisClient client = new ApiSynthesisClient("ganymede.cs.rice.edu", Configuration.RequestListenPort.AsInt);
        try {
          client.synthesise(cu.toString, 10)
        } catch {
          case _ : Throwable =>
            this.logger.warn("Failed to synthesize call sequence")
            new java.util.ArrayList[String]()
        }
      }.asScala

        // preprocess the output, mainly removing some code that will make our program crash
        .map(out => {
        /*
        if(out.contains("FilePermission")) {
          println("FOO")
        }
        */
        out.replace("import net.schmizz.sshj.xfer.FilePermission;\n", "")
          .replace("import smile.classification.LogisticRegression;\n", "")
          .replace("import smile.regression.RidgeRegression;\n", "")
      })
        .filter(out => {
          !out.contains("new Element") &&
          !out.contains("FilterChainContext")
        })

      val result = mutable.ArrayBuffer.empty[(BlockStmt, javaparser.ast.`type`.Type, JavaDraft)]
      // extract the API sequences
      for(i <- syn_results.indices) {
        val r = syn_results(i)

        this.logger.debug("Bayou Output ==============\n{}", r)
        val r_prog = JavaUtils.parse_src(r, None, None, None, this.interpreter.jar_paths, Seq()).get
        val r_cu = r_prog.compilation_unit
        val r_clz = r_cu.getType(0)
        val r_method = r_clz.getMember(0).asInstanceOf[MethodDeclaration]
        val r_body = r_method.getBody.get()
        val r_block = r_body.getStatement(0).asInstanceOf[BlockStmt]
        val r_env = {
          val env_result = mutable.Map.empty[String, javaparser.ast.`type`.Type]
          val env_visitor = new VoidVisitorAdapter[Any] {
            override def visit(vdor : VariableDeclarator, arg : Any) : Unit = {
              env_result.put(vdor.getNameAsString, vdor.getType)
            }
          }

          r_block.accept(env_visitor, null)
          env_result
        }

        def get_last_type(block : BlockStmt, has_return : Boolean = false)  : Option[javaparser.ast.`type`.Type] = {
          // this.logger.debug("Block with return {}: {}", has_return, block.toString)
          val last_stmt = block.getStatement(block.getStatements.size() - (if(has_return) 2 else 1))
          last_stmt match {
            case ts : TryStmt => get_last_type(ts.getTryBlock)
            case es : ExpressionStmt =>
              es.getExpression match {
                case ae : AssignExpr =>
                  val name = ae.getTarget.asInstanceOf[NameExpr].getNameAsString
                  if(!r_env.contains(name)) {
                    logger.warn("Failed to find syntax type for {}", name)
                    None
                  } else {
                    Some(r_env(name))
                  }
                case _ : MethodCallExpr => Some(new javaparser.ast.`type`.VoidType())
                case _ => None
              }
            case _ => Some(new javaparser.ast.`type`.VoidType())
          }
        }

        // get the return type of the last statement
        val last_type : Option[javaparser.ast.`type`.Type] = get_last_type(r_block, true)
        this.logger.debug("Last type : {}", last_type.get)
        result += ((r_block, last_type.get, r_prog))

        // need to replace the last return statement into an assignment that assigns the result value into a variable
        // defined in the original draft program. In other words, we need to plug the synthesized code into the
        // original program

        /*
        // this is a list of candidate variable names we can use for return value
        for(i <- 0 until r_block.getStatements.size()) {
          val decls = JavaUtils.get_defs(r_block.getStatement(i))
          decls.foreach(d => {
            if(ret_val._2.endsWith(d.getType.asString())) {
              // replace the last return statement
              val cand_block = r_block.clone()
              val new_decl = new VariableDeclarator(JavaUtils.str_to_syntax_type(ret_val._2), ret_val._1, new NameExpr(d.getName))
              cand_block.setStatement(cand_block.getStatements.size() - 1, new ExpressionStmt(new VariableDeclarationExpr(new_decl)))

              this.logger.debug("Candidate API sequence ================\n{}", cand_block)
              result += cand_block
            }
          })
        }
      */
      }
      result
    }

    def is_correct(new_seq: Seq[Statement]) : Boolean = {
      val target_block = APITrans.find_refactoring_block(orig_prog)
      val new_method = APITrans.inject(orig_prog, target_block.get._1 ,new_seq)
      val test = orig_prog.get_test.get
      val result = test.accept(interpreter, new_method.toString)
      // this.logger.info("Result: {}", result._1)
      // this.logger.info("Runtime: {}", result._2)
      // this.logger.info("Output =================\n{}\n", result._3)

      if(result._1) {
        this.logger.info("Solution ===================\n{}\n", new_method.toString)
        true
      } else {
        this.logger.debug("Incorrect solution =================\n{}\n{}\n", new_method.toString, result._3)
        // return None
        false
      }
    }

    val result = mutable.ArrayBuffer.empty[(BlockStmt, JavaDraft)]

    val used_methods = mutable.Set.empty[String]

    // for now the semantic model is just the original variable name, javadoc var name and its type
    type SemanticModel = (String, Option[String], javaparser.ast.`type`.Type)

    /**
      * Use semantic model to match the parameters and the variables
      * @param block - original block containing the method calls
      * @param cand_args - argument positions
      */
    def match_args(block: BlockStmt,
                   cand_args : Seq[(Expression, MethodDesc, Int)]) : BlockStmt = {
      val text_proc = new EndingPreProcessor
      val vars = semantics.toSeq
      val new_block = block.clone()

      val SUBTYPE_MAP = Map(
        "MimeMessage" -> "Message"
      )
      /**
        * check if t1 <: t2
        */
      def is_subtype(t1 : String, t2 : String) : Boolean = {
        if(SUBTYPE_MAP.contains(t1) && SUBTYPE_MAP(t1) == t2) {
          true
        } else if(t2.endsWith(t1)) {
          true
        } else {
          false
        }
      }

      this.logger.debug("Candidate arguments =============\n{}",
        cand_args.foldLeft("")((acc, ca) => acc + "\n  " + ca._2.params(ca._3)._1 + " for " + ca._1.toString + " pos " + ca._3 +  " : " + ca._2.params(ca._3)._2))

      this.logger.debug("Variables for arguments =============\n{}",
        vars.foldLeft("")((acc, v) => acc + "\n  " + v._1 + " : (" + v._2._1 + ", " + v._2._2 + ", " + v._2._3.asString() + ")"))

      val cand_with_vars = cand_args.map(carg => {
        val pname = carg._2.params(carg._3)._1
        val req_t = JavaUtils.str_to_syntax_type(carg._2.t.params(carg._3))
        val vars_with_sim = vars.map(v => {
          if(!is_subtype(v._2._3.asString(), req_t.asString())) {
            (v._1, -10.0)
          } else if(v._2._2.isDefined) {
            // we have NL semantic information
            val w1 = text_proc.preProcess(pname.toLowerCase)
            val w2 = text_proc.preProcess(v._2._2.get.toLowerCase)
            val sim = this.mapper.get.model.similarity(w1, w2)
            (v._1, if(sim.isNaN) 0.0 else sim)
          } else {
            // we don't have NL semantic information but the type match
            val w1 = text_proc.preProcess(pname.toLowerCase)
            val w2 = text_proc.preProcess(v._2._1.toLowerCase())
            val sim = this.mapper.get.model.similarity(w1, w2)
            (v._1, if(sim.isNaN) 0.0 else sim)
          }
        }).sortBy(-_._2)
        (carg, vars_with_sim)
      })

      cand_with_vars.foreach(cv => {
        this.logger.debug("Arguments match result for {} pos {} with name {} : {} ==============={}",
          cv._1._1.toString,
          cv._1._3,
          cv._1._2.params(cv._1._3)._1,
          cv._1._2.t.params(cv._1._3),
          cv._2.foldLeft("")((acc, sem) => acc + "\n  " + sem._1 + " - " + sem._2))})

      cand_with_vars.foreach(carg => {
        // put the variables into the argument positions
        carg._1._1 match {
          case mce : MethodCallExpr =>
            new_block.accept(new VoidVisitorAdapter[Any] {
              override def visit(node : MethodCallExpr, args : Any) : Unit = {
                if(node.getNameAsString == mce.getNameAsString && carg._1._3 < node.getArguments.size()) {
                  node.setArgument(carg._1._3, new NameExpr(carg._2.head._1))
                }
              }}, null)
          case oce : ObjectCreationExpr =>
            new_block.accept(new VoidVisitorAdapter[Any] {
              override def visit(node : ObjectCreationExpr, args : Any) : Unit = {
                if(node.getType.asString() == oce.getType.asString() && carg._1._3 < node.getArguments.size()) {
                  node.setArgument(carg._1._3, new NameExpr(carg._2.head._1))
                }
              }}, null)
        }
      })
      new_block
    }

    def helper(working_seq : Seq[MethodDesc], pick_index : Int) : Unit = {
      this.logger.debug("========== Working sequence:{}\n", working_seq.foldLeft("")((acc, s) => acc + "\n" + s.toString))
      this.logger.debug("========== Pick index :{}\n", pick_index)

      if(result.size >= APITrans.SEQ_LIMIT) {
        this.logger.debug("We have enough sequences.")
        return
      }

      // check the return type
      val req_type = JavaUtils.str_to_syntax_type(ret_val._2)

      if(pick_index >= mapped_methods.count(m => m._2.nonEmpty) && working_seq.nonEmpty) {
        this.logger.debug("Type requirement : {}", req_type)
        // construct a input program to bayou
        val bayou_seqs = bayou_syn(working_seq)

        this.logger.debug("foo")
        val valid_seq = bayou_seqs
          .filter(bs => {
            this.logger.debug("Sequence type: {}", bs._2)
            val eq = req_type.toString.endsWith(bs._2.toString)
            this.logger.debug("{} equals {} : {}", bs._2, req_type, eq)
            if(this.task_name == "pdf_read") {
              true
            } else {
              eq
            }
          })
          // synthesize for arguments
          .map(bs => {

          // gather all variables
          val names_env = bs._3.name_type_env
          names_env.foreach(name => {
            if(!semantics.contains(name._1)) {
              semantics.put(name._1, (name._1, None, JavaUtils.str_to_syntax_type(name._2.describe().split("\\.").last.trim)))
            }
          })

          // gather argument positions from the methods
          val cand_args = mutable.ArrayBuffer.empty[(Expression, MethodDesc, Int)]
          this.logger.debug("Extracting methods from Bayou program ========================\n{}", bs._3)
          val cand_methods =
            try {
              APITransNL.extract_methods(bs._3, bs._1)
            } catch {
              case _ : Throwable =>
                this.logger.warn("Cannot parse Bayou program")
                Seq.empty
            }

          cand_methods.foreach(cm => { logger.debug("Candidate methods for cand_args: {}", cm._1) })

          cand_methods.foreach(m => {
            val (pname : String, cname : String, mname : String) = APITransNL.split(m._1)
            logger.info("pname : {}\tcname : {}\tmname : {}\tftype: {}", pname, cname, mname, m._2)
            val method = this.mapper.get.methods.get_method(pname, cname, mname, m._2)
            if(method.isDefined) {
              m._3 match {
                case mce : MethodCallExpr =>
                  for(i <- 0 until mce.getArguments.size()) {
                    cand_args += ((mce, method.get, i))
                  }
                case oce : ObjectCreationExpr =>
                  for(i <- 0 until oce.getArguments.size()) {
                    cand_args += ((oce, method.get, i))
                  }
              }
            }
          })

          // run bipartite matching to filling the arguments
          (match_args(bs._1, cand_args), bs._2, bs._3)
        })

        valid_seq
          .foreach(bs => {
            this.logger.debug("Good sequence =============\n{}", bs._1)
          })
        // test the correctness of all the sequences
        // bayou_seqs.filter(seq => is_correct(seq.getStatements.asScala))

        valid_seq.foreach(s => {
          if(result.size < APITrans.SEQ_LIMIT) {
            result += ((s._1, s._3))
          }
        })
        return
      }

      val (_, cand_methods) = mapped_methods.filter(m => m._2.nonEmpty)(pick_index)

      cand_methods.foreach(cand => {

        logger.debug("Trying candidate method: {} : {}", cand._1.name, cand._1.t)
        if(!used_methods.contains(cand._1.name)) {
          used_methods += cand._1.name
          val new_seq = working_seq :+ cand._1
          helper(new_seq, pick_index + 1)
          used_methods -= cand._1.name

          logger.debug("Trying to ignore method: {} : {}", cand._1.name, cand._1.t)
          helper(new_seq, pick_index + 1)
        } else {
          logger.debug("Candidate method {} has been used.", cand._1.name)
          helper(working_seq, pick_index + 1)
        }
      })
    }

    helper(Seq.empty[MethodDesc], 0)
    result
  }

}

