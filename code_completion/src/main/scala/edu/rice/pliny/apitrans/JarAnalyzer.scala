package edu.rice.pliny.apitrans

import java.io.File
import java.lang.reflect.{Constructor, Method}
import java.net.URLClassLoader
import java.util.jar.JarFile

import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade
import com.github.javaparser.symbolsolver.resolution.typesolvers.{CombinedTypeSolver, JarTypeSolver, MemoryTypeSolver, ReflectionTypeSolver}
import com.google.common.reflect.ClassPath
import edu.rice.pliny.apitrans.JarAnalyzer.{JarConstructor, JarFunc, JarMethod}
import edu.rice.pliny.language.java.{FuncType, JavaUtils}

import scala.collection.mutable

object JarAnalyzer {
  trait JarFunc
  case class JarMethod(name : String, t : FuncType, clazz : Class[_], method : Method) extends JarFunc
  case class JarConstructor(name : String, t : FuncType, clazz : Class[_], cons : Constructor[_]) extends JarFunc
}

class JarAnalyzer(val jar_paths : Seq[String]) {

  // val classes : mutable.Map[Class[_], Set[(String, FuncType)]] = this.analyze_jar(jar_paths) ++ analyze_java_lib()
  val funcs : mutable.Map[Class[_], Set[JarFunc]] = this.analyze_jar_methods(jar_paths) ++ analyze_java_methods()

  /**
    * This function analyze the java standard library and store all the classes and methods
    */
  def analyze_java_methods() : mutable.Map[Class[_], Set[JarFunc]] = {
    val loader = ClassLoader.getSystemClassLoader
    val result = mutable.Map.empty[Class[_], Set[JarFunc]]
    val type_solver: JavaParserFacade = {
      val solver = new CombinedTypeSolver()
      solver.add(new ReflectionTypeSolver())
      solver.add(new MemoryTypeSolver())
      JavaParserFacade.get(solver)
    }

    val interesting_classes = Set(
      "java.lang.Integer"
      ,"java.lang.String"
      ,"java.io.File"
      ,"java.io.FileReader"
      ,"java.io.FileOutputStream"
      ,"java.io.FileWriter"
      ,"java.io.BufferedReader"
      ,"java.util.List"
    )

    val classes = {
      val class_result = mutable.ArrayBuffer.empty[Class[_]]

      val paths = ClassPath.from(ClassLoader.getSystemClassLoader)
      val all_classes = paths.getAllClasses
      val iter = all_classes.iterator()
      while(iter.hasNext) {
        val n = iter.next()
        val c =
          if(interesting_classes.contains(n.getName)) {
            try {
              Some(n.load())
            } catch {
              case _: Throwable => None
            }
          } else {
            None
          }
        if(c.isDefined) {
          class_result += n.load()
        }
      }
      class_result
    }

    classes.foreach(clazz => {
      val method_set = mutable.Set.empty[JarFunc]
      val methods = clazz.getMethods
      for(m <- methods) {
        val func_type = JavaUtils.get_functype(m, type_solver)
        method_set += JarMethod(m.getName, func_type, clazz, m)
      }

      // constructors
      val cons = clazz.getConstructors
      for(c <- cons) {
        val func_type = JavaUtils.get_functype(c, type_solver)
        method_set += JarConstructor(c.getName, func_type, clazz, c)
      }
      result.put(clazz, method_set.toSet)
    })

    result
  }

  /**
    * This function analyze the input jar file and store all the classes and methods
    */
  def analyze_jar_methods(jar_paths : Seq[String]) : mutable.Map[Class[_], Set[JarFunc]] = {
    val jar_files = jar_paths.map(p => new File(p))
    val loader : URLClassLoader = URLClassLoader.newInstance(jar_files.map(file => file.toURI.toURL).toArray, getClass.getClassLoader)
    val result = mutable.Map.empty[Class[_], Set[JarFunc]]
    val type_solver: JavaParserFacade = {
      val solver = new CombinedTypeSolver()
      solver.add(new ReflectionTypeSolver())
      solver.add(new MemoryTypeSolver())
      jar_files.foreach(file => solver.add(new JarTypeSolver(file.getAbsolutePath)))
      JavaParserFacade.get(solver)
    }

    jar_files.foreach(file => {
      val jar = new JarFile(file)
      val enumeration = jar.entries()

      while(enumeration.hasMoreElements) {
        val entry = enumeration.nextElement()

        if(entry.getName.endsWith(".class")) {
          val func_type_set = mutable.Set.empty[JarFunc]
          val class_name = entry.getName.replace(".class", "").replace("/", ".")
          try {
            val clazz = loader.loadClass(class_name)
            val methods = clazz.getMethods
            for(m <- methods) {
              val func_type = JavaUtils.get_functype(m, type_solver)
              func_type_set += JarMethod(m.getName, func_type, clazz, m)
            }

            // constructors
            val cons = clazz.getConstructors
            for(c <- cons) {
              val func_type = JavaUtils.get_functype(c, type_solver)
              func_type_set += JarConstructor(c.getName, func_type, clazz, c)
            }
            result.put(clazz, func_type_set.toSet)
          } catch {
            case _ : Throwable =>
          }
        }
      }

    })

    result
  }
}
