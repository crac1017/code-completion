package edu.rice.pliny.apitrans
import java.io.File
import java.net.URLClassLoader
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path, Paths}
import java.nio.charset.Charset
import java.sql.DriverManager
import java.util
import java.util.{Locale, UUID}

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.body.MethodDeclaration
import com.github.javaparser.ast.expr.Expression
import com.github.javaparser.ast.stmt.BlockStmt
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.Utils
import edu.rice.pliny.language.java.{JavaDraft, JavaFastInterpreter, JavaUtils}
import org.jtwig.{JtwigModel, JtwigTemplate}
import spark.Spark._
import spray.json._

import sys.process._
import scala.util.Random
import collection.JavaConverters._
import scala.collection.mutable

object WebMain {
  val PROBLEM_PATH = "code_completion/src/main/resources/api-refactoring-web/problems"
  val PROBLEM_FILENAME = "problem.java"
  val logger = Logger(this.getClass)

  // a dir containing all the cache solutions
  val CACHE_SOLUTION_DIRNAME = "cache"

  // resources needed to run the test
  val RESOURCES_DIRNAME = "resources"

  val JARS_DIRPATH = this.PROBLEM_PATH + "/jars"

  val DB_CONN = DriverManager.getConnection(s"jdbc:sqlite:${this.PROBLEM_PATH}/study.db")

  /**
    * information about a refactoring task
    */
  case class Task(name : String, ret_val : String, ret_type : String)

  val TASK_MAP: Map[String, Task] = Map(
    "csv" -> Task("csv", "fields", "[Ljava.lang.String;"),
    "csv3" -> Task("csv3", "", "void"),

    "ftp" -> Task("ftp", "", "void"),
    "ftp_delete" -> Task("ftp_delete", "", "void"),
    "ftp_upload" -> Task("ftp_upload", "", "void"),
    "ftp_login" -> Task("ftp_login", "", "void"),
    "ftp_download" -> Task("ftp_download", "", "void"),

    "email_send" -> Task("email_send", "", "void"),
    "email_check" -> Task("email_check", "", "void"),

    "htm" -> Task("htm", "href", "java.lang.String"),
    "htm_addattr" -> Task("htm_addattr", "e", "org.jsoup.nodes.Element"),
    "htm_addnode" -> Task("htm_addnode", "e", "org.jsoup.nodes.Element"),
    "htm_rmattr" -> Task("htm_rmattr", "s1", "java.lang.String"),

    "httpserver" -> Task("httpserver", "", "void"),
    "httpget" -> Task("httpget", "text", "java.lang.String"),

    "graphics" -> Task("graphics", "", "void"),

    "ml_classification" -> Task("ml_classification", "c", "int"),
    "ml_cluster" -> Task("ml_cluster", "label", "int"),
    "ml_regression" -> Task("ml_regression", "d", "double"),
    "ml_neural" -> Task("ml_neural", "d", "double"),

    "pdf_read" -> Task("pdf_read", "", "void"),
    "secret_encrypt" -> Task("secret_encrypt", "txt", "java.lang.String"),
    "secret_decrypt" -> Task("secret_decrypt", "txt", "java.lang.String"),
  )

  val BENCH_TASK_NAME: Map[String, Map[String, String]] = Map(
    "csv" -> Map("readRow" -> "csv",
                 "writeRow" -> "csv3"),
    "ftp" -> Map("login" -> "ftp_login",
                 "upload" -> "ftp_upload",
                 "download" -> "ftp_download",
                 "delete" -> "ftp_delete",
                 "list_files" -> "ftp"),
    "htm" -> Map("add_attr" -> "htm_addattr",
                 "add_node" -> "htm_addnode",
                 "get_link" -> "htm",
                 "rm_attr" -> "htm_rmattr"),
    "ml" -> Map("classify" -> "ml_classification",
                "cluster" -> "ml_cluster",
                "regress" -> "ml_regression",
                "neural" -> "ml_neural"),
    "secret" -> Map("encrypt" -> "secret_encrypt",
                    "decrypt" -> "secret_decrypt"),
  )

  case class TaskTestInfo(code_fname : String, test_name : String)
  val TASK_TEST_INFO : Map[String, TaskTestInfo] = Map(
    "pdf_read" -> TaskTestInfo("PDFRead.java", "PDFReadTest"),
    "httpserver" -> TaskTestInfo("HServer.java", "HServerTest"),
    "httpget" -> TaskTestInfo("HTTPGet.java", "HTTPGetTest"),
    "htm" -> TaskTestInfo("HTM.java", "HTMTest"),
    "graphics" -> TaskTestInfo("GraphicsImg.java", "GraphicsImgTest"),
    "ftp" -> TaskTestInfo("FTP.java", "FTPTest"),
    "ml" -> TaskTestInfo("ML.java", "MLTest"),
    "csv" -> TaskTestInfo("CSV.java", "CSVTest")
  )

  def load_problem(task_name: String) : String =
    scala.io.Source.fromFile(this.PROBLEM_PATH + "/" + task_name + "/" + this.PROBLEM_FILENAME).mkString

  def get_filenames(dirname : String) : Seq[String] = {
    new File(dirname).listFiles().filter(_.isFile).map(f => f.getAbsolutePath)
  }

  /**
    * Since we are refactoring multiple methods in a user study session, we should get the corresponding
    * task name in the benchmark set instead of the task name from the directory. For example, we cannot use
    * "csv" for csv_read and csv_write, because we have logic hacks in the code that depends on task name.
    */
  def get_benchmark_taskname(orig_taskname : String, prog : JavaDraft) : String = {
    val block = APITrans.find_refactoring_block(prog)
    if(block.isEmpty) {
      return orig_taskname
    }

    val method_name = JavaUtils.get_enclosing_method(block.get._1).get.getNameAsString

    if(!this.BENCH_TASK_NAME.contains(orig_taskname)) {
      return orig_taskname
    }

    if(!this.BENCH_TASK_NAME(orig_taskname).contains(method_name)) {
      return orig_taskname
    }
    this.BENCH_TASK_NAME(orig_taskname)(method_name)
  }

  /**
    * get the cache synthesis result
    */
  def get_cache_result(bench_tname : String, orig_tname : String) : Option[String] = {
    val cache_fpath = s"${this.PROBLEM_PATH}/$orig_tname/${this.CACHE_SOLUTION_DIRNAME}/$bench_tname.java"
    if(new File(cache_fpath).exists()) {
      Some(scala.io.Source.fromFile(cache_fpath).mkString)
    } else {
      None
    }
  }

  /**
    * Setup test environment for this session and this task. Returns the path to the dir jail
    */
  private def setup_test_env(session_id : String, tname : String) : Path = {
    // create a temp jail
    val root = new File(s"${this.PROBLEM_PATH}/$tname/session-$session_id")
    root.mkdir()
    val test_info = this.TASK_TEST_INFO(tname)

    // copy the test file
    val test_file = Paths.get(s"${this.PROBLEM_PATH}/$tname/${test_info.test_name}.java")
    Files.copy(test_file, root.toPath.resolve(test_file.getFileName))

    // copy all the necessary resources
    {
      val resources_path = s"${this.PROBLEM_PATH}/$tname/${this.RESOURCES_DIRNAME}"
      val resources_dir = new File(resources_path)
      if(resources_dir.exists()) {
        val files = new File(resources_path).listFiles()
        files.foreach(f => Files.copy(f.toPath, root.toPath.resolve(f.getName)))
      }
    }

    root.toPath
  }

  /**
    * Compile the code and return (status code, stdout, stderr)
    */
  private def compile_code(code : String, jail : Path, tname : String) : (Int, String, String) = {
    val test_info = this.TASK_TEST_INFO(tname)
    val jar_dir = Paths.get(this.JARS_DIRPATH)
    val jar_paths = s".:${jar_dir.toAbsolutePath.resolve("*")}"

    val compile_cmd = s"javac -cp $jar_paths ${test_info.code_fname} ${test_info.test_name}.java"
    val stdout = new java.lang.StringBuilder
    val stderr = new java.lang.StringBuilder
    val status =
      Process(compile_cmd, jail.toFile) !
        ProcessLogger(fout => stdout.append(fout + "\n"), ferr => stderr.append(ferr + "\n"))

    (status, stdout.toString, stderr.toString)
  }

  /**
    * Run the test and return (status code, stdout, stderr)
    */
  private def run_test(jail : Path, tname : String) : (Int, String, String) = {
    val test_info = this.TASK_TEST_INFO(tname)
    val jar_dir = Paths.get(this.JARS_DIRPATH)
    val jar_paths = s".:${jar_dir.toAbsolutePath.resolve("*")}"

    val run_cmd = s"java -cp $jar_paths org.junit.runner.JUnitCore ${test_info.test_name}"
    val stdout = new java.lang.StringBuilder
    val stderr = new java.lang.StringBuilder
    val status =
      Process(run_cmd, jail.toFile) !
        ProcessLogger(fout => stdout.append(fout + "\n"), ferr => stderr.append(ferr + "\n"))

    (status, stdout.toString, stderr.toString)
  }

  /**
    * check if we have the dir jail for the session in this task
    */
  private def get_session_root(session_id : String, tname : String) : Option[Path] = {
    val pname = s"${this.PROBLEM_PATH}/$tname/${"session-" + session_id}"
    val path = Paths.get(pname)
    Some(path).filter(Files.exists(_))
  }

  /**
    * record the study result into DB
    * Schema:
    * CREATE TABLE IF NOT EXISTS study (
    * id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    * session TEXT NOT NULL,
    * task TEXT NOT NULL,
    * code TEXT NOT NULL,
    * duration INTEGER NOT NULL,
    * timeout INT NOT NULL,
    * correct INT NOT NULL,
    * tool INT NOT NULL
    * );
    */
  private def record_study_result(session_id : String, tname : String, code : String, duration : Int,
                                  timeout : Boolean, correct : Boolean, compiled : Boolean, use_tool : Boolean) : Unit = {
    val tout = if(timeout) 1 else 0
    val corr = if(correct) 1 else 0
    val com = if(compiled) 1 else 0
    val tool = if(use_tool) 1 else 0
    val query =
      s"""
        |INSERT INTO study (session, task, code, duration, timeout, correct, compiled, use_tool)
        |VALUES ('$session_id', '$tname', '${code.replace("'", "''")}', $duration, $tout, $corr, $com, $tool);
      """.stripMargin
    this.logger.info("Record study result ============\n{}\n", query)
    val statement = this.DB_CONN.prepareStatement(query)
    statement.executeUpdate()
  }

  /**
    * Check if the table exists
    */
  private def db_exists : Boolean = {
    val meta = this.DB_CONN.getMetaData
    val res = meta.getTables(null, null, "study", Array[String]("TABLE"))
    res.next()
  }

  def main(args : Array[String]) : Unit = {
    if(args.length < 1) {
      System.err.println(
        """Usage: [port]

         Start a refactoring web server using the given port
      """)
      return
    }

    val port_number = Integer.parseInt(args(0))

    logger.info("port: {}", port_number)
    port(port_number)
    staticFiles.location("api-refactoring-web/public")

    get("/", (req, res) => "Hello world.")

    get("/db", (req, res) => {
      val model =
        if(this.db_exists) {
          val table = mutable.ArrayBuffer.empty[util.Map[String, String]]
          val statement = this.DB_CONN.prepareStatement("SELECT * FROM study")
          val result = statement.executeQuery()
          while(result.next()) {
            val id = result.getInt("id")
            val session_id = result.getString("session")
            val code = result.getString("code")
            val duration = result.getInt("duration")
            val timeout = result.getInt("timeout")
            val task = result.getString("task")
            val correct = result.getInt("correct")
            val compiled = result.getInt("compiled")
            val tool = result.getInt("use_tool")

            table += Map(
              "id" -> id.toString,
              "session" -> session_id,
              "duration" -> duration.toString,
              "compiled" -> compiled.toString,
              "correct" -> correct.toString,
              "task" -> task,
              "timeout" -> timeout.toString,
              "code" -> code,
              "use_tool" -> tool.toString,
            ).asJava
          }

          JtwigModel.newModel()
            .`with`("has_db", true)
            .`with`("db_table", table.asJava)
        } else {
          JtwigModel.newModel()
            .`with`("has_db", false)
            .`with`("db_table", List().asJava)
        }

      val template = JtwigTemplate.classpathTemplate("api-refactoring-web/public/templates/db.twig")
      template.render(model)
    })

    get("/task/", (req, res) => {
      val task_name = req.queryParams("task")
      val use_tool = req.queryParams("type")
      logger.info("Task name : {}", task_name)

      val code = this.load_problem(task_name)
      logger.info("Loaded code ================\n{}\n", code)

      // create session ID
      val session_id = UUID.randomUUID.toString
      val template = JtwigTemplate.classpathTemplate("api-refactoring-web/public/templates/study.twig")
      val model = JtwigModel.newModel()
        .`with`("draft", code)
        .`with`("task", task_name)
        .`with`("session", session_id)
        .`with`("use_tool", use_tool == "1")

      this.setup_test_env(session_id, task_name)
      assert(this.get_session_root(session_id, task_name).isDefined, "Failed to create session: " + session_id)
      template.render(model)
    })

    post("/test", (req, res) => {
      assert(this.db_exists)
      val reader = req.body().parseJson
      val code = reader.asJsObject.fields("code").asInstanceOf[JsString].value
      val task_name = reader.asJsObject.fields("task").asInstanceOf[JsString].value
      val session_id = reader.asJsObject.fields("session").asInstanceOf[JsString].value
      val duration = reader.asJsObject.fields("duration").asInstanceOf[JsNumber].value.intValue()
      val timeout = reader.asJsObject.fields("timeout").asInstanceOf[JsBoolean].value
      val use_tool = reader.asJsObject.fields("use_tool").asInstanceOf[JsBoolean].value
      logger.info("Input code for testing {} ============\n{}", task_name, code)

      val root = this.get_session_root(session_id, task_name)

      val (runtime, (output, correct)) = Utils.time[(String, Boolean)]{
        if(root.isEmpty) {
          ("No session exists. Please refresh the page.", false)
        } else {
          // write the code into the jail
          val test_info = this.TASK_TEST_INFO(task_name)
          val code_file = new File(s"${root.get.toString}/${test_info.code_fname}")
          Files.write(code_file.toPath, code.getBytes(StandardCharsets.UTF_8))

          val output_builder = new java.lang.StringBuilder
          // compile the code
          val (compile_status, _, compile_stderr) = this.compile_code(code, root.get, task_name)
          val correct = {
            if(compile_status != 0) {
              output_builder.append("**** Failed to compile code ****\n")
              output_builder.append(compile_stderr)
              this.record_study_result(session_id, task_name, code, duration, timeout, correct = false, compiled = false, use_tool)
              false
            } else {
              val (run_status, run_stdout, _) = this.run_test(root.get, task_name)
              if(run_status != 0) {
                output_builder.append("**** Test failed ****\n")
                output_builder.append(run_stdout)
                this.record_study_result(session_id, task_name, code, duration, timeout, correct = false, compiled = true, use_tool)
                false
              } else {
                output_builder.append(run_stdout)
                this.record_study_result(session_id, task_name, code, duration, timeout, correct = true, compiled = true, use_tool)
                true
              }
            }
          }
          (output_builder.toString, correct)
        }
      }

      val output_json =
        JsObject(
          "stdout" -> JsString(output),
          "stderr" -> JsString(""),
          "correct" -> JsBoolean(correct),
          "success" -> JsBoolean(true),
          "runtime" -> JsNumber(runtime)
        )
      res.`type`("application/json")
      output_json.compactPrint
    })

    post("/run_cache", (req, res) => {
      val reader = req.body().parseJson
      val code = reader.asJsObject.fields("code").asInstanceOf[JsString].value
      val task_name = reader.asJsObject.fields("task").asInstanceOf[JsString].value
      val debug = if(reader.asJsObject.fields.contains("debug"))
          reader.asJsObject.fields("debug").asInstanceOf[JsBoolean].value
        else
          false

      val (runtime, solution) = Utils.time({
        val jar_paths = get_filenames(s"${this.PROBLEM_PATH}/jars")
        val prog = JavaUtils.parse_src(code, None, None, None, jar_paths, Seq()).get
        val benchmark_task_name = this.get_benchmark_taskname(task_name, prog)
        val task = this.TASK_MAP(benchmark_task_name)

        logger.debug("Input code for task {} ============\n{}", task.name, code)

        if(debug) {
          this.get_cache_result(task.name, task_name)
        } else {
          val sleep_time = 20000 + Random.nextInt(10) * 1000
          // val sleep_time = 2000
          logger.info(s"Sleep for $sleep_time ms...")
          Thread.sleep(sleep_time)
          this.get_cache_result(task.name, task_name)
        }
      })

      logger.info("Refactor solutions =============\n{}", solution)

      val output_json =
        if(solution.isEmpty) {
          JsObject(
            "stdout" -> JsString(""),
            "stderr" -> JsString("Failed to refactor APIs. No results."),
            "success" -> JsBoolean(false),
            "runtime" -> JsNumber(-1)
          )
        } else{
          JsObject(
            "stdout" -> JsString(solution.get),
            "stderr" -> JsString(""),
            "success" -> JsBoolean(true),
            "runtime" -> JsNumber(runtime)
          )
        }
      res.`type`("application/json")
      output_json.compactPrint
    })

    post("/run", (req, res) => {
      val reader = req.body().parseJson
      val code = reader.asJsObject.fields("code").asInstanceOf[JsString].value
      val task_name = reader.asJsObject.fields("task").asInstanceOf[JsString].value

      val (runtime, solutions) = Utils.time({
        val jar_paths = get_filenames(s"${this.PROBLEM_PATH}/$task_name/jars")
        val prog = JavaUtils.parse_src(code, None, None, None, jar_paths, Seq()).get
        val benchmark_task_name = this.get_benchmark_taskname(task_name, prog)
        val task = this.TASK_MAP(benchmark_task_name)

        logger.debug("Input code for task {} ============\n{}", task.name, code)
        val jar = new JarAnalyzer(jar_paths)
        val interpreter = new JavaFastInterpreter(jar_paths)

        logger.debug("Running good algorithm.")
        val algo = new APITransNL(benchmark_task_name, interpreter, jar)
        algo.refactor(prog, (task.ret_val, task.ret_type))
      })

      logger.info("Refactor solutions =============\n{}", solutions.foldLeft("")((acc, block) => acc + "\n" + block.toString))
      val result_builder = new java.lang.StringBuilder
      solutions.foreach(block => result_builder.append("=============\n").append(block._2.compilation_unit).append("\n"))

      val output_json =
      if(solutions.isEmpty) {
        JsObject(
          "stdout" -> JsString(""),
          "stderr" -> JsString("Failed to refactor APIs. No results."),
          "success" -> JsBoolean(false),
          "runtime" -> JsNumber(-1)
        )
      } else{
        JsObject(
          "stdout" -> JsString(result_builder.toString),
          "stderr" -> JsString(""),
          "success" -> JsBoolean(true),
          "runtime" -> JsNumber(runtime)
        )
      }
      res.`type`("application/json")
      output_json.compactPrint
    })
  }
}
