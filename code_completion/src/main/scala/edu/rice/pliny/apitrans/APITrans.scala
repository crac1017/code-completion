package edu.rice.pliny.apitrans

import com.github.javaparser.ast.NodeList
import com.github.javaparser.ast.body.MethodDeclaration
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.visitor.VoidVisitorAdapter
import com.github.javaparser.symbolsolver.model.typesystem._
import edu.rice.pliny.language.java._

import scala.collection.mutable

object APITrans {
  // class for storing candidate calls. t is the type of this candidate call
  case class TypeBinding(name : String, t : Type)
  case class CandidateCall(call : Expression, req_types : Set[TypeBinding], t : Type)

  // environment type for API refactoring
  type Binding = (Expression, Type)
  type Env = Seq[Binding]

  val VOID_PLACEHOLDER = TypeBinding("_Void_", VoidType.INSTANCE)
  var LENGTH_LIMIT = 10
  var SEQ_LIMIT = 10

  /**
    * Extract actual API calls and object creation expression from a block of API sequence
    */
  def extract_calls(block : BlockStmt) : Seq[Expression] = {
    val result = mutable.ArrayBuffer.empty[Expression]

    val visitor = new VoidVisitorAdapter[Any] {
      override def visit(node : MethodCallExpr, args : Any) : Unit = {
        val children = node.getChildNodes
        for(i <- 0 until children.size()) {
          children.get(i).accept(this, args)
        }
        result += node
      }

      override def visit(node : ObjectCreationExpr, args : Any) : Unit = {
        val children = node.getChildNodes
        for(i <- 0 until children.size()) {
          children.get(i).accept(this, args)
        }
        result += node
      }
    }

    val slist = block.getStatements
    for(i <- 0 until slist.size()) {
      slist.get(i).accept(visitor, null)
    }

    result
  }

  /**
    * calculate the correct number of synthesized API calls. We just use LCS to see
    * how many common API calls we get as a result.
    *
    * By default we don't look at parameters, but we can turn this on.
    */
  def correct_num(expected : Seq[Expression], output : Seq[Expression], consider_param : Boolean = false) : Int = {
    if(expected.isEmpty || output.isEmpty) {
      return 0
    }

    def extract_symbols(e : Expression) : String = e match {
      case mce : MethodCallExpr =>
        if(!consider_param) {
          mce.getNameAsString
        } else {
          val builder = new java.lang.StringBuilder()
          builder.append(mce.getNameAsString)
          builder.append("(")
          for(i <- 0 until mce.getArguments.size()) {
            if(i > 0) { builder.append(", ") }
            builder.append(mce.getArgument(i))
          }
          builder.append(")")
          builder.toString
        }
      case oce : ObjectCreationExpr =>
        if(!consider_param) {
          oce.getType.asString()
        } else {
          val builder = new java.lang.StringBuilder()
          builder.append(oce.getType.asString())
          builder.append("(")
          for(i <- 0 until oce.getArguments.size()) {
            if(i > 0) { builder.append(", ") }
            builder.append(oce.getArgument(i))
          }
          builder.append(")")
          builder.toString
        }
    }

    val l1 = expected.map(extract_symbols)
    val l2 = output.map(extract_symbols)

    val mat = Array.fill(l1.size + 1, l2.size + 1)(0)
    for(i <- 0 to l1.size) {
      for(j <- 0 to l2.size) {
        mat(i)(j) =
          if(i == 0 || j == 0) {
            0
          } else if(l1(i - 1) == l2(j - 1)) {
            mat(i - 1)(j - 1) + 1
          } else {
            Math.max(mat(i - 1)(j), mat(i)(j - 1))
          }
      }
    }

    mat(l1.size)(l2.size)
  }


  /**
    * Find the first refactoring block in the class
    */
  def find_refactoring_block(draft: JavaDraft) : Option[(BlockStmt, String)] = {
    val all_methods = draft.cclass.getMethods
    for(i <- 0 until all_methods.size()) {
      val method = all_methods.get(i)
      val body = method.getBody.get().getStatements
      val refactor = raw"refactor:([A-Za-z0-9.]+)".r
      for(i <- 0 until body.size()) {
        val s = body.get(i)
        s match {
          case bs : BlockStmt =>
            val comment = bs.getComment
            if(comment.isPresent) {
              val content = comment.get().getContent
              content match {
                case refactor(pkg) => return Some((bs, pkg))
              }
            }
          case _ => Unit
        }
      }
    }

    None
  }

  /**
    * Construct an initial environment used by the refactoring block
    */
  def construct_env(prog : JavaDraft, block : BlockStmt) : Env = {
    val result = mutable.Set.empty[Binding]
    val stmt_list = JavaUtils.get_enclosing_method(block).get.getBody.get().getStatements
    val recorded = mutable.Set.empty[String]

    val block_index = {
      var index = 0
      var found = false
      while(index < stmt_list.size() && !found) {
        if(stmt_list.get(index).equals(block)) {
          found = true
        }
        if(!found) {
          index += 1
        }
      }
      index
    }

    // get the declarations before the block
    for(i <- 0 until block_index) {
      val s = stmt_list.get(i)
      val defs = JavaUtils.get_defs(s)
      defs.foreach(vdor => {
        val t = prog.type_solver.convert(vdor.getType, vdor)
        val name = new NameExpr(vdor.getNameAsString)
        val rep = vdor.getNameAsString
        if(!recorded.contains(rep)) {
          result += ((name, t))
          recorded += rep
        }
      })
    }

    // add the parameters
    {
      val params = JavaUtils.get_enclosing_method(block).get.getParameters
      for(i <- 0 until params.size()) {
        val param = params.get(i)
        val name = new NameExpr(param.getNameAsString)
        val t = prog.type_solver.convert(param.getType, param)
        val rep = param.getNameAsString
        if(!recorded.contains(rep)) {
          result += ((name, t))
          recorded += rep
        }
        // result += ((name, t))
      }
    }

    // add the field in the class
    {
      val fields = prog.cclass.getFields
      for(i <- 0 until fields.size()) {
        val f = fields.get(i)
        val vars = f.getVariables
        for(j <- 0 until vars.size()) {
          val vdor = vars.get(j)
          val t = prog.type_solver.convert(vdor.getType, vdor)
          val name = new NameExpr(vdor.getNameAsString)
          val rep = vdor.getNameAsString
          if(!recorded.contains(rep)) {
            result += ((name, t))
            recorded += rep
          }
        }
      }
    }

    result.toSeq
  }

  /**
    * Inject the new API sequence into the program
    */
  def inject(input_prog : JavaDraft, old_block : BlockStmt, new_seq : Seq[Statement]) : MethodDeclaration = {

    // FIXME: we have a way to get enclosing method. input_prog.method might not be correct. We need to get the enclosing
    // method for original block
    val stmt_list = input_prog.method.getBody.get().getStatements

    val block_index = {
      var index = 0
      var found = false
      while(index < stmt_list.size() && !found) {
        if(stmt_list.get(index).equals(old_block)) {
          found = true
        }
        if(!found) {
          index += 1
        }
      }
      index
    }
    val new_stmt_list = new NodeList[Statement]

    for(i <- 0 until block_index) {
      new_stmt_list.add(stmt_list.get(i))
    }

    // add the new sequence in
    new_seq.foreach(s => new_stmt_list.add(s))

    // add the rest
    for(i <- block_index + 1 until stmt_list.size()) {
      new_stmt_list.add(stmt_list.get(i))
    }

    // construct a new draft
    val new_block = new BlockStmt(new_stmt_list)
    val new_method = input_prog.method.clone()
    new_method.setBody(new_block)

    // input_prog.create_new_draft(new_method)
  }
}

abstract class APITrans(val task_name : String,
                        val interpreter : JavaFastInterpreter,
                        val jar : JarAnalyzer) {
  def refactor(prog : JavaDraft, ret_val : (String, String)) : Seq[(BlockStmt, JavaDraft)]
}

