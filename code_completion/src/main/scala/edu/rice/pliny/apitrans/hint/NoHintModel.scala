package edu.rice.pliny.apitrans.hint

import com.github.javaparser.symbolsolver.model.typesystem.Type
import edu.rice.pliny.apitrans.JarAnalyzer.JarFunc

/**
  * get some keywords from several stackoverflow pages
  */
class NoHintModel(task : String) extends HintModel(task) {
  override def is_relevant_method(func: JarFunc): Boolean = true
  override def is_relevant_constructor(func: JarFunc): Boolean = true
  override def is_relevant_type(t: Type): Boolean = true
}
