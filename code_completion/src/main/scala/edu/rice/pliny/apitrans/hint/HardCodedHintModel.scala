package edu.rice.pliny.apitrans.hint

import java.io.File
import java.net.URLClassLoader

import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade
import com.github.javaparser.symbolsolver.model.typesystem.Type
import com.github.javaparser.symbolsolver.resolution.typesolvers.{CombinedTypeSolver, JarTypeSolver, MemoryTypeSolver, ReflectionTypeSolver}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.apitrans.JarAnalyzer.{JarConstructor, JarFunc, JarMethod}
import edu.rice.pliny.apitrans.JarAnalyzer
import edu.rice.pliny.apitrans.hint.HardCodedHintModel.FuncTypeHint
import edu.rice.pliny.language.java.FuncType

import scala.collection.mutable

object HardCodedHintModel {

  // this defines the type hint for a function
  // for constructors, we are using the class object instead of ret
  case class FuncTypeHint(params : Seq[String], ret : String)
}

/**
  * This class provides hints for API sequence synthesis
  */
class HardCodedHintModel(task : String, jar_paths : Seq[String], jar : JarAnalyzer) extends HintModel(task) {

  val logger = Logger(this.getClass)

  val loader : URLClassLoader = {
    val jar_files = jar_paths.map(p => new File(p))
    URLClassLoader.newInstance(jar_files.map(file => file.toURI.toURL).toArray, getClass.getClassLoader)
  }

  val type_solver: JavaParserFacade = {
    val jar_files = jar_paths.map(f => new File(f))
    val solver = new CombinedTypeSolver()
    solver.add(new ReflectionTypeSolver())
    solver.add(new MemoryTypeSolver())
    jar_files.foreach(file => solver.add(new JarTypeSolver(file.getAbsolutePath)))
    JavaParserFacade.get(solver)
  }

  val method_names : Map[String, Map[String, FuncTypeHint]] = task match {
    case "csv" =>
      Map(
        "FileReader" -> Map("new" -> FuncTypeHint(Seq("File"), "FileReader"))
        ,"CSVReader" -> Map("new" -> FuncTypeHint(Seq("Reader"), "CSVReader")
          ,"readNext" -> FuncTypeHint(Seq(), "String[]"))
      )
    case "csv3" =>
      Map(
        "FileWriter" -> Map("new" -> FuncTypeHint(Seq("String"), "FileWriter"))
        ,"CSVWriter" -> Map("new" -> FuncTypeHint(Seq("Writer"), "CSVWriter")
          ,"close" -> FuncTypeHint(Seq(), "void")
          ,"writeNext" -> FuncTypeHint(Seq("String[]"), "void"))
      )
    case "word" =>
      Map(
        "XWPFDocument" -> Map("new" -> FuncTypeHint(Seq("InputStream"), "XWPFDocument"))
        ,"XWPFWordExtractor" -> Map("new" -> FuncTypeHint(Seq("XWPFDocument"), "XWPFWordExtractor")
          ,"getText" -> FuncTypeHint(Seq(), "String"))
      )
    case "word3" =>
      Map(
        "XWPFDocument" -> Map("new" -> FuncTypeHint(Seq(), "XWPFDocument")
          ,"createParagraph" -> FuncTypeHint(Seq(), "XWPFParagraph")
          ,"write" -> FuncTypeHint(Seq("OutputStream"), "void"))
        ,"XWPFParagraph" -> Map("createRun" -> FuncTypeHint(Seq(), "XWPFRun"))
        ,"XWPFRun" -> Map("setText" -> FuncTypeHint(Seq("String"), "void"))
        ,"FileOutputStream" -> Map("new" -> FuncTypeHint(Seq("File"), "FileOutputStream")
          ,"close" -> FuncTypeHint(Seq(), "void"))
      )
    case "pdf" =>
      Map(
        "PdfReader" -> Map("new" -> FuncTypeHint(Seq("String"), "PdfReader")),
        "PdfTextExtractor" -> Map("getTextFromPage" -> FuncTypeHint(Seq("PdfReader", "int"), "String"))
      )
    case "pdf3" =>
      Map(
        // "File" -> Map("new" -> FuncTypeHint(Seq("String"), "File"))
        // ,"PDDocument" -> Map("new" -> FuncTypeHint(Seq(), "PDDocument")
          // ,"addPage" -> FuncTypeHint(Seq("PDPage"), "void")
          // ,"save" -> FuncTypeHint(Seq("File"), "void")
          // ,"close" -> FuncTypeHint(Seq(), "void"))
        // ,"PDPage" -> Map("new" -> FuncTypeHint(Seq(), "PDPage"))
        "PDPageContentStream" -> Map("new" -> FuncTypeHint(Seq("PDDocument", "PDPage"), "PDPageContentStream")
          ,"setFont" -> FuncTypeHint(Seq("PDFont", "float"), "void")
          ,"beginText" -> FuncTypeHint(Seq(), "void")
          ,"showText" -> FuncTypeHint(Seq("String"), "void")
          ,"endText" -> FuncTypeHint(Seq(), "void")
          ,"close" -> FuncTypeHint(Seq(), "void"))
      )
    case "http" =>
      Map(
        "OkHttpClient" -> Map("new" -> FuncTypeHint(Seq(), "OkHttpClient")
          ,"newCall" -> FuncTypeHint(Seq("okhttp3.Request"), "Call"))
        ,"Request$Builder" -> Map("new" -> FuncTypeHint(Seq(), "Request$Builder")
          ,"url" -> FuncTypeHint(Seq("String"), "Builder")
          ,"build" -> FuncTypeHint(Seq(), "okhttp3.Request"))
        ,"Request" -> Map()
        ,"Call" -> Map("execute" -> FuncTypeHint(Seq(), "okhttp3.Response"))
        ,"Response" -> Map("body" -> FuncTypeHint(Seq(), "okhttp3.ResponseBody"))
        ,"ResponseBody" -> Map("string" -> FuncTypeHint(Seq(), "String"))
      )
    case "httpserver" =>
      Map(
        "ServerConfiguration" -> Map("addHttpHandler" -> FuncTypeHint(Seq("HttpHandler"), "void"))
        ,"HttpServer" -> Map("createSimpleServer" -> FuncTypeHint(Seq(), "HttpServer")
         ,"start" -> FuncTypeHint(Seq(), "void")
         ,"getServerConfiguration" -> FuncTypeHint(Seq(), "ServerConfiguration"))
      )
    case "html" =>
      Map(
        "Jsoup" -> Map("parse" -> FuncTypeHint(Seq("String"), "Document"))
        ,"Document" -> Map("select" -> FuncTypeHint(Seq("String"), "Elements"))
        ,"Elements" -> Map("first" -> FuncTypeHint(Seq(), "Element"))
        ,"Element" -> Map("attr" -> FuncTypeHint(Seq("String"), "String"))
      )
    case "html3" =>
      Map(
        "Html" -> Map("new" -> FuncTypeHint(Seq(), "Html")
          ,"appendChild" -> FuncTypeHint(Seq("Node"), "Html")
          ,"write" -> FuncTypeHint(Seq(), "String"))
        ,"Head" -> Map("new" -> FuncTypeHint(Seq(), "Head")
          ,"appendChild" -> FuncTypeHint(Seq("Node"), "Head"))
        ,"Title" -> Map("new" -> FuncTypeHint(Seq(), "Title")
          ,"appendText" -> FuncTypeHint(Seq("String"), "Title"))
        ,"Body" -> Map("new" -> FuncTypeHint(Seq(), "Body")
          ,"appendChild" -> FuncTypeHint(Seq("Node"), "Body"))
        ,"H1" -> Map("new" -> FuncTypeHint(Seq(), "H1")
          ,"appendText" -> FuncTypeHint(Seq("String"), "H1"))
        ,"Div" -> Map("new" -> FuncTypeHint(Seq(), "Div")
          ,"appendChild" -> FuncTypeHint(Seq("Node"), "Div"))
        ,"P" -> Map("new" -> FuncTypeHint(Seq(), "P")
          ,"appendText" -> FuncTypeHint(Seq("String"), "P"))
      )
    case "email" =>
      Map(
        "DefaultAuthenticator" -> Map("new" -> FuncTypeHint(Seq("String", "String"), "DefaultAuthenticator"))
        ,"Properties" -> Map()
        ,"Session" -> Map("getDefaultInstance" -> FuncTypeHint(Seq("Properties", "Authenticator"), "Session"))
        ,"Message" -> Map("setFrom" -> FuncTypeHint(Seq("Address"), "void")
          ,"setText" -> FuncTypeHint(Seq("String"), "void")
          ,"setSubject" -> FuncTypeHint(Seq("String"), "void"))
        ,"MimeMessage" -> Map("new" -> FuncTypeHint(Seq("Session"), "MimeMessage"))
        ,"InternetAddress" -> Map("new" -> FuncTypeHint(Seq("String"), "InternetAddress"))
        ,"Transport" -> Map("send" -> FuncTypeHint(Seq("Message"), "void"))
      )
    case "ftp" =>
      Map(
        "SSHClient" -> Map("new" -> FuncTypeHint(Seq(), "SSHClient")
          ,"authPassword" -> FuncTypeHint(Seq("String", "String"), "void")
          ,"connect" -> FuncTypeHint(Seq("String"), "void"))
          // ,"newSFTPClient" -> FuncTypeHint(Seq(), "SFTPClient"))
        ,"SFTPClient" -> Map("ls" -> FuncTypeHint(Seq("String"), "List")
          ,"close" -> FuncTypeHint(Seq(), "void"))
      )
    case "gui" =>
      Map(
        "Stage" -> Map("new" -> FuncTypeHint(Seq(), "Stage")
          ,"setTitle" -> FuncTypeHint(Seq("String"), "void")
          ,"setScene" -> FuncTypeHint(Seq("Scene"), "void")
          ,"show" -> FuncTypeHint(Seq(), "void"))
        ,"StackPane" -> Map("new" -> FuncTypeHint(Seq(), "StackPane")
          ,"getChildren" -> FuncTypeHint(Seq(), "ObservableList"))
        // ,"ObservableList" -> Map("add" -> FuncTypeHint(Seq("Component"), "void"))
        // ,"List" -> Map("add" -> FuncTypeHint(Seq("*"), "boolean"))
        ,"Button" -> Map("new" -> FuncTypeHint(Seq("String"), "Button"))
        ,"Label" -> Map("new" -> FuncTypeHint(Seq("String"), "Label"))
        ,"Scene" -> Map("new" -> FuncTypeHint(Seq("Parent", "double", "double"), "Scene"))
      )
    case "regression" =>
      Map(
        "NumericAttribute" -> Map("new" -> FuncTypeHint(Seq("String"), "NumericAttribute"))
        ,"DelimitedTextParser" -> Map("new" -> FuncTypeHint(Seq(), "DelimitedTextParser")
          ,"setResponseIndex" -> FuncTypeHint(Seq("Attribute", "int"), "DelimitedTextParser")
          ,"setDelimiter" -> FuncTypeHint(Seq("String"), "DelimitedTextParser")
          ,"parse" -> FuncTypeHint(Seq("String"), "AttributeDataset")
          ,"toArray" -> FuncTypeHint(Seq("double[][]"), "double[][]"))
        ,"AttributeDataset" -> Map("toArray" -> FuncTypeHint(Seq("Object[]"), "Object[]"))
        ,"RidgeRegression$Trainer" -> Map("new" -> FuncTypeHint(Seq("double"), "RidgeRegression$Trainer")
          , "train" -> FuncTypeHint(Seq("double[][]", "double[]"), "RidgeRegression"))
        ,"RidgeRegression" -> Map("predict" -> FuncTypeHint(Seq("double[]"), "double"))
      )
    case "kmeans3" =>
      Map(
        "EKmeans" -> Map("new" -> FuncTypeHint(Seq("double[][]", "double[][]"), "EKmeans")
          ,"setIteration" -> FuncTypeHint(Seq("int"), "void")
          ,"run" -> FuncTypeHint(Seq(), "void"))
      )
    case "graphics" =>
      Map(
        "Graphics" -> Map("drawRect" -> FuncTypeHint(Seq("float", "float", "float", "float"), "void")
          ,"drawImage" -> FuncTypeHint(Seq("Image", "float", "float"), "void"))
        ,"Image" -> Map("new" -> FuncTypeHint(Seq("String"), "Image"))
      )
    case "jparser" =>
      Map(
        "NameExpr" -> Map("new" -> FuncTypeHint(Seq("String"), "NameExpr"))
        ,"FieldAccessExpr" -> Map("new" -> FuncTypeHint(Seq("Expression", "String"), "FieldAccessExpr"))
        ,"Expression" -> Map()
        ,"Node" -> Map()
        ,"NodeList" -> Map("new" -> FuncTypeHint(Seq(), "NodeList"))
        ,"MethodCallExpr" -> Map("new" -> FuncTypeHint(Seq("Expression", "String", "NodeList"), "MethodCallExpr"))
        ,"ExpressionStmt" -> Map("new" -> FuncTypeHint(Seq("Expression"), "ExpressionStmt")
          ,"toString" -> FuncTypeHint(Seq(), "String"))
      )
    case "jparser3" =>
      Map(
        "CompilationUnit" -> Map("accept" -> FuncTypeHint(Seq("ASTVisitor"), "void"))
        ,"String" -> Map("toCharArray" -> FuncTypeHint(Seq(), "char[]"))
        ,"ASTParser" -> Map("newParser" -> FuncTypeHint(Seq("int"), "ASTParser")
          ,"setCompilerOptions" -> FuncTypeHint(Seq("Map"), "void")
          ,"setKind" -> FuncTypeHint(Seq("int"), "void")
          ,"setSource" -> FuncTypeHint(Seq("char[]"), "void"))
      )
  }

  def is_compatible(ft : FuncType, hint : FuncTypeHint) : Boolean = {
    if(ft.params.size != hint.params.size) {
      this.logger.debug("Parameter size is different from the hint")
      return false
    }

    // check return type
    {
      if(!ft.ret.describe().endsWith(hint.ret) && hint.ret != "*") {
        this.logger.debug("Return type is different from the hint")
        return false
      }
    }

    ft.params.map(_.describe()).zip(hint.params).forall(p => {
      this.logger.debug("Comparing {} against hint {}", p._1, p._2)
      p._1.endsWith(p._2) || p._2 == "*"
    })
  }

  val relevant_methods : mutable.Map[Class[_], mutable.Set[JarFunc]] = {
    val result = mutable.Map.empty[Class[_], mutable.Set[JarFunc]]

    this.jar.funcs.foreach(clazz => {
      clazz._2.foreach {
        case f@JarMethod(name, t, clz, method) =>
          if(name == "newSFTPClient") {
            println("FOO")
          }
          val class_name = clz.getName.split("\\.").last
          if(this.method_names.contains(class_name) &&
            this.method_names(class_name).contains(name) &&
            name != "new" &&
            this.is_compatible(t, this.method_names(class_name)(name))) {
            if (!result.contains(clz)) {
              result.put(clz, mutable.Set.empty[JarFunc])
            }

            this.logger.debug("Adding method {} into the relevant set", method.getName)
            result(clz) += f
          }
        case f@JarConstructor(_, t, clz, cons) =>
          val class_name = clz.getName.split("\\.").last
          if (this.method_names.contains(class_name) &&
            this.method_names(class_name).contains("new") &&
            this.is_compatible(t, this.method_names(class_name)("new"))) {
            if (!result.contains(clz)) {
              result.put(clz, mutable.Set.empty[JarFunc])
            }
            this.logger.debug("Adding constructor {} into the relevant set", cons.getName)
            result(clz) += f
          }
      }
    })

    result
  }

  override def is_relevant_method(func : JarFunc) : Boolean = {
    func match {
      case JarMethod(_, t, clz, method) =>
        relevant_methods.contains(clz) && relevant_methods(clz).contains(func)
      case JarConstructor(_, _, _, _) => false
    }
  }

  override def is_relevant_constructor(func: JarFunc): Boolean = {
    func match {
      case JarMethod(_, _, _, _) => false
      case JarConstructor(_, _, clz, _) =>
        relevant_methods.contains(clz) && relevant_methods(clz).contains(func)
    }
  }

  override def is_relevant_type(t : Type) : Boolean = {
    if(t.isPrimitive) {
      true
    } else if(t.isArray) {
      this.is_relevant_type(t.asArrayType().getComponentType)
    } else {
      method_names.contains(t.asReferenceType().getId.split("\\.").last) ||
        t.asReferenceType().getId.startsWith("java")
    }
  }
}
