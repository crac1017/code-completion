package edu.rice.pliny.apitrans

import java.io.File

import java.util
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import org.deeplearning4j.plot.BarnesHutTsne
import org.nd4j.linalg.api.buffer.DataBuffer
import org.nd4j.linalg.factory.Nd4j
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer

/**
  * Train a TSNE model for plotting word2vec performance and generate word vectors for plotting
  */
object TrainTSNEMain {
  val logger = Logger(this.getClass)

  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    Nd4j.setDataType(DataBuffer.Type.DOUBLE)

    val cacheList = new util.ArrayList[String] //cacheList is a dynamic array of strings used to hold all words

    //STEP 2: Turn text input into a list of words
    logger.info("Loading word vector file...")
    val word_file = new File(AppConfig.TranslationConfig.word2vec_wordvec_filename)
    if(!word_file.exists()) {
      this.logger.error("Word vector file does not exists...")
      return
    }

    //Open the file
    //Get the data of all unique word vectors
    val vectors = WordVectorSerializer.loadTxt(word_file)
    val cache = vectors.getSecond
    val weights = vectors.getFirst.getSyn0 //seperate weights of unique words into their own list

    for(i <- 0 until cache.numWords()) {
      cacheList.add(cache.wordAtIndex(i))
    }

    val out_file = new File(AppConfig.TranslationConfig.tsne_words_filename)

    if(out_file.exists()) {
      this.logger.info("TSNE words CSV file already exists.")
    } else {
      logger.info("Building a TSNE model...")
      val tsne = new BarnesHutTsne.Builder()
        .setMaxIter(AppConfig.TranslationConfig.tsne_iter)
        .stopLyingIteration(250)
        .learningRate(500)
        .useAdaGrad(false)
        .theta(0.5)
        .setMomentum(0.5)
        .normalize(false)
        // .usePca(false)
        .build()

      this.logger.info("Generating TSNE vectors...")
      tsne.fit(weights)
      tsne.saveAsFile(cacheList, out_file.getAbsolutePath)
    }
  }
}
