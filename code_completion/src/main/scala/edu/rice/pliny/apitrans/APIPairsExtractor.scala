package edu.rice.pliny.apitrans

import java.io._

import com.github.javaparser.JavaParser
import com.github.javaparser.ast.{CompilationUnit, NodeList}
import com.github.javaparser.ast.`type`.Type
import com.github.javaparser.ast.body.{ClassOrInterfaceDeclaration, MethodDeclaration, Parameter}
import com.github.javaparser.ast.expr.{AssignExpr, Expression, MethodCallExpr, VariableDeclarationExpr}
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.visitor.VoidVisitorAdapter
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.language.java.JavaUtils
import org.apache.commons.io.FileUtils

import scala.collection.JavaConverters._
import scala.collection.mutable

/**
  * This class is used to extract similar API sequences
  */
object APIPairsExtractor {
  case class APICall(call : MethodCallExpr, method : MethodDeclaration, class_decl : ClassOrInterfaceDeclaration, file : File)
  type APISeqPair = (Seq[APICall], Seq[APICall])

  val logger = Logger(this.getClass)

  /**
    * Extract pairs of API sequences from two library source paths
    */
  def extract(path1 : String, path2 : String) : Seq[APISeqPair] = {
    val lib1 = this.extract_methods(path1)
    val lib2 = this.extract_methods(path2)

    // TODO: At least the method names of the sequences don't match
    this.logger.info("Comparing {} with {}", path1, path2)
    val result = lib1.flatMap(m1 => lib2.map(m2 => (m1, m2)))
      .par
      .filter(mp => this.is_similar(mp._1._1, mp._2._1))
      // .map(mp => (this.extract_seq(mp._1._1, mp._1._2), this.extract_seq(mp._2._1, mp._2._2)))
      // .filter(mp => mp._1.nonEmpty && mp._2.nonEmpty)

    // val file_pairs = mutable.Set.empty[(String, String)]
    // result.foreach(pair => file_pairs += ((pair._1.head.file.getAbsolutePath, pair._2.head.file.getAbsolutePath)))

    // println("File pairs: ====================\n")
    // file_pairs.foreach(p => println(p._1 + "," + p._2))

    if(result.nonEmpty) {
      result.seq.foreach(pair => {
        this.logger.info("Method 1 from file {}", pair._1._2.getAbsolutePath)
        this.logger.info("{}", pair._1._1.toString)
        this.logger.info("Method 2 from file {}", pair._2._2.getAbsolutePath)
        this.logger.info("{}", pair._2._1.toString)
      })
    }

    Seq()
  }

  def save_asts(path : String, ast_seq : Seq[(CompilationUnit, String)]) : Unit = {
    logger.info("Saving {} compilation units", ast_seq.size)
    val fos = new FileOutputStream(path + "/cu.obj")
    val oos = new ObjectOutputStream(fos)
    oos.writeObject(ast_seq)
    oos.close()
  }

  /**
    * Return a set of compilation units with class names
    */
  def load_asts(path : String) : Option[Seq[(CompilationUnit, String)]] = {
    val file = new File(path + "/cu.obj")
    if(file.exists()) {
      logger.info("Loading compilation units from file...")
      val fis = new FileInputStream(path + "/cu.obj")
      val ois = new ObjectInputStream(fis)
      Some(ois.readObject().asInstanceOf[Seq[(CompilationUnit, String)]])
    } else {
      None
    }
  }

  /**
    * Extract all methods from a library's source path
    */
  private def extract_methods(path: String) : Seq[(MethodDeclaration, File)] = {
    val cu_seq = FileUtils.listFiles(new File(path), Array("java"), true).asScala
      .par.map(f => {
        try {
          Some(JavaParser.parse(f), f)
        } catch {
          case _ : Throwable => None
        }
      }).toSeq.filter(cu => cu.isDefined).map(cu => cu.get).seq
    cu_seq.foldLeft(Seq.empty[(MethodDeclaration, File)])((acc, cu) => {
      val filename = cu._2.getName
      val class_decl = cu._1.getClassByName(filename.substring(0, filename.length - ".java".length))
      if(class_decl.isPresent) {
        acc ++ class_decl.get().getMethods.asScala.map(m => (m, cu._2))
      } else {
        acc
      }
    })
  }

  private def lemmatize(text : String) : Seq[String] = ???

  /**
    * Check if two methods are similar in terms of functionalities. We check for names and type similarities
    */
  private def is_similar(m1 : MethodDeclaration, m2 : MethodDeclaration) : Boolean = {
    def get_param_types(params: NodeList[Parameter]): Set[Type] = {
      val result = mutable.Set.empty[Type]
      for (i <- 0 until params.size()) {
        val p = params.get(i)
        result += p.getType
      }
      result.toSet
    }

    val EXCLUSIVES = Set("tostring", "equals", "hashcode", "setup", "init", "compareto", "test", "main",
      "set", "update", "get", "add", "write", "read", "run", "swap", "readobject", "writeobject", "hash", "join",
    "max", "min", "sum", "put", "next")

    val name1 = m1.getNameAsString.toLowerCase
    val name2 = m2.getNameAsString.toLowerCase
    val tlist1 = get_param_types(m1.getParameters)
    val tlist2 = get_param_types(m2.getParameters)
    val rt1 = m1.getType
    val rt2 = m2.getType

    // limit the number of statements to be at least 3
    name1.equals(name2) &&
      // tlist1.nonEmpty && tlist2.nonEmpty &&
      // tlist1.size == tlist2.size &&
      tlist1.intersect(tlist2).nonEmpty &&
      // tlist1.zip(tlist2).forall(p => p._1.equals(p._2)) &&
      rt1.equals(rt2) &&
      m1.getBody.isPresent && m1.getBody.get().getStatements.size() >= 3 &&
      m2.getBody.isPresent && m2.getBody.get().getStatements.size() >= 3 &&
      (!EXCLUSIVES.contains(name1) && !EXCLUSIVES.contains(name2))
      // (!JavaUtils.has_condition(m1) && !JavaUtils.has_condition(m2) &&
        // !JavaUtils.has_loop(m1) && !JavaUtils.has_loop(m2))
  }

  /**
    * Extract an API sequence from a method
    */
  private def extract_seq(method : MethodDeclaration, file : File) : Seq[APICall] = {
    val class_decl = method.getParentNode.get().asInstanceOf[ClassOrInterfaceDeclaration]

    val result = mutable.ArrayBuffer.empty[APICall]
    def get_api_calls(expr : Expression) : Unit = {
      val visitor = new VoidVisitorAdapter[Any] {
        override def visit(node : MethodCallExpr, args : Any) : Unit = {
          val args = node.getArguments
          for(i <- 0 until args.size()) {
            args.get(i).accept(this, null)
          }
          result += APICall(node, method, class_decl, file)
        }
      }
      expr.accept(visitor, null)
    }

    val stmt_list = method.getBody.get().getStatements
    for(i <- 0 until stmt_list.size()) {
      val stmt = stmt_list.get(i)
      stmt match {
        case es : ExpressionStmt =>
          es.getExpression match {
            case vde : VariableDeclarationExpr =>
              for(j <- 0 until vde.getVariables.size()) {
                if(vde.getVariables.get(j).getInitializer.isPresent) {
                  get_api_calls(vde.getVariables.get(j).getInitializer.get())
                }
              }
            case ae : AssignExpr => get_api_calls(ae.getValue)
            case _ => get_api_calls(es.getExpression)
          }
        case _ =>
      }
    }

    result
  }
}
