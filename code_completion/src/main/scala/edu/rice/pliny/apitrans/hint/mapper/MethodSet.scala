package edu.rice.pliny.apitrans.hint.mapper
import java.io.File

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.Utils
import spray.json._
import MethodSetJsonProtocol._
import edu.rice.pliny.apitrans.hint.mapper.MethodDesc.FuncType
import org.apache.commons.lang3.ArrayUtils

import scala.collection.mutable

object MethodSet {
  val logger = Logger(this.getClass)

  def load(filename : String) : MethodSet = {
    val json_str = scala.io.Source.fromFile(filename).mkString
    json_str.parseJson.asJsObject.convertTo[MethodSet]
  }

  /**
    * hard-coded subtyping relationships between classes. When we search a class for a derived method, we have to climb
    * up and search the super class for the method we are searching for.
    */
  val subtype_map = Map(
    "java.io.FileWriter" -> "java.io.Writer",
    "java.net.HttpURLConnection" -> "java.net.URLConnection",
    "javax.mail.Store" -> "javax.mail.Service",
    "java.awt.Graphics2D" -> "java.awt.Graphics",
    "javax.swing.JFrame" -> "java.awt.Frame",
    "java.awt.Frame" -> "java.awt.Window",
    "java.awt.Window" -> "java.awt.Container",
    "spoon.reflect.code" -> "java.lang.Object",
    "jsat.clustering.kmeans.NaiveKMeans" -> "jsat.clustering.kmeans.KMeans",
    "jsat.clustering.kmeans.KMeans" -> "jsat.clustering.KClusterer",
    "opennlp.tools.tokenize.TokenizerME" -> "opennlp.tools.tokenize.AbstractTokenizer",
    "javax.mail.Store" -> "javax.mail.Service",
    "org.jsoup.nodes.Document" -> "org.jsoup.nodes.Element",
    "org.jsoup.nodes.Element" -> "org.jsoup.nodes.Node",
    "org.docx4j.openpackaging.packages.WordprocessingMLPackage" -> "org.docx4j.openpackaging.packages.OpcPackage",
    "org.docx4j.openpackaging.packages.OpcPackage" -> "org.docx4j.openpackaging.Base",
    "org.apache.commons.mail.SimpleEmail" -> "org.apache.commons.mail.Email",
    "org.apache.commons.net.ftp.FTPClient" -> "org.apache.commons.net.ftp.FTP",
    "org.apache.commons.net.imap.IMAPClient" -> "org.apache.commons.net.imap.IMAP",
    "org.apache.commons.net.imap.IMAP" -> "org.apache.commons.net.SocketClient",
    "org.apache.commons.net.ftp.FTP" -> "org.apache.commons.net.SocketClient"
  )
}

class MethodSet {
  val logger = Logger(this.getClass)

  // package -> class -> method desc
  val data = mutable.Map.empty[String, mutable.Map[String, mutable.Set[MethodDesc]]]

  var size = 0

  def add(m : MethodDesc) : Unit = {
    logger.trace("Adding method into set: {}", m.name)

    // package
    if(!data.contains(m.package_name)) {
      data.put(m.package_name, mutable.Map.empty[String, mutable.Set[MethodDesc]])
      logger.trace("Added new package: {}", m.package_name)
    }

    // class name
    if(!data(m.package_name).contains(m.class_name)) {
      data(m.package_name).put(m.class_name, mutable.Set.empty[MethodDesc])
      logger.trace("Added new class: {}", m.class_name)
    }

    data(m.package_name)(m.class_name) += m
    this.size += 1
    logger.trace("Added new method: {}", m.name)
  }

  def add(ms : MethodSet): Unit = {
    this.data ++= ms.data
    this.size += ms.size
  }

  def save(filename : String) : Unit = {
    Utils.write_file(new File(filename), this.toJson.prettyPrint)
  }

  /**
    * Get all the methods under the package. If class_name is given, then we only look at the provided class.
    */
  def get(pname_prefix : String, class_name : Option[String] = None) : Seq[MethodDesc] = {
    this.logger.trace("Getting method desc from {} under class {}", pname_prefix, class_name)
    val result = mutable.ArrayBuffer.empty[MethodDesc]

    this.data.keySet.foreach(pkg_name => {
      if(pkg_name.startsWith(pname_prefix)) {
        this.logger.trace("Getting methods from package {}", pkg_name)
        val pkg = this.data(pkg_name)
        if (class_name.isDefined) {
          if (pkg.contains(class_name.get)) {
            val clz = pkg(class_name.get)
            result ++= clz
          } else {
            this.logger.warn("Cannot find class {}", class_name.get)
          }
        } else {
          this.logger.debug("Not using any class")
          // no class constraint
          pkg.values.foreach(methods => result ++= methods)
        }
      }
    })
    result
  }

  /**
    * Get a specific method from the set
    */
  def get_method(pname : String, class_name : String, method_name : String, ftype : FuncType) : Option[MethodDesc] = {
    var result : Option[MethodDesc] = None

    if(!data.contains(pname)) {
      logger.warn("Cannot find package: {}", pname)
    } else if(!data(pname).contains(class_name)) {
      logger.warn("Cannot find class: {}", class_name)
    } else {
      data(pname)(class_name).foreach(m => {
        if(result.isEmpty && m.name == method_name && m.t == ftype) {
          result = Some(m)
        }
      })
    }

    if(result.isEmpty) {
      this.logger.info("Cannot find method in current class.")
      // super class result
      val clz = pname + "." + class_name
      if(MethodSet.subtype_map.contains(clz)) {
        val super_clz = MethodSet.subtype_map(clz)
        val name_list = super_clz.split("\\.")
        val clz_name = name_list.last
        val pnamelist = ArrayUtils.subarray(name_list, 0, name_list.length - 1)
        val super_pname = Utils.string_join(pnamelist, ".")
        this.logger.info("Looking at super class : {}.{}.{}", super_pname, clz_name, method_name)
        get_method(super_pname, clz_name, method_name, ftype)
      } else {
        this.logger.warn("No super class to look at.")
        None
      }
    } else {
      result
    }
  }
}
