package edu.rice.pliny

import edu.rice.pliny.draft.Draft
import edu.rice.pliny.feature.source.ProgramElement

trait CodeDB {
  def query(prog : Draft, comment : String) : Seq[(Draft, Double, ProgramElement)]
}
