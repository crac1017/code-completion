package edu.rice.pliny.language.java

import com.github.javaparser.symbolsolver.model.typesystem.Type
import edu.rice.pliny.draft.DraftNodeType

/**
  * This represents a program in any language
  */
class JavaDraftNodeType(val t : Type) extends DraftNodeType {}
