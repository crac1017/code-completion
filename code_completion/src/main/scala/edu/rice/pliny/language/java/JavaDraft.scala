package edu.rice.pliny.language.java

import java.io.File
import java.net.URLClassLoader
import java.util

import com.github.javaparser.ast.body._
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.expr.AssignExpr
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast._
import com.github.javaparser.ast.visitor._
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade
import com.github.javaparser.symbolsolver.javaparsermodel.declarations.JavaParserClassDeclaration
import com.github.javaparser.symbolsolver.model.typesystem._
import com.github.javaparser.symbolsolver.reflectionmodel.ReflectionClassDeclaration
import com.github.javaparser.symbolsolver.resolution.typesolvers._
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft._

import scala.collection.JavaConversions._
import scala.collection.mutable

class JavaDraft(val compilation_unit : CompilationUnit,
                val cclass : ClassOrInterfaceDeclaration,
                val method : MethodDeclaration,
                val jar_paths : Seq[String],
                val dir_paths : Seq[String]) extends Draft {


  lazy val type_solver: JavaParserFacade = {
    val solver = new CombinedTypeSolver()
    solver.add(new ReflectionTypeSolver())
    solver.add(new MemoryTypeSolver())
    this.jar_paths.foreach(p => solver.add(new JarTypeSolver(p)))
    this.dir_paths.foreach(p => solver.add(new JavaParserTypeSolver(new File(p))))
    JavaParserFacade.get(solver)
  }
  val logger = Logger(this.getClass)

  // add necessary imports from all the types in the holes
  def add_imports_from_hole(): Unit = {
    val import_set = mutable.Set.empty[String]

    // add the ones already in the CU
    {
      val old_imports = this.compilation_unit.getImports
      for (i <- 0 until old_imports.size()) {
        import_set += old_imports.get(i).getName.asString()
      }
    }

    // walk the ast and add imports as necessary from holes
    new TreeVisitor {
      override def process(node: Node) : Unit = {
        node match {
          case mce: MethodCallExpr if JavaUtils.is_hole(mce) && mce.getArguments.size() > 1 =>
            val type_name = mce.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString()
            type_name match {
              case "int" => Unit
              case "byte" => Unit
              case "short" => Unit
              case "char" => Unit
              case "long" => Unit
              case "boolean" => Unit
              case "float" => Unit
              case "double" => Unit
              case "void" => Unit
              case _ =>
                if (!import_set.contains(type_name)) {
                  logger.debug("Adding new import: {}", type_name)
                  import_set.add(type_name)
                  // add the imports first
                  compilation_unit.addImport(new ImportDeclaration(JavaUtils.name_list_to_qname(type_name.split("\\.")), false, false))

                }
            }
          case _ =>
        }
      }
    }.visitPreOrder(this.method)
  }

  add_imports_from_hole()

  // this stores the class names imported at the beginning of the compilation unit
  // val class_loader: URLClassLoader = URLClassLoader.newInstance(jar_paths.map(p => new URL("jar:file:" + p + "!/")).toArray, getClass.getClassLoader)
  val class_loader: URLClassLoader = URLClassLoader.newInstance(jar_paths.map(p => new File(p).toURL).toArray, getClass.getClassLoader)
  val imported_class: mutable.Map[String, Name] = JavaUtils.get_imported_class_names(this.compilation_unit)

  // function level test cases and comments
  val (test_case: Option[TestCase], comment: Option[String]) = JavaUtils.parse_comment_test(this.method)

  override def get_test: Option[TestCase] = this.test_case

  override def get_comment: Option[String] = this.comment

  lazy val (type_env: mutable.Map[Node, Type], name_type_env: mutable.Map[String, Type]) = this.infer_type

  // val defset : mutable.Map[String, Option[Int]] = this.get_defs
  val defset: mutable.Set[String] = this.get_defs


  // methods from all the imported classes
  var class_methods: mutable.Map[(Name, Type), (Class[_], Boolean)] = _

  // class method types
  var class_method_types : mutable.Map[Name, Type] = _

  def count_nodes: (Int, Int, Int) = {
    var stmt_count = 0
    var expr_count = 0
    var node_count = 0
    val iter = JavaDraftIteratorFactory.create_draft_iterator(this)
    while (iter.hasNext) {
      val node = iter.next()
      node_count += 1
      if (is_expr(node)) {
        expr_count += 1
      } else if (is_stmt(node)) {
        stmt_count += 1
      }
    }
    (stmt_count, expr_count, node_count)
  }

  val (stmt_count: Int, expr_count: Int, node_count: Int) = this.count_nodes

  override def get_stmt_count: Int = this.stmt_count

  override def get_expr_count: Int = this.expr_count

  override def get_node_count: Int = this.node_count

  override def hashCode : Int = this.method.hashCode()

  /**
    * Put all defined names into a set
    *
    * @return
    */
  def get_defs: mutable.Set[String] = {
    val result = mutable.Set.empty[String]
    new VoidVisitorAdapter[Object] {
      override def visit(vdor: VariableDeclarator, obj: Object): Unit = {
        result += vdor.getNameAsString
        if (vdor.getInitializer.isPresent) {
          vdor.getInitializer.get().accept(this, null)
        }
      }

      override def visit(param: Parameter, obj: Object): Unit = {
        result += param.getNameAsString
      }
    }.visit(this.compilation_unit, null)
    result.foreach(r => logger.debug("Defined name: " + r))
    result
  }

  override def get_copy: Draft = {
    val new_cu = this.compilation_unit.clone()
    val new_class = new_cu.getClassByName(this.cclass.getNameAsString).get
    val new_method = JavaUtils.get_method(new_class, this.method.getNameAsString).get
    val new_jar_paths = mutable.ArrayBuffer.empty[String]
    this.jar_paths.foreach(p => new_jar_paths += p)
    val new_dir_paths = mutable.ArrayBuffer.empty[String]
    this.dir_paths.foreach(p => new_dir_paths += p)
    new JavaDraft(new_cu, new_class, new_method, new_jar_paths, new_dir_paths)
  }

  override def toString: String = this.method.toString()

  override def toStringNoComment: String = this.method.clone().removeComment().toString

  def toStringWithClass: String = this.compilation_unit.toString


  /**
    * Create a new java draft given a new method
    */
  def create_new_draft(new_method: MethodDeclaration): JavaDraft = {
    // get a new class with new method
    val new_class = this.cclass.clone()
    val method_index = this.cclass.getMembers.indexOf(this.method)
    new_class.getMembers.remove(method_index)
    new_class.getMembers.add(method_index, new_method)

    // get a new compilation unit
    val new_cu = this.compilation_unit.clone()
    val class_index = this.compilation_unit.getTypes.indexOf(this.cclass)
    new_cu.getTypes.remove(class_index)
    new_cu.getTypes.add(class_index, new_class)
    new JavaDraft(new_cu, new_class, new_method, jar_paths, dir_paths)
  }

  /**
    * This function is used to replace a node with another node. Notice that if there are multiple
    * source nodes that look exactly the same, they will all be replaced with the same target node.
    */
  override def replace(src: DraftNode, target: DraftNode): Draft = {
    assume(src.isInstanceOf[JavaDraftNode])
    assume(target.isInstanceOf[JavaDraftNode])
    val java_src_node = src.asInstanceOf[JavaDraftNode]
    val java_target_node = target.asInstanceOf[JavaDraftNode]

    // get a new method
    val new_method = this.method.clone()

    // This is a hack.
    // When doing renaming, usually we want to replace a NameExpr Node with another.
    // But the problem is that in a program where a vairable is defined but never used,
    // we will not have any NameExpr for that variable because it's never used. We only
    // have its VariableDeclaratorId. Therefore, when doing renaming, sometimes we want
    // to convert that VariableDeclaratorId into NameExpr
    (java_src_node.node, java_target_node.node) match {
      case (_: NameExpr, vid: SimpleName) =>
        JavaUtils.replace(new_method, java_src_node.node, new NameExpr(vid.getIdentifier))
      case _ => JavaUtils.replace(new_method, java_src_node.node, java_target_node.node.clone)
    }

    // get a new compilation unit
    this.create_new_draft(new_method)
  }

  /**
    * This function is used to replace the statement hole with a list of statement
    */
  override def replace(src: DraftNode, taget_parent: DraftNode, target: Seq[DraftNode]): Draft = {
    val jsrc_node = src.asInstanceOf[JavaDraftNode].node.asInstanceOf[Statement]
    val jtarget = mutable.ArrayBuffer.empty[Statement]
    target.foreach(t => jtarget.add(t.asInstanceOf[JavaDraftNode].node.asInstanceOf[Statement]))

    val new_method = this.method.clone()
    JavaUtils.replace(new_method, jsrc_node, jtarget)
    this.create_new_draft(new_method)
  }


  /**
    * This function is used to rename the reference inside a stand-alone window
    */
  override def replace(block: Seq[DraftNode], src: DraftNode, target: DraftNode): Seq[DraftNode] = {
    val statements = new NodeList[Statement]()
    block.foreach(d => statements.add(d.asInstanceOf[JavaDraftNode].node.clone().asInstanceOf[Statement]))
    val jblock = new BlockStmt(statements)
    val jsrc = src.asInstanceOf[JavaDraftNode].node
    val jtarget = target.asInstanceOf[JavaDraftNode].node
    val new_block = jblock.clone()
    (jsrc, jtarget) match {
      case (_: NameExpr, vid: SimpleName) =>
        JavaUtils.replace(jblock, jsrc, new NameExpr(vid.getIdentifier)).asInstanceOf[BlockStmt]
      case _ => JavaUtils.replace(jblock, jsrc, jtarget).asInstanceOf[BlockStmt]
    }
    new_block.getStatements.map(s => new JavaDraftNode(s))
  }

  /**
    * Get an arbitrary hole
    */
  override def get_hole: Option[DraftHole] = {
    var hole: Option[DraftHole] = None
    new TreeVisitor {
      override def process(node: Node): Unit = {
        if (hole.isEmpty) {
          node match {
            case mce: MethodCallExpr =>
              if (hole.isEmpty &&
                mce.getNameAsString.equals(JavaUtils.HOLE_NAME) &&
                mce.getParentNode.isPresent &&
                !mce.getParentNode.get().isInstanceOf[ExpressionStmt]) {
                val t =
                  if(mce.getArguments.size() > 1)
                    JavaUtils.str_to_type(mce.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), type_solver)
                  else
                    Wildcard.UNBOUNDED
                hole = Some(new JavaDraftHole(mce, None, None, ttype = t))
              }
            case es: ExpressionStmt =>
              es.getExpression match {
                case mce: MethodCallExpr if mce.getNameAsString.equals(JavaUtils.HOLE_NAME) =>
                  val t =
                    if(mce.getArguments.size() > 1)
                      JavaUtils.str_to_type(mce.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), type_solver)
                    else
                      Wildcard.UNBOUNDED
                  val (test, comment) = JavaUtils.parse_comment_test(es)
                  if (test.isDefined && comment.isDefined) {
                    hole = Some(new JavaDraftHole(es, comment, test, ttype = t))
                  } else {
                    hole = Some(new JavaDraftHole(es, None, None, ttype = t))
                  }
                case _ =>
              }
            case _ =>
          }
        }
      }
    }.visitPostOrder(method)
    hole
  }

  /**
    * Return the first node in the depth-first order from left to right. Return None if there's no hole.
    */
  override def get_stmt_hole_with_tests: Option[DraftHole] = {
    var hole: Option[DraftHole] = None
    new VoidVisitorAdapter[Any] {
      override def visit(node: ExpressionStmt, arg: Any): Unit = {
        if (hole.isEmpty) {
          node.getExpression match {
            case mce: MethodCallExpr if mce.getNameAsString.equals(JavaUtils.HOLE_NAME) =>
              val (test, comment) = JavaUtils.parse_comment_test(node)
              if (test.isDefined && comment.isDefined) {
                val t =
                  if(mce.getArguments.size() > 1)
                    JavaUtils.str_to_type(mce.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), type_solver)
                  else
                    Wildcard.UNBOUNDED
                hole = Some(new JavaDraftHole(node, comment, test, ttype = t))
              }
            case _ =>
          }
        }
      }
    }.visit(method, null)
    hole
  }

  override def get_stmt_hole_without_tests: Option[DraftHole] = {
    var hole: Option[DraftHole] = None
    new VoidVisitorAdapter[Any] {
      override def visit(node: ExpressionStmt, arg: Any): Unit = {
        if (hole.isEmpty) {
          node.getExpression match {
            case mce: MethodCallExpr if mce.getNameAsString.equals(JavaUtils.HOLE_NAME) =>
              val (test, comment) = JavaUtils.parse_comment_test(node)
              if (test.isEmpty) {
                val t =
                  if(mce.getArguments.size() > 1)
                    JavaUtils.str_to_type(mce.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), type_solver)
                  else
                    Wildcard.UNBOUNDED
                hole = Some(new JavaDraftHole(node, comment, test, ttype = t))
              }
            case _ =>
          }
        }
      }
    }.visit(method, null)
    hole
  }

  override def get_all_stmt_hole_without_tests: Seq[DraftHole] = {
    val result = mutable.ArrayBuffer.empty[DraftHole]
    new VoidVisitorAdapter[Any] {
      override def visit(node: ExpressionStmt, arg: Any): Unit = {
        node.getExpression match {
          case mce: MethodCallExpr if mce.getNameAsString.equals(JavaUtils.HOLE_NAME) =>
            val (test, comment) = JavaUtils.parse_comment_test(node)
            if (test.isEmpty) {
              val t =
                if(mce.getArguments.size() > 1)
                  JavaUtils.str_to_type(mce.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), type_solver)
                else
                  Wildcard.UNBOUNDED
              result += new JavaDraftHole(node, comment, test, ttype = t)
            }
          case _ =>
        }
      }
    }.visit(method, null)
    result
  }

  override def get_expr_hole: Option[DraftHole] = {
    val hole = JavaUtils.get_expr_hole(this.method)
    if (hole.isDefined) {
      val mce = hole.get.asInstanceOf[MethodCallExpr]
      val t =
        if(mce.getArguments.size() > 1)
          JavaUtils.str_to_type(mce.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), type_solver)
        else
          Wildcard.UNBOUNDED
      Some(new JavaDraftHole(hole.get, None, None, ttype = t))
    } else {
      None
    }
  }

  override def get_all_expr_hole: Seq[DraftHole] =
    JavaUtils.get_all_expr_hole(this.method).map(n => {
      val mce = n.asInstanceOf[MethodCallExpr]
      val t =
        if(mce.getArguments.size() > 1)
          JavaUtils.str_to_type(mce.getArguments.get(1).asInstanceOf[StringLiteralExpr].asString(), type_solver)
        else
          Wildcard.UNBOUNDED
      new JavaDraftHole(n, None, None, ttype = t)})

  /**
    * Check if this reference is defined within the draft
    */
  override def is_defined(node: DraftNode): Boolean = {
    val jnode = node.asInstanceOf[JavaDraftNode].node
    jnode.isInstanceOf[NameExpr] && this.defset.contains(jnode.asInstanceOf[NameExpr].getNameAsString)
  }

  /**
    * Return an undefined reference inside this draft for renaming
    */
  override def get_unref: Option[DraftNode] = {
    var result: Option[DraftNode] = None

    new VoidVisitorAdapter[Object] {
      override def visit(name: NameExpr, obj: Object): Unit = {
        if (result.isEmpty) {
          if (!type_env.contains(name)) {
            if (!imported_class.contains(name.getNameAsString)) {
              result = Some(new JavaDraftNode(name))
            }
          } else {
            val t = type_env(name)
            if (!imported_class.contains(name.getNameAsString)) {
              if (t.isWildcard) {
                result = Some(new JavaDraftNode(name))
              } else if (!defset.contains(name.toString)) {
                result = Some(new JavaDraftNode(name))
              }
            }
          }
        }
      }
    }.visit(this.method, null)
    result
  }

  override def size: Int = JavaUtils.node_size(this.method)

  /**
    * Infer the type of a node
    */
  override def get_type(node: DraftNode): DraftNodeType = {
    val ast_node = node.asInstanceOf[JavaDraftNode].node
    if (this.type_env.contains(ast_node)) {
      new JavaDraftNodeType(this.type_env(ast_node))
    } else {
      ast_node match {
        case name: NameExpr if this.name_type_env.contains(name.getNameAsString) =>
          new JavaDraftNodeType(this.name_type_env(name.getNameAsString))
        case _ => new JavaDraftNodeType(Wildcard.UNBOUNDED)
      }
      new JavaDraftNodeType(Wildcard.UNBOUNDED)
    }
  }

  /**
    * This is used to infer the type of a hole
    */
  def infer_unknown_type(expr: Expression, type_map: mutable.Map[Node, Type]): Type = {
    val parent = expr.getParentNode
    if (!parent.isPresent) {
      return Wildcard.UNBOUNDED
    }

    if (JavaUtils.is_hole(expr)) {
      val ht = JavaUtils.get_hole_typename(expr)
      if (ht.isDefined) {
        return JavaUtils.str_to_type(ht.get, type_solver)
      }
    }

    parent.get() match {
      case aae: ArrayAccessExpr if aae.getIndex.equals(expr) =>
        PrimitiveType.INT
      case aae: ArrayAccessExpr if aae.getName.equals(expr) =>
        val t = this.infer_unknown_type(aae, type_map)
        new ArrayType(t)
      case fes: ForeachStmt if fes.getIterable.equals(expr) =>
        val vdor = fes.getVariable.getVariables.get(0)
        val name = vdor.getName.getIdentifier
        val t =
          try {
            this.type_solver.convertToUsageVariableType(vdor)
          } catch {
            case _: Throwable =>
              logger.warn("Failed to get the type for name {}", name)
              val cd = new ClassOrInterfaceDeclaration(util.EnumSet.of(Modifier.PUBLIC), false, vdor.getType.toString)
              new ReferenceTypeImpl(new JavaParserClassDeclaration(cd, this.type_solver.getTypeSolver), this.type_solver.getTypeSolver)
          }
        new ArrayType(t)
      case vdor: VariableDeclarator =>
        this.type_solver.convertToUsageVariableType(vdor)
      case is: IfStmt if is.getCondition.equals(expr) =>
        PrimitiveType.BOOLEAN
      case ws: WhileStmt if ws.getCondition.equals(expr) =>
        PrimitiveType.BOOLEAN
      case assign: AssignExpr
        if assign.getValue.equals(expr) &&
          type_map.contains(assign.getTarget) && !type_map(assign.getTarget).isWildcard =>
        type_map(assign.getTarget)
      case assign: AssignExpr
        if assign.getTarget.equals(expr) &&
          type_map.contains(assign.getValue) && !type_map(assign.getValue).isWildcard =>
        type_map(assign.getValue)
      case binop: BinaryExpr
        if binop.getOperator.equals(BinaryExpr.Operator.LESS) ||
          binop.getOperator.equals(BinaryExpr.Operator.LESS_EQUALS) ||
          binop.getOperator.equals(BinaryExpr.Operator.GREATER) ||
          binop.getOperator.equals(BinaryExpr.Operator.GREATER_EQUALS) =>
        if (binop.getRight.equals(expr)) {
          type_map(binop.getLeft)
        } else {
          type_map(binop.getRight)
        }
      case binop: BinaryExpr
        if binop.getOperator.equals(BinaryExpr.Operator.AND) ||
          binop.getOperator.equals(BinaryExpr.Operator.OR) ||
          binop.getOperator.equals(BinaryExpr.Operator.LESS) ||
          binop.getOperator.equals(BinaryExpr.Operator.LESS_EQUALS) ||
          binop.getOperator.equals(BinaryExpr.Operator.GREATER) ||
          binop.getOperator.equals(BinaryExpr.Operator.GREATER_EQUALS) =>
        PrimitiveType.BOOLEAN
      case binop: BinaryExpr
        if binop.getOperator.equals(BinaryExpr.Operator.BINARY_AND) ||
          binop.getOperator.equals(BinaryExpr.Operator.BINARY_OR) =>
        if (binop.getRight.equals(expr)) {
          type_map(binop.getLeft)
        } else {
          type_map(binop.getRight)
        }
      case unop: UnaryExpr
        if unop.getOperator.equals(UnaryExpr.Operator.LOGICAL_COMPLEMENT) =>
        PrimitiveType.BOOLEAN
      case mce: MethodCallExpr =>
        if (mce.getArguments.contains(expr)) {
          // since we ensure that the scope is filled in already, we only need to consider parameters

          // get the method type
          val cand_methods = this.get_methods_from_class().filter(m => {
            (m._1._2.isInstanceOf[FuncType] &&
              // match the number of params
              mce.getArguments.length == m._1._2.asInstanceOf[FuncType].params.length &&

              // match the name
              mce.getNameAsString == m._1._1.getIdentifier &&

              // match the scope
              // if this is a static method call
              (mce.getScope.get().toString == m._1._1.getQualifier.get().toString ||

                // this is not a static method call
                this.is_subtype(type_map(mce.getScope.get()), this.class_to_type(m._2._1))))
          })

          if (cand_methods.isEmpty) {
            this.logger.warn("Empty candidate methods for inferring unknown types.")
            Wildcard.UNBOUNDED
          } else {
            val target_method = cand_methods.head
            val method_type = target_method._1._2.asInstanceOf[FuncType]
            /*
            this.logger.debug("method type: {}", method_type.describe())
            if(mce.getArguments.zip(method_type.params).filter(p => p._1 == expr).length == 0) {
              println("FOO")
            }
            */
            mce.getArguments.zip(method_type.params).filter(p => p._1 == expr).head._2
          }
        } else {
          Wildcard.UNBOUNDED
        }
      case _ =>
        Wildcard.UNBOUNDED
    }
  }

  def infer_type: (mutable.Map[Node, Type], mutable.Map[String, Type]) = {
    val iter = JavaDraftIteratorFactory.create_draft_iterator(this)
    val type_map = mutable.Map.empty[Node, Type]
    val name_type_map = mutable.Map.empty[String, Type]

    while (iter.hasNext) {
      val node = iter.next().asInstanceOf[JavaDraftNode].node
      node match {
        case e: Expression =>
          if (!type_map.contains(e) || type_map(e).isWildcard) {
            val t = try {
              this.type_solver.getType(e)
            } catch {
              case _: Throwable => Wildcard.UNBOUNDED
            }

            if (t.isReferenceType) {
              logger.trace("ID: " + t.asReferenceType().getId)
              logger.trace("QualifiedName: " + t.asReferenceType().getQualifiedName)
            }
            logger.trace("Node {} with parent {} has type {}", e.toString, e.getParentNode, t)
            type_map(e) = t
          }
          e match {
            case ne : NameExpr =>
              if(this.imported_class.contains(ne.getNameAsString)) {
                val class_name = this.imported_class(ne.getNameAsString)
                val t = {
                  val cd = new ClassOrInterfaceDeclaration(util.EnumSet.of(Modifier.PUBLIC), false, class_name.toString)
                  new ReferenceTypeImpl(new JavaParserClassDeclaration(cd, this.type_solver.getTypeSolver), this.type_solver.getTypeSolver)
                }
                type_map(ne) = t
                name_type_map(ne.getNameAsString) = t
                logger.trace("Name {} with parent {} has type {}", ne.toString, ne.getParentNode, t)
              }
            case vexpr: VariableDeclarationExpr =>
              vexpr.getVariables.foreach(vdor => {
                val name = new NameExpr(vdor.getName)
                name.setParentNode(vdor)
                if (!type_map.contains(name) || type_map(name).isWildcard) {
                  val t =
                    try {
                      this.type_solver.convertToUsageVariableType(vdor)
                    } catch {
                      case e: Throwable =>
                        logger.warn("Failed to get the type for name {}", name.getNameAsString)
                        val cd = new ClassOrInterfaceDeclaration(util.EnumSet.of(Modifier.PUBLIC), false, vdor.getType.toString)
                        new ReferenceTypeImpl(new JavaParserClassDeclaration(cd, this.type_solver.getTypeSolver), this.type_solver.getTypeSolver)
                    }
                  type_map(name) = t
                  name_type_map(name.getNameAsString) = t
                  logger.trace("Name {} with parent {} has type {}", name.toString, name.getParentNode, t)
                }
              })
            case _ =>
          }
        case p: Parameter =>
          val name = new NameExpr(p.getName)
          if (!type_map.contains(name) || type_map(name).isWildcard) {
            logger.trace("param type: " + p.getType)
            val t =
              try {
                this.type_solver.convert(p.getType, p)
              } catch {
                case _: Throwable =>
                  logger.warn("Failed to get the type for parameter {}", name.getNameAsString)
                  val cd = new ClassOrInterfaceDeclaration(util.EnumSet.of(Modifier.PUBLIC), false, p.getType.toString)
                  new ReferenceTypeImpl(new JavaParserClassDeclaration(cd, this.type_solver.getTypeSolver), this.type_solver.getTypeSolver)
              }
            type_map(name) = t
            name_type_map(name.getNameAsString) = t
            logger.trace("Parameter {} with parent {} has type {}", name.toString, name.getParentNode, t)
          }
        case _ =>
      }
    }

    val unknown_type_iter = JavaDraftIteratorFactory.create_draft_iterator(this,
      Some(n => n.asInstanceOf[JavaDraftNode].node.isInstanceOf[Expression] && type_map.contains(n.asInstanceOf[JavaDraftNode].node) && type_map(n.asInstanceOf[JavaDraftNode].node).isWildcard))

    while (unknown_type_iter.hasNext) {
      val hole = unknown_type_iter.next().asInstanceOf[JavaDraftNode].node.asInstanceOf[Expression]
      this.logger.trace("Inferring node with unknown type: {}", hole.toString)
      if (!type_map.contains(hole) || type_map(hole).isWildcard) {
        val my_t = infer_unknown_type(hole, type_map)
        val t =
          if (my_t.isWildcard) {
            try {
              this.type_solver.getType(hole)
            } catch {
              case _: Throwable => Wildcard.UNBOUNDED
            }
          } else {
            my_t
          }

        logger.trace("Unknown {} with parent {} has type {}", hole.toString, hole.getParentNode, t)
        type_map(hole) = t
      }
    }

    (type_map, name_type_map)
  }

  private def same_type(t1: Type, t2: Type): Boolean = {
    (t1, t2) match {
      // case (_, _) => true
      // case (k1 : Wildcard, _) => true
      // case (_, k2 : Wildcard) => true
      case (r1: ReferenceType, r2: ReferenceType) =>
        val id1 = r1.getId.split("\\.").last
        val id2 = r2.getId.split("\\.").last
        id1.equals(id2)
      case (f1: FuncType, f2: FuncType) => f1.equals(f2)
      case (_, _) if t1.isWildcard || t2.isWildcard => true
      case (_, _) => t1.equals(t2)
    }
  }

  private val subtype_cache: mutable.Map[(Type, Type), Boolean] = mutable.Map.empty[(Type, Type), Boolean]

  /**
    * Check if t1 is a subtype of t2
    */
  def is_subtype(t1: Type, t2: Type): Boolean = {
    if(!subtype_cache.contains((t1, t2))) {
      val r = try {
        // logger.debug("t1 : {}, t2 : {}", t1.describe(), t2.describe())
        (t1, t2) match {
          case _ if t1.isWildcard || t2.isWildcard => true
          case (ft1 : FuncType, ft2 : FuncType) =>
            // check ft2.param <: ft1.param
            if(ft1.params.size > ft2.params.size) {
              return false
            }
            val used_index = mutable.Set.empty[Int]
            for(pt1 <- ft1.params) {
              var done = false
              for(index <- 0 until ft2.params.size()) {
                val pt2 = ft2.params.get(index)
                if(!done && !used_index.contains(index) && this.is_subtype(pt2, pt1)) {
                  done = true
                  used_index += index
                }
              }
              if(!done) {
                return false
              }
            }
            // check ft1.ret <: ft2.ret
            this.is_subtype(ft1.ret, ft2.ret)
          case (_ : VoidType, _ : VoidType) => true
          case (a1 : ArrayType, a2 : ArrayType) => is_subtype(a1.getComponentType, a2.getComponentType)
          case _ =>
            if(t1.describe() == "java.lang.Object" || t2.describe() == "java.lang.Object") {
              true
            } else {
              t2.isAssignableBy(t1)
            }
        }
      } catch {
        case _ : IllegalStateException => false
        case _ : UnsupportedOperationException => false
      }
      subtype_cache((t1, t2)) = r
    }
    subtype_cache((t1, t2))
  }

  /**
    * A function for getting types from reflection classes
    */
  def class_to_type(clazz : Class[_]) : Type = JavaUtils.class_to_type(clazz, this.type_solver)

  // cache for quick retrieval for type_to_class function
  private val type_to_class_cache : mutable.Map[Type, Option[Class[_]]] = mutable.Map.empty[Type, Option[Class[_]]]

  /**
    * Convert a type object to a class object
    */
  def type_to_class(t : Type) : Option[Class[_]] = {
    if(type_to_class_cache.contains(t)) {
      return type_to_class_cache(t)
    }

    val c = t match {
      case PrimitiveType.BYTE => Some(classOf[Byte])
      case PrimitiveType.SHORT => Some(classOf[Short])
      case PrimitiveType.CHAR => Some(classOf[Char])
      case PrimitiveType.INT => Some(classOf[Int])
      case PrimitiveType.LONG => Some(classOf[Long])
      case PrimitiveType.BOOLEAN => Some(classOf[Boolean])
      case PrimitiveType.FLOAT => Some(classOf[Float])
      case PrimitiveType.DOUBLE => Some(classOf[Double])
      case _ if t.isArray =>
        val ct = t.asArrayType().getComponentType
        if(ct.isPrimitive) {
          Some(Class.forName("[" + (ct match {
            case PrimitiveType.BYTE => "B"
            case PrimitiveType.SHORT => "S"
            case PrimitiveType.CHAR => "C"
            case PrimitiveType.INT => "I"
            case PrimitiveType.LONG => "J"
            case PrimitiveType.BOOLEAN => "Z"
            case PrimitiveType.FLOAT => "F"
            case PrimitiveType.DOUBLE => "D"
          }), true, this.class_loader))
        } else if(ct.isArray) {
          Some(Class.forName("[" + type_to_class(ct).get.getName, true, this.class_loader))
        } else {
          Some(Class.forName("[L" + type_to_class(ct).get.getName + ";", true, this.class_loader))
        }
      case _ if t.isReferenceType =>
        Some(Class.forName(t.asReferenceType().getQualifiedName, true, this.class_loader))
      case _ => None
    }

    this.type_to_class_cache(t) = c
    c
  }

  /**
    * This function generates a set of methods and constants inside the class and also
    * their types.
    *
    * This returns a map from method/field name, type to a 2-tuple where
    * the first is the scope represented by a class object
    * the second is whether the method/field is static
    */
  def get_methods_from_class() : mutable.Map[(Name, Type), (Class[_], Boolean)] = {
    if(this.class_methods != null) {
      return this.class_methods
    }

    val result = mutable.Map.empty[(Name, Type), (Class[_], Boolean)]
    this.class_method_types = mutable.Map.empty[Name, Type]

    val imports = this.compilation_unit.getImports
    for(i <- 0 until imports.size()) {
      val full_classname = imports.get(i).getNameAsString

      // get the class from the class name
      val clazz : Class[_] = if(!full_classname.startsWith("java")) {
        Class.forName(full_classname, true, this.class_loader)
      } else {
        Class.forName(full_classname)
      }

      // get all the methods and their types
      clazz.getMethods
        .filter(m => java.lang.reflect.Modifier.isPublic(m.getModifiers))
        .foreach(m => {
          // get parameter types
          val param_types = m.getParameters.map(p => this.class_to_type(p.getType))

          // get return type
          val ret_type = this.class_to_type(m.getReturnType)

          // put the type into the set
          val method_name = JavaUtils.classname_to_name(clazz.getName + "." + m.getName)
          val ft = new FuncType(param_types, ret_type)
          this.class_method_types.put(method_name, ft)
          // result.put((JavaUtils.classname_to_name(clazz.getName + "." + m.getName), new FuncType(param_types, ret_type)), (clazz, java.lang.reflect.Modifier.isStatic(m.getModifiers)))
          result.put((method_name, ft), (clazz, java.lang.reflect.Modifier.isStatic(m.getModifiers)))

      })

      clazz.getFields
        .filter(f => java.lang.reflect.Modifier.isPublic(f.getModifiers))
        .foreach(f => {
          val t = class_to_type(f.getType)
          result.put((JavaUtils.classname_to_name(clazz.getName + "." + f.getName), t), (clazz, java.lang.reflect.Modifier.isStatic(f.getModifiers)))
      })
    }


    this.class_methods = result
    this.class_methods
  }


  /**
    * Check whether these two nodes serve the same role in the surrounding context
    */
  override def same_role(hole: DraftNode, n: DraftNode): Boolean = {
    val hole_node = hole.asInstanceOf[JavaDraftNode].node
    val n_node = n.asInstanceOf[JavaDraftNode].node
    val hole_parent = hole_node.getParentNode
    val n_parent = n_node.getParentNode

    if(hole_parent.isPresent != n_parent.isPresent) {
      return false
    }

    if(!hole_parent.isPresent && !n_parent.isPresent) {
      return true
    }

    if(!hole_parent.getClass.equals(n_parent.getClass)) {
      return false
    }

    (hole_parent.get(), n_parent.get()) match {

      // Expressions
      case (aae1 : ArrayAccessExpr, aae2 : ArrayAccessExpr) =>
        (aae1.getIndex.equals(hole_node) && aae2.getIndex.equals(n_node)) ||
          (aae1.getName.equals(hole_node) && aae2.getName.equals(n_node))
      case (acl1 : ArrayCreationLevel, acl2 : ArrayCreationLevel) =>
        (acl1.getAnnotations.contains(hole_node) && acl2.getAnnotations.contains(n_node)) ||
          (acl1.getDimension.isPresent && acl1.getDimension.get().equals(hole_node) && acl2.getDimension.isPresent && acl2.getDimension.get().equals(n_node)) ||
          (acl1.getNodeLists.contains(hole_node) && acl2.getNodeLists.contains(n_node))
      case (ace1 : ArrayCreationExpr, ace2 : ArrayCreationExpr) =>
        (ace1.getLevels.contains(hole_node) && ace2.getLevels.contains(n_node)) ||
          (ace1.getInitializer.isPresent && ace1.getInitializer.get().equals(hole_node) && ace2.getInitializer.isPresent && ace2.getInitializer.get().equals(n_node)) ||
          (ace1.getElementType.equals(hole_node) && ace2.getElementType.equals(n_node))
      case (aie1 : ArrayInitializerExpr, aie2 : ArrayInitializerExpr) =>
        aie1.getValues.contains(hole_node) && aie2.getValues.contains(n_node)
      case (ae1 : AssignExpr, ae2 : AssignExpr) =>
          (ae1.getTarget.equals(hole_node) && ae2.getTarget.equals(n_node)) ||
          (ae1.getValue.equals(hole_node) && ae2.getValue.equals(n_node))
      case (be1 : BinaryExpr, be2 : BinaryExpr) =>
        (be1.getLeft.equals(hole_node) && be2.getLeft.equals(n_node)) ||
          (be1.getRight.equals(hole_node) && be2.getRight.equals(n_node))
      case (_ : BooleanLiteralExpr, _ : BooleanLiteralExpr) => true
      case (ce1 : CastExpr, ce2 : CastExpr) =>
        (ce1.getExpression.equals(hole_node) && ce2.getExpression.equals(n_node)) ||
          (ce1.getType.equals(hole_node) && ce2.getType.equals(n_node))
      case (_ : CharLiteralExpr, _ : CharLiteralExpr) => true
      case (classe1 : ClassExpr, classe2 : ClassExpr) =>
        classe1.getType.equals(hole_node) && classe2.getType.equals(n_node)
      case (conde1 : ConditionalExpr, conde2 : ConditionalExpr) =>
        (conde1.getCondition.equals(hole_node) && conde2.getCondition.equals(n_node)) ||
          (conde1.getElseExpr.equals(hole_node) && conde2.getElseExpr.equals(n_node)) ||
          (conde1.getThenExpr.equals(hole_node) && conde2.getThenExpr.equals(n_node))
      case (_ : DoubleLiteralExpr, _ : DoubleLiteralExpr) => true
      case (enc1 : EnclosedExpr, enc2 : EnclosedExpr) =>
        enc1.getInner != null && enc1.getInner.equals(hole_node) && enc2.getInner != null && enc2.getInner.equals(n_node)
      case (fae1 : FieldAccessExpr, fae2 : FieldAccessExpr) =>
        (fae1.getName.equals(hole_node) && fae2.getName.equals(n_node)) ||
          (fae1.getScope.equals(hole_node) && fae2.getScope.equals(n_node)) ||
          (fae1.getTypeArguments.isPresent && fae1.getTypeArguments.get().contains(hole_node) && fae2.getTypeArguments.isPresent && fae2.getTypeArguments.get().contains(n_node))
      case (ioe1 : InstanceOfExpr, ioe2 : InstanceOfExpr) =>
        (ioe1.getExpression.equals(hole_node) && ioe2.getExpression.equals(n_node)) ||
          (ioe1.getType.equals(hole_node) && ioe2.getType.equals(n_node))
      case (_ : IntegerLiteralExpr, _ : IntegerLiteralExpr) => true
      case (_ : LambdaExpr, _ : LambdaExpr) => assume(false); false // TODO: Need to work on this one
      case (_ : LongLiteralExpr, _ : LongLiteralExpr) => true
      // case (mae1 : MarkerAnnotationExpr, mae2 : MarkerAnnotationExpr) =>
      case (mvp1 : MemberValuePair, mvp2 : MemberValuePair) =>
        (mvp1.getName.equals(hole_node) && mvp2.getName.equals(n_node)) ||
          mvp1.getValue.equals(hole_node) && mvp2.getValue.equals(n_node)
      case (mce1 : MethodCallExpr, mce2 : MethodCallExpr) =>
        (mce1.getArguments.contains(hole_node) && mce2.getArguments.contains(n_node)) ||
          (mce1.getName.equals(hole_node) && mce2.getName.equals(n_node)) ||
          (mce1.getScope.isPresent && mce1.getScope.get().equals(hole_node) && mce2.getScope.isPresent && mce2.getScope.get().equals(n_node))
      case (mre1 : MethodReferenceExpr, mre2 : MethodReferenceExpr) =>
        mre1.getScope.equals(hole_node) && mre2.getScope.equals(n_node)
      case (name1 : Name, name2 : Name) =>
        name1.getQualifier.isPresent && name1.getQualifier.get().equals(hole_node) && name2.getQualifier.isPresent && name2.getQualifier.get().equals(n_node)
      case (_ : NameExpr, _ : NameExpr) => true
      case (_ : NormalAnnotationExpr, _ : NormalAnnotationExpr) => assume(false); false // TODO: Unsupported
      case (_ : NullLiteralExpr, _ : NullLiteralExpr) => true
      case (oce1 : ObjectCreationExpr, oce2 : ObjectCreationExpr) =>
        (oce1.getAnonymousClassBody.isPresent && oce1.getAnonymousClassBody.get().contains(hole_node) && oce2.getAnonymousClassBody.isPresent && oce2.getAnonymousClassBody.get().contains(n_node)) ||
          (oce1.getArguments.contains(hole_node) && oce2.getArguments.contains(n_node)) ||
          (oce1.getScope.isPresent && oce1.getScope.get().equals(hole_node) && oce2.getScope.isPresent && oce2.getScope.get().equals(n_node)) ||
          (oce1.getType.equals(hole_node) && oce2.getType.equals(n_node))
      case (smae1 : SingleMemberAnnotationExpr, smae2 : SingleMemberAnnotationExpr) =>
        smae1.getMemberValue.equals(hole_node) && smae2.equals(n_node)
      case (_ : StringLiteralExpr, _ : StringLiteralExpr) => true
      case (_ : SuperExpr, _ : SuperExpr) => true
      case (_ : ThisExpr, _ : ThisExpr) => true
      case (tpe1 : TypeExpr, tpe2 : TypeExpr) =>
        tpe1.getType.equals(hole_node) && tpe2.getType.equals(n_node)
      case (ue1 : UnaryExpr, ue2 : UnaryExpr) =>
        ue1.getExpression.equals(hole_node) && ue2.getExpression.equals(n_node)
      case (vde1 : VariableDeclarationExpr, vde2 : VariableDeclarationExpr) =>
        (vde1.getAnnotations.contains(hole_node) && vde2.getAnnotations.contains(n_node)) ||
          (vde1.getVariables.contains(hole_node) && vde2.getVariables.contains(n_node))
      case (vdor1 : VariableDeclarator, vdor2 : VariableDeclarator) =>
        (vdor1.getName.equals(hole_node) && vdor2.getName.equals(n_node)) ||
        (vdor1.getType.equals(hole_node) && vdor2.getType.equals(n_node)) ||
        (vdor1.getInitializer.isPresent && vdor1.getInitializer.get().equals(hole_node) && vdor2.getInitializer.isPresent && vdor2.getInitializer.get().equals(n_node))

      // statements
      case (_ : ExpressionStmt, _ : ExpressionStmt) => true
      case (_ : ReturnStmt, _ : ReturnStmt) => true
      case (while1 : WhileStmt, while2 : WhileStmt) =>
        while1.getCondition.equals(hole_node) && while2.getCondition.equals(n_node)
      case (fes1 : ForeachStmt, fes2 : ForeachStmt) =>
        fes1.getIterable.equals(hole_node) && fes2.getIterable.equals(n_node) ||
        fes1.getBody.equals(hole_node) && fes2.getBody.equals(n_node)
      case (ifs1 : IfStmt, ifs2 : IfStmt) =>
        ifs1.getCondition.equals(hole_node) && ifs2.getCondition.equals(n_node)
      case _ => false
    }
  }

  /**
    * Check whether this hole is an expression hole or not
    */
  override def is_expr_hole(node: DraftHole): Boolean = {
    val jnode = node.asInstanceOf[JavaDraftHole].node
    JavaUtils.is_expr_hole(jnode)
  }

  override def is_expr(n: DraftNode): Boolean =
    n.asInstanceOf[JavaDraftNode].node.isInstanceOf[Expression]

  override def is_stmt(n: DraftNode): Boolean =
    n.asInstanceOf[JavaDraftNode].node.isInstanceOf[Statement]

  /**
    * Test whether this node is a variable
    */
  override def is_var(n : DraftNode) : Boolean = {
    val jnode = n.asInstanceOf[JavaDraftNode].node
    jnode match {
      case e : NameExpr =>
        if(this.imported_class.contains(e.getNameAsString)) {
          return false
        }

        val t = this.get_type(n).asInstanceOf[JavaDraftNodeType].t
        logger.debug("is var type for {} : {}", e.getNameAsString, t)
        t match {
          case rt : ReferenceType => !rt.getId.equals(e.getNameAsString)
          case _ => true
        }
      case _ => false
    }
  }

  /**
    * This function is used to rename the variables to avoid name collision. It takes a string transformation function
    * and applies it to every variable name in the draft method
    */
  override def rename(f: String => String, cond : Option[DraftNode => Boolean]): Draft = {
    val new_method = this.method.clone()

    // get all variable names that is local to the method
    val local_names = mutable.Set.empty[String]
    new VoidVisitorAdapter[Object] {
      override def visit(name : NameExpr, obj : Object) : Unit = {
        if(cond.isEmpty) {
          local_names += name.getName.getIdentifier
        } else {
          val node = new JavaDraftNode(name)
          if(cond.get(node)) {
            local_names += name.getName.getIdentifier
          }
        }
      }

      override def visit(vdor : VariableDeclarator, obj : Object) : Unit = {
        if(cond.isEmpty) {
          local_names += vdor.getName.getIdentifier
        } else {
          val node = new JavaDraftNode(new NameExpr(vdor.getName.getIdentifier))
          if(cond.get(node)) {
            local_names += vdor.getName.getIdentifier
          }
        }
      }

      override def visit(param : Parameter, obj : Object) : Unit = {
        local_names += param.getName.getIdentifier
      }
    }.visit(new_method, null)

    // rename all the variables here
    for(name <- local_names) {
      JavaUtils.rename(new_method, name, f(name))
    }

    this.create_new_draft(new_method)
  }

  /**
    * This is used to rename a single variable
    */
  override def rename(from: String, to: String): Draft = {
    val new_method = this.method.clone()
    JavaUtils.rename(new_method, from, to)

    this.create_new_draft(new_method)
  }

  /**
    * Rename an arbitrary node
    */
  override def rename(node : DraftNode, from : String, to : String) : DraftNode = {
    val new_node = node.asInstanceOf[JavaDraftNode].node.clone()
    JavaUtils.rename(new_node, from, to)
    new JavaDraftNode(new_node)
  }

  /**
    * Check whether the renaming is valid or not.
 *
    * @param completed_draft - the completed draft
    * @param unref - the reference that needs to be replaced
    * @param relevant_prog - the completed draft where new references come from
    * @param r - the new reference
    */
  override def valid_rename(completed_draft : Draft, unref : DraftNode,
                            relevant_prog : Draft, r : DraftNode) : Boolean = {
    val unref_type = relevant_prog.get_type(unref).asInstanceOf[JavaDraftNodeType].t
    val r_type = completed_draft.get_type(r).asInstanceOf[JavaDraftNodeType].t
    logger.debug("unref type: " + unref_type)
    logger.debug("R type: " + r_type)
    if(r_type.isWildcard) { return false }
    completed_draft.asInstanceOf[JavaDraft].is_subtype(unref_type, r_type)
  }

  /**
    * Check whether the expression filling is valid
    *
    * @param working_draft - the draft that receives the filling
    * @param hole - the hole to be filled
    * @param relevant_prog - the draft where the new node is from
    * @param n - the new node
    * @return true if this filling is valid
    */
  override def valid_expr_fill(working_draft : Draft, hole : DraftNode,
                               relevant_prog : Draft, n : DraftNode) : Boolean = {

    // check role
    if(!working_draft.same_role(hole, n)) { return false }
    this.logger.debug("They are serving the same role.")

    val hole_type = working_draft.get_type(hole).asInstanceOf[JavaDraftNodeType].t
    val n_type = relevant_prog.get_type(n).asInstanceOf[JavaDraftNodeType].t
    logger.debug("hole type: " + hole_type)
    logger.debug("N type: " + n_type)
    if(n_type.isWildcard) { return false }
    if(hole_type.isWildcard) {
      this.logger.debug("Hole type is unknown.")
      return true
    }

    // check type
    if(!working_draft.asInstanceOf[JavaDraft].is_subtype(hole_type, n_type)) { return false }
    this.logger.debug("They are of the same type.")

    true

  }

  /**
    * Abstract an expression based on the draft environment. Basically, we turn a subexpression into a hole
    * if the draft has at least one node that has the same type for substitution.
    */
  private def abstract_expr(expr : Expression, src_draft : JavaDraft) : Seq[Expression] = {
    // this.logger.debug("Abstracting " + expr.toString())
    val result = mutable.Set.empty[Expression]

    val iter = JavaDraftIteratorFactory.create_draftnode_iterator(new JavaDraftNode(expr), Some(p => src_draft.is_expr(p)))
    val all_children = iter.get_all_nodes

    def has_undefined_names(node : Expression) : Boolean = {
      node match {
        case _ : NameExpr => this.is_defined(new JavaDraftNode(node))
        case _ =>
          val name_iter = JavaDraftIteratorFactory.create_draftnode_ref_iterator(new JavaDraftNode(node), Some(p => !this.is_defined(p)))
          name_iter.hasNext
      }
    }

    def helper(node : Expression, pos : Int) : Unit = {
      if(pos >= all_children.length) {
        if(!has_undefined_names(node)) {
          result += node
        }
        return
      }

      val child = all_children(pos).asInstanceOf[JavaDraftNode].node
      helper(node, pos + 1)
      if(src_draft.type_env.contains(child) && !src_draft.type_env(child).isWildcard) {
        val child_type = this.type_to_class(src_draft.type_env(child))
        node match {
          // Expressions
          case aae : ArrayAccessExpr =>
            if(aae.getIndex == child) {
              helper(aae.clone().setIndex(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
            if(aae.getName == child) {
              helper(aae.clone().setName(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
            /*
          case acl : ArrayCreationLevel =>
            if(acl.getDimension.isPresent && acl.getDimension.get() == child) {
              helper(acl.clone().setDimension(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
            */
          case ace : ArrayCreationExpr =>
            if(ace.getLevels.contains(child)) {
              val index = ace.getLevels.indexOf(child)
              val new_node = ace.clone()
              new_node.getLevels.get(index).setParentNode(null)
              new_node.getLevels.set(index, new ArrayCreationLevel(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))))
              helper(new_node, pos + 1)
            }
          case aie : ArrayInitializerExpr =>
            if(aie.getValues.contains(child)) {
              val index = aie.getValues.indexOf(child)
              helper(aie.clone().getValues.set(index, JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          case ae : AssignExpr =>
            if(ae.getTarget == child) {
              helper(ae.clone().setTarget(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            } else if(ae.getValue == node) {
              helper(ae.clone().setValue(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          case be : BinaryExpr =>
            if(be.getLeft == child) {
              helper(be.clone().setLeft(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            } else if(be.getRight == node) {
              helper(be.clone().setRight(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          // case (ble1 : BooleanLiteralExpr, ble2 : BooleanLiteralExpr) => true
          case ce : CastExpr =>
            if(ce.getExpression == node) {
              helper(ce.clone().setExpression(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          // case (cle1 : CharLiteralExpr, cle2 : CharLiteralExpr) => true
          // case classe : ClassExpr => classe1.getType.equals(hole_node) && classe2.getType.equals(n_node)
          case conde : ConditionalExpr =>
            if(conde.getCondition == child) {
              helper(conde.clone().setCondition(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            } else if(conde.getElseExpr == child) {
              helper(conde.clone().setElseExpr(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            } else if(conde.getThenExpr == child) {
              helper(conde.clone().setThenExpr(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          // case (dle1 : DoubleLiteralExpr, dle2 : DoubleLiteralExpr) => true
          case enc : EnclosedExpr =>
            if(enc.getInner == child) {
              helper(enc.clone().setInner(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          case fae : FieldAccessExpr =>
            if(fae.getScope == child) {
              helper(fae.clone().setScope(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          case ioe : InstanceOfExpr =>
            if(ioe.getExpression == child) {
              helper(ioe.clone().setExpression(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          // case (ile1 : IntegerLiteralExpr, ile2 : IntegerLiteralExpr) => true
          case _ : LambdaExpr => assume(false) // TODO: Need to work on this one
          // case (lte1 : LongLiteralExpr, lte2 : LongLiteralExpr) => true
          // case (mae1 : MarkerAnnotationExpr, mae2 : MarkerAnnotationExpr) =>
            /*
          case mvp : MemberValuePair =>
            if(mvp.getValue == child) {
              helper(mvp.clone().setValue(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
            */
          case mce : MethodCallExpr =>
            if(mce.getArguments.contains(child)) {
              val index = mce.getArguments.indexOf(child)
              // val new_node = mce.clone().getArguments.set(index, JavaUtils.gen_expr_hole_without_test())
              val new_node = mce.clone()
              new_node.getArguments.get(index).setParentNode(null)
              new_node.getArguments.set(index, JavaUtils.gen_expr_hole_without_test(child_type))
              helper(new_node, pos + 1)
            } else if(mce.getScope.isPresent && mce.getScope.get() == child) {
              // we replace the scope with expressions in the draft program that has the same type
              val new_node = mce.clone()
              new_node.setScope(JavaUtils.gen_expr_hole_without_test(child_type))
              helper(new_node, pos + 1)
            }
          case mre : MethodReferenceExpr =>
            if(mre.getScope == child) {
              helper(mre.clone().setScope(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          // case name : Name => if(name.getQualifier.isPresent && name.getQualifier.get() == child) { }
          // case (ne1 : NameExpr, ne2 : NameExpr) => true
          case _ : NormalAnnotationExpr => assume(false) // TODO: Unsupported
          // case (nle1 : NullLiteralExpr, nle2 : NullLiteralExpr) => true
          case oce : ObjectCreationExpr =>
            if(oce.getArguments.contains(child)) {
              val index = oce.getArguments.indexOf(child)
              val new_node = oce.clone()
              new_node.getArguments.get(index).setParentNode(null)
              new_node.getArguments.set(index, JavaUtils.gen_expr_hole_without_test(child_type))
              helper(new_node, pos + 1)
            } else if(oce.getScope.isPresent && oce.getScope.get() == child) {
              helper(oce.clone().setScope(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          case smae : SingleMemberAnnotationExpr =>
            if(smae.getMemberValue == child) {
              helper(smae.clone().setMemberValue(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          // case (sle1 : StringLiteralExpr, sle2 : StringLiteralExpr) => true
          // case (se1 : SuperExpr, se2 : SuperExpr) => true
          // case (te1 : ThisExpr, te2 : ThisExpr) => true
          // case (tpe1 : TypeExpr, tpe2 : TypeExpr) => tpe1.getType.equals(hole_node) && tpe2.getType.equals(n_node)
          case ue : UnaryExpr =>
            if(ue.getExpression == child) {
              helper(ue.clone().setExpression(JavaUtils.gen_expr_hole_without_test(child_type)), pos + 1)
            }
          case vde : VariableDeclarationExpr =>
            for(i <- 0 until vde.getNodeLists.size()) {
              val vdor = vde.getNodeLists.get(i).asInstanceOf[VariableDeclarator]
              if(vdor.getInitializer.isPresent && vdor == child) {
                // make new declarators
                val new_vdors = vde.getNodeLists.clone().asInstanceOf[NodeList[VariableDeclarator]]

                // make a hole
                val new_vdor = new_vdors.get(i)
                new_vdors.set(i, new_vdor.setInitializer(JavaUtils.gen_expr_hole_without_test(child_type)))
                helper(new VariableDeclarationExpr(new_vdors), pos + 1)
              }
            }

          // statements
          // case (es1 : ExpressionStmt, es2 : ExpressionStmt) => true
          // case (r1 : ReturnStmt, r2 : ReturnStmt) => true
          // case (while1 : WhileStmt, while2 : WhileStmt) => while1.getCondition.equals(hole_node) && while2.getCondition.equals(n_node)
          case _ =>
        }
      }
    }

    // new TreeVisitor { override def process(node: Node): Unit = helper(node, 0) }.visitPostOrder(expr)
    helper(expr, 0)
    result.toSeq
  }

  /**
    * Extract methods from the imported class in the given draft
    */
  private def extract_methods(draft : JavaDraft): mutable.Map[Type, mutable.Set[Expression]] = {
    val result = mutable.Map.empty[Type, mutable.Set[Expression]]
    draft.get_methods_from_class()
      .filter(m => m._1._2.isInstanceOf[FuncType])

      // ignoring .equals method
      .filter(m => m._1._1.getId != "equals")

      .foreach(m => {
        val is_static = m._2._2
        val method_type = m._1._2.asInstanceOf[FuncType]
        if(is_static) {
          // this is a static method. Construct a method call directly using the class name
          assert(m._1._1.getQualifier.isPresent, "This has to have a qualifier.")
          val scope =
            if(!m._1._1.getQualifier.get().getQualifier.isPresent)
              new NameExpr(m._1._1.getQualifier.get().getIdentifier)
            else
              JavaUtils.qname_to_fieldaccess(m._1._1.getQualifier.get())
          val new_node = new MethodCallExpr(scope, m._1._1.getIdentifier)

          if(!result.contains(method_type.ret)) {
            result.put(method_type.ret, mutable.Set.empty[Expression])
          }
          result(method_type.ret).add(new_node)
        } else {
          // this is not a static method. construct a method call
          // construct a list of holes
          val param_list = new NodeList[Expression]
          for(pt <- method_type.params) {
            param_list.add(JavaUtils.gen_expr_hole_without_test(this.type_to_class(pt)))
          }

          // construct the method call and add it into the result list
          val new_node = new MethodCallExpr(
            // nt._1.asInstanceOf[Expression],
            JavaUtils.gen_expr_hole_without_test(Some(m._2._1)),
            m._1._1.getIdentifier,
            param_list)
          if(!result.contains(method_type.ret)) {
            result.put(method_type.ret, mutable.Set.empty[Expression])
          }
          result(method_type.ret).add(new_node)
        }
      })
    result
  }

  /**
    * Extract fields from the imported classes in the given draft
    */
  private def extract_fields(draft : JavaDraft): mutable.Map[Type, mutable.Set[Expression]] = {
    val result = mutable.Map.empty[Type, mutable.Set[Expression]]
    draft.get_methods_from_class()
      .filter(f => !f._1._2.isInstanceOf[FuncType])
      .foreach(f => {
        val is_static = f._2._2
        val field_type = f._1._2

        if(is_static) {
          // this is a static field. Just construct the field access expression directly using the class name
          // construct the field access hole
          val new_node = JavaUtils.qname_to_fieldaccess(f._1._1)

          // put the hole into the result list
          if(!result.contains(field_type)) {
            result.put(field_type, mutable.Set.empty[Expression])
          }
          result(field_type).add(new_node)
        } else {
          // this isn't a static field. Find a scope in the environment and use that to construct
          // this field access expression
          this.type_env
            // we need a scope that has the same type as the class of the field
            .filter(nt => !JavaUtils.has_hole(nt._1) && draft.is_subtype(nt._2, draft.class_to_type(f._2._1)))
            .foreach(nt => {
              val new_node = new FieldAccessExpr(nt._1.asInstanceOf[Expression], f._1._1.getIdentifier)
              if(!result.contains(field_type)) {
                result.put(field_type, mutable.Set.empty[Expression])
              }
              result(field_type).add(new_node)
            })
        }
      })
    result
  }

  /**
    * This function extracts expressions from a draft
    */
  private def extract_expr(hole : DraftHole, draft : JavaDraft) : Seq[(Expression, Type)] = {
    val result = mutable.ArrayBuffer.empty[(Expression, Type)]
    val iter = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(p => draft.is_expr(p)))
    while(iter.hasNext) {
      val node = iter.next()
      if(this.same_role(hole, node)) {
        val node_type = draft.get_type(node).asInstanceOf[JavaDraftNodeType].t
        node.asInstanceOf[JavaDraftNode].node match {
          case name : NameExpr =>
            if(this.type_env.contains(name)) {
              result += ((name, node_type))
              // add_to_cache(name, node_type)
            }
          case _ =>
            this.abstract_expr(node.asInstanceOf[JavaDraftNode].node.asInstanceOf[Expression], draft).foreach(n => {
              result += ((n, node_type))
              // add_to_cache(n, node_type)
            })
        }
      }
    }
    result
  }


  /**
    * This stores a set of transformation function for making new expression when doing local search on relevant
    * expressions. This maps from the input expression type to a list of transformation functions along with their
    * return types.
    */
  private val relevant_step_funcs_cache : mutable.Map[Type, mutable.ArrayBuffer[((Expression => Expression), Type)]] = mutable.Map.empty[Type, mutable.ArrayBuffer[((Expression => Expression), Type)]]
  /**
    * Get a list of relevant expression from some relevant programs
    */
  def get_relevant_exprs(hole : DraftHole, relevant_progs : Seq[Draft]) : Seq[DraftNode] = {
    assume(this.is_expr_hole(hole))
    val result = mutable.ArrayBuffer.empty[DraftNode]
    val parent = hole.asInstanceOf[JavaDraftHole].node.getParentNode.get()

    def add_to_cache(skel_expr : Expression, in_type : Type, ret_type : Type) : Unit = {
      if(!this.relevant_step_funcs_cache.contains(in_type)) {
        this.relevant_step_funcs_cache.put(in_type, mutable.ArrayBuffer.empty[((Expression => Expression), Type)])
      }

      // make a function that makes new expressions
      val skel_hole = JavaUtils.get_expr_hole(skel_expr).get
      val func = (e: Expression) => JavaUtils.replace(skel_expr, skel_hole, e).asInstanceOf[Expression]
      this.relevant_step_funcs_cache(in_type) += ((func, ret_type))
    }

    def get_relevant_steps : mutable.Map[Type, mutable.ArrayBuffer[((Expression => Expression), Type)]] = {
      /*
      if(relevant_step_funcs_cache.nonEmpty) {
        return this.relevant_step_funcs_cache
      }
      */
      this.relevant_step_funcs_cache.clear()
      for(e <- JavaUtils.EXPRS) {
        e match {
            /*
          case _ : ArrayAccessExpr if !parent.isInstanceOf[ExpressionStmt] =>
            for(vname <- this.defset) {
              val name_node = new NameExpr(vname)
              val t = this.get_type(new JavaDraftNode(name_node)).asInstanceOf[JavaDraftNodeType].t
              if(t.isArray) {
                add_to_cache(new ArrayAccessExpr()
                  .setName(name_node)
                  .setIndex(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))),
                  t.asArrayType().getComponentType)
              }
            }
            */
          // case ace : ArrayCreationExpr =>
          // case aie : ArrayInitializerExpr =>
          case _ : BinaryExpr =>
            /*
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.AND)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean]))), PrimitiveType.BOOLEAN)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.BINARY_AND)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.BINARY_OR)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.DIVIDE)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              */
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.EQUALS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(None))
              .setRight(new NullLiteralExpr()), Wildcard.UNBOUNDED, PrimitiveType.BOOLEAN)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.NOT_EQUALS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(None))
              .setRight(new NullLiteralExpr()), Wildcard.UNBOUNDED, PrimitiveType.BOOLEAN)
            /*
            // having holes with no types might not be a good idea
            result += ((new BinaryExpr()
              .setOperator(BinaryExpr.Operator.EQUALS)
              .setLeft(JavaUtils.gen_expr_hole_without_test())
              .setRight(JavaUtils.gen_expr_hole_without_test()), PrimitiveType.BOOLEAN))
            result += ((new BinaryExpr()
              .setOperator(BinaryExpr.Operator.NOT_EQUALS)
              .setLeft(JavaUtils.gen_expr_hole_without_test())
              .setRight(JavaUtils.gen_expr_hole_without_test()), PrimitiveType.DOUBLE))
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.GREATER)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.GREATER_EQUALS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.LEFT_SHIFT)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.LESS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.LESS_EQUALS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              */
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.MINUS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
              .setRight(new DoubleLiteralExpr("1.0")), PrimitiveType.DOUBLE, PrimitiveType.DOUBLE)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.MINUS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(new IntegerLiteralExpr("1")), PrimitiveType.INT, PrimitiveType.INT)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.MULTIPLY)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
              .setRight(new DoubleLiteralExpr("0.0")), PrimitiveType.DOUBLE, PrimitiveType.DOUBLE)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.MULTIPLY)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(new IntegerLiteralExpr("0")), PrimitiveType.INT, PrimitiveType.INT)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.PLUS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
              .setRight(new DoubleLiteralExpr("1.0")), PrimitiveType.DOUBLE, PrimitiveType.DOUBLE)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.PLUS)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(new IntegerLiteralExpr("1")), PrimitiveType.INT, PrimitiveType.INT)
            /*
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.REMAINDER)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.SIGNED_RIGHT_SHIFT)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.UNSIGNED_RIGHT_SHIFT)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new BinaryExpr()
              .setOperator(BinaryExpr.Operator.XOR)
              .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
              .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
          case _ : BooleanLiteralExpr =>
            add_to_cache(new BooleanLiteralExpr(true), PrimitiveType.BOOLEAN)
            add_to_cache(new BooleanLiteralExpr(false), PrimitiveType.BOOLEAN)
              */
          // case cle : CharLiteralExpr =>
          /*
          // having a conditional expression might not be a good thing
        case ce : ConditionalExpr =>
          result += ((new ConditionalExpr()
            .setCondition(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
            .setThenExpr(JavaUtils.gen_expr_hole_without_test())
            .setElseExpr(JavaUtils.gen_expr_hole_without_test()),
            Wildcard.UNBOUNDED))
          case _ : DoubleLiteralExpr =>
            add_to_cache(new DoubleLiteralExpr("1.0"), PrimitiveType.DOUBLE)
            add_to_cache(new DoubleLiteralExpr("2.0"), PrimitiveType.DOUBLE)
            add_to_cache(new DoubleLiteralExpr("3.0"), PrimitiveType.DOUBLE)
            add_to_cache(new DoubleLiteralExpr("0.0"), PrimitiveType.DOUBLE)
            add_to_cache(new DoubleLiteralExpr("-1.0"), PrimitiveType.DOUBLE)
            add_to_cache(new DoubleLiteralExpr("-2.0"), PrimitiveType.DOUBLE)
            add_to_cache(new DoubleLiteralExpr("-3.0"), PrimitiveType.DOUBLE)
          case _ : FieldAccessExpr =>
            relevant_progs.asInstanceOf[Seq[JavaDraft]].+:(this).foreach(d => {
              this.extract_fields(d).foreach(p => {
                if(!this.valid_expr_fills_cache.contains(p._1)) {
                  this.valid_expr_fills_cache(p._1) = mutable.Set.empty[(Expression, Boolean)]
                }
                this.valid_expr_fills_cache(p._1) ++= p._2.map(n => (n, JavaUtils.has_hole(n)))
              })
            })
          // case ioe : InstanceOfExpr =>
          case _ : IntegerLiteralExpr =>
            add_to_cache(new IntegerLiteralExpr("1"), PrimitiveType.INT)
            add_to_cache(new IntegerLiteralExpr("2"), PrimitiveType.INT)
            add_to_cache(new IntegerLiteralExpr("3"), PrimitiveType.INT)
            add_to_cache(new IntegerLiteralExpr("0"), PrimitiveType.INT)
            add_to_cache(new IntegerLiteralExpr("-1"), PrimitiveType.INT)
            add_to_cache(new IntegerLiteralExpr("-2"), PrimitiveType.INT)
            add_to_cache(new IntegerLiteralExpr("-3"), PrimitiveType.INT)
          case _ : LongLiteralExpr =>
            add_to_cache(new LongLiteralExpr("1"), PrimitiveType.LONG)
            add_to_cache(new LongLiteralExpr("2"), PrimitiveType.LONG)
            add_to_cache(new LongLiteralExpr("3"), PrimitiveType.LONG)
            add_to_cache(new LongLiteralExpr("0"), PrimitiveType.LONG)
            add_to_cache(new LongLiteralExpr("-1"), PrimitiveType.LONG)
            add_to_cache(new LongLiteralExpr("-2"), PrimitiveType.LONG)
            add_to_cache(new LongLiteralExpr("-3"), PrimitiveType.LONG)
          case _ : MethodCallExpr =>
            relevant_progs.asInstanceOf[Seq[JavaDraft]].+:(this).foreach(d => {
              this.extract_methods(d).foreach(p => {
                if(!this.valid_expr_fills_cache.contains(p._1)) {
                  this.valid_expr_fills_cache(p._1) = mutable.Set.empty[(Expression, Boolean)]
                }
                this.valid_expr_fills_cache(p._1) ++= p._2.map(n => (n, JavaUtils.has_hole(n)))
              })
            })
          case _ : Name =>
            for(vname <- this.defset) {
              val name_node = new NameExpr(vname)
              val t = this.get_type(new JavaDraftNode(name_node)).asInstanceOf[JavaDraftNodeType].t
              add_to_cache(name_node, t)
            }

            // we want to use class names if the hole is a scope of a method call
            parent match {
              case mce : MethodCallExpr if mce.getScope.isPresent && mce.getScope.get() == hole.asInstanceOf[JavaDraftHole].node =>
                // loop through all the imported classes and put them into the results
                val imports = this.compilation_unit.getImports
                for(i <- 0 until imports.size()) {
                  val ip = imports.get(i)
                  val ct = this.str_to_type(ip.getNameAsString)
                  if(!this.valid_expr_fills_cache.contains(ct)) {
                    this.valid_expr_fills_cache(ct) = mutable.Set.empty[(Expression, Boolean)]
                  }
                  this.valid_expr_fills_cache(ct) += ((JavaUtils.qname_to_fieldaccess(ip.getName), false))
                }
              case _ =>
            }
          // case ne : NameExpr =>
          case _ : NullLiteralExpr =>
          // skipping null
          // result += ((new NullLiteralExpr(), NullType.INSTANCE))
          // case oce : ObjectCreationExpr =>
          case _ : StringLiteralExpr =>
            val t = new ReferenceTypeImpl(new ReflectionClassDeclaration(classOf[String], this.type_solver.getTypeSolver), this.type_solver.getTypeSolver)
            add_to_cache(new StringLiteralExpr(""), t)
            add_to_cache(new StringLiteralExpr(" "), t)
            add_to_cache(new StringLiteralExpr("\t"), t)
            add_to_cache(new StringLiteralExpr("\n"), t)
            */
          // case te : ThisExpr => result += ((new ThisExpr(), Wildcard.UNBOUNDED))
          case _ : UnaryExpr =>
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.MINUS)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE, PrimitiveType.DOUBLE)
            /*
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.BITWISE_COMPLEMENT)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.LOGICAL_COMPLEMENT)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean]))), PrimitiveType.BOOLEAN)
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.MINUS)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.PLUS)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.POSTFIX_DECREMENT)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.POSTFIX_INCREMENT)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.PREFIX_DECREMENT)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
            add_to_cache(new UnaryExpr()
              .setOperator(UnaryExpr.Operator.PREFIX_INCREMENT)
              .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              */
          // case vde : VariableDeclarationExpr =>
          case _ =>
        }
      }
      this.relevant_step_funcs_cache
    }

    // get hole type
    /*
    var hole_type = this.get_type(hole).asInstanceOf[JavaDraftNodeType].t
    if(hole_type.isWildcard) {
      hole_type = hole.asInstanceOf[JavaDraftHole].ttype
    }
    */
    var hole_type = hole.asInstanceOf[JavaDraftHole].ttype
    if(hole_type.isWildcard) {
      hole_type = this.get_type(hole).asInstanceOf[JavaDraftNodeType].t
    }

    this.logger.debug("Hole type : {}", hole_type.describe())

    def local_search(rel_expr : Expression, limit : Int = 3) : mutable.Set[Expression] = {
      val result : mutable.Set[Expression] = mutable.Set.empty[Expression]

      def helper(old_exprs : Set[Expression], step : Int) : Unit = {
        if(step == limit) { return }
        old_exprs.foreach(e => {
          val new_exprs = get_relevant_steps(hole_type).map(fe => fe._1(e)).toSet
          new_exprs.foreach(ne => result += ne)
          helper(new_exprs, step + 1)
        })
      }

      helper(Set(rel_expr), 0)
      result
    }

    // get exprs from the relevant programs
    relevant_progs.asInstanceOf[Seq[JavaDraft]]
      .map(rp => this.extract_expr(hole, rp).filter(p => this.is_subtype(p._2, hole_type)))
      .foldLeft(mutable.Set.empty[Expression])((acc, seq) => acc ++ seq.map(_._1))
      .map(e => local_search(e))
      .foreach(se => se.foreach(e => result += new JavaDraftNode(e)))

    // for each expr, we do a depth limited local search
    result
  }


  // the boolean represents whether the node has a hole or not
  private val valid_expr_fills_cache : mutable.Map[Type, mutable.Set[(Expression, Boolean)]] = mutable.Map.empty[Type, mutable.Set[(Expression, Boolean)]]
  /**
    * Return a sequence of valid fillings for a given hole from scratch. A list of relevant programs can be given to
    * provide more expressions for filling.
    */
  override def get_valid_expr_fills(hole: DraftHole, relevant_progs : Seq[Draft]): Seq[DraftNode] = {
    assume(this.is_expr_hole(hole))

    // get hole type
    /*
    var hole_type = this.get_type(hole).asInstanceOf[JavaDraftNodeType].t
    if(hole_type.isWildcard) {
      hole_type = hole.asInstanceOf[JavaDraftHole].ttype
    }
    */
    var hole_type = hole.asInstanceOf[JavaDraftHole].ttype
    if(hole_type.isWildcard) {
      hole_type = this.get_type(hole).asInstanceOf[JavaDraftNodeType].t
    }

    this.logger.debug("Hole type : {}", hole_type.describe())

    val parent = hole.asInstanceOf[JavaDraftHole].node.getParentNode.get()

    /**
      * Filter that applied universally despite the hole's parent
      */
    def universal_filter(cand_expr : Node) : Boolean = {
      cand_expr match {
        case mce : MethodCallExpr if mce.getScope.isPresent =>
          !mce.getScope.get().isInstanceOf[NullLiteralExpr]
        case _ => true
      }
    }

    /**
      * Filter that applied based on parent
      */
    def parent_filter(cand_expr : Node, parent : Node) : Boolean = {
      cand_expr match {
        case _ : NullLiteralExpr if parent.isInstanceOf[MethodCallExpr] =>
          !parent.asInstanceOf[MethodCallExpr].getScope.isPresent ||
            parent.asInstanceOf[MethodCallExpr].getScope.get() != hole.asInstanceOf[JavaDraftHole].node
        case _ : BooleanLiteralExpr =>
          parent.isInstanceOf[AssignExpr] || parent.isInstanceOf[VariableDeclarator]
        case ue : UnaryExpr if parent.isInstanceOf[UnaryExpr] =>
          val pue = parent.asInstanceOf[UnaryExpr]
          !ue.getOperator.equals(pue.getOperator)
        case _ => true
      }
    }


    if(this.valid_expr_fills_cache.nonEmpty) {
      val modifier = new TreeVisitor {
        def mk_new_hole(node : Node) : Node = {
          val result = node.clone()
          this.visitPostOrder(result)
          result
        }

        override def process(node: Node): Unit = {
          node match {
            case mce : MethodCallExpr if mce.getNameAsString.equals(JavaUtils.HOLE_NAME) =>
              val new_hole = JavaUtils.gen_expr_hole_without_test().asInstanceOf[MethodCallExpr]
              mce.getArguments.set(0, new_hole.getArguments.get(0))
            case _ =>
          }
        }
      }

      if(hole_type.isWildcard) {
        val result = this.valid_expr_fills_cache.values
          .foldLeft(Seq.empty[Node])((acc, s) => acc ++ s.map(n => if(n._2) modifier.mk_new_hole(n._1) else n._1))
          .filter(n => parent_filter(n, parent))
          .map(n => new JavaDraftNode(n))
        result.foreach(n => this.logger.debug("Valid Expr Fills {}", n.node))
        return result
      } else {
        val result = this.valid_expr_fills_cache.foldLeft(Seq.empty[Node])((acc, p) => {
          if(p._1.isWildcard || this.is_subtype(p._1, hole_type)) {
            acc ++ p._2.map(n => if(n._2) modifier.mk_new_hole(n._1) else n._1).toSeq
          } else {
            acc
          }
        }).filter(n => parent_filter(n, parent))
          .map(n => new JavaDraftNode(n))
        result.foreach(n => this.logger.debug("Valid Expr Fills {}", n.node))
        return result
      }
    }

    def add_to_cache(node : Expression, t : Type) : Unit = {
      if(!this.valid_expr_fills_cache.contains(t)) {
        this.valid_expr_fills_cache.put(t, mutable.Set.empty[(Expression, Boolean)])
      }
      this.valid_expr_fills_cache(t) += ((node, JavaUtils.has_hole(node)))
    }

    val seen = mutable.Set.empty[String]

    if(this.valid_expr_fills_cache.isEmpty) {
      relevant_progs.foreach(rp => {
        val iter = JavaDraftIteratorFactory.create_draft_iterator(rp, Some(p => rp.is_expr(p)))
        while(iter.hasNext) {
          val rp_node = iter.next().asInstanceOf[JavaDraftNode]
          val e = rp_node.node.asInstanceOf[Expression]
          val rp_node_type = rp.get_type(rp_node).asInstanceOf[JavaDraftNodeType].t
          e match {
            case aae : ArrayAccessExpr if !parent.isInstanceOf[ExpressionStmt] && !seen.contains(aae.getClass.toString) =>
              seen += aae.getClass.toString
              for(vname <- this.defset) {
                val name_node = new NameExpr(vname)
                val t = this.get_type(new JavaDraftNode(name_node)).asInstanceOf[JavaDraftNodeType].t
                if(t.isArray) {
                  add_to_cache(new ArrayAccessExpr()
                    .setName(name_node)
                    .setIndex(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))),
                    t.asArrayType().getComponentType)
                }
              }
            // case ace : ArrayCreationExpr =>
            // case aie : ArrayInitializerExpr =>
            case be : BinaryExpr =>
              val op = be.getOperator
              val left_type = rp.get_type(new JavaDraftNode(be.getLeft)).asInstanceOf[JavaDraftNodeType].t
              val right_type = rp.get_type(new JavaDraftNode(be.getRight)).asInstanceOf[JavaDraftNodeType].t
              val signature_str = be.getClass + " " + left_type.toString + " " + op.toString + " " + right_type.toString
              if(!seen.contains(signature_str)) {
                seen += signature_str
                add_to_cache(new BinaryExpr()
                  .setOperator(be.getOperator)
                  .setLeft(JavaUtils.gen_expr_hole_without_test(rp.asInstanceOf[JavaDraft].type_to_class(left_type)))
                  .setRight(JavaUtils.gen_expr_hole_without_test(rp.asInstanceOf[JavaDraft].type_to_class(right_type))), rp_node_type)
              }

              /*
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.AND)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean]))), PrimitiveType.BOOLEAN)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.BINARY_AND)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.BINARY_OR)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.DIVIDE)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              // having holes with no types might not be a good idea
              result += ((new BinaryExpr()
                .setOperator(BinaryExpr.Operator.EQUALS)
                .setLeft(JavaUtils.gen_expr_hole_without_test())
                .setRight(JavaUtils.gen_expr_hole_without_test()), PrimitiveType.BOOLEAN))
              result += ((new BinaryExpr()
                .setOperator(BinaryExpr.Operator.NOT_EQUALS)
                .setLeft(JavaUtils.gen_expr_hole_without_test())
                .setRight(JavaUtils.gen_expr_hole_without_test()), PrimitiveType.DOUBLE))
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.GREATER)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.GREATER_EQUALS)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.LEFT_SHIFT)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.LESS)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.LESS_EQUALS)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.MINUS)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.MULTIPLY)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.OR)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean]))), PrimitiveType.BOOLEAN)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.PLUS)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.REMAINDER)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.SIGNED_RIGHT_SHIFT)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.UNSIGNED_RIGHT_SHIFT)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new BinaryExpr()
                .setOperator(BinaryExpr.Operator.XOR)
                .setLeft(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))
                .setRight(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
                */
            case _ : BooleanLiteralExpr =>
              add_to_cache(new BooleanLiteralExpr(true), PrimitiveType.BOOLEAN)
              add_to_cache(new BooleanLiteralExpr(false), PrimitiveType.BOOLEAN)
            // case cle : CharLiteralExpr =>
            /*
            // having a conditional expression might not be a good thing
          case ce : ConditionalExpr =>
            result += ((new ConditionalExpr()
              .setCondition(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
              .setThenExpr(JavaUtils.gen_expr_hole_without_test())
              .setElseExpr(JavaUtils.gen_expr_hole_without_test()),
              Wildcard.UNBOUNDED))
              */
            case dle : DoubleLiteralExpr =>
              val signature_str = dle.getClass + " " + dle.asDouble().toString
              if(!seen.contains(signature_str)) {
                seen += signature_str
                add_to_cache(dle, PrimitiveType.DOUBLE)
                add_to_cache(new DoubleLiteralExpr("1.0"), PrimitiveType.DOUBLE)
                add_to_cache(new DoubleLiteralExpr("-1.0"), PrimitiveType.DOUBLE)
                add_to_cache(new DoubleLiteralExpr("0.0"), PrimitiveType.DOUBLE)
              }
              /*
              add_to_cache(new DoubleLiteralExpr("1.0"), PrimitiveType.DOUBLE)
              add_to_cache(new DoubleLiteralExpr("2.0"), PrimitiveType.DOUBLE)
              add_to_cache(new DoubleLiteralExpr("3.0"), PrimitiveType.DOUBLE)
              add_to_cache(new DoubleLiteralExpr("0.0"), PrimitiveType.DOUBLE)
              add_to_cache(new DoubleLiteralExpr("-1.0"), PrimitiveType.DOUBLE)
              add_to_cache(new DoubleLiteralExpr("-2.0"), PrimitiveType.DOUBLE)
              add_to_cache(new DoubleLiteralExpr("-3.0"), PrimitiveType.DOUBLE)
              */
            case _ : FieldAccessExpr =>
              relevant_progs.asInstanceOf[Seq[JavaDraft]].+:(this).foreach(d => {
                this.extract_fields(d).foreach(p => {
                  if(!this.valid_expr_fills_cache.contains(p._1)) {
                    this.valid_expr_fills_cache(p._1) = mutable.Set.empty[(Expression, Boolean)]
                  }
                  this.valid_expr_fills_cache(p._1) ++= p._2.map(n => (n, JavaUtils.has_hole(n)))
                })
              })
            // case ioe : InstanceOfExpr =>
            case ile : IntegerLiteralExpr =>
              val signature_str = ile.getClass + " " + ile.asInt().toString
              if(!seen.contains(signature_str)) {
                seen += signature_str
                add_to_cache(ile, PrimitiveType.DOUBLE)
                add_to_cache(new IntegerLiteralExpr("1"), PrimitiveType.INT)
                add_to_cache(new IntegerLiteralExpr("1"), PrimitiveType.INT)
                add_to_cache(new IntegerLiteralExpr("0"), PrimitiveType.INT)
              }
              /*
              add_to_cache(new IntegerLiteralExpr("1"), PrimitiveType.INT)
              add_to_cache(new IntegerLiteralExpr("2"), PrimitiveType.INT)
              add_to_cache(new IntegerLiteralExpr("3"), PrimitiveType.INT)
              add_to_cache(new IntegerLiteralExpr("0"), PrimitiveType.INT)
              add_to_cache(new IntegerLiteralExpr("-1"), PrimitiveType.INT)
              add_to_cache(new IntegerLiteralExpr("-2"), PrimitiveType.INT)
              add_to_cache(new IntegerLiteralExpr("-3"), PrimitiveType.INT)
            case _ : LongLiteralExpr =>
              add_to_cache(new LongLiteralExpr("1"), PrimitiveType.LONG)
              add_to_cache(new LongLiteralExpr("2"), PrimitiveType.LONG)
              add_to_cache(new LongLiteralExpr("3"), PrimitiveType.LONG)
              add_to_cache(new LongLiteralExpr("0"), PrimitiveType.LONG)
              add_to_cache(new LongLiteralExpr("-1"), PrimitiveType.LONG)
              add_to_cache(new LongLiteralExpr("-2"), PrimitiveType.LONG)
              add_to_cache(new LongLiteralExpr("-3"), PrimitiveType.LONG)
              */
            case _ : MethodCallExpr =>
              relevant_progs.asInstanceOf[Seq[JavaDraft]].+:(this).foreach(d => {
                this.extract_methods(d).foreach(p => {
                  if(!this.valid_expr_fills_cache.contains(p._1)) {
                    this.valid_expr_fills_cache(p._1) = mutable.Set.empty[(Expression, Boolean)]
                  }
                  this.valid_expr_fills_cache(p._1) ++= p._2.map(n => (n, JavaUtils.has_hole(n)))
                })
              })
            case _ : NameExpr =>
              for(vname <- this.defset) {
                val name_node = new NameExpr(vname)
                val t = this.get_type(new JavaDraftNode(name_node)).asInstanceOf[JavaDraftNodeType].t
                add_to_cache(name_node, t)
              }

              // we want to use class names if the hole is a scope of a method call
              parent match {
                case mce : MethodCallExpr if mce.getScope.isPresent && mce.getScope.get() == hole.asInstanceOf[JavaDraftHole].node =>
                  // loop through all the imported classes and put them into the results
                  val imports = this.compilation_unit.getImports
                  for(i <- 0 until imports.size()) {
                    val ip = imports.get(i)
                    val ct = JavaUtils.str_to_type(ip.getNameAsString, this.type_solver)
                    if(!this.valid_expr_fills_cache.contains(ct)) {
                      this.valid_expr_fills_cache(ct) = mutable.Set.empty[(Expression, Boolean)]
                    }
                    this.valid_expr_fills_cache(ct) += ((JavaUtils.qname_to_fieldaccess(ip.getName), false))
                  }
                case _ =>
              }
            // case ne : NameExpr =>
            case _ : NullLiteralExpr =>
              add_to_cache(new NullLiteralExpr(), NullType.INSTANCE)
            // case oce : ObjectCreationExpr =>
            case sle : StringLiteralExpr =>
              val t = new ReferenceTypeImpl(new ReflectionClassDeclaration(classOf[String], this.type_solver.getTypeSolver), this.type_solver.getTypeSolver)
              val signature_str = sle.getClass + " " + sle.asString()
              if(!seen.contains(signature_str)) {
                seen += signature_str
                add_to_cache(sle, t)
              }
              /*
              add_to_cache(new StringLiteralExpr(""), t)
              add_to_cache(new StringLiteralExpr(" "), t)
              add_to_cache(new StringLiteralExpr("\t"), t)
              add_to_cache(new StringLiteralExpr("\n"), t)
              */
            // case te : ThisExpr => result += ((new ThisExpr(), Wildcard.UNBOUNDED))
            case ue : UnaryExpr =>
              val op = ue.getOperator
              val v_type = rp.get_type(new JavaDraftNode(ue.getExpression)).asInstanceOf[JavaDraftNodeType].t
              val signature_str = ue.getClass + " " + v_type.toString
              if(!seen.contains(signature_str)) {
                seen += signature_str
                add_to_cache(new UnaryExpr()
                  .setOperator(ue.getOperator)
                  .setExpression(JavaUtils.gen_expr_hole_without_test(rp.asInstanceOf[JavaDraft].type_to_class(v_type))), rp_node_type)
              }
              /*
              add_to_cache(new UnaryExpr()
                .setOperator(UnaryExpr.Operator.BITWISE_COMPLEMENT)
                .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new UnaryExpr()
                .setOperator(UnaryExpr.Operator.LOGICAL_COMPLEMENT)
                .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean]))), PrimitiveType.BOOLEAN)
              add_to_cache(new UnaryExpr()
                .setOperator(UnaryExpr.Operator.MINUS)
                .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new UnaryExpr()
                .setOperator(UnaryExpr.Operator.PLUS)
                .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double]))), PrimitiveType.DOUBLE)
              add_to_cache(new UnaryExpr()
                .setOperator(UnaryExpr.Operator.POSTFIX_DECREMENT)
                .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new UnaryExpr()
                .setOperator(UnaryExpr.Operator.POSTFIX_INCREMENT)
                .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new UnaryExpr()
                .setOperator(UnaryExpr.Operator.PREFIX_DECREMENT)
                .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
              add_to_cache(new UnaryExpr()
                .setOperator(UnaryExpr.Operator.PREFIX_INCREMENT)
                .setExpression(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int]))), PrimitiveType.INT)
                */
            // case vde : VariableDeclarationExpr =>
            case _ =>
          }
        }
      })
    }

    // extract expressions from relevant programs
    relevant_progs.foreach(rp => this.extract_expr(hole, rp.asInstanceOf[JavaDraft]).foreach(p => add_to_cache(p._1, p._2)))

    if(hole_type.isWildcard) {
      val result = this.valid_expr_fills_cache.values.foldLeft(Seq.empty[Node])((acc, s) => acc ++ s.map(_._1).toSeq)
        .filter(universal_filter)
        .map(n => new JavaDraftNode(n))
      result.foreach(n => this.logger.debug("Valid Expr Fills {}", n.node))
      result
    } else {
      val result = this.valid_expr_fills_cache.foldLeft(Seq.empty[Node])((acc, p) => {
        if(p._1.isWildcard || this.is_subtype(p._1, hole_type)) {
          acc ++ p._2.map(_._1).toSeq
        } else {
          acc
        }
      }).filter(universal_filter)
        .map(n => new JavaDraftNode(n))
      result.foreach(n => this.logger.debug("Valid Expr Fills {}", n.node))
      result
    }
  }

  /**
    * Return a sequence of valid fillings for a given hole from scratch.
    */
  override def get_valid_stmt_fills(hole: DraftHole, relevant_progs : Seq[Draft]): Seq[Seq[DraftNode]] = {
    val result = mutable.ArrayBuffer.empty[Seq[Node]]
    this.logger.debug("front parent {}", hole.get_parent.get.asInstanceOf[JavaDraftNode].node.getClass)
    assume(!this.is_expr_hole(hole))

    val expr_stmt = hole.asInstanceOf[JavaDraftNode].node.asInstanceOf[ExpressionStmt]
    val hole_id = expr_stmt.getExpression.asInstanceOf[MethodCallExpr].getArguments.get(0).asInstanceOf[IntegerLiteralExpr].asInt()

    // add an empty sequence to stop the statement sequence
    result += Seq()

    val seen = mutable.Set.empty[String]

    relevant_progs.foreach(rp => {
      val iter = JavaDraftIteratorFactory.create_draft_iterator(rp, Some(p => rp.is_stmt(p)))
      // we are generating statement holes
      while(iter.hasNext) {
        val rp_node = iter.next().asInstanceOf[JavaDraftNode]
        val stmt = rp_node.node.asInstanceOf[Statement]
        stmt match {
          // case bs : BlockStmt =>
          case bs : BreakStmt if !seen.contains(bs.getClass.toString) =>
            seen += bs.getClass.toString
            result += Seq(new BreakStmt().setLabel(null))
          case cs : ContinueStmt if !seen.contains(cs.getClass.toString) =>
            seen += cs.getClass.toString
            result += Seq(new ContinueStmt())
          case ds : DoStmt if !seen.contains(ds.getClass.toString) =>
            seen += ds.getClass.toString
            val body = new NodeList[Statement]
            body.add(JavaUtils.gen_stmt_hole_without_test())
            val stmt = new DoStmt()
              .setCondition(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
              .setBody(new BlockStmt(body))
            result += Seq(stmt)
          case es : ExpressionStmt =>
            es.getExpression match {
              case mce : MethodCallExpr if !seen.contains(mce.getClass.toString) =>
                seen += mce.getClass.toString
                relevant_progs.asInstanceOf[Seq[JavaDraft]].+:(this).foreach(d => {
                  this.extract_methods(d).foreach(p => {
                    p._2.foreach(n => {
                      result += Seq(new ExpressionStmt(n))
                    })
                  })
                })
              case vexpr: VariableDeclarationExpr if !seen.contains(vexpr.getClass.toString) =>
                seen += vexpr.getClass.toString
                val vdor = vexpr.getVariables.get(0)
                val name = vdor.getName

                // get the type from the variable declarator
                val t =
                  try {
                    this.type_solver.convertToUsageVariableType(vdor)
                  } catch {
                    case _: Throwable =>
                      logger.warn("Failed to get the type for name {}", name.getIdentifier)
                      val cd = new ClassOrInterfaceDeclaration(util.EnumSet.of(Modifier.PUBLIC), false, vdor.getType.toString)
                      new ReferenceTypeImpl(new JavaParserClassDeclaration(cd, this.type_solver.getTypeSolver), this.type_solver.getTypeSolver)
                  }
                val new_vdor = new VariableDeclarator(vdor.getType, new SimpleName(JavaUtils.mk_var(hole_id)), JavaUtils.gen_expr_hole_without_test(this.type_to_class(t)))
                val new_decl = new VariableDeclarationExpr(new_vdor)
                result += Seq(new ExpressionStmt(new_decl))
              case ae: AssignExpr =>
                val op = ae.getOperator
                val left_type = rp.get_type(new JavaDraftNode(ae.getTarget)).asInstanceOf[JavaDraftNodeType].t
                val right_type = rp.get_type(new JavaDraftNode(ae.getValue)).asInstanceOf[JavaDraftNodeType].t
                val signature_str = ae.getClass + " " + left_type.toString + " " + op.toString + " " + right_type.toString
                if(!seen.contains(signature_str)) {
                  seen += signature_str
                  result += Seq(new ExpressionStmt(new AssignExpr()
                    .setOperator(ae.getOperator)
                    .setTarget(JavaUtils.gen_expr_hole_without_test(rp.asInstanceOf[JavaDraft].type_to_class(left_type)))
                    .setValue(JavaUtils.gen_expr_hole_without_test(rp.asInstanceOf[JavaDraft].type_to_class(right_type)))))
                }


                // gather all the variables
                /*
                val vars = mutable.Map.empty[Type, mutable.Set[NameExpr]]
                for(vname <- this.defset) {
                  val name_node = new NameExpr(vname)
                  val t = this.get_type(new JavaDraftNode(name_node)).asInstanceOf[JavaDraftNodeType].t
                  if(!vars.contains(t)) {
                    vars.put(t, mutable.Set.empty[NameExpr])
                  }
                  vars(t) += name_node
                }

                // boolean
                vars.foreach(p => {
                  val t = p._1
                  val names = p._2

                  // boolean
                  if(this.is_subtype(t, PrimitiveType.BOOLEAN)) {
                    names.foreach(name => {
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.AND)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))), JavaUtils.gen_stmt_hole_without_test())
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.OR)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))), JavaUtils.gen_stmt_hole_without_test())
                    })
                  }

                  // integer
                  if(this.is_subtype(t, PrimitiveType.INT)) {
                    names.foreach(name => {
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.LEFT_SHIFT)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))), JavaUtils.gen_stmt_hole_without_test())
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.REMAINDER)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))), JavaUtils.gen_stmt_hole_without_test())
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.SIGNED_RIGHT_SHIFT)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))), JavaUtils.gen_stmt_hole_without_test())
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.UNSIGNED_RIGHT_SHIFT)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))), JavaUtils.gen_stmt_hole_without_test())
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.XOR)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Int])))), JavaUtils.gen_stmt_hole_without_test())
                    })
                  }

                  // double
                  if(this.is_subtype(t, PrimitiveType.DOUBLE)) {
                    names.foreach(name => {
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.DIVIDE)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))), JavaUtils.gen_stmt_hole_without_test())
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.MINUS)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))), JavaUtils.gen_stmt_hole_without_test())
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.MULTIPLY)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))), JavaUtils.gen_stmt_hole_without_test())
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.PLUS)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[Double])))), JavaUtils.gen_stmt_hole_without_test())
                    })
                  }

                  // string
                  val string_type = new ReferenceTypeImpl(new ReflectionClassDeclaration(classOf[String], this.type_solver.getTypeSolver), this.type_solver.getTypeSolver)
                  if(this.is_subtype(t, string_type)) {
                    names.foreach(name => {
                      result += Seq(new ExpressionStmt(new AssignExpr()
                        .setOperator(AssignExpr.Operator.PLUS)
                        .setTarget(name)
                        .setValue(JavaUtils.gen_expr_hole_without_test(Some(classOf[String])))), JavaUtils.gen_stmt_hole_without_test())
                    })
                  }

                  // any type
                  names.foreach(name => {
                    result += Seq(new ExpressionStmt(new AssignExpr()
                      .setOperator(AssignExpr.Operator.ASSIGN)
                      .setTarget(name)
                      .setValue(JavaUtils.gen_expr_hole_without_test())), JavaUtils.gen_stmt_hole_without_test())
                    result += Seq(new ExpressionStmt(new AssignExpr()
                      .setOperator(AssignExpr.Operator.ASSIGN)
                      .setTarget(name)
                      .setValue(JavaUtils.gen_expr_hole_without_test())))
                  })
                })
                  */
              case _ =>
            }
          case fs : ForStmt if !seen.contains(fs.getClass.toString) =>
            seen += fs.getClass.toString
            val init_list = new NodeList[Expression]
            for(_ <- 0 until fs.getInitialization.size()) {
              init_list.add(JavaUtils.gen_expr_hole_without_test())
            }
            val update_list = new NodeList[Expression]
            for(_ <- 0 until fs.getUpdate.size()) {
              update_list.add(JavaUtils.gen_expr_hole_without_test())
            }
            val body = new NodeList[Statement]
            body.add(JavaUtils.gen_stmt_hole_without_test())
            val stmt = new ForStmt()
                .setInitialization(init_list)
                .setUpdate(update_list)
                .setBody(new BlockStmt(body))
                .setCompare(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
            result += Seq(stmt)
          case fes : ForeachStmt if !seen.contains(fes.getClass.toString) =>
            seen += fes.getClass.toString
            val vexpr = fes.getVariable

            // construct a new declarator
            val vdor = vexpr.getVariables.get(0)
            val new_vdor = new VariableDeclarator(vdor.getType, new SimpleName(JavaUtils.mk_var(hole_id)))
            val new_decl = new VariableDeclarationExpr(new_vdor)

            val body = new NodeList[Statement]
            body.add(JavaUtils.gen_stmt_hole_without_test())
            val stmt = new ForeachStmt()
                .setVariable(new_decl)

                // TODO: We don't actually have a way to put constraints on types here
                .setIterable(JavaUtils.gen_expr_hole_without_test())
                .setBody(new BlockStmt(body))
            result += Seq(stmt)
          case is : IfStmt if !seen.contains(is.getClass.toString) =>
            seen += is.getClass.toString
            val stmt1 = {
              val then_body = new NodeList[Statement]
              then_body.add(JavaUtils.gen_stmt_hole_without_test())
              val else_body = new NodeList[Statement]
              else_body.add(JavaUtils.gen_stmt_hole_without_test())
              new IfStmt()
                .setCondition(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
                .setThenStmt(new BlockStmt(then_body))
                .setElseStmt(new BlockStmt(else_body))
            }
            val stmt2 = {
              val then_body = new NodeList[Statement]
              then_body.add(JavaUtils.gen_stmt_hole_without_test())
              new IfStmt()
                .setCondition(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
                .setThenStmt(new BlockStmt(then_body))
            }

            result += Seq(stmt1)
            result += Seq(stmt2)
          case rs : ReturnStmt if !seen.contains(rs.getClass.toString) =>
            seen += rs.getClass.toString
            result += Seq(new ReturnStmt(JavaUtils.gen_expr_hole_without_test(this.type_to_class(this.type_solver.convertToUsage(this.method.getType)))))
          /*
        case ses : SwitchEntryStmt =>
          val stmt_hole = new NodeList[Statement]()
          stmt_hole.add(JavaUtils.gen_stmt_hole_without_test())
          result += new SwitchEntryStmt()
            .setLabel(JavaUtils.gen_expr_hole_without_test())
            .setStatements(stmt_hole)
        case ss : SwitchStmt =>
        case ts : ThrowStmt =>
        case trs : TryStmt =>
        */
          case ws : WhileStmt if !seen.contains(ws.getClass.toString) =>
            seen += ws.getClass.toString
            val body = new NodeList[Statement]
            body.add(JavaUtils.gen_stmt_hole_without_test())
            val stmt = new WhileStmt()
                .setCondition(JavaUtils.gen_expr_hole_without_test(Some(classOf[Boolean])))
                .setBody(new BlockStmt(body))
            result += Seq(stmt)
          case _ =>
        }
      }

    })
    // result += Seq(new EmptyStmt())
    result += Seq(JavaUtils.gen_stmt_hole_without_test(), JavaUtils.gen_stmt_hole_without_test())
    result.foreach(s => this.logger.debug("Candidate stmts =========\n {}", if(s.isEmpty) "Empty" else s.foldLeft("")((acc, node) => acc + "\n" + node.toString())))
    result.map(s => s.map(n => new JavaDraftNode(n)))
  }
}
