package edu.rice.pliny.language.java

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.`type`.ClassOrInterfaceType
import com.github.javaparser.symbolsolver.model.typesystem.Type
import edu.rice.pliny.draft.Binding

/**
  * Variable Binding for java
  */
class JavaBinding(val v : String, val t : Type, val init : Option[Node], val scope : Option[ClassOrInterfaceType]) extends Binding {
  override def equals(o : Any) : Boolean = {
    if(!o.isInstanceOf[JavaBinding]) { return false }
    val b = o.asInstanceOf[JavaBinding]
    b.v.equals(v) && b.t.equals(t) && b.init.equals(init) && b.scope.equals(scope)
  }

  override def toString: String = scope + "." + v + " : " + t.toString + " = " + init
}
