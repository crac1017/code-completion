package edu.rice.pliny.language.java

import com.github.javaparser.ast.{Node, NodeList}
import com.github.javaparser.ast.body.VariableDeclarator
import com.github.javaparser.ast.expr.{NameExpr, VariableDeclarationExpr}
import com.github.javaparser.ast.stmt.{BlockStmt, ExpressionStmt, Statement, SwitchEntryStmt}
import com.github.javaparser.ast.visitor.{TreeVisitor, VoidVisitorAdapter}
import edu.rice.pliny.draft.DraftStmtWindowIterator

import scala.collection.mutable
import scalaj.collection.Imports._
import com.typesafe.scalalogging.Logger

/**
  * This is used as a sliding window on the statements in the draft used for filling in statement holes
  */
class JavaStmtWindowIterator(draft: JavaDraft, condition: Option[Node => Boolean]) extends DraftStmtWindowIterator(draft) {
  val logger = Logger(this.getClass)

  /**
    * Used to keep track of index
    */
  var curr_index : Int = 0

  // a list of windows. A window is a tuple where the first element is the statement sequence and the second
  // one is the parent node
  var window_list : mutable.ArrayBuffer[Window] = mutable.ArrayBuffer.empty[Window]
  this.initialize()

  /**
    * We want to sort the windows according to define-usage ratio. We want to put windows with low renaming cost
    * at the front.
    */
  private def sort_define_usage(wa : Window, wb : Window) : Boolean = {
    val wa_nodes = wa._1.map(dn => dn.asInstanceOf[JavaDraftNode].node.asInstanceOf[Statement])
    val wb_nodes = wb._1.map(dn => dn.asInstanceOf[JavaDraftNode].node.asInstanceOf[Statement])

    /**
      * Get all the defined names
      */
    def get_define_names(nodes : Seq[Statement]) : mutable.Set[String] = {
      val result = mutable.Set.empty[String]
      nodes.foreach {
        case es: ExpressionStmt => es.getExpression match {
          case vder: VariableDeclarationExpr =>
            vder.getVariables.foreach(vtor => {
              result += vtor.getNameAsString
            })
          case _ => Unit
        }
        case _ => Unit
      }
      result
    }

    /**
      * Get all the used names
      */
    def get_used_names(nodes : Seq[Statement]) : mutable.Set[String] = {
      val result = mutable.Set.empty[String]

      val used_name_visitor = new VoidVisitorAdapter[Any] {
        override def visit(name : NameExpr, arg : Any) : Unit = result += name.getNameAsString
        override def visit(vdor : VariableDeclarator, arg : Any) : Unit = if(vdor.getInitializer.isPresent) vdor.getInitializer.get().accept(this, arg)
      }

      nodes.foreach(n => n.accept(used_name_visitor, null))
      result
    }

    /*
    println("============")
    wa_nodes.foreach(n => println(n.toString))
    println("============")
    wb_nodes.foreach(n => println(n.toString))
    */
    val defined_a = get_define_names(wa_nodes)
    // println("Defined A " + defined_a.size)
    val defined_b = get_define_names(wb_nodes)
    // println("Defined B " + defined_b.size)
    if(defined_a.size == defined_b.size) {
      val used_a = get_used_names(wa_nodes)
      // println("Used A")
      val used_b = get_used_names(wb_nodes)
      // println("Used B")
      used_a.size < used_b.size
    } else {
      defined_a.size > defined_b.size
    }
  }

  /**
    * Use a sliding window of different sizes to store the statements
    */
  private def store_windows(stmts : NodeList[Statement], parent : Statement): Unit = {
    val stmts_node_list = mutable.ArrayBuffer.empty[JavaDraftNode]
    for(i <- 0 until stmts.size()) {
      stmts_node_list += new JavaDraftNode(stmts.get(i))
    }
    val parent_node = new JavaDraftNode(parent)

    for(i <- stmts_node_list.indices) {
      for(window_size <- 1 to (stmts_node_list.size - i)) {
        if(i + window_size <= stmts_node_list.size) {
          val window = stmts_node_list.slice(i, i + window_size)
          if(window.nonEmpty) {
            this.window_list += ((window, parent_node))
          }
        }
      }
    }
  }

  private def initialize() : Unit = {
    new TreeVisitor {
      override def process(n : Node) : Unit = {
        n match {
          case bs : BlockStmt => store_windows(bs.getStatements, bs)
          case ses : SwitchEntryStmt => store_windows(ses.getStatements, ses)
          case _ =>
        }
      }
    }.visitPostOrder(draft.method)
    this.window_list = this.window_list.sortBy(seq => seq._1.foldLeft(0)((accu, node) => accu + node.get_size))
    // this.window_list = this.window_list.sortWith(sort_define_usage)
  }

  /**
    * Get the current node
    */
  override def get_curr_node: Window = this.window_list(this.curr_index)

  /**
    * Get all the nodes
    */
  override def get_all_nodes: mutable.ArrayBuffer[Window] = this.window_list

  override def hasNext: Boolean = this.curr_index < this.window_list.size

  override def next(): Window = {
    val result = this.get_curr_node
    this.curr_index += 1
    result
  }

  /**
    * This function is used to visualize a window
    */
  def print_window(w : Window) : String = {
    val result = new StringBuilder()
    result.append("Window ============\n")
    w._1.foreach(s => result.append(s + "\n"))
    result.append("Parent ============\n")
    result.append(w._2.toString)
    result.toString()
  }
}
