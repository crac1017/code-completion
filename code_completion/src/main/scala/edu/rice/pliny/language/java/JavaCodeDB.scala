package edu.rice.pliny.language.java

import java.io.{ByteArrayInputStream, File}
import java.util

import com.github.javaparser.JavaParser
import com.github.javaparser.ast.body.{ConstructorDeclaration, MethodDeclaration}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft.Draft
import edu.rice.pliny.feature.source._
import edu.rice.pliny.{AppConfig, CodeDB}
import spray.json._

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.sys.process._

/**
  * Interface to the code database
  */
object JavaCodeDB extends CodeDB {
  val logger = Logger(this.getClass)

  override def query(prog: Draft, comment : String): Seq[(Draft, Double, ProgramElement)] = AppConfig.CodeCompletionConfig.query_type match {
    case "dir" => this.query_local_dir(prog, AppConfig.CodeCompletionConfig.local_dir_path)
    case "json" => this.query_local_json(prog, AppConfig.CodeCompletionConfig.local_json_path)
    case "pdb" => this.query_pdb(prog, comment)
  }

  /**
    * Generate the features we need from a program. Comment is used to generate NL terms feature and it must be
    * meaningful.
    */
  def gen_features(prog : JavaDraft, comment : String) : util.HashSet[SourceFeature] = {
    val prog_src = prog.toStringWithClass
    val method_name = prog.method.getNameAsString
    val names_feature = NamesFeature.gen_names_feature(prog_src, method_name)
    val nl_terms_feature =
      WeightedNLTermsFeature.gen_nlterms_from_string(AppConfig.PDBConfig.nl_terms_path, comment, method_name, "", "").values()
    val feature_set = new util.HashSet[SourceFeature]
    feature_set.add(names_feature)
    feature_set.addAll(nl_terms_feature)
    feature_set
  }

  /**
    * Query the actual PDB
    */
  def query_pdb(prog : Draft, comment : String) : Seq[(Draft, Double, ProgramElement)] = {
    logger.info("Querying PDB using this draft ===================\n" + prog.toString)
    val java_prog = prog.asInstanceOf[JavaDraft]
    val method_name = java_prog.method.getNameAsString
    logger.info("Method name: " + method_name)

    // generate all features and only select the one with correct program element
    val feature_set = gen_features(java_prog, comment)
    val pe = ProgramElement.fromMaven12("", "", method_name, prog.toString, feature_set)
    val pe_set = new util.HashSet[ProgramElement]
    pe_set.add(pe)
    logger.info("Program Element from source ======\n{}", pe.toJsonStr)

    // input for pliny-query
    val input_json_str = SourceFeatureMain.gen_json(pe_set).asScala.head
    logger.debug("Input JSON String ========\n" + input_json_str)
    val input_stream = new ByteArrayInputStream(input_json_str.getBytes)

    val query_config = AppConfig.PDBConfig.QueryConfig

    val cmd = Seq(AppConfig.PDBConfig.path + "/" + AppConfig.PDBConfig.query_script_name,
      AppConfig.PDBConfig.hostname,
      (AppConfig.CodeCompletionConfig.db_prog_num * AppConfig.CodeCompletionConfig.db_prog_num).toString,
      query_config.names.toString,
      query_config.weighted_nl_terms_intersect.toString)

    logger.debug("Input JSON String ========\n" + input_json_str)
    logger.debug("Command: {}", cmd)

    val plogger = ProcessLogger(
      (o : String) => logger.debug("Output from PDB =============\n{}", o),
      (e : String) => logger.debug("Stderr from PDB =============\n{}", e))
    (cmd #< input_stream).!(plogger)

    val result_path = AppConfig.PDBConfig.path + "/" + AppConfig.PDBConfig.query_result_name
    val json_results = scala.io.Source.fromFile(result_path).mkString
    logger.info("Result from PDB ==========\n{}", json_results)

    // get the external library paths from the draft
    val result = get_draft_from_json(json_results, java_prog.jar_paths, java_prog.dir_paths)

    logger.info("We have " + result.size + " result drafts.")
    result.foreach(r => logger.info("Resulting Program =========\n" + r.toString))

    result
  }

  /**
    * Get all drafts from json arrays
    */
  private def get_draft_from_json(json_str : String, jar_paths : Seq[String], dir_paths : Seq[String]) : Seq[(Draft, Double, ProgramElement)] = {
    val result = mutable.ArrayBuffer.empty[(Draft, Double, ProgramElement)]
    val elements = json_str.parseJson.asInstanceOf[JsArray].elements
    logger.debug("Got " + elements.size + " program elements.")
    for(e <- elements) {
      val score = e.asJsObject.fields("Score").asInstanceOf[JsNumber].value.toDouble
      logger.debug("Score: " + score)
      val pe = e.asJsObject.fields("Program Element")
      logger.debug("Program element =========\n{}", pe.toString())
      val draft = get_draft_from_json_object(pe.asJsObject, dir_paths, jar_paths)
      if(draft.isDefined) {
        result += ((draft.get, score, ProgramElement.fromJson(pe.toString())))
        logger.debug("Program draft =========\n{}", draft.get)
      } else {
        logger.warn("Cannot get draft from the program element.")
      }
    }
    result
  }

  private def get_signature(method_str : String) : Seq[String] = {
    JavaParser.parseBodyDeclaration(method_str) match {
      case method : MethodDeclaration => JavaUtils.get_type_string(method)
      case cons : ConstructorDeclaration => JavaUtils.get_type_string(cons) :+ cons.getNameAsString
    }
  }


  /**
    * Get the draft from a program element JSON string
    */
  private def get_draft_from_json_object(json_object : JsObject, jar_paths : Seq[String], dir_paths : Seq[String]) : Option[Draft] = {
    json_object.getFields("Features", "Element Name", "Textual Representation") match {
      case Seq(JsObject(features), JsString(element_name), JsString(method_text)) if features.contains("CUSource") =>
        val src_json = features("CUSource").asJsObject
          .fields("Feature").asJsObject
          .fields("Content").asInstanceOf[JsString].value

        // get the method name
        val (class_name, method_name) = this.get_method_name(element_name)

        // get the signature
        val signature = this.get_signature(method_text)
        JavaUtils.parse_src(src_json, Some(class_name), Some(method_name), Some(signature), jar_paths, dir_paths)
      case Seq(JsObject(features), JsString(element_name), JsString(method_text)) if features.contains("ClassSource") =>
        val src_json = features("ClassSource").asJsObject
          .fields("Feature").asJsObject
          .fields("Content").asInstanceOf[JsString].value

        // get the method name
        val (class_name, method_name) = this.get_method_name(element_name)

        // get the signature
        val signature = this.get_signature(method_text)
        JavaUtils.parse_src(src_json, Some(class_name), Some(method_name), Some(signature), jar_paths, dir_paths)
      case _ => None
    }
  }

  /**
    * Get the method name from the program element name. The program element name is in the form
    * of package1.package2.methodname(args)
    */
  private def get_method_name(element_name : String) : (String, String) = {
    // get the string before parenthesis
    val paren_index = element_name.indexOf('(')
    val str_before_paren = element_name.substring(0, paren_index)
    // split by dot and get the last name
    val names = str_before_paren.split('.')
    val method_name = names.last
    val class_name = names(names.length - 2)
    (class_name, method_name)
  }

  /**
    * This function returns a list of drafts stored in the JSONL file
    */
  def query_local_json(prog : Draft, json_filename : String) : Seq[(Draft, Double, ProgramElement)] = {
    val java_prog = prog.asInstanceOf[JavaDraft]
    val json_str = scala.io.Source.fromFile(json_filename).mkString
    this.get_draft_from_json(json_str, java_prog.jar_paths, java_prog.dir_paths)
  }

  /**
    * This function returns a list of drafts stored in the local directory
    */
  def query_local_dir(prog : Draft, dirname : String) : Seq[(Draft, Double, ProgramElement)] = {
    val java_prog = prog.asInstanceOf[JavaDraft]
    val result = mutable.ArrayBuffer.empty[(Draft, Double, ProgramElement)]

    val corpus_dir = new File(dirname)
    val file_array = corpus_dir.listFiles().filter(_.isFile)
    for(file <- file_array) {
      val src = scala.io.Source.fromFile(file).mkString
      val jprog = JavaUtils.parse_src(src, None, None, None, java_prog.jar_paths, java_prog.dir_paths)
      if(jprog.isDefined) {
        result += ((jprog.get, -1.0, null))
      } else {
        logger.warn("Failed to parse java program : =============\n{}", src)
      }
    }

    result
  }

}
