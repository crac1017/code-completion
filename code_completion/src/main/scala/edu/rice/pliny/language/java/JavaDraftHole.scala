package edu.rice.pliny.language.java

import com.github.javaparser.ast.Node
import com.github.javaparser.symbolsolver.model.typesystem.{Type, Wildcard}
import edu.rice.pliny.draft.{DraftHole, DraftNode, TestCase}

class JavaDraftHole(node : Node, val comment : Option[String], val test : Option[JavaTestCase], val ttype : Type = Wildcard.UNBOUNDED) extends JavaDraftNode(node) with DraftHole {
  override def toString : String = this.node.toString
  // get the parent node
  override def get_parent: Option[DraftNode] = {
    val p = node.getParentNode
    if(p.isPresent) {
      Some(new JavaDraftNode(node.getParentNode.get()))
    } else {
      None
    }
  }

  override def get_comments: Option[String] = this.comment

  override def get_test: Option[TestCase] = this.test

  def get_type : Type = this.ttype
}
