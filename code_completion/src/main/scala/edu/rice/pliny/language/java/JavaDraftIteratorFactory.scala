package edu.rice.pliny.language.java

import edu.rice.pliny.draft._

object JavaDraftIteratorFactory extends IteratorFactory {

  /**
    * This is used for filling in statement holes
    */
  override def create_stmt_sliding_window(draft: Draft, pred: Option[DraftNode => Boolean]) : DraftStmtWindowIterator = draft match {
    case j : JavaDraft =>
      new JavaStmtWindowIterator(j, if(pred.isEmpty) None else Some(n => pred.get(new JavaDraftNode(n))))
  }

  /**
    * Given a draft in any language, create its draft node iterator
    */
  override def create_draft_iterator(draft : Draft, pred : Option[DraftNode => Boolean] = None) : DraftNodeIterator = draft match {
    case j : JavaDraft =>
      new JavaDraftNodeIterator(new JavaDraftNode(j.method), if(pred.isEmpty) None else Some(n => pred.get(new JavaDraftNode(n))))
  }

  override def create_draftnode_iterator(node : DraftNode, pred : Option[DraftNode => Boolean] = None) : DraftNodeIterator = node match {
    case jnode : JavaDraftNode =>
      new JavaDraftNodeIterator(jnode, if(pred.isEmpty) None else Some(n => pred.get(new JavaDraftNode(n))))
  }

  /**
    * Given a draft in any language, create its reference iterator
    */
  override def create_draftnode_ref_iterator(node : DraftNode, pred : Option[DraftNode => Boolean] = None) : DraftRefIterator = node match {
    case jnode : JavaDraftNode =>
      new JavaRefIterator(jnode, if(pred.isEmpty) None else Some(n => pred.get(new JavaDraftNode(n))))
  }
  override def create_draft_ref_iterator(draft : Draft, pred : Option[DraftNode => Boolean] = None) : DraftRefIterator = draft match {
    case j : JavaDraft =>
      new JavaRefIterator(new JavaDraftNode(j.method), if(pred.isEmpty) None else Some(n => pred.get(new JavaDraftNode(n))))
  }
}
