package edu.rice.pliny.language.java

import com.github.javaparser.ast.Node
import edu.rice.pliny.draft.DraftNode

import scala.collection.mutable

/**
  * This represents a program in any language
  */
class JavaDraftNode(val node : Node) extends DraftNode {
  override def toString : String = this.node.toString

  // get the parent node
  override def get_parent: Option[DraftNode] = {
    val p = node.getParentNode
    if(p.isPresent) {
      Some(new JavaDraftNode(node.getParentNode.get()))
    } else {
      None
    }
  }

  override def get_copy: DraftNode = new JavaDraftNode(node.clone())

  override def get_size: Int = JavaUtils.node_size(node)

  override def get_children : Seq[DraftNode] = {
    val result = mutable.ArrayBuffer.empty[DraftNode]
    val c = node.getChildNodes
    for(i <- 0 until c.size()) {
      result += new JavaDraftNode(c.get(i))
    }
    result
  }
}
