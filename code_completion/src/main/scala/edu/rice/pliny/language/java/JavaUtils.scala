package edu.rice.pliny.language.java

import java.lang.reflect.{Constructor, Method}
import java.util.Optional

import com.github.javaparser.ast.comments.{BlockComment, JavadocComment, LineComment}
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.stmt._
import com.github.javaparser.ast.body._
import com.github.javaparser.ast.visitor.{ModifierVisitor, TreeVisitor, Visitable, VoidVisitorAdapter}
import com.github.javaparser._
import com.github.javaparser.ast._
import com.github.javaparser.ast.`type`.ClassOrInterfaceType
import com.github.javaparser.ast.modules._
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade
import com.typesafe.scalalogging.Logger
import com.github.javaparser
import com.github.javaparser.symbolsolver.javaparsermodel.declarations.JavaParserClassDeclaration
import com.github.javaparser.symbolsolver.model.typesystem._
import com.github.javaparser.symbolsolver.model.typesystem

import scala.collection.mutable
import scalaj.collection.Imports._


object JavaUtils {

  // this controls the token inside the hole parameter to make sure we are not
  // generating duplicate holes
  private var hole_token = 0


  // this is the number of new variable we have created so var
  var varnum_map : mutable.Map[Int, Int] = mutable.Map.empty[Int, Int]

  def varnum_init() : Unit = varnum_map.clear()
  // a function for making new variables
  def mk_var(hole_id : Int) : String = {
    if(!varnum_map.contains(hole_id)) {
      varnum_map(hole_id) = varnum_map.size
    }
    "v_" + varnum_map(hole_id)
  }

  var var_count : Int = 0
  def mk_var() : String = {
    this.synchronized({
      val result = "v_" + var_count
      var_count += 1
      result
    })
  }


  // this is a set of candidate expressions used to fill in expression holes
  // using random search without any data
  val EXPRS = Array(
    new ArrayAccessExpr(),
    new ArrayCreationExpr(),
    new ArrayInitializerExpr(),
    new AssignExpr(),
    new BinaryExpr(),
    new BooleanLiteralExpr(),
    new CharLiteralExpr(),
    new ConditionalExpr(),
    new DoubleLiteralExpr(),
    new FieldAccessExpr(),
    new InstanceOfExpr(),
    new IntegerLiteralExpr(),
    // new LongLiteralExpr(),
    new MethodCallExpr(),
    new Name(),
    new NameExpr(),
    new NullLiteralExpr(),
    new StringLiteralExpr(),
    new ThisExpr(),
    new UnaryExpr(),
    new VariableDeclarationExpr()
  )

  val STMTS = Array(
    new BlockStmt(),
    new BreakStmt(),
    new ContinueStmt(),
    new DoStmt(),
    new ExpressionStmt(),
    new EmptyStmt(),
    new ForeachStmt(),
    new IfStmt(),
    new ReturnStmt(),
    new SwitchEntryStmt(),
    new SwitchStmt(),
    new ThrowStmt(),
    new TryStmt(),
    new WhileStmt()
  )

  val BUILT_IN_CLASS_NAMES = Set(
    "System",
    "AttributeEvent",
    "AttributeType",
    "Math",
    "R",
    "JJFrame",
    "JJLabel",
    "JJButton",
    "Imgcodecs",
    "Imgproc",
    "Integer"
  )

  val logger = Logger(this.getClass)

  /**
    * Name of the hole function
    */
  val HOLE_NAME = "__pliny_hole__"
  val STMT_PLACEHOLDER = "__pliny_solution__"

  // this is the java parser
  val parser_config = new ParserConfiguration
  val parser = new JavaParser(parser_config)

  /**
    * replace all question marks in the code with pliny_hole call
    */
  def replace_question_marks(src : String) : String = {
    val lines = src.split("\n")
    val builder = new StringBuilder
    for(l <- lines) {
      if(l.trim.equals("??")) {
        builder.append(l.replace("??", s"__pliny_hole__(${JavaUtils.next_hole_token()});") + "\n")
      } else if(l.nonEmpty) {
        var pos = 0
        while(pos + 1 < l.length) {
          if(l.charAt(pos) == '?' && l.charAt(pos + 1) == '?') {
            builder.append(s"__pliny_hole__(${JavaUtils.next_hole_token()})")
            pos += 2
          } else {
            builder.append(l.charAt(pos))
            pos += 1
          }
        }
        builder.append(l.charAt(l.length - 1))
        builder.append("\n")
      }
    }
    builder.toString()
  }

  /**
    * Parse the java source file and return the given method in a class.
    * Signature is a list of type names where the last one is the return type
    */
  def parse_file(path : String,
                 class_name : Option[String],
                 method_name : Option[String],
                 signature : Option[Seq[String]],
                 jar_paths : Seq[String],
                 dir_paths : Seq[String]) : Option[JavaDraft] = {
    val src = scala.io.Source.fromFile(path).mkString
    val parse_result = this.parser.parse(ParseStart.COMPILATION_UNIT, Providers.provider(replace_question_marks(src)))
    if(!parse_result.isSuccessful) {
      logger.error("Failed to parse the java program from path '{}' with class name '{}' and method name '{}'", path, class_name, method_name)
      parse_result.getProblems.foreach(p => {
        logger.error("Message:\n  {}" + p.getMessage)
        logger.error("Cause:\n  {}" + p.getCause)
        logger.error("Range:\n  {}" + p.getRange)
      })
      return None
    }
    get_parse_result(parse_result, class_name, method_name, signature, jar_paths, dir_paths)
  }

  /**
    * Parse the java from a string. Signature is a list of type name where the last one is the return type
    */
  def parse_src(src : String,
                class_name : Option[String],
                method_name : Option[String],
                signature : Option[Seq[String]],
                jar_paths : Seq[String],
                dir_paths : Seq[String]) : Option[JavaDraft] = {
    val parse_result = this.parser.parse(ParseStart.COMPILATION_UNIT, Providers.provider(replace_question_marks(src)))
    if(!parse_result.isSuccessful || !parse_result.getResult.isPresent) {
      logger.error("Failed to parse the java program from source with class name '{}' and method name '{}'", src, class_name, method_name)
      parse_result.getProblems.foreach(p => {
        logger.error("Message:\n  {}" + p.getMessage)
        logger.error("Cause:\n  {}" + p.getCause)
        logger.error("Range:\n  {}" + p.getRange)
      })
      return None
    }
    this.get_parse_result(parse_result, class_name, method_name, signature, jar_paths, dir_paths)
  }

  /**
    * Construct a java draft from a parse result
    */
  private def get_parse_result(parse_result : ParseResult[CompilationUnit],
                               class_name : Option[String],
                               method_name : Option[String],
                               signature : Option[Seq[String]],
                               jar_paths : Seq[String],
                               dir_paths : Seq[String]) : Option[JavaDraft] = {
    val cu = parse_result.getResult.get()
    val cclass =
      if(class_name.isEmpty) {
        var c : Option[ClassOrInterfaceDeclaration] = None
        for(t <- cu.getTypes) {
          t match {
            case ci : ClassOrInterfaceDeclaration => c = Some(ci)
            case _ =>
          }
        }
        if(c.isEmpty) {
          logger.error("There is no class defined in the file.")
          return None
        }
        c.get
      } else {
        val c = cu.getClassByName(class_name.get)
        if(!c.isPresent) {
          logger.error("Cannot find class with name '{}'", class_name)
          return None
        }
        c.get()
      }

    val method =
      if(method_name.isEmpty) {
        // we get the first method
        val all_methods = cclass.getMethods
        if(all_methods.isEmpty) {
          logger.error("There is no method in the class '{}'", cclass.getNameAsString)
          return None
        }
        all_methods.get(0)
      } else {
        val m = this.get_method(cclass, method_name.get, signature)
        if(m.isEmpty) return None
        m.get
      }

    Some(new JavaDraft(cu, cclass, method, jar_paths, dir_paths))
  }

  /**
    * Get a list of type string from a method. The return type is the last.
    */
  def get_type_string(m : MethodDeclaration) : Seq[String] = {
    val result = mutable.ArrayBuffer.empty[String]
    val params = m.getParameters
    for(i <- 0 until params.size()) {
      result += params.get(i).getType.asString()
    }
    result += m.getType.asString()
    result
  }

  /**
    * Get a list of type string from a method. We are ignoring
    */
  def get_type_string(cons : ConstructorDeclaration) : Seq[String] = {
    val result = mutable.ArrayBuffer.empty[String]
    val params = cons.getParameters
    for(i <- 0 until params.size()) {
      result += params.get(i).getType.asString()
    }
    result
  }

  /**
    * Get a method given its name
    */
  def get_method(cclass : ClassOrInterfaceDeclaration, name : String, signature : Option[Seq[String]] = None) : Option[MethodDeclaration] = {

    val all_methods = cclass.getMethodsByName(name)
    if(all_methods.isEmpty) {
      logger.error("There is no method in the class '{}'", cclass.getNameAsString)
      return None
    }

    if(all_methods.size() > 1) {
      logger.warn("There are more than one method with the name '{}'", name)
    }

    if(signature.isDefined) {
      logger.debug("We are using the signature '{}'", signature.get)
      for(method <- all_methods) {
        val type_strings = get_type_string(method)
        if(type_strings.size == signature.get.size && type_strings.zip(signature.get).forall(p => p._1 == p._2)) {
          return Some(method)
        }
      }
      logger.warn("Cannot find method with signature '{}'", signature.get)
      None
    } else {
      Some(all_methods.get(0))
    }
  }

  /**
    * Get all methods within a class
    */
  def get_all_methods(cclass : ClassOrInterfaceDeclaration) : Seq[MethodDeclaration] = cclass.getMethods.asScala

  /**
    * Get a class given its name
    */
  def get_class(unit : CompilationUnit, name : String) : Option[ClassOrInterfaceDeclaration] = {
    val c = unit.getClassByName(name)
    if(c.isPresent) Some(c.get()) else None
  }

  /**
    * This function is used to replace a statement hole using a list of statements
    */
  def replace(method : MethodDeclaration, src : Statement, target : Seq[Statement]): Unit = {
    val src_parent = src.getParentNode
    if(!src_parent.isPresent) return
    src_parent.get() match {
      case bs : BlockStmt =>
        new ModifierVisitor[Any] {
          override def visit(n : BlockStmt, arg : Any): BlockStmt = {
            val stmts = n.getStatements.accept(this, null).asInstanceOf[NodeList[Statement]]
            if(stmts.contains(src)) {
              val new_node = new BlockStmt()

              // don't forget the comments as well
              if(bs.getComment.isPresent) {
                new_node.setComment(bs.getComment.get())
              }

              val index = stmts.indexOf(src)
              stmts.subList(0, index).foreach(s => new_node.getStatements.add(s.clone()))
              target.foreach(t => new_node.getStatements.add(t.clone()))
              if(index + 1 < stmts.size()) {
                stmts.subList(index + 1, stmts.size()).foreach(s => new_node.getStatements.add(s.clone()))
              }
              new_node
            } else {
              n
            }
          }
        }.visit(method, null).asInstanceOf[Node]
      case ses : SwitchEntryStmt =>
        new ModifierVisitor[Any] {
          override def visit(n : SwitchEntryStmt, arg : Any): SwitchEntryStmt = {
            val stmts = n.getStatements.accept(this, null).asInstanceOf[NodeList[Statement]]
            if(stmts.contains(src)) {
              val new_node = new SwitchEntryStmt()

              // don't forget the comments as well
              if(ses.getComment.isPresent) {
                new_node.setComment(ses.getComment.get())
              }

              if(n.getLabel.isPresent) {
                new_node.setLabel(n.getLabel.get())
              }
              val index = stmts.indexOf(src)
              stmts.subList(0, index).foreach(s => new_node.getStatements.add(s.clone()))
              target.foreach(t => new_node.getStatements.add(t.clone()))
              if(index + 1 < stmts.size()) {
                stmts.subList(index + 1, stmts.size()).foreach(s => new_node.getStatements.add(s.clone()))
              }
              new_node
            } else {
              n
            }
          }
        }.visit(method, null).asInstanceOf[Node]
      case _ =>
    }
  }

  /**
    * Parse function-level comment and test
    */
  def parse_comment_test(node : Node): (Option[JavaTestCase], Option[String]) = {
    val comment = node.getComment
    if(!comment.isPresent) { return (None, None) }

    val lines = comment.get().getContent.stripMargin('*').split("\n").map(l => l.trim)
    val test_builder = new StringBuilder
    val comment_builder = new StringBuilder

    var getting_test = false
    var getting_comment = false
    for(l <- lines) {
      if(l == "TEST:") {
        getting_test = true
        getting_comment = false
      } else if(l == "COMMENT:") {
        getting_test = false
        getting_comment = true
      } else if(l.nonEmpty && getting_comment) {
        comment_builder.append(l + "\n")
      } else if(l.nonEmpty && getting_test) {
        test_builder.append(l + "\n")
      }
    }
    val function_test =
      if(test_builder.nonEmpty) {
        Some(new JavaTestCase(test_builder.toString()))
      } else {
        None
      }
    val function_comment = if(comment_builder.nonEmpty) Some(comment_builder.toString()) else None

    (function_test, function_comment)
  }

  /**
    * This function is used to rename the variables inside a method IN PLACE!
    */
  def rename(node : Node, from : String, to : String) : Unit = {
    val renamer = new VoidVisitorAdapter[Any] {
      override def visit(name : NameExpr, arg : Any): Unit = {
        if(name.getNameAsString.equals(from)) { name.setName(to) }
      }

      override def visit(id : SimpleName, arg : Any): Unit = {
        if(id.getIdentifier.equals(from)) { id.setIdentifier(to) }
      }
    }
    node.accept(renamer, null)
  }

  def rename(node : Node, mapping : Map[String, String]) : Unit = {
    val renamer = new VoidVisitorAdapter[Any] {
      override def visit(name : NameExpr, arg : Any): Unit = {
        if(mapping.contains(name.getNameAsString)) { name.setName(mapping(name.getNameAsString)) }
      }

      override def visit(id : SimpleName, arg : Any): Unit = {
        if(mapping.contains(id.getIdentifier)) { id.setIdentifier(mapping(id.getIdentifier)) }
      }
    }
    node.accept(renamer, null)
  }


  /**
    * Return the size of a given node
    */
  def node_size(in_node : Node): Int = {
    var node_size = 0
    new TreeVisitor() {
      override def process(node : Node): Unit = {
        for(n <- node.getChildNodes) {
          node_size += 1
        }
      }
    }.visitDepthFirst(in_node)
    node_size + 1
  }

  /**
    * Post process on a Java draft solution
    */
  def post_process(jd: JavaDraft) : JavaDraft = {
    val new_method = jd.method.clone()

    // get rid of unreachable code after return statement
    new ModifierVisitor[Any] {
      override def visit(n : BlockStmt, arg : Any): BlockStmt = {
        val stmts = n.getStatements.accept(this, null).asInstanceOf[NodeList[Statement]]
        val new_stmts = new NodeList[Statement]
        var saw_return : Boolean = false
        for(s <- stmts) {
          if(!saw_return) {
            new_stmts.add(s)
            if(s.isInstanceOf[ReturnStmt]) {
              saw_return = true
            }
          }
        }
        new BlockStmt(new_stmts)
      }

      override def visit(n : SwitchEntryStmt, arg : Any): SwitchEntryStmt = {
        val stmts = n.getStatements.accept(this, null).asInstanceOf[NodeList[Statement]]
        val new_stmts = new NodeList[Statement]
        var saw_return : Boolean = false
        for(s <- stmts) {
          if(!saw_return) {
            new_stmts.add(s)
            if(s.isInstanceOf[ReturnStmt]) {
              saw_return = true
            }
          }
        }
        if(n.getLabel.isPresent) {
          new SwitchEntryStmt(n.getLabel.get(), new_stmts)
        } else {
          new SwitchEntryStmt(null, new_stmts)
        }
      }
    }.visit(new_method, null)

    jd.create_new_draft(new_method)
  }

  /**
    * Return a set of imported class names
    */
  def get_imported_class_names(compilation_unit: CompilationUnit) : mutable.Map[String, Name] = {
    val result = mutable.Map.empty[String, Name]
    // result ++= this.BUILT_IN_CLASS_NAMES
    result.put("System", JavaUtils.name_list_to_qname(Array("java", "lang", "System")))
    compilation_unit.getImports.foreach(im => {
      if(!im.isAsterisk) {
        val id = im.getName.getId
        result(id) = im.getName
      }
    })
    result
  }

  /**
    * Test whether the given node is a hole
    */
  def is_hole(node : Node) : Boolean = node match {
    case mce : MethodCallExpr =>
      mce.getName.toString == JavaUtils.HOLE_NAME
    case _ => false
  }

  /**
    * Check whether this hole is an expression hole or not
    */
  def is_expr_hole(node: Node): Boolean = {
    node match {
      case mce : MethodCallExpr if mce.getNameAsString.equals(JavaUtils.HOLE_NAME) =>
        if(mce.getParentNode.isPresent && mce.getParentNode.get().isInstanceOf[ExpressionStmt]) {
          false
        } else {
          true
        }
      case _ => false
    }
  }

  /**
    * Test whether this AST has a node
    */
  def has_hole(node : Node) : Boolean = {
    var result = false
    new TreeVisitor {
      override def process(node : Node): Unit = {
        node match {
          case mce : MethodCallExpr if mce.getNameAsString.equals(JavaUtils.HOLE_NAME) =>
            result = true
          case mce : MethodCallExpr =>
            if(mce.getScope.isPresent) {
              this.visitPreOrder(mce.getScope.get())
            }
            for(a <- mce.getArguments) {
              this.visitPreOrder(a)
            }
          case _ =>
        }
      }
    }.visitPreOrder(node)
    result
  }

  /**
    * Get the type of the hole from its second argument
    */
  def get_hole_typename(node : Node) : Option[String] = {
    val call = node.asInstanceOf[MethodCallExpr]
    val args = call.getArguments

    // the type information is at the second argument
    if(args.size() < 2) {
      return None
    }
    Some(args.get(1).asInstanceOf[StringLiteralExpr].asString())
  }

  /**
    * Generate a hole with no tests. A class name can be provided for type inference.
    */
  def gen_expr_hole_without_test(clazz : Option[Class[_]] = None) : Expression = {
    val args = new NodeList[Expression]()
    args.add(new IntegerLiteralExpr(Integer.toString(this.next_hole_token())))
    if(clazz.isDefined) {
      args.add(new StringLiteralExpr(clazz.get.getName))
    }
    new MethodCallExpr().setName(this.HOLE_NAME).setArguments(args)
  }

  /**
    * Generate a hole with no tests
    */
  def gen_stmt_hole_without_test() : Statement = {
    val args = new NodeList[Expression]()
    args.add(new IntegerLiteralExpr(Integer.toString(this.next_hole_token())))
    val hole = new MethodCallExpr().setName(this.HOLE_NAME).setArguments(args)
    new ExpressionStmt(hole)
  }

  def next_hole_token() : Int = {
    val v = this.hole_token
    this.hole_token += 1
    v
  }

  /**
    * A function for converting class names into Name AST node
    */
  def classname_to_name(class_name : String) : Name = {
    def helper(name_list : Seq[String]) : Name = {
      if(name_list.size == 1) {
        new Name(name_list.head)
      } else {
        new Name(helper(name_list.tail), name_list.head)
      }
    }

    helper(class_name.split("\\.").reverse)
  }

  /**
    * Convert a qualified name to a field access expression
    */
  def qname_to_fieldaccess(qname : Name) : FieldAccessExpr = {
    def helper(name : Name) : FieldAccessExpr = {
      if(!name.getQualifier.get().getQualifier.isPresent) {
        new FieldAccessExpr(new NameExpr(name.getQualifier.get().getIdentifier), name.getIdentifier)
      } else {
        new FieldAccessExpr(helper(name.getQualifier.get()), name.getIdentifier)
      }
    }
    assert(qname.getQualifier.isPresent)
    helper(qname)
  }

  /**
    * Convert a list of string separated by dots into a field access expression
    * @param name_list - a list of strings. If we have [a, b, c, method], then the resulting expression is a.b.c.method
    */
  def name_list_to_fieldaccess(name_list : Array[String]) : FieldAccessExpr = {
    def helper(nlist : Array[String], pos : Int) : FieldAccessExpr = {
      if(nlist.length - pos < 2) {
        assert(assertion = false, "Must have at least two names")
        null
      } else if(nlist.length - pos <= 2) {
        new FieldAccessExpr().setScope(new NameExpr(nlist(pos + 1))).setName(new SimpleName(nlist(pos)))
      } else {
        new FieldAccessExpr().setName(new SimpleName(nlist(pos))).setScope(helper(nlist, pos + 1))
      }
    }
    helper(name_list.reverse, 0)
  }

  /**
    * Convert a list of string separated by dots into a field access expression
    * @param name_list - a list of strings. If we have [a, b, c, method], then the resulting expression is a.b.c.method
    */
  def name_list_to_qname(name_list : Seq[String]) : Name = {
    def helper(nlist : Seq[String], pos : Int) : Name = {
      if(pos + 1 == nlist.length) {
        new Name(nlist(pos))
      } else {
        new Name(helper(nlist, pos + 1), nlist(pos))
      }
    }
    helper(name_list.reverse, 0)
  }

  /**
    * Test whether the enclosing node contains the target node
    */
  def node_contains(enclosing : Node, target : Node) : Boolean = {
    var result = false
    new TreeVisitor {
      override def process(node: Node): Unit = result = node.equals(target)
    }.visitPostOrder(enclosing)
    result
  }

  /**
    * Get an expression hole from a node
    */
  def get_expr_hole(n : Node) : Option[Node] = {
    var hole : Option[Node] = None
    new TreeVisitor {
      override def process(node : Node): Unit = {
        node match {
          case mce : MethodCallExpr =>
            if(hole.isEmpty &&
              mce.getNameAsString.equals(JavaUtils.HOLE_NAME) &&
              mce.getParentNode.isPresent &&
              !mce.getParentNode.get().isInstanceOf[ExpressionStmt]) {
              hole = Some(mce)
            }
          case _ => Unit
        }
      }
    }.visitPreOrder(n)
    hole
  }

  /**
    * Get all the expression holes in a node
    */
  def get_all_expr_hole(n : Node) : Seq[Node] = {
    val result = mutable.ArrayBuffer.empty[Node]
    new TreeVisitor {
      override def process(node : Node) : Unit = {
        node match {
          case mce : MethodCallExpr =>
            if(mce.getNameAsString.equals(JavaUtils.HOLE_NAME) &&
              mce.getParentNode.isPresent &&
              !mce.getParentNode.get().isInstanceOf[ExpressionStmt]) {
              result += mce
            }
          case _ => Unit
        }
      }
    }.visitPreOrder(n)
    result
  }

  /**
    * Get all the holes in a node
    */
  def get_all_holes(n : Node) : Seq[Node] = {
    val result = mutable.ArrayBuffer.empty[Node]
    new TreeVisitor {
      override def process(node : Node) : Unit = {
        logger.debug("Processing node {}", node.toString)
        node match {
          case mce : MethodCallExpr =>
            if(mce.getNameAsString.equals(JavaUtils.HOLE_NAME)) {
              result += mce
            }
          case vdor : VariableDeclarator =>
            if(vdor.getInitializer.isPresent) {
              this.visitPreOrder(vdor.getInitializer.get())
            }
          case assign : AssignExpr =>
            this.visitPreOrder(assign.getTarget)
            this.visitPreOrder(assign.getValue)
          case _ => Unit
        }
      }
    }.visitPreOrder(n)
    result
  }

  /**
    * Get the number of holes in this ast node
    */
  def get_hole_count(n : Node) : Int = {
    var result : Int = 0
    new TreeVisitor {
      override def process(node: Node): Unit = {
        node match {
          case mce : MethodCallExpr if mce.getName.asString() == JavaUtils.HOLE_NAME =>
            result += 1
          case _ =>
        }
      }
    }.visitPreOrder(n)
    result
  }


  /**
    * Get all name usages
    */
  def get_usages(node : Node) : mutable.Set[NameExpr] = {
    val result = mutable.Set.empty[NameExpr]
    val visitor = new VoidVisitorAdapter[Any] {
      override def visit(name : NameExpr, arg : Any) : Unit = {
        result += name
      }
    }
    node.accept[Any](visitor, null)
    result
  }

  /**
    * Get all name definitions
    */
  def get_defs(node : Node) : mutable.Set[VariableDeclarator] = {
    val result = mutable.Set.empty[VariableDeclarator]
    val visitor = new VoidVisitorAdapter[Any] {
      override def visit(vdor : VariableDeclarator, arg : Any) : Unit = {
        result += vdor
      }
    }
    node.accept[Any](visitor, null)
    result
  }

  /**
    * Test whether n1 depends on n2 in terms of name usages and definitions
    */
  def depends(n1 : Node, n2 : Node) : Boolean =
    this.get_usages(n1).map(ne => ne.getNameAsString)
      .intersect(this.get_defs(n2).map(vdor => vdor.getNameAsString)).nonEmpty

  /**
    * Climb up the AST and get the enclosing statement. If it is a statement, then we return the input
    */
  def get_enclosing_statement(node : Node) : Option[Statement] = {
    node match {
      case stmt: Statement => Some(stmt)
      case e : Expression =>
        if(e.getParentNode.isPresent) {
          val parent = e.getParentNode.get()
          parent match {
            case statement: Statement =>
              Some(statement)
            case _ =>
              get_enclosing_statement(e.getParentNode.get())
          }
        } else {
          None
        }
      case _ =>
        if(node.getParentNode.isPresent) {
          get_enclosing_statement(node.getParentNode.get())
        } else {
          None
        }
    }
  }

  /**
    * Get the enclosing method decl for this node
    */
  def get_enclosing_method(node : Node) : Option[MethodDeclaration] = {
    node match {
      case md : MethodDeclaration => Some(md)
      case _ =>
        if(node.getParentNode.isPresent) {
          get_enclosing_method(node.getParentNode.get())
        } else {
          None
        }
    }
  }

  /**
    * Convert a class to its semantic type
    */
  def class_to_type(clazz : Class[_], type_solver : JavaParserFacade) : javaparser.symbolsolver.model.typesystem.Type = {
    val tname = clazz.getTypeName
    tname match {
      case "byte" => javaparser.symbolsolver.model.typesystem.PrimitiveType.BYTE
      case "int" => javaparser.symbolsolver.model.typesystem.PrimitiveType.INT
      case "short" => javaparser.symbolsolver.model.typesystem.PrimitiveType.SHORT
      case "char" => javaparser.symbolsolver.model.typesystem.PrimitiveType.CHAR
      case "long" => javaparser.symbolsolver.model.typesystem.PrimitiveType.LONG
      case "boolean" => javaparser.symbolsolver.model.typesystem.PrimitiveType.BOOLEAN
      case "float" => javaparser.symbolsolver.model.typesystem.PrimitiveType.FLOAT
      case "double" => javaparser.symbolsolver.model.typesystem.PrimitiveType.DOUBLE
      case "void" => javaparser.symbolsolver.model.typesystem.VoidType.INSTANCE
      case "null" => javaparser.symbolsolver.model.typesystem.NullType.INSTANCE
      case _ if clazz.isArray => new javaparser.symbolsolver.model.typesystem.ArrayType(class_to_type(clazz.getComponentType, type_solver))
      case _ =>
        val cd = new ClassOrInterfaceDeclaration(java.util.EnumSet.of(Modifier.PUBLIC), false, tname)
        new javaparser.symbolsolver.model.typesystem.ReferenceTypeImpl(new javaparser.symbolsolver.javaparsermodel.declarations.JavaParserClassDeclaration(cd, type_solver.getTypeSolver), type_solver.getTypeSolver)
    }
  }

  /**
    * Convert a string into a syntax type
    */
  def str_to_syntax_type(s : String) : javaparser.ast.`type`.Type = {
    s match {
      case "byte" => javaparser.ast.`type`.PrimitiveType.byteType()
      case "int" => javaparser.ast.`type`.PrimitiveType.intType()
      case "short" => javaparser.ast.`type`.PrimitiveType.shortType()
      case "char" => javaparser.ast.`type`.PrimitiveType.charType()
      case "long" => javaparser.ast.`type`.PrimitiveType.longType()
      case "boolean" => javaparser.ast.`type`.PrimitiveType.booleanType()
      case "float" => javaparser.ast.`type`.PrimitiveType.floatType()
      case "double" => javaparser.ast.`type`.PrimitiveType.doubleType()
      case "void" => new javaparser.ast.`type`.VoidType()
      case "B" => javaparser.ast.`type`.PrimitiveType.byteType()
      case "S" => javaparser.ast.`type`.PrimitiveType.shortType()
      case "C" => javaparser.ast.`type`.PrimitiveType.charType()
      case "I" => javaparser.ast.`type`.PrimitiveType.intType()
      case "J" => javaparser.ast.`type`.PrimitiveType.longType()
      case "Z" => javaparser.ast.`type`.PrimitiveType.booleanType()
      case "F" => javaparser.ast.`type`.PrimitiveType.floatType()
      case "D" => javaparser.ast.`type`.PrimitiveType.doubleType()
      case _ if s.startsWith("[") =>
        s.charAt(1) match {
          case 'B' => new javaparser.ast.`type`.ArrayType(javaparser.ast.`type`.PrimitiveType.byteType())
          case 'S' => new javaparser.ast.`type`.ArrayType(javaparser.ast.`type`.PrimitiveType.shortType())
          case 'C' => new javaparser.ast.`type`.ArrayType(javaparser.ast.`type`.PrimitiveType.charType())
          case 'I' => new javaparser.ast.`type`.ArrayType(javaparser.ast.`type`.PrimitiveType.intType())
          case 'J' => new javaparser.ast.`type`.ArrayType(javaparser.ast.`type`.PrimitiveType.longType())
          case 'Z' => new javaparser.ast.`type`.ArrayType(javaparser.ast.`type`.PrimitiveType.booleanType())
          case 'F' => new javaparser.ast.`type`.ArrayType(javaparser.ast.`type`.PrimitiveType.floatType())
          case 'D' => new javaparser.ast.`type`.ArrayType(javaparser.ast.`type`.PrimitiveType.doubleType())
          case 'L' => new javaparser.ast.`type`.ArrayType(str_to_syntax_type(s.substring(2, s.length - 1)))
          case '[' => new javaparser.ast.`type`.ArrayType(str_to_syntax_type(s.substring(1, s.length)))
        }
      case _ if s.endsWith("[]") => new javaparser.ast.`type`.ArrayType(str_to_syntax_type(s.substring(0, s.length - 2)))
      case _ => JavaParser.parseClassOrInterfaceType(s)
    }
  }


  def str_to_type(s : String, type_solver : JavaParserFacade) : Type = {
    s match {
      case "byte" => PrimitiveType.BYTE
      case "int" => PrimitiveType.INT
      case "short" => PrimitiveType.SHORT
      case "char" => PrimitiveType.CHAR
      case "long" => PrimitiveType.LONG
      case "boolean" => PrimitiveType.BOOLEAN
      case "float" => PrimitiveType.FLOAT
      case "double" => PrimitiveType.DOUBLE
      case "void" => VoidType.INSTANCE
      case "null" => NullType.INSTANCE
      case "B" => PrimitiveType.BYTE
      case "S" => PrimitiveType.SHORT
      case "C" => PrimitiveType.CHAR
      case "I" => PrimitiveType.INT
      case "J" => PrimitiveType.LONG
      case "Z" => PrimitiveType.BOOLEAN
      case "F" => PrimitiveType.FLOAT
      case "D" => PrimitiveType.DOUBLE
      case _ =>
        if(s.startsWith("[")) {
          s.charAt(1) match {
            case 'B' => new ArrayType(PrimitiveType.BYTE)
            case 'S' => new ArrayType(PrimitiveType.SHORT)
            case 'C' => new ArrayType(PrimitiveType.CHAR)
            case 'I' => new ArrayType(PrimitiveType.INT)
            case 'J' => new ArrayType(PrimitiveType.LONG)
            case 'Z' => new ArrayType(PrimitiveType.BOOLEAN)
            case 'F' => new ArrayType(PrimitiveType.FLOAT)
            case 'D' => new ArrayType(PrimitiveType.DOUBLE)
            case 'L' => new ArrayType(str_to_type(s.substring(2, s.length - 1), type_solver))
            case '[' => new ArrayType(str_to_type(s.substring(1, s.length), type_solver))
          }
        } else {
          val cd = new ClassOrInterfaceDeclaration(java.util.EnumSet.of(Modifier.PUBLIC), false, s)
          new ReferenceTypeImpl(new JavaParserClassDeclaration(cd, type_solver.getTypeSolver), type_solver.getTypeSolver)
        }
    }

  }

  /**
    * Get the function type from a method object
    */
  def get_functype(m : Method, type_solver : JavaParserFacade) : FuncType = {
    // get parameter types
    val param_types = m.getParameters.map(p => this.class_to_type(p.getType, type_solver))

    // get return type
    val ret_type = this.class_to_type(m.getReturnType, type_solver)

    // put the type into the set
    new FuncType(param_types, ret_type)
  }

  /**
    * Get the function type from a constructor
    */
  def get_functype(cons : Constructor[_], type_solver : JavaParserFacade) : FuncType = {
    // get parameter types
    val param_types = cons.getParameters.map(p => this.class_to_type(p.getType, type_solver))

    // get return type
    val ret_type = this.class_to_type(cons.getDeclaringClass, type_solver)

    // put the type into the set
    new FuncType(param_types, ret_type)
  }

  def has_condition(node : Node) : Boolean = {
    var result = false
    val visitor = new VoidVisitorAdapter[Any] {
      override def visit(node : IfStmt, args : Any) : Unit = {
        result = true
      }
      override def visit(node : SwitchStmt, args : Any) : Unit = {
        result = true
      }
      override def visit(node : SwitchEntryStmt, args : Any) : Unit = {
        result = true
      }
    }
    node.accept(visitor, null)
    result
  }

  def has_loop(node : Node) : Boolean = {
    var result = false
    val visitor = new VoidVisitorAdapter[Any] {
      override def visit(node : ForStmt, args : Any) : Unit = {
        result = true
      }
      override def visit(node : ForeachStmt, args : Any) : Unit = {
        result = true
      }
      override def visit(node : WhileStmt, args : Any) : Unit = {
        result = true
      }
      override def visit(node : DoStmt, args : Any) : Unit = {
        result = true
      }
    }
    node.accept(visitor, null)
    result
  }


  def get_label(expr : Node) : String = {
    def helper_list(nlist : NodeList[Node]) : String = {
      val builder = new StringBuilder
      for(i <- 0 until nlist.size()) {
        builder.append(helper(nlist.get(i)))
      }
      builder.toString()
    }

    def helper_option(o : Optional[Node]) : String = {
      if(o.isPresent) helper(o.get()) else ""
    }

    def helper(node : Node) : String = {
      node match {
        case aae : ArrayAccessExpr => "{ArrayAccess" + helper(aae.getName) + helper(aae.getIndex) + "}"
        case ace : ArrayCreationExpr =>
          val builder = new StringBuilder
          builder.append("{ArrayCreation")
          for(i <- 0 until ace.getLevels.size()) {
            val level = ace.getLevels.get(i)
            if(level.getDimension.isPresent) {
              builder.append(helper(level.getDimension.get()))
            }
          }
          builder.append("}")
          builder.toString()
        case aie : ArrayInitializerExpr =>
          "{ArrayInitializer" + helper_list(aie.getValues.asInstanceOf[NodeList[Node]]) + "}"
        case ae : AssignExpr =>
          "{Assign" + helper(ae.getTarget) + "{" + ae.getOperator.asString() + "}" + helper(ae.getValue) + "}"
        case be : BinaryExpr =>
          "{Binary" + helper(be.getLeft) + "{" + be.getOperator.asString() + "}" + helper(be.getRight) + "}"
        case ble : BooleanLiteralExpr =>
          "{" + ble.getValue.toString + "}"
        case ce : CastExpr =>
          "{Cast" + ce.getType.toString + helper(ce.getExpression) + "}"
        case cle : CharLiteralExpr =>
          "{" + cle.asChar() + "}"
        // case classe : ClassExpr =>
        case conde : ConditionalExpr =>
          "{Conditional" + helper(conde.getCondition) + helper(conde.getThenExpr) + helper(conde.getElseExpr) + "}"
        case dle : DoubleLiteralExpr =>
          "{" + dle.asDouble() + "}"
        case enc : EnclosedExpr =>
          "{Enclosed" + helper(enc.getInner) + "}"
        case fae : FieldAccessExpr =>
          "{FieldAccess" + helper(fae.getScope) + "{" + fae.getName.asString() + "}" + "}"
        case ioe : InstanceOfExpr =>
          "{InstanceOf" + helper(ioe.getExpression) + "{" + ioe.getType.toString + "}" + "}"
        case ile : IntegerLiteralExpr =>
          "{" + ile.asInt() + "}"
        case le : LambdaExpr =>
          "{Lambda}"
        case lte : LongLiteralExpr =>
          "{" + lte.asLong() + "}"
        // case (mae1 : MarkerAnnotationExpr, mae2 : MarkerAnnotationExpr) =>
        case mce : MethodCallExpr =>
          "{MethodCall" + helper_option(mce.getScope.asInstanceOf[Optional[Node]]) + "{" + mce.getName.asString() + "}" + helper_list(mce.getArguments.asInstanceOf[NodeList[Node]]) + "}"
        case mre : MethodReferenceExpr =>
          "{MethodReference" + helper(mre.getScope) + "{" + mre.getIdentifier + "}" + "}"
        case ne : NameExpr =>
          "{Name" + "{" + ne.getName.asString() + "}}"
        case nae : NormalAnnotationExpr =>
          "{NormalAnnotation}"
        case nle : NullLiteralExpr =>
          "{Null}"
        case oce : ObjectCreationExpr =>
          "{ObjectCreation" + helper_option(oce.getScope.asInstanceOf[Optional[Node]]) + helper_list(oce.getArguments.asInstanceOf[NodeList[Node]]) + "}"
        case smae : SingleMemberAnnotationExpr =>
          "{SingleMemberAnnotation" + helper(smae.getMemberValue) + "}"
        case sle : StringLiteralExpr =>
          "{String{" + sle.asString() + "}}"
        case se : SuperExpr =>
          "{Super" + helper_option(se.getClassExpr.asInstanceOf[Optional[Node]]) + "}"
        case te : ThisExpr =>
          "{This" + helper_option(te.getClassExpr.asInstanceOf[Optional[Node]]) + "}"
        case tpe : TypeExpr =>
          "{Type{" + tpe.getType.asString() + "}}"
        case ue : UnaryExpr =>
          "{Unary{" + ue.getOperator.asString() + "}" + helper(ue.getExpression) + "}"
        case vde : VariableDeclarationExpr =>
          val result = new StringBuilder
          result.append("{VariableDeclaration")
          for(i <- 0 until vde.getVariables.size()) {
            val decl = vde.getVariables.get(i)
            val vname = decl.getName.asString()
            val t = decl.getType.asString()
            val init = decl.getInitializer
            result.append("{Decl")
            result.append("{" + t + "}")
            result.append("{" + vname + "}")
            result.append(helper_option(init.asInstanceOf[Optional[Node]]))
            result.append("}")
          }
          result.append("}")
          result.toString()

        // statements
        case as : AssertStmt =>
          "{AssertStmt" + helper(as.getCheck) + "}"
        case bs : BlockStmt =>
          "{BlockStmt" + helper_list(bs.getStatements.asInstanceOf[NodeList[Node]]) + "}"
        case brs : BreakStmt =>
          "{BreakStmt}"
        case cc : CatchClause =>
          "{Catch" + helper(cc.getParameter.asInstanceOf[Node]) + helper(cc.getBody) + "}"
        case cs : ContinueStmt =>
          "{ContinueStmt}"
        case ds : DoStmt =>
          "{DoStmt" + helper(ds.getCondition) + helper(ds.getBody.asInstanceOf[Node]) + "}"
        case es : EmptyStmt =>
          "{EmptyStmt}"
        case exprstmt : ExpressionStmt =>
          "{ExprStmt" + helper(exprstmt.getExpression) + "}"
        case fes : ForeachStmt =>
          "{ForeachStmt" + helper(fes.getIterable) + helper(fes.getVariable) + helper(fes.getBody) + "}"
        case forstmt : ForStmt =>
          "{ForStmt" +
            helper_list(forstmt.getInitialization.asInstanceOf[NodeList[Node]]) +
            helper_option(forstmt.getCompare.asInstanceOf[Optional[Node]]) +
            helper_list(forstmt.getUpdate.asInstanceOf[NodeList[Node]]) +
            helper(forstmt.getBody) +
            "}"
        case ifstmt : IfStmt =>
          "{IfStmt" +
            helper(ifstmt.getCondition) +
            helper(ifstmt.getThenStmt) +
            helper_option(ifstmt.getElseStmt.asInstanceOf[Optional[Node]]) +
            "}"
        case lbstmt : LabeledStmt =>
          "{LabeledStmt" + "{" + lbstmt.getLabel.asString() + "}" + helper(lbstmt.getStatement) + "}"
        case rtnstmt : ReturnStmt =>
          "{ReturnStmt" + helper_option(rtnstmt.getExpression.asInstanceOf[Optional[Node]]) + "}"
        case swestmt : SwitchEntryStmt =>
          "{SwitchEntryStmt" +
            helper_option(swestmt.getLabel.asInstanceOf[Optional[Node]]) +
            helper_list(swestmt.getStatements.asInstanceOf[NodeList[Node]]) +
            "}"
        case swstmt : SwitchStmt =>
          "{SwitchStmt" + helper(swstmt.getSelector) + helper_list(swstmt.getEntries.asInstanceOf[NodeList[Node]]) + "}"
        case synstmt : SynchronizedStmt =>
          "{SynStmt" + helper(synstmt.getExpression) + helper(synstmt.getBody) + "}"
        case throwstmt : ThrowStmt =>
          "{ThrowStmt" + helper(throwstmt.getExpression) + "}"
        case trystmt : TryStmt =>
          "{TryStmt" +
            helper(trystmt.getTryBlock) +
            helper_list(trystmt.getCatchClauses.asInstanceOf[NodeList[Node]]) +
            helper_option(trystmt.getFinallyBlock.asInstanceOf[Optional[Node]]) +
            "}"
        case whilestmt : WhileStmt =>
          "{WhileStmt" + helper(whilestmt.getCondition) + helper(whilestmt.getBody) + "}"
        case param : Parameter =>
          "{Param" + "{" + param.getType.toString + "}" + "{" + param.getName.asString() + "}}"
        case mtd : MethodDeclaration =>
          "{Method" + helper_list(mtd.getParameters.asInstanceOf[NodeList[Node]]) + helper_option(mtd.getBody.asInstanceOf[Optional[Node]]) + "}"
      }
    }

    helper(expr)
  }

  /**
    * A class used to replace a node in the AST with another node
    */
  class ReplaceModifier(val src : Node, val target : Node) extends ModifierVisitor[Any] {
    override def visit(n : AnnotationDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : AnnotationMemberDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ArrayAccessExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ArrayCreationExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ArrayCreationLevel, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ArrayInitializerExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : javaparser.ast.`type`.ArrayType, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : AssertStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : AssignExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : BinaryExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : BlockComment, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : BlockStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : BooleanLiteralExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : BreakStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : CastExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : CatchClause, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : CharLiteralExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ClassExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ClassOrInterfaceDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ClassOrInterfaceType, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : CompilationUnit, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ConditionalExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ConstructorDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ContinueStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : DoStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : DoubleLiteralExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : EmptyStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : EnclosedExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : EnumConstantDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : EnumDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ExplicitConstructorInvocationStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ExpressionStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : FieldAccessExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : FieldDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ForeachStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ForStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : IfStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ImportDeclaration, arg : Any) : Node = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : InitializerDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : InstanceOfExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : IntegerLiteralExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : javaparser.ast.`type`.IntersectionType, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : JavadocComment, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : LabeledStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : LambdaExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : LineComment, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : LocalClassDeclarationStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : LongLiteralExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : MarkerAnnotationExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : MemberValuePair, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : MethodCallExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : MethodDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : MethodReferenceExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ModuleDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ModuleExportsStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ModuleOpensStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ModuleProvidesStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ModuleRequiresStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ModuleUsesStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : Name, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : NameExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : NodeList[_ <: Node], arg : Any) : Visitable = {
      val node_list = n.asInstanceOf[NodeList[Node]]
      for(i <- 0 until n.size()) {
        node_list.set(i, n.get(i).accept(this, arg).asInstanceOf[Node])
      }
      node_list
    }
    override def visit(n : NormalAnnotationExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : NullLiteralExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ObjectCreationExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : PackageDeclaration, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : Parameter, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : javaparser.ast.`type`.PrimitiveType, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ReturnStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : SimpleName, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : SingleMemberAnnotationExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : StringLiteralExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : SuperExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : SwitchEntryStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : SwitchStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : SynchronizedStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ThisExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : ThrowStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : TryStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : TypeExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : javaparser.ast.`type`.TypeParameter, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : UnaryExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : javaparser.ast.`type`.UnionType, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : javaparser.ast.`type`.UnknownType, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : UnparsableStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : VariableDeclarationExpr, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : VariableDeclarator, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : javaparser.ast.`type`.VoidType, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : WhileStmt, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
    override def visit(n : javaparser.ast.`type`.WildcardType, arg : Any) : Visitable = { if(n == src) { n.setParentNode(null); return target }; super.visit(n, arg) }
  }

  def replace(enclosing : Node, src : Node, target : Node) : Node =
    enclosing.accept[Visitable, Any](new ReplaceModifier(src, target), null).asInstanceOf[Node]

}
