package edu.rice.pliny.language.java

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.visitor.TreeVisitor
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft.{DraftNode, DraftNodeIterator}

import scala.collection.mutable
import scala.collection.JavaConversions._

/**
  * This represents a program in any language
  */
class JavaDraftNodeIterator(draftnode : JavaDraftNode, condition: Option[Node => Boolean] = None) extends DraftNodeIterator(draftnode) {

  val logger = Logger(this.getClass)
  /**
    * Used to keep track of index
    */
  var curr_index : Int = 0

  /**
    * Used to store the nodes
    */
  val postorder_mapping : mutable.ArrayBuffer[JavaDraftNode] = mutable.ArrayBuffer.empty[JavaDraftNode]
  this.initialize()

  /**
    * The current node
    */
  override def get_curr_node : Option[JavaDraftNode] = if(this.postorder_mapping.nonEmpty && this.curr_index < this.postorder_mapping.size) Some(this.postorder_mapping(this.curr_index)) else None

  override def get_all_nodes: Array[DraftNode] = this.postorder_mapping.toArray

  /**
    * This initialization function mainly builds the post order mapping array
    */
  private def initialize() : Unit = {
    new TreeVisitor() {
      override def process(node: Node): Unit =  {
        if((condition.isEmpty || condition.get(node)) && node != null) {
          postorder_mapping += new JavaDraftNode(node)
        }
      }
    }.visitPostOrder(draftnode.node)
    this.curr_index = 0
  }

  override def next(): DraftNode = {
    val result = postorder_mapping(this.curr_index)
    this.curr_index += 1
    result
  }

  override def hasNext: Boolean = this.curr_index < this.postorder_mapping.size
}
