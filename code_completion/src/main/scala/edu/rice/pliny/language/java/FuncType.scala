package edu.rice.pliny.language.java

import com.github.javaparser.symbolsolver.model.typesystem.Type
import com.typesafe.scalalogging.Logger

/**
  * Java function type
  */
class FuncType(val params: Seq[Type], val ret: Type) extends Type {
  val logger = Logger(this.getClass)
  /**
    * Check if other type is a subtype of this function type
    */
  override def isAssignableBy(other: Type): Boolean = {
    this.logger.error("Please use Java draft class to check function subtyping.")
    false
  }


  override def describe(): String = {
    val result = new StringBuilder
    result.append("(")
    var counter = 0
    for (p <- params) {
      if (counter > 0) {
        result.append(", ")
      }
      result.append(p.toString)
      counter += 1
    }
    result.append(")")
    result.append(" => " + ret.toString)
    result.toString()
  }

  override def toString : String = this.describe()

  override def equals(other : Any) : Boolean = {
    if(!other.isInstanceOf[FuncType]) {
      return false
    }

    val of = other.asInstanceOf[FuncType]
    if(this.params.size != of.params.size) {
      return false
    }

    this.params.zip(of.params).forall(p => p._1.equals(p._2)) && this.ret.equals(of.ret)
  }
}
