package edu.rice.pliny.language.java

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.body.{MethodDeclaration, VariableDeclarator}
import com.github.javaparser.ast.expr._
import com.github.javaparser.ast.visitor.TreeVisitor
import edu.rice.pliny.draft.{DraftNode, DraftRefIterator}

import scala.collection.mutable
import scala.collection.JavaConversions._

class JavaRefIterator(draftnode : JavaDraftNode, predicate : Option[Node => Boolean] = None) extends DraftRefIterator(draftnode) {

  /**
    * Used to keep track of index
    */
  var curr_index : Int = 0

  /**
    * Used to store the nodes
    */
  val postorder_mapping : mutable.ArrayBuffer[JavaDraftNode] = mutable.ArrayBuffer.empty[JavaDraftNode]
  this.initialize()

  override def get_curr_node: JavaDraftNode = this.postorder_mapping(this.curr_index)
  override def get_all_nodes: Array[DraftNode] = this.postorder_mapping.toArray

  private def add_node(node : Node) = {
    if(predicate.isEmpty || predicate.get(node)) {
      postorder_mapping += new JavaDraftNode(node)
    }
  }

  /**
    * This initialization function mainly builds the post order mapping array
    */
  private def initialize() : Unit = {
    curr_index = 0
    new TreeVisitor {
      override def process(node: Node): Unit =  {
        node match {
          case m : MethodDeclaration =>
            for(p <- m.getParameters) {
              add_node(new NameExpr(p.getName))
            }
            if(m.getBody.isPresent) {
              visitDepthFirst(m.getBody.get())
            }
          case vdor : VariableDeclarator => add_node(new NameExpr(vdor.getName))
          case n : NameExpr if n.getName != null => add_node(node)
          case f : FieldAccessExpr => add_node(node)
          case a : ArrayAccessExpr => add_node(a.getName)
          case _ =>
        }
      }

      override def visitDepthFirst(node : Node) : Unit = {
        node match {
          case m : MethodDeclaration =>
            for(p <- m.getParameters) {
              add_node(new NameExpr(p.getName))
            }
            if(m.getBody.isPresent) {
              visitDepthFirst(m.getBody.get())
            }
          case n : NameExpr => process(n)
          case f : FieldAccessExpr =>
            if(f.getScope != null) {
              visitDepthFirst(f.getScope)
            }
            visitDepthFirst(f.getName)
            add_node(node)
          case vdor : VariableDeclarator => add_node(new NameExpr(vdor.getName))
          case a : ArrayAccessExpr => process(a.getIndex); process(a.getName)
          case _ =>
            for(c <- node.getChildNodes) { visitDepthFirst(c) }
        }
      }
    }.visitDepthFirst(draftnode.node)
  }

  override def next(): DraftNode = {
    val result = postorder_mapping(this.curr_index)
    this.curr_index += 1
    result
  }

  override def hasNext: Boolean = this.curr_index < this.postorder_mapping.size

}
