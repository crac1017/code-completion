package edu.rice.pliny.language.java

import java.io.FileWriter

import edu.rice.pliny.{AppConfig, Utils}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft.{Draft, DraftInterpreter, TestCase}

import scala.concurrent._
import scala.concurrent.duration._
import scala.sys.process._
import ExecutionContext.Implicits.global
import scala.util.Random


/**
  * This is the interpreter for java snippets. It is used to check whether a snippet is correct or not. It returns a
  * boolean indicating whether the program is correct or not, a Long representing the runtime and a String containing
  * any error message if the program is not correct.
  */
object JavaInterpreter extends DraftInterpreter {
  var incorrect_count = 0

  def set_incorrect_count(c : Int) : Unit = this.incorrect_count = c

  val logger = Logger(this.getClass)
  logger.info("Creating Java Interpreter...")

  def test(prog : String) : (Boolean, Long, String) = {
    this.logger.info("Running ==============\n" + prog)

    // create a temporary file for the program
    val tmp_file = java.io.File.createTempFile("java-" + Random.nextInt(), ".java")
    this.logger.debug("Temporary file: " + tmp_file.getAbsolutePath)
    val writer = new FileWriter(tmp_file)
    writer.write(prog)
    writer.flush()
    writer.close()

    val cmd = Process(List("src/main/resources/ng", "bsh.Interpreter", tmp_file.getAbsolutePath), None)
    val stderr = new StringBuilder
    val stdout = new StringBuilder
    val proc_logger = ProcessLogger(out => stdout.append(out + "\n"), err => stderr.append(err))

    this.logger.debug("Testing..")
    val p = cmd.run(proc_logger)

    val runtime =
    try {
      Utils.time(Await.result(Future(p.exitValue()), AppConfig.InterpreterConfig.time_limit millis))._1
    } catch {
      case e : TimeoutException =>
        p.destroy()
        this.logger.debug("Exception thrown during testing:\n" + e.getMessage)
        tmp_file.delete()
        return (false, -1L, e.getMessage)
    }

    val output = stdout.toString()
    val err = stderr.toString()
    this.logger.info("OUTPUT =======\n" + output)
    this.logger.info("ERROR =======\n" + err)

    val passed = output.trim == "true"
    if(!passed) {
      incorrect_count += 1
    }

    this.logger.info("Test result: " + passed)
    this.logger.info("Time Elapsed: " + runtime)
    tmp_file.delete()
    (passed, runtime, output)
  }

  /**
    * This is used to test whether the given code snippet is correct under the given input test
    */
  override def test(test_case : TestCase, code : String) : (Boolean, Long, String) = {
    val jtest_case = test_case.asInstanceOf[JavaTestCase]
    val test_func = jtest_case.test.replace("\\n", "\n")
    this.logger.debug("Code ===========\n" + code)
    this.logger.debug("Test ===========\n" + test_func)
    val prog = test_func.replace(JavaUtils.STMT_PLACEHOLDER, code)
    this.test(prog)
  }

  /**
    * This is used to test whether the given code snippet is correct under the given input test
    */
  override def test(test_case: TestCase, code: Draft): (Boolean, Long, String) = {
    assume(assumption = false, "Not supported.")
    (false, -1L, "")
  }
}
