package edu.rice.pliny.language.java

import java.io.{ByteArrayOutputStream, File, PrintStream}
import java.nio.charset.StandardCharsets

import bsh.{Interpreter, NameSpace}
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.draft.{Draft, DraftInterpreter, TestCase}
import edu.rice.pliny.AppConfig


/**
  * This is the interpreter for java snippets. It is used to check whether a snippet is correct or not. It returns a
  * boolean indicating whether the program is correct or not, a Long representing the runtime and a String containing
  * any error message if the program is not correct.
  */
class JavaFastInterpreter(val jar_paths : Seq[String]) extends DraftInterpreter {
  var incorrect_count = 0

  def set_incorrect_count(c : Int) : Unit = this.incorrect_count = c

  val logger = Logger(this.getClass)
  logger.info("Creating Java Interpreter...")
  val inter = new Interpreter
  val namespace : NameSpace = inter.getNameSpace
  this.jar_paths.foreach(s => this.inter.getClassManager.addClassPath(new File(s).toURI.toURL))

  def test(prog : String) : (Boolean, Long, String) = {
    this.logger.debug("Running ==============\n" + prog)

    // val new_namespace = this.namespace.clone().asInstanceOf[NameSpace]
    val thread = new Thread(inter)

    // setup uncaught exceptions
    thread.setUncaughtExceptionHandler((t: Thread, e: Throwable) => {
      return (false, -1, e.getMessage)
    })
    this.namespace.clear()

    // output streams
    val out_stream = new ByteArrayOutputStream
    val error_stream = new ByteArrayOutputStream
    inter.prepare_run(prog, this.namespace, new PrintStream(out_stream, true, "utf-8"), new PrintStream(error_stream, true, "utf-8"))

    // run the code in a separate thread
    val t0 = System.currentTimeMillis
    thread.start()
    thread.synchronized {
      thread.wait(AppConfig.InterpreterConfig.time_limit)
    }
    while(thread.isAlive) {
      inter.set_abort(true)
      thread.interrupt()
      this.logger.debug("Aborted.")
    }
    assert(!thread.isAlive)
    val t1 = System.currentTimeMillis

    // Get outputs
    val output = "Output ===============\n" +
      new String(out_stream.toByteArray, StandardCharsets.UTF_8) + "\n" +
      "Error ==================\n" +
      new String(error_stream.toByteArray, StandardCharsets.UTF_8) + "\n"

    val result = inter.get("_result_").asInstanceOf[Boolean]
    val runtime = if(result) t1 - t0 else -1
    this.logger.debug("Result: {} in {} ms", result, runtime)

    // create a temporary file for the program
    (result, runtime, output)
  }

  /**
    * This is used to test whether the given code snippet is correct under the given input test.
    */
  override def test(test_case : TestCase, code : String) : (Boolean, Long, String) = {
    val jtest_case = test_case.asInstanceOf[JavaTestCase]
    val test_func = jtest_case.test.replace("\\n", "\n")

    this.logger.debug("Code ===========\n" + code)
    this.logger.debug("Test ===========\n" + test_func)
    val prog = test_func.replace(JavaUtils.STMT_PLACEHOLDER, code)
    this.test(prog)
  }

  /**
    * This is used to test whether the given code snippet is correct under the given input test
    */
  override def test(test_case: TestCase, code: Draft): (Boolean, Long, String) = {
    assume(assumption = false, "Not supported.")
    (false, -1L, "")
  }
}
