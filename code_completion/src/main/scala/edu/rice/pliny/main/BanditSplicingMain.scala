package edu.rice.pliny.main

import java.io.File

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.{AppConfig, Utils}
import edu.rice.pliny.language.java.{JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.mcts.evaluator.{CombinedEvaluator, SyntaxDistEvaluator, TestCaseEvaluator}
import edu.rice.pliny.mcts.splicing.JavaSplicingTree

import scala.collection.mutable

object BanditSplicingMain {
  val logger = Logger(this.getClass)

  val DRAFT_FILENAME = "draft.java"
  val REF_DIRNAME = "training"
  val BENCHMARK_PATH_PREFIX = "src/main/resources/benchmarks"

  /**
    * Test whether we have a solution for this sketch
    */
  def complete_sketch(dirname : String, jar_names : Seq[String] = Seq()) : Boolean = {
    val jar_paths = jar_names.map(n => dirname + "/" + n)

    // get a list of reference programs
    val ref_dir = new File(dirname + "/" + REF_DIRNAME)
    val ref_names : Seq[String] =
      if(ref_dir.exists() && ref_dir.isDirectory) {
        ref_dir.listFiles().filter(_.isFile).map(f => f.getPath)
      } else {
        Seq.empty[String]
      }
    val refs = ref_names.map(ref_src => JavaUtils.parse_file(ref_src, None, None, None, jar_paths, Seq()).get)

    val draft = JavaUtils.parse_file(dirname + "/" + DRAFT_FILENAME, None, None, None, jar_paths, Seq()).get
    val interpreter = new JavaFastInterpreter(jar_paths)
    val ref_evaluator = new SyntaxDistEvaluator(refs.head)
    val test_evaluator = new TestCaseEvaluator(TestCaseEvaluator.MAX_SIM)
    val combined_eval = new CombinedEvaluator(Seq(ref_evaluator, test_evaluator), Seq(0.8, 0.2))
    val task = new BanditSearch(ref_evaluator)
    val tree = new JavaSplicingTree(draft, None, interpreter, relevant_progs = refs)
    val (runtime, solution) = Utils.time{task.complete(tree, 500)}
    solution match {
      case Solution(root, leaf) =>
        // val view = new TreeView(new_tree)
        // view.showTree("foo")
        this.logger.info("Solution ==============\n{}\n", leaf.toString)
        this.logger.info("Runtime: {} ms\n", runtime)
        true
      case NoSolution() =>
        this.logger.info("No Solution.\n")
        false
    }
  }

  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    // assert(complete_sketch(this.BENCHMARK_PATH_PREFIX + "/testio"))
    // assert(complete_sketch(this.BENCHMARK_PATH_PREFIX + "/html2", Seq("jsoup-1.10.2.jar")))
    assert(complete_sketch(this.BENCHMARK_PATH_PREFIX + "/binsearch2", Seq()))
  }
}
