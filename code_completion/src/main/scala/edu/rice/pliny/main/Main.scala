import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java.{JavaDraft, JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.AppConfig
import edu.rice.pliny.mcts.bayou.{BayouSketchUtils, JavaSketchTree}
import edu.rice.pliny.mcts.evaluator.DefUseRatioEvaluator

object Main extends App {
  val logger = Logger(this.getClass)
  /*
  AppConfig.log_config
  val draft = JavaUtils.parse_file("src/test/resources/simple_expr_hole3.java", None, None, Seq(), Seq()).get
  val binsearch = JavaUtils.parse_file("src/test/resources/mcts/binsearch_bad.java", None, None, Seq(), Seq()).get
  val hole = draft.get_stmt_hole_with_tests
  val evaluator = new RandomFillEvaluator
  val evaluator2 = new RefProgEvaluator(binsearch)
  val task = new MCTSTask(evaluator)
  val tree = new DraftTree(draft, None)
  val int_tree = new IntTree(0, None)
  // TreeVisualizer.visualize(tree)
  val (new_tree, solution) = task.complete(int_tree, 20)
  println("Solution ==============")
  println(solution.get)
  TreeVisualizer.visualize(new_tree)
  */

  /*
  AppConfig.log_config
  val tree = new IntTree(0, None)
  val evaluator = new IntDistEvaluator(tree.SOLUTION_NUM)
  val task = new MCTSTask(evaluator)
  // TreeVisualizer.visualize(tree)
  val solution = task.complete(tree, 300)

  // TreeVisualizer.visualize(new_tree)
  solution match {
    case Solution(root, leaf) =>
      val view = new TreeView(root)
      view.showTree("foo")
      println("Solution ==============")
      println(leaf)
    case NoSolution() => println("No Solution.")
  }
  val draft = JavaUtils.parse_file("foo.javadraft", None, None, Seq(), Seq()).get
  draft.infer_type
  System.exit(0)
  */

  val sketch_file = "src/test/resources/mcts/bayou/synthesizer/TestIO1.json"
  val draft_file = "src/test/resources/mcts/bayou/synthesizer/TestIO1.java"
  val ref_srcs = Seq("src/test/resources/mcts/bayou/read.java")
  val jar_paths = Seq()
  val refs = ref_srcs.map(ref_src => JavaUtils.parse_file(ref_src, None, None, None, jar_paths, Seq()).get)

  val sketch = BayouSketchUtils.parse_sketch(sketch_file, draft_file, jar_paths = jar_paths)
  val result = sketch.slice(0, sketch.size)
    .map(s => {
      logger.info("Synthesizing ================\n{}\n", s.compilation_unit.toString)
      val interpreter = new JavaFastInterpreter(jar_paths)
      val tree = new JavaSketchTree(s, None, interpreter, relevant_progs = refs)
      val evaluator = new DefUseRatioEvaluator
      val task = new BanditSearch(evaluator)
      val solution = task.complete(tree, 100)
      solution match {
        case NoSolution() =>
          logger.info("NO SOLUTION.")
          false
        case Solution(root, leaf) =>
          logger.info("Solution ===============\n{}\n", leaf.toString)
          true
      }
    })
  logger.info("SUCCESS COUNT: {}/{}", result.count(r => r), sketch.size)
  result.contains(true)
}

