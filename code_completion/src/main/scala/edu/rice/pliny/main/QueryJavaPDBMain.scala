package edu.rice.pliny.main

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.feature.source.{ClassSourceFeature, CommentFeature, NamesFeature, WeightedNLTermsFeature}
import edu.rice.pliny.language.java.{JavaCodeDB, JavaDraft, JavaUtils}


object QueryJavaPDBMain {
  val logger = Logger(this.getClass)
  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    if(args.length < 1) {
      System.err.println(
        """
          |Usage: [input filename] (method name)
          |
          |Query the PDB using the input method if method name is specified. If no method name is
          |specified, we use the first method, assuming there is only one class defined in the class.
          |
          |We use the draft's comment to query PDB if there's method-level comment. Otherwise, we use
          |the comment from statement holes. If no comment at all, we stop querying.
        """.stripMargin)
      sys.exit(1)
    }
    val pathname = args(0)
    val method_name = if(args.length > 1) Some(args(1)) else None

    val draft =
      if(method_name.isDefined) JavaUtils.parse_file(pathname, None, method_name, None, Seq(), Seq())
      else JavaUtils.parse_file(pathname, None, None, None, Seq(), Seq())

    if(draft.isEmpty) {
      logger.error("Failed to parse the input program.")
      return
    }

    val comment : Option[String] =
      if(draft.get.get_comment.isDefined) {
        Some(draft.get.get_comment.get)
      } else {
        val stmt_hole = draft.get.get_stmt_hole_with_tests
        if(stmt_hole.isDefined) stmt_hole.get.get_comments else None
      }

    if(comment.isEmpty) {
      logger.error("We don't have any comment.")
      return
    }

    logger.info("Using comment:\n{}", comment.get)

    val result = JavaCodeDB.query(draft.get, comment.get)
    println(result.size + " programs.")
    result.sortBy(_._2).foreach(r => {
      println("================ Score: " + r._2.toString + "\n" + r._1.toString)
      val features = r._3.features
      val iter = features.iterator()
      while(iter.hasNext) {
        val f = iter.next()
        f match {
          case name : NamesFeature =>
            println("Names Feature: " + name.toString)
          case nlterms : WeightedNLTermsFeature =>
            println("Weighted NL Terms:")
            println(nlterms.toString)
          case csrc : ClassSourceFeature =>
            // println("Class Source:")
            // println(csrc.class_src)
          case comments : CommentFeature =>
            println("Comments:")
            for(i <- 0 until comments.comments.size()) {
              val content = comments.comments.get(i)
              // println("Location: (" + content.start.line + ", " + content.start.column + ") - (" + content.end.line + ", " + content.end.column + ")")
              println("---------------\n" + content.comment + "\n")
            }
        }
      }
    })
  }

}
