package edu.rice.pliny.main

import java.io.{File, FileWriter}

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}

import scala.concurrent.duration._
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.splicing.HoleTask
import edu.rice.pliny.Utils.{ActorMsg, CompletionSolution}
import edu.rice.pliny.draft.Draft
import edu.rice.pliny.language.java._
import spray.json.{JsArray, JsNumber, JsObject, JsString}

import scala.collection.mutable
import scala.concurrent.Await
import scala.util.Random

class MainActor(interpreter : JavaFastInterpreter) extends Actor {
  val logger = Logger(this.getClass)
  val solutions : mutable.ArrayBuffer[CompletionSolution] = mutable.ArrayBuffer.empty[CompletionSolution]

  val task_actor : ActorRef =
    this.context.actorOf(Props(new HoleTask(interpreter, JavaCodeDB, JavaDraftIteratorFactory, Some(AppConfig.CodeCompletionConfig.solution_num))),
      name = "HoleTaskActor-" + Random.nextInt.toString)

  override def receive: Receive = {
    case ActorMsg.StartHoleTask(draft) =>
      task_actor ! ActorMsg.StartHoleTask(draft)
    case ActorMsg.HoleTaskSolution(c) =>
      logger.info("Received a completed program solution.")
      this.solutions += c
      if(this.solutions.size >= AppConfig.CodeCompletionConfig.solution_num) {
        logger.info("We have collected enough solutions.")

        logger.info("Stopping actors...")
        this.task_actor ! ActorMsg.StopHoleTask
      }
    case ActorMsg.StopHoleTask =>
      logger.info("Shutting down actor system...")
      this.context.children.foreach(c => this.context.stop(c))
      this.context.stop(self)
      logger.info("Done shutting down actor system...")
    case ActorMsg.RequestAllSolutions =>
      sender ! ActorMsg.ResponseAllSolutions(this.solutions)
  }
}


object CodeCompletionMain {
  val logger = Logger(this.getClass)
  val config : Config = ConfigFactory.load()
    .withValue("akka.loglevel", ConfigValueFactory.fromAnyRef("OFF"))
    .withValue("akka.stdout-loglevel", ConfigValueFactory.fromAnyRef("OFF"))
  implicit val system = ActorSystem("CodeCompletionSystem", config)

  /**
    * Main function for completing a draft. Return a list of completion solutions, the runtime and the number of
    * incorrect solutions
    */
  def complete(draft : Draft, jar_paths : Seq[String] = Seq()) : (mutable.ArrayBuffer[CompletionSolution], Long, Int) = {
    val interpreter = new JavaFastInterpreter(jar_paths)
    interpreter.set_incorrect_count(0)

    // fire up the main actor
    val main_actor = this.system.actorOf(Props(new MainActor(interpreter)), name = "main_actor-" + Random.nextInt().toString)
    logger.info("Start completing the draft program.")
    main_actor ! ActorMsg.StartHoleTask(draft)

    // keep looping until we have solution or timeout
    val start_time = System.currentTimeMillis()
    var end_time = System.currentTimeMillis()
    var current_solution : mutable.ArrayBuffer[CompletionSolution] = null
    logger.debug("Synthesis time limit: " + AppConfig.CodeCompletionConfig.time_limit * 1000 + " ms")

    while(end_time - start_time < AppConfig.CodeCompletionConfig.time_limit * 1000  &&
         (current_solution == null || current_solution.size < AppConfig.CodeCompletionConfig.solution_num)) {
      Thread.sleep(1000)
      val duration = end_time - start_time

      implicit val timeout = Timeout(1 seconds)
      val response = Await.result(main_actor ? ActorMsg.RequestAllSolutions, timeout.duration)
        .asInstanceOf[ActorMsg.ResponseAllSolutions]
      current_solution = response.solutions
      logger.info("Waited " + duration + " ms. " + current_solution.size + " solutions so far.")
      end_time = System.currentTimeMillis()
    }

    val incorrect_count = interpreter.incorrect_count
    val runtime = System.currentTimeMillis() - start_time

    logger.info("Incorrect solution : " + incorrect_count)
    logger.info("Runtime            : " + runtime)

    if(current_solution.size < AppConfig.CodeCompletionConfig.solution_num) {
      logger.warn("Timeout.")
    }

    // stop the worker
    main_actor ! ActorMsg.StopHoleTask


    (current_solution, runtime, incorrect_count)
  }

  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    if(args.length < 1) {
      System.err.println(
      """Usage: [draft filename] (class name) (method name) (jar path) (dir path)

         This program takes a filename to an incomplete Java program which
         contains one or more holes and test cases. It will produce one or
         more complete Java program which satisfies the given test cases.
      """)
      logger.error("Must provide a draft as the first input argument")
      system.terminate()
      println(JsObject("solutions" -> JsArray()).toString())
      return
    }

    val class_name = if(args.length >= 2) Some(args(1)) else None
    val method_name = if(args.length >= 3) Some(args(2)) else None
    val jar_paths = if(args.length >= 4) Seq(args(3)) else Seq()
    val dir_paths = if(args.length >= 5) Seq(args(4)) else Seq()

    val draft = JavaUtils.parse_file(args(0), class_name, method_name, None, jar_paths, dir_paths)
    val output_json =
      if(draft.isEmpty) {
        logger.error("Failed to parse the code.")
        JsObject("solutions" -> JsArray(),
                 "runtime" -> JsNumber(0),
                 "incorrect_count" -> JsNumber(0))
      } else {
        val completion = this.complete(draft.get, jar_paths)
        completion._1.zipWithIndex.foreach(c => {
          val filename = s"solution_${c._2}.javadraft"
          logger.info("Writing solution to " + filename)
          val writer = new FileWriter(new File(filename))
          writer.write(c._1.draft.asInstanceOf[JavaDraft].toStringWithClass)
          writer.close()
        })
        JsObject("solutions" -> JsArray(completion._1.map(s => JsString(s.draft.toString)).toVector),
                 "runtime" -> JsNumber(completion._2),
                 "incorrect_count" -> JsNumber(completion._3))
      }

    if(AppConfig.CodeCompletionConfig.output == "text") {
      val all_solutions = output_json.getFields("solutions").head.asInstanceOf[JsArray].elements
      all_solutions.foreach(s => {
        logger.info("Solution =========\n" + s.asInstanceOf[JsString].value)
        println("Solution =========\n" + s.asInstanceOf[JsString].value)
      })
      println("Incorrect Count : " + output_json.getFields("incorrect_count"))
      println("Runtime         : " + output_json.getFields("runtime"))
    } else {
      logger.info("Result JSON =========\n" + output_json.toString)
      println(output_json.toString)
    }
    system.terminate()
  }
}
