package edu.rice.pliny.main

import com.martiansoftware.nailgun.NGServer
import com.typesafe.scalalogging.Logger

object JavaInterpreterServer{
  val logger = Logger(this.getClass)

  def main(args : Array[String]) : Unit = {
    logger.info("Starting NailGun Server")
    val ngserver = new NGServer()
    ngserver.run()
  }
}
