package edu.rice.pliny.main

import edu.rice.pliny.language.java.JavaUtils
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.ServletHandler
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}

import spray.json._
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig

object CodeCompletionServer {

  class CodeCompletionServlet extends HttpServlet {

    val logger = Logger(this.getClass)

    override def doGet(request : HttpServletRequest, response : HttpServletResponse) : Unit = {
      response.addHeader("Access-Control-Allow-Origin", "*")
      response.addHeader("Access-Control-Allow-Methods", "*")
      response.getWriter.println("GET resquest is only used for testing for code completion")
    }

    override def doPost(request : HttpServletRequest, response : HttpServletResponse) : Unit = {
      response.addHeader("Access-Control-Allow-Origin", "*")
      response.addHeader("Access-Control-Allow-Methods", "*")

      val body = {
        val builder = new StringBuilder
        val reader = request.getReader
        var line = reader.readLine()
        while(line != null) {
          builder.append(line).append("\n")
          line = reader.readLine()
        }
        reader.close()
        builder.toString()
      }

      val json_data = body.parseJson
      val code = json_data.asJsObject.getFields("code") match {
        case Seq(JsString(c)) => c
      }

      logger.info("Code:")
      logger.info(code)

      response.setContentType("application/json")
      response.setStatus(HttpServletResponse.SC_OK)

      // invalid input data
      if(code == null) {
        logger.info("Invalid input data.")
        val json = JsObject(
          "code" -> JsString("Invalid input data."),
          "succeeded" -> JsBoolean(false)
        )
        response.getWriter.println(json.toString())
      } else {
        val draft = JavaUtils.parse_src(code, None, None, None, Seq(), Seq())
        if(draft.isEmpty) {
          logger.info("Invalid code.")
          val json = JsObject(
            "code" -> JsString("Failed to parse the program."),
            "succeeded" -> JsBoolean(false)
          )
          response.getWriter.println(json.toString())
        } else {
          val completion = CodeCompletionMain.complete(draft.get)
          logger.info("Completion solution count: " + completion._1.size)
          val response_json =
            if(completion._1.nonEmpty) {
              JsObject(
                "code" -> JsString(completion._1.head.draft.toString),
                "succeeded" -> JsBoolean(true),
                "runtime" -> JsNumber(completion._2),
                "incorrect_count" -> JsNumber(completion._3)
              )
            } else {
              JsObject(
                "code" -> JsString("Failed to complete the program."),
                "succeeded" -> JsBoolean(false),
                "runtime" -> JsNumber(completion._2),
                "incorrect_count" -> JsNumber(completion._3)
              )
            }
          response.getWriter.println(response_json.toString)
        }
      }
    }
  }



  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    if(args.length < 1) {
      System.err.println("Must provide a port number for the server.")
      sys.exit(1)
    }

    val port = Integer.parseInt(args(0))
    val server = new Server(port)
    val handler = new ServletHandler()
    server.setHandler(handler)
    handler.addServletWithMapping(classOf[CodeCompletionServlet], "/completion")

    server.start()
    server.join()
  }
}
