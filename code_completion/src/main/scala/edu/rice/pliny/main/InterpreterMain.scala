package edu.rice.pliny.main

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java.JavaFastInterpreter

object InterpreterMain {
  val logger = Logger(this.getClass)

  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    if(args.length < 1) {
      System.err.println(
        """Usage: [filename] (jar_path)

         This program runs the provided program using the internal interpreter and prints the results.
      """)
      logger.error("Must provide a program filename as the first input argument")
      return
    }

    val code = scala.io.Source.fromFile(args(0)).mkString
    val jar_path =
      if(args.length > 1) {
        Seq(args(1))
      } else {
        Seq()
      }
    val inter = new JavaFastInterpreter(jar_path)
    this.logger.info("Running ====================\n{}\n", code)
    val result = inter.test(code)
    this.logger.info("Result: {}\n", result._1)
    this.logger.info("Runtime: {}\n", result._2)
    this.logger.info("Output ==============\n{}\n", result._3)
  }
}

