package edu.rice.pliny.main

import java.io.File

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.{AppConfig, Utils}
import edu.rice.pliny.language.java.{JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.mcts.evaluator.{CombinedEvaluator, DefUseRatioEvaluator, SyntaxDistEvaluator, TestCaseEvaluator}
import edu.rice.pliny.mcts.splicing.JavaSplicingTree
import edu.rice.pliny.mcts.vis.TreeVisualizer

import scala.util.Random

object BanditBenchmarkMain {
  val logger = Logger(this.getClass)

  val DRAFT_FILENAME = "draft.java"
  val BENCHMARK_PATH_PREFIX = "src/main/resources/benchmarks"

  /**
    * Test whether we have a solution for this sketch
    */
  def complete_sketch(dirname : String, jar_names : Seq[String] = Seq()) : Boolean = {
    val jar_paths = jar_names.map(n => dirname + "/" + n)

    // get a list of reference programs
    val ref_dir = new File(dirname + "/training")
    val ref_names : Seq[String] =
      if(ref_dir.exists() && ref_dir.isDirectory) {
        ref_dir.listFiles().filter(_.isFile).map(f => f.getPath)
      } else {
        Seq.empty[String]
      }
    val ref_progs = ref_names.map(ref_src => JavaUtils.parse_file(ref_src, None, None, None, jar_paths, Seq()).get)

    // combine the sketch and the draft
    val draft_prog = JavaUtils.parse_file(dirname + "/" + this.DRAFT_FILENAME, None, None, None, jar_paths = jar_paths, dir_paths = Seq()).get

    // val stmt_eval = new SyntaxDistEvaluator(ref_progs.head)
    val stmt_eval = {
      val evals = ref_progs.map(rp => {
        new SyntaxDistEvaluator(rp)
      })
      new CombinedEvaluator(evals, Seq.fill(evals.size)(1.0 / evals.size.toDouble))
    }
    val expr_eval = stmt_eval
    // val expr_eval = new SyntaxDistEvaluator(ref_progs.head)
    // val stmt_eval = new TestCaseEvaluator(TestCaseEvaluator.MAX_DEPTH)
    // val expr_eval = new TestCaseEvaluator(TestCaseEvaluator.MAX_DEPTH)
    // val expr_prior = new ExprDistPrior(ref_progs.head)
    val task = new JavaDraftSearch(stmt_eval, expr_eval)
    val tree = new JavaSplicingTree(draft_prog, None, new JavaFastInterpreter(Seq()), relevant_progs = ref_progs)
    val (runtime, solution) = Utils.time(task.complete(tree, 10000))
    TreeVisualizer.visualize(tree)
    solution match {
      case Solution(root, leaf) =>
        // val view = new TreeView(new_tree)
        // view.showTree("foo")
        this.logger.info("Solution ==============")
        this.logger.info(leaf.toString)
        this.logger.info("Runtime: {} ms", runtime)
        true
      case NoSolution() =>
        // println("No Solution.")
        false
    }
  }

  def main(args : Array[String]) : Unit = {
    Random.setSeed(System.currentTimeMillis)
    AppConfig.log_config
    // assert(complete_sketch(this.BENCHMARK_PATH_PREFIX + "/testio"))
    // assert(complete_sketch(this.BENCHMARK_PATH_PREFIX + "/html", Seq("jsoup-1.10.2.jar")))
    // assert(complete_sketch(this.BENCHMARK_PATH_PREFIX + "/binsearch2", Seq()))
    // assert(complete_sketch(this.BENCHMARK_PATH_PREFIX + "/lcs", Seq()))
    assert(complete_sketch(this.BENCHMARK_PATH_PREFIX + "/csv2", Seq()))
  }
}
