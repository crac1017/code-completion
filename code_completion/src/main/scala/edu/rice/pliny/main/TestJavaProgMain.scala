package edu.rice.pliny.main

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java._

object TestJavaProgMain {
  val logger = Logger(this.getClass)
  def main(args : Array[String]) : Unit = {
    AppConfig.log_config
    if(args.length < 2) {
      System.err.println(
      """Usage: [filename] [method_name] (jar_paths)

         This program takes a filename to a Java program which
         contains a test and the name of the method. It will test whether the method
         is correct according to the tests provided.
      """)
      logger.error("Must provide a draft as the first input argument")
      return
    }

    val jar_paths = args.takeRight(args.length - 2)
    jar_paths.foreach(jp => logger.info("Jar: {}", jp))

    val draft = JavaUtils.parse_file(args(0), None, Some(args(1)), None, jar_paths, Seq())
    if(draft.isEmpty) {
      logger.error("Cannot parse the java file or cannot find the method.")
      println("Cannot parse the java file or cannot find the method.")
      return
    }
    val test_case = draft.get.get_test
    if(test_case.isEmpty) {
      logger.error("Cannot find any tests for the method.")
      println("Cannot find any tests for the method.")
      return
    }

    logger.info("Testing program ===============\n" + draft.get.toString)

    logger.info("Tests ======================\n" + test_case.get.asInstanceOf[JavaTestCase].test)

    val interpreter = new JavaFastInterpreter(jar_paths)
    val result = interpreter.test(test_case.get, draft.get.toString)

    if(result._1) {
      val msg = "This program is correct. Total run time is " + result._2.toString + " ms."
      logger.info(msg)
    } else {
      val msg = "This program is not correct. Here is the error message:\n" + result._3
      logger.info(msg)
    }
  }
}
