// run the code in the editor
function run() {
    var code = editor.getValue();
    console.log("Running code ======");
    console.log(code);

    $.ajax({
        type : 'POST',
        url : '/run',
        data: JSON.stringify({code : code}),
        contentType: 'application/json',
        success: function(data) {
            $('#status').html('Successful run.');
            $('#console-stdout').html(data.stdout);
            $('#console-stderr').html(data.stderr);
        },
        error: function(xhr, type) {
            $('#status').html('Run error.');
        }
    });
}

function complete() {
    var code = editor.getValue();
    console.log("Completing code ======");
    console.log(code);

    $.ajax({
        type : 'POST',
        url : '/complete',
        timeout: 60000,
        data: JSON.stringify({code : code}),
        contentType: 'application/json',
        success: function(data) {
            $('#status').html('Completion successful.');
            output_editor.setValue(data.code);
        },
        error: function(xhr, type) {
            $('#status').html('Failed to complete.');
        }
    });

    $('#status').html('Completing draft code.');
}

// load the example stored in the data folder
function load_example(name) {
    var filename = '/data/' + name + '.java';
    $('#console-stdout').load(filename, function(data, status) {
        editor.setValue(data);
        $('#console-stdout').html("Loaded " + name + " example.");
    });
}

// init function
function init() {
    editor = ace.edit("editor");
    editor.setTheme("ace/theme/chrome");
    editor.getSession().setMode("ace/mode/java");
    editor.resize();
    editor.setFontSize(12);
    editor.setValue("// Please write your code here.");
    editor.$blockScrolling = Infinity;

    output_editor = ace.edit("output-editor");
    output_editor.setTheme("ace/theme/chrome");
    output_editor.getSession().setMode("ace/mode/java");
    output_editor.resize();
    output_editor.setReadOnly(true);
    output_editor.setFontSize(12);
    output_editor.setValue("// output goes here.");
    output_editor.$blockScrolling = Infinity;
}

$(document).ready(function() {
    init();
});
