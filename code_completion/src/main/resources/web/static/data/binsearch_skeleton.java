public class Binsearch {

  /**
   * TODO 1.
   * Complete the binary search algorithm. Return -1 if x does not
   * exist in the array.
   */
  static public int binsearch(int[] array, int x) {
    return -1;
  }

  
  /**
   * TODO 2:
   * Test the binary search you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * array: [1, 2, 3, 4, 5], x: 1
   * array: [1, 2, 3, 4, 5], x: 3
   * array: [1, 2, 3, 4, 5], x: 5
   * array: [1, 10], x: 10
   * array: [1, 10], x: 1
   * array: [10], x: 10
   * array: [10], x: 100
   *
   * Return true if the binary search is correct. Otherwise, return false.
   */
  static public boolean test() {
    return false;
  }

  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
