public class Sieve {
    static public boolean sieve(int n) {
        if(n == 2) {
            return true;
        }
        return false;
    }

    static public void main(String[] args) {
        int[] input = {1,     2,    3,    4,     5,    6,     7,    8,     9,     10,    27,    29,   83};
        boolean[] eo ={false, true, true, false, true, false, true, false, false, false, false, true, false};

        for(int i = 0; i < input.length; ++i) {
            boolean out = sieve(input[i]);
            System.out.println("-----------------------");
            System.out.println("Input:    " + input[i]);
            System.out.println("Expected: " + eo[i]);
            System.out.println("Output:   " + out);
            if(out != eo[i]) {
                error(out + " does not equal to " + eo[i]);
                exit();
            }
        }
        System.out.println("PASS!");
    }
}
