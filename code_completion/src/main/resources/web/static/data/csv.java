import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {
    static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
        int n = 3;
        int[][] mat1 = new int[n][n];
        int[][] mat2 = new int[n][n];

        {
            File f = new File(filename);
            Scanner scanner = new Scanner(f);

            for(int i = 0; i < n; ++i) {
                String line = scanner.nextLine();
                String[] fields = line.split(",");
                for(int j = 0; j < n; ++j) {
                    mat1[i][j] = Integer.parseInt(fields[j]);
                }
            }
        }

        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                int s = 0;
                for(int k = 0; k < n; ++k) {
                    s += mat1[i][k] * mat1[k][j];
                }
                mat2[i][j] = s;
            }
        }

        return mat2;
    }

    static public void main(String[] args) throws FileNotFoundException {
        int[][] result = csv_mat_mul("static/data/mat1.csv");

        int n = 3;
        int[][] eo = new int[n][n];
        {
            File f = new File("static/data/mat2.csv");
            Scanner scanner = new Scanner(f);

            for(int i = 0; i < n; ++i) {
                String line = scanner.nextLine();
                String[] fields = line.split(",");
                for(int j = 0; j < n; ++j) {
                    eo[i][j] = Integer.parseInt(fields[j]);
                }
            }
        }

        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < n; ++j) {
                System.out.println("---------------------");
                System.out.println("result[" + i + "][" + j + "] = " + result[i][j]);
                System.out.println("eo    [" + i + "][" + j + "] = " + eo[i][j]);
                if(result[i][j] != eo[i][j]) {
                  exit();
                }
            }
        }
        print("PASS!");
    }
}
