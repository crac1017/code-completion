import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {

  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2. The matrix in the CSV looks like this:
   * 1,2,3
   * 2,3,4
   * 3,4,5
   */
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    int[][] mat1 = new int[n][n];
    int[][] mat2 = new int[n][n];

    // read a 3x3 matrix

    // square mat1 and store the results into mat2
    
    return mat2;
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix.
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat1) {
    return false;
  }

  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
