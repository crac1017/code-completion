import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class HTTP {
  /**
   * TODO 1:
   * Setup an HTTP server listening to a given port which returns the
   * content of a given file.
   */
  public static HttpServer http(String filename, int port) throws IOException {
    String content;
    HttpServer server;

    // we have a handler written for you
    HttpHandler handler = new HttpHandler() {
      public void handle(HttpExchange he) throws IOException {
        he.sendResponseHeaders(200, content.length());
        OutputStream os = he.getResponseBody();
        os.write(content.getBytes());
        os.close();
      }
    };
    
    return server;
  }
  /**
   * TODO 2:
   * Query the http server and check the content of the response. You
   * could use URL and URLConnection class to query the server.
   */
  static public boolean test(int port) {
    String address = "http://localhost:" + port + "/";
    return false;
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    int port = 9999;
    String filename = "static/data/mat1.csv";
    HttpServer server = http(filename, port);

    if(test(port)) {
      print("PASS!");
    } else {
      pirnt("FAIL!");
    }

    server.stop(1);
  }
}
