import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class HTTP {
    public static HttpServer http(String filename, int port) throws IOException {
        String content;

        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");
        while((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        content = stringBuilder.toString().trim();

        HttpServer server;
        HttpHandler handler = new HttpHandler() {
            public void handle(HttpExchange he) throws IOException {
                he.sendResponseHeaders(200, content.length());
                OutputStream os = he.getResponseBody();
                os.write(content.getBytes());
                os.close();
            }
        };

        server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/" , handler);
        server.setExecutor(null); // creates a default executor
        server.start();
        return server;
    }

    static public void main(String[] args) throws IOException {
        int port = 9999;
        String filename = "static/data/mat1.csv";

        HttpServer server = http(filename, port);

        // get URL content
        String a= "http://localhost:" + port + "/";
        URL url = new URL(a);
        URLConnection conn = url.openConnection();

        // open the stream and put it into BufferedReader
        BufferedReader br = new BufferedReader(
            new InputStreamReader(conn.getInputStream()));

        String inputLine;
        int line = 0;
        while ((inputLine = br.readLine()) != null) {
            String[] fields = inputLine.split(",");
            for(int i = 0; i < 3; ++i) {
                int eo = i + line + 1;
                int output = Integer.parseInt(fields[i]);
                System.out.println("---------------------");
                System.out.println("result[" + line + "][" + i + "] = " + output);
                System.out.println("eo    [" + line + "][" + i + "] = " + eo);
                if(output != eo) { exit(); }
            }
            line += 1;
        }
        server.stop(1);
        br.close();
        if(line == 3) { print("PASS!"); }
    }
}

