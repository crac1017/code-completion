public class Sieve {

  /**
   * TODO 1: Complete the following function. You could use our system
   * by replacing the contents in COMMENT and TEST.
   *
   * COMMENT:
   * Replace this comment with something related to sieve of
   * eratosthenes algorithm. It can be something like 
   * "sieve of eratosthenes algorithm for testing primality"
   *
   * TEST:
   * // make this tests stronger
   * __pliny_solution__
   * print(sieve(1) == false);
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    ??
      
    return primes[n];
  }

  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    return false;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
