import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class HTTP {
  /**
   * TODO 1:
   * Setup an HTTP server listening to a given port which returns the
   * content of a given file.
   *
   * COMMENT:
   * Create an HTTP server which serves the content of a local file
   *
   * TEST:
   * import com.sun.net.httpserver.HttpExchange;
   * import com.sun.net.httpserver.HttpHandler;
   * import com.sun.net.httpserver.HttpServer;
   * import java.io.OutputStream;
   * // Create an http server using the following function listening
   * // to a random port from 20000-55000
   * HttpServer server = null;
   * __pliny_solution__
   *
   * // the following code checks whether you have created a
   * // server. Don't change it.
   * source(new URL("http://localhost:" + Integer.toString(p) + "/"));
   * server.stop(0);
   */
  public static HttpServer http(String filename, int port) throws IOException {
    String content;

    /**
     * COMMENT:
     * Read a text file
     *
     * TEST:
     * String content;
     * String filename = "static/data/http_test.txt";
     * __pliny_solution__
     * print(content.trim().equals("print(true);"));
     */
    ??

    HttpServer server;

    return server;
  }

  /**
   * TODO 2:
   * Query the http server and check the content of the response. You
   * could use URL and URLConnection class to query the server.
   */
  static public boolean test(int port) {
    String address = "http://localhost:" + port + "/";
    return false;
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    int port = 9999;
    String filename = "static/data/mat1.csv";
    HttpServer server = http(filename, port);

    if(test(port)) {
      print("PASS!");
    } else {
      pirnt("FAIL!");
    }

    server.stop(1);
  }
}
