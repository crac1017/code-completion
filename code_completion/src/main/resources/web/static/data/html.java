import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
    public static List get_links(String html_file, String website) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(html_file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");
        while((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        String content = stringBuilder.toString();
        List result = new ArrayList();

        Document doc = Jsoup.parse(content);
        Elements links = doc.select("a[href]");

        Pattern pattern = Pattern.compile(website);

        for(Element link : links) {
            String l = link.attr("href");
            Matcher m = pattern.matcher(l);
            if(m.find()) {
                result.add(l);
            }
        }
        return result;
    }

    static public void main(String[] args) throws IOException {
        addClassPath("static/data/jsoup-1.10.2.jar");
        List links = get_links("static/data/jsoup.html", "jsoup");
        String[] eo = {
            "//try.jsoup.org/",
            "/apidocs/org/jsoup/select/Elements.html#html--",
            "/apidocs/index.html?org/jsoup/select/Elements.html",
            "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g",
            "http://github.com/jhy/jsoup/"
        };

        for(int i = 0; i < eo.length; ++i) {
            String result = (String) links.get(i);
            System.out.println("-----------------------");
            System.out.println("Expected: " + eo[i]);
            System.out.println("Output:   " + result);
            if(!eo[i].equals(result)) {
                exit();
            }
        }
        print("PASS!");
    }
}

