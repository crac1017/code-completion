import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {
  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2
   */
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    int[][] mat1 = new int[n][n];
    int[][] mat2 = new int[n][n];

    // read a 3x3 matrix
    /**
     * COMMENT:
     * Reading a matrix from a CSV file
     *
     * TEST:
     * // make the following test stronger
     * import java.io.File;
     * import java.util.*;
     * integer n = 3;
     * integer[][] mat1 = new integer[n][n];
     * __pliny_solution__
     * print(mat1[0][0] == 1);
     */
    ??

    // square mat1 and store the results into mat2
    /**
     * COMMENT:
     * Squaring a matrix
     *
     * TEST:
     * // make the following test stronger
     * integer n = 3;
     * integer[][] mat1 = new integer[n][n];
     * integer[][] mat2 = new integer[n][n];
     * for(integer i = 0; i < n; ++i) {
     *   for(integer j = 0; j < n; ++j) {
     *     mat1[i][j] = i + j + 1;
     *   }
     * }
     * __pliny_solution__
     * print(mat2[0][0] == 14);
     */
    ??
    
    return mat2;
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix. 
   *
   * Return true if the code is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat1) {
    return false;
  }

  /**
   * Do not change this function
   */
  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
