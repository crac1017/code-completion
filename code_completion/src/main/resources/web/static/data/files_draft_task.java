import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   *
   * COMMENT:
   * Get all file names in the directory
   *
   * TEST:
   * import java.io.File;
   * import java.util.List;
   * import java.util.ArrayList;
   * __pliny_solution__
   * List name_list = dfs("static/data/test_dir");
   * print(name_list.contains("bar.txt"));
   */
  static public List dfs(String dirname) {
    List result = new ArrayList();

    ??
    
    return result;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    return false;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = list_files("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
