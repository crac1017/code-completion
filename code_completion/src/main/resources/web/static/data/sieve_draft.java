public class Sieve {

    /**
     * COMMENT:
	   * Use Sieve of Eratosthenes algorithm to test primality
     *
     * TEST:
     * __pliny_solution__
     * print(
     * sieve(1) == false &&
     * sieve(2) == true &&
     * sieve(3) == true &&
     * sieve(4) == false &&
     * sieve(9) == false &&
     * sieve(17) == true &&
     * sieve(27) == false
     * );
     */
    static boolean sieve(int n) {
        boolean[] primes = new boolean[100];
        for(int i = 2; i <= n; i++) {
            primes[i] = ??;
        }
        ??

        return primes[n];
    }

    static public void main(String[] args) {
        int[] input = {1,     2,    3,    4,     5,    6,     7,    8,     9,     10,    27,    29,   83};
        boolean[] eo ={false, true, true, false, true, false, true, false, false, false, false, true, false};

        for(int i = 0; i < input.length; ++i) {
            boolean out = sieve(input[i]);
            System.out.println("-----------------------");
            System.out.println("Input:    " + input[i]);
            System.out.println("Expected: " + eo[i]);
            System.out.println("Output:   " + out);
            if(out != eo[i]) {
                error(out + " does not equal to " + eo[i]);
                exit();
            }
        }
        System.out.println("PASS!");
    }
}
