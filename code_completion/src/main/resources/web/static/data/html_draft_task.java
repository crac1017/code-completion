import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  
  /**
   * TODO 1:
   * Complete the following function which parse an html file and then
   * put all the hrefs in anchor tags which contains a given word into
   * a list. There should be five hrefs that contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   */
  public List get_links(String html_file, String word) {
    List result = new ArrayList();
    String content;

    /**
     * COMMENT:
     * Read text file
     *
     * TEST:
     * String html_file = "static/data/jsoup.html";
     * String content;
     * __pliny_solution__
     * print(content.startsWith("<!doctype html>"));
     */
    ??

    /**
     * COMMENT:
     * get links from html
     *
     * TEST:
     * addClassPath("static/data/jsoup-1.10.2.jar");
     * import org.jsoup.Jsoup;
     * import org.jsoup.nodes.Document;
     * import org.jsoup.nodes.Element;
     * import org.jsoup.select.Elements;
     * import java.util.regex.Matcher;
     * import java.util.regex.Pattern;
     * String content = "<html> <body> <a href=\"http://foo\"></a> <a href=\"bar\"></a> </body> </html>";
     * String word = "foo";
     * List result = new ArrayList();
     * __pliny_solution__
     * print(result.get(0).equals("http://foo"));
     */
    ??

    return result;
  }
  
  /**
   * TODO 2:
   * Test the code you've just written. There are five hrefs that
   * contains the word "jsoup".
   *
   * Return true if the binary search is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
    return false;
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }

}
