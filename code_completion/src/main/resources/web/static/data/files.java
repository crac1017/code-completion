import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.

   * COMMENT:
   * Get all file names in the directory
   *
   * TEST:
   * import java.io.File;
   * import java.util.List;
   * import java.util.ArrayList;
   * __pliny_solution__
   * name_list = list_files("static/data/test_dir");
   * print(
   * name_list.get(0).equals("bar.txt")  &&
   * name_list.get(1).equals("foo.txt")  &&
   * name_list.get(2).equals("test11.txt")  &&
   * name_list.get(3).equals("foo1.txt")  &&
   * name_list.get(4).equals("foo2.txt")  &&
   * name_list.get(5).equals("test1.txt")  &&
   * name_list.get(6).equals("test2.txt"));
   */
  static public List list_files(String dirname) {
    List result = new ArrayList();
    
    File dir_file = new File(dirname);
    File[] file_list = dir_file.listFiles();
    for(int i = 0; i < file_list.length; ++i) {
      File f = file_list[i];
      if(f.isDirectory()) {
        List new_files = list_files(f.getAbsolutePath());
        for(int j = 0; j < new_files.size(); ++j) {
          result.add(new_files.get(j));
        }
      } else {
        result.add(f.getName());
      }
    }
    return result;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    return false;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = list_files("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
