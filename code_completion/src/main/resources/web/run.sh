#!/bin/bash
# docker run --rm --name splicing-web -v $(pwd):/usr/share/nginx/html:ro -p 8080:80 nginx
# docker run -it --name splicing-web -v "$PWD":/usr/src/app -w /usr/src/app node node your-daemon-or-script.js
# docker run --rm -it --name splicing-web -p 8888:80 -v "$PWD":/usr/src/app -w /usr/src/app crac1017/splicing-user-study node app.js
docker run --rm -it --name splicing-web -p 8888:8888 -v "$PWD":/usr/src/app -w /usr/src/app crac1017/splicing-user-study nodemon app.js
# docker run -it --name splicing-web -p 8888:8888 -v "$PWD":/usr/src/app -w /usr/src/app crac1017/splicing-user-study bash
