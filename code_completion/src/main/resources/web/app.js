function run(code, run_cb) {
    const tmp = require('tmp');
    const fs = require('fs');
    var stdout = "";
    var stderr = "";

    tmp.file(function _tempFileCreated(err, path, fd, cleanup_callback) {
        console.log('Created tmp file ' + path);
        fs.writeSync(fd, code);
        console.log('Running  ======== in dir ' + __dirname);
        console.log(code);
        const spawn = require('child_process').spawn;
        const child = spawn('/usr/local/bin/ng',
                            ['bsh.Interpreter', path]);

        child.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
            stdout += data;
        });

        child.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
            stderr += data;
        });

        child.on('close', (code) => {
            console.log('Program execution finished.');
            run_cb(stdout, stderr);
        });

        child.on('error', (err) => {
            console.log('Program execution failed:' + err);
            stderr += err;
            run_cb(stdout, stderr);
        });

        console.log('Done with running java code.');
    });
}

function complete(code, cb) {
    const request = require('request');
    // const config = require('./config');
    // const port = config.completion_port;
    const port = 8889;
    const url = 'http://localhost:' + port + '/completion';
    console.log('url:' + url);
    request(
        {url: url,
         method: 'POST',
         json: {code : code}},
        function (error, res, body) {
            if(!error && res.statusCode == 200) {
                cb(body);
            }
        });
}

function main() {
    var express = require('express');
    var body_parser = require('body-parser');
    var app = express();
    app.use(express.static('static'));
    app.set('views', './static/view');
    app.set('view engine', 'pug');
    app.use(body_parser.json());

    app.get('/', function (req, res) {
        res.render('index', {title : "splicing study",
                             examples: ['---',
                                        'binsearch_draft',
                                        'sieve_draft',
                                        'sieve',
                                        'html',
                                        'http',
                                        'foo',
                                        'csv']});
    });

    app.get('/consent', function (req, res) {
        res.render('consent', {});
    });

    app.get('/task', function (req, res) {
        const task_name = req.query.task_name;
        res.render('task', {task_name : task_name});
    });

    app.post('/complete', function(req, res) {
        var code = req.body.code;
        console.log('completing code:');
        console.log(code);
        complete(code, (output) => {
            console.log('output ========');
            console.log(output);
            res.send(output);
        });
    });

    app.post('/run', function(req, res) {
        var code = req.body.code;
        console.log('code:');
        console.log(code);
        run(code, (stdout, stderr) => {
            console.log('stdout ========');
            console.log(stdout);
            console.log('stderr ========');
            console.log(stderr);
            res.send({stdout: stdout, stderr : stderr});
        });
    });

    app.listen(8888, function () {
        console.log('Example app listening on port 8888!');
    });
}

main();
