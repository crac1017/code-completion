/*
public class AttributeEvent {
  static public String STATE_CONNECTED = "STATE_CONNECTED";
  static public String STATE_DISCONNECTED = "STATE_DISCONNECTED";
  static public String ALTITUDE_UPDATED = "ALTITUDE_UPDATED";
  static public String BATTERY_UPDATED = "BATTERY_UPDATED";
  static public String GPS_COUNT = "GPS_COUNT";
  static public String STATE_VEHICLE_MODE = "STATE_VEHICLE_MODE";
}

public class Attribute {}

public class TextView {
  public String text;
  public String name;
  public TextView(String name) {
    this.name = name;
    this.text = "";
  }
  public void setText(String in) { this.text = in; }
}

public class RId {
  public String connectionStatus = "connectionStatus";
  public String altitudeTextView = "altitudeTextView";
  public String batteryTextView = "batteryTextView";
  public String satelliteTextView = "satelliteTextView";
  public String flightModeTextView = "flightModeTextView";
  public RId() {}
}

public class R {
  static public RId id = new RId();
}

public TextView findViewById(String input) {
  switch(input) {
    case "connectionStatus":
    case "altitudeTextView":
    case "batteryTextView":
    case "satelliteTextView":
    case "flightModeTextView":
      return new TextView(input);
  }
}

public class VehicleMode {
  public String label = "mode label";
  public VehicleMode() {}
  public String getLabel() { return this.label; }
}

public class State extends Attribute {
  public VehicleMode mode;
  public State() {this.mode = new VehicleMode();}
  public VehicleMode getVehicleMode() { return this.mode; }
}

public class Gps extends Attribute {
  public int count = 30;
  public Gps() {}

  public int getSatellitesCount() { return this.count; }
}

public class Battery extends Attribute {
  public int level = 300;
  public Battery() {}

  public int getBatteryRemain() {
    return this.level;
  }
}

public class Altitude extends Attribute {
  public Altitude() {}
  public double alt = 3.0;

  public double getAltitude() {
    return this.alt;
  }
}

public class AttributeType {
  static public String ALTITUDE = "ALTITUDE";
  static public String BATTERY = "BATTERY";
  static public String GPS = "GPS";
  static public String STATE = "STATE";
}

public class Drone {
  public Drone() {}
  public Attribute getAttribute(String attr) {
    switch(attr) {
      case "ALTITUDE":
        return new Altitude();
      case "BATTERY":
        return new Battery();
      case "GPS":
        return new Gps();
      case "STATE":
        return new State();
    }
  }
}
*/

public TextView onDroneEvent(String event, Drone drone) {
  switch (event) {
    case AttributeEvent.STATE_CONNECTED:
      TextView connectionStatusTextView = (TextView) findViewById(R.id.connectionStatus);
      connectionStatusTextView.setText("Connected");
      return connectionStatusTextView;

    case AttributeEvent.STATE_DISCONNECTED:
      connectionStatusTextView = (TextView) findViewById(R.id.connectionStatus);
      connectionStatusTextView.setText("Disconnected");
      return connectionStatusTextView;

    case AttributeEvent.ALTITUDE_UPDATED:
      TextView altitudeTextView = (TextView) findViewById(R.id.altitudeTextView);
      Altitude droneAltitude = drone.getAttribute(AttributeType.ALTITUDE);
      altitudeTextView.setText("Alt: " + droneAltitude.getAltitude() + "m");
      return altitudeTextView;

    case AttributeEvent.BATTERY_UPDATED:
      TextView batteryTextView = (TextView) findViewById(R.id.batteryTextView);
      Battery batt = drone.getAttribute(AttributeType.BATTERY);
      batteryTextView.setText("Batt: " + batt.getBatteryRemain() + "%");
      return batteryTextView;

    case AttributeEvent.GPS_COUNT:
      TextView satelliteTextView = (TextView) findViewById(R.id.satelliteTextView);
      Gps gps = drone.getAttribute(AttributeType.GPS);
      satelliteTextView.setText("Sats: " + gps.getSatellitesCount());
      return satelliteTextView;

    case AttributeEvent.STATE_VEHICLE_MODE:
      State vehicleState = drone.getAttribute(AttributeType.STATE);
      VehicleMode vehicleMode = vehicleState.getVehicleMode();
      TextView flightModeTextView = (TextView) findViewById(R.id.flightModeTextView);
      flightModeTextView.setText("Mode: " + vehicleMode.getLabel());
      return flightModeTextView;

    default:
      break;
  }
}

/*
TextView tv1 = onDroneEvent("STATE_CONNECTED", new Drone());
print(tv1.name + " : " + tv1.text);
TextView tv2 = onDroneEvent("STATE_DISCONNECTED", new Drone());
print(tv2.name + " : " + tv2.text);
TextView tv3 = onDroneEvent("ALTITUDE_UPDATED", new Drone());
print(tv3.name + " : " + tv3.text);
TextView tv4 = onDroneEvent("BATTERY_UPDATED", new Drone());
print(tv4.name + " : " + tv4.text);
TextView tv5 = onDroneEvent("GPS_COUNT", new Drone());
print(tv5.name + " : " + tv5.text);
TextView tv6 = onDroneEvent("STATE_VEHICLE_MODE", new Drone());
print(tv6.name + " : " + tv6.text);
*/
