public class AttributeEvent {
  static public String STATE_CONNECTED = "STATE_CONNECTED";
  static public String STATE_DISCONNECTED = "STATE_DISCONNECTED";
  static public String ALTITUDE_UPDATED = "ALTITUDE_UPDATED";
  static public String BATTERY_UPDATED = "BATTERY_UPDATED";
  static public String GPS_COUNT = "GPS_COUNT";
  static public String STATE_VEHICLE_MODE = "STATE_VEHICLE_MODE";
}

public class Attribute {}

public class TextView {
  public String text;
  public String name;
  public TextView(String name) {
    this.name = name;
    this.text = "";
  }
  public void setText(String in) { this.text = in; }
}

public class RId {
  public String connectionStatus = "connectionStatus";
  public String altitudeTextView = "altitudeTextView";
  public String batteryTextView = "batteryTextView";
  public String satelliteTextView = "satelliteTextView";
  public String flightModeTextView = "flightModeTextView";
  public RId() {}
}

public class R {
  static public RId id = new RId();
}

  public TextView findViewById(String input) {
    switch(input) {
      case "connectionStatus":
      case "altitudeTextView":
      case "batteryTextView":
      case "satelliteTextView":
      case "flightModeTextView":
        return new TextView(input);
    }
  }

public class VehicleMode {
  public String label = "mode label";
  public VehicleMode() {}
  public String getLabel() { return this.label; }
}

public class State extends Attribute {
  public VehicleMode mode;
  public State() {this.mode = new VehicleMode();}
  public VehicleMode getVehicleMode() { return this.mode; }
}

public class Gps extends Attribute {
  public int count = 30;
  public Gps() {}

  public int getSatellitesCount() { return this.count; }
}

public class Battery extends Attribute {
  public int level = 300;
  public Battery() {}

  public int getBatteryRemain() {
    return this.level;
  }
}

public class Altitude extends Attribute {
  public Altitude() {}
  public double alt = 3.0;

  public double getAltitude() {
    return this.alt;
  }
}

public class AttributeType {
  static public String ALTITUDE = "ALTITUDE";
  static public String BATTERY = "BATTERY";
  static public String GPS = "GPS";
  static public String STATE = "STATE";
}

public class Drone {
  public Drone() {}
  public Attribute getAttribute(String attr) {
    switch(attr) {
      case "ALTITUDE":
        return new Altitude();
      case "BATTERY":
        return new Battery();
      case "GPS":
        return new Gps();
      case "STATE":
        return new State();
    }
  }
}
