/**
 * This class handles DroidPlanner's status bar, and audible notifications. It
 * also provides support for the Android Wear functionality.
 */
public class NotificationHandler implements DroneInterfaces.OnDroneListener {

  /**
   * Handles Droidplanner's audible notifications.
   */
  private final TTSNotificationProvider mTtsNotification;

  /**
   * Handles Droidplanner's status bar notification.
   */
  private final StatusBarNotificationProvider mStatusBarNotification;

  /**
   * Handles Pebble notification.
   */
  private final PebbleNotificationProvider mPebbleNotification;

  /**
   * Handles emergency beep notification.
   */
  private final EmergencyBeepNotificationProvider mBeepNotification;

  @Override
  public void onDroneEvent(DroneInterfaces.DroneEventsType event, Drone drone) {
    mTtsNotification.onDroneEvent(event, drone);
    mStatusBarNotification.onDroneEvent(event, drone);
    mPebbleNotification.onDroneEvent(event, drone);
    mBeepNotification.onDroneEvent(event, drone);

    switch(event){
      case AUTOPILOT_WARNING:
        final HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder()
          .setCategory(GAUtils.Category.FAILSAFE)
          .setAction("Autopilot warning")
          .setLabel(drone.getState().getWarning());
        GAUtils.sendEvent(eventBuilder);
        break;
    }
  }

}