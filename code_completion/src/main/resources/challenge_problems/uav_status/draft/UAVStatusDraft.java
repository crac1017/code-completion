public class MainActivity {
  /**
   * COMMENT:
   * Listens to drone events and set the text to a TextView
   *
   * TEST:
   * public class AttributeEvent {
   *   static public String STATE_CONNECTED = "STATE_CONNECTED";
   *   static public String STATE_DISCONNECTED = "STATE_DISCONNECTED";
   *   static public String ALTITUDE_UPDATED = "ALTITUDE_UPDATED";
   *   static public String BATTERY_UPDATED = "BATTERY_UPDATED";
   *   static public String GPS_COUNT = "GPS_COUNT";
   *   static public String STATE_VEHICLE_MODE = "STATE_VEHICLE_MODE";
   * }
   *
   * public class Attribute {}
   *
   * public class TextView {
   *   public String text;
   *   public String name;
   *   public TextView(String name) {
   *     this.name = name;
   *     this.text = "";
   *   }
   *   public void setText(String in) { this.text = in; }
   * }
   *
   * public class RId {
   *   public String connectionStatus = "connectionStatus";
   *   public String altitudeTextView = "altitudeTextView";
   *   public String batteryTextView = "batteryTextView";
   *   public String satelliteTextView = "satelliteTextView";
   *   public String flightModeTextView = "flightModeTextView";
   *   public RId() {}
   * }
   *
   * public class R {
   *   static public RId id = new RId();
   * }
   *
   *   public TextView findViewById(String input) {
   *     switch(input) {
   *       case "connectionStatus":
   *       case "altitudeTextView":
   *       case "batteryTextView":
   *       case "satelliteTextView":
   *       case "flightModeTextView":
   *         return new TextView(input);
   *     }
   *   }
   *
   * public class VehicleMode {
   *   public String label = "mode label";
   *   public VehicleMode() {}
   *   public String getLabel() { return this.label; }
   * }
   *
   * public class State extends Attribute {
   *   public VehicleMode mode;
   *   public State() {this.mode = new VehicleMode();}
   *   public VehicleMode getVehicleMode() { return this.mode; }
   * }
   *
   * public class Gps extends Attribute {
   *   public integer count = 30;
   *   public Gps() {}
   *
   *   public integer getSatellitesCount() { return this.count; }
   * }
   *
   * public class Battery extends Attribute {
   *   public integer level = 300;
   *   public Battery() {}
   *
   *   public integer getBatteryRemain() {
   *     return this.level;
   *   }
   * }
   *
   * public class Altitude extends Attribute {
   *   public Altitude() {}
   *   public double alt = 3.0;
   *
   *   public double getAltitude() {
   *     return this.alt;
   *   }
   * }
   *
   * public class AttributeType {
   *   static public String ALTITUDE = "ALTITUDE";
   *   static public String BATTERY = "BATTERY";
   *   static public String GPS = "GPS";
   *   static public String STATE = "STATE";
   * }
   *
   * public class Drone {
   *   public Drone() {}
   *   public Attribute getAttribute(String attr) {
   *     switch(attr) {
   *       case "ALTITUDE":
   *         return new Altitude();
   *       case "BATTERY":
   *         return new Battery();
   *       case "GPS":
   *         return new Gps();
   *       case "STATE":
   *         return new State();
   *     }
   *   }
   * }
   *
   * __pliny_solution__
   * TextView tv1 = onDroneEvent("STATE_CONNECTED", new Drone());
   * if(!tv1.name.equals("connectionStatus") || !tv1.text.equals("Connected")) {
   *   print("STATE CONNECTED Failed.");
   *   print("false");
   *   exit();
   * }
   * TextView tv2 = onDroneEvent("STATE_DISCONNECTED", new Drone());
   * if(!tv2.name.equals("connectionStatus") || !tv2.text.equals("Disconnected")) {
   *   print("STATE DISCONNECTED Failed.");
   *   print("false");
   *   exit();
   * }
   * TextView tv3 = onDroneEvent("ALTITUDE_UPDATED", new Drone());
   * if(!tv3.name.equals("altitudeTextView") || !tv3.text.equals("Alt: 3.0m")) {
   *   print("ALTITUDE_UPDATED Failed.");
   *   print(tv3.name + " : |" + tv3.text + "|");
   *   print(tv3.name == "altitudeTextView");
   *   print(tv3.text == "Alt: 3.0m");
   *   print("false");
   *   exit();
   * }
   * TextView tv4 = onDroneEvent("BATTERY_UPDATED", new Drone());
   * if(!tv4.name.equals("batteryTextView") || !tv4.text.equals("Batt: 300%")) {
   *   print("BATTERY_UPDATED Failed.");
   *   print("false");
   *   exit();
   * }
   * TextView tv5 = onDroneEvent("GPS_COUNT", new Drone());
   * if(!tv5.name.equals("satelliteTextView") || !tv5.text.equals("Sats: 30")) {
   *   print("GPS_COUNT Failed.");
   *   print("false");
   *   exit();
   * }
   * TextView tv6 = onDroneEvent("STATE_VEHICLE_MODE", new Drone());
   * if(!tv6.name.equals("flightModeTextView") || !tv6.text.equals("Mode: mode label")) {
   *   print("STATE_VEHICLE_MODE Failed.");
   *   print("|" + tv6.name + "| : |" + tv6.text + "|");
   *   print("false");
   *   exit();
   * }
   *
   * print("true");
   *
   */
  public TextView onDroneEvent(String event, Drone drone) {
    switch (event) {
      case AttributeEvent.STATE_CONNECTED:
          ??

      case AttributeEvent.STATE_DISCONNECTED:
        TextView connectionStatusTextView = (TextView) findViewById(R.id.connectionStatus);
        connectionStatusTextView.setText("Disconnected");
        return connectionStatusTextView;

      case AttributeEvent.ALTITUDE_UPDATED:
        TextView alTextView = (TextView) findViewById(R.id.altitudeTextView);
        Altitude droneAltitude = drone.getAttribute(AttributeType.ALTITUDE);
        alTextView.setText("Alt: " + droneAltitude.getAltitude() + "m");
        return alTextView;

      case AttributeEvent.BATTERY_UPDATED:
        TextView battTextView = (TextView) findViewById(R.id.batteryTextView);
        Battery batt = drone.getAttribute(AttributeType.BATTERY);
        battTextView.setText("Batt: " + batt.getBatteryRemain() + "%");
        return battTextView;

      case AttributeEvent.GPS_COUNT:
        TextView satTextView = (TextView) findViewById(R.id.satelliteTextView);
        Gps gps = drone.getAttribute(AttributeType.GPS);
        satTextView.setText("Sats: " + gps.getSatellitesCount());
        return satTextView;

      case AttributeEvent.STATE_VEHICLE_MODE:
        State vehicleState = drone.getAttribute(AttributeType.STATE);
        VehicleMode vehicleMode = vehicleState.getVehicleMode();
        TextView fModeTextView = (TextView) findViewById(R.id.flightModeTextView);
        fModeTextView.setText("Mode: " + vehicleMode.getLabel());
        return fModeTextView;
      default:
        break;
    }
  }
}
