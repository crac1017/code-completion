public class MainActivity {

  /**
   * Listens to drone events and set the text to a TextView
   */
  public TextView onDroneEvent(String event, Drone drone) {

    switch (event) {
      case AttributeEvent.STATE_CONNECTED:
        TextView connectionStatusTextView = (TextView) findViewById(R.id.connectionStatus);
        connectionStatusTextView.setText("Connected");
        return connectionStatusTextView;

      case AttributeEvent.STATE_DISCONNECTED:
        connectionStatusTextView = (TextView) findViewById(R.id.connectionStatus);
        connectionStatusTextView.setText("Disconnected");
        return connectionStatusTextView;

      case AttributeEvent.ALTITUDE_UPDATED:
        TextView alTextView = (TextView) findViewById(R.id.altitudeTextView);
        Altitude droneAltitude = drone.getAttribute(AttributeType.ALTITUDE);
        alTextView.setText("Alt: " + droneAltitude.getAltitude() + "m");
        return alTextView;

      case AttributeEvent.BATTERY_UPDATED:
        TextView battTextView = (TextView) findViewById(R.id.batteryTextView);
        Battery batt = drone.getAttribute(AttributeType.BATTERY);
        battTextView.setText("Batt: " + batt.getBatteryRemain() + "%");
        return battTextView;

      case AttributeEvent.GPS_COUNT:
        TextView satTextView = (TextView) findViewById(R.id.satelliteTextView);
        Gps gps = drone.getAttribute(AttributeType.GPS);
        satTextView.setText("Sats: " + gps.getSatellitesCount());
        return satTextView;

      case AttributeEvent.STATE_VEHICLE_MODE:
        State vehicleState = drone.getAttribute(AttributeType.STATE);
        VehicleMode vehicleMode = vehicleState.getVehicleMode();
        TextView fModeTextView = (TextView) findViewById(R.id.flightModeTextView);
        fModeTextView.setText("Mode: " + vehicleMode.getLabel());
        return fModeTextView;
      default:
        break;
    }
  }
}
