TextView tv1 = onDroneEvent("STATE_CONNECTED", new Drone());
if(!tv1.name.equals("connectionStatus") || !tv1.text.equals("Connected")) {
  print("STATE CONNECTED Failed.");
  print("false");
  exit();
}
TextView tv2 = onDroneEvent("STATE_DISCONNECTED", new Drone());
if(!tv2.name.equals("connectionStatus") || !tv2.text.equals("Disconnected")) {
  print("STATE DISCONNECTED Failed.");
  print("false");
  exit();
}
TextView tv3 = onDroneEvent("ALTITUDE_UPDATED", new Drone());
if(!tv3.name.equals("altitudeTextView") || !tv3.text.equals("Alt: 3.0m")) {
  print("ALTITUDE_UPDATED Failed.");
  print(tv3.name + " : |" + tv3.text + "|");
  print(tv3.name == "altitudeTextView");
  print(tv3.text == "Alt: 3.0m");
  print("false");
  exit();
}
TextView tv4 = onDroneEvent("BATTERY_UPDATED", new Drone());
if(!tv4.name.equals("batteryTextView") || !tv4.text.equals("Batt: 300%")) {
  print("BATTERY_UPDATED Failed.");
  print("false");
  exit();
}
TextView tv5 = onDroneEvent("GPS_COUNT", new Drone());
if(!tv5.name.equals("satelliteTextView") || !tv5.text.equals("Sats: 30")) {
  print("GPS_COUNT Failed.");
  print("false");
  exit();
}
TextView tv6 = onDroneEvent("STATE_VEHICLE_MODE", new Drone());
if(!tv6.name.equals("flightModeTextView") || !tv6.text.equals("Mode: mode label")) {
  print("STATE_VEHICLE_MODE Failed.");
  print("|" + tv6.name + "| : |" + tv6.text + "|");
  print("false");
  exit();
}

print("true");
