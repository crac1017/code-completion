public boolean test_no_collision() {
  Vector3 a_min = new Vector3(0, 0, 0);
  Vector3 a_max = new Vector3(1, 1, 1);
  AABB a = new AABB(a_min, a_max);

  Vector3 b_min = new Vector3(10, 10, 10);
  Vector3 b_max = new Vector3(20, 20, 20);
  AABB b = new AABB(b_min, b_max);

  if(testCollision(a, b)) {
    return false;
  }

  Vector3 c_min = new Vector3(0, 0, 0);
  Vector3 c_max = new Vector3(10, 10, 10);
  AABB c = new AABB(c_min, c_max);

  Vector3 d_min = new Vector3(0, 0, 30);
  Vector3 d_max = new Vector3(20, 20, 2);
  AABB d = new AABB(d_min, d_max);

  if(testCollision(c, d)) {
    return false;
  }

  Vector3 e_min = new Vector3(0, 0, 0);
  Vector3 e_max = new Vector3(10, 10, 10);
  AABB e = new AABB(e_min, e_max);

  Vector3 f_min = new Vector3(30, 0, 0);
  Vector3 f_max = new Vector3(50, 20, 2);
  AABB f = new AABB(f_min, f_max);

  if(testCollision(e, f)) {
  return false;
  }

  return true;
}

public boolean test_collision() {
  Vector3 a_min = new Vector3(0, 0, 0);
  Vector3 a_max = new Vector3(10, 10, 10);
  AABB a = new AABB(a_min, a_max);

  Vector3 b_min = new Vector3(0, 0, 0);
  Vector3 b_max = new Vector3(20, 20, 2);
  AABB b = new AABB(b_min, b_max);

  if(!testCollision(a, b)) {
    return false;
  }

  return true;
}

public void test() {
  if(!test_no_collision() || !test_collision()) {
    println("false");
  }
  print("true");
}
test();
