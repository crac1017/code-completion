import net.smert.jreactphysics3d.mathematics.Vector3;
import net.smert.jreactphysics3d.collision.shapes.AABB;
public class AABB2 {
  /**
   * COMMENT:
   * Testing whether two AABB objects collide
   *
   * TEST:
   * import net.smert.jreactphysics3d.mathematics.Vector3;
   * import net.smert.jreactphysics3d.collision.shapes.AABB;
   *
   * __pliny_solution__
   * public boolean test_no_collision() {
   *   Vector3 a_min = new Vector3(0, 0, 0);
   *   Vector3 a_max = new Vector3(1, 1, 1);
   *   AABB a = new AABB(a_min, a_max);
   *
   *   Vector3 b_min = new Vector3(10, 10, 10);
   *   Vector3 b_max = new Vector3(20, 20, 20);
   *   AABB b = new AABB(b_min, b_max);
   *
   *   if(testCollision(a, b)) {
   *     return false;
   *   }
   *
   *   Vector3 c_min = new Vector3(0, 0, 0);
   *   Vector3 c_max = new Vector3(10, 10, 10);
   *   AABB c = new AABB(c_min, c_max);
   *
   *   Vector3 d_min = new Vector3(0, 0, 30);
   *   Vector3 d_max = new Vector3(20, 20, 2);
   *   AABB d = new AABB(d_min, d_max);
   *
   *   if(testCollision(c, d)) {
   *     return false;
   *   }
   *
   *   Vector3 e_min = new Vector3(0, 0, 0);
   *   Vector3 e_max = new Vector3(10, 10, 10);
   *   AABB e = new AABB(e_min, e_max);
   *
   *   Vector3 f_min = new Vector3(30, 0, 0);
   *   Vector3 f_max = new Vector3(50, 20, 2);
   *   AABB f = new AABB(f_min, f_max);
   *
   *   if(testCollision(e, f)) {
   *   return false;
   *   }
   *
   *   return true;
   * }
   *
   * public boolean test_collision() {
   *   Vector3 a_min = new Vector3(0, 0, 0);
   *   Vector3 a_max = new Vector3(10, 10, 10);
   *   AABB a = new AABB(a_min, a_max);
   *
   *   Vector3 b_min = new Vector3(0, 0, 0);
   *   Vector3 b_max = new Vector3(20, 20, 2);
   *   AABB b = new AABB(b_min, b_max);
   *
   *   if(!testCollision(a, b)) {
   *     return false;
   *   }
   *
   *   return true;
   * }
   *
   * public boolean test() {
   *   if(!test_no_collision() || !test_collision()) {
   *     return false;
   *   }
   *   return true;
   * }
   * boolean _result_ = test();
   */
  public boolean testCollision(AABB a, AABB aabb) {
    if (a.getMax().getX() < aabb.getMin().getX() || aabb.getMax().getX() < a.getMin().getX()) {
      return false;
    }
    if (a.getMax().getZ() < aabb.getMin().getZ() || aabb.getMax().getZ() < a.getMin().getZ()) {
      return false;
    }
    return a.getMax().getY() >= aabb.getMin().getY() && aabb.getMax().getY() >= a.getMin().getY();
  }

}

