import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
public class FaceDetection {

  /**
   * COMMENT:
   * Doing face detection using OpenCV
   *
   * TEST:
   * source("src/main/resources/challenge_problems/face_detection/FaceDetectionTestLib.java");
   * __pliny_solution__
   * run();
   * print(_has_detector_ == true && _has_image_ == true && _has_detection_ == true && _image_written_ == true);
   *
   */
  public void run() {
    String input_image = "lena.png";
    String filename = "faceDetection.png";

    // Create a face detector from the cascade file in the resources
    // directory.
    CascadeClassifier faceDetector = new CascadeClassifier(getClass().getResource("lbpcascade_frontalface.xml").getPath());
    Mat image = Highgui.imread(getClass().getResource(input_image).getPath());

    // Detect faces in the image.
    // MatOfRect is a special container class for Rect.
    MatOfRect faceDetections = new MatOfRect();
    faceDetector.detectMultiScale(image, faceDetections);

    // Draw a bounding box around each face.
    for (Rect rect : faceDetections.toArray()) {
      Core.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
    }

    // Save the visualized detection.
    Highgui.imwrite(filename, image);
  }


}
