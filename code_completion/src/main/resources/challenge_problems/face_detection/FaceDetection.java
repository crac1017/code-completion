public void run() {
    // System.out.println("\nRunning DetectFaceDemo");
    String input_img = "lena.jpg";
    String output_img = "faceDetection.png";


    // Create a face detector from the cascade file in the resources
    // directory.
    CascadeClassifier faceDetector = new CascadeClassifier(getClass().getResource("haarcascade_frontalface_alt.xml").getPath());
    Mat image = Imgcodecs.imread(getClass().getResource(input_img).getPath());

    // Detect faces in the image.
    // MatOfRect is a special container class for Rect.
    MatOfRect faceDetections = new MatOfRect();
    faceDetector.detectMultiScale(image, faceDetections);

    // System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));

    // Draw a bounding box around each face.
    for (Rect rect : faceDetections.toArray()) {
        Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
    }

    // Save the visualized detection.
    // System.out.println(String.format("Writing %s", filename));
    Imgcodecs.imwrite(output_img, image);
}
