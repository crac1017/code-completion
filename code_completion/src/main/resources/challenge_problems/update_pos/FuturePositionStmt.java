double cos_lat0 = Math.cos(ref.lat * Math.PI / 180.0);
double sin_lat0 = Math.sin(ref.lat * Math.PI / 180.0);
double sin_lat = Math.sin(gpsPosPlusAlt.lat * Math.PI / 180.0);
double cos_lat = Math.cos(gpsPosPlusAlt.lat * Math.PI / 180.0);
double cos_d_lon = Math.cos(gpsPosPlusAlt.lon * Math.PI / 180.0 - ref.lon * Math.PI / 180.0);
double c = Math.acos(sin_lat0 * sin_lat + cos_lat0 * cos_lat * cos_d_lon);
double k = (c == 0.0) ? 1.0 : (c / Math.sin(c));
double y = k * cos_lat * Math.sin(gpsPosPlusAlt.lon * Math.PI / 180.0 - ref.lon * Math.PI / 180.00) * r_earth;
double x = k * (cos_lat0 * sin_lat - sin_lat0 * cos_lat * cos_d_lon) * r_earth;
double z = ref.alt - gpsPosPlusAlt.alt;
pos[0] = x;
pos[1] = y;
pos[2] = z;
