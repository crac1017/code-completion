public class LatLonAlt {
  public static double RADIUS_OF_EARTH = 6371000.0;

  public final double lat;
  public final double lon;
  public final double alt;

  public LatLonAlt(double lat, double lon, double alt) {
    this.lat = lat;
    this.lon = lon;
    this.alt = alt;
  }

  public LatLonAlt add(double[] vector) {
    return new LatLonAlt(
      lat + vector[0] / RADIUS_OF_EARTH * 180.0 / Math.PI,
      lon + vector[1] / (Math.cos(lat * Math.PI / 180.0) * RADIUS_OF_EARTH) * 180.0 / Math.PI,
      alt - vector[2]);
  }

  public double[] sub(LatLonAlt pos) {
    return new double[]{
      (lat - pos.lat) * Math.PI / 180.0 * RADIUS_OF_EARTH,
      (lon - pos.lon) * Math.PI / 180.0 * Math.cos(lat * Math.PI / 180.0) * RADIUS_OF_EARTH,
      pos.alt - alt
    };
  }

  public boolean isFinite() {
    return Double.isFinite(lat) && Double.isFinite(lon) && Double.isFinite(alt);
  }

}
LatLonAlt gpsPosPlusAlt = new LatLonAlt(1.0f, 1.0f, 1.0f);
LatLonAlt ref = new LatLonAlt(0.0f, 0.0f, 0.0f);
double r_earth = 6371000.0;
double [] pos = new double[3];
