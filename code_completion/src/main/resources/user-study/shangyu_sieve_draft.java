public class Sieve {

  /**
   * TODO 1: Complete the following function. You could use our system
   * by replacing the contents in COMMENT and TEST.
   *
   * COMMENT:
   * sieve of eratosthenes algorithm for testing primality
   *
   * TEST:
   * // make this tests stronger
   * __pliny_solution__
   * print(sieve(1) == false && sieve(2) == true && 
   * sieve(3) == true && sieve(6) == false && sieve(29) == true);
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    primes[0] = false;
    primes[1] = false;
    for (int _2_i_ = 2; _2_i_ <= n; _2_i_++) {
        primes[_2_i_] = true;
    }
    for (int _2_i_ = 2; _2_i_ <= n / 2; _2_i_++) {
        for (int _2_j_ = 2; _2_j_ <= n / _2_i_; _2_j_++) {
            primes[_2_i_ * _2_j_] = false;
        }
    }
    return primes[n];
  }

  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    return sieve(1) == false && sieve(2) == true && sieve(3) == true && sieve(4) == false
    && sieve(5) == true && sieve(6) == false && sieve(7) == true && sieve(8) == false
    && sieve(9) == false && sieve(10) == false && sieve(27) == false && sieve(29) == true && sieve(73) == true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}