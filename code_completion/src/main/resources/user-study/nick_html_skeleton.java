import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  /**
   * TODO 1: 
   * Get all hrefs from anchor tags where href has the input word
   * from the input html file. Please use jsoup as the HTML parsing library.
   */
  public static List get_links(String html_file, String word) throws IOException {
    List result = new ArrayList();
    
    BufferedReader in = new BufferedReader(new FileReader(html_file));
    
    StringBuilder sb = new StringBuilder();
    for (;;) {
        String line = in.readLine();
        if (line == null) break;
        sb.append(line);
    }
    
    Document doc = Jsoup.parse(sb.toString());
    Elements links = doc.select("a[href]");
    
    for (Element x : links) {
        String href = x.attr("href");
        if (href.contains(word)) {
            result.add(href);
        }
    }
    
    return result;
  }

  /**
   * TODO 2:
   * Test the code you've just written. There should be five hrefs that
   * contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
    List expected = new ArrayList();
    expected.add("//try.jsoup.org/");
    expected.add("/apidocs/org/jsoup/select/Elements.html#html--");
    expected.add("/apidocs/index.html?org/jsoup/select/Elements.html");
    expected.add("//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g");
    expected.add("http://github.com/jhy/jsoup/");
    return result.equals(expected);
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
