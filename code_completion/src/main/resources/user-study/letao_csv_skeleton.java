public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    
    int total = 99;
    for (int i = 1; i <= total; i++) {
        primes[i] = true;
    }
    
    for (int factor = 2; factor * factor <= total; factor++) {
        if (primes[factor]) {
            for (int j = factor; factor * j <= total; j++) {
                primes[factor * j] = false;
            }
        }
    }
    
    return primes[n];
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    int[] primes = {1, 2, 3, 5, 7, 29, 73};
    for (int i : primes) {
        if (!sieve(i)) {
            return false;
        }
    }
    
    int[] composites = {4, 6, 6, 8, 9, 10, 27};
    for (int i : composites) {
        if (sieve(i)) {
            return false;
        }    
    }

    return true; 
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}

CSV:

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.util.Scanner;

public class CSV {

  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2. The matrix in the CSV looks like this:
   * 1,2,3
   * 2,3,4
   * 3,4,5
   */
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    
    // read a 3x3 matrix
    int[][] mat1 = new int[n][n];
    BufferedReader br = new BufferedReader(new FileReader(filename));
    String line;
    int row = 0;
    while ((line = br.readLine()) != null) {
        String[] nums = line.split(",");
        for (int i = 0; i < nums.length; i++) {
            System.out.println(nums[i]);
        }
        for (int col = 0; col < n; col++) {
            mat1[row][col] = Integer.parseInt(nums[col]);
            System.out.println(mat1[row][col]);
        }
        row++;
    }
    
    // square mat1 and store the results into mat2
    int[][] mat2 = new int[n][n];
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                mat2[i][j] += mat1[i][k] * mat1[k][j];
            }
        }
    }
    
    return mat2;
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix.
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat2) {
    int[][] ans = {{14, 20, 26}, {20, 29, 38}, {26, 38, 50}};
    int n = 3;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            System.out.println(mat2[i][j]);
            System.out.println(ans[i][j]);
            if (mat2[i][j] != ans[i][j]) {
                return false;
            }
        }
    }
    
    return true;
  }

  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}