public class Sieve {

  /**
   * TODO 1: Complete the following function. You could use our system
   * by replacing the contents in COMMENT and TEST.
   *
   * COMMENT:
   * sieve of eratosthenes algorithm for testing primality
   *
   * TEST:
   * // make this tests stronger
   * __pliny_solution__
   * boolean test1 = sieve(1) == false;
   * boolean test2 = sieve(2) == true;
   * boolean test3 = sieve(13) == true;
   * boolean test4 = sieve(14) == false;
   * boolean test5 = sieve(25) == false;
   * print(test1 && test2 && test3 && test4 && test5);
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];

    for (int _1_i_ = 2; _1_i_ <= n; _1_i_++) {
        primes[_1_i_] = true;
    }
    for (int _1_i_ = 2; _1_i_ <= n / 2; _1_i_++) {
        for (int _1_j_ = 2; _1_j_ <= n / _1_i_; _1_j_++) {
            primes[_1_i_ * _1_j_] = false;
        }
    }

    return primes[n];
  }

  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   *
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    boolean test1 = sieve(1) == false;
    boolean test2 = sieve(2) == true;
    boolean test3 = sieve(3) == true;
    boolean test4 = sieve(4) == false;
    boolean test5 = sieve(5) == true;
    boolean test6 = sieve(6) == false;
    boolean test7 = sieve(7) == true;
    boolean test8 = sieve(8) == false;
    boolean test9 = sieve(9) == false;
    boolean test10 = sieve(10) == false;
    boolean test11 = sieve(27) == false;
    boolean test12 = sieve(29) == true;
    boolean test13 = sieve(73) == true;
    return test1 && test2 && test3 && test4 && test5 && test6
    && test7 && test8 && test9 && test10 && test11 && test12 && test13;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
