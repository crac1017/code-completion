public class Sieve { 

/** 
* TODO 1: 
* Use Sieve of eratosthenes to test primality of the given 
* integer. 
*/ 
static boolean sieve(int n) { 
boolean[] primes = new boolean[100]; 

for (int i = 0; i < 100; i++) { 
primes[i] = true; 
} 
primes[0] = false; 
primes[1] = false; 
primes[2] = true; 
for (int i = 2; i < 100; i++) { 
for (int j =2; true; j++) { 
if((i*j) >= 100) { 
break; 
} else { 
if(primes[i*j] == true){ 
primes[i*j] = false; 
} 
} 

} 

} 
return primes[n]; 
} 

/** 
* TODO 2: 
* Test the sieve of eratosthenes you've just written. Make sure to test the 
* program with the following inputs: 
* 
* n: 1 
* n: 2 
* n: 3 
* n: 4 
* n: 5 
* n: 6 
* n: 6 
* n: 7 
* n: 8 
* n: 9 
* n: 10 
* n: 27 
* n: 29 
* n: 73 
* 
* Return true if the program is correct. Otherwise, return false. 
*/ 
static public boolean test() { 

return (sieve(1) == false) && (sieve(2) == true) && (sieve(3) == true) && 
(sieve(4)==false) && (sieve(5)==true) && (sieve(6)==false) && 
(sieve(7) == true) 
&& (sieve(8) == false) && (sieve(9)== false) && 
(sieve(10)==false) && (sieve(27)==false) 
&&(sieve(29)==true) && (sieve(73)==true); 
} 


/** 
* Don't modify this function 
*/ 
static public void main(String[] args) { 
if(test()) { 
print("PASS!"); 
} else { 
print("FAIL!"); 
} 
} 
} 