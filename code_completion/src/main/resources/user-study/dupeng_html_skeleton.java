import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  /**
   * TODO 1: 
   * Get all hrefs from anchor tags where href has the input word
   * from the input html file. Please use jsoup as the HTML parsing library.
   */
  public static List get_links(String html_file, String word) throws IOException {
    List result = new ArrayList();
    File file = new File(html_file);
    StringBuilder sb = new StringBuilder();
    BufferedReader br = new BufferedReader(new FileReader(file));
    String st = br.readLine();
    
    while(st!= null){
        sb.append(st);
        st = br.readLine();
    }
    //System.out.println(sb.toString());
    Document doc = Jsoup.parse(sb.toString());
    Elements imports = doc.select("a[href]");
    for (Element link : imports) {
           String l  = link.attr("href");
            //System
           if(l.contains(word)){
               result.add(l);
           }
        }
    return result;
  }

  /**
   * TODO 2:
   * Test the code you've just written. There should be five hrefs that
   * contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
     List trueResult = new ArrayList();
    trueResult.add("//try.jsoup.org/");
    trueResult.add("/apidocs/org/jsoup/select/Elements.html#html--");
    trueResult.add("/apidocs/index.html?org/jsoup/select/Elements.html");
    trueResult.add("//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g");
    trueResult.add("http://github.com/jhy/jsoup/");
    if(result.size() != trueResult.size()){
        return false;
    }
    else{
        for(String f : result){
            if(trueResult.contains(f)){
                trueResult.remove(f);
            }
        }
        return trueResult.size() == 0;
    }
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}