import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {

  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2. The matrix in the CSV looks like this:
   * 1,2,3
   * 2,3,4
   * 3,4,5
   */
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    int[][] mat1 = new int[n][n];
    int[][] mat2 = new int[n][n];

    // read a 3x3 matrix
    
    BufferedReader br = null;
	FileReader fr = null;

	try {

		String currLine;

		br = new BufferedReader(new FileReader(filename));

        int row = 0; 
		while ((currLine = br.readLine()) != null) {
		    // print(row);
		    int col = 0; 
			for (String num: currLine.split(",")) {
			  // print(col);
              mat1[row][col++] = Integer.parseInt(num); 
            }
            row++; 
		}

	} catch (IOException e) {
		e.printStackTrace();
	} 
    // square mat1 and store the results into mat2
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < n; k++) {
                mat2[i][j] += mat1[i][k] * mat1[k][j]; 
            }
        }
    }
    print(mat2[1][2]);
    print(mat2[2][2]);
    
    return mat2;
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix.
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat1) {
    int[] xes = {0,1,2};
    int[] yes = {2,1,0}; 
    int[] nums = {26, 29, 26};
    for (int i = 0; i < 3; i++) {
        int x = xes[i]; 
        int y = yes[i]; 
        if (mat1[x][y] != nums[i]) return false;  
    }
    return true;
  }

  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
