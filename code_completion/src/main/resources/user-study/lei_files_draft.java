import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   *
   * COMMENT:
   * Get all file names in the directory
   *
   * TEST:
   * import java.io.File;
   * import java.util.List;
   * import java.util.ArrayList;
   * __pliny_solution__
   * List name_list = dfs("static/data/test_dir");
   * List files = new ArrayList();
   * files.add("bar.txt"); 
   * files.add("foo.txt"); 
   * files.add("test11.txt");
   * files.add("foo1.txt"); 
   * files.add("foo2.txt"); 
   * files.add("test1.txt"); 
   * files.add("test2.txt");
   * for (String file: files) {
   *   if (!name_list.contains(file)) {
   *     print(false); 
   *     exit();
   * }
   *       
   * }
   * print(true);
   */
   
    public static List dfs(String dirname) {
        List result = new ArrayList();
        File _0_dir_file_ = new File(dirname);
        File[] _0_file_list_ = _0_dir_file_.listFiles();
        for (int _0_i_ = 0; _0_i_ < _0_file_list_.length; ++_0_i_) {
            File _0_f_ = _0_file_list_[_0_i_];
            if (_0_f_.isDirectory()) {
                List _0_new_files_ = dfs(_0_f_.getAbsolutePath());
                for (int _0_j_ = 0; _0_j_ < _0_new_files_.size(); ++_0_j_) {
                    result.add(_0_new_files_.get(_0_j_));
                }
            } else {
                result.add(_0_f_.getName());
            }
        }
        return result;
    }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
        List files = new ArrayList();
        files.add("bar.txt"); 
        files.add("foo.txt"); 
        files.add("test11.txt");
        files.add("foo1.txt"); 
        files.add("foo2.txt"); 
        files.add("test1.txt"); 
        files.add("test2.txt");
        for (String file: files) {
            if (!filenames.contains(file)) {
                return false; 
            }
        }
        return true;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
