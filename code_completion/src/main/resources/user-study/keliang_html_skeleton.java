import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  /**
   * TODO 1: 
   * Get all hrefs from anchor tags where href has the input word
   * from the input html file. Please use jsoup as the HTML parsing library.
   */
  public static List get_links(String html_file, String word) throws IOException {
    List result = new ArrayList();
    String html;
    
    // Read file into string
    BufferedReader br = new BufferedReader(new FileReader(html_file));
    try {
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
            sb.append(line);
            sb.append("\n");
            line = br.readLine();
        }
        html = sb.toString();
    } finally {
        br.close();
    }
    
    Document doc = Jsoup.parse(html);
    Elements links = doc.select("a[href]");
    for (Element link : links){
        String ref = link.attr("href");
        if (ref.contains(word)){
            result.add(ref);
        }
    }
    return result;
  }

  /**
   * TODO 2:
   * Test the code you've just written. There should be five hrefs that
   * contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
    String[]  linkname_test = {"//try.jsoup.org/",
   "/apidocs/org/jsoup/select/Elements.html#html--",
   "/apidocs/index.html?org/jsoup/select/Elements.html",
   "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", 
   "http://github.com/jhy/jsoup/"};
    for (int i=0; i<linkname_test.length; i++){
        if (!result.contains(linkname_test[i])){
            print(linkname_test[i]);
            return false;
        }
    }
    return true;
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
