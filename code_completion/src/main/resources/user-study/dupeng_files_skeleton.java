import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   */
  static public List dfs(String dirname) {
    List result = new ArrayList();
    dfsHelp(result, dirname);
    return result;
  }
  
  static public void dfsHelp(List result, String dir){
      File directory = new File(dir);
      File [] listOfFiles = directory.listFiles();
       for (int i = 0; i < listOfFiles.length; i++){
          if (listOfFiles[i].isDirectory()) 
          {
              System.out.println(listOfFiles[i].getName());
             dfsHelp(result, listOfFiles[i].getAbsolutePath());
          }
        }
      for (int i = 0; i < listOfFiles.length; i++){
          if (listOfFiles[i].isFile()) 
          {
             
                result.add(listOfFiles[i].getName());
          }
        }
       
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    List trueResult = new ArrayList();
    trueResult.add("bar.txt");
    trueResult.add("foo.txt");
    trueResult.add("test11.txt");
    trueResult.add("foo1.txt");
    trueResult.add("foo2.txt");
    trueResult.add("test1.txt");
    trueResult.add("test2.txt");
    if(filenames.size() != trueResult.size()){
        return false;
    }
    else{
        for(String f : filenames){
            if(trueResult.contains(f)){
                trueResult.remove(f);
            }
        }
        return trueResult.size() == 0;
    }
  
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}