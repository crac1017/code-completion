public class Sieve {

  /**
   * TODO 1: Complete the following function. You could use our system
   * by replacing the contents in COMMENT and TEST.
   *
   * COMMENT:
   * sieve of eratosthenes algorithm for testing primality
   *
   * TEST:
   * // make this tests stronger
   * pliny_solution
   * print(sieve(1) == false && sieve(2) == true && sieve(3) == true
   * && sieve(4) == false && sieve(9) == false && sieve(17) == true
   * && sieve(23) == true && sieve(39) == false);
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    for (int 1_i = 2; 1_i <= n; _1_i_++) {
        primes[_1_i_] = true;
    }
    for (int 1_i = 2; 1_i <= n / 2; _1_i_++) {
        for (int 1_j = 2; 1_j <= n / 1_i_; 1_j_++) {
            primes[_1_i * 1_j_] = false;
        }
    }
      
    return primes[n];
  }

  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    int [] test = {1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 10, 27, 29, 73};
    boolean[] real = {false, true, true, false, true, false, false, 
        true, false, false, false, false, true, true};
    boolean[] my = new boolean[14];
    for(int i = 0; i < 14; i ++){
        my[i] = sieve(test[i]);
        if(my[i] != real[i]){
            return false;
        }
    }
    return true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}