import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {
  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2
   * 1,2,3
   * 2,3,4
   * 3,4,5
   */
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    int[][] mat1 = new int[n][n];
    int[][] mat2 = new int[n][n];

    // read a 3x3 matrix
    /**
     * COMMENT:
     * read csv file
     *
     * TEST:
     * // make the following test stronger
     * import java.io.File;
     * import java.util.*;
     * integer n = 3;
     * String filename = "static/data/mat1.csv";
     * integer[][] mat1 = new integer[n][n];
     * __pliny_solution__
     * boolean test = (mat1[0][0] == 1) && (mat1[0][1] == 2) && (mat1[0][2] == 3)
     * && (mat1[1][0] == 2) && (mat1[1][1] == 3) && (mat1[1][2] == 4)
     * && (mat1[2][0] == 3) && (mat1[2][1] == 4) && (mat1[2][2] == 5);
     * print(test);
     */

    File _0_f_ = new File(filename);
    Scanner _0_scanner_ = new Scanner(_0_f_);
    for (int _0_i_ = 0; _0_i_ < n; ++_0_i_) {
        String _0_line_ = _0_scanner_.nextLine();
        String[] _0_fields_ = _0_line_.split(",");
        for (int _0_j_ = 0; _0_j_ < n; ++_0_j_) {
            mat1[_0_i_][_0_j_] = Integer.parseInt(_0_fields_[_0_j_]);
        }
    }

    // square mat1 and store the results into mat2
    /**
     * COMMENT:
     * squaring matrix
     *
     * TEST:
     * // make the following test stronger
     * integer n = 3;
     * integer[][] mat1 = new integer[n][n];
     * integer[][] mat2 = new integer[n][n];
     * for(integer i = 0; i < n; ++i) {
     *   for(integer j = 0; j < n; ++j) {
     *     mat1[i][j] = i + j + 1;
     *   }
     * }
     * __pliny_solution__
     * boolean test = (mat2[0][0] == 14) && (mat2[0][1] == 20) && (mat2[0][2] == 26)
     * && (mat2[1][0] == 20) && (mat2[1][1] == 29) && (mat2[1][2] == 38)
     * && (mat2[2][0] == 26) && (mat2[2][1] == 38) && (mat2[2][2] == 50);
     * print(test);
     */
    for (int _0_i_ = 0; _0_i_ < n; ++_0_i_) {
        for (int _0_j_ = 0; _0_j_ < n; ++_0_j_) {
            int _0_s_ = 0;
            for (int _0_k_ = 0; _0_k_ < n; ++_0_k_) {
                _0_s_ += mat1[_0_i_][_0_k_] * mat1[_0_k_][_0_j_];
            }
            mat2[_0_i_][_0_j_] = _0_s_;
        }
    }
    return mat2;
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix.
   *
   * Return true if the code is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat2) {
    boolean test = (mat2[0][0] == 14) && (mat2[0][1] == 20) && (mat2[0][2] == 26)
      && (mat2[1][0] == 20) && (mat2[1][1] == 29) && (mat2[1][2] == 38)
      && (mat2[2][0] == 26) && (mat2[2][1] == 38) && (mat2[2][2] == 50);
    return test;
  }

  /**
   * Do not change this function
   */
  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
