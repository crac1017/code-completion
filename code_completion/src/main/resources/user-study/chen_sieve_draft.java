public class Sieve {

  /**
   * TODO 1: Complete the following function. You could use our system
   * by replacing the contents in COMMENT and TEST.
   *
   * COMMENT:
   * sieve of eratosthenes algorithm for testing primality
   *
   * TEST:
   * // make this tests stronger
   * __pliny_solution__
   * print(sieve(1) == false && sieve(59) == true && sieve(71) == true && sieve(97) == true && sieve(91) == false);
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    for (int _1_i_ = 2; _1_i_ <= n; _1_i_++) {
        primes[_1_i_] = true;
    }
    for (int _1_i_ = 2; _1_i_ <= n / 2; _1_i_++) {
        for (int _1_j_ = 2; _1_j_ <= n / _1_i_; _1_j_++) {
            primes[_1_i_ * _1_j_] = false;
        }
    }
      
    return primes[n];
  }

  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    
    if (sieve(1)) return false;
    if (sieve(2)) return true;
    if (sieve(3)) return true;
    if (sieve(4)) return false;
    if (sieve(5)) return true;
    if (sieve(6)) return false;
    if (sieve(7)) return true;
    if (sieve(8)) return false;
    if (sieve(9)) return false;
    if (sieve(10)) return false;
    if (sieve(27)) return false;
    if (sieve(29)) return true;
    if (sieve(73)) return true;
    
    return true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}