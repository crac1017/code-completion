import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {

  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2. The matrix in the CSV looks like this:
   * 1,2,3
   * 2,3,4
   * 3,4,5
   * 
   * 1 2 | 1 2 = (1*1 + 2*3) (1*1 + 2*4) 
   * 3 4 | 3 4   (3*1 + 4*3) (3*2 + 4*4) 
   */
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    int[][] mat1 = new int[n][n];
    int[][] mat2 = new int[n][n];

    // read a 3x3 matrix
    File f = new File(filename);
    Scanner sc = new Scanner(f);
    int row = 0;
    while(sc.hasNext()) {
        String s = sc.nextLine();
        String[] ele = s.split(",");
        mat1[row][0] = Integer.parseInt(ele[0]);
        mat1[row][1] = Integer.parseInt(ele[1]);
        mat1[row][2] = Integer.parseInt(ele[2]);
        row++;
    }

    // square mat1 and store the results into mat2
    for ( row = 0; row<3; row++) {
        for (int col = 0; col<3; col++) {
            int sum = 0;
            for (int i=0; i<3; i++) {
                sum += mat1[row][i] * mat1[i][col];
            }
            mat2[row][col] = sum;
        }
    }
    
    return mat2;
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix.
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat1) {
      
    /**
    14,20,26
    20,29,38
    26,38,50
    */
    
    int[][] result = { {14,20,26},
        {20,29,38},
        {26,38,50},
    };
    for(int i=0; i<3; i++) {
        for(int j =0 ; j<3; j++) {
            if (mat1[i][j]!=result[i][j]) return false;
        }
    }
    return true;
  }

  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}