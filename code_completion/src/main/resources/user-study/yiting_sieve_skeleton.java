public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    
    for (int i = 2; i < 100; i++) {
        primes[i] = true;
    }
    
    primes[0] = false;
    primes[1] = false;
    
    for (int i = 2; i < 100; i++) {
        int fold = 2;
        while (i*fold < 100) {
            if (primes[i*fold] == true)
                primes[i*fold] = false;
                
            fold++;
        }
    }
    
    return primes[n];
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
      int[] inputs = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 27, 29, 71};
      boolean[] results = {false, true, true, false, true, false, true, false, false, false, false, true, true};
      
      for (int i = 0; i < inputs.length; i++) {
          //System.out.println("i = " + i);
          if (sieve(inputs[i]) != results[i])
            return false;
      }
        
    return true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}