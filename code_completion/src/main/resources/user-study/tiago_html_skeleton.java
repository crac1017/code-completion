import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.*;

public class HTML {
    public static String readFile(String filename) {
        File file = new File(filename);
        BufferedInputStream bin = new BufferedInputStream(new FileInputStream(
                        file));
        byte[] buffer = new byte[(int) file.length()];
        bin.read(buffer);
        return new String(buffer);       
    }
  /**
   * TODO 1:
   * Get all hrefs from anchor tags where href has the input word
   * from the input html file. Please use jsoup as the HTML parsing library.
   */
  public static List get_links(String html_file, String word) throws IOException {
    List result = new ArrayList();
    Document doc = Jsoup.parse(readFile(html_file));
    Elements links = doc.select("a[href]");
    for (Element link : links) {
        String url = link.attr("href");
        if (url.indexOf(word) != -1) {
            result.add(url);
        }
    }
    return result;
  }

  /**
   * TODO 2:
   * Test the code you've just written. There should be five hrefs that
   * contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
      String[] expected = {
          "//try.jsoup.org/",
    "/apidocs/org/jsoup/select/Elements.html#html--",
    "/apidocs/index.html?org/jsoup/select/Elements.html",
    "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g",
    "http://github.com/jhy/jsoup/"
      };
    return Arrays.asList(expected).equals(result);
  }
 
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}