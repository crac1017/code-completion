public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    
    int total = 99;
    for (int i = 1; i <= total; i++) {
        primes[i] = true;
    }
    
    for (int factor = 2; factor * factor <= total; factor++) {
        if (primes[factor]) {
            for (int j = factor; factor * j <= total; j++) {
                primes[factor * j] = false;
            }
        }
    }
    
    return primes[n];
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    int[] primes = {1, 2, 3, 5, 7, 29, 73};
    for (int i : primes) {
        if (!sieve(i)) {
            return false;
        }
    }
    
    int[] composites = {4, 6, 6, 8, 9, 10, 27};
    for (int i : composites) {
        if (sieve(i)) {
            return false;
        }    
    }

    return true; 
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}