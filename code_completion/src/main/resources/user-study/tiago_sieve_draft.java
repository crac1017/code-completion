import java.util.*;

public class Sieve {

  /**
   * TODO 1: Complete the following function. You could use our system
   * by replacing the contents in COMMENT and TEST.
   *
   * COMMENT:
   * sieve of eratosthenes algorithm for testing primality
   *
   * TEST:
   * // make this tests stronger
   * __pliny_solution__
   * print(sieve(1) == false && sieve(2) == true && sieve(3) == true && sieve(4) == false);
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    for (int _1_i_ = 2; _1_i_ <= n; _1_i_++) {
        primes[_1_i_] = true;
    }
    for (int _1_i_ = 2; _1_i_ <= n / 2; _1_i_++) {
        for (int _1_j_ = 2; _1_j_ <= n / _1_i_; _1_j_++) {
            primes[_1_i_ * _1_j_] = false;
        }
    }
    return primes[n];
}

  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   *
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
        int[] all = {
   1,
   2,
   3,
   4,
   5,
   6,
   6,
   7,
   8,
   9,
10,
27,
29,
73
        };     
      Integer[] primes = {
        2,
        3,
        5,
    7,
   29,
   73
      };
      List expected = Arrays.asList(primes);
      for (int i : all) {
          if (sieve(i) && !expected.contains(i)) {
              return false;
          }
      }
    return true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}