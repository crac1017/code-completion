import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  /**
   * TODO 1: 
   * Get all hrefs from anchor tags where href has the input word
   * from the input html file. Please use jsoup as the HTML parsing library.
   */
  public static List get_links(String html_file, String word) throws IOException {
    List result = new ArrayList();
    
    String content;
    BufferedReader br = new BufferedReader(new FileReader(html_file));
    try {
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
            //System.out.println(line);
            sb.append(line);
            sb.append("\n");
            
            line = br.readLine();
        }
        content = sb.toString();
    } finally {
        br.close();
    }
    
    Document doc = Jsoup.parse(content);
    
    Elements links = doc.select("a[href]");
    
    for (Element link : links) {
        
        if (link.attr("href").toLowerCase().contains(word.toLowerCase()))
        {
            result.add(link.attr("href"));
            System.out.println(link.attr("href"));
        }
    }

    return result;
  }

  /**
   * TODO 2:
   * Test the code you've just written. There should be five hrefs that
   * contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
    if (!result.contains("//try.jsoup.org/")) return false;
    if (!result.contains("/apidocs/org/jsoup/select/Elements.html#html--")) return false;
    if (!result.contains("/apidocs/index.html?org/jsoup/select/Elements.html")) return false;
    if (!result.contains("//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g")) return false;
    if (!result.contains("http://github.com/jhy/jsoup/")) return false;
    return true;
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}