import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {

  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2. The matrix in the CSV looks like this:
   * 1,2,3
   * 2,3,4
   * 3,4,5
   */
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    int[][] mat1 = new int[n][n];
    int[][] mat2 = new int[n][n];

    Scanner csvIntput = new Scanner(new File(filename));

    // read a 3x3 matrix

    for (int row=0; row<n; row++) {
        String line = csvIntput.nextLine();
        String[] entries = line.split(",");
        for (int col=0; col<n; col++) {
            mat1[row][col] = Integer.parseInt(entries[col]);
        }
    }

    // square mat1 and store the results into mat2
    
    for (int i=0; i<n; i++) {
        for (int j=0; j<n; j++) {
            int sum = 0;
            for (int k=0; k<n; k++) {
                sum += mat1[i][k] * mat1[k][j];
            }
            mat2[i][j] = sum;
        }
    }
    
    return mat2;
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix.
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat1) {
    int[][] expected = {{14,20,26},{20,29,38},{26,38,50}};
    return java.util.Arrays.deepEquals(mat1,expected);
  }

  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
