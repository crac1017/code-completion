import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   *
   * COMMENT:
   * Get all file names in the directory
   *
   * TEST:
   * import java.io.File;
   * import java.util.List;
   * import java.util.ArrayList;
   * pliny_solution
   * List name_list = dfs("static/data/test_dir");
   * print(
   *  name_list.contains("bar.txt") &&
   *  name_list.contains("foo.txt") &&
   *  name_list.contains("test11.txt") &&
   *  name_list.contains("foo1.txt") &&
   *  name_list.contains("foo2.txt") &&
   *  name_list.contains("test1.txt") &&
   *  name_list.contains("test2.txt")
   * );
   */
  static public List dfs(String dirname) {
    List result = new ArrayList();
    File 0_dir_file = new File(dirname);
    File[] 0_file_list = _0_dir_file_.listFiles();
    for (int 0_i = 0; 0_i < 0_file_list_.length; ++_0_i) {
        File 0_f = _0_file_list_[_0_i_];
        if (_0_f_.isDirectory()) {
            List 0_new_files = dfs(_0_f_.getAbsolutePath());
            for (int 0_j = 0; 0_j < 0_new_files_.size(); ++_0_j) {
                result.add(_0_new_files_.get(_0_j_));
            }
        } else {
            result.add(_0_f_.getName());
        }
    }
    return result;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List name_list) {
    return (name_list.contains("bar.txt") &&
            name_list.contains("foo.txt") &&
            name_list.contains("test11.txt") &&
            name_list.contains("foo1.txt") &&
            name_list.contains("foo2.txt") &&
            name_list.contains("test1.txt") &&
            name_list.contains("test2.txt"));
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}