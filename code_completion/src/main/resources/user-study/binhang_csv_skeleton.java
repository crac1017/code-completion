import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {

  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2. The matrix in the CSV looks like this:
   * 1,2,3
   * 2,3,4
   * 3,4,5
   */
  
  public static int[][] multiplicar(int[][] A, int[][] B) {

        int aRows = A.length;
        int aColumns = A[0].length;
        int bRows = B.length;
        int bColumns = B[0].length;

        int[][] C = new int[aRows][bColumns];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                C[i][j] = 0;
            }
        }

        for (int i = 0; i < aRows; i++) { // aRow
            for (int j = 0; j < bColumns; j++) { // bColumn
                for (int k = 0; k < aColumns; k++) { // aColumn
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }

        return C;
    }
  
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    int[][] mat1 = new int[n][n];
    int[][] mat2 = new int[n][n];

    // read a 3x3 matrix
    Scanner scanner = new Scanner(new File(filename));
    int i = 0;
    while(scanner.hasNextLine()){
        String line = scanner.nextLine();
        String [] nums = line.split(",");
        for(int j=0;j<n;j++){
            mat1[i][j] = Integer.parseInt(nums[j]);
        }
        i++;
    }
    scanner.close();
    // square mat1 and store the results into mat2
    mat2 = multiplicar(mat1,mat1);
    return mat2;
    
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix.
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat2) {
    int[][] result = {{14,20,26},{20,29,38},{26,38,50}};
    if(result.length!=mat2.length){
        return false;
    }
    for(int i=0;i<mat2.length;i++){
        if(result[i].length!=mat2.length){
            return false;
        }
        for(int j=0;j<mat2[i].length;j++){
            if(mat2[i][j]!=result[i][j]){
                return false;
            }
        }
    }
    return true;
  }

  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}