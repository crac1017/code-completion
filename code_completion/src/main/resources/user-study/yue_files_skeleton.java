import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   */
  static public List dfs(String dirname) {
    List result = new ArrayList();
    File dir = new File(dirname);
    for (File f : dir.listFiles() ) {
        if (f.isDirectory()) {
         List new_result = dfs(f.getAbsolutePath());
         result.addAll(new_result);
        } else {
            result.add(f.getName());
        }
    }
    print(result);
    return result;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    List result = new ArrayList();
    result.add("bar.txt");
    result.add("foo.txt");
    result.add("test11.txt");
    result.add("foo1.txt");
    result.add("foo2.txt");
    result.add("test2.txt");
    result.add("test1.txt");
    for (int i = 0; i < filenames.size(); ++i) {
        if (filenames.get(i).equals(result.get(i))) {
            continue;
        }
        return false;
    }
    return true;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
