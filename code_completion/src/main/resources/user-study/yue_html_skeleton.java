import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {

private static String get_file_content(String file_loc) {
    BufferedReader br = new BufferedReader(new FileReader(file_loc));
    String everything = "";
    try {
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            line = br.readLine();
        }
        everything = sb.toString();
    } finally {
        br.close();
    }
    return everything;

}
  /**
   * TODO 1:
   * Get all hrefs from anchor tags where href has the input word
   * from the input html file. Please use jsoup as the HTML parsing library.
   */
  public static List get_links(String html_file, String word) throws IOException {
    List result = new ArrayList();

    String content = get_file_content(html_file);

    if (content.equals("")) {
        print("Error!");
    } else {
        Document doc = Jsoup.parse(content);

        Elements links = doc.getElementsByTag("a");

        for (Element link : links) {
          String linkHref = link.attr("href");
          if (linkHref.contains(word)) {
              result.add(linkHref);
          }
        }

    }
    print(result);
    return result;
  }

  /**
   * TODO 2:
   * Test the code you've just written. There should be five hrefs that
   * contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
    List ans = new ArrayList();
    ans.add("//try.jsoup.org/");
    ans.add("/apidocs/org/jsoup/select/Elements.html#html--");
    ans.add("/apidocs/index.html?org/jsoup/select/Elements.html");
    ans.add("//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g");
    ans.add("http://github.com/jhy/jsoup/");
    if (result.size() != ans.size()) {
        return false;
    }

    for (int i = 0; i < result.size(); ++i) {
        if (result.get(i).equals(ans.get(i))) {
            continue;
        }
        return false;
    }
    return true;
  }

  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
