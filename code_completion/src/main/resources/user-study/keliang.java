//// Test 1

import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   */
  static public List dfs(String dirname) {
    File f = new File(dirname);
    return dfs_file(f);
  }
  
  static public List dfs_file(File f){
    List output = new ArrayList();
    if (!f.isDirectory()){
        output.add(f.getName());
        return output;
    }
    File[] listOfFiles = f.listFiles();
    for (int i=0; i<listOfFiles.length; i++){
        List temp = dfs_file(listOfFiles[i]);
        output.addAll(temp);
    }
    return output;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    String[]  filenames_test = {"bar.txt",
    "foo.txt",
    "test11.txt",
    "foo1.txt",
    "foo2.txt",
    "test1.txt",
    "test2.txt"};
    for (int i=0; i<filenames_test.length; i++){
        if (!filenames.contains(filenames_test[i])){
            print(filenames_test[i]);
            return false;
        }
    }
    return true;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}


/// Test 2

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  /**
   * TODO 1: 
   * Get all hrefs from anchor tags where href has the input word
   * from the input html file. Please use jsoup as the HTML parsing library.
   */
  public static List get_links(String html_file, String word) throws IOException {
    List result = new ArrayList();
    String html;
    
    // Read file into string
    BufferedReader br = new BufferedReader(new FileReader(html_file));
    try {
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
            sb.append(line);
            sb.append("\n");
            line = br.readLine();
        }
        html = sb.toString();
    } finally {
        br.close();
    }
    
    Document doc = Jsoup.parse(html);
    Elements links = doc.select("a[href]");
    for (Element link : links){
        String ref = link.attr("href");
        if (ref.contains(word)){
            result.add(ref);
        }
    }
    return result;
  }

  /**
   * TODO 2:
   * Test the code you've just written. There should be five hrefs that
   * contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
    String[]  linkname_test = {"//try.jsoup.org/",
   "/apidocs/org/jsoup/select/Elements.html#html--",
   "/apidocs/index.html?org/jsoup/select/Elements.html",
   "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", 
   "http://github.com/jhy/jsoup/"};
    for (int i=0; i<linkname_test.length; i++){
        if (!result.contains(linkname_test[i])){
            print(linkname_test[i]);
            return false;
        }
    }
    return true;
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}


/// Test 3
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CSV {
  /**
   * TODO 1: Complete the following function. It should read a 3x3 matrix
   * from a CSV file into mat1, square mat1 and store the resulting
   * matrix into mat2
   * 1,2,3
   * 2,3,4
   * 3,4,5
   */
  static public int[][] csv_mat_mul(String filename) throws FileNotFoundException {
    int n = 3;
    int[][] mat1 = new int[n][n];
    int[][] mat2 = new int[n][n];

    // read a 3x3 matrix
    /**
     * COMMENT:
     * read a csv file into a matrix
     *
     * TEST:
     * // make the following test stronger
     * import java.io.File;
     * import java.util.*;
     * integer n = 3;
     * String filename = "static/data/mat1.csv";
     * integer[][] mat1 = new integer[n][n];
     * __pliny_solution__
     * boolean result = true;
     * for (integer i=0; i<3; i++){
         for (integer j=0; j<3; j++){
            if (mat1[i][j]!=i+j+1){
                print(false);
                exit();
            }
         }
        }
     * print(true);
     */
    File _0_f_ = new File(filename);
    Scanner _0_scanner_ = new Scanner(_0_f_);
    for (int _0_i_ = 0; _0_i_ < n; ++_0_i_) {
        String _0_line_ = _0_scanner_.nextLine();
        String[] _0_fields_ = _0_line_.split(",");
        for (int _0_j_ = 0; _0_j_ < n; ++_0_j_) {
            mat1[_0_i_][_0_j_] = Integer.parseInt(_0_fields_[_0_j_]);
        }
    }

    // square mat1 and store the results into mat2
    /**
     * COMMENT:
     * squaring a matrix
     *
     * TEST:
     * // make the following test stronger
     * integer n = 3;
     * integer[][] mat1 = new integer[n][n];
     * integer[][] mat2 = new integer[n][n];
     * for(integer i = 0; i < n; ++i) {
     *   for(integer j = 0; j < n; ++j) {
     *     mat1[i][j] = i + j + 1;
     *   }
     * }
     * __pliny_solution__
     * // print(mat2[0][0]==14 && mat2[0][1]==20 && mat2[1][0]==20 && mat2[0][2]==26 && mat2[2][0]==26 && mat2[1][1]==29 && mat2[1][2]==38 && mat2[2][1]==38 && mat2[2][2]==50);
     * print(mat2[0][0]==14);
     */
    for (int _0_i_ = 0; _0_i_ < n; ++_0_i_) {
        for (int _0_j_ = 0; _0_j_ < n; ++_0_j_) {
            int _0_s_ = 0;
            for (int _0_k_ = 0; _0_k_ < n; ++_0_k_) {
                _0_s_ += mat1[_0_i_][_0_k_] * mat1[_0_k_][_0_j_];
            }
            mat2[_0_i_][_0_j_] = _0_s_;
        }
    }
    return mat2;
  }

  /**
   * TODO 2:
   * Test the code you've just written. Make sure to test every
   * element from the matrix. 
   *
   * Return true if the code is correct. Otherwise, return false.
   */
  static public boolean test(int[][] mat2) {
    return (mat2[0][0]==14 && mat2[0][1]==20 && mat2[1][0]==20 && mat2[0][2]==26 && mat2[2][0]==26 && mat2[1][1]==29 && mat2[1][2]==38 && mat2[2][1]==38 && mat2[2][2]==50);
  }

  /**
   * Do not change this function
   */
  static public void main(String[] args) throws FileNotFoundException {
    int[][] result = csv_mat_mul("static/data/mat1.csv");

    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
