import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   */
  static public List dfs(String dirname) {
    List result = new ArrayList();
    recDFS(dirname, result);
    return result;
  }
  static public void recDFS(String dirname, List result){
    File folder = new File(dirname);
    File[] listOfFiles = folder.listFiles();

    for (int i = 0; i < listOfFiles.length; i++) {
      if (listOfFiles[i].isFile()) {
        result.add(listOfFiles[i].getName());
      } else if (listOfFiles[i].isDirectory()) {
        recDFS(listOfFiles[i].getAbsolutePath(),  result);
      }
    }
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    String[] files = {"bar.txt","foo.txt","test11.txt","foo1.txt","foo2.txt","test1.txt","test2.txt"};
    Set set = new HashSet(Arrays.asList(files));
    for(String str:filenames){
        if(set.contains(str)){
            set.remove(str);
        }
        else
            return false;
    }
    return true;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}