import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  
  /**
   * TODO 1:
   * Complete the following function which parse an html file and then
   * put all the hrefs in anchor tags which contains a given word into
   * a list. There should be five hrefs that contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   */
  static public List get_links(String html_file, String word) {
    List result = new ArrayList();
    String content;

    /**
     * COMMENT:
     * read text file
     *
     * TEST:
     * String html_file = "static/data/jsoup.html";
     * String content;
     * pliny_solution
     * print(true && content.contains("body") && content.contains("html"));
     */
    BufferedReader 0_reader = new BufferedReader(new FileReader(html_file));
    String 0_line = null;
    StringBuilder 0_stringBuilder = new StringBuilder();
    String 0_ls = System.getProperty("line.separator");
    while ((0_line = _0_reader_.readLine()) != null) {
        _0_stringBuilder_.append(_0_line_);
        _0_stringBuilder_.append(_0_ls_);
    }
    content = _0_stringBuilder_.toString();

    /**
     * COMMENT:
     * get links from html
     *
     * TEST:
     * addClassPath("static/data/jsoup-1.10.2.jar");
     * import org.jsoup.Jsoup;
     * import org.jsoup.nodes.Document;
     * import org.jsoup.nodes.Element;
     * import org.jsoup.select.Elements;
     * import java.util.regex.Matcher;
     * import java.util.regex.Pattern;
     * String content = "<html><body><a href='http://www.ebay.com/index.php'>...</a></body></html>";
     * String word = "ebay";
     * List result = new ArrayList();
     * pliny_solution
     * print(true && result.contains("http://www.ebay.com/index.php"));
     */
    Document 0_doc = Jsoup.parse(content);
    Elements 0_links = _0_doc_.select("a[href]");
    Pattern 0_pattern = Pattern.compile(word);
    for (Element 0_link : 0_links) {
        String 0_l = _0_link_.attr("href");
        Matcher 0_m = _0_pattern_.matcher(_0_l_);
        if (_0_m_.find()) {
            result.add(_0_l_);
        }
    }

    return result;
  }
  
  /**
   * TODO 2:
   * Test the code you've just written. There are five hrefs that
   * contains the word "jsoup".
   *
   * Return true if the binary search is correct. Otherwise, return false.
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   */
  static public boolean test(List result) {
     List target = new ArrayList();
     target.add("//try.jsoup.org/");
     target.add("/apidocs/org/jsoup/select/Elements.html#html--");
     target.add("/apidocs/index.html?org/jsoup/select/Elements.html");
     target.add("//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g");
     target.add("http://github.com/jhy/jsoup/");
    return result.containsAll(target);
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }

}