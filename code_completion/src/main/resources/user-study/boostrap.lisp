#!/usr/local/bin/sbcl --script
(load "~/quicklisp/setup.lisp")
(quicklisp:quickload "cl-csv")
(quicklisp:quickload "alexandria")
(defstruct (sub) task author time useful)

(defun read-data (filename)
  (cl-csv:read-csv filename
                   :map-fn #'(lambda (row)
                               (make-sub
                                :author (nth 0 row)
                                :time (parse-integer (nth 2 row))
                                :task (nth 1 row)
                                :useful (nth 4 row)))))

(defun sample-data (data)
  (let* ((task-names
          (remove-duplicates (mapcar #'(lambda (sub) (sub-task sub)) data)
                             :test #'equal))
         (task-groups
          (mapcar #'(lambda (name)
                      (remove-if #'(lambda (d)
                                     (not (equal (sub-task d)
                                                 name)))
                                 data))
                  task-names))
         (sampled-groups
          (mapcar #'(lambda (group)
                      (mapcar #'(lambda (i)
                                  (declare (ignore i))
                                  (alexandria:random-elt group))
                              (alexandria:iota (length group))))
                  task-groups)))
    (reduce #'append sampled-groups)))

(defun get-mean (data task-name)
  (let* ((subs
          (remove-if #'(lambda (s)
                         (not (equal (sub-task s) task-name)))
                     data))
         (time-seq
          (mapcar #'(lambda (s) (sub-time s)) subs)))
    ; (format t "task-name : ~A~% ~A~%" task-name subs)
    (float (alexandria:mean time-seq))))

(defun test-null (data t1 t2)
  (let ((m1 (get-mean data t1))
        (m2 (get-mean data t2)))
    (>= m1 m2)))

(defun run (t1 t2 total-attempt)
  (format t "Testing ~A and ~A ~A times~%" t1 t2 total-attempt)
  (let* ((orig-data (read-data #P"data.csv"))
         (reject-seq (mapcar #'(lambda (i)
                                 (declare (ignore i))
                                 (test-null (sample-data orig-data)
                                            t1
                                            t2))
                             (alexandria:iota total-attempt)))
         (true-count (count t reject-seq)))
    (format t "True count: ~A~%" true-count)
    (float (/ true-count total-attempt))))


(defparameter *data* (read-data #P"data.csv"))
(defparameter *d1* (sample-data *data*))

(format t "~A~%" (run "sieve_draft" "sieve_skeleton" 100000))
(format t "sieve skeleton mean : ~A~%" (get-mean *data* "sieve_skeleton"))
(format t "sieve draft    mean : ~A~%" (get-mean *data* "sieve_draft"))
(format t "~A~%" (run "files_draft" "files_skeleton" 100000))
(format t "files skeleton mean : ~A~%" (get-mean *data* "files_skeleton"))
(format t "files draft    mean : ~A~%" (get-mean *data* "files_draft"))
(format t "~A~%" (run "html_draft" "html_skeleton" 100000))
(format t "html skeleton mean : ~A~%" (get-mean *data* "html_skeleton"))
(format t "html draft    mean : ~A~%" (get-mean *data* "html_draft"))
(format t "~A~%" (run "csv_draft" "csv_skeleton" 100000))
(format t "csv skeleton mean : ~A~%" (get-mean *data* "csv_skeleton"))
(format t "csv draft    mean : ~A~%" (get-mean *data* "csv_draft"))
