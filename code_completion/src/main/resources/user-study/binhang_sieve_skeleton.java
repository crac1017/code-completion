public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    if(n==1){
        return false;
    }
    boolean[] primes = new boolean[n+1];
    Arrays.fill(primes, true);
    int i = 2;
    while(i<=n){
        int j = 2;
        while(i*j<=n){
            if(i*j==n){
                return false;
            }
            primes[i*j] = false;
            j++;
        }
        i++;
        while(i<n && !primes[i]){
            i++;
        }
    }
    return true;
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    if(!sieve(1)==false){
        System.out.print(1);
        return false;
    }
    if(!sieve(2)==true){
        System.out.print(2);
        return false;
    }
    if(!sieve(3)==true){
        System.out.print(3);
        return false;
    }
    if(!sieve(4)==false){
        System.out.print(4);
        return false;
    }
    if(!sieve(5)==true){
        System.out.print(5);
        return false;
    }
    if(!sieve(6)==false){
        System.out.print(6);
        return false;
    }
    if(!sieve(7)==true){
        System.out.print(7);
        return false;
    }
    if(!sieve(8)==false){
        System.out.print(8);
        return false;
    }
    if(!sieve(9)==false){
        System.out.print(9);
        return false;
    }
    if(!sieve(10)==false){
        System.out.print(10);
        return false;
    }
    if(!sieve(27)==false){
        System.out.print(27);
        return false;
    }
    if(!sieve(29)==true){
        System.out.print(29);
        return false;
    }
    if(!sieve(73)==true){
        System.out.print(73);
        return false;
    }
    return true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}