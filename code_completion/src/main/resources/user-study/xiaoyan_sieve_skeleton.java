public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    primes[0]=false;
    primes[1]=false;
    for(int i = 2;i<100;i++)
        primes[i] = true;
    for(int i=2;i<100;i++){
        if(primes[i]){
            int mul = 2;
            while(i * mul < 100){
                primes[i*mul] = false;
                mul++;
            }
        }
    }
    return primes[n];
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    int[] testArray = {1,2,3,4,5,6,6,7,8,9,10,27,29,73};
    boolean[] results = {false,true,true,false,true,false,false,true,false,false,false,false,true,true};
    for(int i=0;i<14;i++)
        if(sieve(testArray[i])!=results[i]){
            print(testArray[i]);
            return false;
        }
    return true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}