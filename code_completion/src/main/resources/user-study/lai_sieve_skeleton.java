

public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    
    for (int i = 0; i < 100; i++)
      primes[i] = true;
    primes[1] = false;
    
    for (int i = 2; i < 10; i++)
      if (primes[i])
        for (int j = i; i*j < 100; j++)
          primes[i*j] = false;
    
    return primes[n];
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    int[] n = {         1,      2,      3,      4,      5,      6,      6,
                        7,      8,      9,      10,     27,     29,     73};
    boolean[] result = {false,  true,   true,   false,  true,   false,  false,
                        true,   false,  false,  false,  false,  true,   true};
                        
    for (int i = 0; i < n.length; i++)
      if (sieve(n[i]) != result[i]) {
         // print(i);
          return false;
      }
                        
    return true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}