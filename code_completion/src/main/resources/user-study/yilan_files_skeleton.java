import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   */
  static public List dfs(String dirname) {
    List result = new ArrayList();
    File f = new File(dirname);
    if (!f.isDirectory()) {
      result.add(f.getName());
    } else {
      for (File subFile: f.listFiles()) {
        result.addAll(dfs(subFile.getAbsolutePath()));
      }
    }
    return result;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    List expected = new ArrayList();
    expected.add("bar.txt");
    expected.add("foo.txt");
    expected.add("test11.txt");
    expected.add("foo1.txt");
    expected.add("foo2.txt");
    expected.add("test1.txt");
    expected.add("test2.txt");
    if (filenames.size() != expected.size()) {
      return false;    
    }
    
    for (String expectedFileName : expected) {
      if (!filenames.contains(expectedFileName)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}