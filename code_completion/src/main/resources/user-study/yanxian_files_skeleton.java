import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory. 
   * "static/data/test_dir" looks like this:
   *
   * ├── 1
   * │   ├── 0
   * │   │   └── bar.txt
   * │   └── foo.txt
   * └── 2
   *     ├── 1
   *     │   └── test11.txt
   *     ├── 2
   *     │   ├── foo1.txt
   *     │   └── foo2.txt
   *     ├── test1.txt
   *     └── test2.txt
   */
  static List result = new ArrayList();
  static public List dfs(String dirname) {
    File file = new File(dirname);
    File[] listFile = file.listFiles();
    if (listFile != null) {
        for (int i = 0; i < listFile.length; i++) {
            if (listFile[i].isDirectory()) {
                dfs(listFile[i].getAbsolutePath());
            } else {
                result.add(listFile[i].getName());
            }
        }
    }
    return result;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < filenames.size(); i++) {
        sb.append(filenames.get(i)).append(" ");
    }
    return sb.toString().equals("bar.txt foo.txt test11.txt foo1.txt foo2.txt test2.txt test1.txt ");
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}