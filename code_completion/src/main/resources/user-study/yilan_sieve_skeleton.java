public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    primes [0] = false;
    primes [1] = false;
    for (int i = 2; i < 100; i++) {
      primes[i] = true;
    }
    for (int i = 2; i < 100; i++) {
      for (int j = 2; j < 100; j++) {
          if (i * j < 100) {
              primes[i * j] = false;
          } else {
              break;
          }
      }
    }
    return primes[n];
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    int [] primes = {2, 3, 5, 7, 29, 73};
    int [] nonPrimes = {1, 4, 6, 8, 9, 10, 27};
    for (int i = 0; i < primes.length; i++) {
      if (!sieve(primes[i])) {
        return false;
      }
    }
    for (int i = 0; i < nonPrimes.length; i++) {
      if (sieve(nonPrimes[i])) {
        return false;
      }
    }
    return true;
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}