import csv;
import sys;

def read_data(filename):
    result = {};
    with open(filename, "rb") as infile:
        reader = csv.reader(infile);
        header = reader.next();

        for fields in reader:
