import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  
  /**
   * TODO 1:
   * Complete the following function which parse an html file and then
   * put all the hrefs in anchor tags which contains a given word into
   * a list. There should be five hrefs that contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   */
  static public List get_links(String html_file, String word) {
    List result = new ArrayList();
    String content;

    /**
     * COMMENT:
     * read text file
     *
     * TEST:
     * String html_file = "static/data/jsoup.html";
     * String content;
     * __pliny_solution__
     * print(content.contains("html") && content.contains("body") && content.contains("doctype"));
     */
    BufferedReader _0_reader_ = new BufferedReader(new FileReader(html_file));
    String _0_line_ = null;
    StringBuilder _0_stringBuilder_ = new StringBuilder();
    String _0_ls_ = System.getProperty("line.separator");
    while ((_0_line_ = _0_reader_.readLine()) != null) {
        _0_stringBuilder_.append(_0_line_);
        _0_stringBuilder_.append(_0_ls_);
    }
    content = _0_stringBuilder_.toString();

    /**
     * COMMENT:
     * get links from html
     *
     * TEST:
     * addClassPath("static/data/jsoup-1.10.2.jar");
     * import org.jsoup.Jsoup;
     * import org.jsoup.nodes.Document;
     * import org.jsoup.nodes.Element;
     * import org.jsoup.select.Elements;
     * import java.util.regex.Matcher;
     * import java.util.regex.Pattern;
     * String content = "<html><body><a href=\"www.google.com\">google</a></body></html>";
     * String word = "google";
     * List result = new ArrayList();
     * __pliny_solution__
     * print(result.size() == 1 && result.get(0).equals("www.google.com"));
     */
    Document _0_doc_ = Jsoup.parse(content);
    Elements _0_links_ = _0_doc_.select("a[href]");
    Pattern _0_pattern_ = Pattern.compile(word);
    for (Element _0_link_ : _0_links_) {
        String _0_l_ = _0_link_.attr("href");
        Matcher _0_m_ = _0_pattern_.matcher(_0_l_);
        if (_0_m_.find()) {
            result.add(_0_l_);
        }
    }

    return result;
  }
  
  /**
   * TODO 2:
   * Test the code you've just written. There are five hrefs that
   * contains the word "jsoup".
   *
   * Return true if the binary search is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
      String[] links = {"//try.jsoup.org/", "/apidocs/org/jsoup/select/Elements.html#html--", "/apidocs/index.html?org/jsoup/select/Elements.html", "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", "http://github.com/jhy/jsoup/"};
      if (result.size() != links.length)
        return false;
    
      for (int i = 0; i < links.length; i++) {
          if (!result.contains(links[i]))
            return false;
      }
      
      return true;
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }

}