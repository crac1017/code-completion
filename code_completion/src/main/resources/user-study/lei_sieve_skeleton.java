public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];
    primes[0] = true; 
    primes[1] = true; 
    int pivot = 2; 
    while (pivot < 99) {
        // sift 
        for (int i = pivot + 1; i < 100; i++) {
            if (i % pivot == 0) {
                primes[i] = true; 
            }
        }
        // update pivot
        while (pivot < 99) {
            if (!primes[++pivot]) break; 
        }
    }
    return primes[n];
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    int[] nums =     {1,   2,    4,   5,    6,   7,    8,   9,   10,  27,  29,   73};
    boolean[] tags = {true,false,true,false,true,false,true,true,true,true,false,false};
    for (int i = 0; i < nums.length; i++) {
        if(sieve(nums[i]) != tags[i]) {
            return false;
        } 
    }
    return true; 
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
