public class Sieve {

  /**
   * TODO 1:
   * Use Sieve of eratosthenes to test primality of the given
   * integer. 
   */
  static boolean sieve(int n) {
    boolean[] primes = new boolean[100];

    primes[1] = true;
    for (int i=2; i*i<100; i++) {
        for (int j=i*2; j<100; j+=i) {
            primes[j] = true;
        }
    }
    
    return !primes[n];
  }
  
  /**
   * TODO 2:
   * Test the sieve of eratosthenes you've just written. Make sure to test the
   * program with the following inputs:
   * 
   * n: 1
   * n: 2
   * n: 3
   * n: 4
   * n: 5
   * n: 6
   * n: 6
   * n: 7
   * n: 8
   * n: 9
   * n: 10
   * n: 27
   * n: 29
   * n: 73
   *
   * Return true if the program is correct. Otherwise, return false.
   */
  static public boolean test() {
    return sieve(2) && sieve(3) && sieve(5) && !sieve(6) && sieve(7) && !sieve(8) && !sieve(9) && !sieve(10)  && !sieve(27)  && sieve(29)  && sieve(73);
  }


  /**
   * Don't modify this function
   */
  static public void main(String[] args) {
    if(test()) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}