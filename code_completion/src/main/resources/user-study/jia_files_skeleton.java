import java.io.File; 

public class Files { 
/** 
* TODO 1: 
* Complete the following function. This function should recursively 
* colelct all the filenames under the given directory. 
* "static/data/test_dir" looks like this: 
* 
* ├── 1 
* │   ├── 0 
* │   │   └── bar.txt 
* │   └── foo.txt 
* └── 2 
* ├── 1 
* │   └── test11.txt 
* ├── 2 
* │   ├── foo1.txt 
* │   └── foo2.txt 
* ├── test1.txt 
* └── test2.txt 
*/ 
static public void dfs1(String curFolderName, File[] 
curListOfFiles, List result) { 
int numFiles = curListOfFiles.length; 
print(numFiles); 
for (int i = 0; i < numFiles; i++) { 
File curFolder = curListOfFiles[i]; 
print("iteration-"+i); 
print(curFolder.getName()); 
if(curFolder.isFile() == false) { 
File[] myChildren = curFolder.listFiles(); 
dfs1(curListOfFiles[i].getName(), myChildren, result); 
} 
else { 
print(curFolder.getName()); 
result.add(curFolder.getName()); 
} 
} 

//result.add(curFolderName); 

} 

static public List dfs(String dirname) { 
List result = new ArrayList(); 
File folder = new File(dirname); 
File[] curListOfFiles; 
curListOfFiles = folder.listFiles(); 
print(curListOfFiles.length); 
print(curListOfFiles); 
dfs1 (dirname, curListOfFiles, result); 
return result; 
} 

/** 
* TODO 2: 
* Test the function you've just written. These are the filenames 
* under "static/data/test_dir": 
* "bar.txt" 
* "foo.txt" 
* "test11.txt" 
* "foo1.txt" 
* "foo2.txt" 
* "test1.txt" 
* "test2.txt" 
*/ 
public static boolean test(List filenames) { 
return filenames.contains("bar.txt") && filenames.contains("foo.txt") && 
filenames.contains("test11.txt") && filenames.contains("foo1.txt") 
&& filenames.contains("foo2.txt") && 
filenames.contains("test1.txt") && filenames.contains("test2.txt"); 


} 

/** 
* Do not change this function 
*/ 
public static void main(String[] args) { 
List result = dfs("static/data/test_dir"); 
if(test(result)) { 
print("PASS!"); 
} else { 
print("FAIL!"); 
} 
} 
}