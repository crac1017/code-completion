import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   */
  static public List dfs(String dirname) {
    File f = new File(dirname);
    return dfs_file(f);
  }
  
  static public List dfs_file(File f){
    List output = new ArrayList();
    if (!f.isDirectory()){
        output.add(f.getName());
        return output;
    }
    File[] listOfFiles = f.listFiles();
    for (int i=0; i<listOfFiles.length; i++){
        List temp = dfs_file(listOfFiles[i]);
        output.addAll(temp);
    }
    return output;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
    String[]  filenames_test = {"bar.txt",
    "foo.txt",
    "test11.txt",
    "foo1.txt",
    "foo2.txt",
    "test1.txt",
    "test2.txt"};
    for (int i=0; i<filenames_test.length; i++){
        if (!filenames.contains(filenames_test[i])){
            print(filenames_test[i]);
            return false;
        }
    }
    return true;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}
