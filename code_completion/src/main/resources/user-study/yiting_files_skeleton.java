import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   */
  static public List dfs(String dirname) {
    List result = new ArrayList();

    File[] files = new File(dirname).listFiles();
    
    List folders = new ArrayList();

    for (File file : files) {
        if (file.isFile()) {
            result.add(file.getName());
        }
        else {
            folders.add(file);
        }
    }
    
    while (!folders.isEmpty()) {
        List nextFolders = new ArrayList();
        
        for (int i = 0; i < folders.size(); i++) {
            File curnt = folders.get(i);
            
            File[] files = curnt.listFiles();
            for (File file : files) {
                print(file.getName());
                if (file.isFile()) {
                    result.add(file.getName());
                }
                else {
                    nextFolders.add(file);
                }
            }
        }
        
        folders = nextFolders;
    }
    
    return result;
  }

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
      String[] results = {"bar.txt", "foo.txt", "test11.txt", "foo1.txt", "foo2.txt", "test1.txt", "test2.txt"};
      
      //print(filenames.size());
      if (filenames.size() != results.length)
        return false;
      
      for (int i = 0; i < results.length; i++) {
          System.out.println("case " + i);
          if (!filenames.contains(results[i]))
            return false;
      }
      
    return true;
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}