import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTML {
  
  /**
   * TODO 1:
   * Complete the following function which parse an html file and then
   * put all the hrefs in anchor tags which contains a given word into
   * a list. There should be five hrefs that contains the word "jsoup":
   *
   * "//try.jsoup.org/",
   * "/apidocs/org/jsoup/select/Elements.html#html--",
   * "/apidocs/index.html?org/jsoup/select/Elements.html",
   * "//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g", and
   * "http://github.com/jhy/jsoup/".
   */
  static public List get_links(String html_file, String word) {
    List result = new ArrayList();
    String content;

    /**
     * COMMENT:
     * read text file
     *
     * TEST:
     * String html_file = "static/data/jsoup.html";
     * String content;
     * pliny_solution
     * print(content.startsWith("<!DOCTYPE") || content.startsWith("<!doctype"));
     */
    BufferedReader 1_reader = new BufferedReader(new FileReader(html_file));
    String 1_line = null;
    StringBuilder 1_stringBuilder = new StringBuilder();
    String 1_ls = System.getProperty("line.separator");
    while ((1_line = _1_reader_.readLine()) != null) {
        _1_stringBuilder_.append(_1_line_);
        _1_stringBuilder_.append(_1_ls_);
    }
    content = _1_stringBuilder_.toString();
    
    /**
     * COMMENT:
     * get links from html
     *
     * TEST:
     * addClassPath("static/data/jsoup-1.10.2.jar");
     * import org.jsoup.Jsoup;
     * import org.jsoup.nodes.Document;
     * import org.jsoup.nodes.Element;
     * import org.jsoup.select.Elements;
     * import java.util.regex.Matcher;
     * import java.util.regex.Pattern;
     * String content = "<!DOCTYPE html><html><body><a href=\"www.google.com\">Hello</a></body></html>";
     * String word = "google";
     * List result = new ArrayList();
     * pliny_solution
     * print(result.contains("www.google.com"));
     */
    Document 0_doc = Jsoup.parse(content);
    Elements 0_links = _0_doc_.select("a[href]");
    Pattern 0_pattern = Pattern.compile(word);
    for (Element 0_link : 0_links) {
        String 0_l = _0_link_.attr("href");
        Matcher 0_m = _0_pattern_.matcher(_0_l_);
        if (_0_m_.find()) {
            result.add(_0_l_);
        }
    }    

    return result;
  }
  
  /**
   * TODO 2:
   * Test the code you've just written. There are five hrefs that
   * contains the word "jsoup".
   *
   * Return true if the binary search is correct. Otherwise, return false.
   */
  static public boolean test(List result) {
    return (result.contains("//try.jsoup.org/") &&
    result.contains("/apidocs/org/jsoup/select/Elements.html#html--") &&
    result.contains("/apidocs/index.html?org/jsoup/select/Elements.html") &&
    result.contains("//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g") &&
    result.contains("http://github.com/jhy/jsoup/"));
  }
  
  /**
   * Do not change this function
   */
  static public void main(String[] args) throws IOException {
    addClassPath("static/data/jsoup-1.10.2.jar");
    List links = get_links("static/data/jsoup.html", "jsoup");
    if(test(links)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }

}