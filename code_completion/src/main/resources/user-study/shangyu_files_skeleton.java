import java.io.File;

public class Files {
  /**
   * TODO 1:
   * Complete the following function. This function should recursively
   * colelct all the filenames under the given directory.
   */
  static public List dfs(String dirname) {
     List result = new ArrayList();
    
    File directory = new File(dirname);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList){
            if (file.isFile()){
                result.add(file.getName());
                //System.out.println(file.getName());
               // System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()){
                result.addAll(dfs(file.getAbsolutePath()));
            }
        }
    
    return result;
  }
  
  

  /**
   * TODO 2:
   * Test the function you've just written. These are the filenames
   * under "static/data/test_dir":
   * "bar.txt"
   * "foo.txt"
   * "test11.txt"
   * "foo1.txt"
   * "foo2.txt"
   * "test1.txt"
   * "test2.txt"
   */
  public static boolean test(List filenames) {
      //String fileName = "bar.txt";
      //List t = new ArrayList();
      //t.add(fileName);
      //return t.contains(fileName);
    //return filenames.contains(fileName);
    //System.out.println(filenames.get(0));
    //List places = Arrays.asList("Buenos Aires", "Córdoba", "La Plata");
    //System.out.println(places.get(0));
    return filenames.contains("bar.txt") && filenames.contains("foo.txt") &&
            filenames.contains("test11.txt") && filenames.contains("foo1.txt") && filenames.contains("foo2.txt") && filenames.contains("test1.txt") && filenames.contains("test2.txt");
  }

  /**
   * Do not change this function
   */
  public static void main(String[] args) {
    List result = dfs("static/data/test_dir");
    if(test(result)) {
      print("PASS!");
    } else {
      print("FAIL!");
    }
  }
}

