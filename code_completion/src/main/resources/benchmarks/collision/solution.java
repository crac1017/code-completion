import net.smert.jreactphysics3d.mathematics.Vector3;
import net.smert.jreactphysics3d.collision.shapes.AABB;
public class AABB2 {

  /*
  // Maximum world coordinates of the AABB on the x,y and z axis
  public Vector3 mMaxCoordinates;

  // Minimum world coordinates of the AABB on the x,y and z axis
  public Vector3 mMinCoordinates;

  // Constructor
  public AABB() {
    mMaxCoordinates = new Vector3();
    mMinCoordinates = new Vector3();
  }

  // Constructor
  public AABB(Vector3 minCoordinates, Vector3 maxCoordinates) {
    mMaxCoordinates = maxCoordinates;
    mMinCoordinates = minCoordinates;
  }

  // Return the center point of the AABB in world coordinates
  public Vector3 getCenter() {
    return new Vector3(mMinCoordinates).add(mMaxCoordinates).multiply(0.5f);
  }

  // Return the maximum coordinates of the AABB
  public Vector3 getMax() {
    return mMaxCoordinates;
  }

  // Set the maximum coordinates of the AABB
  public void setMax(Vector3 max) {
    mMaxCoordinates.set(max);
  }

  // Return the minimum coordinates of the AABB
  public Vector3 getMin() {
    return mMinCoordinates;
  }

  // Set the minimum coordinates of the AABB
  public void setMin(Vector3 min) {
    mMinCoordinates.set(min);
  }
  */

  /**
   * COMMENT:
   * Testing whether two AABB objects collide
   *
   * TEST:
   * public class Vector3 {
   *   float x;
   *   float y;
   *   float z;
   *
   *   // Constructor
   *   public Vector3() { zero(); }
   *
   *   // Constructor with arguments
   *   public Vector3(float x, float y, float z) { set(x, y, z); }
   *
   *   // Copy-constructor
   *   public Vector3(Vector3 vector) { set(vector); }
   *
   *   // Return true if the vector is unit and false otherwise
   *   public boolean isUnit() { return Mathematics.ApproxEqual(lengthSquare(), 1.0f, Defaults.MACHINE_EPSILON); }
   *
   *   // Return true if the vector is the zero vector
   *   public boolean isZero() { return Mathematics.ApproxEqual(lengthSquare(), 0.0f, Defaults.MACHINE_EPSILON); }
   *
   *   // Scalar product of two vectors (public)
   *   public float dot(Vector3 vector) { return x * vector.x + y * vector.y + z * vector.z; }
   *
   *   // Overloaded operator for value access
   *   public float get(integer index) {
   *     if (index == 0) {
   *       return x;
   *     } else if (index == 1) {
   *       return y;
   *     } else if (index == 2) {
   *       return z;
   *     }
   *     throw new IllegalArgumentException("Unknown index: " + index);
   *   }
   *
   *   public float getX() { return x; }
   *   public float getY() { return y; }
   *   public float getZ() { return z; }
   *
   *   // Return the length of the vector
   *   public float length() { return Mathematics.Sqrt(x * x + y * y + z * z); }
   *
   *   // Return the square of the length of the vector
   *   public float lengthSquare() { return x * x + y * y + z * z; }
   *
   *   // Return the axis with the maximal value
   *   public integer getMaxAxis() { return (x < y ? (y < z ? 2 : 1) : (x < z ? 2 : 0)); }
   *
   *   // Return the axis with the minimal value
   *   public integer getMinAxis() { return (x < y ? (x < z ? 0 : 2) : (y < z ? 1 : 2)); }
   *
   *   // Return the corresponding absolute value vector
   *   public Vector3 abs() {
   *     x = Math.abs(x);
   *     y = Math.abs(y);
   *     z = Math.abs(z);
   *     return this;
   *   }
   *
   *   // Overloaded operator for addition with assignment
   *   public Vector3 add(Vector3 vector) {
   *     x += vector.x;
   *     y += vector.y;
   *     z += vector.z;
   *     return this;
   *   }
   *
   *   // Cross product of two vectors (public)
   *   public Vector3 cross(Vector3 vector) {
   *     return set(
   *       y * vector.z - z * vector.y,
   *       z * vector.x - x * vector.z,
   *       x * vector.y - y * vector.x);
   *   }
   *
   *   // Overloaded operator for division by a number with assignment
   *   public Vector3 divide(float number) {
   *     assert (number > Defaults.MACHINE_EPSILON);
   *     x /= number;
   *     y /= number;
   *     z /= number;
   *     return this;
   *   }
   *
   *   // Overloaded operator for the negative of a vector
   *   public Vector3 invert() {
   *     x = -x;
   *     y = -y;
   *     z = -z;
   *     return this;
   *   }
   *
   *   // Overloaded operator for multiplication with a number with assignment
   *   public Vector3 multiply(float number) {
   *     x *= number;
   *     y *= number;
   *     z *= number;
   *     return this;
   *   }
   *
   *   // Normalize the vector
   *   public Vector3 normalize() {
   *     float len = length();
   *     assert (len > Defaults.MACHINE_EPSILON);
   *     float lenInv = 1.0f / len;
   *     x *= lenInv;
   *     y *= lenInv;
   *     z *= lenInv;
   *     return this;
   *   }
   *
   *   // Set all the values of the vector
   *   public final Vector3 set(float x, float y, float z) {
   *     this.x = x;
   *     this.y = y;
   *     this.z = z;
   *     return this;
   *   }
   *
   *   // Assignment operator
   *   public final Vector3 set(Vector3 vector) {
   *     x = vector.x;
   *     y = vector.y;
   *     z = vector.z;
   *     return this;
   *   }
   *
   *   // Return one unit orthogonal vector of the current vector
   *   public Vector3 setUnitOrthogonal() {
   *
   *     float len, lenInv;
   *
   *     // Get the minimum element of the vector
   *     Vector3 abs = new Vector3(this).abs();
   *     integer minElement = abs.getMinAxis();
   *
   *     if (minElement == 0) {
   *       len = Mathematics.Sqrt(y * y + z * z);
   *       lenInv = 1.0f / len;
   *       set(0.0f, -z, y).multiply(lenInv);
   *     } else if (minElement == 1) {
   *       len = Mathematics.Sqrt(x * x + z * z);
   *       lenInv = 1.0f / len;
   *       set(-z, 0.0f, x).multiply(lenInv);
   *     } else {
   *       len = Mathematics.Sqrt(x * x + y * y);
   *       lenInv = 1.0f / len;
   *       set(-y, x, 0.0f).multiply(lenInv);
   *     }
   *
   *     return this;
   *   }
   *
   *   public Vector3 setX(float x) {
   *     this.x = x;
   *     return this;
   *   }
   *
   *   public Vector3 setY(float y) {
   *     this.y = y;
   *     return this;
   *   }
   *
   *   public Vector3 setZ(float z) {
   *     this.z = z;
   *     return this;
   *   }
   *
   *   // Overloaded operator for substraction with assignment
   *   public Vector3 subtract(Vector3 vector) {
   *     x -= vector.x;
   *     y -= vector.y;
   *     z -= vector.z;
   *     return this;
   *   }
   *
   *   // Set the vector to zero
   *   public final Vector3 zero() {
   *     x = 0.0f;
   *     y = 0.0f;
   *     z = 0.0f;
   *     return this;
   *   }
   *
   *   public static void Lerp(Vector3 oldVector, Vector3 newVector, float t, Vector3 vectorOut) {
   *     assert (t >= 0.0f && t <= 1.0f);
   *     vectorOut.set(oldVector).multiply(1.0f - t).add(new Vector3(newVector).multiply(t));
   *   }
   * }
   *
   * public class AABB {
   *   // Maximum world coordinates of the AABB on the x,y and z axis
   *   public Vector3 mMaxCoordinates;
   *
   *   // Minimum world coordinates of the AABB on the x,y and z axis
   *   public Vector3 mMinCoordinates;
   *
   *   // Constructor
   *   public AABB() {
   *     mMaxCoordinates = new Vector3();
   *     mMinCoordinates = new Vector3();
   *   }
   *
   *   // Constructor
   *   public AABB(Vector3 minCoordinates, Vector3 maxCoordinates) {
   *     mMaxCoordinates = maxCoordinates;
   *     mMinCoordinates = minCoordinates;
   *   }
   *
   *   // Return the center point of the AABB in world coordinates
   *   public Vector3 getCenter() { return new Vector3(mMinCoordinates).add(mMaxCoordinates).multiply(0.5f); }
   *
   *   // Return the maximum coordinates of the AABB
   *   public Vector3 getMax() { return mMaxCoordinates; }
   *
   *   // Set the maximum coordinates of the AABB
   *   public void setMax(Vector3 max) { mMaxCoordinates.set(max); }
   *
   *   // Return the minimum coordinates of the AABB
   *   public Vector3 getMin() { return mMinCoordinates; }
   *
   *   // Set the minimum coordinates of the AABB
   *   public void setMin(Vector3 min) { mMinCoordinates.set(min); }
   * }
   * __pliny_solution__
   * public boolean test_no_collision() {
   *   Vector3 a_min = new Vector3(0, 0, 0);
   *   Vector3 a_max = new Vector3(1, 1, 1);
   *   AABB a = new AABB(a_min, a_max);
   *
   *   Vector3 b_min = new Vector3(10, 10, 10);
   *   Vector3 b_max = new Vector3(20, 20, 20);
   *   AABB b = new AABB(b_min, b_max);
   *
   *   if(testCollision(a, b)) {
   *     return false;
   *   }
   *
   *   Vector3 c_min = new Vector3(0, 0, 0);
   *   Vector3 c_max = new Vector3(10, 10, 10);
   *   AABB c = new AABB(c_min, c_max);
   *
   *   Vector3 d_min = new Vector3(0, 0, 30);
   *   Vector3 d_max = new Vector3(20, 20, 2);
   *   AABB d = new AABB(d_min, d_max);
   *
   *   if(testCollision(c, d)) {
   *     return false;
   *   }
   *
   *   Vector3 e_min = new Vector3(0, 0, 0);
   *   Vector3 e_max = new Vector3(10, 10, 10);
   *   AABB e = new AABB(e_min, e_max);
   *
   *   Vector3 f_min = new Vector3(30, 0, 0);
   *   Vector3 f_max = new Vector3(50, 20, 2);
   *   AABB f = new AABB(f_min, f_max);
   *
   *   if(testCollision(e, f)) {
   *   return false;
   *   }
   *
   *   return true;
   * }
   *
   * public boolean test_collision() {
   *   Vector3 a_min = new Vector3(0, 0, 0);
   *   Vector3 a_max = new Vector3(10, 10, 10);
   *   AABB a = new AABB(a_min, a_max);
   *
   *   Vector3 b_min = new Vector3(0, 0, 0);
   *   Vector3 b_max = new Vector3(20, 20, 2);
   *   AABB b = new AABB(b_min, b_max);
   *
   *   if(!testCollision(a, b)) {
   *     return false;
   *   }
   *
   *   return true;
   * }
   *
   * public void test() {
   *   if(!test_no_collision() || !test_collision()) {
   *     println("false");
   *   }
   *   print("true");
   * }
   * test();
   *
   */
  public boolean testCollision(AABB a, AABB aabb) {
    if (a.mMaxCoordinates.getX() < aabb.mMinCoordinates.getX() || aabb.mMaxCoordinates.getX() < a.mMinCoordinates.getX()) {
      return false;
    }
    if (a.mMaxCoordinates.getZ() < aabb.mMinCoordinates.getZ() || aabb.mMaxCoordinates.getZ() < a.mMinCoordinates.getZ()) {
      return false;
    }
    return a.mMaxCoordinates.getY() >= aabb.mMinCoordinates.getY() && aabb.mMaxCoordinates.getY() >= a.mMinCoordinates.getY();
  }

}
