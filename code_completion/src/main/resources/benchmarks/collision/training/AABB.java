package net.smert.jreactphysics3d.collision.shapes;
import net.smert.jreactphysics3d.mathematics.Vector3;
public class AABB {
    /**
     * Testing whether two AABB objects collide
     */
    static public boolean testCollision(AABB a, AABB aabb) {
        if (a.mMaxCoordinates.getX() < aabb.mMinCoordinates.getX() || aabb.mMaxCoordinates.getX() < a.mMinCoordinates.getX()) {
            return false;
        }
        if (a.mMaxCoordinates.getZ() < aabb.mMinCoordinates.getZ() || aabb.mMaxCoordinates.getZ() < a.mMinCoordinates.getZ()) {
            return false;
        }

        return a.mMaxCoordinates.getY() >= aabb.mMinCoordinates.getY() && aabb.mMaxCoordinates.getY() >= a.mMinCoordinates.getY();
    }

  // Maximum world coordinates of the AABB on the x,y and z axis
  public Vector3 mMaxCoordinates;

  // Minimum world coordinates of the AABB on the x,y and z axis
  public Vector3 mMinCoordinates;

  // Constructor
  public AABB() {
    mMaxCoordinates = new Vector3();
    mMinCoordinates = new Vector3();
  }

  // Constructor
  public AABB(Vector3 minCoordinates, Vector3 maxCoordinates) {
    mMaxCoordinates = maxCoordinates;
    mMinCoordinates = minCoordinates;
  }

  // Return the center point of the AABB in world coordinates
  public Vector3 getCenter() {
    return new Vector3(mMinCoordinates).add(mMaxCoordinates).multiply(0.5f);
  }

  // Return the maximum coordinates of the AABB
  public Vector3 getMax() {
    return mMaxCoordinates;
  }

  // Set the maximum coordinates of the AABB
  public void setMax(Vector3 max) {
    mMaxCoordinates.set(max);
  }

  // Return the minimum coordinates of the AABB
  public Vector3 getMin() {
    return mMinCoordinates;
  }

  // Set the minimum coordinates of the AABB
  public void setMin(Vector3 min) {
    mMinCoordinates.set(min);
  }
}
