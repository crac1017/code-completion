public class BodyTest {
  protected Vector2Float pos, size;

  /**
   * Testing whether two AABB objects collide
   */
  public static boolean collides(Body a, Body b) {
    if(Math.abs(a.pos.x - b.pos.x) < a.size.x + b.size.x) {
      if(Math.abs(a.pos.y - b.pos.y) < a.size.y + b.size.y) {
        return true;
      }
    }
    return false;
  }
}
