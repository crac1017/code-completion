public class HelloWorld {

  /**
   * COMMENT:
   * Creating a program with buttons and counters
   *
   * TEST:
   * source("src/main/resources/benchmarks/hello/testlib.java");
   * __pliny_solution__
   * source("src/main/resources/benchmarks/hello/test.java");
   */
  public void run() {
      JJFrame frame;
      JJLabel label;

      //Create and set up the window.
      frame = new JJFrame("HelloWorldSwing");

      // Add the ubiquitous "Hello World" label.
      label = new JJLabel("0");
      frame.getContentPane().add(label);

      int counter = 0;
      JJButton btn = new JJButton("increase");
      btn.addActionListener(new JActionListener() {
          public void actionPerformed(JActionEvent e) {
              counter += 1;
              label.setText(Integer.toString(counter));
          }
      });
      frame.getContentPane().add(btn);

      // Display the window.
      frame.setDefaultCloseOperation(JJFrame.EXIT_ON_CLOSE);
      frame.pack();
      frame.setVisible(true);
  }


}
