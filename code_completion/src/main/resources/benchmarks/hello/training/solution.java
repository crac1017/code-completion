public class HelloWorld {

  /**
   * COMMENT:
   * Creating a program with buttons and counters
   *
   * INPUT:
   * source("src/main/resources/benchmarks/hello/testlib.java");
   *
   * TEST:
   * source("src/main/resources/benchmarks/hello/test.java");
   *
   */
  public void run() {
      JJFrame frame;
      JJLabel label;

      frame = new JJFrame("HelloWorldSwing");

      label = new JJLabel("0");
      frame.getContentPane().add(label);

      int counter = 0;
      JJButton btn = new JJButton("increase");
      btn.addActionListener(new JActionListener() {
          public void actionPerformed(JActionEvent e) {
              counter += 1;
              label.setText(Integer.toString(counter));
          }
      });
      frame.getContentPane().add(btn);

      frame.setDefaultCloseOperation(JJFrame.EXIT_ON_CLOSE);
      frame.pack();
      frame.setVisible(true);
  }


}
