public class HelloWorld {

  /**
   * COMMENT:
   * Creating a program with buttons and counters
   *
   * TEST:
   * source("src/main/resources/benchmarks/hello/testlib.java");
   * __pliny_solution__
   * source("src/main/resources/benchmarks/hello/test.java");
   */
  public void run() {
      JJFrame frame;
      JJLabel label;

      //Create and set up the window.
      frame = new JJFrame(??);
      ??

      int counter = 0;
      JJButton btn = new JJButton("increase");
      btn.addActionListener(new JActionListener() {
          public void actionPerformed(JActionEvent e) {
              counter += 1;
              label.setText(Integer.toString(counter));
          }
      });
      frame.getContentPane().add(btn);

      ??
  }


}
