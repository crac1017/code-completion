boolean has_frame = false;
boolean has_label = false;
boolean has_button = false;
boolean has_listener = false;
boolean added_label = false;
boolean added_button = false;
boolean set_close_op = false;
boolean is_packed = false;
boolean is_visible = false;

public class JJLabel {
    public String label;
    public JJLabel(String s) {
        this.label = s;
        has_label = true;
    }
}

public class JActionEvent {
    public JActionEvent() {
    }
}

interface JActionListener {
    public void actionPerformed(JActionEvent e);
}

public class JJButton {
    public JActionListener listener;
    public String text;
    public void addActionListener(JActionListener l) {
        this.listener = l;
        has_listener = true;
    }
    public JJButton(String s) {
        this.text = s;
        has_button = true;
    }
}


public class JJPane {
    public JJLabel label;
    public JJButton btn;
    public JJPane() {
        this.label = null;
    }

    public add(JJLabel l) {
        added_label = true;
        this.label = l;
    }

    public add(JJButton b) {
        added_button = true;
        this.btn = b;
    }
}

public class JJFrame {
    public String txt;
    public int exit_op = -1;
    public boolean packed = false;
    public boolean visible = false;
    public JJPane pane;

    static public int EXIT_ON_CLOSE = 1;

    public JJFrame(String s) {
        this.txt = s;
        has_frame = true;
    }

    public JJPane getContentPane() {
        return new JJPane();
    }

    public void pack() {
        this.packed = true;
        is_packed = true;
    }

    public void setVisible(boolean v) {
        this.visible = v;
        is_visible = this.visible;
    }

    public void setDefaultCloseOperation(int op) {
        this.exit_op = op;
        set_close_op = true;
    }
}
