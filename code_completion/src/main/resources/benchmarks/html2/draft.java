import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.lang.System;
import java.lang.StringBuilder;
public class HTML {

    /**
     * COMMENT:
     * get all links with a given website name from an html file
     *
     * TEST:
     * import org.jsoup.Jsoup;
     * import org.jsoup.nodes.Document;
     * import org.jsoup.nodes.Element;
     * import org.jsoup.select.Elements;
     * import java.io.BufferedReader;
     * import java.io.FileReader;
     * import java.nio.file.Files;
     * import java.util.regex.Matcher;
     * import java.util.regex.Pattern;
     * import java.util.ArrayList;
     * import java.lang.System;
     * __pliny_solution__
     * ArrayList links = get_links("src/main/resources/benchmarks/html/jsoup.html", "jsoup");
     * boolean _result_ = (
     * links.get(0).equals("//try.jsoup.org/") &&
     * links.get(1).equals("/apidocs/org/jsoup/select/Elements.html#html--") &&
     * links.get(2).equals("/apidocs/index.html?org/jsoup/select/Elements.html") &&
     * links.get(3).equals("//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g") &&
     * links.get(4).equals("http://github.com/jhy/jsoup/"));
     */
    public ArrayList get_links(String html_file, String website) {
        ArrayList result = new ArrayList();
        String content = "";
        BufferedReader reader = new BufferedReader(new FileReader (html_file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");
        while((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        content = stringBuilder.toString();
        Pattern pattern = Pattern.compile(website);
        ??
        return result;
    }
}
