import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class HTML {

    /**
     * COMMENT:
     * Creating a program with buttons and counters
     *
     * TEST:
     * addClassPath("src/main/resources/benchmarks/html/jsoup-1.10.2.jar");
     * import org.jsoup.Jsoup;
     * import org.jsoup.nodes.Document;
     * import org.jsoup.nodes.Element;
     * import org.jsoup.select.Elements;
     * import java.util.regex.Matcher;
     * import java.util.regex.Pattern;
     * __pliny_solution__
     * List links = get_links("src/main/resources/benchmarks/html/jsoup.html", "jsoup");
     * print(
     * links.get(0).equals("//try.jsoup.org/") &&
     * links.get(1).equals("/apidocs/org/jsoup/select/Elements.html#html--") &&
     * links.get(2).equals("/apidocs/index.html?org/jsoup/select/Elements.html") &&
     * links.get(3).equals("//try.jsoup.org/~LGB7rk_atM2roavV0d-czMt3J_g") &&
     * links.get(4).equals("http://github.com/jhy/jsoup/"));
     */
    public List get_links(String html_file, String website) {
        BufferedReader reader = new BufferedReader(new FileReader (html_file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");
        while((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        String content = stringBuilder.toString();
        List result = new ArrayList();

        Document doc = Jsoup.parse(content);
        String title = doc.title();
        Elements links = doc.select("a[href]");

        Pattern pattern = Pattern.compile(website);

        for(Element link : links) {
            String l = link.attr("href");
            Matcher m = pattern.matcher(l);
            if(m.find()) {
                result.add(l);
            }
        }
        return result;
    }
}
