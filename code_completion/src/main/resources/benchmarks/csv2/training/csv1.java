import java.io.File;
import java.util.Scanner;
import java.lang.Integer;
import java.io.BufferedReader;

public class CSV {
	/**
	 * Read a matrix from a CSV file
	 */
    public int[][] read_csv(String filename, int row, int col) {
        int[][] mat = new int[row][col];

		String splitBy = ",";
		BufferedReader br = new BufferedReader(new FileReader(filename));

		for(int i = 0; i < row; ++i) {
			String line = br.readLine();
			String[] b = line.split(splitBy);
			for(int j = 0; j < col; ++j) {
				mat[i][j] = Integer.parseInt(b[j]);
			}
		}
		br.close();
		return mat;
    }
}
