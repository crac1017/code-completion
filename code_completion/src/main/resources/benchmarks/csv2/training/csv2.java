import java.io.File;
import java.util.Scanner;
import java.lang.Integer;

public class CSV {
	/**
	 * Read a matrix from a CSV file
	 */
    public int[][] read_csv(String filename, int row, int col) {
		int[][] lines = new int[row][col];
		File file= new File(fileName);
		Scanner inputStream;

		inputStream = new Scanner(file);
		for(int i = 0; i < row; ++i) {
			String line= inputStream.next();
			String[] values = line.split(",");
			for(int j = 0; j < col; ++j) {
				lines[i][j] = Integer.parseInt(values[j]);
			}
		}
		inputStream.close();
		return lines;
	}
}
