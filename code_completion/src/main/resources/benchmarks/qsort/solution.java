public class QSort {
    /**
     * COMMENT:
     * This is quick sort using partition
     *
     * TEST:
     * __pliny_solution__
     integer NIL = 0;
     public boolean array_equal(integer[] a1, integer[] a2) {
       if(a1.length != a2.length) { return false; }
       for(integer i = 0; i < a1.length; ++i) {
         if(a1[i] != a2[i]) { return false; }
       }
       return true;
     }

     public void swap(integer[] array, integer p1, integer p2) {
       integer tmp = array[p1];
       array[p1] = array[p2];
       array[p2] = tmp;
     }

     integer[][] test_array = {{6, 2, 10, 8, NIL, NIL, NIL, NIL, NIL, NIL},
                           {10, 9, -10, 7, NIL, NIL, NIL, NIL, NIL, NIL},
                           {1, 3, -5, -9, NIL, NIL, NIL, NIL, NIL, NIL},
                           {0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                           {2, 6, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                           {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                           {-1, -4, -7, -19, -22, -33, -47, -73, -87, -92},
                           {2, 6, 14, 15, 21, 27, 33, 37, 49, NIL},
                           {-10, -5, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                           {-1, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL}};
     integer[] test_sizes = {4, 4, 4, 1, 2, 10, 10, 9, 2, 1};
     integer[][] test_ans = {{2, 6, 8, 10, NIL, NIL, NIL, NIL, NIL, NIL},
                         {-10, 7, 9, 10, NIL, NIL, NIL, NIL, NIL, NIL},
                         {-9, -5, 1, 3, NIL, NIL, NIL, NIL, NIL, NIL},
                         {0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                         {2, 6, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                         {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                         {-92, -87, -73, -47, -33, -22, -19, -7, -4, -1},
                         {2, 6, 14, 15, 21, 27, 33, 37, 49, NIL},
                         {-10, -5, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                         {-1, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL}};

     integer[][] result = {new integer[10],
                       new integer[10],
                       new integer[10],
                       new integer[10],
                       new integer[10],
                       new integer[10],
                       new integer[10],
                       new integer[10],
                       new integer[10],
                       new integer[10]};

     for(i = 0; i < 10; ++i) {
       for(j = 0; j < test_sizes[i]; ++j) {
         result[i][j] = test_array[i][j];
       }
       qsort(result[i], 0, test_sizes[i] - 1);
       if(!array_equal(test_ans[i], result[i])) {
         print("false");
         exit();
       }
     }
     print("true");
     */
    void qsort(int[] a, int l, int r) {
        int i;
        int j;
        int m = l;
        int i = 0;
        if(l >= r)
        {
            return ;
        }
        for((i = l + 1); (i <= r); (i ++))
        {
            if(a[i] < a[l])
            {
                swap(a, ++m, i);
            }
        }
        swap(a, l, m);
        qsort(a, l, m - 1);
        qsort(a, m + 1, r);
    }
}