public class QSort {
    /**
     * quick sort partition
     */
    void qsort(int[] a, int l, int r) {
        int j;
        int m = l;
        int i = 0;
        if(l >= r) {
            return ;
        }
        for(i = l + 1; i <= r; i++) {
            if(a[i] < a[l]) {
                swap(a, ++m, i);
            }
        }
        swap(a, l, m);
        qsort(a, l, m - 1);
        qsort(a, m + 1, r);
    }
}
