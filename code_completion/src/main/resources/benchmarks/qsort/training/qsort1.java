public class QSort {
	/**
	 * Quick sort
	 */
    void qsort(int[] a, int l, int r) {
        int i, j, x, w;

        i = l;
        j = r;
        x = a[l+r / 2];
        while(i<= j) {
            while (a[i] < x) i = i + 1;
            while (x < a[j]) j = j - 1;
            if (i<= j) {
                w = a[i];
                a[i] = a[j];
                a[j] = w;
                i = i + 1;
                j = j - 1;
            }
        }
        if (l < j) qsort(a, l,j);
        if (i < r) qsort(a, i, r);
    }
}
