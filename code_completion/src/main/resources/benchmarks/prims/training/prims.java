public class Prims {
	/**
	 * Prims algorithm
	 */
    public int prim() {
        int n;
        int[] low = new int[20];
        boolean[] vis = new boolean[20];
        int[][] d = new int[20][20];
        int sum = 0;
        for(int i = 1; i < n; i++) {
            int t = -1;
            int min = 2147483647;
            for(int j = 0; j < n; j ++)
            {
                if((!vis[j]) && (min > low[j]))
                {
                    t = j;
                    min = low[j];
                }
            }
            vis[t] = true;
            sum += min;
            for(int j = 0; j < n; j ++)
            {
                if((!vis[j]) && (d[t][j] < low[j]))
                {
                    low[j] = d[t][j];
                }
            }
        }
        return sum;
    }

}
