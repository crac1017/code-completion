public class Prims {
    /**
     * COMMENT:
     * Prims algorithm
     *
     * TEST:
     integer NIL = 100000;
     __pliny_solution__

     public boolean array_equal(integer[] a1, integer[] a2) {
       if(a1.length != a2.length) { return false; }
       for(integer i = 0; i < a1.length; ++i) {
         if(a1[i] != a2[i]) { return false; }
       }
       return true;
     }

     integer[][][] test_graph = {
     {{0, 5, NIL, NIL, NIL},
     {NIL, 0, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL}},
     {{0, 1, 10, NIL, NIL},
     {NIL, 0, 2, NIL, NIL},
     {NIL, NIL, 0, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL}},
     {{0, 2, 5, NIL, 8},
     {NIL, 0, NIL, NIL, 1},
     {NIL, NIL, 0, 4, NIL},
     {NIL, NIL, NIL, 0, NIL},
     {NIL, NIL, NIL, 3, 0}},
     {{0, 5, NIL, NIL, NIL},
     {3, 0, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL}}};

     integer[][] test_loc = {{2, 2, 0}, // the third element is the index of the test graph
                         {2, 2, 0},
                         {3, 2, 1},
                         {3, 2, 1},
                         {3, 3, 1},
                         {5, 2, 2},
                         {5, 2, 2},
                         {5, 3, 2},
                         {5, 4, 2},
                         {5, 5, 2}};

     integer[] ans = {2, 2, 2, 2, 3, 2, 2, 3, 4, 5};
     integer[][] result_cluster = {{0, 0},
                               {0, 0},
                               {0, 0},
                               {0, 0},
                               {0, 0, 0},
                               {0, 0},
                               {0, 0},
                               {0, 0, 0},
                               {0, 0, 0, 0},
                               {0, 0, 0, 0, 0}};

     integer[][] ans_cluster = {{0, 1},
                            {0, 1},
                            {0, 1},
                            {0, 1},
                            {0, 1, 2},
                            {0, 1},
                            {0, 1},
                            {0, 1, 4},
                            {0, 1, 4, 3},
                            {0, 1, 4, 3, 2}};

     for(integer i = 0; i < 10; ++i) {
         integer test_size = test_loc[i][1];
         integer result = prims(test_graph[test_loc[i][2]], result_cluster[i], test_loc[i][0], test_size);

         // print("i : " + i);
         // print(ans_cluster[i]);
         // print(result_cluster[i]);
         if(!array_equal(ans_cluster[i], result_cluster[i])) {
             print(false);
             exit();
         }
     }
     print(true);
     */
    public int prims(int[][] graph, int[] cluster, int nsize, int csize) {
        int[] key = new int[5];   // Key values used to pick minimum weight edge in cut
        boolean[] used = new boolean[5];
        int cluster_size = 0;
        int u;
        int min_dist;
        int count;

        // Initialize all keys as INFINITE
        for (int i = 0; i < nsize; i++)
          // key[i] = 100000;
          key[i] = ??;

        // Always include first 1st vertex in MST.
        key[0] = 0;     // Make key 0 so that this vertex is picked as first vertex
        // used[0] = true;
        // cluster[cluster_size++] = 0;

        // The MST will have V vertices
        for (count = 0; count < nsize; count++) {
            // Pick thd minimum key vertex from the set of vertices
            // not yet included in MST
            u = -1;

            int dist = 2147483647;
            for((min_dist = 0); (min_dist < nsize); (min_dist ++))
            {
                if((!used[min_dist]) && (dist > key[min_dist]))
                {
                    u = min_dist;
                    dist = key[min_dist];
                }
            }
            /*
            */


            if(u == -1) { break; }

            // print("next node: " + u + " with distance : " + dist);

            // Add the picked vertex to the cluster
            used[u] = true;
            cluster[cluster_size++] = u;
            if(cluster_size == csize) {
                return cluster_size;
            }

            // update the node distances
            /*
            for(integer j = 0; j < nsize; j++) {
                if((!used[j]) && (graph[u][j] < key[j])) {
                    key[j] = graph[u][j];
                }
            }
            */
            // print("dist:");
            // print(key);
            /**
             * COMMENT:
             * Prims update
             * 
             * TEST:
             * integer u = 0;
             * integer nsize = 4;
             * boolean[] used = {true, false, false, true};
             * integer[] key = {10, 10, 10, 10};
             * integer[][] graph = {{0, 100, 1, 0}};
             * __pliny_solution__
             * print(key[0] == 10 && key[1] == 10 && key[2] == 1 && key[3] == 10);
             */
            ??
        }

        return cluster_size;
    }
}
