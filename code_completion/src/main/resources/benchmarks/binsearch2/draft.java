public class Binsearch {

  /**
   * COMMENT:
   * Binary search
   *
   * TEST:
   * int[] array = new int[] {-100, -20, 0, 4, 100, 600, 601};
   * int[] array2 = new int[] {0};
   * int[] array3 = new int[] {};
   * __pliny_solution__
   * int _test_num_ = 7;
   * boolean _case_0_ = (binsearch(array, -20) == 1);
   * boolean _case_1_ = (binsearch(array, 4) == 3);
   * boolean _case_2_ = (binsearch(array, 0) == 2);
   * boolean _case_3_ = (binsearch(array, 601) == 6);
   * boolean _case_4_ = (binsearch(array, 602) == -1);
   * boolean _case_5_ = (binsearch(array2, 0) == 0);
   * boolean _case_6_ = (binsearch(array3, 0) == -1);
   * boolean _result_ = (
   * _case_0_ &&
   * _case_1_ &&
   * _case_2_ &&
   * _case_3_ &&
   * _case_4_ &&
   * _case_5_ &&
   * _case_6_);
   */
  public int binsearch(int[] array, int x) {
      int low = 0;
      int high = array.length - 1;
      int result = ??;

      while(low <= high) {
          ??
      }
      return result;
  }
}
