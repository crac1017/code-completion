public class Binsearch {

  /**
   * COMMENT:
   * Binary search on an integer array
   *
   * TEST:
   * integer [] array = new integer [] {-100, -20, 0, 4, 100, 600, 601};
   * integer [] array2 = new integer [] {0};
   * integer [] array3 = new integer [] {};
   * __pliny_solution__
   * print(
   * binsearch(array, -20) == 1 &&
   * binsearch(array, 4) == 3 &&
   * binsearch(array, 0) == 2 &&
   * binsearch(array, 601) == 6 &&
   * binsearch(array, 602) == -1 &&
   * binsearch(array2, 0) == 0 &&
   * binsearch(array3, 0) == -1);
   */
  public int binsearch(int[] array, int x) {
    int result = -1;
    int low = 0;
    int high = array.length - 1;

    while(low <= high) {
      int mid = (low + high) / 2;
      if(array[mid] > x) {
        high = mid - 1;
      } else if(array[mid] < x) {
        low = mid + 1;
      } else {
        result = mid;
        break;
      }
    }

    return result;
  }
}
