public class SievePrime {
	/**
	 * Use Sieve of Eratosthenes algorithm to test primality
	 */
  void sieve(boolean[] primes, int n) {
    primes[1] = false;
    for(int i = 2; i <= n; i++) {
      primes[i] = true;
    }
    for(int i = 2; i <= n / 2; i++) {
      for(int j = 2; j <= n / i; j++) {
        primes[i * j] = false;
      }
    }
  }
}
