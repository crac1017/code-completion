public class Sieve {

    /**
     * COMMENT:
	   * Use Sieve of Eratosthenes algorithm to test primality
     *
     * TEST:
     * __pliny_solution__
     * print(
     * sieve(1) == false &&
     * sieve(2) == true &&
     * sieve(27) == false
     * );
     */
    boolean sieve(int n) {
        boolean[] primes = new boolean[100];
        for(int i = ??; i <= n; i++) {
            primes[i] = ??;
        }
        ??

        return primes[n];
    }
}
