public class Sieve {

    /**
     * COMMENT:
     * Prime sieve
     *
     * TEST:
     * __pliny_solution__
     * print(
     * sieve(1) == false &&
     * sieve(2) == true &&
     * sieve(3) == true &&
     * sieve(4) == false &&
     * sieve(9) == false &&
     * sieve(17) == true &&
     * sieve(27) == false
     * );
     */
    boolean sieve(int n) {
        boolean[] primes = new boolean[100];
        primes[1] = false;
        for(int i = 2; (i <= n); (i ++)) {
            primes[i] = true;
        }
        for(int i = 2; (i <= (n / 2)); (i ++)) {
            for(int j = 2; (j <= (n / i)); (j ++)) {
                primes[(i * j)] = false;
            }
        }
        return primes[n];
    }
}
