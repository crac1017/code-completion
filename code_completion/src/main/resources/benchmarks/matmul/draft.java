public class MatMul {
    /**
     * COMMENT:
     * Matrix multiplication
     *
     * TEST:
     import java.util.Random;
     __pliny_solution__
     integer[][] m1 = new integer[20][20];
     integer[][] m2 = new integer[20][20];
     integer[][] m3 = new integer[20][20];
     integer[][] m4 = new integer[20][20];
     integer[][] result = new integer[20][20];
     integer[][] ans = new integer[20][20];

     public void my_mul(integer[][] A, integer[][] B, integer[][] C, integer n) {
       // matrix multiplication
       integer i, j, k;
       integer s;
       for (i=0; i<n; i++) {
         for (j=0; j<n; j++) {
           s=0;
           for (k=0; k<n; k++)
             s=s+A[i][k]*B[k][j];
           C[i][j]=s;
         }
       }
     }
     public boolean test_mat(integer[][] m1, integer[][] m2, integer n) {
	   for(integer i = 0; i < n; ++i) {
           for(integer j = 0; j < n; ++j) {
               if(m1[i][j] != m2[i][j]) {
                   return false;
               }
           }
       }
	   return true;
     }

		 boolean passed = true;
     // Random rn = new Random();
     for(integer k = 0; k < 5; ++k) {
       // m1 = mat_gen(m1, k + 1);
       // m2 = mat_gen(m2, k + 1);

       for(integer i = 0; i < k + 1; ++i){
         for(integer j = 0; j < k + 1; ++j) {
           // m[i][j] = rn.nextInt(100) - 50;
           m1[i][j] = i + j;
					 m2[i][j] = i + j;
           m3[i][j] = i + j;
					 m4[i][j] = i + j;
         }
       }

       matmul(m1, m2, result, k + 1);
       my_mul(m3, m4, ans, k + 1);

       if(test_mat(result, ans, k + 1)) {
       } else {
			   passed = false;
				 break;
       }
     }
     print(passed);
     */
    void matmul(int[][] A, int[][] B, int[][] C, int n) {
			int i, j, k;
			int s;
			for (i=0; i<n; i++) {
				for (j=0; j<n; j++) {
					s=??;
					??
					C[i][j]=s;
				}
			}
    }
}
