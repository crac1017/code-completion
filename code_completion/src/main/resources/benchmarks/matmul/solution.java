public class MatMul {
    /**
     * COMMENT:
     * Matrix multiplication
     *
     * TEST:
     import java.util.Random;
     __pliny_solution__
     integer[][] m1 = new integer[20][20];
     integer[][] m2 = new integer[20][20];
     integer[][] result = new integer[20][20];
     integer[][] ans = new integer[20][20];

     public void my_mul(integer[][] A, integer[][] B, integer[][] C, integer n) {
       // matrix multiplication
       integer i, j, k;
       integer s;
       for (i=0; i<n; i++) {
         for (j=0; j<n; j++) {
           s=0;
           for (k=0; k<n; k++)
             s=s+A[i][k]*B[k][j];
           C[i][j]=s;
         }
       }
     }
     public boolean test_mat(integer[][] m1, integer[][] m2, integer n) {
	   for(integer i = 0; i < n; ++i) {
           for(integer j = 0; j < n; ++j) {
               if(m1[i][j] != m2[i][j]) {
                   return false;
               }
           }
       }
	   return true;
     }

     public void mat_gen(integer[][] m, integer n) {
       Random rn = new Random();
       integer i, j;
       for(i = 0; i < n; ++i){
         for(j = 0; j < n; ++j) {
           m[i][j] = rn.nextInt(100) - 50;
         }
       }
     }
     for(integer i = 0; i < 10; ++i) {
       mat_gen(m1, i + 1);
       mat_gen(m2, i + 1);

       matmul(m1, m2, result, i);
       my_mul(m1, m2, ans, i);

       if(test_mat(result, ans, i)) {
       } else {
         print("false");
         exit();
       }
     }
     print("true");
     */
    void matmul(int[][] A, int[][] B, int[][] C, int n) {
        int i, j, k;
        int s;
        for (i=0; i<n; i++) {
            for (j=0; j<n; j++) {
                s=0;
                for (k=0; k<n; k++)
                    s=s+A[i][k]*B[k][j];
                C[i][j]=s;
            }
        }
    }
}
