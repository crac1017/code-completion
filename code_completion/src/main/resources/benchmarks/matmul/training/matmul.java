public class MatMul {
    /**
     * COMMENT:
     * Matrix multiplication
     */
    void matmul(int[][] A, int[][] B, int[][] C, int n) {
        int i, j, k;
        int s;
        for (i=0; i<n; i++) {
            for (j=0; j<n; j++) {
                s=0;
                for (k=0; k<n; k++)
                    s=s+A[i][k]*B[k][j];
                C[i][j]=s;
            }
        }
    }
}
