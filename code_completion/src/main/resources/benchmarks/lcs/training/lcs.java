public class LCS {
	/**
	 * Longest common subsequence algorithm
	 */
    int lcs(int[] a, int[] b, int[][] c, int n, int m, int[] result){
        int i, j;
        for(i = 0; i < n; i ++) c[i][0] = 0;
        for(j = 0; j < m; j ++) c[0][j] = 0;
        for(i = 1; i <= n; i ++)    {
            for(j = 1; j <= m; j++)
            {
                if(a[i - 1] == b[j - 1])
                {
                    c[i][j] = 1 + c[i - 1][j - 1];
                }
                else
                {
                    c[i][j] = max(c[i - 1][j], c[i][j - 1]);
                }
            }
        }

        index = c[m][n];
        s = index;
        i = m;
        j = n;
        while(i > 0 && j > 0) {
            if(a[i - 1] == b[j - 1]) {
                result[index - 1] = a[i - 1];
                i --;
                j --;
                index --;
            } else {
                if(c[i - 1][j] > c[i][j - 1]) {
                    i --;
                } else {
                    j --;
                }
            }
        }
        return c[n][m];
    }
}
