public class LCS {
    /**
     * COMMENT:
     * This is longest comment subsequence algorithm.
     *
     * TEST:
     __pliny_solution__
     integer NIL = -1;
     public boolean array_equal(integer[] a1, integer[] a2) {
       if(a1.length != a2.length) { return false; }
       for(integer i = 0; i < a1.length; ++i) {
         if(a1[i] != a2[i]) { return false; }
       }
       return true;
     }

     public integer max(integer a, integer b) {
       return a > b ? a : b;
     }

     integer[][] test_array1 = {{2,   6,   8,   10,   NIL, NIL, NIL, NIL, NIL, NIL},
                            {-10, 7,   11,  14,   NIL, NIL, NIL, NIL, NIL, NIL},
                            {1,   3,   5,   9,    NIL, NIL, NIL, NIL, NIL, NIL},
                            {0,   NIL, NIL, NIL,  NIL, NIL, NIL, NIL, NIL, NIL},
                            {1,   4,   7,   19,   22,  33,  47,  73,  87,  92},
                            {1,   7,   22,  47,   87,  NIL, NIL, NIL, NIL, NIL},
                            {2,   6,   14,  15,   21,  27,  NIL, NIL, NIL, NIL},
                            {2,   10,  6,   40,   -1,  NIL, NIL, NIL, NIL, NIL},
                            {-10, -5,  10,  5,    -5,  NIL, NIL, NIL, NIL, NIL},
                            {1,   1,   1,   1,    1,   1,   1,   1,   1,   1}};
     integer[] test_sizes1 = {4, 4, 4, 1, 10, 5, 6, 5, 5, 10};
     integer[][] test_array2 = {{2, 6, 8, 10, NIL, NIL, NIL, NIL, NIL, NIL},
                            {-10, 7, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                            {3, 5, 9, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                            {0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                            {19, 22, 33, 47, 73, NIL, NIL, NIL, NIL, NIL},
                            {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                            {15, 21, 27, 33, 37, 49, NIL, NIL, NIL, NIL},
                            {10, -1, 40, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                            {5, 10, 5, -5, -10, NIL, NIL, NIL, NIL, NIL},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
     integer[] test_sizes2 = {4, 2, 3, 1, 5, 10, 6, 3, 5, 10};

     integer[][] test_ans = {{2, 6, 8, 10, NIL, NIL, NIL, NIL, NIL, NIL},
                        {-10, 7, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {3, 5, 9, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {19, 22, 33, 47, 73, NIL, NIL, NIL, NIL, NIL},
                        {1, 7, 22, 47, 87, NIL, NIL, NIL, NIL, NIL},
                        {15, 21, 27, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {10, -1, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {10, 5, -5, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
     integer[] test_ans_sizes = {4, 2, 3, 1, 5, 5, 3, 2, 3, 10};
     integer[] result = new integer[10];
     for(integer i = 0; i < 10; ++i) {
       for(integer j = 0; j < 10; ++j) {
         result[j] = -1;
       }
       lcs(test_array1[i], test_array2[i], test_sizes1[i], test_sizes2[i], result);
       if(!array_equal(result, test_ans[i])) {
         print("false");
         exit();
       }
     }
     print("true");
     */
    void lcs(int[] X, int[] Y, int m, int n, int[] result) {
        int[][] L = new int[15][15];
        int i;
        int j;
        int index;
        int s;
        for((j = 1); (j <= m); (j ++)) {
            for((i = 1); (i <= n); (i ++)) {
                if(X[(j - 1)] == Y[(i - 1)]) {
                    L[j][i] = 1 + L[(j - 1)][(i - 1)];
                } else {
                    L[j][i] = max(L[(j - 1)][i], L[j][(i - 1)]);
                }
            }
        }
        index = L[m][n];
        s = index;
        i = m;
        j = n;
        while((i > 0) && (j > 0)) {
            if(X[(i - 1)] == Y[(j - 1)]) {
                result[(index - 1)] = X[(i - 1)];
                i --;
                j --;
                index --;
            } else {
                if(L[(i - 1)][j] > L[i][(j - 1)]) {
                    i --;
                } else {
                    j --;
                }
            }
        }
    }
}
