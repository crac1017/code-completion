public class LCS {
    /**
     * COMMENT:
     * Longest common subsequence algorithm
     *
     * TEST:
     __pliny_solution__
     int NIL = -1;
     public boolean array_equal(int[] a1, int[] a2) {
       if(a1.length != a2.length) { return false; }
       for(int i = 0; i < a1.length; ++i) {
         if(a1[i] != a2[i]) { return false; }
       }
       return true;
     }

     public int max(int a, int b) {
       return a > b ? a : b;
     }

     int[][] test_array1 = {{2,   6,   8,   10,   NIL, NIL, NIL, NIL, NIL, NIL},
                            {-10, 7,   11,  14,   NIL, NIL, NIL, NIL, NIL, NIL},
                            {1,   3,   5,   9,    NIL, NIL, NIL, NIL, NIL, NIL},
                            {0,   NIL, NIL, NIL,  NIL, NIL, NIL, NIL, NIL, NIL},
                            {1,   4,   7,   19,   22,  33,  47,  73,  87,  92},
                            {1,   7,   22,  47,   87,  NIL, NIL, NIL, NIL, NIL},
                            {2,   6,   14,  15,   21,  27,  NIL, NIL, NIL, NIL},
                            {2,   10,  6,   40,   -1,  NIL, NIL, NIL, NIL, NIL},
                            {-10, -5,  10,  5,    -5,  NIL, NIL, NIL, NIL, NIL},
                            {1,   1,   1,   1,    1,   1,   1,   1,   1,   1}};
     int[] test_sizes1 = {4, 4, 4, 1, 10, 5, 6, 5, 5, 10};
     int[][] test_array2 = {{2, 6, 8, 10, NIL, NIL, NIL, NIL, NIL, NIL},
                            {-10, 7, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                            {3, 5, 9, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                            {0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                            {19, 22, 33, 47, 73, NIL, NIL, NIL, NIL, NIL},
                            {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                            {15, 21, 27, 33, 37, 49, NIL, NIL, NIL, NIL},
                            {10, -1, 40, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                            {5, 10, 5, -5, -10, NIL, NIL, NIL, NIL, NIL},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
     int[] test_sizes2 = {4, 2, 3, 1, 5, 10, 6, 3, 5, 10};

     int[][] test_ans = {{2, 6, 8, 10, NIL, NIL, NIL, NIL, NIL, NIL},
                        {-10, 7, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {3, 5, 9, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {19, 22, 33, 47, 73, NIL, NIL, NIL, NIL, NIL},
                        {1, 7, 22, 47, 87, NIL, NIL, NIL, NIL, NIL},
                        {15, 21, 27, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {10, -1, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {10, 5, -5, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
     int[] test_ans_sizes = {4, 2, 3, 1, 5, 5, 3, 2, 3, 10};
     int[] result = new int[10];
     boolean _result_ = false;

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[0], test_array2[0], test_sizes1[0], test_sizes2[0], result);
     boolean _case_0_ = array_equal(result, test_ans[0]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[1], test_array2[1], test_sizes1[1], test_sizes2[1], result);
     boolean _case_1_ = array_equal(result, test_ans[1]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[2], test_array2[2], test_sizes1[2], test_sizes2[2], result);
     boolean _case_2_ = array_equal(result, test_ans[2]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[3], test_array2[3], test_sizes1[3], test_sizes2[3], result);
     boolean _case_3_ = array_equal(result, test_ans[3]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[4], test_array2[4], test_sizes1[4], test_sizes2[4], result);
     boolean _case_4_ = array_equal(result, test_ans[4]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[5], test_array2[5], test_sizes1[5], test_sizes2[5], result);
     boolean _case_5_ = array_equal(result, test_ans[5]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[6], test_array2[6], test_sizes1[6], test_sizes2[6], result);
     boolean _case_6_ = array_equal(result, test_ans[6]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[7], test_array2[7], test_sizes1[7], test_sizes2[7], result);
     boolean _case_7_ = array_equal(result, test_ans[7]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[8], test_array2[8], test_sizes1[8], test_sizes2[8], result);
     boolean _case_8_ = array_equal(result, test_ans[8]);

     for(int j = 0; j < 10; ++j) { result[j] = -1; }
     lcs(test_array1[9], test_array2[9], test_sizes1[9], test_sizes2[9], result);
     boolean _case_9_ = array_equal(result, test_ans[9]);

     _result_ = (
       _case_0_ &&
       _case_1_ &&
       _case_2_ &&
       _case_3_ &&
       _case_4_ &&
       _case_5_ &&
       _case_6_ &&
       _case_7_ &&
       _case_8_ &&
       _case_9_ );
     */
    void lcs(int[] X, int[] Y, int m, int n, int[] result) {
        int[][] L = new int[15][15];
        int i = 0;
        int j = 0;
        int index = 0;
        int s = 0;
        ??

        index = L[m][n];
        s = index;
        i = m;
        j = n;
        while((i > 0) && (j > 0)) {
            if(X[(i - 1)] == Y[(j - 1)]) {
                result[(index - 1)] = X[(i - 1)];
                i --;
                j --;
                index --;
            } else {
                if(L[(i - 1)][j] > L[i][(j - 1)]) {
                    i --;
                } else {
                    j --;
                }
            }
        }
    }
}
