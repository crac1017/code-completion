import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.Reader;

/**
 * This is the class's comment
 */
public class FooMain {

    /**
     * COMMENT:
     * comment here
     *
     * TEST:
     * print(true);
     */
    public int main(String filename) {
        String s;
        java.io.FileReader reader = new java.io.FileReader(filename);
        java.io.BufferedReader breader = new java.io.BufferedReader(reader);
        while(breader.ready()) {
            s += breader.readLine();
        }
        breader.close();
    }
}
