/*
Copyright 2017 Rice University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
public class TestIO1 {

    void read(String file) {
        String content;
        /**
         * COMMENT:
         * comment here
         *
         * TEST:
         * import java.io.FileNotFoundException;
         * import java.io.FileReader;
         * import java.io.BufferedReader;
         * import java.io.Reader;
         * String file = "/Users/yanxin/Documents/work/rice/code-completion/src/main/resources/benchmarks/testio/read_file.txt";
         * String content = "";
         * __pliny_solution__
         * boolean _result_ = content.startsWith("foobar");
         */
        ??
    }

}
