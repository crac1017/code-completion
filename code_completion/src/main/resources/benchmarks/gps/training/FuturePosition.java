public class FuturePosition {

  /**
   * calculate the future position given a gps coordinate
   */
  public double[] estimateFuturePosition(LatLonAlt gpsPosPlusAlt, LatLonAlt ref, double[] scaledVelocity, double timeInterval) {
    double pos[] = new double[3];
    double futurePos[] = new double[3];
    double r_earth = 6371000.0;

    double cos_lat0 = Math.cos(ref.lat * Math.PI / 180.0);
    double sin_lat0 = Math.sin(ref.lat * Math.PI / 180.0);
    double sin_lat = Math.sin(gpsPosPlusAlt.lat * Math.PI / 180.0);
    double cos_lat = Math.cos(gpsPosPlusAlt.lat * Math.PI / 180.0);
    double cos_d_lon = Math.cos(gpsPosPlusAlt.lon * Math.PI / 180.0 - ref.lon * Math.PI / 180.0);
    double c = Math.acos(sin_lat0 * sin_lat + cos_lat0 * cos_lat * cos_d_lon);
    double k = (c == 0.0) ? 1.0 : (c / Math.sin(c));
    double y = k * cos_lat * Math.sin(gpsPosPlusAlt.lon * Math.PI / 180.0 - ref.lon * Math.PI / 180.00) * r_earth;
    double x = k * (cos_lat0 * sin_lat - sin_lat0 * cos_lat * cos_d_lon) * r_earth;
    double z = ref.alt - gpsPosPlusAlt.alt;

    pos[0] = x;
    pos[1] = y;
    pos[2] = z;

    futurePos[0] = scaledVelocity[0]*timeInterval + pos[0];
    futurePos[1] = scaledVelocity[1]*timeInterval + pos[1];
    futurePos[2] = scaledVelocity[2]*timeInterval + pos[2];

    return futurePos;
  }
}
