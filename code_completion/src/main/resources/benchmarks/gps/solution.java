public class FuturePosition {
  /**
   * COMMENT:
   * calculate the future position given a GPS coordinate
   *
   * TEST:
   * public class LatLonAlt {
   *   public static double RADIUS_OF_EARTH = 6371000.0;
   *
   *   public final double lat;
   *   public final double lon;
   *   public final double alt;
   *
   *   public LatLonAlt(double lat, double lon, double alt) {
   *     this.lat = lat;
   *     this.lon = lon;
   *     this.alt = alt;
   *   }
   *
   *   public LatLonAlt add(double[] vector) {
   *     return new LatLonAlt(
   *       lat + vector[0] / RADIUS_OF_EARTH * 180.0 / Math.PI,
   *       lon + vector[1] / (Math.cos(lat * Math.PI / 180.0) * RADIUS_OF_EARTH) * 180.0 / Math.PI,
   *       alt - vector[2]);
   *   }
   *
   *   public double[] sub(LatLonAlt pos) {
   *     return new double[]{
   *       (lat - pos.lat) * Math.PI / 180.0 * RADIUS_OF_EARTH,
   *       (lon - pos.lon) * Math.PI / 180.0 * Math.cos(lat * Math.PI / 180.0) * RADIUS_OF_EARTH,
   *       pos.alt - alt
   *     };
   *   }
   *
   *   public boolean isFinite() {
   *     return Double.isFinite(lat) && Double.isFinite(lon) && Double.isFinite(alt);
   *   }
   * }
   *
   * __pliny_solution__
   * LatLonAlt gps = new LatLonAlt(1.0f, 1.0f, 1.0f);
   * LatLonAlt ref = new LatLonAlt(0.0f, 0.0f, 0.0f);
   * double[] v = new double[3];
   * v[0] = 0.0;
   * v[1] = 0.0;
   * v[2] = 0.0;
   * double t = 5.0;
   * double[] pos = estimateFuturePosition(gps, ref, v, t);
   * print((integer) pos[0] == 111200 && (integer) pos[1] == 111183 && (integer) pos[2] == -1);
   */
  public double[] estimateFuturePosition(LatLonAlt gpsPosPlusAlt, LatLonAlt ref, double[] scaledVelocity, double timeInterval) {
    double[] pos = new double[3];
    double[] futurePos = new double[3];
    double r_earth = 6371000.0;

    double cos_lat0 = Math.cos(ref.lat * Math.PI / 180.0);
    double sin_lat0 = Math.sin(ref.lat * Math.PI / 180.0);
    double sin_lat = Math.sin(gpsPosPlusAlt.lat * Math.PI / 180.0);
    double cos_lat = Math.cos(gpsPosPlusAlt.lat * Math.PI / 180.0);
    double cos_d_lon = Math.cos(gpsPosPlusAlt.lon * Math.PI / 180.0 - ref.lon * Math.PI / 180.0);
    double c = Math.acos(sin_lat0 * sin_lat + cos_lat0 * cos_lat * cos_d_lon);
    double k = (c == 0.0) ? 1.0 : (c / Math.sin(c));
    double y = k * cos_lat * Math.sin(gpsPosPlusAlt.lon * Math.PI / 180.0 - ref.lon * Math.PI / 180.00) * r_earth;
    double x = k * (cos_lat0 * sin_lat - sin_lat0 * cos_lat * cos_d_lon) * r_earth;
    double z = ref.alt - gpsPosPlusAlt.alt;

    pos[0] = x;
    pos[1] = y;
    pos[2] = z;

    futurePos[0] = scaledVelocity[0]*timeInterval + pos[0];
    futurePos[1] = scaledVelocity[1]*timeInterval + pos[1];
    futurePos[2] = scaledVelocity[2]*timeInterval + pos[2];

    return futurePos;
  }
}
