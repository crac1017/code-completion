import java.io.File;

public class Files {
    /**
     * COMMENT:
     * Get all file names in the directory
     *
     * TEST:
     * import java.io.File;
     * import java.util.List;
     * import java.util.ArrayList;
     * __pliny_solution__
     * name_list = dfs("src/main/resources/benchmarks/files/test_dir");
     * print(
     * name_list.get(0).equals("bar.txt")  &&
     * name_list.get(1).equals("foo.txt")  &&
     * name_list.get(2).equals("test11.txt")  &&
     * name_list.get(3).equals("foo1.txt")  &&
     * name_list.get(4).equals("foo2.txt")  &&
     * name_list.get(5).equals("test1.txt")  &&
     * name_list.get(6).equals("test2.txt"));
     */
    public List dfs(String dirname) {
        List result = new ArrayList();

        File dir_file = new File(??);
        ??

        return result;
    }
}
