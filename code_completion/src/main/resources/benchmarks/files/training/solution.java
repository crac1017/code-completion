import java.io.File;

public class Files {
    /**
     * Get all file names in the directory
     */
    public List dfs(String dirname) {
        List result = new ArrayList();

        File dir_file = new File(dirname);
        File[] file_list = dir_file.listFiles();
        for(int i = 0; i < file_list.length; ++i) {
            File f = file_list[i];
            if(f.isDirectory()) {
                List new_files = dfs(f.getAbsolutePath());
                for(int j = 0; j < new_files.size(); ++j) {
                    result.add(new_files.get(j));
                }
            } else {
                result.add(f.getName());
            }
        }
        return result;
    }
}
