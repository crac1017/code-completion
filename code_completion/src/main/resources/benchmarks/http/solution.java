import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class HTTP {

    /**
     * COMMENT:
     * Creating a program with buttons and counters
     *
     * TEST:
     * import com.sun.net.httpserver.HttpExchange;
     * import com.sun.net.httpserver.HttpHandler;
     * import com.sun.net.httpserver.HttpServer;
     * import java.io.OutputStream;
     * __pliny_solution__
     * HttpServer server = http("src/main/resources/benchmarks/http/http_test.txt", 10808, "test");
     * source(new URL("http://localhost:10808/test"));
     * server.stop(0);
     */
    public HttpServer http(String filename, int port, String url) {
        String content;

        BufferedReader reader = new BufferedReader(new FileReader (filename));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");
        while((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        content = stringBuilder.toString().trim();

        HttpServer server;
        HttpHandler handler = new HttpHandler() {
            public void handle(HttpExchange he) throws IOException {
                he.sendResponseHeaders(200, content.length());
                OutputStream os = he.getResponseBody();
                os.write(content.getBytes());
                os.close();
            }
        };

        server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/" + url, handler);
        server.setExecutor(null); // creates a default executor
        server.start();
        return server;
    }
}
