import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class HTTP {

    /**
     * COMMENT:
     * Create an HTTP server which serves the content of a local file
     *
     * TEST:
     * import com.sun.net.httpserver.HttpExchange;
     * import com.sun.net.httpserver.HttpHandler;
     * import com.sun.net.httpserver.HttpServer;
     * import java.io.OutputStream;
     * __pliny_solution__
     * Random rand = new Random(System.currentTimeMillis());
     * integer p = rand.nextInt(20000) + 35000;
     * HttpServer server = http("src/main/resources/benchmarks/http/http_test.txt", p, "test");
     * source(new URL("http://localhost:" + Integer.toString(p) + "/test"));
     * server.stop(0);
     */
    public HttpServer http(String filename, int port, String url) {
        String content;

        /**
         * COMMENT:
         * Create an HTTP server which serves the content of a local file
         *
         * TEST:
         * String content;
         * String filename = "src/main/resources/benchmarks/http/http_test.txt";
         * __pliny_solution__
         * print(content.equals("print(true);"));
         */
        ??

        HttpServer server;
        HttpHandler handler = new HttpHandler() {
            public void handle(HttpExchange he) throws IOException {
   			  he.sendResponseHeaders(??, content.length());
              OutputStream os = he.getResponseBody();
              os.write(content.getBytes());
              os.close();
            }
        };

        ??
        return server;
    }
}
