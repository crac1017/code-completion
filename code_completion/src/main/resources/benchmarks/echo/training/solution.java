public class Echo {
    /**
     * COMMENT:
     * Echo server in java
     *
     * INPUT:
     * source("src/main/resources/benchmarks/echo/testlib.java");
     *
     * TEST:
     * source("src/main/resources/benchmarks/echo/test.java");
     */
    public void run() {
        int port = 4444;
        SServerSocket serverSocket = new SServerSocket(port);

        SSocket clientSocket = serverSocket.accept();

        SIn  in  = new SIn (clientSocket);
        SOut out = new SOut(clientSocket);

        String s;
        while ((s = in.readLine()) != null) {
            out.println(s);
        }

        out.close();
        in.close();
        clientSocket.close();
    }
}
