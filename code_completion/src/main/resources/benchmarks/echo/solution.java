public class Echo {
    /**
     * COMMENT:
     * Echo server in java
     *
     * TEST:
     * source("src/main/resources/benchmarks/echo/testlib.java");
     * __pliny_solution__
     * source("src/main/resources/benchmarks/echo/test.java");
     */
    public void run() {
        // create socket
        int port = 4444;
        SServerSocket serverSocket = new SServerSocket(port);
        // System.err.println("Started server on port " + port);

        // a "blocking" call which waits until a connection is requested
        SSocket clientSocket = serverSocket.accept();
        // System.err.println("Accepted connection from client");

        // open up IO streams
        SIn  in  = new SIn (clientSocket);
        SOut out = new SOut(clientSocket);

        // waits for data and reads it in until connection dies
        // readLine() blocks until the server receives a new line from client
        String s;
        while ((s = in.readLine()) != null) {
            out.println(s);
        }

        // close IO streams, then socket
        // System.err.println("Closing connection with client");
        out.close();
        in.close();
        clientSocket.close();
    }
}
