public class Echo {
    /**
     * COMMENT:
     * Echo server in java
     *
     * TEST:
     * source("src/main/resources/benchmarks/echo/testlib.java");
     * __pliny_solution__
     * source("src/main/resources/benchmarks/echo/test.java");
     */
    public void run() {
        // create socket
        int port = 4444;
        SServerSocket serverSocket = new SServerSocket(??);
        ??
    }
}
