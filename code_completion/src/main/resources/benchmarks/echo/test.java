run();
// print(has_server_sock);
// print(has_client_sock);
// print(has_in);
// print(has_out);
// print(has_readline);
// print(has_println);
// print(in_closed);
// print(out_closed);
// print(client_closed);
print(
    has_server_sock == true &&
    has_client_sock == true &&
    has_in == true &&
    has_out == true &&
    has_readline == true &&
    has_println == true &&
    in_closed == true &&
    out_closed == true &&
    client_closed == true);
