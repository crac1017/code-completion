boolean has_server_sock = false;
boolean has_client_sock = false;
boolean has_in = false;
boolean has_out = false;
boolean has_readline = false;
boolean has_println = false;
boolean in_closed = false;
boolean out_closed = false;
boolean client_closed = false;

public class SSocket {
    public SSocket() {
        has_client_sock = true;
    }

    public void close() {
        client_closed = true;
    }
}

public class SServerSocket {
    public int port;
    public SServerSocket(int p) {
        has_server_sock = true;
        this.port = p;
    }

    public SSocket accept() {
        return new SSocket();
    }

}

public class SIn {
    int count = 0;
    public SSocket sock;
    public SIn(SSocket s) {
        this.sock = s;
        has_in = true;
    }

    public String readLine() {
        has_readline = true;
        if(count < 3) {
            return Integer.toString(count++);
        } else {
            return null;
        }
    }

    public void close() {
        in_closed = true;
    }
}

public class SOut {
    public SSocket sock;
    public SOut(SSocket s) {
        has_out = true;
        this.sock = s;
    }

    public void println(String s) {
        has_println = true;
    }

    public void close() {
        out_closed = true;
    }
}

