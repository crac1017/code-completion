public class Floyd {
    /**
     * COMMENT:
     * This is the floyd warshall algorithm. All-pairs shortest path.
     *
     * TEST:
     __pliny_solution__
     integer NIL = 1000000;
     integer[][][] test_graph = {
     {{0, 5, NIL, NIL, NIL},
     {NIL, 0, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL}},
     {{0, 1, 10, NIL, NIL},
     {NIL, 0, 2, NIL, NIL},
     {NIL, NIL, 0, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL}},
     {{0, 2, 5, NIL, 8},
     {NIL, 0, NIL, NIL, 1},
     {NIL, NIL, 0, 4, NIL},
     {NIL, NIL, NIL, 0, NIL},
     {NIL, NIL, NIL, 3, 0}},
     {{0, 5, NIL, NIL, NIL},
     {3, 0, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL},
     {NIL, NIL, NIL, NIL, NIL}}};

     integer[][] test_loc = {
     {0, 1, 0}, // the third element is the index of the test graph
     {1, 0, 0},
     {0, 1, 1},
     {0, 2, 1},
     {1, 2, 1},
     {0, 2, 2},
     {0, 4, 2},
     {0, 3, 2},
     {0, 1, 3},
     {1, 0, 3}};

     integer[] test_sizes = {2, 2, 3, 3, 3, 5, 5, 5, 2, 2};

     integer[] ans = {5, NIL, 1, 3, 2, 5, 3, 6, 5, 3};
     integer[] result = {0};

        for(integer i = 0; i < 10; ++i) {
            integer start = test_loc[i][0];
            integer end = test_loc[i][1];
            integer size = test_sizes[i];
            integer result = floyd(test_graph[test_loc[i][2]], start, end, 5);
            if(result == ans[i]) {
            } else if(ans[i] > 100 && result > 100) {
            } else {
                print("false");
                exit();
            }
        }
       print("true");
     */
      public int floyd(int[][] dist, int start, int end, int n) {
      int i, j, k;

      for (k = 0; k < n; ++k) {
          for (i = 0; i < n; ++i) {
              for (j = 0; j < n; ++j) {
                  if (dist[i][k] + dist[k][j] < dist[i][j]) {
                      dist[i][j] = dist[i][k] + dist[k][j];
                  }
              }
          }
      }


      return dist[start][end];
    }
}
