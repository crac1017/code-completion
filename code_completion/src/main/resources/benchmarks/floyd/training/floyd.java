public class Floyd {
    /**
     * floyd warshall algorithm all pairs shortest path
     */
    public int floyd_warshall(int[][] dist, int start, int end, int n) {
        int i, j, k;

        for (k = 0; k < n; ++k) {
            for (i = 0; i < n; ++i) {
                for (j = 0; j < n; ++j) {
                    if (dist[i][k] + dist[k][j] < dist[i][j]) {
                        dist[i][j] = dist[i][k] + dist[k][j];
                    }
                }
            }
        }
        return dist[start][end];
    }
}
