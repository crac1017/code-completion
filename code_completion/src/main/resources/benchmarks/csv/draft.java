import java.io.File;
import java.util.Scanner;
public class CSV {
    /**
     * COMMENT:
     * Reading a matrix from a CSV file
     *
     * TEST:
     * import java.io.File;
     * import java.util.*;
     * __pliny_solution__
     *
     * int[][] mat1 = read_csv("src/main/resources/benchmarks/csv/data1.csv");
     * boolean _case_0_ =
     *   (mat1[0][0] == 1 && mat1[0][1] == 2 && mat1[0][2] == 3 && mat1[0][3] == 4 &&
     *    mat1[1][1] == 3 && mat1[2][2] == 5 && mat1[3][3] == 7 && mat1[4][3] == 8);
     *
     * boolean _result_ = (
     *   _case_0_
     * );
     */
    public int[][] read_csv(String filename, int row, int col) {
        int row, col;
        File f = new File(??);
        /**
         * COMMENT:
         * Reading a matrix from a CSV file
         *
         * TEST:
         * import java.io.File;
         * import java.util.*;
         * integer row, col;
         * File f = new File("src/main/resources/benchmarks/csv/data1.csv");
         * __pliny_solution__
         * boolean _result_ = (row == 5 && col == 4);
         */
        ??

				/*
        Scanner scanner = new Scanner(f);
        {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            row = Integer.parseInt(fields[0]);
            col = Integer.parseInt(fields[1]);
        }
				*/
        int[][] mat = new int[row][col];

        ??
        return mat;
    }
}
