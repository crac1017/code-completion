import java.io.File;
import java.util.Scanner;
public class CSV {
	/**
	 * Read a matrix from a CSV file
	 */
    public int[][] read_csv(String filename) {
        int[][] mat = new int[row][col];
        int row, col;

        File f = new File(filename);
        Scanner scanner = new Scanner(f);
        {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            row = Integer.parseInt(fields[0]);
            col = Integer.parseInt(fields[1]);
        }
        for(int i = 0; i < row; ++i) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            for(int j = 0; j < col; ++j) {
                mat[i][j] = Integer.parseInt(fields[j]);
            }
        }
        return mat;
    }
}
