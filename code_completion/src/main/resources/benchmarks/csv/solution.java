import java.io.File;
import java.util.Scanner;

public class CSV {
    /**
     * COMMENT:
     * Reading a csv file
     *
     * TEST:
     * import java.io.File;
     * import java.util.*;
     * __pliny_solution__
     * integer num_elements = 0;
     * integer[][] mat = read_csv("src/main/resources/benchmarks/csv/data1.csv");
     * for(integer i = 0; i < mat.length; ++i) {
     *   for(integer j = 0; j < mat[0].length; ++j) {
     *     if(i + j + 1 != mat[i][j]) {
     *       print("false");
     *       exit();
     *     }
     *     num_elements += 1;
     *   }
     * }
     * print(num_elements == 20);
     */
    public int[][] read_csv(String filename) {
        int row, col;

        File f = new File(filename);
        Scanner scanner = new Scanner(f);
        {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            row = Integer.parseInt(fields[0]);
            col = Integer.parseInt(fields[1]);
        }

        int[][] mat = new int[row][col];

        for(int i = 0; i < row; ++i) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            for(int j = 0; j < col; ++j) {
                mat[i][j] = Integer.parseInt(fields[j]);
            }
        }
        return mat;
    }
}