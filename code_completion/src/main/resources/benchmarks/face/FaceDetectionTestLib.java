boolean _rectangle_drawn_ = false;
boolean _face_detected_ = false;
boolean _has_detector_ = false;
boolean _has_image_ = false;
boolean _has_detection_ = false;
boolean _image_written_ = false;

public class System {
  public static void loadLibrary(String in) {}

}

public class Resource {
  public String myname;
  public Resource(String name) {
    this.myname = name;
  }

  public String getPath() {
    return this.myname;
  }
}

public class MyClass {
  public String my_class;
  public MyClass(String name) {
    this.my_class = name;
  }

  public MyClass() {
    this.my_class = "";
  }

  public Resource getResource(String name) {
    return new Resource(name);
  }
}

public MyClass getClass() {
  return new MyClass();
}

public class CascadeClassifier {

  public String myname;
  public CascadeClassifier(String path) {
    _has_detector_ = true;
    this.myname = path;
  }

  public void detectMultiScale(Mat img, MatOfRect mor) {
    if(!myname.equals("haarcascade_frontalface_alt.xml") && !myname.equals("lbpcascade_frontalface.xml")) {
      print("detectMultiScale: invalid XML input.");
      print("false");
      exit();
    }
    _face_detected_ = true;
  }
}

public class Mat {
  public String myname;
  public Mat(String path) {
    _has_image_ = true;
    this.myname = path;
  }
}

public class Highgui {
  public static Mat imread(String path) {
    return new Mat(path);
  }
  public static void imwrite(String filename, Mat img) {
    if(!_rectangle_drawn_) {
      print("imwrite: rectangle is not drawn yet.");
      exit();
    }
    _image_written_ = true;
  }

}

public class Rect {
  public int x, y, width, height;
  public Rect(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.width = w;
    this.height = h;
  }
}

public class Scalar {
  public int myr, myg, myb;
  public Scalar(int r, int g, int b) {
    this.myr = r;
    this.myg = g;
    this.myb = b;
  }
}

public class Point {
  public int px, py;
  public Point(int x, int y) {
    this.px = x;
    this.py = y;
  }
}

public class Core {
  public String input_img;
  public Point myp1;
  public Point myp2;
  public Scalar myscalar;
  public static void rectangle(Mat img, Point p1, Point p2, Scalar s) {
    if(p1.px >= p2.px || p1.py >= p2.py || (p1.px != 2 && p1.py != 1 && p1.px != 6 && p1.px != 8)) {
      print("rectangle: invalid points.");
      print("false");
      exit();
    }
    if(s.myg < 100 && s.myb < 100 && s.myr < 100){
      print("rectangle: invalid scalar.");
      print("false");
      exit();
    }
    _rectangle_drawn_ = true;
  }
  static public String NATIVE_LIBRARY_NAME = "NAME";
}


public class MatOfRect {
  public MatOfRect() {
    _has_detection_ = true;
  }

  public Rect[] toArray() {
    if(!_face_detected_) {
      print("toArray: face not detected.");
      print("false");
      exit();
    }

    return new Rect[] {new Rect(1, 1, 5, 5), new Rect(2, 2, 6, 6)};
  }
}
