public class Merge {
    /**
     * COMMENT:
     * Merge two sorted lists
     *
     * TEST:
     * __pliny_solution__
     int NIL = -1;
     public boolean array_equal(int[] a1, int[] a2) {
       if(a1.length != a2.length) { return false; }
       for(int i = 0; i < a1.length; ++i) {
         if(a1[i] != a2[i]) { return false; }
       }
       return true;
     }
     int[][] test_array = {{2, 6, 8, 10, 2, 6, 8, 10, NIL, NIL},
                           {-10, 7, 11, 14, -10, 7, 11, 14, NIL, NIL},
                           {1, 3, 5, 9, 3, 5, 9, NIL, NIL, NIL},
                           {0, 0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                           {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                           {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                           {6, 14, 15, 21, 27, 15, 21, 27, 33, 37},
                           {-1, 2, 6, 10, 40, -1, 10, 40, NIL, NIL},
                           {-10, -5, -5, 5, 10, -10, -5, 5, 5, 10},
                           {1, 3, 2, 3, 4, 5, NIL, NIL, NIL, NIL}};
     int[] test_left = {0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0};

     int[] test_mid = {3,
                       3,
                       3,
                       0,
                       8,
                       0,
                       4,
                       4,
                       4,
                       1};

     int[] test_right = {7,
                         7,
                         6,
                         1,
                         9,
                         9,
                         9,
                         7,
                         9,
                         5};

     int[][] test_ans = {{2, 2, 6, 6, 8, 8, 10, 10, NIL, NIL},
                         {-10, -10, 7, 7, 11, 11, 14, 14, NIL, NIL},
                         {1, 3, 3, 5, 5, 9, 9, NIL, NIL, NIL},
                         {0, 0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                         {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                         {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                         {6, 14, 15, 15, 21, 21, 27, 27, 33, 37},
                         {-1, -1, 2, 6, 10, 10, 40, 40, NIL, NIL},
                         {-10, -10, -5, -5, -5, 5, 5, 5, 10, 10},
                         {1, 2, 3, 3, 4, 5, NIL, NIL, NIL, NIL}};

     int[] test_ans_sizes = {8, 6, 7, 2, 10, 10, 10, 8, 10, 6};

     int[][] result = {{2, 6, 8, 10, 2, 6, 8, 10, NIL, NIL},
                       {-10, 7, 11, 14, -10, 7, 11, 14, NIL, NIL},
                       {1, 3, 5, 9, 3, 5, 9, NIL, NIL, NIL},
                       {0, 0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                       {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                       {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                       {6, 14, 15, 21, 27, 15, 21, 27, 33, 37},
                       {-1, 2, 6, 10, 40, -1, 10, 40, NIL, NIL},
                       {-10, -5, -5, 5, 10, -10, -5, 5, 5, 10},
                       {1, 3, 2, 3, 4, 5, NIL, NIL, NIL, NIL}};
     boolean _result_ = false;
     merge(result[0], test_left[0], test_mid[0], test_right[0]);
     boolean _case_0_ = array_equal(result[0], test_ans[0]);

     merge(result[1], test_left[1], test_mid[1], test_right[1]);
     boolean _case_1_ = array_equal(result[1], test_ans[1]);

     merge(result[2], test_left[2], test_mid[2], test_right[2]);
     boolean _case_2_ = array_equal(result[2], test_ans[2]);

     merge(result[3], test_left[3], test_mid[3], test_right[3]);
     boolean _case_3_ = array_equal(result[3], test_ans[3]);

     merge(result[4], test_left[4], test_mid[4], test_right[4]);
     boolean _case_4_ = array_equal(result[4], test_ans[4]);

     merge(result[5], test_left[5], test_mid[5], test_right[5]);
     boolean _case_5_ = array_equal(result[5], test_ans[5]);

     merge(result[6], test_left[6], test_mid[6], test_right[6]);
     boolean _case_6_ = array_equal(result[6], test_ans[6]);

     merge(result[7], test_left[7], test_mid[7], test_right[7]);
     boolean _case_7_ = array_equal(result[7], test_ans[7]);

     merge(result[8], test_left[8], test_mid[8], test_right[8]);
     boolean _case_8_ = array_equal(result[8], test_ans[8]);

     merge(result[9], test_left[9], test_mid[9], test_right[9]);
     boolean _case_9_ = array_equal(result[9], test_ans[9]);
     _result_ = (
       _case_0_ &&
       _case_1_ &&
       _case_2_ &&
       _case_3_ &&
       _case_4_ &&
       _case_5_ &&
       _case_6_ &&
       _case_7_ &&
       _case_8_ &&
       _case_9_ );
     */
    void merge(int[] a, int start, int mid, int end) {
        int i = start;
        int begin = start;
        int m = (mid + 1);
        int k;
        int[] b = new int[50];
        ??
				/*
        while((start <= mid) && (m <= end))
        {
            if(a[start] < a[m])
            {
                b[i] = a[start];
                start ++;
                i ++;
            }
            else
            {
                b[i] = a[m];
                m ++;
                i ++;
            }
        }
				*/
        if(m > end)
        {
            for((k = start); (k <= mid); (k ++))
            {
                b[(i ++)] = a[k];
            }
        }
        else
        {
            for((k = m); (k <= end); (k ++))
            {
                b[(i ++)] = a[k];
            }
        }
        for((i = begin); (i <= end); (i ++))
        {
            a[i] = b[i];
        }
    }

}
