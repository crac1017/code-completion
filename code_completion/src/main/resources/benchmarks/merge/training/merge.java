public class Merge {

    /**
     * Merge two sorted lists
     */
    void merge(int[] a, int start, int mid, int end) {
        int i = start;
        int begin = start;
        int m = mid + 1;
        int k;
        int[] b = new int[50];
        while(start <= mid && m <= end) {
            if(a[start] < a[m]) {
                b[i] = a[start];
                start++;
                i++;
            } else {
                b[i] = a[m];
                m++;
                i++;
            }
        }
        if(m > end) {
            for(k = start; k <= mid; k++) {
                b[i++] = a[k];
            }
        } else {
            for(k = m; k <= end; k++) {
                b[i++] = a[k];
            }
        }
        for(i = begin; i <= end; i++) {
            a[i] = b[i];
        }
    }
}
