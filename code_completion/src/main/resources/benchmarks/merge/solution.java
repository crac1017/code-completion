public class Merge {
    /**
     * COMMENT:
     * Merge two sorted lists
     *
     * TEST:
     * __pliny_solution__
     integer NIL = -1;
     public boolean array_equal(integer[] a1, integer[] a2) {
       if(a1.length != a2.length) { return false; }
       for(integer i = 0; i < a1.length; ++i) {
         if(a1[i] != a2[i]) { return false; }
       }
       return true;
     }
     integer[][] test_array = {{2, 6, 8, 10, 2, 6, 8, 10, NIL, NIL},
                           {-10, 7, 11, 14, -10, 7, 11, 14, NIL, NIL},
                           {1, 3, 5, 9, 3, 5, 9, NIL, NIL, NIL},
                           {0, 0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                           {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                           {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                           {6, 14, 15, 21, 27, 15, 21, 27, 33, 37},
                           {-1, 2, 6, 10, 40, -1, 10, 40, NIL, NIL},
                           {-10, -5, -5, 5, 10, -10, -5, 5, 5, 10},
                           {1, 3, 2, 3, 4, 5, NIL, NIL, NIL, NIL}};
     integer[] test_left = {0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0};

     integer[] test_mid = {3,
                       3,
                       3,
                       0,
                       8,
                       0,
                       4,
                       4,
                       4,
                       1};

     integer[] test_right = {7,
                         7,
                         6,
                         1,
                         9,
                         9,
                         9,
                         7,
                         9,
                         5};

     integer[][] test_ans = {{2, 2, 6, 6, 8, 8, 10, 10, NIL, NIL},
                         {-10, -10, 7, 7, 11, 11, 14, 14, NIL, NIL},
                         {1, 3, 3, 5, 5, 9, 9, NIL, NIL, NIL},
                         {0, 0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                         {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                         {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                         {6, 14, 15, 15, 21, 21, 27, 27, 33, 37},
                         {-1, -1, 2, 6, 10, 10, 40, 40, NIL, NIL},
                         {-10, -10, -5, -5, -5, 5, 5, 5, 10, 10},
                         {1, 2, 3, 3, 4, 5, NIL, NIL, NIL, NIL}};

     integer[] test_ans_sizes = {8, 6, 7, 2, 10, 10, 10, 8, 10, 6};

     integer[][] result = {{2, 6, 8, 10, 2, 6, 8, 10, NIL, NIL},
                       {-10, 7, 11, 14, -10, 7, 11, 14, NIL, NIL},
                       {1, 3, 5, 9, 3, 5, 9, NIL, NIL, NIL},
                       {0, 0, NIL, NIL, NIL, NIL, NIL, NIL, NIL, NIL},
                       {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                       {1, 4, 7, 19, 22, 33, 47, 73, 87, 92},
                       {6, 14, 15, 21, 27, 15, 21, 27, 33, 37},
                       {-1, 2, 6, 10, 40, -1, 10, 40, NIL, NIL},
                       {-10, -5, -5, 5, 10, -10, -5, 5, 5, 10},
                       {1, 3, 2, 3, 4, 5, NIL, NIL, NIL, NIL}};
     for(integer i = 0; i < 10; ++i) {
       merge(result[i], test_left[i], test_mid[i], test_right[i]);
       if(!array_equal(result[i], test_ans[i])) {
         print("false");
         exit();
       }
     }
     print("true");
     */
    void merge(int[] a, int start, int mid, int end) {
        int i = start;
        int begin = start;
        int m = (mid + 1);
        int k;
        int[] b = new int[50];
        while((start <= mid) && (m <= end))
        {
            if(a[start] < a[m])
            {
                b[i] = a[start];
                start ++;
                i ++;
            }
            else
            {
                b[i] = a[m];
                m ++;
                i ++;
            }
        }
        if(m > end)
        {
            for((k = start); (k <= mid); (k ++))
            {
                b[(i ++)] = a[k];
            }
        }
        else
        {
            for((k = m); (k <= end); (k ++))
            {
                b[(i ++)] = a[k];
            }
        }
        for((i = begin); (i <= end); (i ++))
        {
            a[i] = b[i];
        }
    }

}