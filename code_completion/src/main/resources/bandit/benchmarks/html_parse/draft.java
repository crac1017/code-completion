import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.lang.System;

public class HTML {
    public List get_links(String html_file, String website) {
        ArrayList result = new java.util.ArrayList();
        String content;
        BufferedReader reader = new BufferedReader(new FileReader (html_file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = java.lang.System.getProperty("line.separator");
        while((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        content = stringBuilder.toString();
        Pattern pattern = java.util.regex.Pattern.compile(website);

        String select_str = "a[href]";
        String attr_str = "href";
        /**
         * COMMENT:
         * get all links with a given website name from an html file
         *
         * TEST:
         * import org.jsoup.Jsoup;
         * import org.jsoup.nodes.Document;
         * import org.jsoup.nodes.Element;
         * import org.jsoup.select.Elements;
         * import java.util.regex.Matcher;
         * import java.util.regex.Pattern;
         * import java.util.ArrayList;
         * List result = new ArrayList();
         * String content;
         * BufferedReader reader = new BufferedReader(new FileReader("/Users/yanxin/Documents/work/rice/code-completion/src/main/resources/bandit/benchmarks/html/jsoup.html"));
         * String         line = null;
         * StringBuilder  stringBuilder = new StringBuilder();
         * String         ls = System.getProperty("line.separator");
         * while((line = reader.readLine()) != null) {
         *   stringBuilder.append(line);
         *   stringBuilder.append(ls);
         * }
         * content = stringBuilder.toString();
         * Pattern pattern = Pattern.compile("jsoup");
         * String select_str = "a[href]";
         * String attr_str = "href";
         * __pliny_solution__
         * boolean _result_ = result.contains("//try.jsoup.org/");
         */
        ??
        /*
        Document doc = Jsoup.parse(content);
        Elements links = doc.select("a[href]");

        for(Element link : links) {
            String l = link.attr("href");
            Matcher m = pattern.matcher(l);
            if(m.find()) {
                result.add(l);
            }
        }
        */
        return result;
    }
}
