import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class HTML {

    /**
     * Get all links with a given website name from an html file
     */
    public List get_links(String html_file, String website) {
        String content;
        List result = new ArrayList();

        BufferedReader reader = new BufferedReader(new FileReader (html_file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");
        while((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        content = stringBuilder.toString();

        Pattern pattern = Pattern.compile(website);

        Document doc = Jsoup.parse(content);
        Elements links = doc.select("a[href]");


        for(Element link : links) {
            String l = link.attr("href");
            Matcher m = pattern.matcher(l);
            if(m.find()) {
                result.add(l);
            }
        }
        return result;
    }
}
