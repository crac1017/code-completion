#!/usr/bin/python
import sys;
import re;
import json;
import math;
from nltk.corpus import words;
from nltk.corpus import stopwords;
from nltk.tokenize import word_tokenize;
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer

STEMMER = PorterStemmer()
LEMMATIZER = WordNetLemmatizer()
PREFIX_MIN_LEN = 3;
SUFFIX_MIN_LEN = 3;
ENG_WORDS = set(words.words());
ENG_STOPWORDS = set(stopwords.words("english"));
JAVA_STOPWORDS = set([
    'abstract',   'continue',   'for',          'new',         'switch',
    'assert,'     'default',    'if',           'package',     'synchronized',
    'boolean',    'do',         'goto',         'private',     'this',
    'break',      'double',     'implements',   'protected',   'throw',
    'byte',       'else',       'import',       'public',      'throws',
    'case',       'enum',       'instanceof',   'return',      'transient',
    'catch',      'extends',    'int',          'short',       'try',
    'char',       'final',      'interface',    'static',      'void',
    'class',      'finally',    'long',         'strictfp',    'volatile',
    'const',      'float',      'native',       'super',       'while',
    'fixme', 'todo', 'and', 'bitand', 'bitor', 'bool', 'decltype',
    'template', 'typename', 'xor', 'import']);
    
def longest_prefix(term):
    if len(term) <= PREFIX_MIN_LEN:
        return term;

    i = len(term);
    while i >= PREFIX_MIN_LEN:
        if term[0:i] in ENG_WORDS:
            return term[0:i];
        i -= 1;

def longest_suffix(term):
    if len(term) <= SUFFIX_MIN_LEN:
        return term;

    i = 0;
    while i <= len(term) - SUFFIX_MIN_LEN:
        if term[i : len(term)] in ENG_WORDS:
            return term[i : len(term)];
        i += 1;

def normal_split(term, result):
    prefix = longest_prefix(term);
    suffix = longest_suffix(term);

    if prefix is None and suffix is None:
        result.append(term);
    elif prefix is not None and suffix is None:
        result.append(prefix);
        normal_split(term[len(prefix):], result);
    elif prefix is None and suffix is not None:
        result.append(suffix);
        normal_split(term[:len(term) - len(suffix)], result);
    else:
        if len(prefix) >= len(suffix):
            result.append(prefix);
            term = term[len(prefix):];
        else:
            result.append(suffix);
            term = term[:(len(term) - len(suffix))];

        if len(term) > 0:
            normal_split(term, result);

    return result;

FEATURE ="""
[
{
    "Project Name": "ACME",
    "File Path": "Factory.java",
    "Method Name": "Factory",
    "Words": "foo foo three"
},
{
    "Project Name": "ACME",
    "File Path": "Factory2.java",
    "Method Name": "Factory",
    "Words": "bar two bar"
},
{
    "Project Name": "ACME",
    "File Path": "Factory1.java",
    "Method Name": "Factory",
    "Words": "foo one foo one one"
}
]

"""

def process_terms(term_list):
    """
    Preprocess all the terms. This step includes case splitting,
    stemming, lemmatization, etc.. This should return a list of terms
    that can be directly used for calculating TFIDF.
    """

    result_list = term_list

    # replace all non alphabetical char into underscore
    result_list = [re.sub("[^a-zA-Z]", '_', w) for w in result_list];

    # break the terms using underscores
    tmp_list = [];
    for t in result_list:
        s = re.split("_+", t);
        tmp_list.extend(s);

    result_list = [x for x in tmp_list if len(x) > 1];

    # if there are capital chars in a term, do camelcase splitting
    # otherwise, do greedy splitting
    tmp_list = [];
    for t in result_list:
        if re.search("[A-Z]", t) is None:
            tmp_list.extend(normal_split(t, []));
        else:
            s = re.sub("(.)([A-Z][a-z]+)", r"\1 \2", t);
            s = re.sub("([a-z])([A-Z])", r"\1 \2", s).lower().split();
            tmp_list.extend(s);
    
    result_list = [x for x in tmp_list if len(x) > 1];

    # remove stop words and java keywords 
    tmp_list = [];
    for t in result_list:
        if t not in ENG_STOPWORDS and t not in JAVA_STOPWORDS:
            s = STEMMER.stem(t);
            s = LEMMATIZER.lemmatize(s);
            tmp_list.append(s);
    
    result_list = [x for x in tmp_list if len(x) > 1];

    # TODO: weight more on the function names
    
    return result_list;

def gen_doc_string(proj_name, path, method_name):
    """
    Generate a single string to uniquely represent this program
    element for calculate TFIDF.
    """
    # return prog_element["File Path"];

    # return json.dumps(prog_element);

    return ";".join([proj_name, path, method_name]);

def get_tfidf(all_data):
    """
    Return a dictionary containing a set of words with their TFIDF.
    """
    final_term_freq = {};
    doc_freq = {};
    result_dict = {};

    for doc, term_list in all_data.items():
        term_freq = {};
        for t in term_list:
            # calculate term frequency
            tf = term_list.count(t);
            term_freq[t] = tf;

            if t not in doc_freq:
                doc_freq[t] = set([doc]);
            else:
                doc_freq[t].add(doc);

        tf_sum = 0.0;
        for term, tf in term_freq.items():
            tf_sum += tf * tf;
        tf_sum = math.sqrt(tf_sum);

        for term, tf in term_freq.items():
            tf_norm = tf / tf_sum;
            if doc not in final_term_freq:
                final_term_freq[doc] = {term : tf_norm};
            else:
                final_term_freq[doc][term] = tf_norm;

    # print "term freq:", final_term_freq;
    # print "doc freq:", doc_freq;
    # exit();
    for doc, term_dict in final_term_freq.items():
        for term, freq in term_dict.items():
            idf = math.log((float(len(all_data)) + 1.0) /
                           (float(len(doc_freq[term])) + 1.0)) + 1.0;
            # print "term '%s' in %s: %.4f * %.4f (%d / %d = %.4f)" % (term, doc, freq, idf, len(all_data), len(doc_freq[term]), math.log(len(all_data) / len(doc_freq[term])));
            if doc in result_dict:
                result_dict[doc][term] = freq * idf;
            else:
                result_dict[doc] = {term : freq * idf};

    return result_dict;


def gen_nlterms(input_data_dict):
    """
    Main function for generating weighted NL terms features for a
    program, given dictionary where the key is a program element ID
    under a project and the value is a list of terms. Notice that all
    the program element IDs assume to be under the same project for
    calculating document frequency.
    """


    data_dict = {};
    for doc, term_list in input_data_dict.items():

        # preprocess all the terms
        terms = process_terms(term_list);

        data_dict[doc] = terms;
    
    return get_tfidf(data_dict);

def main():
    if len(sys.argv) < 2:
        sys.exit("Must provide a file name containing the input JSON.");
        
    input_json_txt = open(sys.argv[1]).read();
    json_data = json.loads(input_json_txt);
    result_dict = {};
    for feature in json_data:
        key = gen_doc_string(feature["Project Name"], feature["File Path"], feature["Method Name"]);
        j = feature["Words"];
        result_dict[key] = j;

    weights_dict = gen_nlterms(result_dict);
    result = [];
    for k, v in weights_dict.items():
        [proj_name, path, method_name] = k.split(";");
        weights = v;
        result.append({"Project Name" : proj_name,
                       "File Path" : path,
                       "Method Name": method_name,
                       "Weights" : weights});

    print json.dumps(result);

def main_test():
    json_data = json.loads(FEATURE);
    result_dict = {};
    for feature in json_data:
        key = gen_doc_string(feature["Project Name"], feature["File Path"], feature["Method Name"]);
        j = feature["Words"].split(" ");
        result_dict[key] = j;

    weights_dict = gen_nlterms(result_dict);
    result = [];
    for k, v in weights_dict.items():
        [proj_name, path, method_name] = k.split(";");
        weights = v;
        result.append({"Project Name" : proj_name,
                       "File Path" : path,
                       "Method Name": method_name,
                       "Weights" : weights});

    print json.dumps(result);

if __name__ == '__main__':
    # main_test();
    main();
