# API refactoring user study

## Get started
There are 8 user study problems, and they correspond to the folders in `problems`:
1. `csv`
2. `email_check`
3. `ftp`
4. `graphics` - No good Bayou results.
5. `htm`
6. `httpserver`
7. `ml` - No good Bayou results.
8. `pdf_read`

There are other folders like `la`, `old` and `secret`, but they are
not used. To run a user study problem, we need to start a web server
and a bayou synthesis server.

## Web server
This is the web app where the participant will write their code and
run their code. To start the web app, `cd` into the root folder and
run: `start_refactor_server.sh <port>` where `<port>` is the port
number that is used to run the web server. I suggest you putting
`17171` so that no one else is using the port. Suppose you run the web
server on `ganymede`, open the web browser and open
`ganymede.cs.rice.edu:17171/task/?task=csv`. This will open the CSV
user study problem.

The left panel is the code editor, and the right panel shows the
synthesis results. Test results are shown at the bottom of the page.

The web server code is at
`code_completion/src/main/scala/edu/rice/pliny/apitrans/WebMain.scala`
and the web stuffs is at
`code_completion/src/main/resources/api-refactoring-web`.

The input code for the participants to work on is at 
`code_completion/src/main/resources/api-refactoring-web/problems/<problem folder>/problem.java`.
Correct answer is at
`code_completion/src/main/resources/api-refactoring-web/problems/<problem folder>/<problem folder>.java`.

This web server has two functions: 
1. refactoring code using `Run` button
2. Run code using `Test` button.

## Bayou synthesis server
When a user hits the `Run` button, the code in the editor will be
analyzed and the block that looks like this:
```
//refactor:org.apache.pdfbox
{
    PdfReader reader = new PdfReader(filename);
    String text = PdfTextExtractor.getTextFromPage(reader, 1);
    reader.close();
    return text;
}
```
will be refactored. If there are multiple refactor blocks, it will
refactor the first one. API translation will be done and Bayou evidence
will be extracted and sent to Bayou server. So Bayou server MUST be
running with the correct dataset.

To run Bayou server, start my docker container `bayou-yl64`. In the
`root` folder, run
`./start_server.sh <model>` where `model` is the folder name that
contains the model for api synthesis. Here are the names of model
you should use for each user study problem:
1. `csv` (model name) - `csv` (user study problem name)
2. `email` - `email_check`
3. `ftp` - `ftp`
4. `graphics` - `graphics`
5. `htm` - `html`
6. `httpserver` - `httpserver`
7. `ml` - `ml`
8. `pdf_read` - `pdf`

Everytime you start a new user study problem, you must stop the
previous one and start a new one with the new model and wait for the
bayou server to be ready:
```
===================================
            Bayou Ready
===================================
 * Serving Flask app "ast_server" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 ```

If model is misused, participants will not get back meaningful
synthesized code. For `graphics` and `ml`, even if you give the
correct model, it won't be able to give good bayou results. This is
expected. 
 
Normally, it takes around 1 min to get back Bayou results. Be
patient!
 
## Run code
When a user hits the `Test` button, the web server will use
`BeanShell` to run the code. `BeanShell` is an Java interpreter and it
does things differently. You MUST obey the rules here:
1. DO NOT use generics
2. DO NOT use libraries newer than java 7
3. DO NOT declare variables without assigning values. 
```
// BeanShell will give "Type declaration" error for this one
Object foo;`

// You need to assign it a value, even it's null
Object bar = null;
```

A lot of the times `BeanShell` will just give `Type declaration error`
without telling what the error is. Here are the common problems:
1. Did not import necessary libraries
2. Declare variables without assigning values
3. Syntax error: no semi-colon.

`BeanShell`'s error message is not good. Scroll down to the bottom to
see the useful messages.

The tests for each problem is in `<problem folder>/test.java`. You can
go to
`code_completion/src/main/resources/api-refactoring-web/problems` and
run
`./run_test.sh <problem folder name>` to run the test locally. You
probably need to change the `test.java` code for some input filename
path such as `csv`.

`email`, `ftp`, `graphics` and `httpserver` use API specification to
check correctness. The rests use IO tests.

## Common errors for running code in different user study problems

### `csv`
None

### `email_check`
None

### `ftp`
1. Remember to remove `//refactor:...` for the method you don't want
   to refactor.
2. Read the comments and correct answer to understand how `put`, `get`
   function work.

### `graphics`
1. Remember to remove all the imports when running a solution against
   the test. We mock those imported libraries, so you need to remove
   them so that the real libraries don't get imported.
2. Bayou doesn't work for this one. Let's see what the participants
   will come up.

### `htm`
None

### `httpserver`
None

### `ml`
1. Bayou doesn't work for this one.
2. `Smile` and `Jsat` have the same class name for the models we use
   such as `LogisticRegression` and `RidgeRegression`. You CANNOT
   import both. MAKE SURE you only have one. Otherwise, Bayou will not
   work and testing will not work.

### `pdf_read`
1. The synthesized code might bring in new exceptions that you need to
   import. Otherwise, running the test will be failure.
   
   
## Things you need to record for each user study session
1. Programming time
2. Number of times they ask `Bayou` to synthesize code.
3. Ask participants: is this tool helpful? why?
4. Ask participants: what can be improved about this tool to make
   programming easier? why?
5. You need to write down what is the thing they spend the most their
   time doing. Are they spending time mostly on waiting for synthesis
   results? Are they mostly working on getting the test cases right?

