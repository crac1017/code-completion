import org.la4j.Matrix;
import org.la4j.matrix.DenseMatrix;

public class LATest {

    public static boolean forwardUnitTest() {
        int batchSize = 32;
        int outputDim = 5;
        int inputDim = 10;
        DenseMatrix input = DenseMatrix.constant(batchSize,inputDim, 1.0);
        DenseMatrix weight = DenseMatrix.constant(outputDim, inputDim, 0.5);
        DenseMatrix bias = DenseMatrix.constant(outputDim, 1,1.0);
        Matrix output = FCLayer.forward(input,weight,bias);

        if(!TestUtils.isIntEq(output.rows(), batchSize)) {
            return false;
        }

        if(!TestUtils.isIntEq(output.columns(), outputDim)) {
            return false;
        }

        for(int i = 0; i < output.rows(); i++){
            for(int j = 0; j < output.columns(); j++){
                double value = output.get(i,j);
                if(!TestUtils.isDblEq(value, 6.0, 0.0001)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean backwardUnitTest() {
        int batchSize = 32;
        int outputDim = 5;
        int inputDim = 10;
        DenseMatrix input = DenseMatrix.constant(batchSize,inputDim, 1.0);
        DenseMatrix weight = DenseMatrix.constant(outputDim, inputDim, 0.5);
        DenseMatrix bias = DenseMatrix.constant(outputDim, 1,1.0);
        DenseMatrix grad_output = DenseMatrix.constant(batchSize,outputDim, 0.1);
        Object[] results = FCLayer.backward(input,weight,bias,grad_output);
        print("FOO");
        Matrix grad_input = (Matrix) results[0];
        Matrix grad_weight = (Matrix) results[1];
        Matrix grad_bias = (Matrix) results[2];

        if(!TestUtils.isIntEq(grad_input.rows(), batchSize)) {
            return false;
        }

        if(!TestUtils.isIntEq(grad_input.columns(), inputDim)) {
            return false;
        }
        for(int i = 0; i < grad_input.rows(); i++){
            for(int j = 0; j < grad_input.columns(); j++){
                double value = grad_input.get(i,j);
                if(!TestUtils.isDblEq(value, 0.25, 0.0001)) {
                    return false;
                }
            }
        }

        if(!TestUtils.isIntEq(grad_weight.rows(), outputDim)) {
            return false;
        }

        if(!TestUtils.isIntEq(grad_weight.columns(), inputDim)) {
            return false;
        }
        for(int i = 0; i < grad_weight.rows(); i++){
            for(int j = 0; j < grad_weight.columns(); j++){
                double value = grad_weight.get(i,j);
                if(!TestUtils.isDblEq(value, 3.2, 0.0001)) {
                    return false;
                }
            }
        }
        if(!TestUtils.isIntEq(grad_bias.rows(), outputDim)) {
           return false;
        }
        if(!TestUtils.isIntEq(grad_bias.columns(), 1)) {
            return false;
        }
        for(int i = 0; i < grad_bias.rows(); i++){
            double value = grad_bias.get(i,0);
            if(!TestUtils.isDblEq(value, 3.2, 0.0001)) {
                return false;
            }
        }
        return true;
    }
}

boolean _result_ = LATest.forwardUnitTest() && LATest.backwardUnitTest();