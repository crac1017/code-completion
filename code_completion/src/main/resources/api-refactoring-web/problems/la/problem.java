import Jama.*;

//Library link: https://math.nist.gov/javanumerics/jama/

public class FCLayer {

    /*
     *  The forward propagation computation of the fully connected layer:
     *  Y = X * W ^ T + B
     *
     *  @input: input activation X of shape: (batch_size, input_dim)
     *  @weight: weight matrix W of shape: (output_dim, input_dim)
     *  @bias: bias vector B of shape: (output_dim, 1)
     *
     *  return:
     *  @output: output activation Y of shape: (batch_size, output_dim)
     */
    static public Matrix forward(Matrix input, Matrix weight, Matrix bias, Matrix ones){
        Matrix result;

        //refactor:org.la4j
        {
            Matrix step1 = weight.transpose();
            Matrix step2 = input.times(step1);
            Matrix bias_transpose = bias.transpose();
            Matrix step3 = ones.times(bias_transpose);
            result = step2.plus(step3);
        }
        return result;
    }


    /*
     *  The backward propagation computation of the fully connected layer:
     *  dX = dY * W
     *  dW = dY ^ T * X
     *  dB : row wise sum of dY
     *
     *  @input: input activation X of shape: (batch_size, input_dim)
     *  @weight: weight matrix W of shape: (output_dim, input_dim)
     *  @bias: bias vector B of shape: (output_dim)
     *  @grad_output: the gradient of the output dY from the last layer of shape: (batch_size, output_dim)
     *
     *  return a list of Ojbects that includes:
     *  @grad_input: the gradient of the input dX: (batch_size, input_dim)
     *  @grad_weight: the gradient of the weight dW: (output_dim, input_dim)
     *  @grad_bias: the gradient of the bias dB: (output_dim)
     */
    static public Object[] backward(Matrix input, Matrix weight, Matrix bias, Matrix grad_output, Matrix ones){
        Matrix grad_input, grad_weight, grad_bias;

        //refactor:org.la4j
        {
            grad_input = grad_output.times(weight);
            Matrix grad_weight_transpose = grad_output.transpose();
            grad_weight = grad_weight_transpose.times(input);
            Matrix grad_ouput_transpose = grad_output.transpose();
            grad_bias = grad_ouput_transpose.times(ones);
        }
        return new Object[]{grad_input, grad_weight, grad_bias};
    }
}