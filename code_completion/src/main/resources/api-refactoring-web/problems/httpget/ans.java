import okhttp3.Call;
import okhttp3.OkHttpClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;

import java.io.IOException;

public class HTTPGet {
    static public String get(URL url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        okhttp3.Request.Builder builder = new okhttp3.Request.Builder();
        builder.url(url);
        okhttp3.Request request = builder.build();
        Call call = client.newCall(request);
        okhttp3.Response response = call.execute();
        okhttp3.ResponseBody rbody = response.body();
        String text = rbody.string();
        return text;
    }
}
