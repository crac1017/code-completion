import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import okhttp3.Call;
import okhttp3.OkHttpClient;

public class HTTPGet {
    static public String get(URL url) throws IOException {
        //refactor:okhttp
        {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            InputStream rd = conn.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(rd));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            String result = response.toString();
            return result;
        }
    }
}
