import org.junit.Test;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;

public class HTTPGetTest {
    @Test
    public void getTest() throws IOException {
        String url = "https://httpbin.org/get";
        String response = HTTPGet.get(url);
        System.out.println(response);
        assertTrue(response.contains("\"Host\": \"httpbin.org\""));
    }
}
