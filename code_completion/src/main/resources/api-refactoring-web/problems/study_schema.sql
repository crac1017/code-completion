DROP TABLE IF EXISTS study;
CREATE TABLE IF NOT EXISTS study (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    session TEXT NOT NULL,
    task TEXT NOT NULL,
    code TEXT NOT NULL,
    duration INTEGER NOT NULL,
    timeout INT NOT NULL,
    compiled INT NOT NULL,
    correct INT NOT NULL,
    use_tool INT NOT NULL
);
