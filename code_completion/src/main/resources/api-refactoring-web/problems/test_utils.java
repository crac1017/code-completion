public class TestUtils {
    static public boolean isIntEq(int test, int expected) {
        boolean r = (test == expected);
        print("result: " + test + "\texpected: " + expected);
        if(!r) {
            print("Incorrect");
        }
        print("");
        return r;
    }

    static public boolean isBoolEq(boolean test, boolean expected) {
        boolean r = (test == expected);
        print("result: " + test + "\texpected: " + expected);
        if(!r) {
            print("Incorrect");
        }
        print("");
        return r;
    }


    static public boolean isStrEq(String test, String expected) {
        boolean r = (test.equals(expected));
        print("result: " + test + "\texpected: " + expected);
        if(!r) {
            print("Incorrect");
        }
        print("");
        return r;
    }

    static public boolean isStrNotEq(String test, String expected) {
        boolean r = !(test.equals(expected));
        print("result: " + test + "\tnot equal: " + expected);
        if(!r) {
            print("Incorrect");
        }
        print("");
        return r;
    }

    static public boolean isDblEq(double test, double expected, double range) {
        boolean r = (Math.abs(expected - test) <= range);
        print("result: " + test + "\texpected: " + expected);
        if(!r) {
            print("Incorrect");
        }
        print("");
        return r;
    }

    static public boolean isStrArrayEq(String[] test, String[] expected) {
        print("result length: " + test.length + "\texpected length: " + expected.length);
        if(test.length != expected.length) {
            print("Two array lengths are different");
            return false;
        }

        for(int i = 0; i < test.length; ++i) {
            print("---------------------");
            print("result  [" + i + "] = " + test[i]);
            print("expected[" + i + "] = " + expected[i]);
            if(!test[i].equals(expected[i])) {
                print("Values are different.");
                return false;
            }
            print("");
        }
        return true;
    }
}
