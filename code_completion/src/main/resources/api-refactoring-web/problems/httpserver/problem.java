import com.sun.net.httpserver.HttpExchange;
import org.glassfish.grizzly.http.server.*;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

// refactor using org.glassfish
// URL: https://javaee.github.io/grizzly/httpserverframework.html
public class HServer {

    /* original code
    {
        InetSocketAddress address = new InetSocketAddress(port);
        HttpServer server = HttpServer.create(address, 0);
        server.createContext(url, handler);
        server.start();
    }
    */

    /**
     * Set up an http server on localhost using the given port and the endpoint. Then register
     * the given http handler to this endpoint. No need to stop the server.
     */
    static public HttpServer run(int port, String endpoint, HttpHandler handler) throws IOException {
        {
            // TODO: Write the code similar to the original code
        }
    }
}
