import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class HServerTest {
    private String getHTML(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }

    @Test
    public void testServer() throws Exception {
        HttpHandler handler = new HttpHandler() {
            public void service(Request request, Response response) throws Exception {
                final String msg = "Hello server!";
                response.setContentType("text/plain");
                response.setContentLength(msg.length());
                response.getWriter().write(msg);
            }
        };

        ServerSocket s = new ServerSocket(0);
        int port = s.getLocalPort();
        s.close();
        HttpServer server = HServer.run(port, "/foo", handler);
        String res = this.getHTML("http://localhost:" + port + "/foo");
        server.shutdownNow();
        assertEquals("Hello server!", res);
    }
}
