import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.ServerConfiguration;

public class HServer{

    static public HttpServer run(int port, String endpoint, HttpHandler handler) {
        HttpServer server = HttpServer.createSimpleServer(null, "localhost", port);
        ServerConfiguration config = server.getServerConfiguration();
        config.addHttpHandler(handler, endpoint);
        server.start();
        return server;
    }
}
