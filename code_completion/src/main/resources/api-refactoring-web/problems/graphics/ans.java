import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Image;

public class GraphicsImg {
    static public void graphics_img(Graphics g, int rx, int ry, int rw, int rh, int x, int y, Image img) {
        g.drawRect(rx, ry, rw, rh);
        g.drawImage(img, x, y);
    }
}
