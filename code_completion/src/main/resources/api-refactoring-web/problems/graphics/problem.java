import java.io.File;
import java.io.IOException;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Image;

// refactor the program using org.newdawn
// URL: https://github.com/ariejan/slick2d
public class GraphicsImg {

    /* original code
    {
        import java.awt.Graphics2D;
        import java.awt.image.BufferedImage;
        import javax.imageio.ImageIO;
        File img_file = new File(filename);
        g2d.drawRect(rx, ry, rw, rh);
        BufferedImage img = ImageIO.read(img_file);
        g2d.drawImage(img, x, y, null);
    }
    */

    /**
     * draw a rectangle at (rx, ry) with (width, height) to be (rw, rh).
     * Then draw the given image at (x, y)
     * @param g - graphics object.
     * @param rx - rectangle's x
     * @param ry - rectangle's y
     * @param rw - rectangle's width
     * @param rh - rectangle's height
     * @param x - image's x
     * @param y - image's y
     * @param img - image to be drawn
     */
  static public void graphics_img(Graphics g, int rx, int ry, int rw, int rh, int x, int y, Image img) throws SlickException{
      {
          // TODO: write the code here similar to the original code
      }
  }
}
