import org.junit.Test;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static org.junit.Assert.*;

public class CSVTest {

    @Test
    public void readRowUnitTest() throws IOException {
        // String filename = "code_completion/src/main/resources/api-refactoring-web/problems/csv/in.csv";
        String filename = "in.csv";
        String[] fields = CSV.readRow(filename);

        String[] expected = new String[]{"field0",
            "field1",
            "field2",
            "field3",
            "field4"
        };

        assertArrayEquals(expected, fields);
    }


    @Test
    public void writeRowUnitTest() throws IOException {
        String[] entries = new String[]{"entry1", "entry2", "entry2", "entry2"};
        String filename = "out.csv";
        CSV.writeRow(filename, entries);
        FileReader fr = new FileReader(filename);
        BufferedReader scanner = new BufferedReader(fr);
        String line = scanner.readLine();
        String[] test_entries = line.split(",");
        assertArrayEquals(entries, test_entries);
    }
}
