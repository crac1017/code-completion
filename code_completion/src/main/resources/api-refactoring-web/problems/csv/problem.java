import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;

// refactor using com.opencsv;
// URL: http://opencsv.sourceforge.net/
public class CSV {

    /* Original code
    {
        BufferedReader scanner = new BufferedReader(fr);
        String line = scanner.readLine();
        String[] fields = line.split(",");
        return fields;
    }
    */

    /**
     * read a single row from the given csv file and return the row content
     * @param filename - an input csv file
     * @return the first row content
     */
    static public String[] readRow(String filename) throws IOException {
        File f = new File(filename);
        FileReader fr = new FileReader(f);

        {
            // TODO: complete this block using the original code as reference
        }
    }


    /* original code
    {
        for(int i = 0; i < entries.length; ++i) {
            if(i > 0) {
                fw.write(",");
            }
            fw.write(entries[i]);
        }
        fw.close();
    }
    */

    /**
     * Write the given entries into the csv file as the first row with no quotes.
     * @param filename - output csv filename
     * @param entries - first row content to be written
     */
    static public void writeRow(String filename, String[] entries) throws IOException {
        FileWriter fw = new FileWriter(filename);

        {
            // TODO: complete this block using the original code as reference
        }
    }
}
