import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import org.junit.Test;
import org.mockito.InOrder;
import java.io.IOException;
import static org.mockito.Mockito.*;

public class FTPTest {


    @Test
    public void testLogin() throws IOException {
        SSHClient ssh = mock(SSHClient.class);
        String username = "demo";
        String password = "password";
        String host = "test.rebex.net";
        FTP.login(username, password, host, ssh);
        InOrder inOrder = inOrder(ssh);
        inOrder.verify(ssh).authPassword(username, password);
        inOrder.verify(ssh).connect(host);
        inOrder.verify(ssh).disconnect();
    }

    @Test
    public void testList() throws IOException {
        SSHClient ssh = mock(SSHClient.class);
        SFTPClient sftp = mock(SFTPClient.class);
        String username = "demo";
        String password = "password";
        String host = "ftp://test.rebex.net/";
        String path = "/pub/example/";
        FTP.list_files(username, password, host, path, ssh, sftp);
        InOrder inOrder = inOrder(ssh, sftp);
        inOrder.verify(ssh).authPassword(username, password);
        inOrder.verify(ssh).connect(host);
        inOrder.verify(sftp).ls(path);
        inOrder.verify(sftp).close();
        inOrder.verify(ssh).disconnect();

        /*
        Set<String> fileset = new TreeSet<>();
        Collections.addAll(fileset, files);
        assertTrue(fileset.contains("ConsoleClient.png"));
        assertTrue(fileset.contains("pocketftp.png"));
        assertTrue(fileset.contains("readme.txt"));
        assertTrue(fileset.contains("WinFormClientSmall.png"));
        */
    }
}
