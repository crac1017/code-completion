import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.RemoteResourceInfo;
import net.schmizz.sshj.sftp.SFTPClient;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//use schmizz sshj library: net.schmizz
// URL: https://github.com/hierynomus/sshj
public class FTP {

    /* original code
        {
            import org.apache.commons.mail.EmailException;
            import org.apache.commons.net.ftp.FTPClient;
            import org.apache.commons.net.ftp.FTPFile;
            FTPClient f = new FTPClient();
            f.connect(host);
            f.login(username, password);
            f.disconnect();
        }
     */

    /**
     * Log into the FTP server using the username and password. Then disconnect.
     */
    static public void login(String username, String password, String host, SSHClient ssh) throws IOException {
        {
            // TODO: write a program similar to the original code
        }
    }


    /* original code
        {
            import org.apache.commons.mail.EmailException;
            import org.apache.commons.net.ftp.FTPClient;
            import org.apache.commons.net.ftp.FTPFile;
            FTPClient f = new FTPClient();
            f.connect(host);
            f.login(username, password);
            f.listFiles(path);
            f.disconnect();
        }
     */

    /**
     * Log into the FTP server and call the function that lists all the files in the given path.
     * Just call the function that list the files. No need to assign the results to a variable.
     * @param username - username
     * @param password - password
     * @param host - hostname
     * @param path - we want to list the files in this given path
     */
    static public void list_files(String username, String password, String host, String path, SSHClient ssh, SFTPClient ftp) throws IOException {
        {
            // TODO: write a program similar to the original code
        }
    }

}
