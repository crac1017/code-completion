import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;

/**
 * README:
 * This is a warm up problem. The purpose of this problem is to let you undersand what you will need to to
 * in this user study.
 */

/**
 * STEP 1: Understand the new library information such as package name and its website
 *
 * From the comment below, you need to rewrite the methods using a new library indicated by 'org.apache.pdfbox'
 */
// rewrite using org.apache.pdfbox
// URL: https://pdfbox.apache.org/
public class PDFRead {

    /**
     * STEP 2: Understand the original code
     *
     * The original code uses PdfReader to read the content of the given pdf file and returns the content as
     * string, assuming all the given pdf file only has one page.
     */
    /* original code
    {
        import com.itextpdf.text.pdf.PdfReader;
        import com.itextpdf.text.pdf.parser.PdfTextExtractor;
        PdfReader reader = new PdfReader(filename);
        String text = PdfTextExtractor.getTextFromPage(reader, 1);
        reader.close();
        return text;
    }
     */
    /**
     * Read the content of the given pdf file and return the content as string.
     * @param filename - a pdf file
     * @return the content as string
     */
    static public String read(String filename) throws IOException {
        /**
         * STEP 3: complete the block with TODO using the new library so that the resulting method
         * does the same thing as the original code.
         *
         * You may need to use the internet to find relevant code examples to help you.
         * Once you finish writing the code, hit 'Test' to run test cases so make sure the code is correct.
         * Once the solution is correct, you can move on to the next problem
         */
        {
            // TODO: Read the given pdf file using org.apache.pdfbox
        }
    }
}
