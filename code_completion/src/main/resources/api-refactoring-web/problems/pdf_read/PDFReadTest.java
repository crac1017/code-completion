import org.junit.Test;

import java.io.IOException;
import static org.junit.Assert.*;

public class PDFReadTest {

    @Test
    public void readTest() throws IOException {
        String filename = "foobar.pdf";
        String content = PDFRead.read(filename);
        assertEquals("foobar", content.trim());
    }
}

