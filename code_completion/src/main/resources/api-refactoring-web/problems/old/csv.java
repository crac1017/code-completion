public class Main {
    /**
     *  Parse the CSV file and get the first row of data
     */
    public String[] read_csv(String filename) {
        File f = new File(filename);
        FileReader fr = new FileReader(f);

        /**
         * Refactor the block using OpenCSV
         * target package: com.opencsv
         */

        {
            BufferedReader scanner = new BufferedReader(fr);
            String line = scanner.readLine();
            String[] fields = line.split(",");
        }

        return fields;
    }
}
