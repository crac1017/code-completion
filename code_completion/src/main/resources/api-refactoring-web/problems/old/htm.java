import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;
import java.lang.System;


public class HTMLLink {

    /**
     * Parse the HTML and get the first link.
     * @param content - content of this HTML
     * @param attr - The attribute string we use to extract the link. attr = "href"
     * @param selector - JSoup's selector for selecting "a" tags.
     */
    public void get_link(String content, String attr, String selector) {

        /**
         * Refactor the block using JSoup
         * target library : org.jsoup
         */

        {
            HtmlCleaner cleaner = new HtmlCleaner();
            TagNode node = cleaner.clean(content);
            TagNode[] links = node.getElementsHavingAttribute(attr, true);
            TagNode link = links[0];
            String href = link.getAttributeByName(attr);
        }
    }
}
