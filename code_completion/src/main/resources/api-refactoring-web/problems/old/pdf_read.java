import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;


public class PDFRead {
    /**
     * Extract the text from the given pdf file
     */
    public void read(String filename) {

        /**
         * Refactor the block using Apache's PDFBox
         * target library : org.apache.pdfbox
         */

        {
            PdfReader reader = new PdfReader(filename);
            String text = PdfTextExtractor.getTextFromPage(reader, 1);
            reader.close();
        }
    }
}
