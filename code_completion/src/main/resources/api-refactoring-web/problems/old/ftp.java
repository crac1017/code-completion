import org.apache.commons.mail.EmailException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

public class FTPList {
    /**
     * Connect to an FTP server and list the files under the given path.
     * @param username - username for login
     * @param password - password for login
     * @param host - we want to connect to this hostname
     * @param path - we want to list the files under this path
     */
    public void list_files(String username, String password, String host, String path) {

        /**
         * Refactor the block using schmizz's sshj library
         * target package : net.schmizz
         */

        {
            FTPClient f = new FTPClient();
            f.connect(host);
            f.login(username, password);
            FTPFile[] files = f.listFiles(path);
            f.disconnect();
        }
    }
}
