import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class HttpServerBench {

    /**
     * Create a simple http server and add the given http handler.
     */
    public void run() {
        int port = 8000;
        String url = "/test";
        HttpHandler handler = new HttpHandler();

        /**
         * Refactor the block using project Grizzly's httpserver.
         * No need to specify the port and the url.
         * target library : org.glassfish.grizzly
         */

        {
            InetSocketAddress address = new InetSocketAddress(port);
            HttpServer server = HttpServer.create(address, 0);
            server.createContext(url, handler);
            server.start();
        }
    }
}
