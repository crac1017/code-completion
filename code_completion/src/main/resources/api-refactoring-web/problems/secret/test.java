import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class SecretTest {
    public static boolean testEncrypt() throws IOException {
        String secret = "12345678";
        String plain = "plain";
        String encryptedString = Secret.encrypt(plain, secret) ;
        return TestUtils.isStrNotEq(encryptedString, plain);
    }

    public static boolean testDecrypt() throws IOException {
        String secret = "12345678";
        String plain = "plain";
        String encryptedString = Secret.encrypt(plain, secret);
        String decryptedString = Secret.decrypt(encryptedString, secret);
        return TestUtils.isStrEq(decryptedString, plain);
    }
}

boolean _result_ = SecretTest.testEncrypt() && SecretTest.testDecrypt();
