import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.aead.AeadKeyTemplates;
import com.google.crypto.tink.Aead;
import com.google.crypto.tink.aead.AeadFactory;

import java.security.*;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.InputStream;
import java.util.Base64;

// refactor using java
public class Secret {


    static public String encrypt(String plain, String secret) {
        //refactor:java
        {
            byte[] plain_bytes = plain.getBytes();
            byte[] secret_bytes = secret.getBytes();
            KeysetHandle handle = KeysetHandle.generateNew(AeadKeyTemplates.AES128_GCM);
            Aead aead = AeadFactory.getPrimitive(handle);
            byte[] cipher_bytes = aead.encrypt(plain_bytes, secret_bytes);
            String cipher = new String(cipher_bytes);
            return cipher;
        }
    }

    static public String decrypt(String cipher, String secret) {
        //refactor:java
        {
            byte[] cipher_bytes = cipher.getBytes();
            byte[] secret_bytes = secret.getBytes();
            KeysetHandle handle = KeysetHandle.generateNew(AeadKeyTemplates.AES128_GCM);
            Aead aead = AeadFactory.getPrimitive(handle);
            byte[] plain_bytes = aead.decrypt(cipher_bytes, secret);
            String plain = new String(plain_bytes);
            return plain;
        }
    }
}
