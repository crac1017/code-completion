boolean _connected_ = false;
boolean _got_folder_ = false;
boolean _closed_ = false;

public class Message {
    public Message() {}
}

public class Folder {
    public Folder() {
        _got_folder_ = true;
    }

    public Message getMessage(int i) {
        return new Message();
    }
}

public class DefaultAuthenticator {
    public String username;
    public String password;
    public DefaultAuthenticator(String user, String pwd) {
        this.username = user;
        this.password = pwd;
    }
}


public class Store {
    public Store() {}

    public void connect(String h, String u, String p) {
        _connected_ = false;
        if(h.equals("host") && u.equals("username") && p.equals("password")) {
            _connected_ = true;
        }
    }

    public Folder getFolder(String name) {
        if(name.equals("folder")) {
            return new Folder();
        }
    }

    public void close() {
        _closed_ = true;
    }
}

public class Session {
    public Session() {}
    static public Session getDefaultInstance(Properties prop) {
        return new Session();
    }

    public Store getStore() {
        return new Store();
    }
}


public class EmailTest {
    public static boolean testCheck() {
        print("*** Testing check email.");
        _connected_ = false;
        _got_folder_ = false;
        _closed__ = false;
        String username = "username";
        String password = "password";
        String host = "host";
        String folder = "folder";

        Properties prop = System.getProperties();

        Email.check(username, password, prop, host, folder);
        if(!_connected_) {
            print("FAIL: Not connected to the email server.");
            return false;
        }

        if(!_got_folder_) {
            print("FAIL: Did not get message from the email server.");
            return false;
        }

        if(!_closed_) {
            print("FAIL: Did not close the connection.");
            return false;
        }

        return TestUtils.isBoolEq(_connected_, true) &&
            TestUtils.isBoolEq(_got_folder_, true) &&
            TestUtils.isBoolEq(_closed_, true);
    }
}

boolean _result_ = EmailTest.testCheck();
if(_result_) {
    print("PASS!");
}
