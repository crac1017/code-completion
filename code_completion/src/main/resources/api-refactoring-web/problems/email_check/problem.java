import org.apache.commons.net.imap.IMAPClient;

import javax.mail.PasswordAuthentication;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Folder;
import javax.mail.Store;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

// refactor using javax.mail
// URL: https://javaee.github.io/javamail/
public class Email{

    /* original code
    {
        IMAPClient client = new IMAPClient();
        client.connect(hostname);
        client.login(username, password);
        client.select(folder);
        client.logout();
        client.disconnect();
    }
    */
    /**
     * This function does the following thing:
     * 1. connect and login to a IMAP server using the provided host name, user name and password,
     * 2. select the given folder
     * 3. disconnect and logout
     *
     * @param username - user name
     * @param password - password
     * @param prop - will be used in javamail
     * @param hostname - host name
     * @param folder - folder name
     */
    static public void check(String username, String password, Properties prop, String hostname, String folder) {
        {
            // TODO: put the refactoring result here
        }
    }
}
