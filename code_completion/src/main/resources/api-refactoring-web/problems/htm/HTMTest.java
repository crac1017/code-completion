import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import static org.junit.Assert.*;

public class HTMTest {

    @Test
    public void testAddAttr() {
        String html = "<html><head></head><a id=\"target\">hello</a></body></html>";
        String new_html = HTM.add_attr(html, "target", "href", "a1");

        Document doc = Jsoup.parse(new_html);
        Element target = doc.getElementsByAttribute("href").first();
        String href = target.attr("href");
        assertEquals("a1", href);
    }

    @Test
    public void testAddNode() {
        String html = "<html id=\"target\"><head></head><a href=\"www.google.com\">hello</a></body></html>";
        String new_html = HTM.add_node(html, "target", "h2");
        Document doc = Jsoup.parse(new_html);
        Element target = doc.getElementsByTag("h2").first();
        assertEquals("h2", target.tagName());
    }

    @Test
    public void testGetLink() {
        String html = "<html><head></head><a id=\"target\" href=\"www.google.com\">hello</a></body></html>";
        String link = HTM.get_link(html);
        assertEquals("www.google.com", link);
    }

    @Test
    public void testRmAttr() {
        String html = "<html><head></head><a href=\"www.google.com\">hello</a></body></html>";
        String new_html = HTM.rm_attr(html, "href");
        Document doc = Jsoup.parse(new_html);
        Elements targets = doc.getElementsByAttribute("href");
        assertTrue(targets.isEmpty());
    }
}

