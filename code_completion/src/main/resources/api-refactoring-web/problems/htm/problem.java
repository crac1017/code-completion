import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Node;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;
import java.lang.System;

// refactor the methods using org.jsoup
// URL: https://jsoup.org/
public class HTM {

    /* original code
    {
        import org.htmlcleaner.HtmlCleaner;
        import org.htmlcleaner.TagNode;

        HtmlCleaner cleaner = new HtmlCleaner();
        TagNode doc = cleaner.clean(content);
        TagNode node = doc.findElementByAttValue("id", id, true, true);
        node.addAttribute(attr, attr_val);
        return doc.toString();
    }
    */
    // parse the html "content" and add an attribute "attr" with attribute value into the node indicated by "id"
    // returns the string representation of the root node
    static public String add_attr(String content, String id, String attr, String attr_val) {
        {
            // TODO: Write the code here similar to the original code
        }
    }

    /* original code
        {
            import org.htmlcleaner.HtmlCleaner;
            import org.htmlcleaner.TagNode;
            HtmlCleaner cleaner = new HtmlCleaner();
            TagNode doc = cleaner.clean(content);
            TagNode node = doc.findElementByAttValue("id", id, true, true);
            node.addChild(new_html);
            return doc.toString();
        }
    */
    // parse the html "content" and add a child "new_html" into a node "id"
    // returns the string representation of the root node
    static public String add_node(String content, String id, String new_html) {
        {
            // TODO: Write the code here similar to the original code
        }
    }

    /* original code
        {
            import org.htmlcleaner.HtmlCleaner;
            import org.htmlcleaner.TagNode;
            HtmlCleaner cleaner = new HtmlCleaner();
            TagNode node = cleaner.clean(content);
            TagNode[] links = node.getElementsHavingAttribute("href", true);
            TagNode link = links[0];
            String href = link.getAttributeByName("href");
            return href;
        }
    */
    // parse the html "content" and get the first link
    static public String get_link(String content) {
        {
            // TODO: Write the code here similar to the original code
        }
    }

    /* original code
        {
            import org.htmlcleaner.HtmlCleaner;
            import org.htmlcleaner.TagNode;
            HtmlCleaner cleaner = new HtmlCleaner();
            TagNode node = cleaner.clean(content);
            TagNode link_node = node.findElementHavingAttribute(attr, true);
            link_node.removeAttribute(attr);
            return node.toString();
        }
    */
    // parse the html "content" and remove the attribute "attr"
    // returns the string representation of the root node
    static public String rm_attr(String content, String attr) {
        {
            // TODO: Write the code here similar to the original code
        }
    }
}
