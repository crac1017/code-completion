import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.regression.LogisticRegression;
import jsat.regression.RidgeRegression;
import jsat.clustering.kmeans.KMeans;
import jsat.clustering.kmeans.NaiveKMeans;
import jsat.classifiers.neuralnetwork.BackPropagationNet;
import jsat.classifiers.DataPoint;


// refactoring using jsat
// URL: https://github.com/EdwardRaff/JSAT
public class ML {

    /* original code
        {
            import smile.classification.LogisticRegression;

            double[][] x;
            int[] y;
            double[] test;
            LogisticRegression.Trainer trainer = new LogisticRegression.Trainer();
            LogisticRegression model = trainer.train(x, y);
            int c = model.predict(test);
            return c;
        }
     */
    /**
     * Train an LogisticRegression usign the given (x, y) and predict the category of the test data point
     * @param train_data - training data for the program after refactoring
     * @param test_data - testing data for the program after refactoring
     * @return the lable for the given test data point
     */
    static public int classify(ClassificationDataSet train_data, DataPoint test_data) {
        {
            // TODO: Write the code here similar to the original code
        }
    }

    /* original code
        {
            import smile.clustering.KMeans;

            double[][] x;
            KMeans model = KMeans.lloyd(x, k);
            int[] labels = model.getClusterLabel();
            int label = labels[0];
            return label;
        }
     */
    /**
     * Cluster the given data into k clusters and return the label of the first training data point.
     * @param train_data - training data for the program after refactoring
     * @param k - number of clusters
     * @return the lable for the first training data point
     */
    static public int cluster(ClassificationDataSet train_data, int k) {
        {
            // TODO: Write the code here similar to the original code
        }
    }

    /* original code
        {
            import smile.regression.RidgeRegression;

            double[][] x;
            int[] y;
            double[] test;
            RidgeRegression.Trainer trainer = new RidgeRegression.Trainer();
            RidgeRegression model = trainer.train(x, y);
            double d = model.predict(test);
            return d;
        }
     */
    /**
     * Train an RidgeRegression usign the given (x, y) and predict the value of the test data point
     * @param train_data - training data for the program after refactoring
     * @param test_data - testing data for the program after refactoring
     * @return the predicted value of the test data point
     */
    static public double regress(RegressionDataSet train_data, DataPoint test_data) {
        {
            // TODO: Write the code here similar to the original code
        }
    }

    /* original code
        {
            import smile.classification.NeuralNetwork;

            double[][] x;
            int[] y;
            double[] test;
            NeuralNetwork.Trainer trainer = new NeuralNetwork.Trainer(NeuralNetwork.ErrorFunction.LEAST_MEAN_SQUARES, 1);
            NeuralNetwork model = trainer.train(x, y);
            double d = model.predict(test);
            return d;
        }
     */
    /**
     * Train an back-prop neural network usign the given (x, y) and predict the value of the test data point
     * No requirement on the error function and the number of hidden layers and hidden units. Just focus
     * on the training and predicting.
     * @param train_data - training data for the program after refactoring
     * @param test_data - testing data for the program after refactoring
     * @return the predicted value of the test data point
     */
    static public double neural(RegressionDataSet train_data, DataPoint test_data) {
        {
            // TODO: Write the code here similar to the original code
        }
    }
}
