=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y) {
        {
            int i1 = 0;
            Vec v1 = null;
            double d1 = 0.0;
            i1 = train_data.getNumNumericalVars();
            v1 = test_data.getNumericalValues();
            d1 = v1.dot(v1);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.classifiers.CategoricalData;
import jsat.regression.RegressionDataSet;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y, Vec _v1) {
        {
            CategoricalData[] cd1 = 0.0;
            int i1 = 0;
            double d1 = 0.0;
            i1 = train_data.getNumNumericalVars();
            cd1 = train_data.getCategories();
            d1 = _v1.dot(_v1);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y) {
        {
            int i1 = 0;
            Vec v1 = null;
            double d1 = 0.0;
            i1 = train_data.getNumNumericalVars();
            v1 = test_data.getNumericalValues();
            d1 = v1.dot(v1);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.regression.RegressionDataSet;
import jsat.linear.Vec;
import jsat.classifiers.CategoricalData;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y, Vec _v1) {
        {
            int i1 = 0;
            double d1 = 0.0;
            CategoricalData[] cd1 = 0.0;
            i1 = train_data.getNumNumericalVars();
            cd1 = train_data.getCategories();
            d1 = _v1.dot(_v1);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.distributions.kernels.KernelTrick;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y, KernelTrick _kt1) {
        {
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            d1 = _kt1.eval(v1, v1);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.linear.distancemetrics.DenseSparseMetric;
import jsat.distributions.kernels.KernelTrick;
import jsat.classifiers.DataPoint;
import java.util.List;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y, KernelTrick _kt1, DenseSparseMetric _dsm1) {
        {
            Vec v1 = null;
            List l1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            l1 = _kt1.getQueryInfo(v1);
            d1 = _dsm1.getVectorConstant(v1);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.distributions.kernels.KernelTrick;
import jsat.classifiers.DataPoint;
import java.util.List;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y, KernelTrick _kt1, int _arg01) {
        {
            Vec v1 = null;
            List l1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            l1 = _kt1.getQueryInfo(v1);
            d1 = _kt1.eval(_arg01, v1, l1, l1, l1);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.distributions.Normal;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y, double _arg01, double _arg11, double _arg21) {
        {
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            d1 = Normal.invcdf(_arg01, _arg11, _arg21);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;
import jsat.classifiers.DataPoint;
import jsat.distributions.kernels.KernelTrick;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y, KernelTrick _kt1) {
        {
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            d1 = _kt1.eval(v1, v1);
            return;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;
import java.util.List;
import jsat.classifiers.DataPoint;
import jsat.distributions.kernels.KernelTrick;
import jsat.linear.distancemetrics.DenseSparseMetric;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.classifiers.DataPoint test_data, jsat.regression.RegressionDataSet train_data, double[] y, KernelTrick _kt1, DenseSparseMetric _dsm1) {
        {
            List l1 = null;
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            l1 = _kt1.getQueryInfo(v1);
            d1 = _dsm1.getVectorConstant(v1);
            return;
        }
    }
}

