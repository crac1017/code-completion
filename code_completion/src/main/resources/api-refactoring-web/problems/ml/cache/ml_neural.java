=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.distributions.kernels.KernelTrick;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y, KernelTrick _kt1) {
        {
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            d1 = _kt1.eval(v1, v1);
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.distributions.kernels.KernelTrick;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;
import java.util.List;
import jsat.linear.distancemetrics.DenseSparseMetric;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y, KernelTrick _kt1, DenseSparseMetric _dsm1) {
        {
            List l1 = null;
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            l1 = _kt1.getQueryInfo(v1);
            d1 = _dsm1.getVectorConstant(v1);
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.distributions.kernels.KernelTrick;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;
import java.util.List;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y, KernelTrick _kt1, int _arg01) {
        {
            List l1 = null;
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            l1 = _kt1.getQueryInfo(v1);
            d1 = _kt1.eval(_arg01, v1, l1, l1, l1);
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;
import jsat.distributions.kernels.KernelTrick;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y, KernelTrick _kt1) {
        {
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            d1 = _kt1.eval(v1, v1);
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import java.util.List;
import jsat.linear.distancemetrics.DenseSparseMetric;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;
import jsat.distributions.kernels.KernelTrick;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y, KernelTrick _kt1, DenseSparseMetric _dsm1) {
        {
            Vec v1 = null;
            List l1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            l1 = _kt1.getQueryInfo(v1);
            d1 = _dsm1.getVectorConstant(v1);
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import java.util.List;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;
import jsat.distributions.kernels.KernelTrick;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y, KernelTrick _kt1, int _arg01) {
        {
            Vec v1 = null;
            List l1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            l1 = _kt1.getQueryInfo(v1);
            d1 = _kt1.eval(_arg01, v1, l1, l1, l1);
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.distributions.Normal;
import jsat.classifiers.DataPoint;
import jsat.linear.Vec;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y, double _arg01, double _arg11, double _arg21) {
        {
            Vec v1 = null;
            double d1 = 0.0;
            v1 = test_data.getNumericalValues();
            d1 = Normal.invcdf(_arg01, _arg11, _arg21);
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.math.OnLineStatistics;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y) {
        {
            OnLineStatistics ols1 = null;
            double d1 = 0.0;
            d1 = (ols1 = new OnLineStatistics()).getMin();
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.math.OnLineStatistics;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y) {
        {
            OnLineStatistics ols1 = null;
            double d1 = 0.0;
            d1 = (ols1 = new OnLineStatistics()).getMax();
            return d1;
        }
    }
}

=============
import smile.clustering.KMeans;
import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;
import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.CategoricalResults;
import jsat.classifiers.DataPoint;
import jsat.regression.RegressionDataSet;
import jsat.math.OnLineStatistics;

class BayouInput {

    public double bayou_input(double[][] x, double[] test, jsat.regression.RegressionDataSet train_data, jsat.classifiers.DataPoint test_data, double[] y) {
        {
            OnLineStatistics ols1 = null;
            RegressionDataSet rds1 = null;
            double d1 = 0.0;
            rds1 = train_data.shallowClone();
            d1 = (ols1 = new OnLineStatistics()).getMin();
            return d1;
        }
    }
}

