var timer = new Timer();
var TOTAL_DURATION = 25 * 60;
// var TOTAL_DURATION = 10;

// run the code in the editor
function run() {
    var code = editor.getValue();
    console.log(code);
    $('#status').html("Querying the server....");
    $.ajax({
        type : 'POST',
        url : '/run_cache',
        data: JSON.stringify({code : code,
                              task : $("#task").html(),
                              debug : document.getElementById('debug-box').checked}),
        contentType: 'application/json',
        success: function(data) {
            output_editor.setValue(data.stdout);
            if(data.success) {
                $('#status').html("Success! Used " + data.runtime + " ms");
            } else {
                $('#status').html(data.stderr);
            }
        },
        error: function(xhr, type) {
            $('#status').html('Failed to query the server.');
        }
    });
}

function test(is_timeout) {
    var code = editor.getValue();
    console.log(code);
    $('#status').html("Querying the server....");
    $.ajax({
        type : 'POST',
        url : '/test',
        data: JSON.stringify({code : code,
                              task : $("#task").html(),
                              duration : TOTAL_DURATION - timer.getTotalTimeValues().seconds,
                              timeout : is_timeout,
                              use_tool : $('#use_tool').html() == "true",
                              session : $("#session").html()}),
        contentType: 'application/json',
        success: function(data) {
            $('#test').html(data.stdout);
            if(data.success) {
                $('#status').html("Success! Used " + data.runtime + " ms");
            } else {
                $('#status').html(data.stderr);
            }

            if(data.correct) {
                setTaskDone("Solution correct! You can exit this task.");
            }
        },
        error: function(xhr, type) {
            $('#status').html('Failed to query the server.');
        }
    });
}

// init function
function init() {
    editor = ace.edit("editor");
    editor.setTheme("ace/theme/chrome");
    editor.getSession().setMode("ace/mode/java");
    editor.resize();
    editor.setFontSize(12);
    editor.setValue("// Please write your code here.");
    editor.$blockScrolling = Infinity;

    output_editor = ace.edit("output-editor");
    output_editor.setTheme("ace/theme/chrome");
    output_editor.getSession().setMode("ace/mode/java");
    output_editor.resize();
    output_editor.setFontSize(12);
    output_editor.setReadOnly(true);
    output_editor.setValue("Output goes here");
    output_editor.$blockScrolling = Infinity;

    timer.start({countdown: true, startValues: {seconds: TOTAL_DURATION}});
    timer.addEventListener('secondsUpdated', function (e) {
        $('#timer').html(timer.getTimeValues().toString());
    });
    timer.addEventListener('targetAchieved', function (e) {
        test(true);
        setTaskDone("Timeout. You may exit this task.");
    });
}

function setTaskDone(message) {
    editor.setReadOnly(true);
    $('#test-btn').prop('disabled', true);
    $('#submit-btn').prop('disabled', true);
    timer.stop();
    alert(message);
}

$(document).ready(function() {
    init();
    editor.setValue($("#draft").text());
});
