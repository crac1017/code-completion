#!/usr/local/bin/sbcl --script
(load "~/quicklisp/setup.lisp")
(quicklisp:quickload "cl-csv")
(quicklisp:quickload "alexandria")
(defstruct (sub) task author time useful)
(setf *random-state* (make-random-state t))

(defun read-data (filename)
  (cl-csv:read-csv filename
                   :map-fn #'(lambda (row)
                               (make-sub
                                :author (nth 0 row)
                                :time (parse-integer (nth 2 row))
                                :task (nth 1 row)
                                :useful (nth 4 row)))))

(defun sample-data (data)
  (let* ((task-names
          (remove-duplicates (mapcar #'(lambda (sub) (sub-task sub)) data)
                             :test #'equal))
         (task-groups
          (mapcar #'(lambda (name)
                      (remove-if #'(lambda (d)
                                     (not (equal (sub-task d)
                                                 name)))
                                 data))
                  task-names))
         (sampled-groups
          (mapcar #'(lambda (group)
                      (mapcar #'(lambda (i)
                                  (declare (ignore i))
                                  (alexandria:random-elt group))
                              (alexandria:iota (length group))))
                  task-groups)))
    (reduce #'append sampled-groups)))

(defun get-mean (data task-name)
  (let* ((subs
          (remove-if #'(lambda (s)
                         (not (equal (sub-task s) task-name)))
                     data))
         (time-seq
          (mapcar #'(lambda (s) (sub-time s)) subs)))
    ; (format t "task-name : ~A~% ~A~%" task-name subs)
    (float (alexandria:mean time-seq))))

(defun test-null (data t1 t2)
  (let ((m1 (get-mean data t1))
        (m2 (get-mean data t2)))
    (>= m1 m2)))

(defun run (t1 t2 total-attempt)
  (format t "Testing ~A and ~A ~A times~%" t1 t2 total-attempt)
  (let* ((orig-data (read-data #P"data.csv"))
         (reject-seq (mapcar #'(lambda (i)
                                 (declare (ignore i))
                                 (test-null (sample-data orig-data)
                                            t1
                                            t2))
                             (alexandria:iota total-attempt)))
         (true-count (count t reject-seq)))
    (format t "True count: ~A~%" true-count)
    (float (/ true-count total-attempt))))


(defparameter *data* (read-data #P"data.csv"))
(defparameter *d1* (sample-data *data*))

(format t "~A~%" (run "http_algorithm" "http_search" 100000))
(format t "HTTP search     mean : ~A~%" (get-mean *data* "http_search"))
(format t "HTTP algorithm  mean : ~A~%" (get-mean *data* "http_algorithm"))
(format t "~A~%" (run "ftp_algorithm" "ftp_search" 100000))
(format t "FTP search     mean : ~A~%" (get-mean *data* "ftp_search"))
(format t "FTP algorithm  mean : ~A~%" (get-mean *data* "ftp_algorithm"))
(format t "~A~%" (run "pdf_algorithm" "pdf_search" 100000))
(format t "PDF search     mean : ~A~%" (get-mean *data* "pdf_search"))
(format t "PDF algorithm  mean : ~A~%" (get-mean *data* "pdf_algorithm"))
(format t "~A~%" (run "html_algorithm" "html_search" 100000))
(format t "HTML search     mean : ~A~%" (get-mean *data* "html_search"))
(format t "HTML algorithm  mean : ~A~%" (get-mean *data* "html_algorithm"))
