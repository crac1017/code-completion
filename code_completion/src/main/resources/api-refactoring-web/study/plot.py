import matplotlib.pyplot as plt
import numpy as np
import csv;

data = {"http": {"algorithm" : [], "search" : []},
        "ftp" : {"algorithm" : [], "search" : []},
        "pdf" : {"algorithm" : [], "search" : []},
        "html" : {"algorithm" : [], "search" : []}};

with open("data.csv") as infile:
    reader = csv.reader(infile);
    for row in reader:
        if row[1] == "http_search":
            data["http"]["search"].append(int(row[2]));
        elif row[1] == "http_algorithm":
            data["http"]["algorithm"].append(int(row[2]));
        if row[1] == "ftp_search":
            data["ftp"]["search"].append(int(row[2]));
        elif row[1] == "ftp_algorithm":
            data["ftp"]["algorithm"].append(int(row[2]));
        if row[1] == "pdf_search":
            data["pdf"]["search"].append(int(row[2]));
        elif row[1] == "pdf_algorithm":
            data["pdf"]["algorithm"].append(int(row[2]));
        if row[1] == "html_search":
            data["html"]["search"].append(int(row[2]));
        elif row[1] == "html_algorithm":
            data["html"]["algorithm"].append(int(row[2]))

print(data);

def mean(array): return float(sum(array)) / float(len(array));

pdata = [data["http"]["search"],
         data["http"]["algorithm"],
         data["ftp"]["search"],
         data["ftp"]["algorithm"],
         data["pdf"]["search"],
         data["pdf"]["algorithm"],
         data["html"]["search"],
         data["html"]["algorithm"]];
positions = [1, 1.25, 2, 2.25, 3, 3.25, 4, 4.25];
mean_data = [mean(d) for d in pdata];

print(pdata);
meanpointprops = dict(marker='D', markeredgecolor='black',
                      markerfacecolor='black', alpha = 0.5)

fig = plt.figure();
ax = fig.add_subplot(111);
box = ax.boxplot(pdata,
                  positions = positions,
                  widths = 0.2,
                  showbox = True,
                  showcaps = True,
                  showmeans = True,
                  showfliers = False,
                  meanprops = meanpointprops);
                  # patch_artist = True);

ax.set_ylabel("Avg. time (s)");
ax.tick_params(axis = "both", which = "major", labelsize=14);


plt.xticks([1, 2, 3, 4], ["http", "ftp", "pdf", "html"]);
plt.setp(box['boxes'][0], color = 'red');
plt.setp(box['boxes'][1], color = 'blue');
plt.setp(box['boxes'][2], color = 'red');
plt.setp(box['boxes'][3], color = 'blue');
plt.setp(box['boxes'][4], color = 'red');
plt.setp(box['boxes'][5], color = 'blue');
plt.setp(box['boxes'][6], color = 'red');
plt.setp(box['boxes'][7], color = 'blue');

plt.setp(box['caps'][0], color = 'red');
plt.setp(box['caps'][1], color = 'red');
plt.setp(box['caps'][2], color = 'blue');
plt.setp(box['caps'][3], color = 'blue');
plt.setp(box['caps'][4], color = 'red');
plt.setp(box['caps'][5], color = 'red');
plt.setp(box['caps'][6], color = 'blue');
plt.setp(box['caps'][7], color = 'blue');
plt.setp(box['caps'][8], color = 'red');
plt.setp(box['caps'][9], color = 'red');
plt.setp(box['caps'][10], color = 'blue');
plt.setp(box['caps'][11], color = 'blue');
plt.setp(box['caps'][12], color = 'red');
plt.setp(box['caps'][13], color = 'red');
plt.setp(box['caps'][14], color = 'blue');
plt.setp(box['caps'][15], color = 'blue');

plt.setp(box['whiskers'][0], color = 'red');
plt.setp(box['whiskers'][1], color = 'red');
plt.setp(box['whiskers'][2], color = 'blue');
plt.setp(box['whiskers'][3], color = 'blue');
plt.setp(box['whiskers'][4], color = 'red');
plt.setp(box['whiskers'][5], color = 'red');
plt.setp(box['whiskers'][6], color = 'blue');
plt.setp(box['whiskers'][7], color = 'blue');
plt.setp(box['whiskers'][8], color = 'red');
plt.setp(box['whiskers'][9], color = 'red');
plt.setp(box['whiskers'][10], color = 'blue');
plt.setp(box['whiskers'][11], color = 'blue');
plt.setp(box['whiskers'][12], color = 'red');
plt.setp(box['whiskers'][13], color = 'red');
plt.setp(box['whiskers'][14], color = 'blue');
plt.setp(box['whiskers'][15], color = 'blue');

plt.setp(box['medians'][0], color = 'red');
plt.setp(box['medians'][1], color = 'blue');
plt.setp(box['medians'][2], color = 'red');
plt.setp(box['medians'][3], color = 'blue');
plt.setp(box['medians'][4], color = 'red');
plt.setp(box['medians'][5], color = 'blue');
plt.setp(box['medians'][6], color = 'red');
plt.setp(box['medians'][7], color = 'blue');

# colors = ['white', 'gray'] * 4;
# for patch, color in zip(box['boxes'], colors):
    # patch.set_facecolor(color);

for i, d in enumerate(pdata):
    plt.plot([positions[i] for j in d], d, 'kx', alpha = 0.5);

hB, = plt.plot([1,1],'b-')
hR, = plt.plot([1,1],'r-')
plt.legend((hB, hR),('Algorithm', 'Search'), prop = {"size" : 14})
hB.set_visible(False)
hR.set_visible(False)
plt.savefig("user-study.png");

plt.show();


exit();
# fake up some data
spread = np.random.rand(50) * 100
center = np.ones(25) * 50
flier_high = np.random.rand(10) * 100 + 100
flier_low = np.random.rand(10) * -100
data = np.concatenate((spread, center, flier_high, flier_low), 0)

# basic plot
plt.boxplot(data)

# notched plot
plt.figure()
plt.boxplot(data, 1)

# change outlier point symbols
plt.figure()
plt.boxplot(data, 0, 'gD')

# don't show outlier points
plt.figure()
plt.boxplot(data, 0, '')

# horizontal boxes
plt.figure()
plt.boxplot(data, 0, 'rs', 0)

# change whisker length
plt.figure()
plt.boxplot(data, 0, 'rs', 0, 0.75)

# fake up some more data
spread = np.random.rand(50) * 100
center = np.ones(25) * 40
flier_high = np.random.rand(10) * 100 + 100
flier_low = np.random.rand(10) * -100
d2 = np.concatenate((spread, center, flier_high, flier_low), 0)
data.shape = (-1, 1)
d2.shape = (-1, 1)
# data = concatenate( (data, d2), 1 )
# Making a 2-D array only works if all the columns are the
# same length.  If they are not, then use a list instead.
# This is actually more efficient because boxplot converts
# a 2-D array into a list of vectors internally anyway.
data = [data, d2, d2[::2, 0]]
# multiple box plots on one figure
plt.figure()
plt.boxplot(data)

plt.show()
