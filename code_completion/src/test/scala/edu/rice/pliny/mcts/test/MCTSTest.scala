package edu.rice.pliny.mcts.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java.{JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.mcts.evaluator.SyntaxDistEvaluator
import edu.rice.pliny.mcts.integer.{IntDistEvaluator, IntTree}
import edu.rice.pliny.mcts.vis.TreeVisualizer
import org.scalatest._

class MCTSTest extends FlatSpec with Matchers {

  "The MCTS algorithm" should "work with integers" in {
    AppConfig.log_config
    val tree = new IntTree(0, None)
    val evaluator = new IntDistEvaluator(tree.SOLUTION_NUM)
    val task = new BanditSearch(evaluator)
    // TreeVisualizer.visualize(tree)
    val solution = task.complete(tree, 1000)

    solution match {
      case Solution(root, leaf) =>
        TreeVisualizer.visualize(root)
        // val view = new TreeView(new_tree)
        // view.showTree("foo")
        println("Solution ==============")
        println(leaf)
      case NoSolution() => println("No Solution.")
    }

  }

  def complete(draft : String, ref : String, iter : Int): Unit = {
    AppConfig.log_config
    val draft_prog = JavaUtils.parse_file(draft, None, None, None, Seq(), Seq()).get
    // val binsearch = JavaUtils.parse_file("src/test/resources/corpus/binsearch/binsearch2.java", None, None, Seq(), Seq()).get
    val ref_prog = JavaUtils.parse_file(ref, None, None, None, Seq(), Seq()).get
    val evaluator = new SyntaxDistEvaluator(ref_prog)
    val task = new BanditSearch(evaluator)
    val tree = new DraftTree(draft_prog, None, new JavaFastInterpreter(Seq()), relevant_progs = Seq(ref_prog))
    val solution = task.complete(tree, iter)
    solution match {
      case Solution(root, leaf) =>
        TreeVisualizer.visualize(root)
        // val view = new TreeView(new_tree)
        // view.showTree("foo")
        println("Solution ==============")
        println(leaf)
      case NoSolution() => println("No Solution.")
    }
  }

  "The MCTS algorithm" should "complete a simple program" in {
    this.complete("src/test/resources/binsearch_draft6.java", "src/test/resources/mcts/binsearch_bad.java", 100)
  }

  "The system" should "extract class information and use it to complete drafts" in {
    AppConfig.log_config
    val draft = "src/test/resources/mcts/integer_draft.java"
    val ref_prog = "src/test/resources/mcts/integer.java"
    this.complete(draft, ref_prog, 100)
  }

  "The system" should "use expressions from external programs to complete drafts" in {
    AppConfig.log_config
    val draft = "src/test/resources/mcts/split_draft.java"
    val ref_prog = "src/test/resources/mcts/split2.java"
    this.complete(draft, ref_prog, 100)
  }

  "The system" should "use statements to complete empty drafts" in {
    AppConfig.log_config
    val draft = "src/test/resources/mcts/integer_empty.java"
    val ref_prog = "src/test/resources/mcts/integer2.java"
    this.complete(draft, ref_prog, 200)
  }

  "The system" should "complete empty drafts with everything" in {
    AppConfig.log_config
    val draft = "src/test/resources/mcts/sum_draft.java"
    val ref_prog = "src/test/resources/mcts/sum.java"
    this.complete(draft, ref_prog, 200)
  }

  "The system" should "complete binary search using the original program" in {
    AppConfig.log_config
    val draft = "src/test/resources/mcts/binsearch_draft.java"
    val ref_prog = "src/test/resources/mcts/binsearch2.java"
    this.complete(draft, ref_prog, 200)
  }
}
