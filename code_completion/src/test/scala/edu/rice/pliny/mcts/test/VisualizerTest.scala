package edu.rice.pliny.mcts.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java.{JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.mcts.evaluator.{RandomFillEvaluator, SyntaxDistEvaluator}
import edu.rice.pliny.mcts.vis.TreeVisualizer
import org.scalatest._

class VisualizerTest extends FlatSpec with Matchers {

  "The MCTS algorithm" should "complete a simple program" in {
    AppConfig.log_config
    val draft = JavaUtils.parse_file("src/test/resources/simple_expr_hole3.java", None, None, None, Seq(), Seq()).get
    val binsearch = JavaUtils.parse_file("src/test/resources/corpus/binsearch/binsearch2.java", None, None, None, Seq(), Seq()).get
    val hole = draft.get_stmt_hole_with_tests
    val evaluator = new RandomFillEvaluator
    val evaluator2 = new SyntaxDistEvaluator(binsearch)
    val task = new BanditSearch(evaluator2)
    val interpreter = new JavaFastInterpreter(Seq())
    val tree = new DraftTree(draft, None, interpreter)
    val solution = task.complete(tree, 10)
    solution match {
      case Solution(root, leaf) =>
        TreeVisualizer.visualize(root)
        // val view = new TreeView(new_tree)
        // view.showTree("foo")
        println("Solution ==============")
        println(leaf)
      case NoSolution() => println("No Solution.")
    }
  }
}
