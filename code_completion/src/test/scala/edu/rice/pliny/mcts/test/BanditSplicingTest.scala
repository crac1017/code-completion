package edu.rice.pliny.mcts.test

import edu.rice.pliny.{AppConfig, Utils}
import edu.rice.pliny.language.java.{JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.mcts.evaluator.SyntaxDistEvaluator
import edu.rice.pliny.mcts.splicing.JavaSplicingTree
import edu.rice.pliny.mcts.vis.TreeVisualizer
import org.scalatest._

class BanditSplicingTest extends FlatSpec with Matchers {
  def complete(draft : String, ref : String, iter : Int): Boolean = {
    AppConfig.log_config
    val draft_prog = JavaUtils.parse_file(draft, None, None, None, Seq(), Seq()).get
    val ref_prog = JavaUtils.parse_file(ref, None, None, None, Seq(), Seq()).get
    // val evaluator = new RefProgEvaluator(ref_prog)
    val ref_eval = new SyntaxDistEvaluator(ref_prog)
    // val evaluator = new CombinedEvaluator(Seq(TestCaseEvaluator, new RefProgEvaluator(ref_prog)), Seq(0.5, 0.5))
    val task = new BanditSearch(ref_eval)
    // val task = new MCTSTask(evaluator)
    val tree = new JavaSplicingTree(draft_prog, None, new JavaFastInterpreter(Seq()), relevant_progs = Seq(ref_prog))
    val (runtime, solution) = Utils.time{task.complete(tree, iter)}
    TreeVisualizer.visualize(tree)
    solution match {
      case Solution(root, leaf) =>
        // val view = new TreeView(new_tree)
        // view.showTree("foo")
        println("Solution ==============")
        println(leaf)
        println("Runtime: " + runtime + " ms")
        true
      case NoSolution() =>
        println("No Solution.")
        false
    }
  }

  "The system" should "complete binary search using the original program" in {
    AppConfig.log_config
    val draft = "src/test/resources/mcts/binsearch_draft.java"
    val ref_prog = "src/test/resources/mcts/binsearch2.java"
    this.complete(draft, ref_prog, 100) shouldBe true
  }
}
