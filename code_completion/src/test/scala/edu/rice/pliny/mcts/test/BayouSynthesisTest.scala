package edu.rice.pliny.mcts.test

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java.{JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.mcts.bayou.{BayouSketchUtils, JavaSketchTree}
import edu.rice.pliny.mcts.evaluator.DefUseRatioEvaluator
import org.scalatest._

class BayouSynthesisTest extends FlatSpec with Matchers {
  val logger = Logger(this.getClass)

  /**
    * Test whether we have a solution for this sketch
    */
  def complete_sketch(sketch_file : String, draft_file : String, jar_paths : Seq[String], ref_srcs : Seq[String]) : Boolean = {
    val refs = ref_srcs.map(ref_src => JavaUtils.parse_file(ref_src, None, None, None, jar_paths, Seq()).get)

    val sketch = BayouSketchUtils.parse_sketch(sketch_file, draft_file, jar_paths = jar_paths)
    // val result = sketch.slice(0, sketch.size)
    val result = sketch.slice(3, 4)
      .map(s => {
        logger.info("Synthesizing ================\n{}\n", s.compilation_unit.toString)
        val interpreter = new JavaFastInterpreter(jar_paths)
        val tree = new JavaSketchTree(s, None, interpreter, relevant_progs = refs)
        val evaluator = new DefUseRatioEvaluator
        val task = new BanditSearch(evaluator)
        JavaUtils.varnum_init()
        BayouSketchUtils.clear_filling_cache()
        val solution = task.complete(tree, 100)
        solution match {
          case NoSolution() =>
            logger.info("NO SOLUTION.")
            false
          case Solution(_, leaf) =>
            logger.info("Solution ===============\n{}\n", leaf.toString)
            true
        }
      })
    logger.info("SUCCESS COUNT: {}/{}", result.count(r => r), sketch.size)
    result.contains(true)
  }

  "The system" should "complete testIO1" in {
    AppConfig.log_config
    val read_sketch = "src/test/resources/mcts/bayou/sketches/sketch.json"
    val read_draft = "src/test/resources/mcts/bayou/sketches/draft.java"
    val ref_src = "src/test/resources/mcts/bayou/read.java"
    complete_sketch(read_sketch, read_draft, Seq(), Seq(ref_src)) shouldBe true
  }
}
