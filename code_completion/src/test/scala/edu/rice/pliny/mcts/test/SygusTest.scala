package edu.rice.pliny.mcts.test
import edu.rice.pliny.AppConfig
import edu.rice.pliny.mcts.sygus.{SygusAST, SygusInterpreter, SygusParser}
import org.scalatest._

class SygusTest extends FlatSpec with Matchers {
  "The system" should "parse a sygus problem" in {
    AppConfig.log_config
    val problem_fname = "src/test/resources/mcts/sygus/test.sl"
    val problem = scala.io.Source.fromFile(problem_fname).mkString
    val ast = SygusParser.parse(problem)
    ast.isDefined shouldBe true
  }

  it should "evaluate a sygus problem successfully" in {
    AppConfig.log_config
    val problem_fname = "src/test/resources/mcts/sygus/test.sl"
    val problem = scala.io.Source.fromFile(problem_fname).mkString
    val ast = SygusParser.parse(problem)
    val interpreter = new SygusInterpreter

    // evaluate all the sexps
    ast.get.foreach(sexpr => {
      if(!(SygusAST.to_src(sexpr) == "(check-synth)")) {
        interpreter.eval(sexpr.asInstanceOf[SygusAST.SExpr])
      }
    })
    interpreter.synth_fun should not be null
    interpreter.grammar should not be null
    interpreter.constraints.nonEmpty shouldBe true
  }

  it should "synthesize an easy program" in {
    AppConfig.log_config
    val problem_fname = "src/test/resources/mcts/sygus/test.sl"
    val problem = scala.io.Source.fromFile(problem_fname).mkString
    val ast = SygusParser.parse(problem)
    val interpreter = new SygusInterpreter

    // evaluate all the sexps
    ast.get.foreach(sexpr => {
      interpreter.eval(sexpr.asInstanceOf[SygusAST.SExpr])
    })
    println("Solution found ===========\n" + interpreter.solution.toString + "\n")
    interpreter.solution should not be null
    interpreter.synth_result should not be -1L
  }

  it should "synthesize a non-trivial program" in {
    AppConfig.log_config
    val problem_fname = "src/test/resources/mcts/sygus/length.sl"
    val problem = scala.io.Source.fromFile(problem_fname).mkString
    val ast = SygusParser.parse(problem)
    val interpreter = new SygusInterpreter

    // evaluate all the sexps
    ast.get.foreach(sexpr => {
      interpreter.eval(sexpr.asInstanceOf[SygusAST.SExpr])
    })
    println("Solution found ===========\n" + interpreter.solution.toString + "\n")
    interpreter.solution should not be null
    interpreter.synth_result should not be -1L
  }

  it should "synthesize a hard program" in {
    AppConfig.log_config
    val problem_fname = "src/test/resources/mcts/sygus/bikes-long.sl"
    val problem = scala.io.Source.fromFile(problem_fname).mkString
    val ast = SygusParser.parse(problem)
    val interpreter = new SygusInterpreter

    // evaluate all the sexps
    ast.get.foreach(sexpr => {
      interpreter.eval(sexpr.asInstanceOf[SygusAST.SExpr])
    })
    println("Solution found ===========\n" + interpreter.solution.toString + "\n")
    interpreter.solution should not be null
    interpreter.synth_result should not be -1L
  }
}
