package edu.rice.pliny.mcts.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java.{JavaFastInterpreter, JavaUtils}
import edu.rice.pliny.mcts._
import edu.rice.pliny.mcts.evaluator.{SyntaxDistEvaluator, SyntaxMappingDistEvaluator, TestCaseEvaluator}
import edu.rice.pliny.mcts.splicing.JavaSplicingTree
import edu.rice.pliny.mcts.vis.TreeVisualizer
import org.scalatest._

class SynthesisTest extends FlatSpec with Matchers {

  def complete(draft : String, refs : Seq[String]): Unit = {
    AppConfig.log_config
    val draft_prog = JavaUtils.parse_file(draft, None, None, None, Seq(), Seq()).get
    // val binsearch = JavaUtils.parse_file("src/test/resources/corpus/binsearch/binsearch2.java", None, None, Seq(), Seq()).get
    val ref_progs = refs.map(s => JavaUtils.parse_file(s, None, None, None, Seq(), Seq()).get)
    // val evaluator = new RefProgEvaluator(ref_progs.head)
    val stmt_eval = new SyntaxDistEvaluator(ref_progs.head)
    val expr_eval = new TestCaseEvaluator(10)
    // val expr_prior = new ExprDistPrior(ref_progs.head)
    val task = new JavaDraftSearch(stmt_eval, expr_eval)
    val tree = new JavaSplicingTree(draft_prog, None, new JavaFastInterpreter(Seq()), relevant_progs = ref_progs)
    val solution = task.complete(tree, 500)
    TreeVisualizer.visualize(tree)
    solution match {
      case Solution(root, leaf) =>
        // val view = new TreeView(new_tree)
        // view.showTree("foo")
        println("Solution ==============")
        println(leaf)
      case NoSolution() =>
        // println("No Solution.")
        fail()
    }
  }

  "The algorithm" should "complete a simple program" in {
    this.complete("src/test/resources/mcts/binsearch_draft.java", Seq("src/test/resources/mcts/binsearch_bad.java"))
  }
}
