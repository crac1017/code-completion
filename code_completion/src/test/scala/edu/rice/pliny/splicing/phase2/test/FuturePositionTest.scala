package edu.rice.pliny.splicing.phase2.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java._
import edu.rice.pliny.main.CodeCompletionMain
import org.scalatest.{FlatSpec, Matchers}

/**
  * Test class for java draft
  */
class FuturePositionTest extends FlatSpec with Matchers {
  val VALID_PROG_FILE = "src/main/resources/challenge_problems/update_pos/FuturePositionComplete.java"

  val TRAINING_DIR = "src/main/resources/challenge_problems/update_pos/training"
  val DRAFT_FILENAME = "src/main/resources/challenge_problems/update_pos/draft/FuturePositionDraft.java"

  "A correct program" should "return true" in {
    val draft = JavaUtils.parse_file(VALID_PROG_FILE, None, Some("estimateFuturePosition"), None, Seq(), Seq()).get
    val test_case = draft.get_test
    val result = JavaInterpreter.test(test_case.get, draft.toString)
    result._1 shouldBe true
  }

  "Code completion algorithm" should "should timeout with little time" in {
    AppConfig.CodeCompletionConfig.query_type = "dir"
    AppConfig.CodeCompletionConfig.local_dir_path = this.TRAINING_DIR
    AppConfig.CodeCompletionConfig.time_limit = 10

    val draft = JavaUtils.parse_file(DRAFT_FILENAME, None, None, None, Seq(), Seq()).get
    val completion = CodeCompletionMain.complete(draft)
    completion._1.isEmpty shouldBe true
  }

  "Code completion algorithm" should "complete a draft with enough time" in {
    AppConfig.CodeCompletionConfig.query_type = "dir"
    AppConfig.CodeCompletionConfig.local_dir_path = this.TRAINING_DIR
    AppConfig.CodeCompletionConfig.time_limit = 600

    val draft = JavaUtils.parse_file(DRAFT_FILENAME, None, None, None, Seq(), Seq()).get
    val completion = CodeCompletionMain.complete(draft)
    completion._1.nonEmpty shouldBe true
    for(c <- completion._1) {
      println("Solution ======\n" + c.toString)
    }
  }
}
