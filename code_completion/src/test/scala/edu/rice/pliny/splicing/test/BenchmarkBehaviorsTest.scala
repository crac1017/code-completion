package edu.rice.pliny.splicing.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java._
import edu.rice.pliny.main.CodeCompletionMain
import org.scalatest.FlatSpec

/**
  * Test class for java draft
  */
class BenchmarkBehaviorsTest extends FlatSpec with BenchmarkBehaviors {
  // "binsearch" should behave like work("binsearch")
  // "csv" should behave like work("csv")
  // "collision" should behave like work("collision")
  // "echo" should behave like work("echo")
  // "face" should behave like work("face")
  // "files" should behave like work("files")
  // "floyd" should behave like work("floyd")
  // "gps" should behave like work("gps")
  // "hello" should behave like work("hello")
  // "html" should behave like work("html")
  // "http" should behave like work("http")
  // "lcs" should behave like work("lcs")
  // "matmul" should behave like work("matmul")
  // "merge" should behave like work("merge")
  // "prims" should behave like work("prims")
  // "qsort" should behave like work("qsort")
  "sieve" should behave like work("sieve")
}

trait BenchmarkBehaviors { this : FlatSpec =>
  case class Problem(class_name : String, method_name : String, jar_paths : Seq[String], dir_paths : Seq[String])
  val PROBLEMS = Map(
    "binsearch" -> Problem("Binsearch", "binsearch", Seq(), Seq()),
    "csv" -> Problem("CSV", "read_csv", Seq(), Seq()),
    "collision" -> Problem("AABB2", "testCollision", Seq("src/main/resources/benchmarks/collision/jReactPhysics3D-0.4.0.jar"), Seq()),
    "face" -> Problem("DetectFaceDemo", "run", Seq("src/main/resources/benchmarks/face/opencv-2413.jar"), Seq()),
    "files" -> Problem("Files", "dfs", Seq(), Seq()),
    "floyd" -> Problem("Floyd", "floyd", Seq(), Seq()),
    "gps" -> Problem("FuturePosition", "estimateFuturePosition", Seq(), Seq()),
    "hello" -> Problem("HelloWorldTest", "run", Seq(), Seq()),
    "html" -> Problem("HTML", "get_links", Seq("src/main/resources/benchmarks/html/jsoup-1.10.2.jar"), Seq()),
    "http" -> Problem("HTTP", "http", Seq(), Seq()),
    "lcs" -> Problem("LCS", "lcs", Seq(), Seq()),
    "matmul" -> Problem("MatMul", "matmul", Seq(), Seq()),
    "merge" -> Problem("Merge", "merge", Seq(), Seq()),
    "prims" -> Problem("Prims", "prims", Seq(), Seq()),
    "qsort" -> Problem("QSort", "qsort", Seq(), Seq()),
    "sieve" -> Problem("Sieve", "sieve", Seq(), Seq()),
    "echo" -> Problem("Echo", "run", Seq(), Seq())
  )

  def work(name : String) : Unit = {
    it should "have a solution" in {
      val info = PROBLEMS(name)
      val solution_path = s"src/main/resources/benchmarks/$name/csv_db.java"
      val solution = JavaUtils.parse_file(solution_path, Some(info.class_name), Some(info.method_name), None, info.jar_paths, info.dir_paths).get
      val test_case = solution.get_test
      val result = JavaInterpreter.test(test_case.get, solution.toString)
      assert(result._1)
    }

    it should "complete a draft" in {
      val info = PROBLEMS(name)
      val training_path = s"src/main/resources/benchmarks/$name/training"
      val draft_path = s"src/main/resources/benchmarks/$name/draft.java"

      AppConfig.CodeCompletionConfig.query_type = "dir"
      AppConfig.CodeCompletionConfig.local_dir_path = training_path

      val draft = JavaUtils.parse_file(draft_path, Some(info.class_name), Some(info.method_name), None, info.jar_paths, info.dir_paths).get
      val completion = CodeCompletionMain.complete(draft)
      assert(completion._1.nonEmpty)
      for(c <- completion._1) { println("Solution ======\n" + c.toString) }
    }
  }
}

