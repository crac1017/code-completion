package edu.rice.pliny.splicing.test

import edu.rice.pliny.language.java.{JavaFastInterpreter, JavaInterpreter, JavaTestCase}
import org.scalatest._
import Matchers._
import java.io.File

import edu.rice.pliny.Utils

class JavaDraftInterpreterTest extends FlatSpec {


  "An interpreter" should "run tests cases" in {
    val inter = new JavaFastInterpreter(Seq())
    val input = Utils.read_file("code_completion/src/test/resources/binsearch_test_stmt.java")
    val code = Utils.read_file("code_completion/src/test/resources/binsearch_test_code.java")
    val result = inter.test(new JavaTestCase(input), code)._1
    result should be (true)
  }

  "An interpreter" should "report runtime" in {
    val inter = new JavaFastInterpreter(Seq())
    val input = Utils.read_file("code_completion/src/test/resources/binsearch_test_stmt.java")
    val code = Utils.read_file("code_completion/src/test/resources/binsearch_test_code.java")
    val result = inter.test(new JavaTestCase(input), code)
    result._2 shouldBe a [java.lang.Long]
    result._2 should be > 0L
  }

  "An interpreter" should "timeout" in {
    val inter = new JavaFastInterpreter(Seq())
    val input = Utils.read_file("code_completion/src/test/resources/binsearch_test_stmt.java")
    val code = Utils.read_file("code_completion/src/test/resources/binsearch_test_inf_loop.java")
    val result = inter.test(new JavaTestCase(input), code)._1

    result shouldBe false
  }

  "An interpreter" should "report runtime -1 on failed test cases" in {
    val inter = new JavaFastInterpreter(Seq())
    val input = Utils.read_file("code_completion/src/test/resources/binsearch_test_stmt.java")
    val code = Utils.read_file("code_completion/src/test/resources/binsearch_test_inf_loop.java")
    val result = inter.test(new JavaTestCase(input), code)
    result._2 shouldBe a [java.lang.Long]
    result._2 shouldBe -1L
  }

  "An interpreter" should "report error message on failed test cases" in {
    val inter = new JavaFastInterpreter(Seq())
    val error_code = scala.io.Source.fromFile(new File("code_completion/src/test/resources/error.bsh")).mkString
    val result = inter.test(error_code)
    println(result)
    result._2 shouldBe a [java.lang.Long]
    result._2 shouldBe -1L
    result._3.nonEmpty shouldBe true
  }

  "An interpreter" should "run fast" in {
    val inter = new JavaFastInterpreter(Seq())
    val good_code = scala.io.Source.fromFile(new File("code_completion/src/test/resources/foo.bsh")).mkString
    val inf_code = scala.io.Source.fromFile(new File("code_completion/src/test/resources/bar.bsh")).mkString
    (1 to 10).foreach(i =>{
      inter.test(inf_code)
    })
    val test_result = inter.test(good_code)

    test_result._1 shouldBe true
  }

}
