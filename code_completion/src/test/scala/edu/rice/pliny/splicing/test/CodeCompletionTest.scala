package edu.rice.pliny.splicing.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.splicing.{ExprHoleTask, HoleTask, StmtHoleTask}
import edu.rice.pliny.language.java.{JavaCodeDB, JavaDraftIteratorFactory, JavaInterpreter, JavaUtils}
import edu.rice.pliny.main.CodeCompletionMain
import org.scalatest._

class CodeCompletionTest extends FlatSpec with Matchers {

  "The code completion algorithm" should "fill in statement holes" in {
    AppConfig.CodeCompletionConfig.query_type = "dir"
    AppConfig.CodeCompletionConfig.local_dir_path = "src/test/resources/corpus/binsearch"
    val draft = JavaUtils.parse_file("src/test/resources/completion_test_stmt_draft.java", None, None, None, Seq(), Seq()).get
    val hole = draft.get_stmt_hole_with_tests
    hole.isDefined should be (true)
    val completion = CodeCompletionMain.complete(draft)
    completion._1.nonEmpty shouldBe true
  }

  "The code completion algorithm" should "fill in expression holes" in {
    AppConfig.CodeCompletionConfig.query_type = "dir"
    AppConfig.CodeCompletionConfig.local_dir_path = "src/test/resources/corpus/binsearch"

    val draft = JavaUtils.parse_file("src/test/resources/completion_test_expr_draft.java", None, None, None, Seq(), Seq()).get
    draft.comment.isDefined shouldBe true
    draft.comment.get shouldBe "this is the comment.\n"

    val completion = CodeCompletionMain.complete(draft)
    completion._1.nonEmpty shouldBe true
  }

  "The code completion algorithm" should "fill in all holes" in {
    AppConfig.CodeCompletionConfig.query_type = "dir"
    AppConfig.CodeCompletionConfig.local_dir_path = "src/test/resources/corpus/binsearch"

    val draft = JavaUtils.parse_file("src/test/resources/completion_test_draft.java", None, None, None, Seq(), Seq()).get
    val completion = CodeCompletionMain.complete(draft)
    completion._1.nonEmpty shouldBe true
  }
}
