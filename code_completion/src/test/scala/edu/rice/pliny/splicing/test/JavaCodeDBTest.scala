package edu.rice.pliny.splicing.test

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import edu.rice.pliny.AppConfig
import edu.rice.pliny.language.java.{JavaCodeDB, JavaDraft, JavaUtils}
import org.scalatest._

class JavaCodeDBTest extends FlatSpec with Matchers {

  val CORPUS_DIR = "src/test/resources/corpus/binsearch"

  "Query local dir" should "get back some programs in a directory" in {
    val draft = JavaUtils.parse_file("src/test/resources/completion_test_expr_draft.java", None, None, None, Seq(), Seq()).get
    val sim_progs = JavaCodeDB.query_local_dir(draft, CORPUS_DIR)
    sim_progs.length should be (3)
    for(sp <- sim_progs) {
      val java_prog = sp.asInstanceOf[JavaDraft]
      val unit = java_prog.compilation_unit
      val cclass = unit.getType(0).asInstanceOf[ClassOrInterfaceDeclaration]
      val method = JavaUtils.get_method(cclass, "binarySearch").get
      assert(method != null)
    }
  }

  val JSON_FILENAME = "src/test/resources/face_query.json"
  val JSON_CLASS_NAMES = Set("FaceDetector", "DetectFaceDemo", "HelloOpenCV")
  val VALID_FACE_DETECTION = "src/main/resources/challenge_problems/face_detection/HelloOpenCVMethod.java"
  "Query local json" should "get back some programs in a json" in {
    val draft = JavaUtils.parse_file(VALID_FACE_DETECTION, Some("FaceDetection"), Some("run"), None, Seq("src/main/resources/challenge_problems/face_detection/opencv-2413.jar"), Seq()).get
    val sim_progs = JavaCodeDB.query_local_json(draft, JSON_FILENAME)
    sim_progs.length should be (16)
  }

  "Query PDB" should "get back some programs" in {
    AppConfig.CodeCompletionConfig.query_type = "pdb"
    val draft = JavaUtils.parse_file("src/test/resources/completion_test_expr_draft.java", None, None, None, Seq(), Seq()).get
    val sim_progs = JavaCodeDB.query_pdb(draft, "comment goes here.")
    sim_progs.nonEmpty shouldBe true
  }
}
