package edu.rice.pliny.splicing.phase2.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.splicing.HoleTask
import edu.rice.pliny.language.java._
import edu.rice.pliny.main.CodeCompletionMain
import org.scalatest.{FlatSpec, Matchers}

/**
  * Test class for java draft
  */
class FaceDetectorTest extends FlatSpec with Matchers {
  val FACEDETECTOR_FILENAME = "src/main/resources/challenge_problems/face_detection/HelloOpenCVStmt.java"
  val FACEDETECTOR_TRAINING_DIR = "src/main/resources/challenge_problems/face_detection/training"
  val FACEDETECTOR_DRAFT_FILENAME = "src/main/resources/challenge_problems/face_detection/draft/face_draft.java"

  val VALID_PROG_FILE = "src/main/resources/challenge_problems/face_detection/HelloOpenCVMethod.java"

  val TEST_LIB_FILENAME = "src/main/resources/challenge_problems/face_detection/FaceDetectionTestLib.java"
  val DETECTION_STMTS = "src/main/resources/challenge_problems/face_detection/HelloOpenCVStmt.java"


  "Face detection validator" should "return true given the input program is correct" in {
    val draft = JavaUtils.parse_file(VALID_PROG_FILE, Some("FaceDetection"), Some("run"), None, Seq("src/main/resources/challenge_problems/face_detection/opencv-2413.jar"), Seq()).get
    val test_case = draft.get_test
    val result = JavaInterpreter.test(test_case.get, draft.toString)
    result._1 shouldBe true
  }

  "Code completion algorithm" should "complete a face detection program draft" in {
    AppConfig.CodeCompletionConfig.query_type = "dir"
    AppConfig.CodeCompletionConfig.local_dir_path = this.FACEDETECTOR_TRAINING_DIR
    val draft = JavaUtils.parse_file(FACEDETECTOR_DRAFT_FILENAME, Some("DetectFaceDemo"), Some("run"), None, Seq("src/main/resources/challenge_problems/face_detection/opencv-2413.jar"), Seq()).get
    val completion = CodeCompletionMain.complete(draft)
    completion._1.nonEmpty shouldBe true
    for(c <- completion._1) {
      println("Solution ======\n" + c.toString)
    }

  }

}
