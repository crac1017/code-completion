package edu.rice.pliny.splicing.phase2.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.splicing.HoleTask
import edu.rice.pliny.language.java._
import edu.rice.pliny.main.CodeCompletionMain
import org.scalatest.{FlatSpec, Matchers}

/**
  * Test class for java draft
  */
class CollisionTest extends FlatSpec with Matchers {
  val VALID_PROG_FILE = "src/main/resources/challenge_problems/collision/AABBComplete.java"

  val TRAINING_DIR = "src/main/resources/challenge_problems/collision/training"
  val DRAFT_FILENAME = "src/main/resources/challenge_problems/collision/draft/AABBdraft.java"

  "A correct program" should "return true" in {
    val jar_paths = Seq("src/main/resources/challenge_problems/collision/jReactPhysics3D-0.4.0.jar")
    val draft = JavaUtils.parse_file(VALID_PROG_FILE, Some("AABB2"), Some("testCollision"), None, jar_paths, Seq()).get
    val test_case = draft.get_test
    val interpreter = new JavaFastInterpreter(jar_paths)
    val result = interpreter.test(test_case.get, draft.toString)
    result._1 shouldBe true
  }

  "Code completion algorithm" should "complete a collision detection program draft" in {
    AppConfig.CodeCompletionConfig.query_type = "dir"
    AppConfig.CodeCompletionConfig.local_dir_path = this.TRAINING_DIR

    val jar_paths = Seq("src/main/resources/challenge_problems/collision/jReactPhysics3D-0.4.0.jar")
    val draft = JavaUtils.parse_file(DRAFT_FILENAME, Some("AABB2"), Some("testCollision"), None, jar_paths, Seq()).get
    val completion = CodeCompletionMain.complete(draft, jar_paths)
    completion._1.nonEmpty shouldBe true
    for(c <- completion._1) {
      println("Solution ======\n" + c.toString)
    }
  }

}
