package edu.rice.pliny.apitrans.test

import java.io.{File, FilenameFilter}

import edu.rice.pliny.AppConfig
import edu.rice.pliny.apitrans.APIPairsExtractor
import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.IOFileFilter
import org.scalatest._
import scala.collection.JavaConverters._

class APIPairsExtractorTest extends FlatSpec with Matchers {

  def recursiveListFiles(f: File): Array[File] = {
    val these = f.listFiles
    these ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }

  "The API pairs extractor" should "logs similar methods" in {
    AppConfig.log_config
    // val proj_dir = "/home/yanxin/Downloads/similar_progs/static_analysis"
    val proj_dir = "D:\\home\\Documents\\yanxin\\similar_progs\\web"
    val repos = new File(proj_dir).listFiles()
    val src_lists : Array[Array[File]] = repos.map(repo => recursiveListFiles(repo).filter(f => f.getName == "src"))
    for(i <- 0 until src_lists.length - 1) {
      for(j <- i + 1 until src_lists.length) {
        val slist1 = src_lists(i)
        val slist2 = src_lists(j)
        for(m <- slist1.indices) {
          for(n <- slist2.indices) {
            val path1 = slist1(m).getAbsolutePath
            val path2 = slist2(n).getAbsolutePath
            val pairs = APIPairsExtractor.extract(path1, path2)
            pairs.nonEmpty shouldBe false
          }
        }
      }
    }
  }
}
