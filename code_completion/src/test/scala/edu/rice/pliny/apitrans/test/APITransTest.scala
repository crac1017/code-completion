package edu.rice.pliny.apitrans.test

import java.io.File

import edu.rice.pliny.AppConfig
import edu.rice.pliny.apitrans.JarAnalyzer.{JarConstructor, JarMethod}
import edu.rice.pliny.apitrans.hint.{HardCodedHintModel, WebHintModel}
import edu.rice.pliny.apitrans.{APITransSearch, JarAnalyzer, APITrans}
import edu.rice.pliny.language.java.{JavaDraft, JavaFastInterpreter, JavaUtils}
import org.scalatest._

class APITransTest extends FlatSpec with Matchers {

  def translate(filename : String, source : String, target : String,
                jar_paths : Seq[String] = Seq(), class_paths : Seq[String] = Seq()) : Option[JavaDraft] = {
    val prog = JavaUtils.parse_file(filename, None, None, None, jar_paths, class_paths).get
    val from_class = JavaUtils.name_list_to_qname(source.split("\\."))
    val to_class = JavaUtils.name_list_to_qname(target.split("\\."))
    val interpreter = new JavaFastInterpreter(jar_paths)
    val jar = new JarAnalyzer(jar_paths)
    val hint = new HardCodedHintModel("csv", jar_paths, jar)
    val task = new APITransSearch("csv", hint, interpreter, jar)
    task.translate(prog, from_class, to_class, interpreter)
  }

  def get_filenames(dirname : String) : Seq[String] = {
    new File(dirname).listFiles().filter(_.isFile).map(f => f.getAbsolutePath)
  }

  "The API translation algorithm" should "translate easy read line functions" in {
    AppConfig.log_config
    val csv_filename = "code_completion/src/test/resources/apitrans/csv/csv_db.java"
    val from = "java.util.Scanner"
    val to = "java.io.BufferedReader"
    val result = this.translate(csv_filename, from, to)
    result.isDefined shouldBe true
  }

  "The web scraper" should "extract the tfidf terms from the web page" in {
    val web_paths = get_filenames("code_completion/src/test/resources/apitrans/pdf/web")
    val web = new WebHintModel("pdf", web_paths)
    val jar_paths = Seq(
      "code_completion/src/test/resources/apitrans/pdf/jars/pdfbox-2.0.9.jar",
      "code_completion/src/test/resources/apitrans/pdf/jars/itextpdf-5.5.13.jar"
    )
    val analyzer = new JarAnalyzer(jar_paths)
    analyzer.funcs.foreach(clazz => {
      clazz._2.foreach {
        case JarMethod(name, t, clz, method) =>
          val clazz_name = clz.getName.split("\\.").last
          if (clazz._1.getName.startsWith("com.itextpdf.text")) {
            if (web.is_mentioned(name) && web.is_mentioned(clazz_name)) {
              println("Method: " + name + " from class " + clazz_name)
            }
          }
        case JarConstructor(name, t, clz, cons) =>
          val clazz_name = clz.getName.split("\\.").last
          if (clazz._1.getName.startsWith("com.itextpdf.text")) {
            if (web.is_mentioned(clazz_name)) {
              println("Constructor: " + name)
            }
          }
      }
    })

      /*
    List<List<String>> documents = Arrays.asList(doc1, doc2, doc3);

    TFIDFCalculator calculator = new TFIDFCalculator();
    double tfidf = calculator.tfIdf(doc1, documents, "ipsum");
    */
    println("FOO")
  }
}
