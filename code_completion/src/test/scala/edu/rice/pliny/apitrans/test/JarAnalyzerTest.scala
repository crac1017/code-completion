package edu.rice.pliny.apitrans.test

import edu.rice.pliny.AppConfig
import edu.rice.pliny.apitrans.JarAnalyzer
import edu.rice.pliny.apitrans.JarAnalyzer.{JarConstructor, JarMethod}
import org.scalatest._

class JarAnalyzerTest extends FlatSpec with Matchers {

  "The Jar Analyzer" should "get things from a jar file" in {
    AppConfig.log_config
    val jar_file = "code_completion/src/test/resources/apitrans/csv/jars/opencsv-4.1.jar"
    val analyzer = new JarAnalyzer(Seq(jar_file))
    analyzer.funcs.isEmpty shouldBe false
    for(p <- analyzer.funcs) {
      val clazz = p._1
      val methods = p._2
      println("Class: " + clazz.getName)
      for(m <- methods) {
        m match {
          case JarMethod(name, ft, clazz, method) => println(name + " : " + ft.describe())
          case JarConstructor(name, ft, clazz, cons) => println(name + " : " + ft.describe())
        }
      }
    }
  }
}
