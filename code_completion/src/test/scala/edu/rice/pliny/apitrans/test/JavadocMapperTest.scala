package edu.rice.pliny.apitrans.test

import java.io.File

import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.apitrans.MapAPIMain
import edu.rice.pliny.apitrans.hint.mapper.JavadocMapper
import org.deeplearning4j.models.embeddings.learning.impl.elements.SkipGram
import org.deeplearning4j.models.word2vec.{VocabWord, Word2Vec}
import org.deeplearning4j.text.sentenceiterator.FileSentenceIterator
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory
import org.scalatest._

import scala.util.Random

class JavadocMapperTest extends FlatSpec with Matchers {
  val logger = Logger(this.getClass)

  def print_similar(model : Word2Vec, word : String) : Unit = {
    val true_similar = model.wordsNearest(word, 10)
    this.logger.info(true_similar.size() + " '" + word + "' similar words")
    val iter = true_similar.iterator()
    while(iter.hasNext) {
      val w = iter.next()
      this.logger.info(w)
    }
  }

  "The DL4J" should "create word2vec model" in {
    val path = "code_completion/src/test/resources/apitrans/mapper/warpeace.txt"
    val sentenceIterator = new FileSentenceIterator(new File(path))
    val tokenizerFactory = new DefaultTokenizerFactory
    tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor)

    val vec = new Word2Vec.Builder()
      .minWordFrequency(2)
      .layerSize(300)
      .windowSize(5)
      .seed(42)
      .epochs(3)
      .elementsLearningAlgorithm(new SkipGram[VocabWord])
      .iterate(sentenceIterator)
      .tokenizerFactory(tokenizerFactory).build
    vec.fit()

    print_similar(vec, "body")
    print_similar(vec, "day")
    val list1 = vec.wordsNearest("body", 50)
    list1.isEmpty shouldBe false
    list1.contains("weight") shouldBe true
    list1.contains("neck") shouldBe true
    val list2 = vec.wordsNearest("day", 50)
    list2.isEmpty shouldBe false
    list2.contains("morning") shouldBe true
    list2.contains("night") shouldBe true
  }

  /*
  it should "load google's model" in {
    val path = "/Users/yanxin/Downloads/GoogleNews-vectors-negative300.bin.gz"
    val gModel = new File(path)
    val model = WordVectorSerializer.loadGoogleModel(gModel, true)

    print_similar(model, "body")
    print_similar(model, "day")
  }
  */

  it should "create a good word2vec model for javadocs" in {
    AppConfig.log_config
    val text_path = "code_completion/src/test/resources/apitrans/mapper"
    val method_path = "code_completion/src/test/resources/apitrans/mapper/methods"
    val mapper = new JavadocMapper(text_path, method_path)
    print_similar(mapper.model, "writer")
    val list1 = mapper.model.wordsNearest("writer", 50)
    list1.isEmpty shouldBe false
    print_similar(mapper.model, "open")
    val list2 = mapper.model.wordsNearest("open", 50)
    list2.isEmpty shouldBe false
    print_similar(mapper.model, "float")
    val list3 = mapper.model.wordsNearest("float", 50)
    list3.isEmpty shouldBe false
    print_similar(mapper.model, "string")
    val list4 = mapper.model.wordsNearest("string", 50)
    list4.isEmpty shouldBe false
    print_similar(mapper.model, "connect")
    val list5 = mapper.model.wordsNearest("connect", 50)
    list5.isEmpty shouldBe false
  }

  it should "find top-k similar method" in {
    AppConfig.log_config
    val text_path = "code_completion/src/test/resources/apitrans/mapper"
    val method_path = "code_completion/src/test/resources/apitrans/mapper/methods"
    val mapper = new JavadocMapper(text_path, method_path)
    /*
    val m = mapper.methods(Random.nextInt(100))
    this.logger.info("Querying using method: " + m.name)
    val result = mapper.query(m)
    this.logger.info("Result:")
    result.foreach(r => this.logger.info(r._1.toString + "Dist: " + r._2))
    result.nonEmpty shouldBe true
    */
  }
}
