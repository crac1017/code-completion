package edu.rice.pliny.apitrans.test

import java.io.File

import com.github.javaparser.ast.expr.{Expression, MethodCallExpr}
import com.github.javaparser.ast.stmt.BlockStmt
import com.typesafe.scalalogging.Logger
import edu.rice.pliny.AppConfig
import edu.rice.pliny.apitrans.hint.{HardCodedHintModel, NoHintModel}
import edu.rice.pliny.apitrans.{APITrans, APITransNL, APITransSearch, JarAnalyzer}
import edu.rice.pliny.language.java.{JavaDraft, JavaFastInterpreter, JavaUtils}
import org.scalatest._


class APITransBenchmarks extends FunSuite {
  val APITRANS_DIR_PREFIX = "code_completion/src/test/resources/apitrans/benchmark"
  val JAR_DIR = "jars"
  val WEB_DIR = "web"

  val logger = Logger(this.getClass)

  def get_filenames(dirname : String) : Seq[String] = {
    new File(dirname).listFiles().filter(_.isFile).map(f => f.getAbsolutePath)
  }


  type RefactorResult = (BlockStmt, Double)

  /**
    * Read the correct API sequence
    */
  def read_correct_api(task_name : String, jar_paths : Seq[String]) : Seq[Expression] = {
    val filename = APITRANS_DIR_PREFIX + s"/$task_name/${task_name}2.java"
    val prog = JavaUtils.parse_file(filename, None, None, None, jar_paths, Seq()).get
    val block = APITrans.find_refactoring_block(prog)
    assert(block.get._2 == "expected")
    APITrans.extract_calls(block.get._1)
  }

  def run_benchmark(task_name : String, ret_val : (String, String)) : Boolean = {
    AppConfig.log_config
    val jar_paths = get_filenames(APITRANS_DIR_PREFIX + s"/$task_name/$JAR_DIR")
    val jar = new JarAnalyzer(jar_paths)
    val exp_seq = read_correct_api(task_name, jar_paths)

    // val web_paths = get_filenames(APITRANS_DIR_PREFIX + s"/$task_name/$WEB_DIR")
    // val web = new WebHintModel(task_name, web_paths)
    // val hardcoded_hint = new HardCodedHintModel(task_name, jar_paths, jar)
    // val no_hint = new NoHintModel(task_name)
    val filename = APITRANS_DIR_PREFIX + s"/$task_name/$task_name.java"
    val prog = JavaUtils.parse_file(filename, None, None, None, jar_paths, Seq()).get
    val interpreter = new JavaFastInterpreter(jar_paths)


    /**
      * It returns two refactor results. One with no param and one with param
      */
    def run_algo(algo : APITrans) : (Option[RefactorResult], Option[RefactorResult]) = {
      val out_blocks = algo.refactor(prog, ret_val)
      var result_noparam = Option.empty[RefactorResult]
      var result_param = Option.empty[RefactorResult]

      out_blocks.foreach(b => {
        val out_seq = APITrans.extract_calls(b._1)
        logger.debug("Checking sequence correctness ================\n{}\n", out_seq.foldLeft("")((acc, s) => acc + "\n" + s.toString))
        val correct_count = APITrans.correct_num(exp_seq, out_seq)
        val correct_count_param = APITrans.correct_num(exp_seq, out_seq, consider_param = true)
        logger.debug("Correctness no   param : {} / {}", correct_count, exp_seq.size)
        logger.debug("Correctness with param : {} / {}", correct_count_param, exp_seq.size)

        val new_acc = correct_count.toDouble / exp_seq.size.toDouble
        val new_acc_param = correct_count_param.toDouble / exp_seq.size.toDouble
        if(result_noparam.isEmpty || new_acc > result_noparam.get._2) {
          result_noparam = Some(b._1, new_acc)
        }

        if(result_param.isEmpty || new_acc_param > result_param.get._2) {
          result_param = Some(b._1, new_acc_param)
        }
      })

      (result_noparam, result_param)
    }

    // refactoring
    // val task = new APITransSearch(task_name, hardcoded_hint, interpreter, jar)
    /*
    logger.debug("Running baseline algorithm.")
    val baseline = new APITransSearch(task_name, no_hint, interpreter, jar)
    val baseline_seq = run_algo(baseline)
    if(baseline_seq._1.isDefined) {
      this.logger.info("Best sequence with score {} ==============\n{}", baseline_seq._2, baseline_seq._1.get.toString)
    }
    */

    logger.debug("Running good algorithm.")
    val good_method = new APITransNL(task_name, interpreter, jar)
    val (good_seq, good_seq_param) = run_algo(good_method)
    if(good_seq.isEmpty && good_seq_param.isEmpty) {
      this.logger.error("Failed to refactor the API sequence.")
      return false
    }


    if(good_seq.isDefined) {
      this.logger.info("Best sequence with score {} for no param ==============\n{}", good_seq.get._2, good_seq.get._1.toString)
    }

    if(good_seq_param.isDefined) {
      this.logger.info("Best sequence with score {} for param ==============\n{}", good_seq_param.get._2, good_seq_param.get._1.toString)
    }
    true
  }

  test("csv") {
    val task_name = "csv"
    val ret_val = ("fields", "[Ljava.lang.String;")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("csv3") {
    val task_name = "csv3"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("csv_db") {
    val task_name = "csv_db"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("csv_delim") {
    val task_name = "csv_delim"
    val ret_val = ("fields", "[Ljava.lang.String;")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("email_check") {
    val task_name = "email_check"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("email_delete") {
    val task_name = "email_delete"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("email_login") {
    val task_name = "email_login"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("email_send") {
    val task_name = "email_send"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("ftp") {
    val task_name = "ftp"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("ftp_delete") {
    val task_name = "ftp_delete"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("ftp_download") {
    val task_name = "ftp_download"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("ftp_login") {
    val task_name = "ftp_login"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("ftp_upload") {
    val task_name = "ftp_upload"
    val ret_val = ("", "void")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("graphics") {
    val task_name = "graphics"
    val ret_val = ("", "void")
    this.run_benchmark(task_name, ret_val)
  }

  test("gui") {
    val task_name = "gui"
    val ret_val = ("", "void")
    this.run_benchmark(task_name, ret_val)
  }

  test("htm") {
    val task_name = "htm"
    val ret_val = ("href", "java.lang.String")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("htm_addattr") {
    val task_name = "htm_addattr"
    val ret_val = ("e", "org.jsoup.nodes.Element")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("htm_rmattr") {
    val task_name = "htm_rmattr"
    val ret_val = ("n", "org.jsoup.nodes.Node")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("htm_addnode") {
    val task_name = "htm_addnode"
    val ret_val = ("e", "org.jsoup.nodes.Element")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("htm_parse") {
    val task_name = "htm_parse"
    val ret_val = ("doc", "org.jsoup.nodes.Document")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("htm_title") {
    val task_name = "htm_title"
    val ret_val = ("title", "java.lang.String")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("htm3") {
    val task_name = "htm3"
    val ret_val = ("result", "java.lang.String")
    this.run_benchmark(task_name, ret_val)
  }

  test("http") {
    val task_name = "http"
    val ret_val = ("text", "java.lang.String")
    this.run_benchmark(task_name, ret_val)
  }

  test("http_post") {
    val task_name = "http_post"
    val ret_val = ("text", "java.lang.String")
    this.run_benchmark(task_name, ret_val)
  }

  test("httpserver") {
    val task_name = "httpserver"
    val ret_val = ("", "void")

    this.run_benchmark(task_name, ret_val)
  }

  ignore("jparser") {
    val task_name = "jparser"
    val ret_val = ("result", "java.lang.String")
    this.run_benchmark(task_name, ret_val)
  }

  ignore("jparser3") {
    val task_name = "jparser3"
    val ret_val = ("parser", "org.eclipse.jdt.core.dom.ASTParser")
    this.run_benchmark(task_name, ret_val)
  }

  ignore("kmeans3") {
    val task_name = "kmeans3"
    val ret_val = ("", "void")
    this.run_benchmark(task_name, ret_val)
  }

  test("ml_classification") {
    val task_name = "ml_classification"
    val ret_val = ("r", "jsat.classifiers.CategoricalResults")
    this.run_benchmark(task_name, ret_val)
  }

  test("ml_cluster") {
    val task_name = "ml_cluster"
    val ret_val = ("l", "java.util.List")
    this.run_benchmark(task_name, ret_val)
  }

  test("ml_neural") {
    val task_name = "ml_neural"
    val ret_val = ("r", "jsat.classifiers.CategoricalResults")
    this.run_benchmark(task_name, ret_val)
  }

  test("ml_regression") {
    val task_name = "ml_regression"
    val ret_val = ("d", "double")
    this.run_benchmark(task_name, ret_val)
  }

  test("ml_stemming") {
    val task_name = "ml_stemming"
    val ret_val = ("s", "java.lang.String")
    this.run_benchmark(task_name, ret_val)
  }

  test("nlp_pos") {
    val task_name = "nlp_pos"
    val ret_val = ("t", "[Ljava.lang.String;")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("nlp_sentence") {
    val task_name = "nlp_sentence"
    val ret_val = ("t", "[Ljava.lang.String;")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("nlp_token") {
    val task_name = "nlp_token"
    val ret_val = ("t", "[Ljava.lang.String;")
    assert(this.run_benchmark(task_name, ret_val))
  }

  test("pdf_read") {
    val task_name = "pdf_read"
    val ret_val = ("", "void")
    this.run_benchmark(task_name, ret_val)
  }

  test("pdf_write") {
    val task_name = "pdf_write"
    val ret_val = ("", "void")
    this.run_benchmark(task_name, ret_val)
  }

  ignore("regression") {
    val task_name = "regression"
    val ret_val = ("result", "double")
    this.run_benchmark(task_name, ret_val)
  }


  test("word") {
    val task_name = "word"
    val ret_val = ("text", "java.lang.String")
    this.run_benchmark(task_name, ret_val)
  }

  test("word3") {
    val task_name = "word3"
    val ret_val = ("", "void")
    this.run_benchmark(task_name, ret_val)
  }


}
