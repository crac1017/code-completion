package edu.rice.pliny.language.java.test

import com.github.javaparser.symbolsolver.model.typesystem.{ArrayType, PrimitiveType, ReferenceType}
import edu.rice.pliny.language.java._
import org.scalatest.{FlatSpec, Matchers}

/**
  * Test class for java draft
  */
class JavaDraftTest extends FlatSpec with Matchers {

  "Get hole function" should "return a hole when there's a hole" in {
    val filename = "src/test/resources/simple_stmt_hole.java"
    val draft = JavaUtils.parse_file(filename, None, None, None, Seq(), Seq()).get
    val hole = draft.get_stmt_hole_with_tests
    assert(hole.isDefined)
  }

  "Get hole function" should "return none when there's no hole" in {
    val filename = "src/test/resources/simple.java"
    val draft = JavaUtils.parse_file(filename, None, None, None, Seq(), Seq()).get
    val hole = draft.get_stmt_hole_with_tests
    assert(hole.isEmpty)
  }

  "The replace function" should "replace draft node with another node" in {
    val filename = "src/test/resources/simple_expr_hole.java"
    val draft = JavaUtils.parse_file(filename, None, None, None, Seq(), Seq()).get

    val complete_draft = JavaUtils.parse_file("src/test/resources/corpus/binsearch/binsearch2.java", None, None, None, Seq(), Seq()).get

    val hole = draft.get_expr_hole
    assert(hole.isDefined)

    val iter = new JavaDraftNodeIterator(new JavaDraftNode(complete_draft.method))
    while(iter.hasNext) {
      val node = iter.next()
      if(node.toString.equals("(low + high) / 2")) {
        draft.replace(hole.get, node)
      }
    }
  }

  "The replace function" should "not replace all the copies of the same node" in {
    val filename = "src/test/resources/simple_expr_hole2.java"
    val draft = JavaUtils.parse_file(filename, None, None, None, Seq(), Seq()).get

    val complete_draft = JavaUtils.parse_file("src/test/resources/corpus/binsearch/binsearch2.java", None, None, None, Seq(), Seq()).get

    val hole1 = draft.get_expr_hole
    assert(hole1.isDefined)

    val iter1 = new JavaDraftNodeIterator(new JavaDraftNode(complete_draft.method), Some(node => node.toString == "low <= high"))
    val node1 = iter1.next()
    val new_draft1 = draft.replace(hole1.get, node1)

    val hole2 = new_draft1.get_expr_hole
    assert(hole2.isDefined)

    val iter2 = new JavaDraftNodeIterator(new JavaDraftNode(complete_draft.method), Some(node => node.toString == "(low + high) / 2"))
    val node2 = iter2.next()
    val new_draft2 = new_draft1.replace(hole2.get, node2)

    val hole3 = new_draft2.get_expr_hole
    assert(hole3.isEmpty)
  }

  "A draft" should "be well typed." in {
    val draft = JavaUtils.parse_file("src/test/resources/binsearch_float.java", None, None, None, Seq(), Seq()).get
    val iter1 = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(n => n.toString == "mid + 1"))
    val node1 = iter1.next()
    draft.get_type(node1).asInstanceOf[JavaDraftNodeType].t should be (PrimitiveType.INT)

    val iter2 = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(n => draft.is_expr(n) && n.toString == "array"))
    val node2 = iter2.next()
    draft.get_type(node2).asInstanceOf[JavaDraftNodeType].t shouldBe a [ArrayType]
    draft.get_type(node2).asInstanceOf[JavaDraftNodeType].t.arrayLevel() should be (1)
    draft.get_type(node2).asInstanceOf[JavaDraftNodeType].t.asArrayType().getComponentType should be (PrimitiveType.INT)

    val iter3 = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(n => n.toString == "array.length - 1"))
    val node3 = iter3.next()
    draft.get_type(node3).asInstanceOf[JavaDraftNodeType].t should be (PrimitiveType.INT)

    val iter4 = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(n => draft.is_expr(n) && n.toString == "result"))
    val node4 = iter4.next()
    draft.get_type(node4).asInstanceOf[JavaDraftNodeType].t should be (PrimitiveType.INT)

    val iter5 = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(n => n.toString == "array[mid]"))
    val node5 = iter5.next()
    draft.get_type(node5).asInstanceOf[JavaDraftNodeType].t should be (PrimitiveType.INT)

    val iter6 = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(n => n.toString == "2.0"))
    val node6 = iter6.next()
    draft.get_type(node6).asInstanceOf[JavaDraftNodeType].t should be (PrimitiveType.DOUBLE)

    val iter7 = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(n => n.toString == "(low + high) / 2.0"))
    val node7 = iter7.next()
    draft.get_type(node7).asInstanceOf[JavaDraftNodeType].t should be (PrimitiveType.DOUBLE)

    val iter8 = JavaDraftIteratorFactory.create_draft_iterator(draft, Some(n => draft.is_expr(n) && n.toString == "mid"))
    val node8 = iter8.next()
    draft.get_type(node8).asInstanceOf[JavaDraftNodeType].t should be (PrimitiveType.INT)
  }

  "A draft" should "detect expression holes" in {
    val draft1 = JavaUtils.parse_file("src/test/resources/binsearch_draft.java", None, None, None, Seq(), Seq()).get
    val hole1 = draft1.get_expr_hole
    hole1.isDefined should be (true)
    draft1.is_expr_hole(hole1.get) should be (true)

    val draft2 = JavaUtils.parse_file("src/test/resources/binsearch_draft4.java", None, None, None, Seq(), Seq()).get
    val hole2 = draft2.get_expr_hole
    hole2.isDefined should be (true)
    draft2.is_expr_hole(hole2.get) should be (true)
  }

  "A draft" should "detect statement holes" in {
    /*
    val draft1 = JavaUtils.parse_file("src/test/resources/binsearch_draft.java").get
    val hole1 = draft1.get_stmt_hole
    hole1.isDefined should be (true)
    draft1.is_expr_hole(hole1.get) should be (true)
    */

    val draft2 = JavaUtils.parse_file("src/test/resources/binsearch_draft2.java", None, None, None, Seq(), Seq()).get
    val hole2 = draft2.get_stmt_hole_with_tests
    hole2.isDefined should be (true)
    draft2.is_expr_hole(hole2.get) should be (false)

    val draft3 = JavaUtils.parse_file("src/test/resources/binsearch_draft3.java", None, None, None, Seq(), Seq()).get
    val hole3 = draft3.get_stmt_hole_with_tests
    hole3.isDefined should be (true)
    draft3.is_expr_hole(hole3.get) should be (false)
  }

  /*
  "A draft" should "rename properly" in {
    val draft = JavaUtils.parse_file("src/test/resources/rename_test.java").get
    for(p <- draft.env.env) {
      val vname = p._1
      val binding = p._2.asInstanceOf[JavaBinding]
      if(vname.startsWith("field") || vname == "binsearch") {
        binding.scope.isDefined should be (true)
      } else {
        binding.scope.isEmpty should be (true)
      }
    }

    val new_draft = draft.rename(s => s"_${s}_")
  }
  */

}
