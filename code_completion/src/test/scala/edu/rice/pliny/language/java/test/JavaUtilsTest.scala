package edu.rice.pliny.language.java.test

import com.github.javaparser.ast.expr.{BinaryExpr, Expression, MethodCallExpr}
import com.github.javaparser.ast.visitor.VoidVisitorAdapter
import edu.rice.pliny.language.java._
import org.scalatest.{FlatSpec, Matchers}
import java.io.File

import com.github.javaparser.ast.stmt.ExpressionStmt

/**
  * Test class for java draft
  */
class JavaUtilsTest extends FlatSpec with Matchers {

  "JavaUtils" should "parse files" in {
    val draft = JavaUtils.parse_file("src/test/resources/comment_test.java", None, None, None, Seq(), Seq())
    draft.isDefined shouldBe true
    val unit = draft.get.compilation_unit
    unit should not be null

    val foo_main = JavaUtils.get_class(unit, "FooMain")
    foo_main.isDefined shouldBe true
    foo_main.get.getNameAsString shouldBe "FooMain"

    val binsearch_method = JavaUtils.get_method(foo_main.get, "binsearch")
    binsearch_method.isDefined shouldBe true
    binsearch_method.get.getNameAsString shouldBe "binsearch"

    val binsearch2_method = JavaUtils.get_method(foo_main.get, "binsearch2")
    binsearch2_method.isDefined shouldBe true
    binsearch2_method.get.getNameAsString shouldBe "binsearch2"

    draft.get.method.getNameAsString shouldBe "binsearch"
  }

  "Java Utils" should "parse file method" in {
    val draft = JavaUtils.parse_file("src/test/resources/comment_test.java", None, Some("binsearch"), None, Seq(), Seq()).get
    draft.method.getNameAsString shouldBe "binsearch"
    val draft2 = JavaUtils.parse_file("src/test/resources/comment_test.java", None, Some("binsearch2"), None, Seq(), Seq()).get
    draft2.method.getNameAsString shouldBe "binsearch2"
  }

  "Java Utils" should "parse source method" in {
    val src1 = scala.io.Source.fromFile(new File("src/test/resources/comment_test.java")).mkString
    val draft = JavaUtils.parse_src(src1, None, Some("binsearch"), None, Seq(), Seq()).get
    draft.method.getNameAsString shouldBe "binsearch"
    val draft2 = JavaUtils.parse_src(src1, None, Some("binsearch2"), None, Seq(), Seq()).get
    draft2.method.getNameAsString shouldBe "binsearch2"
  }

  "Java Utils" should "get all methods" in {
    val draft = JavaUtils.parse_file("src/test/resources/comment_test.java", None, Some("binsearch"), None, Seq(), Seq()).get
    val all_methods = JavaUtils.get_all_methods(draft.cclass)
    all_methods(0).getNameAsString shouldBe "binsearch"
    all_methods(1).getNameAsString shouldBe "binsearch2"
  }

  "Java Utils" should "generate source code from ast" in {
    val draft = JavaUtils.parse_file("src/test/resources/comment_test.java", None, None, None, Seq(), Seq()).get
    val src = draft.toStringWithClass
    val new_draft = JavaUtils.parse_src(src, None, None, None, Seq(), Seq()).get
    val unit = new_draft.compilation_unit

    val foo_main = JavaUtils.get_class(unit, "FooMain")
    foo_main.isDefined shouldBe true
    foo_main.get.getNameAsString shouldBe "FooMain"

    val binsearch_method = JavaUtils.get_method(foo_main.get, "binsearch")
    binsearch_method.isDefined shouldBe true
    binsearch_method.get.getNameAsString shouldBe "binsearch"
  }

  "Java Utils" should "get hole if there's one" in {
    val draft = JavaUtils.parse_file("src/test/resources/binsearch_draft2.java", None, None, None, Seq(), Seq()).get
    val binsearch_method = JavaUtils.get_method(draft.cclass, "binsearch").get
    val hole = draft.get_stmt_hole_with_tests
    hole.isDefined shouldBe true
    val expr = hole.get.asInstanceOf[JavaDraftHole].node.asInstanceOf[ExpressionStmt].getExpression
    expr.isInstanceOf[MethodCallExpr] shouldBe true
    expr.asInstanceOf[MethodCallExpr].getNameAsString shouldBe JavaUtils.HOLE_NAME
  }

  "Java Utils" should "not return hole if none" in {
    val draft = JavaUtils.parse_file("src/test/resources/comment_test.java", None, None, None, Seq(), Seq()).get
    val binsearch_method = JavaUtils.get_method(draft.cclass, "binsearch").get
    val hole = draft.get_stmt_hole_with_tests
    hole.isEmpty shouldBe true
  }

  "Java Utils" should "help replace expressions" in {
    val draft = JavaUtils.parse_file("src/test/resources/binsearch_draft.java", None, None, None, Seq(), Seq()).get
    val hole = draft.get_expr_hole.get

    val good_prog = JavaUtils.parse_file("src/test/resources/comment_test.java", None, None, None, Seq(), Seq()).get

    var expr : Option[Expression] = None

    new VoidVisitorAdapter[Object] {
      override def visit(b : BinaryExpr, obj : Object) : Unit = {
        if(expr.isEmpty) {
          expr = Some(b)
        }
      }
    }.visit(good_prog.method, null)

    val new_draft = draft.replace(hole, new JavaDraftNode(expr.get))
    val new_hole = new_draft.get_expr_hole
    new_hole.isEmpty shouldBe true

  }
}
