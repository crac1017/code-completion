package edu.rice.pliny.language.java.test

import java.io.File

import com.github.javaparser.ast.Node
import com.github.javaparser.ast.body.MethodDeclaration
import com.github.javaparser.ast.expr.MethodCallExpr
import com.github.javaparser.ast.visitor.{TreeVisitor, VoidVisitorAdapter}
import edu.rice.pliny.language.java._
import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.{DirectoryFileFilter, RegexFileFilter}
import org.scalatest.{FlatSpec, Matchers}

import collection.JavaConverters._
import scala.collection.mutable

/**
  * Test class for java draft
  */
class DSLTest extends FlatSpec with Matchers {

  val JAVA_SRC = "src/main/resources/jdk8/src/share/classes"
  val JAR_REPO = "/Users/yanxin/.m2/repository"
  val JAR_FILES : Seq[String] = {
    val files = FileUtils.listFiles(new File(JAR_REPO), new RegexFileFilter(".*\\.jar$"), DirectoryFileFilter.DIRECTORY).asScala
    files.map(_.getAbsolutePath).toSeq
  }

  /**
    * Get the corresponding method declaration from a qualified method name
    */
  def get_decl(method_qname : String) : MethodDeclaration = {
    val names = method_qname.split("\\.")
    val (pathname, class_name, method_name) = {
      val result = new StringBuilder
      result.append(this.JAVA_SRC)
      for(n <- names.dropRight(1)) {
        result.append("/")
        result.append(n)
      }
      result.append(".java")
      (result.toString(), names(names.size - 2), names(names.size - 1))
    }
    val parsed = JavaUtils.parse_file(pathname, Some(class_name), Some(method_name), None, JAR_FILES, Seq(JAVA_SRC)).get
    parsed.method
  }

  def find_method_calls(node : Node, filter : Option[MethodCallExpr => Boolean]) : Seq[MethodCallExpr] = {
    val result = mutable.ArrayBuffer.empty[MethodCallExpr]
    new TreeVisitor {
      override def process(node: Node): Unit = {
        node match {
          case mce : MethodCallExpr if filter.get(mce) => result += mce
          case _ =>
        }
      }
    }.visitBreadthFirst(node)
    result
  }

  def get_syscalls(draft : JavaDraft) : Seq[MethodDeclaration] = {
    val result = mutable.ArrayBuffer.empty[MethodDeclaration]

    def helper(node : Node) : Unit = {
      val calls = this.find_method_calls(node, Some(
        mce => mce.getNameAsString == "nextLine"
      ))
      val decls = calls.map(mce => {
        val symbol = draft.type_solver.solve(mce)
        val decl = {
          val semantic_decl = symbol.getCorrespondingDeclaration
          val qname = semantic_decl.getQualifiedName
          this.get_decl(qname)
        }
        result += decl
      })
    }
    result
  }

  "The DSL" should "find a method call's declaration" in {
    val filename = "src/test/resources/csv_db.java"
    val draft = JavaUtils.parse_file(filename, None, None, None, JAR_FILES, Seq(JAVA_SRC)).get
    val calls = this.find_method_calls(draft.method, Some(mce => mce.getNameAsString == "nextLine"))
    println(calls)
  }
}
