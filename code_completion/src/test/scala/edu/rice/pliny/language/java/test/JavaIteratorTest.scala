package edu.rice.pliny.language.java.test

import edu.rice.pliny.draft.IteratorFactory
import edu.rice.pliny.language.java.{JavaDraftIteratorFactory, JavaUtils}
import org.scalatest.{FlatSpec, Matchers}

/**
  * Test class for java draft
  */
class JavaIteratorTest extends FlatSpec with Matchers {

  "Draft Node Iterator" should "have nodes to start with" in {
    val filename = "src/test/resources/simple.java"
    val draft = JavaUtils.parse_file(filename, None, None, None, Seq(), Seq()).get
    val iter = JavaDraftIteratorFactory.create_draft_iterator(draft)
    assert(iter.hasNext)
  }

  "Draft Node Iterator next" should "return nodes if there's any" in {
    val filename = "src/test/resources/simple.java"
    val draft = JavaUtils.parse_file(filename, None, None, None, Seq(), Seq()).get
    val iter = JavaDraftIteratorFactory.create_draft_iterator(draft)
    val n1 = iter.next()
    iter.hasNext shouldBe true
    n1.toString shouldBe "f"
    // println(n1)
    val n2 = iter.next()
    iter.hasNext shouldBe true
    n2.toString shouldBe "int"
    // println(n2)
    val n3 = iter.next()
    iter.hasNext shouldBe true
    n3.toString shouldBe "integer x"
    // println(n3)
    val n4 = iter.next()
    iter.hasNext shouldBe true
    n4.toString shouldBe "int"
    // println(n4)
    val n5 = iter.next()
    // println(n5)
    iter.hasNext shouldBe true
    n5.toString shouldBe "integer[]"
  }

  "Draft Ref Iterator next" should "return nodes if there's any" in {
    val filename = "src/test/resources/simple.java"
    val draft = JavaUtils.parse_file(filename, None, None, None, Seq(), Seq()).get
    val iter = JavaDraftIteratorFactory.create_draft_ref_iterator(draft)
    assert(iter.hasNext)
    val n1 = iter.next()
    n1.toString should be ("x")
    assert(iter.hasNext)
    val n2 = iter.next()
    n2.toString should be ("array")
    iter.hasNext should be (true)
    val n3 = iter.next()
    n3.toString should be ("c")
    val n4 = iter.next()
    n4.toString should be ("foo")
    val n5 = iter.next()
    n5.toString should be ("bar")
    val n6 = iter.next()
    n6.toString should be ("new_array")
    val n7 = iter.next()
    n7.toString should be ("g1")
  }

  "Draft Ref Iterator predicate" should "filter out nodes" in {
    val draft = JavaUtils.parse_file("src/test/resources/corpus/binsearch/binsearch2.java", None, None, None, Seq(), Seq()).get
    val iter = JavaDraftIteratorFactory.create_draft_ref_iterator(draft, Some(n => n.toString.equals("x")))
    while(iter.hasNext) {
      iter.next().toString should be ("x")
    }

    val iter2 = JavaDraftIteratorFactory.create_draft_ref_iterator(draft, Some(n => n.toString.equals("mid")))
    while(iter2.hasNext) {
      iter2.next().toString should be ("mid")
    }

    val iter3 = JavaDraftIteratorFactory.create_draft_ref_iterator(draft, Some(n => n.toString.equals("foo")))
    while(iter3.hasNext) {
      fail()
    }
  }

  "Draft node iterator predicate" should "filter out nodes" in {
    val draft = JavaUtils.parse_file("src/test/resources/binsearch_bug.java", None, None, None, Seq(), Seq()).get
    val iter = JavaDraftIteratorFactory.create_draft_ref_iterator(draft, Some(n => n.toString.equals("mid + 1")))
    while(iter.hasNext) {
      iter.next().toString should be ("mid + 1")
    }

    val iter2 = JavaDraftIteratorFactory.create_draft_ref_iterator(draft, Some(n => n.toString.equals("mid - 1")))
    while(iter2.hasNext) {
      fail()
    }
  }

}
