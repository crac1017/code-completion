package edu.rice.pliny.apitrans.problems;

import jsat.classifiers.ClassificationDataSet;
import jsat.classifiers.DataPoint;
import jsat.io.CSV;
import jsat.regression.RegressionDataSet;
import org.junit.Test;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import static org.junit.Assert.*;

public class MLTest {

    @Test
    public void testClassify() throws IOException {
        Set<Integer> cats = new TreeSet<>();
        cats.add(1);
        FileReader reader = new FileReader("code_completion/src/main/resources/api-refactoring-web/problems/ml/resources/data.csv");
        ClassificationDataSet ds = CSV.readC(1, reader, 0, cats);
        DataPoint dp = ds.getDataPoint(8);

        int cat = ML.classify(ds, dp);
        assertEquals(1, cat);
    }

    @Test
    public void testCluster() throws IOException {
        Set<Integer> cats = new TreeSet<>();
        cats.add(1);
        FileReader reader = new FileReader("code_completion/src/main/resources/api-refactoring-web/problems/ml/resources/data.csv");
        ClassificationDataSet ds = CSV.readC(1, reader, 0, cats);
        // DataPoint dp = ds.getDataPoint(8);

        int cat = ML.cluster(ds, 2);
        assertTrue(cat == 1 || cat == 0);
    }

    @Test
    public void testRegress() throws IOException {
        Set<Integer> cats = new TreeSet<>();
        cats.add(1);
        FileReader reader = new FileReader("code_completion/src/main/resources/api-refactoring-web/problems/ml/resources/data.csv");
        RegressionDataSet ds = CSV.readR(1, reader, 0, Collections.EMPTY_SET);
        DataPoint dp = ds.getDataPoint(8);

        double r = ML.regress(ds, dp);
        assertEquals(1.04, r, 0.0001);
    }

    @Test
    public void testNeural() throws IOException {
        Set<Integer> cats = new TreeSet<>();
        cats.add(1);
        FileReader reader = new FileReader("code_completion/src/main/resources/api-refactoring-web/problems/ml/resources/data.csv");
        RegressionDataSet ds = CSV.readR(1, reader, 0, Collections.EMPTY_SET);
        DataPoint dp = ds.getDataPoint(8);

        double r = ML.neural(ds, dp);
        assertEquals(0.99, r, 1);
    }
}

