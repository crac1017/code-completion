package edu.rice.pliny.apitrans.problems;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

public class GraphicsImgTest {

    @Test
    public void testGraphicsImg() throws SlickException {
        Graphics g = Mockito.mock(Graphics.class);
        Image img = Mockito.mock(Image.class);
        int rx = 0;
        int ry = 1;
        int rw = 10;
        int rh = 20;
        int x = 100;
        int y = 200;
        doNothing().when(g).drawRect(isA(Integer.class), isA(Integer.class), isA(Integer.class), isA(Integer.class));
        doNothing().when(g).drawImage(isA(Image.class), isA(Integer.class), isA(Integer.class));
        GraphicsImg.graphics_img(g, rx, ry, rw, rh, x, y, img);
        verify(g, times(1)).drawRect(rx, ry, rw, rh);
        verify(g, times(1)).drawImage(img, x, y);
    }
}

