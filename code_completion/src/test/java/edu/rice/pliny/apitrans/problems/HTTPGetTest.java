package edu.rice.pliny.apitrans.problems;
import org.junit.Test;
import java.io.IOException;
import java.net.URL;

import static junit.framework.TestCase.assertTrue;

public class HTTPGetTest {
    @Test
    public void getTest() throws IOException {
        String url = "https://httpbin.org/get";
        String response = HTTPGet.get(new URL(url));
        System.out.println(response);
        assertTrue(response.contains("\"Host\": \"httpbin.org\""));
    }
}
