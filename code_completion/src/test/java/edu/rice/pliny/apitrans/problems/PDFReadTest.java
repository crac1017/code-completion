package edu.rice.pliny.apitrans.problems;

import org.junit.Test;

import java.io.IOException;
import static org.junit.Assert.*;

public class PDFReadTest {

    @Test
    public void readTest() throws IOException {
        String filename = "code_completion/src/main/resources/api-refactoring-web/problems/pdf_read/resources/foobar.pdf";
        String content = PDFRead.read(filename);
        assertEquals("foobar", content.trim());
    }
}

