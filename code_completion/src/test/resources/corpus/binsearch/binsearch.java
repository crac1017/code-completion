package com.p4.p5;
import p1.p2.p3.Bar;

/**
 * This is the class's comment
 */
public class FooMain {

    /**
     * This is the comment for binsearch
     * @param array
     * @param x
     * @return
     */
    public int binarySearch(int[] array, int x) {
        // this is the first line
        int result = -1;
        int low = 0;
        int high = array.length - 1;

        while(low <= high) {
            // set the mid
            int mid = (low + high) / 2;
            if(array[mid] > x) {
                high = mid - 1;
            } else if(array[mid] < x) {
                low = mid + 1;
            } else {
                result = mid;
                break;
            }
        }

        return result;
    }
}
