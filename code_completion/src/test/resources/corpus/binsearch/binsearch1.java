package com.p4.p5;
import p1.p2.p3.Bar;
public class BinarySearch {

    public static int binarySearch(int value, int lo, int hi, int[] values) {
        if (lo > hi) return -1;
        int mid = (hi - lo) / 2 + lo;
        if (values[mid] == value) {
            return mid;
        } else if (values[mid] < value) {
            lo = mid + 1;
        } else {
            hi = mid - 1;
        }

        return binarySearch(value, lo, hi, values);
    }

}