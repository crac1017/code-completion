/**
 * This is the class's comment
 */
public class FooMain {

    /**
     * COMMENT:
     * test
     *
     * TEST:
     * integer [] array = new integer [] {-100, -20, 0, 4, 100, 600, 601};
     * __pliny_solution__
     * print(binsearch(array, -20) == 1 &&
     *       binsearch(array, 4) == 3 &&
     *       binsearch(array, 601) == 6);
     */
    public int binsearch(int[] array, int x) {
        // this is the first line
        int result = -1;
        int low = ??;
        int high = array.length - 1;

        while(?? <= high) {
            // set the mid
            int mid = (low + high) / 2;
            if(array[mid] > x) {
                high = mid - 1;
            } else if(array[mid] < x) {
                low = mid + 1;
            } else {
                result = mid;
                break;
            }
        }

        return result;
    }
}
