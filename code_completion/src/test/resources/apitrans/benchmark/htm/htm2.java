package edu.rice.pliny.apitrans.examples;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;
import java.lang.System;


public class HTMLLink {

    public void get_link(String content, String attr, String selector) {
        //refactor:expected
        {
            Document doc = Jsoup.parse(content);
            Elements links = doc.select(selector);
            Element link = links.first();
            String href = link.attr(attr);
        }
    }
}
