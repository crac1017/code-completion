import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

import java.io.File;
import java.util.Scanner;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.Reader;

public class NLPSentence {
    public void sentence(String content, File model_file, String[] tokens, String[] whites) {
        //refactor:expected
        {
            SentenceModel model = new SentenceModel(model_file);
            SentenceDetectorME detector = new SentenceDetectorME(model);
            String[] t = detector.sentDetect(content);
        }
    }
}