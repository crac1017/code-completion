import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import org.apache.commons.mail.EmailException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import javax.mail.MessagingException;
import java.io.IOException;
import java.io.OutputStream;

public class FTPDownload {

    public void download(String username, String password, String host, String path, String filename, OutputStream out) {

        //refactor:expected
        {
            SSHClient ssh = new SSHClient();
            SFTPClient ftp = ssh.newSFTPClient();
            ssh.authPassword(username, password);
            ssh.connect(host);
            ftp.get(path, filename);
            ssh.disconnect();
        }
    }
}
