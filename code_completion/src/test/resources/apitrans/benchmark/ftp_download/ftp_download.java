import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import org.apache.commons.mail.EmailException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import java.io.IOException;
import java.io.OutputStream;

public class FTPDownload {
    public void download(String username, String password, String host, String path, String filename, OutputStream out) {
        //refactor:net.schmizz
        {
            FTPClient f = new FTPClient();
            f.connect(host);
            f.login(username, password);
            f.retrieveFile(path, out);
            f.disconnect();
        }
    }
}
