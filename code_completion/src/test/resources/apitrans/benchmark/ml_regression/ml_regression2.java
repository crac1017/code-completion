import jsat.regression.RegressionDataSet;
import jsat.regression.RidgeRegression;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MLRegression {
    public void regress(double lambda, RegressionDataSet train_data, double[][] x, double[] y) {
        //refactor:expected
        {
            RidgeRegression model = new RidgeRegression(lambda);
            model.train(train_data);
            DataPoint dp = train_data.getDataPoint(0);
            double d = model.regress(dp);
        }
    }
}
