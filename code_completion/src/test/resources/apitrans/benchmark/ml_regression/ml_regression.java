import jsat.regression.RegressionDataSet;
import smile.regression.RidgeRegression;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MLRegression {
    public void regress(double lambda, double[][] x, double[] y, RegressionDataSet train_data) {
        //refactor:jsat
        {
            RidgeRegression.Trainer trainer = new RidgeRegression.Trainer(lambda);
            RidgeRegression model = trainer.train(x, y);
            model.predict(x[0]);
        }
    }
}
