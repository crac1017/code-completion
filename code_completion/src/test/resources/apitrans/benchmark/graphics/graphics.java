import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import org.newdawn.slick.Graphics;

public class GraphicsImg {
  public void graphics_img(Graphics2D g2d, Graphics g, int rx, int ry, int rw, int rh, int x, int y, String filename) {
      //refactor:org.newdawn
      {
          File img_file = new File(filename);
          g2d.drawRect(rx, ry, rw, rh);
          BufferedImage img = ImageIO.read(img_file);
          g2d.drawImage(img, x, y, null);
      }
  }
}
