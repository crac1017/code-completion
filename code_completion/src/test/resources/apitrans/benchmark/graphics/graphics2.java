import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import java.awt.Graphics2D;

public class GraphicsImg {
    public void graphics_img(Graphics2D g2d, Graphics g, int rx, int ry, int rw, int rh, int x, int y, String filename) {
        //refactor:expected
        {
            g.drawRect(rx, ry, rw, rh);
            Image img = new Image(filename);
            g.drawImage(img, x, y);
        }
    }
}
