import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CSV {
    /**
     * COMMENT:
     * Reading a csv file
     *
     * TEST:
     * import com.opencsv.CSVReader;
     * import java.io.BufferedReader;
     * import java.io.File;
     * import java.io.FileReader;
     * import java.io.IOException;
     * __pliny_solution__
     *
     * String filename = "code_completion/src/test/resources/apitrans/csv/foo.csv";
     * String[] result = read_csv(filename);
     * String[] ans = new String[]{"1", "2", "3", "4"};
     * boolean _result_ = true;
     * for(int i = 0; i < result.length; ++i) {
     *   if(!result[i].equals(ans[i])) {
     *       _result_ = false;
     *   }
     * }
     */
    public String[] read_csv(String filename) {
        File f = new File(filename);
        FileReader reader = new FileReader(f);

        //refactor:expected
        {
            CSVReader csv_reader = new CSVReader(reader);
            String[] fields = csv_reader.readNext();
        }

        return fields;
    }
}