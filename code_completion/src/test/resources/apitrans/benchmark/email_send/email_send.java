import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

public class EmailSend {

    public void send(String username, String password, String from, String subject, String msg, String to,
                     Properties prop, Message.RecipientType rt) {
        //refactor:javax.mail
        {
            SimpleEmail email = new SimpleEmail();
            // email.setSmtpPort(587);
            DefaultAuthenticator auth = new DefaultAuthenticator(username, password);
            // email.setHostName(smtp);
            email.setFrom(from);
            email.setSubject(subject);
            email.setMsg(msg);
            email.addTo(to);
            email.send();
        }
    }
}
