import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

public class EmailSend {

    public void send(String username, String password, String from, String subject, String msg, String to,
                     Properties prop, Message.RecipientType rt) {
        //refactor:expected
        {
            DefaultAuthenticator auth = new DefaultAuthenticator(username, password);
            Session session = Session.getDefaultInstance(prop, auth);
            MimeMessage message = new MimeMessage(session);
            InternetAddress from_addr = new InternetAddress(from);
            message.setRecipients(rt, to);
            message.setFrom(from_addr);
            message.setSubject(subject);
            message.setText(msg);
            Transport.send(message);
        }
    }
}
