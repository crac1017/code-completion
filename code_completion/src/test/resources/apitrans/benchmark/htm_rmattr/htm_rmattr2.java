package edu.rice.pliny.apitrans.examples;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;
import java.lang.System;


public class HTMLRmAttr {

    public void rm_attr(String content, String attr) {
        //refactor:expected
        {
            Document doc = Jsoup.parse(content);
            Elements nodes = doc.getElementsByAttribute(attr);
            Element target = nodes.first();
            Node n = target.removeAttr(attr);
        }
    }
}
