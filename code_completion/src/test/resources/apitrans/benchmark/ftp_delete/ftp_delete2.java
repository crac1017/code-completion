import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import org.apache.commons.mail.EmailException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import javax.mail.MessagingException;
import java.io.IOException;

public class FTPDelete {

    public void delete(String username, String password, String host, String path) {

        //refactor:expected
        {
            SSHClient ssh = new SSHClient();
            SFTPClient ftp = ssh.newSFTPClient();
            ssh.authPassword(username, password);
            ssh.connect(host);
            ftp.rm(path);
            ssh.disconnect();
        }
    }
}
