import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import org.apache.commons.mail.EmailException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import java.io.IOException;

public class FTPDelete {
    public void delete(String username, String password, String host, String path) {
        //refactor:net.schmizz
        {
            FTPClient f = new FTPClient();
            f.connect(host);
            f.login(username, password);
            f.deleteFile(path);
            f.disconnect();
        }
    }
}
