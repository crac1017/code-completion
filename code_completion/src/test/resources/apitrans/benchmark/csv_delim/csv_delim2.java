import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CSV7 {
    public String[] read_csv(FileReader reader, char sep) {
        //refactor:expected
        {
            CSVReader csv_reader = new CSVReader(reader, sep);
            String[] fields = csv_reader.readNext();
        }

        return fields;
    }
}