package edu.rice.pliny.apitrans.examples;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


public class HTMLAddNode {

    public void add_node(String content, String new_html) {
        //refactor:expected
        {
            Document doc = Jsoup.parse(content);
            Element e = doc.prepend(new_html);
        }
    }
}
