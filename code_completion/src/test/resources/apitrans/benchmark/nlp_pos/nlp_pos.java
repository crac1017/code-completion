import com.aliasi.hmm.HiddenMarkovModel;
import com.aliasi.hmm.HmmDecoder;
import com.aliasi.sentences.MedlineSentenceModel;
import com.aliasi.sentences.SentenceModel;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.Tokenizer;
import com.aliasi.tokenizer.TokenizerFactory;

import java.io.File;
import java.util.Scanner;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.Reader;

public class NLPToken {
    public void nlp_token(String[] tokens, File model_file, HiddenMarkovModel hmm) {
        //refactor:opennlp
        {
            HmmDecoder decoder = new HmmDecoder(hmm);
            decoder.tag(tokens);
        }
    }
}