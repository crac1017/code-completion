import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import java.io.File;
import java.util.Scanner;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.Reader;

public class NLPToken {
    public void nlp_token(String[] tokens, File model_file) {
        //refactor:expected
        {
            POSModel model = new POSModel(model_file);
            POSTaggerME tagger = new POSTaggerME(model);
            String[] t = tagger.tag(tokens);
        }
    }
}