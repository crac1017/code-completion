import org.apache.commons.net.imap.IMAPClient;

import javax.mail.PasswordAuthentication;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

public class EmailCheck {

    public void login(String username, String password, Properties prop, String hostname, String folder) {
        //refactor:javax.mail
        {
            IMAPClient client = new IMAPClient();
            client.connect(hostname);
            client.login(username, password);
            client.select(folder);
            client.check();
            client.logout();
            client.disconnect();
        }
    }
}
