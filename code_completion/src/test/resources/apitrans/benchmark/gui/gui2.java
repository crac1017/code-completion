import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import javax.swing.JButton;

public class GUI {

  /**
   * COMMENT:
   * sending email using java
   *
   * TEST:
   * source("code_completion/src/test/resources/apitrans/gui/testlib.java");
   * __pliny_solution__
   * source("code_completion/src/test/resources/apitrans/gui/test.java");
   */
  public void run() {
      String frame_title = "frame";
      String label = "label";
      String btn_label = "btn";
      int w = 300;
      int h = 250;

      Stage stg = new Stage();
      stg.setTitle(frame_title);
      StackPane root = new StackPane();
      ObservableList children = root.getChildren();
      Button btn = new Button(btn_label);
      children.add(btn);
      Label lbl = new Label(label);
      children.add(lbl);

      //refactor:expected
      {
          Scene scene = new Scene(root, 300, 250);
          stg.setScene(scene);
          stg.show();
      }
  }
}
