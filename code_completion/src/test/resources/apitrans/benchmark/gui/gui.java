import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import javax.swing.JButton;

public class GUI {

  /**
   * COMMENT:
   * sending email using java
   *
   * TEST:
   * source("code_completion/src/test/resources/apitrans/benchmark/gui/testlib.java");
   * __pliny_solution__
   * source("code_completion/src/test/resources/apitrans/benchmark/gui/test.java");
   */
  public void run() {
      String frame_title = "frame";
      String label = "label";
      String btn_label = "btn";
      double w = 300;
      double h = 200;

      // Stage stg = new Stage();
      // stg.setTitle(frame_title);
      StackPane root = new StackPane();
      ObservableList children = root.getChildren();
      Button btn = new Button(btn_label);
      children.add(btn);
      Label lbl = new Label(label);
      children.add(lbl);
      // Scene scene = new Scene(root, 300, 250);
      // stg.setScene(scene);
      // stg.show();

      //refactor:javafx
      {
          JFrame f = new JFrame(frame_title);
          f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
          f.setLayout(new FlowLayout());
          JLabel jlabel = new JLabel(label);
          f.add(jlabel);
          JButton jbtn = new JButton(btn_label);
          f.add(jbtn);
          f.pack();
          f.setVisible(true);
      }
  }
}
