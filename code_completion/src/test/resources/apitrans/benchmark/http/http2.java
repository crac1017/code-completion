import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import java.net.URL;

public class HTTP {
    public void http_get(URL url) {
        //refactor:expected
        {
            OkHttpClient client = new OkHttpClient();
            okhttp3.Request.Builder builder = new okhttp3.Request.Builder();
            builder.url(url);
            okhttp3.Request request = builder.build();
            Call call = client.newCall(request);
            okhttp3.Response response = call.execute();
            okhttp3.ResponseBody rbody = response.body();
            String text = rbody.string();
        }
    }
}
