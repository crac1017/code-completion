import smile.classification.LogisticRegression;
import jsat.regression.RegressionDataSet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MLClassification {
    public void classify(double[][] x, double[] y, RegressionDataSet train_data) {
        //refactor:jsat
        {
            LogisticRegression.Trainer trainer = new LogisticRegression.Trainer();
            LogisticRegression model = trainer.train(x, y);
            model.predict(x[0]);
        }
    }
}
