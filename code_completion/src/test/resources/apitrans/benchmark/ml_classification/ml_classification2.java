import jsat.classifiers.CategoricalResults;
import jsat.regression.LogisticRegression;
import jsat.regression.RegressionDataSet;
import jsat.regression.RidgeRegression;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MLClassification {
    public void classify(double lambda, RegressionDataSet train_data, double[][] x, double[] y) {
        //refactor:expected
        {
            LogisticRegression model = new LogisticRegression();
            model.train(train_data);
            DataPoint dp = train_data.getDataPoint(0);
            CategoricalResults r = model.classify(dp);
        }
    }
}
