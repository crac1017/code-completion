package edu.rice.pliny.apitrans.examples;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.docx4j.jaxb.Context;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.Body;
import org.docx4j.wml.Document;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.Text;
import org.docx4j.wml.R;
import org.junit.Test;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;


public class Word4 {
    /**
     * COMMENT:
     * read a pdf file
     *
     * TEST:
     * import java.io.FileReader;
     * import java.io.BufferedReader;
     * import java.io.FileInputStream;
     * import java.io.FileOutputStream;
     * import java.io.IOException;
     * import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
     * import org.apache.poi.xwpf.usermodel.XWPFDocument;
     * import org.apache.poi.xwpf.usermodel.XWPFParagraph;
     * import org.apache.poi.xwpf.usermodel.XWPFRun;
     * import org.xml.sax.ContentHandler;
     * String read_text(String filename) {
     *   FileInputStream stream = new FileInputStream(filename);
     *   XWPFDocument docx = new XWPFDocument(stream);
     *   XWPFWordExtractor we = new XWPFWordExtractor(docx);
     *   return we.getText();
     * }
     * __pliny_solution__
     *
     * String path = "created.docx";
     * String content = "foobar";
     * create_doc(path, content);
     * String text = read_text(path).trim();
     * File f = new File(path);
     * boolean deleted = f.delete();
     * boolean _result_ = text.equals(content);
     */
    public void create_doc(String filename, String content) {
        File f = new File(filename);

        //refactor:org.apache.poi
        {
            WordprocessingMLPackage word_package = new WordprocessingMLPackage();
            MainDocumentPart mainDocumentPart = new MainDocumentPart();
            ObjectFactory factory = Context.getWmlObjectFactory();
            Body body = factory.createBody();
            Document doc = factory.createDocument();
            // add a new paragraph
            P paragraph = factory.createP();
            Text text = factory.createText();
            text.setValue(content);
            R run = factory.createR();
            List run_content = run.getContent();
            run_content.add(text);
            List para_content = paragraph.getContent();
            para_content.add(run);
            doc.setBody(body);
            mainDocumentPart.setJaxbElement(doc);
            List doc_content = mainDocumentPart.getContent();
            doc_content.add(0, paragraph);
            word_package.addTargetPart(mainDocumentPart);
            word_package.save(f);
        }
    }
}
