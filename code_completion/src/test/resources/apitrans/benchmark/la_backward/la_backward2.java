import org.la4j.Matrix;
import org.la4j.matrix.DenseMatrix;

public class FCLayer {
    static public Object[] backward(Matrix input, Matrix weight, Matrix bias, Matrix grad_output, Matrix ones){
        Matrix grad_input, grad_weight, grad_bias;

        //refactor:expected
        {
            grad_input = grad_output.multiply(weight);
            Matrix grad_weight_transpose = grad_output.transpose();
            grad_weight = grad_weight_transpose.multiply(input);
            Matrix grad_ouput_transpose = grad_output.transpose();
            grad_bias = grad_ouput_transpose.multiply(ones);
        }

        return new Object[]{grad_input, grad_weight, grad_bias};
    }
}