import Jama.*;

public class FCLayer {
    static public Object[] backward(Matrix input, Matrix weight, Matrix bias, Matrix grad_output, Matrix ones){
        Matrix grad_input, grad_weight, grad_bias;

        //refactor:org.la4j
        {
            grad_input = grad_output.times(weight);
            Matrix grad_weight_transpose = grad_output.transpose();
            grad_weight = grad_weight_transpose.times(input);
            Matrix grad_ouput_transpose = grad_output.transpose();
            grad_bias = grad_ouput_transpose.times(ones);
        }
        return new Object[]{grad_input, grad_weight, grad_bias};
    }
}