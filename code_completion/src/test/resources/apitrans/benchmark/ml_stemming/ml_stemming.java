import jsat.regression.RegressionDataSet;
import smile.nlp.stemmer.PorterStemmer;
import smile.nlp.stemmer.Stemmer;
import smile.regression.RidgeRegression;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MLStemming {
    public void stemming(String word) {
        //refactor:jsat
        {
            Stemmer stemmer = new PorterStemmer();
            stemmer.stem(word);
        }
    }
}
