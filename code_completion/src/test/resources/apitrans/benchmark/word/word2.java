package edu.rice.pliny.apitrans.examples;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.apache.tika.parser.Parser;


public class Word2 {
    /**
     * COMMENT:
     * read a pdf file
     *
     * TEST:
     * import java.io.FileReader;
     * import java.io.BufferedReader;
     * import java.io.FileInputStream;
     * import java.io.IOException;
     * import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
     * import org.apache.poi.xwpf.usermodel.XWPFDocument;
     * import org.apache.tika.config.TikaConfig;
     * import org.apache.tika.io.TikaInputStream;
     * import org.apache.tika.metadata.Metadata;
     * import org.apache.tika.parser.ParseContext;
     * import org.apache.tika.sax.BodyContentHandler;
     * import org.xml.sax.ContentHandler;
     * __pliny_solution__
     *
     * String result = read_text("code_completion/src/test/resources/apitrans/benchmark/word/foo.docx");
     * boolean _case_0_ = result.trim().equals("Word document");
     * boolean _result_ = _case_0_;
     */
    public String read_text(String filename) {
        File f = new File(filename);
        FileInputStream stream = new FileInputStream(filename);

        //refactor:expected
        {
            XWPFDocument docx = new XWPFDocument(stream);
            XWPFWordExtractor we = new XWPFWordExtractor(docx);
            String text = we.getText();
        }

        return text;
    }
}
