boolean has_server = false;
boolean has_handler = false;
boolean has_setup = false;
boolean has_start = false;

public class InetSocketAddress {
    public InetSocketAddress(int p) {}
};


public class HttpHandler {
    public HttpHandler() {
        has_handler = true;
    }
}

public class ServerConfiguration {
    public ServerConfiguration() {}
    public void addHttpHandler(HttpHandler handler) {
        has_setup = true;
    }
}

public class HttpServer {
    public HttpServer() {}
    static public HttpServer create(InetSocketAddress addr, int i) {
        has_server = true;
        return new HttpServer();
    }

    static public HttpServer createSimpleServer() {
        has_server = true;
        return new HttpServer();
    }

    public void start() {
        if(has_setup) {
            has_start = true;
        }
    }

    public void createContext(String url, HttpHandler handler) {
        if(!has_start) {
            has_setup = true;
        }
    }

    public ServerConfiguration getServerConfiguration() {
        return new ServerConfiguration();
    }
}

