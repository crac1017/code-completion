import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.ServerConfiguration;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class HttpServerBench {

  public void run() {
      int port = 8000;
      String url = "/test";
      HttpHandler handler = new HttpHandler();

      //refactor:org.glassfish
      {
          InetSocketAddress address = new InetSocketAddress(port);
          HttpServer server = HttpServer.create(address, 0);
          server.createContext(url, handler);
          server.start();
      }
  }
}
