import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.ServerConfiguration;

public class HttpServerBench {

  /**
   * COMMENT:
   * sending email using java
   *
   * TEST:
   * source("code_completion/src/test/resources/apitrans/httpserver/testlib.java");
   * __pliny_solution__
   * source("code_completion/src/test/resources/apitrans/httpserver/test.java");
   */
  public void run() {
      int port = 8000;
      String url = "/test";
      HttpHandler handler = new HttpHandler();

      //refactor:expected
      {
          HttpServer server = HttpServer.createSimpleServer();
          ServerConfiguration config = server.getServerConfiguration();
          config.addHttpHandler(handler);
          server.start();
      }
  }
}
