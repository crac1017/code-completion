import jsat.classifiers.CategoricalResults;
import jsat.classifiers.neuralnetwork.BackPropagationNet;
import jsat.regression.LogisticRegression;
import jsat.regression.RegressionDataSet;
import jsat.regression.RidgeRegression;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MLNeural {
    public void neural(RegressionDataSet train_data, double[][] x, double[] y) {
        //refactor:expected
        {
            BackPropagationNet model = new BackPropagationNet();
            model.train(train_data);
            DataPoint dp = train_data.getDataPoint(0);
            CategoricalResults r = model.classify(dp);
        }
    }
}
