import smile.classification.NeuralNetwork;
import jsat.regression.RegressionDataSet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MLNeural {
    public void neural(double[][] x, double[] y, RegressionDataSet train_data) {
        //refactor:jsat
        {
            NeuralNetwork.Trainer trainer = new NeuralNetwork.Trainer(NeuralNetwork.ErrorFunction.LEAST_MEAN_SQUARES, 1);
            NeuralNetwork model = trainer.train(x, y);
            model.predict(x[0]);
        }
    }
}
