import jsat.regression.RegressionDataSet;
import smile.clustering.KMeans;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MLCluster {
    public void cluster(double[][] x, int k, RegressionDataSet train_data) {
        //refactor:jsat
        {
            KMeans model = new KMeans(x, k);
            model.predict(x[0]);
        }
    }
}
