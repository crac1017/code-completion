import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.type.VoidType;
import spoon.reflect.code.CtFieldRead;
import spoon.reflect.code.CtTypeAccess;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.factory.CodeFactory;
import spoon.reflect.factory.CoreFactory;
import spoon.reflect.factory.Factory;
import spoon.reflect.factory.FactoryImpl;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.support.DefaultCoreFactory;
import spoon.support.StandardEnvironment;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class JParser {

    /**
     * COMMENT:
     * Creating a program with buttons and counters
     *
     * TEST:
     * import com.github.javaparser.ast.Modifier;
     * import com.github.javaparser.ast.NodeList;
     * import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
     * import com.github.javaparser.ast.body.MethodDeclaration;
     * import com.github.javaparser.ast.expr.FieldAccessExpr;
     * import com.github.javaparser.ast.expr.MethodCallExpr;
     * import com.github.javaparser.ast.expr.NameExpr;
     * import com.github.javaparser.ast.expr.StringLiteralExpr;
     * import com.github.javaparser.ast.stmt.BlockStmt;
     * import com.github.javaparser.ast.stmt.ExpressionStmt;
     * import com.github.javaparser.ast.type.VoidType;
     * import spoon.reflect.code.CtFieldRead;
     * import spoon.reflect.code.CtTypeAccess;
     * import spoon.reflect.code.CtInvocation;
     * import spoon.reflect.declaration.CtClass;
     * import spoon.reflect.declaration.CtMethod;
     * import spoon.reflect.factory.CodeFactory;
     * import spoon.reflect.factory.CoreFactory;
     * import spoon.reflect.factory.Factory;
     * import spoon.reflect.factory.FactoryImpl;
     * import spoon.reflect.reference.CtExecutableReference;
     * import spoon.reflect.reference.CtTypeReference;
     * import spoon.support.DefaultCoreFactory;
     * import spoon.support.StandardEnvironment;
     * import java.io.PrintStream;
     * __pliny_solution__
     * String code = make_call();
     * boolean _result_ = code.contains("System.out.println()");
     */
    public String make_call() {
        String sys_str = "System";
        String out_str = "out";
        String print_str = "println";

        //refactor:expected
        {
            NameExpr system_name = new NameExpr(sys_str);
            FieldAccessExpr out_field = new FieldAccessExpr(system_name, out_str);
            NodeList args = new NodeList();
            MethodCallExpr call = new MethodCallExpr(out_field, print_str, args);
            ExpressionStmt stmt = new ExpressionStmt(call);
            String result = stmt.toString();
        }

        return result;
    }
}
