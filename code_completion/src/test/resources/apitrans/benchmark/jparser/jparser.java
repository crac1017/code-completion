package edu.rice.pliny.apitrans.examples;

import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.FieldAccessExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.ast.type.VoidType;
import spoon.reflect.code.CtFieldRead;
import spoon.reflect.code.CtTypeAccess;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.code.CtInvocation;
import spoon.reflect.factory.CodeFactory;
import spoon.reflect.factory.CoreFactory;
import spoon.reflect.factory.Factory;
import spoon.reflect.factory.FactoryImpl;
import spoon.reflect.reference.CtExecutableReference;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;
import spoon.support.DefaultCoreFactory;
import spoon.support.StandardEnvironment;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class JParser {

    /**
     * COMMENT:
     * Creating a program with buttons and counters
     *
     * TEST:
     * import com.github.javaparser.ast.Modifier;
     * import com.github.javaparser.ast.NodeList;
     * import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
     * import com.github.javaparser.ast.body.MethodDeclaration;
     * import com.github.javaparser.ast.expr.FieldAccessExpr;
     * import com.github.javaparser.ast.expr.MethodCallExpr;
     * import com.github.javaparser.ast.expr.NameExpr;
     * import com.github.javaparser.ast.expr.StringLiteralExpr;
     * import com.github.javaparser.ast.expr.Expression;
     * import com.github.javaparser.ast.stmt.BlockStmt;
     * import com.github.javaparser.ast.stmt.ExpressionStmt;
     * import com.github.javaparser.ast.type.VoidType;
     * import spoon.reflect.code.CtFieldRead;
     * import spoon.reflect.code.CtTypeAccess;
     * import spoon.reflect.code.CtInvocation;
     * import spoon.reflect.declaration.CtClass;
     * import spoon.reflect.declaration.CtMethod;
     * import spoon.reflect.factory.CodeFactory;
     * import spoon.reflect.factory.CoreFactory;
     * import spoon.reflect.factory.Factory;
     * import spoon.reflect.factory.FactoryImpl;
     * import spoon.reflect.reference.CtExecutableReference;
     * import spoon.reflect.reference.CtTypeReference;
     * import spoon.support.DefaultCoreFactory;
     * import spoon.support.StandardEnvironment;
     * import java.io.PrintStream;
     * __pliny_solution__
     * String code = make_call();
     * boolean _result_ = code.contains("System.out.println()");
     */
    public String make_call() {
        String sys_str = "System";
        String out_str = "out";
        String print_str = "println";

        //refactor:com.github.javaparser
        {
            FactoryImpl factory = new FactoryImpl(new DefaultCoreFactory(), new StandardEnvironment());
            CoreFactory core = factory.Core();
            CodeFactory code = factory.Code();
            CtFieldRead target = core.createFieldRead();
            CtTypeReference system_type = code.createCtTypeReference(System.class);
            CtTypeAccess type_access = code.createTypeAccess(system_type);
            target.setTarget(type_access);
            CtFieldReference ref = core.createFieldReference();
            ref.setSimpleName("out");
            target.setVariable(ref);
            CtExecutableReference print_method = core.createExecutableReference();
            CtTypeReference pstream_type = code.createCtTypeReference(PrintStream.class);
            print_method.setType(pstream_type);
            print_method.setSimpleName("println");
            CtInvocation call = code.createInvocation(target, print_method);
            String result = call.toString();
        }

        return result;
    }
}
