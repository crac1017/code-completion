import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import java.io.File;
import java.util.Scanner;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.Reader;

public class NLPToken {
    public void nlp_token(String content, File model_file) {
        //refactor:expected
        {
            TokenizerModel model = new TokenizerModel(model_file);
            TokenizerME tokenizer = new TokenizerME(model);
            String[] t = tokenizer.tokenize(content);
        }
    }
}