import com.aliasi.sentences.MedlineSentenceModel;
import com.aliasi.sentences.SentenceModel;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.Tokenizer;
import com.aliasi.tokenizer.TokenizerFactory;

import java.io.File;
import java.util.Scanner;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.Reader;

public class NLPToken {
    public void nlp_token(String content, File model_file) {
        //refactor:opennlp
        {
            TokenizerFactory tokenizer_fac = IndoEuropeanTokenizerFactory.INSTANCE;
            Tokenizer tokenizer = tokenizer_fac.tokenizer(content.toCharArray(), 0, content.length());
            tokenizer.tokenize();
        }
    }
}