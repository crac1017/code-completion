import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.aead.AeadKeyTemplates;
import com.google.crypto.tink.Aead;
import com.google.crypto.tink.aead.AeadFactory;

import java.security.*;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.InputStream;
import java.util.Base64;

public class Secret {

    static public String encrypt(String plain, String secret) {
        String cipher;

        //refactor:java
        {
            byte[] plain_bytes = plain.getBytes();
            byte[] secret_bytes = secret.getBytes();
            KeysetHandle handle = KeysetHandle.generateNew(AeadKeyTemplates.AES128_GCM);
            Aead aead = AeadFactory.getPrimitive(handle);
            byte[] cipher_bytes = aead.encrypt(plain_bytes, secret_bytes);
            cipher = new String(cipher_bytes);
        }

        return cipher;
    }
}
