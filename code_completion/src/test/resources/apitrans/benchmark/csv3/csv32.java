import java.io.File;
import java.util.Scanner;
import java.io.FileReader;
import com.opencsv.CSVWriter;

public class CSV3 {
    /**
     * COMMENT:
     * Reading a csv file
     *
     * TEST:
     * import com.opencsv.CSVReader;
     * import com.opencsv.CSVWriter;
     * import java.io.BufferedReader;
     * import java.io.File;
     * import java.io.FileReader;
     * import java.io.IOException;
     * public String[] read_csv(String filename) {
     *   File f = new File(filename);
     *   FileReader reader = new FileReader(f);
     *   CSVReader csv_reader = new CSVReader(reader);
     *   String[] fields = csv_reader.readNext();
     *   return fields;
     * }
     * __pliny_solution__
     *
     * String filename = "code_completion/src/test/resources/apitrans/csv3/created.csv";
     * String[] ans = new String[]{"1", "2", "3", "4"};
     * File f = new File(filename);
     * if(f.exists()) {
     *   f.delete();
     * }
     * write_csv(filename);
     * String[] result = read_csv(filename);
     * boolean _result_ = true;
     * for(int i = 0; i < result.length; ++i) {
     *   if(!result[i].equals(ans[i])) {
     *       _result_ = false;
     *   }
     * }
     */
    public void write_csv(String filename) throws IOException {
        String[] entries = new String[]{"1", "2", "3", "4"};
        FileWriter fw = new FileWriter(filename);

        //refactor:expected
        {
            CSVWriter writer = new CSVWriter(fw);
            writer.writeNext(entries);
            writer.close();
        }
    }
}