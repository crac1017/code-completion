import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import org.apache.commons.mail.EmailException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import javax.mail.MessagingException;
import java.io.IOException;
import java.io.InputStream;

public class FTPUpload {

    public void upload(String username, String password, String host, String path, String filename, InputStream content) {

        //refactor:expected
        {
            SSHClient ssh = new SSHClient();
            SFTPClient ftp = ssh.newSFTPClient();
            ssh.authPassword(username, password);
            ssh.connect(host);
            ftp.put(filename, path);
            ssh.disconnect();
        }
    }
}
