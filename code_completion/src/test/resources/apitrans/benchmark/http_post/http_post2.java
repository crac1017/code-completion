import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.RequestBody;
import java.net.URL;

public class HTTPPost {
    public void post(URL url, RequestBody body) {
        //refactor:expected
        {
            OkHttpClient client = new OkHttpClient();
            Request.Builder builder = new Request.Builder();
            builder.post(body);
            builder.url(url);
            Request request = builder.build();
            Call call = client.newCall(request);
            Response response = call.execute();
            ResponseBody rbody = response.body();
            String text = rbody.string();
        }
    }
}
