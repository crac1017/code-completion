import com.hp.gagawa.java.elements.Html;
import com.hp.gagawa.java.elements.Head;
import com.hp.gagawa.java.elements.Title;
import com.hp.gagawa.java.elements.Body;
import com.hp.gagawa.java.elements.H1;
import com.hp.gagawa.java.elements.Div;
import com.hp.gagawa.java.elements.P;
import j2html.tags.ContainerTag;

public class HTML3 {

    /**
     * COMMENT:
     * Creating a program with buttons and counters
     *
     * TEST:
     * import com.hp.gagawa.java.elements.Html;
     * import com.hp.gagawa.java.elements.Head;
     * import com.hp.gagawa.java.elements.Title;
     * import com.hp.gagawa.java.elements.Body;
     * import com.hp.gagawa.java.elements.H1;
     * import com.hp.gagawa.java.elements.Div;
     * import com.hp.gagawa.java.elements.P;
     * import j2html.tags.ContainerTag;
     * __pliny_solution__
     * String html = create_html();
     * String ans = "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title>title</title></head><body><h1>header</h1><div><p>foobar</p></div></body></html>";
     * boolean _result_ = html.equals(ans);
     */
    public String create_html() {
        String content_str = "foobar";
        String header_str = "header";
        String title_str = "title";

        Html html = new Html();
        Head head = new Head();
        Title title = new Title();
        title.appendText(title_str);
        head.appendChild(title);
        html.appendChild(head);
        Body body = new Body();
        H1 h1 = new H1();
        h1.appendText(header_str);
        Div div = new Div();
        P p = new P();
        p.appendText(content_str);
        div.appendChild(p);

        //refactor:expected
        {
            body.appendChild(h1);
            body.appendChild(div);
            html.appendChild(body);
            String result = html.write();
        }

        return result;
    }
}
