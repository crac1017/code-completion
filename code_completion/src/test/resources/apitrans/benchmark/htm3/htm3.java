import com.hp.gagawa.java.elements.Html;
import com.hp.gagawa.java.elements.Head;
import com.hp.gagawa.java.elements.Title;
import com.hp.gagawa.java.elements.Body;
import com.hp.gagawa.java.elements.H1;
import com.hp.gagawa.java.elements.Div;
import com.hp.gagawa.java.elements.P;
import j2html.tags.ContainerTag;
import j2html.TagCreator;

public class HTML3 {

    /**
     * COMMENT:
     * Creating a program with buttons and counters
     *
     * TEST:
     * import com.hp.gagawa.java.elements.Html;
     * import com.hp.gagawa.java.elements.Head;
     * import com.hp.gagawa.java.elements.Title;
     * import com.hp.gagawa.java.elements.Body;
     * import com.hp.gagawa.java.elements.H1;
     * import com.hp.gagawa.java.elements.Div;
     * import com.hp.gagawa.java.elements.P;
     * import j2html.tags.ContainerTag;
     * __pliny_solution__
     * String html = create_html();
     * String ans = "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title>title</title></head><body><h1>header</h1><div><p>foobar</p></div></body></html>";
     * boolean _result_ = html.equals(ans);
     */
    public String create_html() {
        String content_str = "foobar";
        String header_str = "header";
        String title_str = "title";
        Html html2 = new Html();
        Head head2 = new Head();
        Title title2 = new Title();
        title2.appendText(title_str);
        head2.appendChild(title2);
        html2.appendChild(head2);
        Body body2 = new Body();
        H1 h12 = new H1();
        h12.appendText(header_str);
        Div div2 = new Div();
        P p2 = new P();
        p2.appendText(content_str);
        div2.appendChild(p2);

        // comment these out to increase difficulty
        body2.appendChild(h12);
        body2.appendChild(div2);
        html.appendChild(body2);
        // String result = html.write();

        //refactor:com.hp.gagawa
        {
            ContainerTag ctitle = TagCreator.title(title_str);
            ContainerTag chead = TagCreator.head(ctitle);
            ContainerTag ch1 = TagCreator.h1(header_str);
            ContainerTag cp = TagCreator.p(content_str);
            ContainerTag cdiv = TagCreator.div(cp);
            ContainerTag cbody = TagCreator.body(ch1, cdiv);
            ContainerTag chtml = TagCreator.html(chead, cbody);
            String result = html.render();
        }

        return result;
    }
}
