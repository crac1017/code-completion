//Library link: http://la4j.org/
import org.la4j.Matrix;
import org.la4j.matrix.DenseMatrix;

public class FCLayer {

    static public Matrix forward(Matrix input, Matrix weight, Matrix bias, Matrix ones){
        Matrix result;
        //refactor:expected
        {
            Matrix step1 = weight.transpose();
            Matrix step2 = input.multiply(step1);
            Matrix bias_transpose = bias.transpose();
            Matrix step3 = ones.multiply(bias_transpose);
            result = step2.add(step3);
        }
        return result;
    }
}