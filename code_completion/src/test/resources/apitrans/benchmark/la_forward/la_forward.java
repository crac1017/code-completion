import Jama.*;

//Library link: https://math.nist.gov/javanumerics/jama/

public class FCLayer {

    static public Matrix forward(Matrix input, Matrix weight, Matrix bias, Matrix ones){
        Matrix result;

        //refactor:org.la4j
        {
            Matrix step1 = weight.transpose();
            Matrix step2 = input.times(step1);
            Matrix bias_transpose = bias.transpose();
            Matrix step3 = ones.times(bias_transpose);
            result = step2.plus(step3);
        }
        return result;
    }
}