import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class EVisitor extends ASTVisitor {
    public int count = 0;
    public boolean visit(org.eclipse.jdt.core.dom.TypeDeclaration node) {
        count += 1;
        return true;
    }
}

public class JPVisitor extends VoidVisitorAdapter {
    public int count = 0;

    public void visit(ClassOrInterfaceDeclaration node, Object args) {
        count += 1;
    }
}

public class JParser3 {

    /**
     * COMMENT:
     * Creating a program with buttons and counters
     *
     * TEST:
     * import com.github.javaparser.JavaParser;
     * import com.github.javaparser.ast.CompilationUnit;
     * import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
     * import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
     * import org.eclipse.jdt.core.dom.AST;
     * import org.eclipse.jdt.core.dom.ASTParser;
     * import org.eclipse.jdt.core.dom.ASTVisitor;
     * import java.io.IOException;
     * import java.io.BufferedReader;
     * import java.io.FileReader;
     * import java.io.FileNotFoundException;
     * import java.io.File;
     * import java.util.HashMap;
     * import java.util.Map;
     * source("code_completion/src/test/resources/apitrans/benchmark/jparser3/testlib.java");
     * __pliny_solution__
     * String prog = "code_completion/src/test/resources/apitrans/benchmark/jparser3/foo.java";
     * int count = count(prog);
     * boolean _result_ = count == 3;
     */
    public int count(String prog) {
        int jversion = AST.JLS3;
        int kind = ASTParser.K_COMPILATION_UNIT;
        String src = read_file(prog);
        Map options = new HashMap();
        char[] src_char = src.toCharArray();


        // comment the code to increase difficulty
        ASTParser parser = ASTParser.newParser(jversion);
        parser.setCompilerOptions(options);
        parser.setKind(kind);
        // parser.setSource(src_char);

        //refactor:org.eclipse
        {
            File f = new File(prog);
            JPVisitor jpv = new JPVisitor();
            CompilationUnit cu = JavaParser.parse(f);
            cu.accept(jpv, null);
        }

        org.eclipse.jdt.core.dom.CompilationUnit cu = (org.eclipse.jdt.core.dom.CompilationUnit) parser.createAST(null);
        EVisitor ev = new EVisitor();
        cu.accept(ev);
        int result = ev.count;
        return result;
    }
}
