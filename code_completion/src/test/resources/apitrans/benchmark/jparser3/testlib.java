//read file content into a string
String read_file(String filePath) throws IOException {
    StringBuilder fileData = new StringBuilder(1000);
    BufferedReader reader = new BufferedReader(new FileReader(filePath));

    char[] buf = new char[10];
    int numRead = -1;
    while ((numRead = reader.read(buf)) != -1) {
        // System.out.println(numRead);
        String readData = String.valueOf(buf, 0, numRead);
        fileData.append(readData);
        buf = new char[1024];
    }

    reader.close();
    return fileData.toString();
}
public class EVisitor extends ASTVisitor {
    public int count = 0;
    public boolean visit(org.eclipse.jdt.core.dom.TypeDeclaration node) {
        count += 1;
        return true;
    }
}

public class JPVisitor extends VoidVisitorAdapter {
    public int count = 0;

    public void visit(ClassOrInterfaceDeclaration node, Object args) {
        count += 1;
    }
}
