import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import org.apache.commons.mail.EmailException;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import java.io.IOException;

public class FTPList {

    public void connect(String username, String password, String host, String path) {
        {
            FTPClient f = new FTPClient();
            f.connect(host);
            f.disconnect();
        }
    }

    public void list_files(String username, String password, String host, String path) {
        //refactor:net.schmizz
        {
            FTPClient f = new FTPClient();
            f.connect(host);
            f.login(username, password);
            FTPFile[] files = f.listFiles(path);
            f.disconnect();
        }
    }
}
