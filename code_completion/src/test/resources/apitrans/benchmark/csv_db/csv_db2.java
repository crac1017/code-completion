import java.io.File;
import java.sql.ResultSet;
import java.util.Scanner;
import java.io.FileReader;
import com.opencsv.CSVWriter;

public class CSV5 {

    public void writedb(ResultSet result, FileWriter fw, boolean include) throws IOException {
        //refactor:expected
        {
            CSVWriter writer = new CSVWriter(fw);
            writer.writeAll(result, include);
            writer.close();
        }
    }
}