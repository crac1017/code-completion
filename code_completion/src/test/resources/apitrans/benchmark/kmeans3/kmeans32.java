import ca.pjer.ekmeans.EKmeans;
import smile.clustering.KMeans;

public class KMeans3 {

  /**
   * COMMENT:
   * sending email using java
   *
   * TEST:
   * source("code_completion/src/test/resources/apitrans/kmeans3/testlib.java");
   * __pliny_solution__
   * source("code_completion/src/test/resources/apitrans/kmeans3/test.java");
   */
  public void run() {
      int n = 6; // the number of data to cluster
      int k = 3; // the number of cluster

      double[][] points = {{-10}, {-11}, {0}, {0}, {10}, {11}};
      double[][] centroids = {{-100}, {100}, {0}};

      //refactor:expected
      {
          EKmeans eKmeans = new EKmeans(centroids, points);
          eKmeans.setIteration(64);
          eKmeans.run();
      }
  }
}
