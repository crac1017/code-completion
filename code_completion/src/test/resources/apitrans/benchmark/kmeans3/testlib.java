import java.io.File;

boolean has_kmeans = false;
boolean has_iter = false;
boolean has_start = false;


/*
public class File {
    public File(String filename) {}
}
*/

public class EKmeans {
    public EKmeans(double[][] c, double[][] p) {
        has_kmeans = true;
    }

    public void setIteration(int i) {
        has_iter = true;
    }

    public void run() {
        has_start = true;
    }


}


public class KMeans {
    public KMeans(double[][] p, int k, int iter) {
        has_kmeans = true;
        has_iter = true;
    }

    public int predict(double[] p) {
        has_start = true;
        return 0;
    }
}