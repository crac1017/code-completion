import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;


public class PDFRead {
    public void read(String filename) {
        //refactor:org.apache.pdfbox
        {
            PdfReader reader = new PdfReader(filename);
            String text = PdfTextExtractor.getTextFromPage(reader, 1);
            reader.close();
        }
    }
}
