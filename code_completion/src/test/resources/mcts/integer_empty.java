import java.math.BigInteger;

public class Main {
    /**
     * This is a function adds two big integers
     *
     * COMMENT:
     * Adds two big integers
     *
     * TEST:
     * import java.math.BigInteger;
     * BigInteger i1 = new BigInteger("120");
     * BigInteger i2 = new BigInteger("20");
     * __pliny_solution__
     * BigInteger sum = add(i1, i2);
     * print(sum.intValue() == 140);
     */
    public BigInteger add(BigInteger i1, BigInteger i2) {
        ??
    }
}
