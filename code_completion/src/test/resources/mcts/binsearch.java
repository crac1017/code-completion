/**
 * This is the class's comment
 */
public class FooMain {

    public int binarySearch(int[] array, int x) {
        int result = -1;
        int low = 0;
        int high = array.length - 1;

        while(low <= high) {
            int mid = (low + high) / 2;
            if(array[mid] > x) {
                high = mid - 1;
            } else if(array[mid] < x) {
                low = mid + 1;
            } else {
                result = mid;
                break;
            }
        }

        return result;
    }
}
