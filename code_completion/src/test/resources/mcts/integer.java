import java.math.BigInteger;

public class Main {
    public BigInteger sub(BigInteger n1, BigInteger n2) {
        //BigInteger sum = i1.subtract(i2);
        BigInteger a1 = new BigInteger(n1.toString()).add(new BigInteger(n2.toString()));
        return a1;
    }
}
