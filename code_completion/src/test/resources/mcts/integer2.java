import java.math.BigInteger;

public class Main {
    public BigInteger sub(BigInteger n1, BigInteger n2) {
        BigInteger a1 = n1.add(n2);
        return a1;
    }
}
