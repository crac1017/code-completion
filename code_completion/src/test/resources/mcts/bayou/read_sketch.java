import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.Reader;

/**
 * This is the class's comment
 */
public class FooMain {

    /**
     * COMMENT:
     * comment here
     *
     * TEST:
     * print(true);
     */
    public int main(String filename) {
        __pliny_hole__(0, "void", new String[] {__pliny_hole__(1, "java.io.FileReader", new FileReader(__pliny_hole__(2, "java.lang.String")))});
        // FileReader reader = new FileReader(filename);

        __pliny_hole__(3, "void", new String[] {__pliny_hole__(4, "java.io.BufferedReader", new BufferedReader(__pliny_hole__(5, "java.io.Reader")))});
        // BufferedReader v_1 = new BufferedReader(v_0);

        while(__pliny_hole__(6, "boolean", new String[] { __pliny_hole__(12, "java.io.BufferedReader").readLine() })) {}
        // while(v_1.readLine() != null) {}

        __pliny_hole__(9, "void", new String[] {__pliny_hole__(10, "void", __pliny_hole__(11, "java.io.BufferedReader").close())});
        // __pliny_hole__(9, "void", new String[] { v_1.close() });
    }
}
