public class Main {
    public void foo(int[] array) {
        int result = 0;
        for(int i : array) {
            result += i;
        }
        return result;
    }
}
