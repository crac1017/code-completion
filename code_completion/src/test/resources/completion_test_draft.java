/**
 * This is the class's comment
 */
public class FooMain {

    /**
     * COMMENT:
     * this is the comment.
     *
     * TEST:
     * integer [] array = new integer [] {-100, -20, 0, 4, 100, 600, 601};
     * __pliny_solution__
     * boolean _result_ = (binsearch(array, -20) == 1 && binsearch(array, 4) == 3);
     */
    public int binsearch(int[] array, int x) {
        // this is the first line
        int result = -1;
        int low = 0;
        int high = ??;
        // integer high = array.length - 1;

        /**
         * COMMENT:
         * binsearch's while loop
         *
         * TEST:
         * integer[] array = new integer[] {-1, 2, 4, 6, 7, 10, 200, 3000};
         * integer x = 10;
         * integer result = -1;
         * integer low = 0;
         * integer high = array.length - 1;
         * __pliny_solution__
         * boolean _result_ = (result == 5);
         *
         */
        ??
        return result;
    }
}
