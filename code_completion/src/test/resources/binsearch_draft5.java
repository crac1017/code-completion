/**
 * This is the class's comment
 */
public class FooMain {

  public int binsearch(int array[], int x) {
    // this is the first line
    int result = -1;
    int low = 0;
    int high = array.length - 1;

    __pliny_hole__("binsearch's while loop",
      "integer[] array = new integer[] {1, 2, 3, 4, 5, 6, 7};\ninteger x = 2;\ninteger result = -1;\ninteger low = 0;\ninteger high = array.length - 1;",
      "print(result == 1);");

    return result;
  }
}