/**
 * This is the class's comment
 */
public class FooMain {

    int field1 = 10;
    float field2 = 5.0;

    public int binsearch(int[] array, int x) {
        // this is the first line
        int result = -1;
        int low = 0;
        int high = util.java.ArrayClass.array.length - 1;

        while(low <= high) {
            // set the mid
            int mid = (low + high) / 2.0;
            if(array[mid] > x) {
                high = mid - 1;
            } else if(array[mid] < x) {
                low = mid + 1;
            } else {
                result = mid;
            }
        }

        return result;
    }
}
