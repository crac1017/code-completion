/**
 * This is the class's comment
 */
public class FooMain {

    public int binsearch(int array[], int x) {
        // this is the first line
        int result = -1;
        int low = 0;
        int high = array.length - 1;

        /**
         * COMMENT:
         * binsearch's while loop
         *
         * TEST:
         * integer[] array = new integer[] {1, 3, 6, 7, 10, 20, 100};
         * integer x = 10;
         * integer result = -1;
         * integer low = 0;
         * integer high = array.length - 1;
         * __pliny_solution__
         * boolean _result_ = (result==4);
         */
        ??

        return result;
    }
}
