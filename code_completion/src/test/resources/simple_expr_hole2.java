/**
 * This is the class's comment
 */
public class FooMain {

    /**
     * INPUT:
     * integer [] array = new integer [] {-100, -20, 0, 4, 100, 600, 601};
     *
     * TEST:
     * print(binsearch(array, -20) == 1 && binsearch(array, 4) == 3);
     */
    public int binsearch(int[] array, int x) {
        // this is the first line
        int result = -1;
        int low = 0;
        // integer high = __pliny_hole__("binary search's high value");
        int high = array.length - 1;

        while(??) {
            // set the mid
            int mid = ??;
            if(array[mid] > x) {
                high = mid - 1;
            } else if(array[mid] < x) {
                low = mid + 1;
            } else {
                result = mid;
            }
        }

        return result;
    }
}
