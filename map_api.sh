#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Usage: map_api.sh [benchmark]"
    echo ""
    echo "Map a benchmark program using from a package into another"
	exit 1
fi 

env JAVA_OPTS="-Xmx32G" sbt --error "code_completion/runMain edu.rice.pliny.apitrans.MapAPIMain $1"
