# Data-driven Program Completion for Java Programs

This project uses a big codebase to automatically complete a partial
program. 

## Installation

This project uses the following softwares:

* Scala >= 2.10.3
* Nailgun: https://github.com/martylamb/nailgun
* SBT >= 0.13.3
* BeanShell https://github.com/beanshell/beanshell
* Pliny Database (PDB) and all the libraries it requires
* NLTK 3.0 with data downloaded: http://www.nltk.org

Before compiling the code, make sure to install the above
softwares. Specifically, Nailgun must be downloaded and compiled from
source. Please compile Naigun using `make` instead of `maven`. We
don't use the Java version. Then the Nailgun executable will be
generated and it must be placed at `src/main/resources/` and the
executable must be named `ng`. BeanShell is already included as a Jar
file at `src/main/resources/bsh-2.0b6.jar`, so no need to download and
install this one. 

## PDB and Other Configurations
In order for the program to use PDB, please compile PDB and put the
PDB interface named `pliny-query` at `src/main/resources/` and start
the PDB server. Please also put the database name and set name into
the config file which will be explained in below.

It is still possible to not use the PDB and use other sources to
retrieve program data as well. 

- One way is to use a local directory. It is possible to specify a
  local directory in the configuration file to be the program data
  source. If local directory is used, simply put Java source code into
  that directory and the program will retrieve programs from that
  directory. One example of such directory is
  `src/test/resources/corpus/binsearch`.

- Another way is to use a local JSON file that has at least one
  program element. This is also another way to test the output from
  PDB locally because PDB outputs program element JSONs. One example
  of such file is `src/test/resources/opencv.jsonl`.

We can configurate the application through the config file:

`src/main/resources/application.conf`

Here is a list of options available in the config file:
* `log_level = {"INFO", "DEBUG"}` - This is the log level for the application. 
* `log_to_console = {true, false}` - Whether the application logs to the console.
* `log_to_file` - A string representing the log file name. Cannot be empty.
* `interpreter`
  * `time_limit` - An integer representing the time limit for each
    candidate solution test run in millisecond. Default is 150. Idealy
    we want small values, because that will speed up the synthesis
    algorithm, but the interpreter needs time to test run candidate
    solutions. If this value is too small, interpreter would not be
    able to have enough time to test each candidate solution and would
    think the candidate solutions are incorrect because of timeout. If
    this is too large, the synthesis algorithm will be slow. 
* `code_completion`
  * `time_limit` - An integer representing the time limit of each synthesis task
  * `solution_num` - An integer representing the number of solutions
    returned for each synthesis task
  * `query_type = {"pdb", "dir", "json"}` - The source which the
    database programs come from. `"dir"` means the DB programs come
    from a local directory. `"pdb"` means the DB programs come from an
    instance of the PDB. This option requires PDB to be
    installed. `"json"` means the DB programs comes from a file where
    each line is a program element JSON.
  * `local_dir_path` - A string representing the local directory path.
  * `local_path_path` - A string representing the local JSON file path.
  * `output` = {"text", "json"} - Two types of output are supported. One is in
    JSON for easy program communication and another is text for easy
    human reading.
  * `db_prog_num` - This is the number of database program we will use.
* `pdb`
  * `db` - A string representing the name of the database used in PDB. 
  * `set` - A string representing the name of the set used in PDB.
  * `hostname` - The hostname of the PDB server. 
  * `port` - The port number of the PDB server. 
  * `logfile` - The log file for the PDB server.
  * `cluster_mode` - A boolean value indicating whether the PDB server
    is in cluster mode.
  * `memory` - The maximum amount of memory used by the PDB in megabytes.
  * `query` - Configs for querying PDB
    * `names` - Weight for Names feature
    * `weighted_nl_terms` - Weight for Weighted NL Terms feature
    * `weighted_nl_terms_intersect` - Weight for Weighted NL Terms
      feature where we only consider the intersection of two sets of
      terms
      


## Compilation and Running the Program 
To compile the program:

`sbt compile`

SBT will download all related dependencies and compile the program.

Before running any synthesis task or unit tests, start Java
interpreter server and make sure it runs during each synthesis task:

`./interp_server.sh`

To run all the unit tests in the project:

`./unit_test.sh`

To run the program:

`./complete.sh [draft filename]`

This program takes a filename to an incomplete Java program which
contains one or more holes and test cases. It will produce one or
more complete Java programs which satisfy the given test cases.

You can test the program using one of the sample test programs at
`src/test/resources/completion_test_stmt_draft.java`

To test the program, run the following command:
`./complete.sh src/test/resources/completion_test_stmt_draft.java`
