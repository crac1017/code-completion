#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Usage: query_mapper.sh [port]"
    echo ""
    echo "Start a refactoring web server"
	exit 1
fi 

env JAVA_OPTS="-Xmx32G" sbt --error "code_completion/runMain edu.rice.pliny.apitrans.WebMain $1"
