#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "Usage: train_mapper.sh [text path] [methods path]"
    echo ""
    echo "Train a Javadoc API mapper"
	exit 1
fi 

if [ ! -d "$1" ]; then
	echo "Text directory does not exist: $1"
	exit 1
fi 

if [ ! -d "$2" ]; then
	echo "Methods directory does not exist: $2"
	exit 1
fi 

env JAVA_OPTS="-Xmx32G" sbt --error "code_completion/runMain edu.rice.pliny.apitrans.TrainJavadocMapperMain $1 $2"
