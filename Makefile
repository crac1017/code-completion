# OSTYPE is not set by default on Mac OS X
ifndef OSTYPE
	OSTYPE = $(shell uname)
endif
# TARGETS = template

# different libraries for different OS
#ifeq ($(OSTYPE), Darwin)
#	LDFLAGS = `pkg-config fuse --libs`
#else
#	LDFLAGS = `pkg-config fuse --libs`
#endif

DOCKER_PREFIX = docker run 

# mount the volume
DOCKER_PREFIX += --rm
DOCKER_PREFIX += -v $(PWD):/app

# map 80 of the host to 17171 of the container 
DOCKER_PREFIX += -p 17171:17171
DOCKER_PREFIX += -it crac1017/code-completion:latest

DOCKER_BUILD = docker build -t crac1017/code-completion:latest .

.PHONY: clean all build

all:
	$(DOCKER_PREFIX) /bin/bash

build: Dockerfile
	docker build -t crac1017/code-completion:latest .

clean:
	$(DOCKER_PREFIX) sbt clean
