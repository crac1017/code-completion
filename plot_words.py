import sys;
from io import StringIO;
import numpy as np;
import csv;
import matplotlib.pyplot as plt;

# this is the set of words we are interested in
VOCAB = set([
    'string',
    'str',
    'char',
    'character',
    'double',
    'float',
    'integer',
    'byte',
    'boolean',
    'bool',
    'void',
    'short',
    'int',
    'long',
    'unsigned',
    'signed']);

def plot_words(filename):
    mat = [];
    words = [];
    count = 0;

    with open(filename, "r") as infile:
        reader = csv.reader(infile);
        for row in reader:
            word = row[2].strip();
            if word not in VOCAB:
                continue;

            mat.append([row[0], row[1]]);
            words.append(word);

    mat = np.asarray(mat, dtype='f');
    words = np.asarray(words);

    x_coords = mat[:, 0]
    y_coords = mat[:, 1]
    # display scatter plot
    plt.scatter(x_coords, y_coords)

    for label, x, y in zip(words, x_coords, y_coords):
        plt.annotate(label, xy=(x, y), xytext=(0, 0), textcoords='offset points')
        plt.xlim(x_coords.min()+0.00005, x_coords.max()+0.00005)
        plt.ylim(y_coords.min()+0.00005, y_coords.max()+0.00005)
    plt.show()

def main(args):
    filename = args[1];
    plot_words(filename);

if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit("""Usage: %s [words filename]
        Use matplotlib to plot words using scatter plot using only the words.
        """);
        
    main(sys.argv);


