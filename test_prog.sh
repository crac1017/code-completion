#!/bin/bash

if [ "$#" -ne 2 ]; then
	  echo "Usage: test_prog.sh [program pathname] [method name]"
    echo ""
    echo "Test the correctness of a method"
	  exit 1
fi 

if [ ! -f "$1" ]; then
	  echo "Source file does not exist: $1"
	  exit 1
fi 

sbt --error "run-main edu.rice.pliny.main.TestJavaProgMain $1 $2"
